#!/bin/bash

PROJECT_DIR=/home/babor/java/projects/VNe4
LICENSE_FILE=$PROJECT_DIR/license/VisNow
FILES=`find . -name "*.java"`

for f in $FILES; do
  cat $LICENSE_FILE > $f.new
  cat $f | sed -n -e '/package/,$p' >> $f.new
  mv $f.new $f
done


