//<editor-fold defaultstate="collapsed" desc=" License ">

/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.grid.gridftp;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Vector;
import org.globus.ftp.Buffer;
import org.globus.ftp.DataSink;
import org.globus.ftp.DataSource;
import org.globus.ftp.FileInfo;
import org.globus.ftp.Marker;
import org.globus.ftp.MarkerListener;
import org.globus.ftp.MlsxEntry;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;

/**
 *
 * @author szpak
 */
@RunWith(Parameterized.class)
public class GridFTPTest
{

    //test paths
    private static final File userHome = new File(System.getProperty("user.home"));
    private static final String temporaryName = "gftptst67912349_";
    private static String remoteFilename = temporaryName;
    private static File localFile = new File(userHome, temporaryName + ".txt");
    //gridftp connection
    private static int port = 2811;
    private String currentHostname;
    //buffer for data to copy
    private static DataSource bufferData;

    public GridFTPTest(String currentHostname)
    {
        this.currentHostname = currentHostname;
    }

    /**
     * Initialization (called once within this test class):
     * <ul>
     * <li>creates file data to copy to/from remote server;
     * <li>sets up certificate directory and environment variables
     * </ul>
     * .
     */
    @BeforeClass
    public static void setUpClass()
    {

        //create buffer with data;
        bufferData = new DataSource()
        {
            String data;
            boolean read = false;

            private void initData()
            {
                if (data == null) {
                    StringBuilder s = new StringBuilder();
                    double width = 100.0;
                    double height = 70.0;
                    String characters = ".-+*#";
                    //build sin-cos 2D pattern
                    //to see this pattern on the screen
                    for (int y = 0; y < height; y++) {
                        for (int x = 0; x < width; x++) {
                            double val = Math.sin((y - height / 2) / height * 5) * Math.cos(x / width * 3);
                            s.append(characters.charAt((int) Math.round((val + 1) / 2 * (characters.length() - 1))));
                        }
                    }
                    data = s.toString() + Calendar.getInstance().getTime().toString() + "\n";
                }
            }

            @Override
            public Buffer read() throws IOException
            {
                if (!read) {
                    initData();
                    read = true;
                    return new Buffer(data.getBytes(), data.getBytes().length);
                } else {
                    return null;
                }
            }

            @Override
            public void close() throws IOException
            {
                read = false;
                data = null;
            }

            @Override
            public long totalSize() throws IOException
            {
                initData();
                return data.getBytes().length;
            }
        };

    }

    /**
     * Removes local temporary file in case of test failure.
     */
    @AfterClass
    public static void tearDownClass()
    {
        System.out.println("TEST");
        localFile.delete();
    }

    @Parameters
    public static Collection<String[]> hostnames()
    {
        String[][] hostnames = {
//            {"ui.grid.task.gda.pl"},
//            {"zeus.cyfronet.pl"},
//            {"ui.plgrid.icm.edu.pl"},
//            {"ui.inula.man.poznan.pl"},
            {"ui.plgrid.wcss.wroc.pl"},
        };
        return Arrays.asList(hostnames);
    }

    /**
     * Tests connection; In fact even if this test passed it doesn't mean that connection was established - it's quite often
     * that credentials are not found but client.authenticate(getDefaultCredential()) looks like working correctly.
     *
     * @throws Exception
     */
    @Test(timeout = 10000)
    public void testConnect() throws Exception
    {
        System.out.println("\n[TEST CONNECT: " + currentHostname + "]}");

        GridFTPConnection connection = GridFTPConnection.getConnection(currentHostname, port);
    }

    /**
     * Tests sequence:
     * <ul>
     * <li>create remote file
     * <li>list dir
     * <li>copy to local
     * <li>partial copy to local (extendedGet)
     * <li>deleting remote file
     * </ul>
     */
    @Test(timeout = 30000)
    public void testPutListMlstGetDelete() throws Exception
    {
        System.out.println("\n[TEST PUT -> LIST -> MLST -> GET -> EXTENDEDGET -> DELETE]");
        long fileSize = bufferData.totalSize();

        //-----create remote file-----//
        System.out.println("-----creating file: " + remoteFilename + " at: " + currentHostname + "-----");

        GridFTPConnection connection = GridFTPConnection.getConnection(currentHostname, port);

        connection.put(remoteFilename, bufferData, new MarkerListener()
               {
                   @Override
                   public void markerArrived(Marker m)
                   {
                   }
        });

        //-----list dir-----//
        System.out.println("-----list directory-----");
        //works well with IMAGE & ASCII
        //MODE_STREAM: OK
        //MODE_BLOCK: unrecognized"
        //MODE_EBLOCK: refusing to store with active mode

        System.out.println(connection.getCurrentDir());
        Vector v = connection.list();
        boolean fileExists = false;
        boolean fileSizeCorrect = false;
        while (!v.isEmpty()) {
            FileInfo f = (FileInfo) v.remove(0);
            System.out.println(f.toString());
            if (f.getName().equals(remoteFilename)) {
                fileExists = true;
                fileSizeCorrect = f.getSize() == fileSize;
            }
        }
        //assert put
        assertTrue(fileExists & fileSizeCorrect);

        //-----mlst-----//
        System.out.println("-----mlst-----");
        MlsxEntry mlsxEntry = connection.mlst(remoteFilename);

        //assert put
        assertEquals(mlsxEntry.getFileName(), remoteFilename);
        System.out.println("Size: " + mlsxEntry.get(MlsxEntry.SIZE));
        System.out.println("Modify: " + mlsxEntry.get(MlsxEntry.MODIFY));

        //-----copy to local (doesn't test if file content match oryginal data)-----//
        System.out.println("-----copying remote file: " + remoteFilename + " to local file: " + localFile + "-----");

        localFile.createNewFile();

        connection.get(remoteFilename, localFile);
        assertEquals(localFile.length(), fileSize);
        localFile.delete();

        //-----partial copy to local-----//
        System.out.println("-----partial copying remote file: " + remoteFilename + " to local -----");

        connection.extendedGet(remoteFilename, 100 * 10, 100 * 40, new DataSink()
                       {
                           @Override
                           public void write(Buffer buffer) throws IOException
                           {
                               System.out.print(new String(buffer.getBuffer(), 0, buffer.getLength()));
                           }

                           @Override
                           public void close() throws IOException
                           {
                           }
        }, new MarkerListener()
                       {
                           @Override
                           public void markerArrived(Marker m)
                           {
                               System.out.println("Marker arrived: " + m);
                           }
        });

        //-----deleting remote file-----//
        System.out.println("-----deleting remote file" + remoteFilename + "-----");

        //works with both absolute and relative path
        connection.deleteFile(remoteFilename);
    }

    /**
     * Directory change test. Tries to go up dir and than back to work dir.
     */
    @Test(timeout = 10000)
    public void testDirChange() throws Exception
    {
        System.out.println("\n[TEST DIR CHANGE]");
        GridFTPConnection connection = GridFTPConnection.getConnection(currentHostname, port);

        String workDir = connection.getCurrentDir();
        System.out.println(workDir);
        String[] pathSeparated = workDir.split("/");

        /////local change/////
        //go to root level
        for (int i = 0; i < pathSeparated.length - 1; i++) {
            connection.goUpDir();
            System.out.println(connection.getCurrentDir());
        }

        //go up
        for (int i = 0; i < pathSeparated.length; i++)
            if (!pathSeparated[i].equals("")) {
                connection.changeDir(pathSeparated[i]);
                System.out.println(connection.getCurrentDir());
            }

        /////absolute change/////
        connection.changeDir("/");
        System.out.println(connection.getCurrentDir());

        connection.changeDir(workDir);
        System.out.println(connection.getCurrentDir());

        assertEquals(connection.getCurrentDir(), workDir);
    }

    /**
     * Directory listing test. Tries to do double list (two transfers) in one test.
     */
    @Test(timeout = 10000)
    public void testDirList() throws Exception
    {
        System.out.println("\n[TEST DIR LIST]");
        GridFTPConnection connection = GridFTPConnection.getConnection(currentHostname, port);

        String workDir = connection.getCurrentDir();
        System.out.println(workDir);

        connection.list();

        connection.changeDir("/");
        System.out.println(connection.getCurrentDir());

        connection.list();

        connection.changeDir(workDir);
        System.out.println(connection.getCurrentDir());

        assertEquals(connection.getCurrentDir(), workDir);
    }
}
