/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.grid;

import eu.unicore.hila.exceptions.HiLAException;
import eu.unicore.hila.grid.Task;
import eu.unicore.hila.grid.TaskStatus;
import java.io.File;
import org.apache.log4j.Logger;

/**
 * @author szpak
 */
public class GridExportTask implements Runnable
{

    private final static Logger LOGGER = Logger.getLogger(GridExportTask.class);
    private GridExportTaskObserver observer;

    private int maxInactivityTime = 60000; //inactivity time (when filesize doesn't increase) in miliseconds - 60 seconds :)
    private int updateSleepTime = 100; //minimum time between observer updates
    private eu.unicore.hila.grid.File remoteFile;
    //ready just after construction GridExportTask object
    private File localDirectory;
    private String fileName;
    //updated in thread
    private Status status;
    private Task exportTask;
    private long remoteFileSize;

    public static GridExportTask startNewTask(eu.unicore.hila.grid.File remoteFile, GridExportTaskObserver observer, String localDirectoryPath) throws GridException
    {
        GridExportTask task = new GridExportTask(remoteFile, localDirectoryPath);
        task.registerObserver(observer);
        new Thread(task).start();
        return task;
    }

    //TODO: make it universal (no Unicore classes)
    // add sth like GridFile
    public GridExportTask(eu.unicore.hila.grid.File remoteFile, String localDirectoryPath) throws GridException
    { //SimpleTransfer simpleTransfer) {
        this.remoteFile = remoteFile;
        fileName = remoteFile.getName();

        if (localDirectoryPath != null) {
            localDirectory = new File(localDirectoryPath);
            if (!localDirectory.isDirectory())
                throw new GridException("Specified path is not a directory");
        } else {
            String tmpPath = System.getProperty("java.io.tmpdir");
            File tmpDir = new File(tmpPath + File.separator + "VNGridExportTMP");
            if (!tmpDir.exists()) {
                tmpDir.mkdirs();
            }
            localDirectory = tmpDir;
        }
    }

    /**
     * Registers observer for this task. Only one observer per task.
     * <p>
     * @param observer - observes this task (and is updated on change)
     */
    public void registerObserver(GridExportTaskObserver observer)
    {
        this.observer = observer;
    }

    /**
     * Removes obsever.
     * Only removes observer. To remove observer and stop the task call abort() method.
     */
    public void removeObserver()
    {
        this.observer = null;
    }

    public void abort()
    {
        LOGGER.info("Aborting transfer");
        removeObserver();
        try {
            exportTask.abort();
        } catch (HiLAException e) {
            LOGGER.warn("Can't abort transfer", e);
            //do not notify observer, it's removed anyway...
            //do not update status, possibly it will be updated in the loop
        }
    }

    public String getFileName()
    {
        return fileName;
    }

    public String getLocalDirectory()
    {
        return localDirectory.getAbsolutePath();
    }

    public long getLocalFileSize()
    {
        try {
            return new File(localDirectory.getPath() + "/" + fileName).length();
        } catch (SecurityException e) {
            return 0; //ignore security exception
        }
    }

    public long getRemoteFileSize()
    {
        return remoteFileSize;
    }

    public Status getStatus()
    {
        return status;
    }

    private void notifyObserver()
    {
        if (observer != null)
            observer.updateStatus(this);
    }

    private void updateStatus(TaskStatus taskStatus)
    {
        switch (taskStatus) {
            case ABORTED:
            case FAILED:
                status = Status.FAILED;
                break;
            case SUCCESSFUL:
                status = Status.SUCCESSFUL;
                break;
            default:
                status = Status.RUNNING;
        }
    }

    @Override
    public void run()
    {
        try {
            remoteFileSize = remoteFile.size();
            exportTask = remoteFile.exportToLocalFile(localDirectory);
        } catch (HiLAException e) {
            LOGGER.warn("Can't export file", e);
            this.status = Status.FAILED;
            notifyObserver();
            return;
        }

        TaskStatus lastExportTaskStatus = null;
        TaskStatus exportTaskStatus = TaskStatus.NEW;
        long lastLocalFileSize = -1;
        long lastActivityTime = System.currentTimeMillis();

        //continue while transfer not finished
        // and there is some activity (status update or filesize update)
        while (lastExportTaskStatus != TaskStatus.SUCCESSFUL && lastExportTaskStatus != TaskStatus.FAILED && lastExportTaskStatus != TaskStatus.ABORTED &&
            (System.currentTimeMillis() - lastActivityTime <= maxInactivityTime)) {
            try {
                exportTaskStatus = exportTask.status();
            } catch (HiLAException ex) {
                LOGGER.error("", ex);
            }
            long fileSize = getLocalFileSize();

            //update observer if exportTask status changed or filesize changed
            if (lastLocalFileSize != getLocalFileSize() || exportTaskStatus != lastExportTaskStatus) {
                lastActivityTime = System.currentTimeMillis();
                lastLocalFileSize = getLocalFileSize();
                lastExportTaskStatus = exportTaskStatus;
                updateStatus(exportTaskStatus);
                //                    setChanged();
                notifyObserver();
            }
            try {
                Thread.sleep(updateSleepTime);
            } catch (InterruptedException ex) {
            }
        }

        if (System.currentTimeMillis() - lastActivityTime > maxInactivityTime) {
            status = Status.FAILED;
            //            setChanged();
            notifyObserver();
        }

        LOGGER.info("Transfer done: " + status);
    }

    public enum Status
    {

        RUNNING, SUCCESSFUL, FAILED;
    }
}
