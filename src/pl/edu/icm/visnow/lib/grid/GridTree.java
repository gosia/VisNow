/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.grid;

import eu.unicore.hila.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//TODO: move methods that operate on multiple GridNodes from GridNode in here... or sth to remove that wrapping/method duplication
// or at least make it consistent (now there is getChildrenValues  but not getChildren)
//TODO: add method getAbsolutePath (calculated manually traversing the tree)
/**
 * Wrapper on GridNode that allows to traverse over tree; so there is a tree made from connected GridNodes
 * and one GridTree object which points to some particular node (current-node) in the tree
 * <p>
 * @author szpak
 */
class GridTree
{

    private GridNode node;

    public GridTree(Resource resource)
    {
        node = new GridNode(resource);
    }

    /**
     * @return current-node in the tree
     */
    public GridNode getNode()
    {
        return node;
    }

    /**
     * Traverse to child at specified index.
     * <p>
     * @param childNum index of child in tree.getNode().getChildren() list
     * <p>
     * @return true if traverse was successful
     *         false otherwise (if index exceeds children.length)
     */
    public boolean traverseToChild(int childIndex)
    {
        try {
            return traverseToChild(this.node.getChildren().get(childIndex));
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    /**
     * Traverse to specified child node.
     * <p>
     * @param child element in tree.getNode().getChildren() list
     * <p>
     * @return true if specified child exists
     */
    public boolean traverseToChild(GridNode child)
    {
        if (!(node.getChildren().indexOf(child) >= 0))
            return false;
        node = child;
        return true;
    }

    /**
     * Traverse to child if childName equals GridResourceInfo.getName()
     * <p>
     * @param childName name of child to find
     * <p>
     * @return true if child with passed childName was found
     */
    public boolean traverseToChild(String childName)
    {
        for (GridNode node : getChildNodes())
            if (node.getInfo().getName().equals(childName))
                return traverseToChild(node);
        return false;
    }

    /**
     * @return true if there was parent to traverse to
     */
    public boolean traverseToParent()
    {
        if (node.hasParent()) {
            node = node.getParent();
            return true;
        }
        return false;
    }

    /**
     * Traverses to root.
     * <p>
     * @return false if tree already is at root level
     *         true if there is parent node and tree traverses successfully to root
     */
    public boolean traverseToRoot()
    {
        if (!node.hasParent())
            return false;
        while (node.hasParent())
            node = node.getParent();
        return true;
    }

    /**
     * Wrapper around GridNode.getValue().
     */
    public Resource getValue()
    {
        return getNode().getValue();
    }

    /**
     * Creates parent node, sets up the tree structure and traverse to newly created parent.
     */
    public void addParentAndTraverse(Resource parentResource)
    {
        GridNode parentNode = new GridNode(parentResource);
        parentNode.addChild(getNode());
        traverseToParent();
    }

    /**
     * Sets children list to reflect specified resources; If child with one of resources is already on the list,
     * than it stays untouched (with all aditional data like e.g., GridResourceInfo).
     * Children are recognized by resource (Resource.equals()).
     */
    public void setChildren(List<Resource> resources)
    {
        List<GridNode> childrenOld = new ArrayList<GridNode>(getNode().getChildren());
        //map to remember old children (to avoid O(n^2))
        Map<Resource, GridNode> map = new HashMap<Resource, GridNode>();
        for (GridNode node : getNode().getChildren())
            map.put(node.getValue(), node);

        getNode().removeChildren();

        for (Resource r : resources) {
            GridNode node = map.get(r);
            if (node != null)
                getNode().addChild(node);
            else
                getNode().addChild(new GridNode(r));
        }
    }

    /**
     * Wrapper around GridNode.removeChildren().
     */
    public void removeChildren()
    {
        getNode().removeChildren();
    }

    /**
     * Wrapper around GridNode.hasLoadedChildren().
     */
    public boolean hasLoadedChildren()
    {
        return getNode().hasLoadedChildren();
    }

    /**
     * Wrapper around GridNode.setLoadedChildren().
     */
    public void setLoadedChildren(boolean loadedChildren)
    {
        getNode().setLoadedChildren(loadedChildren);
    }

    /**
     * Wrapper around GridNode.getChildren().
     */
    public List<GridNode> getChildNodes()
    {
        return getNode().getChildren();
    }

    /**
     * Wrapper around GridNode.getChildrenNum().
     */
    public int getChildrenNum()
    {
        return getNode().getChildrenNum();
    }

    /**
     * Wrapper around GridNode.getChildrenValues().
     */
    public List<Resource> getChildrenValues()
    {
        return getNode().getChildrenValues();
    }

    /**
     * @return grid resource infos for all children of current node
     */
    public List<GridResourceInfo> getChildrenInfo()
    {
        List<GridResourceInfo> childrenInfo = new ArrayList<GridResourceInfo>();

        for (GridNode child : getChildNodes())
            childrenInfo.add(child.getInfo());

        return childrenInfo;
    }

    //TODO: handle outofboundexception
    /**
     * @return grid resource info for children at specified index
     */
    public GridResourceInfo getChildInfo(int childIndex)
    {
        return getChildNodes().get(childIndex).getInfo();
    }

    //TODO: handle no such node... (instead of null?)
    /**
     * @return grid resource info for children witht specified nodeName
     */
    public GridResourceInfo getChildInfo(String childName)
    {
        for (GridNode node : getChildNodes())
            if (node.getInfo().getName().equals(childName))
                return node.getInfo();
        return null;
    }

    class GridTreeException extends Exception
    {

        public GridTreeException(String message)
        {
            super(message);
        }
    }
}
