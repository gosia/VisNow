/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.grid;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;

//TODO: test if methods are called in EDT/other thread
/**
 *
 * @author szpak
 */
public class GridWindowExplorer extends javax.swing.JFrame
{

    private final static Logger LOGGER = Logger.getLogger(GridWindowExplorer.class);

    GridExplorer gridExplorer;
    DefaultListModel listModel;
    DefaultComboBoxModel gridURIModel;
    //TODO: separate "model/data update" and gui update (refresh list)
    String currentURI = "";
    boolean rootLevel = false;
    //stop all flag to stop current blocking task
    boolean stopAll = false;
    int maxAddressItems = 30;
    //user action listener which is called on success (user's chosen file) or failure (cancel/close window)
    ActionListener actionListener;
    public static final String ACTION_OK = "ActionOK";
    public static final String ACTION_CANCEL = "ActionCancel";

    /**
     * Creates new form GridWindowExplorer
     */
    public GridWindowExplorer(GridExplorer gridExplorer, ActionListener actionListener)
    {
        LOGGER.info("");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.actionListener = actionListener;
        this.gridExplorer = gridExplorer;
        //TODO: isCollection exception?
        if (!gridExplorer.getCurrentInfo().isCollection())
            gridExplorer.traverseToParent();

        initComponents();
        listModel = new DefaultListModel();
        gridURIModel = new DefaultComboBoxModel();
        gridURIComboBox.setModel(gridURIModel);
        gridResourcesList.setModel(listModel);

        refreshList();
    }

    /**
     * Traverses grid using visual/window browser; calls actionListener on success/cancel action.
     * This is just a Frame; true traversing is done using GridExplorer object.
     *
     * @param gridExplorer   object that provides Grid traversing methods; GridWindowExplorer starts exploring the grid
     *                       at position specified by gridExplorer...currentURI
     * @param actionListener listener called when user selects file (clicks ok/presses enter on file) or
     *                       user cancels (clicks cancel/closes the window)
     */
    public static void ExploreGrid(final GridExplorer gridExplorer, final ActionListener actionListener)
    {
        LOGGER.info("");
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            LOGGER.info("", ex);
        } catch (InstantiationException ex) {
            LOGGER.info("", ex);
        } catch (IllegalAccessException ex) {
            LOGGER.info("", ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            LOGGER.info("", ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new GridWindowExplorer(gridExplorer, actionListener).setVisible(true);
            }
        });
    }

    private static String timeDiffString(long timeMilisStart)
    {
        return (System.currentTimeMillis() - timeMilisStart) / 1000.0 + " s.";
    }

    private void refreshList()
    {
        //TODO: (asynch) getChildrenInfo  (Resource.getChildren can block the thread)
        final List<GridResourceInfo> infos = gridExplorer.getChildrenInfo();
        //TODO: same with getURI
        currentURI = gridExplorer.getCurrentInfo().getURI();
        try {
            rootLevel = gridExplorer.isRoot();
        } catch (GridException ex) {
            rootLevel = false; //just in case
            setGUIState("GridException...", null, null);
        }

        LOGGER.info("Root level: " + rootLevel);

        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                listModel.clear();
                if (!rootLevel)
                    listModel.addElement(GridExplorer.gridPathParentSymbol);
                for (GridResourceInfo info : infos)
                    listModel.addElement(info.getName());

                int oldIndex = gridURIModel.getIndexOf(currentURI);
                if (oldIndex >= 0)
                    gridURIModel.removeElementAt(oldIndex);
                if (gridURIModel.getSize() > maxAddressItems)
                    gridURIModel.removeElementAt(0);
                gridURIModel.addElement(currentURI);
                gridURIModel.setSelectedItem(currentURI);
            }
        });
    }

    /**
     * Sets different elements of the Frame which indicates some state of the Frame.
     */
    private void setGUIState(final String status, final Boolean enabled, final Integer position)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                if (enabled != null) {
                    gridURIComboBox.setEnabled(enabled);
                    refreshButton.setEnabled(enabled);
                    fullViewButton.setEnabled(enabled);
                    okButton.setEnabled(enabled);
                    cancelButton.setEnabled(enabled);
                    fileNameLabel.setEnabled(enabled);
                    fileNameField.setEnabled(enabled);
                    gridResourcesList.setEnabled(enabled);
                    gridResourcesList.requestFocusInWindow();
                    //TODO: stop button enable/disable
                }
                if (position != null)
                    gridResourcesList.setSelectedIndex(position);
                if (status != null)
                    statusLabel.setText(status);
            }
        });
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        gridURIComboBox = new javax.swing.JComboBox();
        addressLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        gridResourcesList = new javax.swing.JList();
        statusLabel = new javax.swing.JLabel();
        refreshButton = new javax.swing.JButton();
        fullViewButton = new javax.swing.JToggleButton();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        fileNameLabel = new javax.swing.JLabel();
        fileNameField = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("VisNow Grid Explorer");
        setIconImage(new ImageIcon(getClass().getResource("/pl/edu/icm/visnow/gui/icons/big/visnow.png")).getImage());
        setMinimumSize(new java.awt.Dimension(472, 378));
        setPreferredSize(new java.awt.Dimension(472, 378));
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        gridURIComboBox.setEditable(true);
        gridURIComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "unicore6:/" }));
        gridURIComboBox.addPopupMenuListener(new javax.swing.event.PopupMenuListener()
        {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt)
            {
                gridURIComboBoxPopupMenuCanceled(evt);
            }

            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt)
            {
            }

            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt)
            {
            }
        });
        gridURIComboBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                gridURIComboBoxActionPerformed(evt);
            }
        });

        addressLabel.setDisplayedMnemonic('a');
        addressLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addressLabel.setText("Address");

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, gridURIComboBox, org.jdesktop.beansbinding.ObjectProperty.create(), addressLabel, org.jdesktop.beansbinding.BeanProperty.create("labelFor"));
        bindingGroup.addBinding(binding);

        gridResourcesList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        gridResourcesList.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                gridResourcesListMouseClicked(evt);
            }
        });
        gridResourcesList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
        {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt)
            {
                gridResourcesListValueChanged(evt);
            }
        });
        gridResourcesList.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt)
            {
                gridResourcesListKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(gridResourcesList);

        statusLabel.setText("status...");

        refreshButton.setMnemonic('r');
        refreshButton.setText("R");
        refreshButton.setToolTipText("Refresh");
        refreshButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                refreshButtonActionPerformed(evt);
            }
        });

        fullViewButton.setText("F");
        fullViewButton.setToolTipText("Full view");
        fullViewButton.setEnabled(false);
        fullViewButton.setName(""); // NOI18N

        okButton.setText("OK");
        okButton.setToolTipText("");
        okButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cancelButtonActionPerformed(evt);
            }
        });

        fileNameLabel.setText("File name:");

        fileNameField.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt)
            {
                fileNameFieldKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(addressLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(gridURIComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fullViewButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(refreshButton))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(fileNameLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fileNameField)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(okButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cancelButton))
                        .addComponent(statusLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(gridURIComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(addressLabel)
                                        .addComponent(refreshButton)
                                        .addComponent(fullViewButton))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(fileNameLabel)
                                        .addComponent(cancelButton)
                                        .addComponent(okButton)
                                        .addComponent(fileNameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(statusLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void gridResourcesListKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_gridResourcesListKeyPressed
        //enter or backspace (traverse to parent)
        boolean backspace = evt.getKeyCode() == KeyEvent.VK_BACK_SPACE;
        boolean enter = evt.getKeyCode() == KeyEvent.VK_ENTER;

        gridResourcesListAction(backspace, enter);
    }

    private void gridResourcesListAction(final boolean backspace, final boolean enterOrMouseDoubleClick)
    {
        if (enterOrMouseDoubleClick || (backspace && !rootLevel)) {
            //disable Frame (just in case, even if command is invalid (selected child is not a collection)
            //            setGUIState(null, false, null);
            setGUIState("loading...", false, null);
            fileOkAction(gridResourcesList.getSelectedIndex(), backspace, enterOrMouseDoubleClick);
        }
    }//GEN-LAST:event_gridResourcesListKeyPressed

    /**
     * Action for chosing parent/file/collection; Default action is traversing to collection/parent
     * or exporting file (or rather calling actionListener with "ok" action).
     * This action is called when :
     * 1. user double clicks on file/directory/".." at gridResourcesList;
     * 2. chooses it by pressing enter on the list
     * 3. presses backspace to go up
     * 4. presses enter on fileName field (with valid file name)
     * 5. presses/clicks OK button (with valid file name).
     *
     * @return true on success
     */
    private void fileOkAction(final int gridListIndex, final boolean backspace, final boolean enterOrMouseDoubleClick)
    {
        final long milis1 = System.currentTimeMillis();
        //remember window (if "ok" action performed)
        final GridWindowExplorer window = this;

        new Thread(new Runnable()
        {
            public void run()
            {
                //do not calculate ".." if not root level
                int gridExplorerIndex = !rootLevel ? gridListIndex - 1 : gridListIndex;
                //                    gridExplorer.isCollection(
                boolean goParent = backspace || (!rootLevel && gridListIndex == 0);
                //TODO: isCollection exception?
                boolean goChild = !goParent && gridExplorer.isCollection(gridExplorerIndex);
                //go parent or go child (if it's a collection)
                //                    if (goParent || goChild) {
                //                        setGUIState("loading...", false, null);

                boolean success;
                //TODO: gridExplorer synchronized?
                if (goParent)
                    success = gridExplorer.traverseToParent();
                else if (goChild)
                    success = gridExplorer.traverseToChild(gridExplorerIndex);
                //export child
                else {
                    success = gridExplorer.traverseToChild(gridExplorerIndex);
                    if (success) //if successfully traversed to child then dispose the window
                    {
                        callListenerAction(true);
                        SwingUtilities.invokeLater(new Runnable()
                        {
                            public void run()
                            {
                                window.dispose();
                            }
                        });
                        return;
                    }
                }
                if (success)
                    refreshList();

                setGUIState((success ? "Done " : "Failed ") + timeDiffString(milis1), true, 0);
            }
        }).start();
    }

    //    private boolean fileOkAction(final boolean goParent, int childGridExplorerIndex) {
    //        if (goParent)
    //            return gridExplorer.traverseToParent();
    //        else //if (goChild)
    //        {
    //            if (gridExplorer.isCollection(childGridExplorerIndex))
    //                return gridExplorer.traverseToChild(childGridExplorerIndex);
    //            else
    //                LOGGER.info()
    //                //gridExplorer.exportChildFile(childGridExplorerIndex, null, null);
    //        }
    //                        if (success)
    //                            refreshList();
    //
    ////                        if (stopAll) return; //stop flag testing after risky/time consuming operations
    //
    //                        setGUIState((success ? "Done " : "Failed ") + timeDiffString(milis1), true, 0);
    //                    }
    //                    else
    //                        setGUIState("No collection " + timeDiffString(milis1), true, null);
    //                }
    //    }
    private void gridResourcesListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gridResourcesListMouseClicked
        if (gridResourcesList.isEnabled() && evt.getClickCount() == 2 && evt.getButton() == MouseEvent.BUTTON1 && !evt.isConsumed()) {
            gridResourcesListAction(false, true);
        }
    }//GEN-LAST:event_gridResourcesListMouseClicked

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        fileNameFieldAction();
    }//GEN-LAST:event_okButtonActionPerformed

    /**
     * Action when user types (valid) fileName and confirms with enter or OK button.
     */
    private void fileNameFieldAction()
    {
        String fileName = fileNameField.getText().trim();
        int gridIndex = listModel.indexOf(fileName);
        if (gridIndex >= 0) {
            setGUIState("loading...", false, null);
            fileOkAction(gridIndex, false, true);
        }
    }

    private void gridURIComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gridURIComboBoxActionPerformed
        LOGGER.debug("event action command: " + evt.getActionCommand());
        LOGGER.debug("event: " + evt.toString());

        //lame stuff but constants not available
        //http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4808758
        boolean enterPressed = "comboBoxEdited".equals(evt.getActionCommand());
        boolean mouseClicked = "comboBoxChanged".equals(evt.getActionCommand()) && ((evt.getModifiers() & ActionEvent.MOUSE_EVENT_MASK) != 0);
        String newPath = (String) gridURIComboBox.getSelectedItem();

        LOGGER.debug("event, enter: " + enterPressed + " mouse: " + mouseClicked + " newPath: " + newPath + " eventModifiers: " + evt.getModifiers());

        //enter (URI change or refresh) or mouse click (if URI changed)
        if (enterPressed || (mouseClicked && !currentURI.equals(newPath))) {
            gridURIComboBoxChanged(newPath);
        }
    }

    private void gridURIComboBoxChanged(final String selectedPath)
    {
        setGUIState("loading...", false, null);

        final long milis1 = System.currentTimeMillis();
        new Thread(new Runnable()
        {
            public void run()
            {
                //path fully traversed
                boolean fullSuccess = false;
                boolean severe = false;
                try {
                    fullSuccess = gridExplorer.traverseToPath(selectedPath, true, false);
                } catch (GridException ex) {
                    LOGGER.error("", ex);
                    severe = true;
                }
                if (!severe)
                    refreshList();
                else
                    //revert JComboBox changes
                    gridURIModel.setSelectedItem(currentURI);
                setGUIState((fullSuccess ? "Done " : severe ? "Problem occured " : "Full path not found ") + timeDiffString(milis1), true, 0);
            }
        }).start();
        //      }

    }//GEN-LAST:event_gridURIComboBoxActionPerformed

    private void gridURIComboBoxPopupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_gridURIComboBoxPopupMenuCanceled
        LOGGER.debug("event: " + evt.toString());
        //revert JComboBox changes
        gridURIModel.setSelectedItem(currentURI);
    }//GEN-LAST:event_gridURIComboBoxPopupMenuCanceled

    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
        new Thread(new Runnable()
        {
            public void run()
            {
                final long milis1 = System.currentTimeMillis();
                setGUIState("refreshing...", false, null);
                gridExplorer.reloadChildren(true);
                refreshList();
                setGUIState("Done " + timeDiffString(milis1), true, 0);
            }
        }).start();
    }//GEN-LAST:event_refreshButtonActionPerformed

    private void gridResourcesListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_gridResourcesListValueChanged
        LOGGER.debug(evt.toString());
        String selectedValue = (String) (gridResourcesList.getSelectedValue());
        if (selectedValue == null || GridExplorer.gridPathParentSymbol.equals(selectedValue))
            fileNameField.setText("");
        else
            fileNameField.setText(selectedValue);
    }//GEN-LAST:event_gridResourcesListValueChanged

    private void fileNameFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fileNameFieldKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER)
            fileNameFieldAction();
    }//GEN-LAST:event_fileNameFieldKeyPressed

    /**
     * Same behaviour as formWindowClosed but it additionally closes the window.
     */
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        callListenerAction(false);
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    /**
     * Calls listenerAction with default ACTION_CANCEL, although it could be already called with ACTION_OK, in that case
     * nothing happens because actionListener should be already removed
     */
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        callListenerAction(false);
    }//GEN-LAST:event_formWindowClosing

    /**
     * Calls actionListener with "ok" or "cancel" action and removes listener to avoid duplicate calls.
     * Call can be invoked when user selects file/presses ok/presses cancel/close the window
     */
    private void callListenerAction(boolean okAction)
    {
        if (actionListener != null)
            actionListener.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, okAction ? ACTION_OK : ACTION_CANCEL));
        actionListener = null;
    }

    //        if (!currentURI.equals(selectedPath))//"comboBoxEdited".equals(evt.getActionCommand()))
    //     private void gridURIComboBoxKeyPressed(java.awt.event.KeyEvent evt) {
    //        boolean enter = evt.getExtendedKeyCode() == KeyEvent.VK_ENTER;
    //
    //        LOGGER.info(gridURIComboBox.getEditor().getItem().toString());
    //    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws GridException
    {
        //        /* Set the Nimbus look and feel */
        //        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        //        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
        //         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
        //         */
        //        try {
        //            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        //                if ("Nimbus".equals(info.getName())) {
        //                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
        //                    break;
        //                }
        //            }
        //        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
        //            LOGGER.log(Level.SEVERE, null, ex);
        //        }
        //        //</editor-fold>
        //
        //        final GridExplorer explorer = new GridExplorer("unicore6:/sites");
        //
        //        /* CGridWindowExplorerthe form */
        //        java.awt.EventQueue.invokeLater(new Runnable() {
        //            public void run() {
        //                new GridWindowExplorer(explorer).setVisible(true);
        //            }
        //        });

        //        final GridExplorer explorer = new GridExplorer("unicore6:/sites/");
        final GridExplorer explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/tasks/1485e11c-42f1-47d5-8d07-b5d235825c47/wd/files/modules.txt", false, false);
        ExploreGrid(explorer, new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                LOGGER.info(e.toString());
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel addressLabel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField fileNameField;
    private javax.swing.JLabel fileNameLabel;
    private javax.swing.JToggleButton fullViewButton;
    private javax.swing.JList gridResourcesList;
    private javax.swing.JComboBox gridURIComboBox;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton okButton;
    private javax.swing.JButton refreshButton;
    private javax.swing.JLabel statusLabel;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
}
