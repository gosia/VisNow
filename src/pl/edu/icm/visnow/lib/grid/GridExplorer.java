/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.grid;

import eu.unicore.hila.Location;
import eu.unicore.hila.Resource;
import eu.unicore.hila.exceptions.HiLAException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

//TODO: throw exceptions properly (e.g., isCollection should return true/false or Exception if sth goes completely wrong)
//TODO: add setSelection in the tree (for WindowExplorer... this is not really the best class to put it)
/**
 * GridExplorer's main functionality is loading new (HiLA) Resources into the tree; It builds the grid tree. Tree is used to cache grid structure.
 * <p>
 * It handles refreshing the tree (children) while traversing over path. Reloads children before getChildrenNumber, ls or traversing to child.
 * Does not reload children when traversing up the tree.
 *
 * @author szpak
 */
public class GridExplorer
{

    private final static Logger LOGGER = Logger.getLogger(GridExplorer.class);

    private GridTree resourceTree;
    private final static String gridSchemaPattern = "\\w+:.*"; //could be also sth like "unicore6:.*|unicore5:.*|ogsa:.*" (according to HiLA Api doc)
    private final static char gridPathSeparator = '/';
    public final static String gridPathParentSymbol = "..";

    /**
     * Tries to load absolute gridLocation; no leaf reload but leaf collection testing - equivalent to GridExplorer(gridLocation,true,false);.
     * <p>
     * @param gridLocation absolute grid location like eg., unicore6:/sites/SITE/....
     */
    public GridExplorer(String gridLocation) throws GridException
    {
        this(gridLocation, true, false);
    }

    /**
     * Tries to load gridLocation.
     * Uri should be in "official HiLA" format:
     * eg., unicore6:/sites/ICM-HYDRA_201301181133/tasks/f8de2794-3cb1-4f92-a50b-329e77cf8897/wd/files/imagerotate.png
     * or unofficial that reflects tree structure though, eg., unicore6:/storages/SHARE-ACK/files
     * <p>
     * @param gridLocation         absolute grid location like eg., unicore6:/sites/SITE/....
     * @param testIsLeafCollection if true and leaf is not collection (last token on pathString) then it's not traversed
     *                             otherwise enters into the leaf as into standard collection
     * @param reloadLeafChildren   if true then reloads children of the leaf - at the very end
     * <p>
     */
    public GridExplorer(String gridLocation, boolean testIsLeafCollection, boolean reloadLeafChildren) throws GridException
    {
        if (!initLocation(gridLocation, testIsLeafCollection, reloadLeafChildren))
            LOGGER.warn("Location not fully loaded");
    }

    /**
     * Tries to locate grid location manually, by first, locating root schema (eg., unicore6:/) and
     * then traversing the tree to specified path.
     * <p>
     * @param gridURI location URI to load
     * <p>
     * @return true if location was completely loaded
     *         false if some location was loaded but path was not fully traversed
     * <p>
     * @throws GridException if specified URI cannot be located
     */
    private boolean initLocationFromRoot(String gridURI, boolean testIsLeafCollection, boolean reloadLeafChildren) throws GridException
    {
        LOGGER.info("Grid location: " + gridURI);

        gridURI = formatPath(gridURI);

        String[] tokens = gridURI.split(gridPathSeparator + "");
        if (tokens.length == 0)
            throw new GridException("Can't read resource at specified location.");
        Resource r;
        try {
            Location manualLocation = new Location(tokens[0] + gridPathSeparator);
            LOGGER.info("Trying to locate: " + manualLocation);
            r = manualLocation.locate();
        } catch (HiLAException e) {
            throw new GridException(e);
        }
        if (r == null)
            throw new GridException("Can't read resource at specified location.");

        resourceTree = new GridTree(r);

        if (tokens.length > 1) {
            String gridPath = gridURI.substring(tokens[0].length() + 1);
            LOGGER.info("Trying to traverse: " + gridPath);

            return traverseToRelativePath(gridPath, testIsLeafCollection, reloadLeafChildren);
        } else {
            return true;
        }
    }

    /**
     * unfortunately trying to locate without success some resources (with non-standard Unicore URI), like eg., unicore6:/storages/SHARE-ACK/files/
     * using Location.locate stops from working tree traversing
     * [initLocationFromRoot throws eu.unicore.hila.exceptions.HiLAException: No access to storage]
     * probably because of some HiLA's caching mechanism or sth like that :/
     */
    private boolean initLocation(String gridURI, boolean testIsLeafCollection, boolean reloadLeafChildren) throws GridException
    {
        //turned off HiLA Traversing (Location.locate()) -> manual traversing
        return initLocationFromRoot(gridURI, testIsLeafCollection, reloadLeafChildren);
    }

    /**
     * Reloads children at current level tree; This method block the tread and
     * getChildren can take even 45 seconds :/ so try to use it asynchronously! This method is used by getChildrenNum,
     * getChildrenInfo, traverseToChild, traverseToRelativePath, so they should be called asynchronously too.
     * <p>
     * Tries to get children Resources from the Grid API (Resource.getChildren) than makes clean-refresh
     * (loosing all previously stored child data like GridResourceInfo) or soft-refresh (adds only new child elements).
     * Sets loadedChildren to true.
     * <p>
     * @param forceClean indicates that all children should be removed and freshly reloaded
     */
    public void reloadChildren(boolean forceClean)
    {
        LOGGER.info("force clean: " + forceClean);
        List<Resource> resources = new ArrayList<Resource>();
        //load children
        try {
            for (Resource r : resourceTree.getValue().getChildren())
                resources.add(r);
        } catch (HiLAException ex) {
            //exception while fetching children (possibly "File does not exits" exception)
            LOGGER.warn("Can't do Resource.getChildren (possibly file 'does not exist')", ex);
        }

        if (forceClean)
            resourceTree.removeChildren();

        resourceTree.setChildren(resources);

        resourceTree.setLoadedChildren(true);
    }

    //TODO: refactor to getChildInfo/getCurrentInfo
    public int getChildrenNum()
    {
        if (!resourceTree.hasLoadedChildren())
            reloadChildren(false);
        return resourceTree.getChildrenNum();
    }

    //TODO: refactor to getChildInfo/getCurrentInfo
    public List<GridResourceInfo> getChildrenInfo()
    {
        //reload children
        if (!resourceTree.hasLoadedChildren())
            reloadChildren(false);

        return resourceTree.getChildrenInfo();
    }

    //TODO: refactor to getChildInfo/getCurrentInfo
    public GridResourceInfo getCurrentInfo()
    {
        return resourceTree.getNode().getInfo();
    }

    //TODO: refactor to getChildInfo/getCurrentInfo
    /**
     * Tests if child resource is a collection, @see GridResourceInfo.isCollection(Resource) GridResourceInfo.isCollection for details
     */
    public boolean isCollection(int childIndex)
    {
        //TODO: user infoValue from GridNode
        return resourceTree.getChildInfo(childIndex).isCollection();
    }

    //TODO: refactor to getChildInfo/getCurrentInfo
    /**
     * Tests if child resource is a collection, @see GridResourceInfo.isCollection(Resource) GridResourceInfo.isCollection for details
     */
    public boolean isCollection(String childName)
    {
        //TODO: user infoValue from GridNode
        //        Resource child = resourceTree.getChildrenValues().get(childIndex);
        //        return GridResourceInfo.isCollection(child)
        return resourceTree.getChildInfo(childName).isCollection();
    }

    //TODO: refactor to getChildInfo/getCurrentInfo ?? Or keep in here...see pl.edu.icm.visnow.lib.grid.txt
    /**
     * Tests if current resource is a root ie., if it has a parent; first tries to get parent in GridTree, second in Resources tree.
     */
    public boolean isRoot() throws GridException
    {
        if (resourceTree.getNode().hasParent())
            return false; //has parent -> means no root
        try {
            //TODO:refactor (cache parent resource/parent node ??)
            Resource parentResource = resourceTree.getValue().getParent();
            //parent found and parent different from current -> means no root
            if (parentResource != null && !resourceTree.getValue().equals(parentResource))
                return false;
        } catch (HiLAException ex) {
            LOGGER.info("No parent found", ex);
            throw new GridException(ex);
        }
        return true; //no parent Resource found
    }

    /**
     * Asynchronously exports remote file (current node on the tree) to specified local dir.
     * <p>
     * @param exportTaskObserver observer that will be updated (on status change, on filesize change)
     * @param localDirectory     target directory for file tranfer
     * <p>
     * @return export task that can be used to get general info (like filename/size/local directory/transfer status); it can be also used to abort the transfer.
     * <p>
     * @throws GridException typically when some file system problem occurs (can't read/create directory, resource is not a file, etc)
     */
    public GridExportTask exportFile(GridExportTaskObserver exportTaskObserver, String localDirectory) throws GridException
    {
        if (getCurrentInfo().isCollection()) {
            LOGGER.warn("Requested resource is not a file");
            throw new GridException("Requested resource is not a file");
        }
        return GridExportTask.startNewTask((eu.unicore.hila.grid.File) resourceTree.getValue(), exportTaskObserver, localDirectory);
    }

    /**
     * Similar to exportFile(exportTaskObserver,localDirectory) but loads child resource instead of current resource.
     */
    public GridExportTask exportChildFile(int childIndex, GridExportTaskObserver exportTaskObserver, String localDirectory) throws GridException
    {
        if (resourceTree.getChildInfo(childIndex).isCollection()) {
            LOGGER.warn("Requested resource is not a file");
            throw new GridException("Requested resource is not a file");
        }
        return GridExportTask.startNewTask((eu.unicore.hila.grid.File) resourceTree.getChildrenValues().get(childIndex), exportTaskObserver, localDirectory);
    }

    /**
     * Trims and removes final slash "/" if present.
     */
    private static String formatPath(String path)
    {
        path = path.trim();
        //cut last slash "/"
        if (path.length() > 0 && path.charAt(path.length() - 1) == gridPathSeparator)
            path = path.substring(0, path.length() - 1);
        return path;
    }

    /**
     * Takes 2 absolute paths and returns the second one recalculated as a relative to the first one.
     * <p>
     * @return relative path or
     *         null if second path is not relative to the first one (i.e., root element/schema is different)
     */
    private static String convertToRelativePath(String currentPath, String pathToCalculate)
    {
        currentPath = formatPath(currentPath);
        pathToCalculate = formatPath(pathToCalculate);
        //new relative path
        String relativePath = "";

        String[] currentPathTokens = currentPath.split(gridPathSeparator + "");
        String[] pathToCalculateTokens = pathToCalculate.split(gridPathSeparator + "");
        int equalTokens = 0;
        //1. calculate equal tokens 
        while (equalTokens < currentPathTokens.length &&
            equalTokens < pathToCalculateTokens.length &&
            currentPathTokens[equalTokens].equals(pathToCalculateTokens[equalTokens]))
            equalTokens++;

        if (equalTokens == 0)
            return null; //different root/schema element

        //go up the tree by the old path
        for (int i = equalTokens; i < currentPathTokens.length; i++)
            relativePath += gridPathParentSymbol + "" + gridPathSeparator;

        //go down the tree bo the new path
        for (int i = equalTokens; i < pathToCalculateTokens.length; i++)
            relativePath += pathToCalculateTokens[i] + "" + gridPathSeparator;

        return relativePath;
    }

    /**
     * Traverses to absolute or relative path (depends on pathString argument).
     * If absolute path is provided (which is path with Grid schema like e.g., "unicore6:"), than initLocation is used;
     * otherwise traverseToRelativePath is used.
     */
    public boolean traverseToPath(String pathString, boolean testIsLeafCollection, boolean reloadLeafChildren) throws GridException
    {
        //if relative path
        if (!pathString.matches(gridSchemaPattern))
            return traverseToRelativePath(pathString, testIsLeafCollection, reloadLeafChildren);
        //if absolute path
        else {
            String relativePath = convertToRelativePath(getCurrentInfo().getURI(), pathString);
            //if different schema/root element then reinit whole GridExplorer
            if (relativePath == null)
                return initLocation(pathString, testIsLeafCollection, reloadLeafChildren);
            //else just traverse to path
            else
                return traverseToRelativePath(relativePath, testIsLeafCollection, reloadLeafChildren);
        }
    }

    /**
     * Traverses to specified path starting from current tree node. Tries to traverse as deep as possible,
     * stops when some problem occurs (returns false), or when path ends (returns true). Uses child name
     * as unique identifier of the node.
     * <p>
     * @param pathString           path to traverse
     * <p>
     * @param testIsLeafCollection if true and leaf is not collection (last token on pathString) then it's not traversed
     *                             otherwise enters into the leaf as into standard collection
     * @param reloadLeafChildren   if true then reloads children of the leaf - at the very end
     * <p>
     * @return true if path was successfully traversed
     *         false otherwise (could not traverse to child/parent at some point in the path)
     */
    public boolean traverseToRelativePath(String pathString, boolean testIsLeafCollection, boolean reloadLeafChildren)
    {
        pathString = formatPath(pathString);

        LOGGER.info("path: " + pathString);

        String[] tokens = pathString.split("/");

        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i].trim();
            LOGGER.debug("token: " + token);
            if (token.equals(gridPathParentSymbol)) {
                if (!traverseToParent())
                    return false; //return on failure    
            } else {
                if (!resourceTree.hasLoadedChildren()) //reload if not loaded
                    reloadChildren(false);

                boolean skipLeafChild = false;

                //skip leaf if isCollection
                if (i == tokens.length - 1 && testIsLeafCollection) {
                    GridResourceInfo leafInfo = resourceTree.getChildInfo(token);
                    if (leafInfo != null && !leafInfo.isCollection()) {
                        skipLeafChild = true;
                        LOGGER.debug("leaf skipped");
                    }
                }

                if (!skipLeafChild && !resourceTree.traverseToChild(token))
                    return false;
            }
        }

        //reload leaf children if needed
        if (reloadLeafChildren && !resourceTree.hasLoadedChildren())
            reloadChildren(false);

        return true;
    }

    /**
     * Traverses to child at childIndex of current tree node; If necessary children are reloaded before the traverse.
     * <p>
     * @param childIndex index of the child (current GridTree.node.children can be empty, but it's reloaded before the traverse)
     * <p>
     * @return true if child was traversed
     *         false otherwise (out of bound (no such index in children list))
     */
    public boolean traverseToChild(int childIndex)
    {
        //reload children before traversing (even if childIndex parameter indicates some particular number on the list (which is not ready yet) !!)
        if (!resourceTree.hasLoadedChildren())
            reloadChildren(false);

        return resourceTree.traverseToChild(childIndex);
    }

    /**
     * Traverses to parent (if already exists on the tree or new one if there is no parent on the tree, but there is resource.parent at node.value).
     * <p>
     * @return true if traverse was successful
     *         false otherwise
     */
    public boolean traverseToParent()
    {
        LOGGER.info("");

        Resource resourceOld = resourceTree.getValue();

        if (!resourceTree.traverseToParent()) {
            Resource parentResource;
            try {
                parentResource = resourceTree.getValue().getParent();
            } catch (HiLAException ex) {
                LOGGER.info("No parent found", ex);
                return false; //no parent Resource found
            }
            if (parentResource != null && !parentResource.equals(resourceOld))
                resourceTree.addParentAndTraverse(parentResource);
        }

        //return true if parent has been changed
        return !resourceOld.equals(resourceTree.getValue());
    }

    /**
     * Traverses to root (first, within the GridTree, than within the "Resource Tree", and adds new parent nodes on the path).
     * <p>
     * @return true if traverse was successful (no more parent and at least one parent traversed)
     */
    public boolean traverseToRoot()
    {
        Resource resourceOld = resourceTree.getValue();

        resourceTree.traverseToRoot();

        try {
            Resource parentResource = resourceTree.getValue().getParent();

            while (parentResource != null && !parentResource.equals(resourceTree.getValue())) {
                resourceTree.addParentAndTraverse(parentResource);
                parentResource = parentResource.getParent();
            }
        } catch (HiLAException ex) {
            LOGGER.info("No more parents found", ex);
            return false; //Exception while trying to access Resource.parent
        }

        //return true if parent has been changed
        return !resourceOld.equals(resourceTree.getValue());
    }
}
