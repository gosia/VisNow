package pl.edu.icm.visnow.lib.grid.gridftp;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.globus.ftp.MlsxEntry;
import org.globus.ftp.exception.ClientException;
import org.globus.ftp.exception.ServerException;
import org.ietf.jgss.GSSException;
import pl.edu.icm.visnow.engine.core.ProgressAgent;

//<editor-fold defaultstate="collapsed" desc=" License ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class GridFTPLocalUtils
{
    private static final Logger LOGGER = Logger.getLogger(GridFTPLocalUtils.class);

    private final static int DEFAULT_PORT = 2811;
    private final static String GSIURL_PATTERN = "^gsiftp://([^/]+)(:(\\d+))?/(.*)$";
    private final static int GSIURL_PATTERN_HOST_GROUP = 1;
    private final static int GSIURL_PATTERN_PORT_GROUP = 3;
    private final static int GSIURL_PATTERN_PATH_GROUP = 4;

    private final static String LOCAL_TMP_PREFIX = "/tmp/VisNowGSIFTP";

    /**
     * Creates MD5 hash from gsi uri, last modification time and size of file.
     * Modify and size should be taken from MlsxEntry.
     * <p>
     * @return filename hash that can be used as a local temporary filename (cache for remote file).
     */
    public static String getLocalFilenameHash(String remoteGsiURI, String lastModify, String size)
    {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            byte[] hash = messageDigest.digest((remoteGsiURI + lastModify + size).getBytes());

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < hash.length; ++i) sb.append(Integer.toHexString((hash[i] & 0xFF) | 0x100).substring(1, 3));
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            throw new IllegalStateException("Can't initialize java MD5 encoding");
        }
    }

    public static boolean isGSIUri(String remoteGsiURI)
    {
        return remoteGsiURI.matches(GSIURL_PATTERN);
    }

    /**
     * Match remoteGsiURI against {@link #GSIURL_PATTERN} and returns host, port and path components.
     * <p>
     * @return [host:String, port:Integer, path:String] array, if port is empty then {@link #DEFAULT_PORT} is taken
     */
    public static Object[] splitToHostPortPath(String remoteGsiURI)
    {
        Matcher matcher = Pattern.compile(GSIURL_PATTERN).matcher(remoteGsiURI);
        if (!matcher.matches()) throw new IllegalArgumentException("Invalid URI : " + remoteGsiURI);
        else {
            String portGroup = matcher.group(GSIURL_PATTERN_PORT_GROUP);
            if (portGroup == null || portGroup.isEmpty()) portGroup = "" + DEFAULT_PORT;
            int port = Integer.parseInt(portGroup);
            return new Object[]{matcher.group(GSIURL_PATTERN_HOST_GROUP), port, matcher.group(GSIURL_PATTERN_PATH_GROUP)};
        }
    }

    interface ProgressRunnable extends Runnable
    {
//        public void stop();

        public boolean isTimeout();

        public void setShortTimeout();
    }

    /**
     * If given resource at {@code remoteGsiURI} is already downloaded (cached in tmp) then returns this file,
     * otherwise connects to GridFTP location, downloads file, stores file in tmp directory and returns this file.
     * <p>
     * Validity of cached file is tested against last modification date and file size, local cache file is encoded using MD5.
     */
    public static File getLocalCopy(String remoteGsiURI, final ProgressAgent subAgent) throws GridFTPException
    {
        Object[] hostPortPath = splitToHostPortPath(remoteGsiURI);
        try {
            final GridFTPConnection connection = GridFTPConnection.getConnection((String) hostPortPath[0], (Integer) hostPortPath[1]);
            String remotePath = (String) hostPortPath[2];
            System.out.println(remotePath);
            MlsxEntry mlsxEntry = connection.mlst(remotePath);

            String sizeString = mlsxEntry.get(MlsxEntry.SIZE);
            String modifyString = mlsxEntry.get(MlsxEntry.MODIFY);

            String localFileNameHash = getLocalFilenameHash(remoteGsiURI, modifyString, sizeString);
            String localFileName = LOCAL_TMP_PREFIX + localFileNameHash;
            final File localFile = new File(localFileName);
            final long remoteFileSize = Long.parseLong(sizeString);

            if (localFile.exists() && localFile.length() == remoteFileSize) return localFile;
            else {
                if (localFile.exists()) localFile.delete();
                localFile.createNewFile();
                System.out.println("Trying to copy " + remotePath + " to " + localFile);
//                connection.get(remotePath, localFile);

                ProgressRunnable progressRunnable = new ProgressRunnable()
                {
                    private final int SHORT_IDLE_TIMEOUT = 2000;
                    private final int INIT_IDLE_TIMEOUT = 10000;
                    private final int IDLE_TIMEOUT = 30000;
                    private final int SLEEP_TIME = 500;
                    private long fileSizeInProgress = 0;
                    private int idleTime = 0;
                    private boolean timeout = false;
//                    private boolean die = false;
                    private boolean shortTimeout = false;

                    @Override
                    public void run()
                    {
                        try {
                            while (//!die && 
                                    fileSizeInProgress < remoteFileSize &&
                                    (shortTimeout && idleTime < SHORT_IDLE_TIMEOUT ||
                                    !shortTimeout && fileSizeInProgress == 0 && idleTime <= INIT_IDLE_TIMEOUT ||
                                    !shortTimeout && fileSizeInProgress > 0 && idleTime <= IDLE_TIMEOUT)) {

                                Thread.currentThread().sleep(SLEEP_TIME);

                                if (localFile.length() == fileSizeInProgress)
                                    idleTime += SLEEP_TIME;
                                else
                                    idleTime = 0;

                                fileSizeInProgress = localFile.length();
                                subAgent.setProgress((double) fileSizeInProgress / remoteFileSize);
                            }

                            if (fileSizeInProgress != remoteFileSize) {
                                timeout = true;
                                connection.abortCurrentTransfer();
                                connection.close();

                            }
                        } catch (InterruptedException | IOException | ServerException ex) {
                            throw new IllegalStateException(ex);
                        }
                    }

//                    @Override
//                    public void stop()
//                    {
//                        die = true;
//                    }
                    public boolean isTimeout()
                    {
                        return timeout;
                    }

                    @Override
                    public void setShortTimeout()
                    {
                        shortTimeout = true;
                    }
                };

                Thread progressThread = new Thread(progressRunnable);

                progressThread.start();

                boolean connectionClosed = false;
                try {
                    connection.get(remotePath, localFile);
                    connection.close();
                    connectionClosed = true;
                } catch (ServerException | ClientException | IOException ex) {
                    if (progressRunnable.isTimeout())
                        throw new GridFTPException("Connection timeout");
                    else {
                        LOGGER.warn("Problem while accessing GridFTP resource", ex);
                        progressRunnable.setShortTimeout();
                    }
                }

                progressThread.join();
                if (!connectionClosed)
                    connection.close();

                return localFile;
            }
        } catch (ClientException | ServerException | GSSException | GeneralSecurityException | IOException | InterruptedException | NumberFormatException ex) {
            LOGGER.error("Problem occured while accessing Grid location", ex);
            throw new GridFTPException(ex);
        }
    }

    /**
     * If {@code localFileNameOrRemoteGsiURI} is path to local file then returns this file, otherwise if it is valid gsi URI (see
     * {@link #isGSIUri(java.lang.String)}) then downloads file from remote location.
     */
    public static File getFileOrLocalGSIFTPCopy(String localFileNameOrRemoteGsiURI, final ProgressAgent subAgent) throws GridFTPException
    {
        File f;
        if (GridFTPLocalUtils.isGSIUri(localFileNameOrRemoteGsiURI))
            f = GridFTPLocalUtils.getLocalCopy(localFileNameOrRemoteGsiURI, subAgent.getSubAgent(subAgent.getTotalSteps()));
        else {
            f = new File(localFileNameOrRemoteGsiURI);
            subAgent.increase(subAgent.getTotalSteps());
        }
        return f;
    }

    public static void main(String[] args) throws GridFTPException
    {
//        System.out.println(getLocalCopy("gsiftp://zeus.cyfronet.pl/~/regular2.jpg"));
//        System.out.println(getLocalCopy("gsiftp://zeus.cyfronet.pl/~/testowyput.txt"));
        System.out.println(getLocalCopy("gsiftp://ui.plgrid.icm.edu.pl/~/data/43200x21600FloatBig.dat", ProgressAgent.getDummyAgent()));
        System.out.println("End main thread");
    }
}
