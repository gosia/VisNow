package pl.edu.icm.visnow.lib.grid.gridftp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import org.globus.gsi.GSIConstants;
import org.globus.gsi.GlobusCredential;
import org.globus.gsi.OpenSSLKey;
import org.globus.gsi.bc.BouncyCastleCertProcessingFactory;
import org.globus.gsi.bc.BouncyCastleOpenSSLKey;
import org.globus.gsi.util.CertificateLoadUtil;
import org.gridforum.jgss.ExtendedGSSManager;

/**
 * Proxy necessary to connect and transfer files using globus gridftp.
 *
 * It's used to establish connection using certificates found at specified paths.
 * Here is created in-memory file necessary to prepare credentials in {@link ExtendedGSSManager}.
 *
 * @see <a href="http://www.globus.org/toolkit/docs/5.2/5.2.4/gridftp/developer/">GT 5.2.4 GridFTP : Developer's Guide</a>
 *
 * @author szpak
 */
class GridFTPProxyInit
{

    protected X509Certificate[] certificates;
    private PrivateKey userKey = null;

    /**
     * Verifies if {@code file} exists and if it's a regular file.
     */
    private static void verify(String file) throws IOException
    {
        if (file == null)
            throw new IOException("Empty filename");

        File f = new File(file);
        if (!f.exists() || f.isDirectory())
            throw new IOException("Filename " + file + " not found");
    }

    /**
     * Just wraps {@link CertificateLoadUtil#loadCertificates}; Loads certificate from local path.
     *
     * @param certPath path to user certificate in .pem format
     */
    private void loadCertificates(String certPath) throws IOException, GeneralSecurityException
    {
        certificates = CertificateLoadUtil.loadCertificates(certPath);
    }

    /**
     * Loads and verifies key using {@link BouncyCastleOpenSSLKey}. Key cannot be encrypted.
     *
     * @param keyPath path to user key in .pem format
     */
    private void loadKey(String keyPath) throws IOException, GeneralSecurityException
    {
        OpenSSLKey key = new BouncyCastleOpenSSLKey(keyPath);

        if (key.isEncrypted())
            throw new GeneralSecurityException("Key cannot be encrypted");

        userKey = key.getPrivateKey();
    }

    /**
     * Creates credential (proxy) based on previously loaded user key and certificates.
     */
    private GlobusCredential sign(int bits, int lifetime, GSIConstants.CertificateType proxyType) throws GeneralSecurityException
    {
        BouncyCastleCertProcessingFactory factory
            = BouncyCastleCertProcessingFactory.getDefault();

        return factory.createCredential(certificates,
                                        userKey,
                                        bits,
                                        lifetime,
                                        proxyType.getCode(),
                                        null);
    }

    /**
     * Access method to get proxy data based on paths to user cert and key in .pem format.
     *
     * @param certPath path to user certificate in .pem format
     * @param keyPath  path to user key in .pem format
     */
    public byte[] createProxy(String certPath, String keyPath, int bits, int lifetime, GSIConstants.CertificateType proxyType) throws IOException, GeneralSecurityException
    {

        verify(certPath);
        verify(keyPath);

        loadCertificates(certPath);

        loadKey(keyPath);
        GlobusCredential proxy = sign(bits, lifetime, proxyType);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        proxy.save(baos);

        return baos.toByteArray();
    }
}
