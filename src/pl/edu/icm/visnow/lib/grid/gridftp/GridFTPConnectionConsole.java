//<editor-fold defaultstate="collapsed" desc=" License ">

/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.grid.gridftp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Vector;
import org.globus.ftp.FileInfo;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 * Simple test console for testing GridFTPConnection. Hardcoded to qcg.plgrid.icm.edu.pl
 *
 * Only supported commands are:
 * <ul>
 * <li>cd [absolute_or_relative_path] - change dir (CWD)
 * <li>ls - list files in current directory (LIST)
 * <li>quit - quit console
 * </ul>
 *
 * @author szpak
 */
public class GridFTPConnectionConsole
{

    private static String host = "qcg.plgrid.icm.edu.pl";
    private static int port = 2811;

    public static void main(String[] args) throws Exception
    {
        VisNow.initLogging(true);

        GridFTPConnection connection = GridFTPConnection.getConnection(host, port);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.print(connection.getCurrentDir() + " > ");

            String line = in.readLine();
            String[] command = line.split(" ");

            if (command[0].equals("cd")) {
                try {
                    connection.changeDir(command[1]);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if (command[0].equals("ls")) {
                Vector v = new Vector();
                try {
                    v = connection.list();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                while (!v.isEmpty()) {
                    FileInfo f = (FileInfo) v.remove(0);
                    System.out.println(f.toString());
                }
            } else if (command[0].equals("quit"))
                break;
        }
    }
}
