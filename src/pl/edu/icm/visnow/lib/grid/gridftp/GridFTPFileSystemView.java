//<editor-fold defaultstate="collapsed" desc=" License ">

/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.grid.gridftp;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.filechooser.FileSystemView;
import org.apache.log4j.Logger;
import org.globus.ftp.FileInfo;
import org.globus.ftp.exception.ClientException;
import org.globus.ftp.exception.ServerException;
import org.ietf.jgss.GSSException;

/**
 * GridFTPFileSystem together with GridFTPFile provide virtual file system view based on GridFTP directory structure.
 * It's based on FileSystemView and overwrites all of its methods.
 *
 * GridFTPFileSystem is designed to be :
 * <ul>
 * <li>always rooted at "/" (this might be incorrect assumption but for the time being let's assume it's all right)
 * <li>path separated with single slash character: "/" (so only unix-type servers are supported)
 * <li>home directory is set to first work dir just after connection
 * <li>whole file system is read only (to make things easier)
 * </ul>
 *
 *
 * @author szpak
 */
public class GridFTPFileSystemView extends FileSystemView
{

    private static final Logger LOGGER = Logger.getLogger(GridFTPFileSystemView.class);

    //    @Override
    //    static GridFTPFileSystemView createFileSystemView(String host, int port) {
    //        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    //    }
    private GridFTPConnection connection;
    //ignore "fake" fileNames in list (getFiles) like "." or ".."
    private static Set<String> ignoredFileNames = new HashSet<String>(Arrays.asList(new String[]{".", ".."}));
    //TODO: temporary flag
    private static boolean singleton_taken = false;
    /**
     * This home dir should be updated every time host is changed
     */
    private GridFTPFile homeDir = null;
    private GridFTPFile rootDir = null;

    //    private String hostname = "qcg.plgrid.icm.edu.pl";
    //    private String hostname = "qcg.reef.man.poznan.pl";
    //    private int port = 2811;
    private GridFTPFileSystemView(String hostname, int port) throws IOException, ServerException, ClientException, GSSException, CertificateException, GeneralSecurityException
    {
        LOGGER.debug("");
        //        try {
        connection = GridFTPConnection.getConnection(hostname, port);
        homeDir = new GridFTPFile(connection, connection.getCurrentDir(), null, null);
        //        } catch (Exception ex) {
        //            LOGGER.error("Cannot connect to hostname: " + hostname, ex);
        //            homeDir = new GridFTPFile(null, GridFTPFile.root, null, null);
        //        }
    }

    void killConnection() throws IOException, ServerException
    {
        LOGGER.info("");
        if (connection != null)
            connection.close();
    }

    /**
     * Returns home dir (first accessed after connection) as default directory.
     *
     * @return home dir
     */
    @Override
    public synchronized File getDefaultDirectory()
    {
        LOGGER.debug("");
        return homeDir;
    }

    //TODO: add/test support for multiple fsv
    //TODO: return null 
    /**
     * Returns current (should be new) connection for specified host/port.
     */
    public synchronized static GridFTPFileSystemView getFileSystemView(String hostname, int port)
    {
        //        LOGGER.debug("");
        //        if (singleton_taken) throw new UnsupportedOperationException("Multiple FileSystemView not supported yet / singleton not implemented yet");
        try {
            return createFileSystemView(hostname, port);
            //            singleton_taken = true;
            //            return new GridFTPFileSystemView(hostname, port);
        } catch (Exception ex) {
            return null;
            //            throw new RuntimeException(ex);
        }
    }

    public synchronized static GridFTPFileSystemView createFileSystemView(String hostname, int port) throws IOException, ServerException, ClientException, GSSException, CertificateException, GeneralSecurityException
    {
        LOGGER.debug("");
        if (singleton_taken)
            throw new UnsupportedOperationException("Multiple FileSystemView not supported yet / singleton not implemented yet");
        singleton_taken = true;
        return new GridFTPFileSystemView(hostname, port);
    }

    @Override
    public synchronized boolean isRoot(File f)
    {
        LOGGER.debug(f.getPath());
        return f.getAbsolutePath().equals(GridFTPFile.root);
    }

    /**
     * Assume that every passed file can be traversed (this is based on fact that file should be GridFTPFile which reflects real file)
     *
     * @return true
     */
    @Override
    public synchronized Boolean isTraversable(File f)
    {
        boolean dir = f.isDirectory();
        LOGGER.debug(f.getPath() + " " + dir);
        return dir;
    }

    /**
     * Returns always getName as system name
     */
    @Override
    public synchronized String getSystemDisplayName(File f)
    {
        LOGGER.debug("");
        return f.getName();
    }

    @Override
    public synchronized String getSystemTypeDescription(File f)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return super.getSystemTypeDescription(f); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public synchronized Icon getSystemIcon(File f)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return super.getSystemIcon(f); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Determines parent - child relationship. This is done by comparing file.getParent() and folder.getPath().
     */
    @Override
    public synchronized boolean isParent(File folder, File file)
    {
        LOGGER.debug(folder.getPath() + " " + file.getPath());
        return folder.getPath().equals(file.getParent());
    }

    @Override
    public synchronized File getChild(File parent, String fileName)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return super.getChild(parent, fileName); //To change body of generated methods, choose Tools | Templates.
    }

    //TODO: add support for symlinks ??
    /**
     *
     * Returns always true; only true files/directories are allowed here
     */
    @Override
    public synchronized boolean isFileSystem(File f)
    {
        LOGGER.debug(f.getPath());
        return true;
    }

    @Override
    public synchronized boolean isHiddenFile(File f)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
        //        return super.isHiddenFile(f); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Same as {@link isRoot}.
     */
    @Override
    public synchronized boolean isFileSystemRoot(File dir)
    {
        LOGGER.debug(dir.getPath());
        return isRoot(dir);
    }

    /**
     * Returns always false; none of files is considered as drive
     */
    @Override
    public synchronized boolean isDrive(File dir)
    {
        LOGGER.debug(dir.getPath());
        return false;
    }

    /**
     * Returns always false; none of files is considered as drive
     */
    @Override
    public synchronized boolean isFloppyDrive(File dir)
    {
        LOGGER.debug(dir.getPath());
        return false;
    }

    /**
     * Returns always false; none of files is considered as computer node
     */
    @Override
    public synchronized boolean isComputerNode(File dir)
    {
        LOGGER.debug(dir.getPath());
        return false;
    }

    private void findRootDir() throws IOException, ServerException, ClientException
    {
        if (rootDir == null)
            rootDir = new GridFTPFile(connection, GridFTPFile.root, null, null);
    }

    @Override
    public synchronized File[] getRoots()
    {
        LOGGER.debug("");
        try {
            findRootDir();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        return new GridFTPFile[]{rootDir};
    }

    /**
     * Gives home directory (which is landing directory after connection to host).
     *
     * @return home directory on the server
     */
    @Override
    public synchronized File getHomeDirectory()
    {
        LOGGER.debug("");
        return homeDir;
    }

    /**
     * Create file object based on parent {@code dir} and {@code filename}.
     */
    @Override
    public synchronized File createFileObject(File dir, String filename)
    {
        LOGGER.debug("dir: " + dir + " filename: " + filename);
        try {
            return new GridFTPFile(connection, dir, filename, null, null);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Create file object assuming that path is an absolute path.
     *
     * @param path absolute path to object
     */
    @Override
    public synchronized File createFileObject(String path)
    {
        LOGGER.debug("path: " + path);
        try {
            return new GridFTPFile(connection, path, null, null);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public synchronized File[] getFiles(File dir, boolean useFileHiding)
    {
        LOGGER.debug(dir + " " + useFileHiding + " " + Thread.currentThread().getId());
        synchronized (connection) {
            String absolutePath = dir.getAbsolutePath();
            List<FileInfo> files;
            try {
                connection.changeDir(absolutePath);
            } catch (Exception ex) {
                LOGGER.warn("cannot change dir to: " + absolutePath, ex);
                return new File[0];
            }
            try {
                if ((files = GetFilesCache.get(dir)) == null) {
                    files = (List<FileInfo>) connection.list();
                    if (files.size() > 0)
                        GetFilesCache.put(dir, files);
                }
            } catch (Exception ex) {
                LOGGER.error("Error while listing files", ex);
                return new File[0];
            }
            ArrayList<GridFTPFile> list = new ArrayList<GridFTPFile>();
            for (FileInfo f : files) {
                try {
                    if (!ignoredFileNames.contains(f.getName()))
                        list.add(new GridFTPFile(connection, dir, f.getName(), f, null));
                } catch (Exception ex) {
                    LOGGER.warn("Error while constructing GridFTPFile object", ex);
                }
            }
            LOGGER.debug("return " + list.size() + " files");
            return list.toArray(new File[list.size()]);
        }
    }

    @Override
    public synchronized File getParentDirectory(File dir)
    {
        LOGGER.debug("");
        return dir.getParentFile();
    }

    @Override
    protected File createFileSystemRoot(File f)
    {
        LOGGER.debug("");
        try {
            findRootDir();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        return rootDir;
    }

    //TODO: temporal solution -> change to SimpleMemCache + cache per filesystemview (or synch)
    void wipeOutCache()
    {
        GetFilesCache.wipeOut();
        GridFTPFile.wipeOutCache();
    }

    //TODO: possibly temporal solution -> refactor
    String getConnectionURI()
    {
        return connection.getURI();
    }

    @Override
    public synchronized File createNewFolder(File containingDir) throws IOException
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        throw new IOException("New file creation is not supported!");
    }

    static class GetFilesCache
    {

        private static final HashMap<String, GetFilesCache> getFilesCache = new HashMap<String, GetFilesCache>();
        private static final long maxTime = 3600000l; //120 seconds
        private List<FileInfo> listCache;
        private long timestamp;

        public GetFilesCache(List<FileInfo> listCache)
        {
            this.listCache = listCache;
            this.timestamp = System.currentTimeMillis();
        }

        static List<FileInfo> get(File dir)
        {
            String key = generateKey(dir);
            GetFilesCache entry = getFilesCache.get(key);
            if (entry == null || entry.timestamp + maxTime < System.currentTimeMillis())
                return null;
            return entry.listCache;
        }

        static void put(File dir, List<FileInfo> list)
        {
            String key = generateKey(dir);
            getFilesCache.put(key, new GetFilesCache(list));
        }

        //TODO: make it hostname independent (extend key)
        private static String generateKey(File dir)
        {
            return dir.toURI().getPath();
        }

        static void wipeOut()
        {
            getFilesCache.clear();
        }
    }
}
