//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.Locale;
import javax.swing.JOptionPane;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.ByteDataArray;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jscic.dataarrays.DoubleDataArray;
import pl.edu.icm.jscic.dataarrays.FloatDataArray;
import pl.edu.icm.jscic.dataarrays.IntDataArray;
import pl.edu.icm.jscic.dataarrays.ShortDataArray;
import pl.edu.icm.jscic.utils.EngineeringFormattingUtils;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.lib.basic.writers.FieldWriter.Params;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class VisNowRegularFieldWriterCore extends FieldWriterCore
{

    public static final int MAXLEN = 268435455; //2^28 - 1;
    RegularField regularInField;

    public VisNowRegularFieldWriterCore(RegularField regularInField, Params params)
    {
        super(regularInField, params);
        this.regularInField = regularInField;
    }

    private void writeBinary()
    {
        try {
            File outFile = new File(genFileName + ".vnd");
            //System.out.println("writing " + outFile.getName());
            headerWriter.println("file \"" + outFile.getName() + "\" binary");
            largeContentOutput = new MemoryMappedFileWriter(new RandomAccessFile(outFile, "rw"));
            float[] timeSteps = regularInField.getTimesteps();
            if (timeSteps.length == 1) {
                if (regularInField.getCurrentMask() != null) {
                    headerWriter.println("mask");
                    LogicLargeArray mask = regularInField.getCurrentMask();
                    largeContentOutput.writeLogicLargeArray(mask, 0, mask.length());
                }
                if (regularInField.getCurrentCoords() != null) {
                    headerWriter.println("coords");
                    FloatLargeArray coords = regularInField.getCurrentCoords();
                    largeContentOutput.writeFloatLargeArray(coords, 0, coords.length());
                }
                for (int i = 0; i < regularInField.getNComponents(); i++) {
                    DataArray da = regularInField.getComponent(i);
                    if (!da.isNumeric())
                        continue;
                    headerWriter.println(da.getName().replace(' ', '_').replace('.', '_'));
                    long length = da.getNElements() * da.getVectorLength();
                    largeContentOutput.writeLargeArray(da.getRawArray(), 0, length);
                }
            } else
                for (int step = 0; step < timeSteps.length; step++) {
                    float t = timeSteps[step];
                    headerWriter.println("timestep " + t);
                    if (regularInField.isMaskTimestep(t)) {
                        headerWriter.println("mask");
                        LogicLargeArray mask = regularInField.getCurrentMask();
                        largeContentOutput.writeLogicLargeArray(mask, 0, mask.length());
                    }
                    if (regularInField.isCoordTimestep(t)) {
                        headerWriter.println("coords");
                        FloatLargeArray coords = regularInField.getCurrentCoords();
                        largeContentOutput.writeFloatLargeArray(coords, 0, coords.length());
                    }
                    for (int i = 0; i < regularInField.getNComponents(); i++) {
                        DataArray da = regularInField.getComponent(i);
                        if (!da.isNumeric() || !da.isTimestep(t))
                            continue;
                        headerWriter.println(da.getName().replace(' ', '_').replace('.', '_'));
                        long length = da.getNElements() * da.getVectorLength();
                        largeContentOutput.writeLargeArray(da.getRawArray(t), 0, length);
                    }
                    headerWriter.println("end");
                }
            largeContentOutput.close();
        } catch (IOException e) {
        }
    }

    private void writeASCII()
    {
        DataArray da;
        int[] dataFormLengths
            = {
                4, 4, 5, 7, 10, 14
            };
        try {
            File outFile = new File(genFileName + ".txt");
            //System.out.println("writing " + outFile.getName());
            headerWriter.println("file \"" + outFile.getName() + "\" ascii col");
            contentWriter = new PrintWriter(new FileOutputStream(outFile));
            float[] timeSteps = regularInField.getTimesteps();
            if (timeSteps.length == 1) {
                headerWriter.println("skip 1");
                int nCols = 0;
                if (regularInField.getCurrentMask() != null) {
                    headerWriter.print("mask, ");
                    contentWriter.printf(Locale.US, "mask");
                    nCols += 1;
                }
                if (regularInField.getCurrentCoords() != null) {
                    headerWriter.print("coords, ");
                    contentWriter.printf(Locale.US, "%" + (10 * regularInField.getNSpace() - 2) + "s  ", "coordinates");
                    nCols += regularInField.getNSpace();
                }
                for (int i = 0; i < regularInField.getNComponents(); i++) {
                    da = regularInField.getComponent(i);
                    if (!da.isNumeric())
                        continue;
                    nCols += da.getVectorLength();
                    headerWriter.println(da.getName().replace(' ', '_').replace('.', '_'));
                    //String entry = da.getName().replace(' ', '_').replace('.', '_') +
                    //    "                                                                         ";
                    //contentWriter.print(" " + (entry).substring(0, da.getVectorLength() * dataFormLengths[da.getType().getValue()] - 1));

                    contentWriter.print("" + da.getName().replace(' ', '_').replace('.', '_') + "\t");
                }
                contentWriter.println();
                headerWriter.println();
                LogicLargeArray[] boolArrs = new LogicLargeArray[nCols];
                byte[][] byteArrs = new byte[nCols][];
                short[][] shortArrs = new short[nCols][];
                int[][] intArrs = new int[nCols][];
                float[][] floatArrs = new float[nCols][];
                double[][] dblArrs = new double[nCols][];
                DataArrayType[] types = new DataArrayType[nCols];
                int[] vlens = new int[nCols];
                int[] ind = new int[nCols];
                int iCol = 0;

                if (regularInField.getCurrentMask() != null) {
                    types[iCol] = DataArrayType.FIELD_DATA_LOGIC;
                    ind[iCol] = 0;
                    vlens[iCol] = 1;
                    iCol += 1;
                    boolArrs[iCol] = regularInField.getCurrentMask();
                }
                if (regularInField.getCurrentCoords() != null)
                    for (int j = 0; j < regularInField.getNSpace(); j++, iCol++) {
                        types[iCol] = DataArrayType.FIELD_DATA_FLOAT;
                        ind[iCol] = j;
                        vlens[iCol] = regularInField.getNSpace();
                        floatArrs[iCol] = regularInField.getCurrentCoords() == null ? null : regularInField.getCurrentCoords().getData();
                    }
                for (int i = 0; i < regularInField.getNComponents(); i++) {
                    da = regularInField.getComponent(i);
                    if (!da.isNumeric())
                        continue;
                    for (int j = 0; j < da.getVectorLength(); j++, iCol++) {
                        types[iCol] = da.getType();
                        ind[iCol] = j;
                        vlens[iCol] = da.getVectorLength();
                        switch (types[iCol]) {
                            case FIELD_DATA_BYTE:
                                byteArrs[iCol] = (byte[]) regularInField.getComponent(i).getRawArray().getData();
                                break;
                            case FIELD_DATA_SHORT:
                                shortArrs[iCol] = (short[]) regularInField.getComponent(i).getRawArray().getData();
                                break;
                            case FIELD_DATA_INT:
                                intArrs[iCol] = (int[]) regularInField.getComponent(i).getRawArray().getData();
                                break;
                            case FIELD_DATA_FLOAT:
                                floatArrs[iCol] = (float[]) regularInField.getComponent(i).getRawArray().getData();
                                break;
                            case FIELD_DATA_DOUBLE:
                                dblArrs[iCol] = (double[]) regularInField.getComponent(i).getRawArray().getData();
                                break;
                        }
                    }
                }
                for (int k = 0; k < regularInField.getNNodes(); k++) {
                    for (int l = 0; l < nCols; l++)
                        switch (types[l]) {
                            case FIELD_DATA_LOGIC:
                                contentWriter.print(boolArrs[l].getBoolean(ind[l]) ? "1\t" : "0\t");
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_BYTE:
                                contentWriter.printf(Locale.US, "%3d\t", byteArrs[l][ind[l]] & 0xff);
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_SHORT:
                                contentWriter.printf(Locale.US, "%4d\t", shortArrs[l][ind[l]]);
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_INT:
                                contentWriter.printf(Locale.US, "%6d\t", intArrs[l][ind[l]]);
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_FLOAT:
                                contentWriter.printf(Locale.US, "%9.4f\t", floatArrs[l][ind[l]]);
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_DOUBLE:
                                contentWriter.printf(Locale.US, "%13.6f\t", dblArrs[l][ind[l]]);
                                ind[l] += vlens[l];
                                break;
                        }
                    contentWriter.println();
                }
            } else
                for (int step = 0; step < timeSteps.length; step++) {
                    float t = timeSteps[step];
                    headerWriter.println("timestep " + t);
                    headerWriter.println("skip 1");
                    int nCols = 0;
                    if (regularInField.isMaskTimestep(t)) {
                        headerWriter.print("mask, ");
                        contentWriter.printf(Locale.US, "mask");
                        nCols += 1;
                    }
                    if (regularInField.isCoordTimestep(t)) {
                        headerWriter.print("coords, ");
                        contentWriter.printf(Locale.US, "%" + (10 * regularInField.getNSpace() - 2) + "s  ", "coordinates");
                        nCols += regularInField.getNSpace();
                    }
                    
                    for (int i = 0; i < regularInField.getNComponents(); i++) {
                        da = regularInField.getComponent(i);
                        if (!da.isNumeric() || !da.isTimestep(t))
                            continue;
                        nCols += da.getVectorLength();
                        headerWriter.println(da.getName().replace(' ', '_').replace('.', '_'));
//                        String entry = da.getName().replace(' ', '_').replace('.', '_') +
//                            "                                                                         ";
//                        contentWriter.print(" " + (entry).substring(0, da.getVectorLength() * dataFormLengths[da.getType().getValue()] - 1));

                        contentWriter.print("" + da.getName().replace(' ', '_').replace('.', '_') + "\t");
                    }
                    contentWriter.println();
                    headerWriter.println();
                    headerWriter.println("end");

                    LogicLargeArray[] boolArrs = new LogicLargeArray[nCols];
                    byte[][] byteArrs = new byte[nCols][];
                    short[][] shortArrs = new short[nCols][];
                    int[][] intArrs = new int[nCols][];
                    float[][] floatArrs = new float[nCols][];
                    double[][] dblArrs = new double[nCols][];
                    DataArrayType[] types = new DataArrayType[nCols];
                    int[] vlens = new int[nCols];
                    int[] ind = new int[nCols];
                    int iCol = 0;

                    if (regularInField.isMaskTimestep(t)) {
                        types[iCol] = DataArrayType.FIELD_DATA_LOGIC;
                        ind[iCol] = 0;
                        vlens[iCol] = 1;
                        iCol += 1;
                        boolArrs[iCol] = regularInField.getCurrentMask();
                    }
                    if (regularInField.isCoordTimestep(t))
                        for (int j = 0; j < regularInField.getNSpace(); j++, iCol++) {
                            types[iCol] = DataArrayType.FIELD_DATA_FLOAT;
                            ind[iCol] = j;
                            vlens[iCol] = regularInField.getNSpace();
                            floatArrs[iCol] = regularInField.getCurrentCoords() == null ? null : regularInField.getCurrentCoords().getData();
                        }
                    for (int i = 0; i < regularInField.getNComponents(); i++) {
                        da = regularInField.getComponent(i);
                        if (!da.isNumeric() || !da.isTimestep(t))
                            continue;
                        for (int j = 0; j < da.getVectorLength(); j++, iCol++) {
                            types[iCol] = da.getType();
                            ind[iCol] = j;
                            vlens[iCol] = da.getVectorLength();
                            switch (types[iCol]) {
                                case FIELD_DATA_BYTE:
                                    byteArrs[iCol] = ((ByteDataArray) da).getRawArray(t).getByteData();
                                    break;
                                case FIELD_DATA_SHORT:
                                    shortArrs[iCol] = ((ShortDataArray) da).getRawArray(t).getShortData();
                                    break;
                                case FIELD_DATA_INT:
                                    intArrs[iCol] = ((IntDataArray) da).getRawArray(t).getIntData();
                                    break;
                                case FIELD_DATA_FLOAT:
                                    floatArrs[iCol] = ((FloatDataArray) da).getRawArray(t).getData();
                                    break;
                                case FIELD_DATA_DOUBLE:
                                    dblArrs[iCol] = ((DoubleDataArray) da).getRawArray(t).getDoubleData();
                                    break;
                            }
                        }
                    }
                    for (int k = 0; k < regularInField.getNNodes(); k++) {
                        for (int l = 0; l < nCols; l++)
                            switch (types[l]) {
                                case FIELD_DATA_LOGIC:
                                    contentWriter.print(boolArrs[l].getBoolean(ind[l]) ? "1\t" : "0\t");
                                    ind[l] += vlens[l];
                                    break;
                                case FIELD_DATA_BYTE:
                                    contentWriter.printf(Locale.US, "%3d\t", byteArrs[l][ind[l]] & 0xff);
                                    ind[l] += vlens[l];
                                    break;
                                case FIELD_DATA_SHORT:
                                    contentWriter.printf(Locale.US, "%4d\t", shortArrs[l][ind[l]]);
                                    ind[l] += vlens[l];
                                    break;
                                case FIELD_DATA_INT:
                                    contentWriter.printf(Locale.US, "%6d\t", intArrs[l][ind[l]]);
                                    ind[l] += vlens[l];
                                    break;
                                case FIELD_DATA_FLOAT:
                                    contentWriter.printf(Locale.US, "%9.4f\t", floatArrs[l][ind[l]]);
                                    ind[l] += vlens[l];
                                    break;
                                case FIELD_DATA_DOUBLE:
                                    contentWriter.printf(Locale.US, "%13.6f\t", dblArrs[l][ind[l]]);
                                    ind[l] += vlens[l];
                                    break;
                            }
                        contentWriter.println();
                    }
                }
            contentWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean writeField()
    {
        return writeField(regularInField, params.getFileName(), params.isAscii(), params.isSingleFile());
    }

    public boolean writeField(RegularField regularInField, String fileName, boolean ascii, boolean single)
    {
        if (regularInField == null)
            return false;
        outFileName = fileName;
        genFileName = fileName;
        DataArray da;

        if (outFileName.endsWith(".vnf"))
            genFileName = outFileName.substring(0, outFileName.lastIndexOf(".vnf"));
        else
            outFileName = genFileName + ".vnf";
        String headerFileName = fileName;
        outFileName = fileName;

        if (outFileName.endsWith(".vnf"))
            outFileName = outFileName.substring(0, outFileName.lastIndexOf(".vnf"));

        headerFileName = outFileName + ".vnf";
        File headerFile = new File(headerFileName);
        if (headerFile.exists() && !headerFile.canWrite()) {
            JOptionPane.showMessageDialog(null, "Cannot write file");
            return false;
        }
        if (new File(outFileName).exists()) {
            Object ans = JOptionPane.showConfirmDialog(null,
                                                       "File exists. Owerwrite?");
            if (ans instanceof Integer && (Integer) ans == JOptionPane.NO_OPTION)
                return false;
        }

        try {
            headerWriter = new PrintWriter(new FileOutputStream(headerFile));
            headerWriter.println("#VisNow regular field");
            if (regularInField.getName() != null && !regularInField.getName().trim().isEmpty())
                headerWriter.print("field \"" + regularInField.getName() + "\",");
            headerWriter.print(" dims ");
            for (int i = 0; i < regularInField.getDims().length; i++)
                headerWriter.print(" " + regularInField.getDims()[i]);
            if (regularInField.hasMask())
                headerWriter.print(", mask");
            if (regularInField.getCurrentCoords() != null)
                headerWriter.print(", coords");
            if (regularInField.getUserData() != null) {
                headerWriter.print(", user:");
                String[] udata = regularInField.getUserData();
                for (int j = 0; j < udata.length; j++) {
                    if (j > 0)
                        headerWriter.print(";");
                    headerWriter.print("\"" + udata[j] + "\"");
                }
            }
            headerWriter.println();
            if (regularInField.getCurrentCoords() == null) {
                float[][] af = regularInField.getAffine();
                headerWriter.printf(Locale.US, "origin %10.4e %10.4e %10.4e %n", af[3][0], af[3][1], af[3][2]);
                for (int i = 0; i < 3; i++)
                    headerWriter.printf(Locale.US, "    v%d %10.4e %10.4e %10.4e %n", i, af[i][0], af[i][1], af[i][2]);
            }
            float[][] userExtents = regularInField.getPhysExtents();
            if (inField.getCoordsUnits() != null && inField.getCoordsUnits().length == 3) {
                String[] cu = inField.getCoordsUnits();
                headerWriter.printf(Locale.US, "user extent x %10.4e %10.4e %s%n", userExtents[0][0], userExtents[1][0], cu[0]);
                headerWriter.printf(Locale.US, "user extent y %10.4e %10.4e %s%n", userExtents[0][1], userExtents[1][1], cu[1]);
                headerWriter.printf(Locale.US, "user extent z %10.4e %10.4e %s%n", userExtents[0][2], userExtents[1][2], cu[2]);
            } else {
                headerWriter.printf(Locale.US, "user extent x %10.4e %10.4e%n", userExtents[0][0], userExtents[1][0]);
                headerWriter.printf(Locale.US, "user extent y %10.4e %10.4e%n", userExtents[0][1], userExtents[1][1]);
                headerWriter.printf(Locale.US, "user extent z %10.4e %10.4e%n", userExtents[0][2], userExtents[1][2]);
            }
            int maxCmpNameLen = 0;
            for (int i = 0; i < regularInField.getNComponents(); i++)
                if (regularInField.getComponent(i).isNumeric() && regularInField.getComponent(i).getName().length() > maxCmpNameLen)
                    maxCmpNameLen = regularInField.getComponent(i).getName().length();
            for (int i = 0; i < regularInField.getNComponents(); i++) {
                da = regularInField.getComponent(i);
                if (!da.isNumeric())
                    continue;
                headerWriter.printf(Locale.US, "component %" + maxCmpNameLen + "s %7s", da.getName().replace(' ', '_').replace('.', '_'), dataTypes[da.getType().getValue()]);
                if (da.getVectorLength() > 1)
                    if (da.getMatrixDims()[0] != da.getVectorLength()) {
                        headerWriter.print(", array " + da.getMatrixDims()[0]);
                        if (da.isSymmetric())
                            headerWriter.print(", sym");
                    } else
                        headerWriter.print(", vector " + da.getVectorLength());
                if (da.getUnit() != null && !da.getUnit().isEmpty())
                    headerWriter.print(", unit " + da.getUnit());
                headerWriter.print(", preferred_min " + da.getPreferredMinValue());
                headerWriter.print(", preferred_max " + da.getPreferredMaxValue());
                headerWriter.print(", preferred_phys_min " + da.getPreferredPhysMinValue());
                headerWriter.print(", preferred_phys_max " + da.getPreferredPhysMaxValue());
                if (da.getUserData() != null) {
                    headerWriter.print(", user:");
                    String[] udata = da.getUserData();
                    for (int j = 0; j < udata.length; j++) {
                        if (j > 0)
                            headerWriter.print(";");
                        headerWriter.print("\"" + udata[j] + "\"");
                    }
                }
                headerWriter.println();
            }
            if (ascii)
                writeASCII();
            else
                writeBinary();
            headerWriter.close();
            return true;
        } catch (FileNotFoundException e) {
            LOGGER.error("Error writing field", e);
            return false;
        }
    }
}
