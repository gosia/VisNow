//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.io;

import pl.edu.icm.jscic.dataarrays.DataArray;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import javax.swing.JOptionPane;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.Cell;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jscic.utils.EngineeringFormattingUtils;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.visnow.lib.basic.writers.FieldWriter.Params;

/**
 * @author Krzysztof Nowinski (know@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class VisNowIrregularFieldWriterCore extends FieldWriterCore
{

    public static final int MAXLEN = 268435455; //2^28 - 1;
    IrregularField irregularInField;

    public VisNowIrregularFieldWriterCore(IrregularField irregularInField, Params params)
    {
        super(irregularInField, params);
        this.irregularInField = irregularInField;
    }

    private void writeBinary(IrregularField irregularInField, String genFileName, PrintWriter headerWriter, String sName)
    {
        try {
            File outFile = new File(genFileName + ".vnd");
            System.out.println("writing " + outFile.getName());
            headerWriter.println("file \"" + outFile.getName() + "\" binary");
            DataOutputStream outBinary = new DataOutputStream(new FileOutputStream(outFile));
            float[] timeSteps = irregularInField.getTimesteps();
            if (timeSteps.length == 1) {
                if (irregularInField.getCurrentMask() != null) {
                    headerWriter.println("mask");
                    LogicLargeArray mask = irregularInField.getCurrentMask();
                    for (int i = 0; i < mask.length(); i++)
                        outBinary.writeBoolean(mask.getBoolean(i));
                }
                headerWriter.println("coords");
                float[] coords = irregularInField.getCurrentCoords() == null ? null : irregularInField.getCurrentCoords().getData();
                for (int i = 0; i < coords.length; i++)
                    outBinary.writeFloat(coords[i]);
                for (int i = 0; i < irregularInField.getNComponents(); i++) {
                    DataArray da = irregularInField.getComponent(i);
                    if (!da.isNumeric()) {
                        continue;
                    }
                    headerWriter.println(da.getName().replaceAll("\\s", "_").replaceAll(",", "").replaceAll("\\.", "").replaceAll("=", "").replaceAll(":", ""));
                    switch (da.getType()) {
                        case FIELD_DATA_BYTE:
                            outBinary.write((byte[]) da.getRawArray().getData(), 0, ((byte[]) da.getRawArray().getData()).length);
                            break;
                        case FIELD_DATA_SHORT:
                            short[] sdata = (short[]) da.getRawArray().getData();
                            for (int j = 0; j < sdata.length; j++)
                                outBinary.writeShort(sdata[j]);
                            break;
                        case FIELD_DATA_INT:
                            int[] idata = (int[]) da.getRawArray().getData();
                            for (int j = 0; j < idata.length; j++)
                                outBinary.writeInt(idata[j]);
                            break;
                        case FIELD_DATA_FLOAT:
                            float[] fdata = (float[]) da.getRawArray().getData();
                            for (int j = 0; j < fdata.length; j++)
                                outBinary.writeFloat(fdata[j]);
                            break;
                        case FIELD_DATA_DOUBLE:
                            double[] ddata = (double[]) da.getRawArray().getData();
                            for (int j = 0; j < ddata.length; j++)
                                outBinary.writeDouble(ddata[j]);
                            break;
                    }

                }
            } else {
                for (int step = 0; step < timeSteps.length; step++) {
                    float t = timeSteps[step];
                    headerWriter.println("timestep " + t);
                    if (irregularInField.isMaskTimestep(t)) {
                        headerWriter.println("mask");
                        LogicLargeArray mask = irregularInField.getMask(t);
                        for (int i = 0; i < mask.length(); i++) {
                            outBinary.writeBoolean(mask.getBoolean(i));
                        }
                    }
                    if (irregularInField.isCoordTimestep(t)) {
                        headerWriter.println("coords");
                        float[] coords = irregularInField.getCoords(t).getData();
                        for (int i = 0; i < coords.length; i++)
                            outBinary.writeFloat(coords[i]);
                    }
                    for (int i = 0; i < irregularInField.getNComponents(); i++) {
                        DataArray da = irregularInField.getComponent(i);
                        if (!da.isNumeric() || !da.isTimestep(t)) {
                            continue;
                        }
                        switch (da.getType()) {
                            case FIELD_DATA_BYTE:
                                outBinary.write((byte[]) da.getRawArray(t).getData(), 0, ((short[]) da.getRawArray(t).getData()).length);
                                break;
                            case FIELD_DATA_SHORT:
                                short[] sdata = (short[]) da.getRawArray(t).getData();
                                for (int j = 0; j < sdata.length; j++)
                                    outBinary.writeShort(sdata[j]);
                                break;
                            case FIELD_DATA_INT:
                                int[] idata = (int[]) da.getRawArray(t).getData();
                                for (int j = 0; j < idata.length; j++)
                                    outBinary.writeInt(idata[j]);
                                break;
                            case FIELD_DATA_FLOAT:
                                float[] fdata = (float[]) da.getRawArray(t).getData();
                                for (int j = 0; j < fdata.length; j++)
                                    outBinary.writeFloat(fdata[j]);
                                break;
                            case FIELD_DATA_DOUBLE:
                                double[] ddata = (double[]) da.getRawArray(t).getData();
                                for (int j = 0; j < ddata.length; j++)
                                    outBinary.writeDouble(ddata[j]);
                                break;
                        }
                        headerWriter.println(da.getName().replaceAll("\\s", "_").replaceAll(",", "").replaceAll("\\.", "").replaceAll("=", "").replaceAll(":", ""));
                    }
                    headerWriter.println("end");
                }
            }
            for (CellSet cellSet : irregularInField.getCellSets()) {
                String setName = cellSet.getName().replaceAll("\\s", "_").replaceAll(",", "").replaceAll("\\.", "").replaceAll("=", "").replaceAll(":", "");
                System.out.println("writing description of " + setName);
                CellType[] cellTypes = Cell.getProperCellTypes();
                for (CellType cellType : cellTypes) {
                    if (cellSet.getCellArray(cellType) != null) {
                        CellArray cellArray = cellSet.getCellArray(cellType);
                        headerWriter.println(setName + ":" + cellType.getPluralName() + ":nodes");
                        int[] nodes = cellArray.getNodes();
                        for (int i = 0; i < nodes.length; i++)
                            outBinary.writeInt(nodes[i]);
                        if (cellArray.getDataIndices() != null) {
                            headerWriter.println(setName + ":" + cellType.getPluralName() + ":indices");
                            nodes = cellArray.getDataIndices();
                            for (int i = 0; i < nodes.length; i++)
                                outBinary.writeInt(nodes[i]);
                        }
                        if (cellArray.getOrientations() != null) {
                            byte[] b = cellArray.getOrientations();
                            headerWriter.println(setName + ":" + cellType.getPluralName() + ":orientations");
                            outBinary.write(b);
                        }
                    }
                }
                for (int i = 0; i < cellSet.getNComponents(); i++) {
                    DataArray da = cellSet.getComponent(i);
                    if (da.isNumeric()) {
                        headerWriter.println(setName + ":" + da.getName().replaceAll("\\s", "_").replaceAll(",", "").replaceAll("\\.", "").replaceAll("=", "").replaceAll(":", ""));
                        float[] fdata = da.getRawFloatArray().getData();
                        for (int j = 0; j < fdata.length; j++)
                            outBinary.writeFloat(fdata[j]);

                    }
                }
            }
            outBinary.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    int iCol = 0;

    private void prepareColumns(DataArray da,
                                LogicLargeArray[] boolArrs, byte[][] byteArrs,
                                short[][] shortArrs, int[][] intArrs,
                                float[][] floatArrs, double[][] dblArrs,
                                DataArrayType[] types, int[] vlens, int[] ind)
    {
        for (int j = 0; j < da.getVectorLength(); j++, iCol++) {
            types[iCol] = da.getType();
            ind[iCol] = j;
            vlens[iCol] = da.getVectorLength();
            switch (types[iCol]) {
                case FIELD_DATA_LOGIC:
                    boolArrs[iCol] = (LogicLargeArray) da.getRawArray();
                case FIELD_DATA_BYTE:
                    byteArrs[iCol] = (byte[]) da.getRawArray().getData();
                    break;
                case FIELD_DATA_SHORT:
                    shortArrs[iCol] = (short[]) da.getRawArray().getData();
                    break;
                case FIELD_DATA_INT:
                    intArrs[iCol] = (int[]) da.getRawArray().getData();
                    break;
                case FIELD_DATA_FLOAT:
                    floatArrs[iCol] = (float[]) da.getRawArray().getData();
                    break;
                case FIELD_DATA_DOUBLE:
                    dblArrs[iCol] = (double[]) da.getRawArray().getData();
                    break;
            }
        }

    }

    private void printColumns(PrintWriter outA, int nCols,
                              LogicLargeArray[] boolArrs, byte[][] byteArrs,
                              short[][] shortArrs, int[][] intArrs,
                              float[][] floatArrs, double[][] dblArrs,
                              DataArrayType[] types, int[] vlens, int[] ind)
    {
        for (int l = 0; l < nCols; l++) {
            switch (types[l]) {
                case FIELD_DATA_LOGIC:
                    outA.print(boolArrs[l].getBoolean(ind[l]) ? "  1 " : "  0 ");
                    ind[l] += vlens[l];
                    break;
                case FIELD_DATA_BYTE:
                    outA.printf(Locale.US, "%3d ", byteArrs[l][ind[l]] & 0xff);
                    ind[l] += vlens[l];
                    break;
                case FIELD_DATA_SHORT:
                    outA.printf(Locale.US, "%4d ", shortArrs[l][ind[l]]);
                    ind[l] += vlens[l];
                    break;
                case FIELD_DATA_INT:
                    outA.printf(Locale.US, "%6d ", intArrs[l][ind[l]]);
                    ind[l] += vlens[l];
                    break;
                case FIELD_DATA_FLOAT:
                    outA.printf(Locale.US, "%9.4f ", floatArrs[l][ind[l]]);
                    ind[l] += vlens[l];
                    break;
                case FIELD_DATA_DOUBLE:
                    outA.printf(Locale.US, "%13.6f ", dblArrs[l][ind[l]]);
                    ind[l] += vlens[l];
                    break;
            }
        }
        outA.println();
    }

    private void writeASCII(IrregularField irregularInField, String genFileName, PrintWriter headerWriter, String sName)
    {
        DataArray da;
        try {
            File outFile = new File(genFileName + ".txt");
            System.out.println("writing " + outFile.getName());
            headerWriter.println("file \"" + outFile.getName() + "\" ascii col");
            PrintWriter outA = new PrintWriter(new FileOutputStream(outFile));
            float[] timeSteps = irregularInField.getTimesteps();
            if (timeSteps.length == 1) {
                headerWriter.println("skip 1");
                headerWriter.print("coords, ");
                int nCols = 0;
                outA.printf("%" + (10 * irregularInField.getNSpace() - 2) + "s  ", "coordinates");
                nCols += irregularInField.getNSpace();
                if (irregularInField.getCurrentMask() != null)
                    headerWriter.print("mask, ");
                for (int i = 0; i < irregularInField.getNComponents(); i++) {
                    da = irregularInField.getComponent(i);
                    if (!da.isNumeric())
                        continue;
                    nCols += da.getVectorLength();
                    headerWriter.print(da.getName().replaceAll("\\s", "_").replaceAll(",", "").replaceAll("\\.", "").replaceAll("=", "").replaceAll(":", "") + ", ");
                    outA.print("" + da.getName() + "\t");
                }
                if (irregularInField.getCurrentMask() != null) {
                    outA.printf("mask");
                    nCols += 1;
                }
                outA.println();
                headerWriter.println();
                LogicLargeArray[] boolArrs = new LogicLargeArray[nCols];
                byte[][] byteArrs = new byte[nCols][];
                short[][] shortArrs = new short[nCols][];
                int[][] intArrs = new int[nCols][];
                float[][] floatArrs = new float[nCols][];
                double[][] dblArrs = new double[nCols][];
                DataArrayType[] types = new DataArrayType[nCols];
                int[] vlens = new int[nCols];
                int[] ind = new int[nCols];
                iCol = 0;

                if (irregularInField.getCurrentMask() != null) {
                    types[iCol] = DataArrayType.FIELD_DATA_LOGIC;
                    ind[iCol] = 0;
                    vlens[iCol] = 1;
                    iCol += 1;
                    boolArrs[iCol] = irregularInField.getCurrentMask();
                }
                for (int j = 0; j < irregularInField.getNSpace(); j++, iCol++) {
                    types[iCol] = DataArrayType.FIELD_DATA_FLOAT;
                    ind[iCol] = j;
                    vlens[iCol] = irregularInField.getNSpace();
                    floatArrs[iCol] = irregularInField.getCurrentCoords() == null ? null : irregularInField.getCurrentCoords().getData();
                }
                for (int i = 0; i < irregularInField.getNComponents(); i++) {
                    da = irregularInField.getComponent(i);
                    if (da.isNumeric())
                        prepareColumns(da,
                                       boolArrs, byteArrs, shortArrs, intArrs, floatArrs, dblArrs,
                                       types, vlens, ind);
                }
                for (int k = 0; k < irregularInField.getNNodes(); k++)
                    printColumns(outA, nCols,
                                 boolArrs, byteArrs, shortArrs, intArrs, floatArrs, dblArrs,
                                 types, vlens, ind);

            } else {
                for (int step = 0; step < timeSteps.length; step++) {
                    float t = timeSteps[step];
                    headerWriter.println("timestep " + t);
                    headerWriter.print("skip 1, ");
                    int nCols = 0;
                    if (irregularInField.isMaskTimestep(t)) {
                        headerWriter.print("mask, ");
                        outA.printf("mask");
                        nCols += 1;
                    }
                    if (irregularInField.isCoordTimestep(t)) {
                        headerWriter.print("coords, ");
                        outA.printf("%" + (10 * irregularInField.getNSpace() - 2) + "s  ", "coordinates");
                        nCols += irregularInField.getNSpace();
                    }
                    for (int i = 0, k = 0; i < irregularInField.getNComponents(); i++) {
                        da = irregularInField.getComponent(i);
                        if (!da.isNumeric() || !da.isTimestep(t))
                            continue;
                        nCols += da.getVectorLength();
                        if (k > 0)
                            headerWriter.print(", ");
                        headerWriter.print(da.getName().replaceAll("\\s", "_").
                            replaceAll(",", "").
                            replaceAll("\\.", "").
                            replaceAll("=", "").
                            replaceAll(":", ""));
                        outA.print("" + da.getName() + "\t");
                    }
                    outA.println();
                    headerWriter.println();
                    headerWriter.println("end");

                    LogicLargeArray[] boolArrs = new LogicLargeArray[nCols];
                    byte[][] byteArrs = new byte[nCols][];
                    short[][] shortArrs = new short[nCols][];
                    int[][] intArrs = new int[nCols][];
                    float[][] floatArrs = new float[nCols][];
                    double[][] dblArrs = new double[nCols][];
                    DataArrayType[] types = new DataArrayType[nCols];
                    int[] vlens = new int[nCols];
                    int[] ind = new int[nCols];
                    iCol = 0;

                    if (irregularInField.isMaskTimestep(t)) {
                        types[iCol] = DataArrayType.FIELD_DATA_LOGIC;
                        ind[iCol] = 0;
                        vlens[iCol] = 1;
                        iCol += 1;
                        boolArrs[iCol] = irregularInField.getCurrentMask();
                    }
                    if (irregularInField.isCoordTimestep(t))
                        for (int j = 0; j < irregularInField.getNSpace(); j++, iCol++) {
                            types[iCol] = DataArrayType.FIELD_DATA_FLOAT;
                            ind[iCol] = j;
                            vlens[iCol] = irregularInField.getNSpace();
                            floatArrs[iCol] = irregularInField.getCurrentCoords() == null ? null : irregularInField.getCurrentCoords().getData();
                        }
                    for (int i = 0; i < irregularInField.getNComponents(); i++) {
                        da = irregularInField.getComponent(i);
                        if (!da.isNumeric() || !da.isTimestep(t))
                            continue;
                        prepareColumns(da,
                                       boolArrs, byteArrs, shortArrs, intArrs, floatArrs, dblArrs,
                                       types, vlens, ind);
                    }
                    for (int k = 0; k < irregularInField.getNNodes(); k++)
                        printColumns(outA, nCols,
                                     boolArrs, byteArrs, shortArrs, intArrs, floatArrs, dblArrs,
                                     types, vlens, ind);
                }
            }
            for (CellSet cellSet : irregularInField.getCellSets()) {
                String setName = cellSet.getName().replaceAll("\\s", "_").
                    replaceAll(",", "").
                    replaceAll("\\.", "").
                    replaceAll("=", "").
                    replaceAll(":", "");
                headerWriter.println("skip 1");
                outA.println(setName);
                CellType[] cellTypes = Cell.getProperCellTypes();
                for (CellType cellType : cellTypes) {
                    if (cellSet.getCellArray(cellType) != null) {
                        headerWriter.println("skip 2");
                        CellArray cellArray = cellSet.getCellArray(cellType);
                        outA.println(cellType.getPluralName());
                        CellArray ca = cellSet.getCellArray(cellType);
                        int[] nodes = ca.getNodes();
                        int nn = cellType.getNVertices();
                        headerWriter.print(setName + ":" + cellType.getPluralName() + ":nodes, ");
                        outA.printf("%" + (11 * nn) + "s  ", "nodes     ");
                        if (cellArray.getDataIndices() != null) {
                            headerWriter.print(setName + ":" + cellType.getPluralName() + ":indices, ");
                            outA.print("   indices");
                        }
                        if (cellArray.getOrientations() != null) {
                            headerWriter.print(setName + ":" + cellType.getPluralName() + ":orientations, ");
                            outA.print(" orientations");
                        }
                        outA.println();
                        headerWriter.println();
                        for (int i = 0; i < ca.getNCells(); i++) {
                            for (int j = 0; j < nn; j++)
                                outA.printf("%10d ", nodes[nn * i + j]);
                            if (cellArray.getDataIndices() != null)
                                outA.printf("%10d ", cellArray.getDataIndices()[i]);
                            if (cellArray.getOrientations() != null)
                                outA.print(cellArray.getOrientations()[i] == 1 ? "         1" : "         0");
                            outA.println();
                        }
                    }
                }
                if (cellSet.getNComponents() != 0) {
                    headerWriter.println("skip 1");
                    int nCols = 0;
                    for (int i = 0, k = 0; i < cellSet.getNComponents(); i++) {
                        da = cellSet.getComponent(i);
                        if (da.isNumeric()) {
                            if (k > 0)
                                headerWriter.print(", ");
                            headerWriter.print(setName + ":" +
                                da.getName().replaceAll("\\s", "_").
                                replaceAll(",", "").
                                replaceAll("\\.", "").
                                replaceAll("=", "").
                                replaceAll(":", ""));
                            outA.print("" + da.getName() + "\t");
                            nCols += da.getVectorLength();
                            k += 1;
                        }
                    }
                    outA.println();
                    iCol = 0;
                    LogicLargeArray[] boolArrs = new LogicLargeArray[nCols];
                    byte[][] byteArrs = new byte[nCols][];
                    short[][] shortArrs = new short[nCols][];
                    int[][] intArrs = new int[nCols][];
                    float[][] floatArrs = new float[nCols][];
                    double[][] dblArrs = new double[nCols][];
                    DataArrayType[] types = new DataArrayType[nCols];
                    int[] vlens = new int[nCols];
                    int[] ind = new int[nCols];
                    for (int i = 0; i < cellSet.getNComponents(); i++) {
                        da = cellSet.getComponent(i);
                        if (da.isNumeric())
                            prepareColumns(da,
                                           boolArrs, byteArrs, shortArrs, intArrs, floatArrs, dblArrs,
                                           types, vlens, ind);
                    }

                    for (int j = 0; j < cellSet.getComponent(0).getNElements(); j++)
                        printColumns(outA, nCols,
                                     boolArrs, byteArrs, shortArrs, intArrs, floatArrs, dblArrs,
                                     types, vlens, ind);
                }
            }
            outA.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean writeField()
    {
        return writeField(irregularInField, params.getFileName(), params.isAscii(), params.isSingleFile());
    }

    private void writeDataArrayDescription(String prefix, int maxCmpNameLen, DataArray da)
    {
        headerWriter.printf(Locale.US, "component %" + (maxCmpNameLen + prefix.length()) + "s %7s",
                            prefix + da.getName().replaceAll("\\s", "_").replaceAll(",", "").replaceAll("\\.", "").replaceAll("=", "").replaceAll(":", ""),
                            dataTypes[da.getType().getValue()]);
        if (da.getVectorLength() > 1) {
            if (da.getMatrixDims()[0] != da.getVectorLength()) {
                headerWriter.print(", array " + da.getMatrixDims()[0]);
                if (da.isSymmetric())
                    headerWriter.print(", sym");
            } else
                headerWriter.print(", vector " + da.getVectorLength());
        }
        if (da.getUnit() != null && !da.getUnit().isEmpty())
            headerWriter.print(", unit " + da.getUnit());
        headerWriter.print(", preferred_min " + da.getPreferredMinValue());
        headerWriter.print(", preferred_max " + da.getPreferredMaxValue());
        headerWriter.print(", preferred_phys_min " + da.getPreferredPhysMinValue());
        headerWriter.print(", preferred_phys_max " + da.getPreferredPhysMaxValue());
        if (da.getUserData() != null) {
            headerWriter.print(", user:");
            String[] udata = da.getUserData();
            for (int j = 0; j < udata.length; j++) {
                if (j > 0)
                    headerWriter.print(";");
                headerWriter.print("\"" + udata[j] + "\"");
            }
        }
        headerWriter.println();
    }

    public boolean writeField(IrregularField irregularInField, String fileName, boolean ascii, boolean single)
    {
        if (irregularInField == null)
            return false;
        outFileName = fileName;
        genFileName = fileName;
        DataArray da;

        if (outFileName.endsWith(".vnf"))
            genFileName = outFileName.substring(0, outFileName.lastIndexOf(".vnf"));
        else
            outFileName = genFileName + ".vnf";
        File outFile = new File(outFileName);
        if (outFile.exists() && !outFile.canWrite()) {
            JOptionPane.showMessageDialog(null, "Cannot write file");
            return false;
        }
        if (new File(outFileName).exists()) {
            Object ans = JOptionPane.showConfirmDialog(null,
                                                       "File exists. Owerwrite?");
            if (ans instanceof Integer && (Integer) ans == JOptionPane.NO_OPTION)
                return false;
        }

        try {
            headerWriter = new PrintWriter(new FileOutputStream(outFile));
            headerWriter.println("#VisNow irregular field");
            if (irregularInField.getName() != null)
                headerWriter.print("field \"" + irregularInField.getName() + "\"");
            headerWriter.print(", nnodes = " + irregularInField.getNNodes());
            if (irregularInField.hasMask())
                headerWriter.print(", mask");
            if (irregularInField.getUserData() != null) {
                headerWriter.print(", user:");
                String[] udata = irregularInField.getUserData();
                for (int j = 0; j < udata.length; j++) {
                    if (j > 0)
                        headerWriter.print(";");
                    headerWriter.print("\"" + udata[j] + "\"");
                }
            }
            headerWriter.println();
            float[][] userExtents = irregularInField.getPhysExtents();
            if (inField.getCoordsUnits() != null && inField.getCoordsUnits().length == 3) {
                String[] cu = inField.getCoordsUnits();
                headerWriter.printf(Locale.US, "user extent x %10.4e %10.4e %s%n", userExtents[0][0], userExtents[1][0], cu[0]);
                headerWriter.printf(Locale.US, "user extent y %10.4e %10.4e %s%n", userExtents[0][1], userExtents[1][1], cu[1]);
                headerWriter.printf(Locale.US, "user extent z %10.4e %10.4e %s%n", userExtents[0][2], userExtents[1][2], cu[2]);
            } else {
                headerWriter.printf(Locale.US, "user extent x %10.4e %10.4e%n", userExtents[0][0], userExtents[1][0]);
                headerWriter.printf(Locale.US, "user extent y %10.4e %10.4e%n", userExtents[0][1], userExtents[1][1]);
                headerWriter.printf(Locale.US, "user extent z %10.4e %10.4e%n", userExtents[0][2], userExtents[1][2]);
            }
            int maxCmpNameLen = 0;
            for (int i = 0; i < irregularInField.getNComponents(); i++)
                if (irregularInField.getComponent(i).isNumeric() &&
                     irregularInField.getComponent(i).getName().length() > maxCmpNameLen)
                    maxCmpNameLen = irregularInField.getComponent(i).getName().length();
            for (int i = 0; i < irregularInField.getNComponents(); i++) {
                da = irregularInField.getComponent(i);
                if (da.isNumeric())
                    writeDataArrayDescription("", maxCmpNameLen, da);
            }
            for (CellSet cellSet : irregularInField.getCellSets()) {
                maxCmpNameLen = 0;
                int nData = 0;
                for (int i = 0; i < cellSet.getNComponents(); i++)
                    if (cellSet.getComponent(i).isNumeric()) {
                        nData += 1;
                        if (cellSet.getComponent(i).getName().length() > maxCmpNameLen)
                            maxCmpNameLen = cellSet.getComponent(i).getName().length();
                    }
                headerWriter.print("CellSet " + cellSet.getName().replaceAll("\\s", "_").replaceAll(",", "").replaceAll("\\.", "").replaceAll("=", "").replaceAll(":", ""));
                if (nData > 0)
                    headerWriter.println(", nData " + cellSet.getComponent(0).getNElements());
                else
                    headerWriter.println();
                String filler = "             ";
                CellType[] cellTypes = Cell.getProperCellTypes();
                for (CellType cellType : cellTypes) {
                    if (cellSet.getCellArray(cellType) != null) {
                        CellArray cellArray = cellSet.getCellArray(cellType);
                        if (cellArray.getNCells() > 1)
                            headerWriter.printf("%s%s%7d%n", cellType.getPluralName(),
                                                filler.substring(0, 12 - cellType.getPluralName().length()),
                                                cellArray.getNCells());
                        else
                            headerWriter.printf("%s%s%7d%n", cellType.getName(),
                                                filler.substring(0, 12 - cellType.getName().length()),
                                                cellArray.getNCells());
                    }
                }
                for (int i = 0; i < cellSet.getNComponents(); i++) {
                    da = cellSet.getComponent(i);
                    if (da.isNumeric())
                        writeDataArrayDescription("", maxCmpNameLen, da);
                }
            }
            String sName = outFile.getName();
            if (irregularInField.getNNodes() < 500 || ascii)
                writeASCII(irregularInField, genFileName, headerWriter, sName);
            else
                writeBinary(irregularInField, genFileName, headerWriter, sName);
            headerWriter.close();
            return true;
        } catch (FileNotFoundException e) {
            LOGGER.error("Error writing field", e);
            return false;
        }
    }

}
