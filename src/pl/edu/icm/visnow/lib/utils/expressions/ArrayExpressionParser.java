/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.expressions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static pl.edu.icm.visnow.lib.utils.expressions.ExpressionOperators.*;
import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jscic.utils.PhysicalConstants;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl), University of Warsaw, ICM based
 * on:
 * http://www.technical-recipes.com/2011/a-mathematical-expression-parser-in-java-and-cpp
 *
 */
public class ArrayExpressionParser
{

    private static final Logger LOGGER = Logger.getLogger(ArrayExpressionParser.class);

    private final boolean doublePrecision;
    private final boolean ignoreUnits;
    private final Map<String, DataArray> VARIABLES = new HashMap<>();
    private long dataSize;

    public ArrayExpressionParser(long dataSize, boolean doublePrecision, boolean ignoreUnits, Map<String, DataArray> variables)
    {
        this.dataSize = dataSize;
        this.doublePrecision = doublePrecision;
        this.ignoreUnits = ignoreUnits;
        ArrayList<String> vars = new ArrayList<>();
        vars.addAll(variables.keySet());
        VARIABLES.put("PI", DataArray.createConstant(doublePrecision ? DataArrayType.FIELD_DATA_DOUBLE : DataArrayType.FIELD_DATA_FLOAT, dataSize, PI));
        VARIABLES.put("E", DataArray.createConstant(doublePrecision ? DataArrayType.FIELD_DATA_DOUBLE : DataArrayType.FIELD_DATA_FLOAT, dataSize, E));
        VARIABLES.put("I", DataArray.createConstant(DataArrayType.FIELD_DATA_COMPLEX, dataSize, new float[] { 0f, 1f}));
        for (PhysicalConstants var : PhysicalConstants.values()) {
            VARIABLES.put(var.getSymbol(), DataArray.createConstant(doublePrecision ? DataArrayType.FIELD_DATA_DOUBLE : DataArrayType.FIELD_DATA_FLOAT, dataSize, var.getValue(), var.getSymbol(), var.getUnit(), null));
        }
        VARIABLES.putAll(variables);
    }

    public static String[] infixToRPN(String[] inputTokens)
    {
        ArrayList<String> out = new ArrayList<>();
        Stack<String> stack = new Stack<>();
        Operator op;
        for (String token : inputTokens) {
            if (isOperator(token)) {
                while (!stack.empty() && isOperator(stack.peek())) {
                    if ((isAssociative(token, LEFT_ASSOC) &&
                        cmpPrecedence(token, stack.peek()) <= 0) ||
                        (isAssociative(token, RIGHT_ASSOC) &&
                        cmpPrecedence(token, stack.peek()) < 0)) {
                        out.add(stack.pop());
                        continue;
                    }
                    break;
                }
                stack.push(token);
            } else if (token.equals("(")) {
                stack.push(token); //
            } else if (token.equals(")")) {
                while (!stack.empty() && !stack.peek().equals("(")) {
                    out.add(stack.pop());
                }
                stack.pop();
            } else {
                out.add(token);
            }
        }
        while (!stack.empty()) {
            out.add(stack.pop());
        }
        String[] output = new String[out.size()];
        return out.toArray(output);
    }

    public static boolean isNumeric(String token)
    {
        try {
            double d = Double.parseDouble(token);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public boolean isVariable(String token)
    {
        return VARIABLES.containsKey(token);
    }

    public DataArray RPNtoDataArray(String[] tokens)
    {

        Stack<Object> stack = new Stack<>();

        //TODO introduce chunks
        for (String token : tokens) {
            if (!isOperator(token)) {
                stack.push(token);
            } else {
                Operator op = getOperator(token);
                DataArray[] args = new DataArray[op.getNArgumetns()];
                Object tmp;
                String var;
                for (int i = 0; i < args.length; i++) {
                    tmp = stack.pop();
                    if (tmp instanceof String && isVariable((String) tmp)) {
                        var = (String) tmp;
                        args[i] = getVariable(var, true);
                        if (args[i] == null)
                            throw new IllegalArgumentException("ERROR evaluating expression");
                    } else if (tmp instanceof DataArray) {
                        args[i] = (DataArray) tmp;
                    } else if (tmp instanceof String && isNumeric((String) tmp)) {
                        args[i] = DataArray.createConstant(doublePrecision ? DataArrayType.FIELD_DATA_DOUBLE : DataArrayType.FIELD_DATA_FLOAT, dataSize, Double.valueOf((String) tmp));
                    } else {
                        throw new IllegalArgumentException("ERROR evaluating expression");
                    }
                }

                DataArray result = op.evaluate(dataSize, doublePrecision, ignoreUnits, args);
                stack.push(result);
            }
        }

        Object obj = stack.pop();
        DataArray out = null;
        if (!stack.isEmpty()) {
            throw new IllegalArgumentException("ERROR evaluating expression");
        }
        if (obj instanceof DataArray) {
            out = (DataArray) obj;
        }
        else if (obj instanceof String && isVariable((String) obj)) {
            out = getVariable((String) obj, true);

        } else if (obj instanceof String && isNumeric((String) obj)) {
            out = DataArray.createConstant(doublePrecision ? DataArrayType.FIELD_DATA_DOUBLE : DataArrayType.FIELD_DATA_FLOAT, dataSize, Double.valueOf((String) obj));
        }
        
        if (out instanceof DataArray) {
            if(ignoreUnits) {
                out.setUnit("1");
            }
            return out;
        }
        //this is error code - you should not be here if everything is OK                
        if (obj instanceof String) {
            throw new IllegalArgumentException("ERROR evaluating expression - wrong variable: " + ((String) obj));
        }

        throw new IllegalArgumentException("ERROR evaluating expression");
    }

    private DataArray getVariable(String name, boolean remap)
    {
        if (remap && (name.contains(".") || name.equals("x") || name.equals("y") || name.equals("z")) &&
            !(name.equals("index.i") || name.equals("index.j") || name.equals("index.k"))) {
            DataArray data;
            if (VARIABLES.containsKey(name)) {
                data = (DataArray) VARIABLES.get(name);
            } else {
                throw new IllegalArgumentException("ERROR: No such variable " + name);
            }
            int veclen = data.getVectorLength();
            if (veclen == 1)
                return data;
            int v = interpretVindex(name);
            if (v == -1)
                return null; //this should never happen
            return data.getSubcomponent(v);

        } else if (VARIABLES.containsKey(name))
            return (DataArray) VARIABLES.get(name);
        else
            throw new IllegalArgumentException("ERROR: No such variable " + name);
    }

    public DataArray evaluateExpr(String expr)
    {

        String str = preprocessExpr(expr);
        LOGGER.debug(str);
        String[] input = str.split(" ");
        String[] output = infixToRPN(input);

        for (String token : output) {
            LOGGER.debug(token + " ");
        }
        return RPNtoDataArray(output);
    }

    private String preprocessExpr(String input)
    {
        String str = input;
        str = str.replaceAll("\\s", "");
        //str = str.replaceAll(",", ".");
        LOGGER.debug(str);

        String strMask = "";
        for (int i = 0; i < str.length(); i++) {
            strMask += "0";
        }

        //case #1 - minus at the beginning
        if (str.startsWith("-")) {
            str = str.replaceFirst("-", "~");
        }

        //case #2 - minus after (
        str = str.replaceAll("\\(-", "\\(~");

        //case #3 - minus after operators
//        str = fixUnaryMinus(str, "\\,-");
//        str = fixUnaryMinus(str, "\\+-");
//        str = fixUnaryMinus(str, "--");
//        str = fixUnaryMinus(str, "\\*-");
//        str = fixUnaryMinus(str, "/-");
//        str = fixUnaryMinus(str, "\\^-");
//        str = fixUnaryMinus(str, "\\>-");
//        str = fixUnaryMinus(str, "\\<-");
//        str = fixUnaryMinus(str, "\\>=-");
//        str = fixUnaryMinus(str, "\\<=-");
//        str = fixUnaryMinus(str, "\\==-");
//        str = fixUnaryMinus(str, "\\!=-");
        str = str.replaceAll("\\,-", "\\,~");
        str = str.replaceAll("\\+-", "\\+~");
        str = str.replaceAll("--", "-~");
        str = str.replaceAll("\\*-", "\\*~");
        str = str.replaceAll("/-", "/~");
        str = str.replaceAll("\\^-", "\\^~");
        str = str.replaceAll("\\>-", "\\>~");
        str = str.replaceAll("\\<-", "\\<~");
        str = str.replaceAll("\\>=-", "\\>=~");
        str = str.replaceAll("\\<=-", "\\<=~");
        str = str.replaceAll("\\==-", "\\==~");
        str = str.replaceAll("\\!=-", "\\!=~");

        //case #4 - plus at the beginning
        if (str.startsWith("+")) {
            str = str.replaceFirst("+", "");
        }

        //case #5 - plus after (
        str = str.replaceAll("\\(+", "\\(");

        //case #6 - plus after operators
        str = str.replaceAll("\\,\\+", "\\,");
        str = str.replaceAll("\\+\\+", "\\+");
        str = str.replaceAll("-\\+", "-");
        str = str.replaceAll("\\*\\+", "\\*");
        str = str.replaceAll("/\\+", "/");
        str = str.replaceAll("\\^\\+", "\\^");
        str = str.replaceAll("\\>\\+", "\\>");
        str = str.replaceAll("\\<\\+", "\\<");
        str = str.replaceAll("\\>=\\+", "\\>=");
        str = str.replaceAll("\\<=\\+", "\\<=");
        str = str.replaceAll("\\==\\+", "\\==");
        str = str.replaceAll("\\!=\\+", "\\!=+");

        LOGGER.debug(str);

        ArrayList<String> ops = new ArrayList<>();
        Operator[] opsVals = Operator.values();
        for (int i = 0; i < opsVals.length; i++) {
            ops.add(opsVals[i].toString());
        }
        Collections.sort(ops, new StringLengthComparator());

        ArrayList<String> vars = new ArrayList<>();
        vars.addAll(VARIABLES.keySet());
        Collections.sort(vars, new StringLengthComparator());

        String tmp, tmpOp = "", tmpVar = "";
        int j;
        for (int i = 0; i < str.length(); i++) {
            tmp = str.substring(i);
            boolean op = false;
            for (j = 0; j < ops.size(); j++) {
                if (tmp.startsWith(ops.get(j))) {
                    op = true;
                    tmpOp = ops.get(j);
                    break;
                }
            }

            if (op) {
                str = str.substring(0, i) + " " + str.substring(i, str.length());
                strMask = strMask.substring(0, i) + "0";
                for (int k = 0; k < tmpOp.length(); k++) {
                    strMask += "1";
                }
                int tail = str.length() - strMask.length();
                for (int k = 0; k < tail; k++) {
                    strMask += "0";
                }
                i += ops.get(j).length();
            } else {
                boolean var = false;
                for (j = 0; j < vars.size(); j++) {
                    if (tmp.startsWith(vars.get(j))) {
                        var = true;
                        tmpVar = vars.get(j);
                        break;
                    }
                }

                if (var) {
                    str = str.substring(0, i) + " " + str.substring(i, str.length());
                    strMask = strMask.substring(0, i) + "0";
                    for (int k = 0; k < tmpVar.length(); k++) {
                        strMask += "1";
                    }
                    int tail = str.length() - strMask.length();
                    for (int k = 0; k < tail; k++) {
                        strMask += "0";
                    }
                    i += vars.get(j).length();
                }
            }
        }

        //str = str.replace("(", " (");
        Matcher m = Pattern.compile("\\(").matcher(str);
        int c = 0;
        while (m.find()) {
            str = str.substring(0, m.start() + c) + " " + str.substring(m.start() + c, str.length());
            strMask = strMask.substring(0, m.start() + c) + "0" + strMask.substring(m.start() + c, strMask.length());
            c++;
        }

        //str = str.replace(")", " )");
        m = Pattern.compile("\\)").matcher(str);
        c = 0;
        while (m.find()) {
            str = str.substring(0, m.start() + c) + " " + str.substring(m.start() + c, str.length());
            strMask = strMask.substring(0, m.start() + c) + "0" + strMask.substring(m.start() + c, strMask.length());
            c++;
        }

        //TODO: change regexp to support scientific notation of numbers e.g. 1e-10
        m = Pattern.compile("[\\.0-9]+").matcher(str);
        c = 0;
        while (m.find()) {
            if (strMask.charAt(m.start()) == '1') {
                continue;
            }

            str = str.substring(0, m.start() + c) + " " + str.substring(m.start() + c, str.length());
            c++;
        }

        str = str.trim();
        return str;
    }

    private int interpretVindex(String var)
    {
        if (var.contains(".")) {
            String subVarName = var.substring(var.indexOf(".") + 1, var.length());
            try {
                int out = Integer.parseInt(subVarName);
                return out;
            } catch (NumberFormatException ex) {
                return -1;
            }

        } else {
            if (var.equals("x"))
                return 0;
            if (var.equals("y"))
                return 1;
            if (var.equals("z"))
                return 2;
        }
        return -1;

    }

    private class StringLengthComparator implements java.util.Comparator<String>
    {

        public StringLengthComparator()
        {
            super();
        }

        @Override
        public int compare(String s1, String s2)
        {
            if (s1.length() == s2.length()) {
                return s1.compareTo(s2);
            }
            return s2.length() - s1.length();
        }
    }

    public static String[] listVariablesInUse(String[] vars, String[] expressions)
    {
        Map<String, DataArray> variables = new HashMap<>();
        for (int i = 0; i < vars.length; i++) {
            variables.put(vars[i], null);
        }
        ArrayExpressionParser parser = new ArrayExpressionParser(1, false, true, variables);

        ArrayList<String> out = new ArrayList<>();
        for (int n = 0; n < expressions.length; n++) {
            String expr = expressions[n];
            String str = parser.preprocessExpr(expr);
            String[] input = str.split(" ");
            Map<String, DataArray> tokens = new HashMap<>();
            for (int i = 0; i < input.length; i++) {
                tokens.put(input[i], null);
            }

            for (int i = 0; i < vars.length; i++) {
                if (tokens.containsKey(vars[i]))
                    out.add(vars[i]);
            }
        }

        String[] tmp = new String[out.size()];
        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = out.get(i);
        }
        return tmp;
    }

}
