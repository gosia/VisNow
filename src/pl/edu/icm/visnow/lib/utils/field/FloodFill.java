//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.field;

import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.visnow.lib.utils.FastIntQueue;

/**
 *
 * @author know
 */


public class FloodFill
{
    /**
     * creates boolean mask of the region defined by the condition data <gt> thr or data <lt> thr, depending of the param above
     * the mask is extended by a margin of width 1 in each direction to ensure the condition for proper flood fill
     * @param da  data array defining region
     * @param dims dimensions of the field containing the da data arrray
     * @param thr data threshold
     * @param above if above, region is defined by the condition data <gt> thr, else 
     * it is defined by the condition data <lt> thr
     * @return BitArray containing the created mask
     */
    public static boolean[] createExtendedRegion(DataArray da, int[] dims, float thr, boolean above)
    {
        int[] xDims = new int[dims.length];
        int rLen = 1;
        for (int i = 0; i < xDims.length; i++) {
            xDims[i] = dims[i] + 2;
            rLen *=xDims[i];
        }
        boolean[] region = new boolean[rLen];
        switch (dims.length) {
            case 3:
                switch (da.getType()) {
                    case FIELD_DATA_BYTE:
                        byte[] inBData = (byte[])da.getRawArray().getData();
                        for (int i = 0, l = 0; i < dims[2]; i++)
                            for (int j = 0; j < dims[1]; j++)
                                for (int k = 0,
                                        m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                                        k < dims[0]; k++, l++, m++)
                                    region[m] = ((0xff & inBData[l]) > thr) == above;
                        break;
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                        LargeArray inDData = da.getRawArray();
                        for (int i = 0, l = 0; i < dims[2]; i++)
                            for (int j = 0; j < dims[1]; j++)
                                for (int k = 0,
                                        m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                                        k < dims[0]; k++, l++, m++)
                                    region[m] = (inDData.getDouble(l) > thr) == above;
                        break;
                }
                break;
            case 2:
                switch (da.getType()) {
                    case FIELD_DATA_BYTE:
                        byte[] inBData = (byte[])da.getRawArray().getData();
                        for (int j = 0, l = 0; j < dims[1]; j++)
                            for (int k = 0,
                                    m = (j + 1) * xDims[0] + 1;
                                    k < dims[0]; k++, l++, m++)
                                region[m] = ((0xff & inBData[l]) > thr) == above;
                        break;
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                        LargeArray inDData = da.getRawArray();
                        for (int j = 0, l = 0; j < dims[1]; j++)
                            for (int k = 0,
                                    m = (j + 1) * xDims[0] + 1;
                                    k < dims[0]; k++, l++, m++)
                                region[m] = (inDData.getDouble(l) > thr) == above;
                        break;
                }
                break;
            case 1:
                switch (da.getType()) {
                    case FIELD_DATA_BYTE:
                        byte[] inBData = (byte[])da.getRawArray().getData();
                        for (int k = 0; k < dims[0]; k++)
                            region[k + 1] = ((0xff & inBData[k]) > thr) == above;
                        break;
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                        LargeArray inDData = da.getRawArray();
                        for (int k = 0; k < dims[0]; k++)
                            region[k + 1] = (inDData.getDouble(k) > thr) == above;
                        break;
                }
                break;
        }
        return region;
    }

    /**
     * creates boolean mask of the region defined by the condition data <gt> thr or data <lt> thr, depending of the param above
     * the mask is extended by a margin of width 1 in each direction to ensure the condition for proper flood fill
     * @param da  data array defining region
     * @param dims dimensions of the field containing the da data arrray
     * @param low lower data threshold
     * @param up upper data threshold: the region is defined by the condition low <lt> data <lt> up
     * @return BitArray containing the created mask
     */
    public static boolean[] createExtendedRegion(DataArray da, int[] dims, float low, float up)
    {
        int[] xDims = new int[dims.length];
        int rLen = 1;
        for (int i = 0; i < xDims.length; i++) {
            xDims[i] = dims[i] + 2;
            rLen *=xDims[i];
        }
        boolean[] region = new boolean[rLen];
        switch (dims.length) {
            case 3:
                switch (da.getType()) {
                    case FIELD_DATA_BYTE:
                        byte[] inBData = (byte[])da.getRawArray().getData();
                        for (int i = 0, l = 0; i < dims[2]; i++)
                            for (int j = 0; j < dims[1]; j++)
                                for (int k = 0,
                                        m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                                        k < dims[0]; k++, l++, m++)
                                    region[m] = ((0xff & inBData[l]) > low) && ((0xff & inBData[l]) < up);
                        break;
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                        LargeArray inDData = da.getRawArray();
                        for (int i = 0, l = 0; i < dims[2]; i++)
                            for (int j = 0; j < dims[1]; j++)
                                for (int k = 0,
                                        m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                                        k < dims[0]; k++, l++, m++)
                                    region[m] = (inDData.getDouble(l) > low) && (inDData.getDouble(l)< up);
                        break;
                }
                break;
            case 2:
                switch (da.getType()) {
                    case FIELD_DATA_BYTE:
                        byte[] inBData = (byte[])da.getRawArray().getData();
                        for (int j = 0, l = 0; j < dims[1]; j++)
                            for (int k = 0,
                                    m = (j + 1) * xDims[0] + 1;
                                    k < dims[0]; k++, l++, m++)
                                region[m] = ((0xff & inBData[l]) > low) && ((0xff & inBData[l]) < up);
                        break;
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                        LargeArray inDData = da.getRawArray();
                        for (int j = 0, l = 0; j < dims[1]; j++)
                            for (int k = 0,
                                    m = (j + 1) * xDims[0] + 1;
                                    k < dims[0]; k++, l++, m++)
                                region[m] = (inDData.getDouble(l) > low) && (inDData.getDouble(l) < up);
                        break;
                }
                break;
            case 1:
                switch (da.getType()) {
                    case FIELD_DATA_BYTE:
                        byte[] inBData = (byte[])da.getRawArray().getData();
                        for (int k = 0; k < dims[0]; k++)
                            region[k + 1] = ((0xff & inBData[k]) > low) && ((0xff & inBData[k]) < up);
                        break;
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                        LargeArray inDData = da.getRawArray();
                        for (int k = 0; k < dims[0]; k++)
                            region[k + 1] = (inDData.getDouble(k) > low) && (inDData.getDouble(k) < up);
                        break;
                }
                break;
        }
        return region;
    }

    public static void maskRegion(LogicLargeArray mask, int[] dims, boolean[] region)
    {
        int[] xDims = new int[dims.length];
        for (int i = 0; i < xDims.length; i++)
            xDims[i] = dims[i] + 2;
        switch (dims.length) {
            case 3:
                for (int i = 0, l = 0; i < dims[2]; i++)
                    for (int j = 0; j < dims[1]; j++)
                        for (int k = 0,
                                m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                                k < dims[0]; k++, l++, m++)
                            region[m] = region[m] && mask.getBoolean(l);
                break;
            case 2:
                for (int j = 0, l = 0; j < dims[1]; j++)
                    for (int k = 0,
                            m = (j + 1) * xDims[0] + 1;
                            k < dims[0]; k++, l++, m++)
                        region[m] = region[m] && mask.getBoolean(l);
                        
                break;
            case 1:
                for (int k = 1; k <= dims[0]; k++)
                    region[k] = region[k] && mask.getBoolean(k - 1);
        }

    }
    
    public static void fill(int fillVal, boolean[] region, int[] start, boolean recomputeStart, 
                            int[] dims, int[] neighbors, int[] out)
    {
        fill(fillVal, region, start, recomputeStart, dims, neighbors, out, null, null);
    }
    
    public static void fill(int fillVal, boolean[] region, int[] start, boolean recomputeStart, 
                            int[] dims, int[] neighbors, int[] out, 
                            int[][] counts)
    {
        fill(fillVal, region, start, recomputeStart, dims, neighbors, out, counts, null);
    }

    private FloodFill() {}
    
    /**
     * fills a connected component of a 1D, 2D or 3D area with the value fillVal using flood fill algorithm
     * @param fillVal value >= 0 that will be set to the elements of the out array in filled component
     * @param region boolean array with the region to be processed filled by <code>true</code> margins of the width 1 
     * along the boundaries of the array must be filled by <code>false</code>, otherwise unpredictable behavior can occur
     * @param start array of indices of the initial flood fill points 
     * @param recomputeStart 
     * @param dims dimensions of the original box
     * @param neighbors array of offsets from a given point in <code>region</code> to its neighbors
     * @param out modifiable array, all points in the component found will be set to fillVal. 
     * It has to be initialized by -1 before calling a sequence of fill method calls
     * @param elongations
     */
    public static void fill(int fillVal, boolean[] region, int[] start, boolean recomputeStart, 
                            int[] dims, int[] neighbors, int[] out, 
                            int[][] counts, float[][] elongations)
    {
        boolean isCount = counts != null && counts.length >= 1 && counts[0] != null;
        boolean isElongation = elongations != null && elongations.length >= 1 && elongations[0] != null;
        FastIntQueue queue = new FastIntQueue();
        int[] s = start;
        if (recomputeStart)
        {
            s = new int[start.length];
            for (int i = 0; i < start.length; i++) {
                int k = start[i];
                int l = k / dims[0];
                switch (dims.length) {
                case 1:
                    s[i] = k + 1;
                    break;
                case 2:
                    s[i] = (dims[0] + 2) * (l + 1) + k % dims[0] + 1;
                    break;
                case 3:
                    int m = l / dims[1];
                    s[i] = ((dims[1] + 2) * (m + 1) + l % dims[1] + 1) * (dims[0] + 2) + k % dims[0] + 1;
                }
            }
        }
        int elongation = 0, count = 1;
        int step = 0;
        int layerStep = 1;
        queue.init(s);
        while (!queue.isEmpty())
        {
            if (step == layerStep && isElongation) {
                elongation += 1;
                layerStep = queue.setStepMark();
                step = 0;
            }
            int k = queue.get();
            step += 1;
            for (int i = 0; i < neighbors.length; i++) {
                int j = k + neighbors[i];
                if (region[j] && out[j] < 0){
                    out[j] = fillVal;
                    queue.insert(j);
                    count += 1;
                }
            }
        }
        if (isCount) {
            if (fillVal == counts[0].length) {
                int[] tCounts = new int[2 * counts[0].length];
                System.arraycopy(counts[0], 0, tCounts, 0, counts.length);
                counts[0] = tCounts;
            }
            counts[0][fillVal] = count;
        }
        if (isElongation) {
            if (fillVal == elongations[0].length) {
                float[] tElongs = new float[2 * elongations[0].length];
                System.arraycopy(elongations[0], 0, tElongs, 0, elongations[0].length);
                elongations[0] = tElongs;
            }
            float el = (float) elongation;
            elongations[0][fillVal] = el * (float) Math.sqrt(el / count);
        }
    }
    
    public static int[] restoreResultDimensions(int[] dims, int[] xOut)
    {
        int nOut = 1;
        int[] xDims = new int[dims.length];
        for (int i = 0; i < xDims.length; i++) {
            xDims[i] = dims[i] + 2;
            nOut *= dims[i];
        }
        int[] out = new int[nOut];
        switch (dims.length) {
            case 3:
                for (int i = 0, l = 0; i < dims[2]; i++)
                    for (int j = 0; j < dims[1]; j++)
                        for (int k = 0,
                                m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                                k < dims[0]; k++, l++, m++) {
                            out[l] = xOut[m];
                        }
                break;
            case 2:
                for (int j = 0, l = 0; j < dims[1]; j++)
                    for (int k = 0,
                            m = (j + 1) * xDims[0] + 1;
                            k < dims[0]; k++, l++, m++) {
                        out[l] = xOut[m];
                    }
                break;
            case 1:
                for (int k = 0; k < dims[0]; k++) {
                    out[k] = xOut[k + 1];
                }
                break;
        }
        return out;
    }
}
