/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.numeric.fieldAveraging;

import pl.edu.icm.visnow.lib.utils.numeric.splines.SplineValueGradientDP;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class SplineFieldAveraging3DDP implements FieldAveragingDP
{

    protected int[] ctrlDims;
    protected int[] avgDims;
    protected int density;
    //protected short[] mask = null;
    protected int[][] tDims;
    protected double[][] targets;
    protected double[][] coeffs = null;
    protected double[][] dCoeffs = null;
    protected double[][] d2Coeffs = null;
    protected int nThreads = 2;
    protected double deformationWeight = 10;
    protected double volumeWeight = .1f;
    protected int nCtrls, nCtrlsTotal;
    protected int nAvgs;
    protected double matchEnergy, deformationEnergy, volumeEnergy;
    protected boolean gdebug = false;
    protected long totalMillis = 0;
    protected int evaluations = 0;
    protected double shortNorm = 1.0 / Short.MAX_VALUE;
    protected double[] vol0 = null;
    protected int[][][] mBoxOff = new int[][][]{{{0, 2, 4, 6}, {1, 3, 5, 7}},
                                                {{0, 1, 4, 5}, {2, 3, 6, 7}},
                                                {{0, 1, 2, 3}, {4, 5, 6, 7}}};
    protected int[][][] boxOff = new int[3][2][4];
    protected double[] spatialWeights = null;
    protected int N = 0;
    protected double[] avgData;
    protected double[] stddevData;

    /**
     * @param ctrlDims - dimensions of control points grid - ctrlDims.length must be 3
     * @param source   - scalar source data array
     * @param tDims    - dimensions of target grid - tDims.length must be 3
     * @param tArr     - target scalar data array
     * @param
     */
    public SplineFieldAveraging3DDP(int[] ctrlDims, int[] avgDims,
                                    int[][] tDims, double[][] targets,
                                    int nThreads,
                                    float deformationWeight, float volumeWeight)
    {

        if (ctrlDims == null ||
            avgDims == null || avgDims.length != 3 ||
            tDims == null || targets == null ||
            ctrlDims == null || ctrlDims.length != 3)
            return;
        if (ceil((avgDims[0] - 1.) / (ctrlDims[0] - 1.)) != ceil((avgDims[1] - 1.) / (ctrlDims[1] - 1.)) ||
            ceil((avgDims[2] - 1.) / (ctrlDims[2] - 1.)) != ceil((avgDims[1] - 1.) / (ctrlDims[1] - 1.)))
            return;

        N = targets.length;

        for (int n = 0; n < N; n++)
            if (tDims[n] == null || tDims[n].length != 3 || targets[n] == null || targets[n].length != tDims[n][0] * tDims[n][1] * tDims[n][2])
                return;

        vol0 = new double[N];
        for (int i = 0; i < N; i++)
            vol0[i] = 1;
        this.ctrlDims = ctrlDims;
        density = (int) (Math.ceil((avgDims[0] - 1.) / (ctrlDims[0] - 1.)));
        nCtrls = ctrlDims[0] * ctrlDims[1] * ctrlDims[2];
        nCtrlsTotal = N * nCtrls;
        this.avgDims = avgDims;
        nAvgs = avgDims[0] * avgDims[1] * avgDims[2];
        avgData = new double[nAvgs];
        stddevData = new double[nAvgs];

        this.tDims = tDims;
        this.targets = targets;
        coeffs = new double[4][density + 1];
        dCoeffs = new double[4][density + 1];
        d2Coeffs = new double[4][density + 1];
        for (int j = 0; j <= density; j++) {
            double x = (double) j / density;
            coeffs[3][j] = (.5 * x - .5) * x * x;
            coeffs[2][j] = ((-1.5 * x + 2.) * x + .5) * x;
            coeffs[1][j] = (1.5 * x - 2.5) * x * x + 1.;
            coeffs[0][j] = ((-.5 * x + 1.0) * x - .5) * x;
            dCoeffs[3][j] = (1.5 * x - 1.0) * x;
            dCoeffs[2][j] = (-4.5 * x + 4.0) * x + .5;
            dCoeffs[1][j] = (4.5 * x - 5.0) * x;
            dCoeffs[0][j] = (-1.5 * x + 2.0) * x - .5;
            d2Coeffs[3][j] = 3.0 * x - 1.0;
            d2Coeffs[2][j] = -9.0 * x + 4.0;
            d2Coeffs[1][j] = 9.0 * x - 5.0;
            d2Coeffs[0][j] = -3.0 * x + 2.0;
        }

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 2; j++)
                for (int k = 0; k < 4; k++) {
                    boxOff[i][j][k] = 0;
                    if ((mBoxOff[i][j][k] & 1) != 0)
                        boxOff[i][j][k] += 3;
                    if ((mBoxOff[i][j][k] & 2) != 0)
                        boxOff[i][j][k] += 3 * ctrlDims[0];
                    if ((mBoxOff[i][j][k] & 4) != 0)
                        boxOff[i][j][k] += 3 * ctrlDims[0] * ctrlDims[1];
                }

        this.nThreads = nThreads;
        this.deformationWeight = deformationWeight;
        this.volumeWeight = volumeWeight;
    }

    public SplineFieldAveraging3DDP(int[] ctrlDims,
                                    int[] avgDims,
                                    int[][] tDims, double[][] targets,
                                    int nThreads,
                                    float deformationWeight, float volumeWeight, double[] vol0)
    {
        this(ctrlDims, avgDims, tDims, targets, nThreads, deformationWeight, volumeWeight);
        this.vol0 = vol0;
    }

    private double computeMatchValGrad(double[] point, double[] gradient)
    {
        double out = 0;
        for (int i = 0; i < gradient.length; i++)
            gradient[i] = 0;

        ComputeValGrad[] computeThreads = new ComputeValGrad[nThreads];
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            computeThreads[iThread] = new ComputeValGrad(point, nThreads, iThread);
            workThreads[iThread] = new Thread(computeThreads[iThread]);
            workThreads[iThread].start();
        }
        for (int iThread = 0; iThread < workThreads.length; iThread++)
            try {
                workThreads[iThread].join();
            } catch (Exception e) {
            }
        for (int iThread = 0; iThread < nThreads; iThread++) {
            out += computeThreads[iThread].getMatchValue();
            double[] thrGr = computeThreads[iThread].getMatchGradient();
            for (int i = 0; i < thrGr.length; i++)
                gradient[i] += thrGr[i];
        }
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] /= nAvgs;
        }
        out /= nAvgs;

        return out;
    }

    private class ComputeValGrad implements Runnable
    {

        private double[] ctrlCoords;
        private double[] matchGradient;
        private int iThread = 0;
        private double matchValue;

        ComputeValGrad(double[] ctrlCoords, int nThreads, int iThread)
        {
            this.ctrlCoords = ctrlCoords;
            this.iThread = iThread;
            matchGradient = new double[ctrlCoords.length];
        }

        public double[] getMatchGradient()
        {
            return matchGradient;
        }

        public double getMatchValue()
        {
            return matchValue;
        }

        public void run()
        {
            double[] pcoord = new double[3];
            double[] splineVals = new double[N];
            double meanVal = 0;
            double[][] splineGrads = new double[N][3];
            double[][][] jacobians = new double[N][3][3];

            matchValue = 0;
            for (int i = 0; i < ctrlCoords.length; i++)
                matchGradient[i] = 0;

            int di = (ctrlDims[2] - 1) / nThreads;
            int istart = iThread * di + Math.min(iThread, (ctrlDims[2] - 1) % nThreads);
            int iend = (iThread + 1) * di + Math.min(iThread + 1, (ctrlDims[2] - 1) % nThreads);
            for (int i = istart; i < iend; i++)
                for (int j = 0; j < ctrlDims[1] - 1; j++)
                    for (int k = 0; k < ctrlDims[0] - 1; k++) {
                        int px = density;
                        if (i == ctrlDims[2] - 2)
                            px = density + 1;
                        int qx = density;
                        if (j == ctrlDims[1] - 2)
                            qx = density + 1;
                        int rx = density;
                        if (k == ctrlDims[0] - 2)
                            rx = density + 1;
                        for (int p = 0; p < px; p++)
                            for (int q = 0; q < qx; q++)
                                for (int r = 0, l = ((density * i + p) * avgDims[1] +
                                    density * j + q) * avgDims[0] +
                                    density * k; r < rx; r++, l++) {
                                    meanVal = 0;
                                    for (int n = 0, nOff = 0; n < N; n++, nOff += 3 * nCtrls) {
                                        for (int m = 0; m < pcoord.length; m++) {
                                            pcoord[m] = 0;
                                            for (int im = 0; im < 3; im++)
                                                for (int jm = 0; jm < 3; jm++)
                                                    jacobians[n][im][jm] = 0;
                                        }
                                        for (int ii = 0; ii < 4; ii++) {
                                            int ix = i + ii - 1;
                                            if (ix < 0)
                                                ix = 0;
                                            if (ix >= ctrlDims[2])
                                                ix = ctrlDims[2] - 1;
                                            for (int jj = 0; jj < 4; jj++) {
                                                int jx = j + jj - 1;
                                                if (jx < 0)
                                                    jx = 0;
                                                if (jx >= ctrlDims[1])
                                                    jx = ctrlDims[1] - 1;
                                                int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                                for (int kk = 0; kk < 4; kk++)
                                                    for (int v = 0; v < 3; v++) {
                                                        int kx = k + kk - 1;
                                                        if (kx < 0)
                                                            kx = 0;
                                                        if (kx >= ctrlDims[0])
                                                            kx = ctrlDims[0] - 1;
                                                        pcoord[v] += ctrlCoords[nOff + 3 * (isrc + kx) + v] *
                                                            coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                                        jacobians[n][0][v] += ctrlCoords[nOff + 3 * (isrc + kx) + v] *
                                                            dCoeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                                        jacobians[n][1][v] += ctrlCoords[nOff + 3 * (isrc + kx) + v] *
                                                            coeffs[ii][p] * dCoeffs[jj][q] * coeffs[kk][r];
                                                        jacobians[n][2][v] += ctrlCoords[nOff + 3 * (isrc + kx) + v] *
                                                            coeffs[ii][p] * coeffs[jj][q] * dCoeffs[kk][r];
                                                    }
                                            }
                                        }
                                        splineVals[n] = SplineValueGradientDP.getSplineValueGradient(targets[n], tDims[n], pcoord, splineGrads[n]);
                                        meanVal += splineVals[n];
                                    }
                                    meanVal /= (double) N;
                                    avgData[l] = meanVal;
                                    double nrg = 0;
                                    for (int n = 0, nOff = 0; n < N; n++, nOff += 3 * nCtrls) {
                                        nrg += (splineVals[n] - meanVal) * (splineVals[n] - meanVal) / (double) N;
                                        for (int ii = 0; ii < 4; ii++) {
                                            int ix = min(max(i + ii - 1, 0), ctrlDims[2] - 1);
                                            for (int jj = 0; jj < 4; jj++) {
                                                int jx = min(max(j + jj - 1, 0), ctrlDims[1] - 1);
                                                int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                                for (int kk = 0; kk < 4; kk++)
                                                    for (int v = 0; v < 3; v++) {
                                                        int kx = min(max(k + kk - 1, 0), ctrlDims[0] - 1);
                                                        double c = coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                                        matchGradient[nOff + 3 * (isrc + kx) + v] += c * (splineVals[n] - meanVal) * splineGrads[n][v] / (double) N;
                                                    }
                                            }
                                        }
                                    }
                                    stddevData[l] = nrg;
                                    matchValue += nrg;

                                }
                    }
        }
    }

    /**
     * computes energy (sum over source points of (t(F(x)-s(x))^2)) and energy gradient with respect to control points
     * <p>
     * @param ctrlCoords - target coordinates for control points in target index space
     *                   (ctrlCoords.length = 3*ctrlDims[0]*ctrlDims[1]*ctrlDims[2]) - independent variables
     * @param gradient   - array for storing gradient - must be preallocated of length ctrlCoords.length.
     * <p>
     * @return energy value (if negative - wrong input data)
     */
    private double computeDeformationValGrad(double[] point, double[] gradient)
    {
        if (deformationWeight == 0)
            return 0;
        double[] u = new double[3];
        double[] v = new double[3];
        double[] w = new double[3];
        int s0 = 3 * ctrlDims[0];
        int s1 = 3 * ctrlDims[0] * ctrlDims[1];
        double sw;
        double dw = deformationWeight / (double) nCtrls;
        double[] smoothEnergy = new double[N];

        for (int n = 0, cnOff = 0; n < N; n++, cnOff += 3 * nCtrls) {
            smoothEnergy[n] = 0;
            // d2v/dx2
            for (int i = 0; i < ctrlDims[2]; i++)
                for (int j = 0; j < ctrlDims[1]; j++)
                    for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0] - 1; k++, l += 3) {
                        double wnorm = 0;
                        for (int m = 0; m < 3; m++) {
                            u[m] = point[cnOff + l + m] - point[cnOff + l - 3 + m];
                            v[m] = point[cnOff + l + 3 + m] - point[cnOff + l + m];
                            w[m] = v[m] - u[m];
                            wnorm += w[m] * w[m];
                        }
                        if (spatialWeights == null) {
                            smoothEnergy[n] += wnorm;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l + 3 + m] += 2 * w[m] * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * dw;
                                gradient[cnOff + l - 3 + m] += 2 * w[m] * dw;
                            }
                        } else {
                            sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                            smoothEnergy[n] += wnorm * sw;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l + 3 + m] += 2 * w[m] * sw * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * sw * dw;
                                gradient[cnOff + l - 3 + m] += 2 * w[m] * sw * dw;
                            }
                        }
                    }
            // d2v/dxdy
            for (int i = 0; i < ctrlDims[2]; i++)
                for (int j = 1; j < ctrlDims[1]; j++)
                    for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0]; k++, l += 3) {
                        double wnorm = 0;
                        for (int m = 0; m < 3; m++) {
                            u[m] = point[cnOff + l + m] - point[cnOff + l - 3 + m];
                            v[m] = point[cnOff + l - s0 + m] - point[cnOff + l - s0 - 3 + m];
                            w[m] = v[m] - u[m];
                            wnorm += w[m] * w[m];
                        }

                        if (spatialWeights == null) {
                            smoothEnergy[n] += 2 * wnorm;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l - s0 + m] += 4 * w[m] * dw;
                                gradient[cnOff + l - s0 - 3 + m] -= 4 * w[m] * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * dw;
                                gradient[cnOff + l - 3 + m] += 4 * w[m] * dw;
                            }
                        } else {
                            sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                            smoothEnergy[n] += 2 * wnorm * sw;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l - s0 + m] += 4 * w[m] * sw * dw;
                                gradient[cnOff + l - s0 - 3 + m] -= 4 * w[m] * sw * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * sw * dw;
                                gradient[cnOff + l - 3 + m] += 4 * w[m] * sw * dw;
                            }
                        }
                    }
            // d2v/dxdz
            for (int i = 1; i < ctrlDims[2]; i++)
                for (int j = 0; j < ctrlDims[1]; j++)
                    for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0]; k++, l += 3) {
                        double wnorm = 0;
                        for (int m = 0; m < 3; m++) {
                            u[m] = point[cnOff + l + m] - point[cnOff + l - 3 + m];
                            v[m] = point[cnOff + l - s1 + m] - point[cnOff + l - s1 - 3 + m];
                            w[m] = v[m] - u[m];
                            wnorm += w[m] * w[m];
                        }

                        if (spatialWeights == null) {
                            smoothEnergy[n] += 2 * wnorm;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l - s1 + m] += 4 * w[m] * dw;
                                gradient[cnOff + l - s1 - 3 + m] -= 4 * w[m] * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * dw;
                                gradient[cnOff + l - 3 + m] += 4 * w[m] * dw;
                            }
                        } else {
                            sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                            smoothEnergy[n] += 2 * wnorm * sw;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l - s1 + m] += 4 * w[m] * sw * dw;
                                gradient[cnOff + l - s1 - 3 + m] -= 4 * w[m] * sw * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * sw * dw;
                                gradient[cnOff + l - 3 + m] += 4 * w[m] * sw * dw;
                            }
                        }
                    }
            // d2v/dy2
            for (int i = 0; i < ctrlDims[2]; i++)
                for (int j = 0; j < ctrlDims[0]; j++)
                    for (int k = 1, l = ((i * ctrlDims[1] + 1) * ctrlDims[0] + j) * 3; k < ctrlDims[1] - 1; k++, l += s0) {
                        double wnorm = 0;
                        for (int m = 0; m < 3; m++) {
                            u[m] = point[cnOff + l + m] - point[cnOff + l - s0 + m];
                            v[m] = point[cnOff + l + s0 + m] - point[cnOff + l + m];
                            w[m] = v[m] - u[m];
                            wnorm += w[m] * w[m];
                        }
                        if (spatialWeights == null) {
                            smoothEnergy[n] += wnorm;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l + s0 + m] += 2 * w[m] * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * dw;
                                gradient[cnOff + l - s0 + m] += 2 * w[m] * dw;
                            }
                        } else {
                            sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + k * ctrlDims[0] + j];
                            smoothEnergy[n] += wnorm * sw;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l + s0 + m] += 2 * w[m] * sw * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * sw * dw;
                                gradient[cnOff + l - s0 + m] += 2 * w[m] * sw * dw;
                            }
                        }
                    }
            // d2v/dydz
            for (int i = 1; i < ctrlDims[2]; i++)
                for (int j = 0; j < ctrlDims[0]; j++)
                    for (int k = 1, l = ((i * ctrlDims[1] + 1) * ctrlDims[0] + j) * 3; k < ctrlDims[1]; k++, l += s0) {
                        double wnorm = 0;
                        for (int m = 0; m < 3; m++) {
                            u[m] = point[cnOff + l + m] - point[cnOff + l - s1 + m];
                            v[m] = point[cnOff + l - 3 + m] - point[cnOff + l - s1 - 3 + m];
                            w[m] = v[m] - u[m];
                            wnorm += w[m] * w[m];
                        }
                        if (spatialWeights == null) {
                            smoothEnergy[n] += 2 * wnorm;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l - 3 + m] += 4 * w[m] * dw;
                                gradient[cnOff + l - s1 - 3 + m] -= 4 * w[m] * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * dw;
                                gradient[cnOff + l - s1 + m] += 4 * w[m] * dw;
                            }
                        } else {
                            sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + k * ctrlDims[0] + j];
                            smoothEnergy[n] += 2 * wnorm * sw;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l - 3 + m] += 4 * w[m] * sw * dw;
                                gradient[cnOff + l - s1 - 3 + m] -= 4 * w[m] * sw * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * sw * dw;
                                gradient[cnOff + l - s1 + m] += 4 * w[m] * sw * dw;
                            }
                        }
                    }

            // d2v/dz2
            for (int i = 0; i < ctrlDims[1]; i++)
                for (int j = 0; j < ctrlDims[0]; j++)
                    for (int k = 1, l = ((i + ctrlDims[1]) * ctrlDims[0] + j) * 3; k < ctrlDims[2] - 1; k++, l += s1) {
                        double wnorm = 0;
                        for (int m = 0; m < 3; m++) {
                            u[m] = point[cnOff + l + m] - point[cnOff + l - s1 + m];
                            v[m] = point[cnOff + l + s1 + m] - point[cnOff + l + m];
                            w[m] = v[m] - u[m];
                            wnorm += w[m] * w[m];
                        }
                        if (spatialWeights == null) {
                            smoothEnergy[n] += wnorm;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l + s1 + m] += 2 * w[m] * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * dw;
                                gradient[cnOff + l - s1 + m] += 2 * w[m] * dw;
                            }
                        } else {
                            sw = spatialWeights[k * ctrlDims[0] * ctrlDims[1] + i * ctrlDims[0] + j];
                            smoothEnergy[n] += wnorm * sw;
                            for (int m = 0; m < 3; m++) {
                                gradient[cnOff + l + s1 + m] += 2 * w[m] * sw * dw;
                                gradient[cnOff + l + m] -= 4 * w[m] * sw * dw;
                                gradient[cnOff + l - s1 + m] += 2 * w[m] * sw * dw;
                            }
                        }
                    }
            smoothEnergy[n] *= dw;
        }

        double outSmoothEnergy = 0;
        for (int n = 0; n < N; n++)
            outSmoothEnergy += smoothEnergy[n];

        return outSmoothEnergy;
    }

    public double computeValGrad(double[] point, double[] gradient)
    {
        long t0 = System.currentTimeMillis();

        matchEnergy = computeMatchValGrad(point, gradient);
        deformationEnergy = computeDeformationValGrad(point, gradient);
        volumeEnergy = computeVolumeDeformationValGrad(point, gradient);

        evaluations += 1;
        totalMillis += System.currentTimeMillis() - t0;
        System.out.printf("  matchEnergy = %8.3f   deformationEnergy = %8.3f    volumeEnergy = %8.3f     total energy = %8.3f%n",
                          matchEnergy, deformationEnergy, volumeEnergy, matchEnergy + deformationEnergy + volumeEnergy);

        return matchEnergy + deformationEnergy + volumeEnergy;
    }

    private double computeVolumeDeformationValGrad(double[] point, double[] gradient)
    {
        if (volumeWeight == 0)
            return 0;
        double[] en = new double[N];
        double[] v0 = new double[3], v1 = new double[3], v2 = new double[3];
        double[] w0 = new double[3], w1 = new double[3], w2 = new double[3];

        for (int n = 0, nOff = 0; n < N; n++, nOff += 3 * nCtrls) {
            en[n] = 0;
            for (int i = 0; i < ctrlDims[2] - 1; i++)
                for (int j = 0; j < ctrlDims[1] - 1; j++)
                    for (int k = 0; k < ctrlDims[0] - 1; k++) {
                        int k0 = nOff + 3 * ((i * ctrlDims[1] + j) * ctrlDims[0] + k);
                        for (int l = 0; l < 3; l++) {
                            v0[l] = v1[l] = v2[l] = 0;
                            for (int m = 0; m < 4; m++) {
                                v0[l] += point[k0 + boxOff[0][1][m] + l] - point[k0 + boxOff[0][0][m] + l];
                                v1[l] += point[k0 + boxOff[1][1][m] + l] - point[k0 + boxOff[1][0][m] + l];
                                v2[l] += point[k0 + boxOff[2][1][m] + l] - point[k0 + boxOff[2][0][m] + l];
                            }
                            v0[l] *= .25f;
                            v1[l] *= .25f;
                            v2[l] *= .25f;
                        }
                        w0[0] = v1[1] * v2[2] - v1[2] * v2[1];
                        w0[1] = v1[2] * v2[0] - v1[0] * v2[2];
                        w0[2] = v1[0] * v2[1] - v1[1] * v2[0];
                        w1[0] = v2[1] * v0[2] - v2[2] * v0[1];
                        w1[1] = v2[2] * v0[0] - v2[0] * v0[2];
                        w1[2] = v2[0] * v0[1] - v2[1] * v0[0];
                        w2[0] = v0[1] * v1[2] - v0[2] * v1[1];
                        w2[1] = v0[2] * v1[0] - v0[0] * v1[2];
                        w2[2] = v0[0] * v1[1] - v0[1] * v1[0];
                        double v = v0[0] * w0[0] + v0[1] * w0[1] + v0[2] * w0[2];
                        en[n] += (v - vol0[n]) * (v - vol0[n]);
                        double dif = .5f * volumeWeight * (v - vol0[n]) / nCtrls;
                        for (int l = 0; l < 3; l++)
                            for (int m = 0; m < 4; m++) {
                                gradient[k0 + boxOff[0][1][m] + l] += dif * w0[l];
                                gradient[k0 + boxOff[0][0][m] + l] -= dif * w0[l];
                                gradient[k0 + boxOff[1][1][m] + l] += dif * w1[l];
                                gradient[k0 + boxOff[1][0][m] + l] -= dif * w1[l];
                                gradient[k0 + boxOff[2][1][m] + l] += dif * w2[l];
                                gradient[k0 + boxOff[2][0][m] + l] -= dif * w2[l];
                            }
                    }
            en[n] /= nCtrls;
        }

        double outEn = 0;
        for (int n = 0; n < N; n++)
            outEn += en[n];
        return volumeWeight * outEn;
    }

    public double[] getFullDisplacedCoords(double[] ctrlCoords)
    {
        int nData = avgDims[0] * avgDims[1] * avgDims[2];
        double[] dispCoords = new double[N * 3 * nData];
        for (int i = 0; i < dispCoords.length; i++)
            dispCoords[i] = 0;
        double[] pcoord = new double[3];

        for (int n = 0, nOff = 0, nOff2 = 0; n < N; n++, nOff += 3 * nCtrls, nOff2 += 3 * nData)
            for (int i = 0; i < ctrlDims[2] - 1; i++)
                for (int j = 0; j < ctrlDims[1] - 1; j++)
                    for (int k = 0; k < ctrlDims[0] - 1; k++) {
                        int px = density;
                        if (i == ctrlDims[2] - 2)
                            px = density + 1;
                        int qx = density;
                        if (j == ctrlDims[1] - 2)
                            qx = density + 1;
                        int rx = density;
                        if (k == ctrlDims[0] - 2)
                            rx = density + 1;
                        for (int p = 0; p < px; p++)
                            for (int q = 0; q < qx; q++)
                                for (int r = 0, l = ((density * i + p) * avgDims[1] + density * j + q) * avgDims[0] + density * k; r < rx; r++, l++) {
                                    for (int m = 0; m < pcoord.length; m++)
                                        pcoord[m] = 0;
                                    for (int ii = 0; ii < 4; ii++) {
                                        int ix = i + ii - 1;
                                        if (ix < 0)
                                            ix = 0;
                                        if (ix >= ctrlDims[2])
                                            ix = ctrlDims[2] - 1;
                                        for (int jj = 0; jj < 4; jj++) {
                                            int jx = j + jj - 1;
                                            if (jx < 0)
                                                jx = 0;
                                            if (jx >= ctrlDims[1])
                                                jx = ctrlDims[1] - 1;
                                            int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                            for (int kk = 0; kk < 4; kk++)
                                                for (int v = 0; v < 3; v++) {
                                                    int kx = k + kk - 1;
                                                    if (kx < 0)
                                                        kx = 0;
                                                    if (kx >= ctrlDims[0])
                                                        kx = ctrlDims[0] - 1;
                                                    pcoord[v] += ctrlCoords[nOff + 3 * (isrc + kx) + v] * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                                }
                                        }
                                    }
                                    System.arraycopy(pcoord, 0, dispCoords, nOff2 + 3 * l, pcoord.length);
                                }
                    }
        return dispCoords;
    }

    public double[] getDisplacedCoords(int n, double[] ctrlCoords, int scale)
    {
        int nData = avgDims[0] * avgDims[1] * avgDims[2];
        double[] dispCoords = new double[3 * nData];
        for (int i = 0; i < dispCoords.length; i++)
            dispCoords[i] = 0;
        int nOff = 3 * n * nCtrls;
        double[] pcoord = new double[3];
        for (int i = 0; i < ctrlDims[2] - 1; i++)
            for (int j = 0; j < ctrlDims[1] - 1; j++)
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int px = density;
                    if (i == ctrlDims[2] - 2)
                        px = density + 1;
                    int qx = density;
                    if (j == ctrlDims[1] - 2)
                        qx = density + 1;
                    int rx = density;
                    if (k == ctrlDims[0] - 2)
                        rx = density + 1;
                    for (int p = 0; p < px; p++)
                        for (int q = 0; q < qx; q++)
                            for (int r = 0, l = ((density * i + p) * avgDims[1] + density * j + q) * avgDims[0] + density * k; r < rx; r++, l++) {
                                for (int m = 0; m < pcoord.length; m++)
                                    pcoord[m] = 0;
                                for (int ii = 0; ii < 4; ii++) {
                                    int ix = i + ii - 1;
                                    if (ix < 0)
                                        ix = 0;
                                    if (ix >= ctrlDims[2])
                                        ix = ctrlDims[2] - 1;
                                    for (int jj = 0; jj < 4; jj++) {
                                        int jx = j + jj - 1;
                                        if (jx < 0)
                                            jx = 0;
                                        if (jx >= ctrlDims[1])
                                            jx = ctrlDims[1] - 1;
                                        int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                        for (int kk = 0; kk < 4; kk++)
                                            for (int v = 0; v < 3; v++) {
                                                int kx = k + kk - 1;
                                                if (kx < 0)
                                                    kx = 0;
                                                if (kx >= ctrlDims[0])
                                                    kx = ctrlDims[0] - 1;
                                                pcoord[v] += ctrlCoords[nOff + 3 * (isrc + kx) + v] * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                            }
                                    }
                                }
                                System.arraycopy(pcoord, 0, dispCoords, 3 * l, pcoord.length);
                            }
                }
        return dispCoords;
    }

    public double[] getAverageData()
    {
        return avgData;
    }

    public double[] getEnergyData()
    {
        return stddevData;
    }

    public double[] getTargetData(int n)
    {
        return targets[n];
    }

    public int[] getTargetDataDims(int n)
    {
        return tDims[n];
    }

    public void setDeformationWeight(double deformationWeight)
    {
        this.deformationWeight = deformationWeight;
    }

    public void setVolumeWeight(double volumeWeight)
    {
        this.volumeWeight = volumeWeight;
    }

    public void setNThreads(int nThreads)
    {
        this.nThreads = nThreads;
    }

    public double getDeformationEnergy()
    {
        return deformationEnergy;
    }

    public double getVolumeEnergy()
    {
        return volumeEnergy;
    }

    public double getMatchEnergy()
    {
        return matchEnergy;
    }

    public void setGdebug(boolean gdebug)
    {
        this.gdebug = gdebug;
    }

    public int getEvaluations()
    {
        return evaluations;
    }

    public long getTotalMillis()
    {
        return totalMillis;
    }

    public void resetTimer()
    {
        totalMillis = 0;
        evaluations = 0;
    }

    public void setSpatialWeights(double[] weights)
    {
        if (weights == null || weights.length != ctrlDims[0] * ctrlDims[1] * ctrlDims[2]) {
            this.spatialWeights = null;
            return;
        }

        this.spatialWeights = weights;
    }

    public String[] getVarNames()
    {
        return new String[]{"matchEnergy", "deformationEnergy", "volumeEnergy"};
    }

    public double[] getVariables()
    {
        return new double[]{matchEnergy, deformationEnergy, volumeEnergy};
    }

}
