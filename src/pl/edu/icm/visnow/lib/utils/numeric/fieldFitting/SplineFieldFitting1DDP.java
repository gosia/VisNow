/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.numeric.fieldFitting;

import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.lib.utils.numeric.splines.SplineValueGradientDP;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class SplineFieldFitting1DDP implements FieldFittingDP
{

    protected int[] ctrlDims;
    protected int[] srcDims;
    protected int density;
    protected double[] source;
    protected short[] mask = null;
    protected int[] tDims;
    protected double[] target;
    protected double[][] coeffs = null;
    protected double deformationWeight = 10;
    protected double volumeWeight = .1f;
    protected double cnfWeight = .1f;
    protected double isoWeight = .1f;
    protected int nCtrls, nSrcs;
    protected double matchEnergy, deformationEnergy, volumeEnergy;
    protected boolean gdebug = false;
    protected long totalMillis = 0;
    protected int evaluations = 0;
    protected double shortNorm = 1.f / Short.MAX_VALUE;
    protected double vol0 = 1;
    protected int d0 = 0, d1 = 0;
    protected float[] spatialWeights = null;

    /**
     * @param ctrlDims - dimensions of control points grid - ctrlDims.length must be 3
     * @param source   - scalar source data array
     * @param tDims    - dimensions of target grid - tDims.length must be 3
     * @param tArr     - target scalar data array
     * @param
     */
    public SplineFieldFitting1DDP(int[] ctrlDims,
                                  int[] srcDims, double[] source,
                                  int[] tDims, double[] target,
                                  DataArray mask, 
                                  double deformationWeight, double volumeWeight)
    {
        if (ctrlDims == null || ctrlDims.length != 1) {
            return;
        }
        if (srcDims == null || srcDims.length != 1 ||
            source == null || source.length != srcDims[0]) {
            return;
        }
        if (tDims == null || tDims.length != 1 ||
            target == null || target.length != tDims[0]) {
            return;
        }
        density = (int) (Math.ceil((srcDims[0] - 1.) / (ctrlDims[0] - 1.)));
        this.ctrlDims = ctrlDims;
        nCtrls = ctrlDims[0];

        this.srcDims = srcDims;
        nSrcs = srcDims[0];
        this.source = source;
        if (mask == null) {
            this.mask = new short[nSrcs];
            for (int i = 0; i < nSrcs; i++) {
                this.mask[i] = Short.MAX_VALUE;
            }
        } else {
            this.mask = mask.getRawShortArray().getData();
        }
        this.tDims = tDims;
        this.target = target;
        d0 = tDims[0];
        coeffs = new double[4][density + 1];
        for (int j = 0; j <= density; j++) {
            double x = (double) j / density;
            coeffs[3][j] = (.5 * x - .5) * x * x;
            coeffs[2][j] = ((-1.5 * x + 2.) * x + .5) * x;
            coeffs[1][j] = (1.5 * x - 2.5) * x * x + 1.;
            coeffs[0][j] = ((-.5 * x + 1.0) * x - .5) * x;
        }

        this.deformationWeight = deformationWeight;
        this.volumeWeight = volumeWeight;
    }

    public SplineFieldFitting1DDP(int[] ctrlDims,
                                  int[] srcDims, double[] source,
                                  int[] tDims, double[] target,
                                  DataArray mask,
                                  double deformationWeight, double volumeWeight, double vol0)
    {
        this(ctrlDims, srcDims, source, tDims, target, mask, deformationWeight, volumeWeight);
        this.vol0 = vol0;
    }

    public String[] getVarNames()
    {
        return new String[]{"matchEnergy", "deformationEnergy", "volumeEnergy"};
    }

    public double[] getVariables()
    {
        return new double[]{matchEnergy, deformationEnergy, volumeEnergy};
    }

    private class ComputeValGrad implements Runnable
    {

        private double[] ctrlCoords;
        private double[] matchGradient;
        private int iThread = 0;
        private double matchValue;

        ComputeValGrad(double[] ctrlCoords, int nThreads, int iThread)
        {
            this.ctrlCoords = ctrlCoords;
            this.iThread = iThread;
            matchGradient = new double[ctrlCoords.length];
        }

        public double[] getMatchGradient()
        {
            return matchGradient;
        }

        public double getMatchValue()
        {
            return matchValue;
        }

        public void run()
        {
            double pcoord;

            matchValue = 0;
            for (int i = 0; i < ctrlCoords.length; i++) {
                matchGradient[i] = 0;
            }

            for (int k = 0; k < ctrlDims[0] - 1; k++) {
                int rx = density;
                if (k == ctrlDims[0] - 2) {
                    rx = density + 1;
                }
                for (int r = 0, l = density * k; r < rx; r++, l++) {
                    if(l >= mask.length)
                        continue;
                    
                    if (mask[l] == 0) {
                        continue;
                    }
                    pcoord = 0;
                    for (int kk = 0; kk < 4; kk++) {
                        int kx = k + kk - 1;
                        if (kx < 0) {
                            kx = 0;
                        }
                        if (kx >= ctrlDims[0]) {
                            kx = ctrlDims[0] - 1;
                        }
                        pcoord += ctrlCoords[kx] * coeffs[kk][r];
                    }
                    
                    double maskCoeff = mask[l] * shortNorm;
                    double[] splineGradient = new double[1];
                    double splineVal = SplineValueGradientDP.getSplineValueGradient1D(target, tDims, pcoord, splineGradient);
                    double sV = source[l];
                    matchValue += (splineVal - sV) * (splineVal - sV);
                    for (int kk = 0; kk < 4; kk++) {
                        int kx = min(max(k + kk - 1, 0), ctrlDims[0] - 1);
                        double c = maskCoeff * coeffs[kk][r];
                        matchGradient[kx] += c * (splineVal - sV) * splineGradient[0];
                    }
                }
            }
        }
    }

    /**
     * computes energy (sum over source points of (t(F(x)-s(x))^2)) and energy gradient with respect to control points
     * <p>
     * @param ctrlCoords - target coordinates for control points in target index space
     *                   (ctrlCoords.length = ctrlDims[0]) - independent variables
     * @param gradient   - array for storing gradient - must be preallocated of length ctrlCoords.length.
     * <p>
     * @return energy value (if negative - wrong input data)
     */
    private double computeDeformationValGrad(double[] point, double[] gradient)
    {
        if (deformationWeight == 0) {
            return 0;
        }
        double u;
        double v;
        double w;
        double dw = deformationWeight / (double) nCtrls;
        double sw;
        double smoothEnergy = 0;

        // d2v/dx2
        for (int k = 1, l = 1; k < ctrlDims[0] - 1; k++, l ++) {
            u = point[l] - point[l - 1];
            v = point[l + 1] - point[l];
            w = v - u;
            double wnorm = w * w;
            if (spatialWeights == null) {
                smoothEnergy += wnorm;
                gradient[l + 1] += 2 * w * dw;
                gradient[l] -= 4 * w * dw;
                gradient[l - 1] += 2 * w * dw;
            } else {
                sw = spatialWeights[k];
                smoothEnergy += wnorm * sw;
                gradient[l + 1] += 2 * w * sw * dw;
                gradient[l] -= 4 * w * sw * dw;
                gradient[l - 1] += 2 * w * sw * dw;
            }
        }

        return smoothEnergy * dw;
    }

    private double computeVolumeDeformationValGrad(double[] point, double[] gradient)
    {
        //TODO
        return 0;
    }

    public double computeValGrad(double[] point, double[] gradient)
    {
        long t0 = System.currentTimeMillis();
        matchEnergy = 0;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = 0;
        }

        ComputeValGrad[] computeThreads = new ComputeValGrad[1];
        Thread[] workThreads = new Thread[1];
        for (int iThread = 0; iThread < 1; iThread++) {
            computeThreads[iThread] = new ComputeValGrad(point, 1, iThread);
            workThreads[iThread] = new Thread(computeThreads[iThread]);
            workThreads[iThread].start();
        }
        for (int iThread = 0; iThread < workThreads.length; iThread++) {
            try {
                workThreads[iThread].join();
            } catch (Exception e) {
            }
        }
        for (int iThread = 0; iThread < 1; iThread++) {
            matchEnergy += computeThreads[iThread].getMatchValue();
            double[] thrGr = computeThreads[iThread].getMatchGradient();
            for (int i = 0; i < thrGr.length; i++) {
                gradient[i] += thrGr[i];
            }
        }
        matchEnergy /= nSrcs;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] /= nSrcs;
        }

        deformationEnergy = computeDeformationValGrad(point, gradient);
        volumeEnergy = computeVolumeDeformationValGrad(point, gradient);

        evaluations += 1;
        totalMillis += System.currentTimeMillis() - t0;
        //      System.out.printf("  matchEnergy = %8.3f   deformationEnergy = %8.3f    volumeEnergy = %8.3f     total energy = %8.3f%n",
        //                         matchEnergy, deformationEnergy, volumeEnergy, matchEnergy + deformationEnergy + volumeEnergy);
        return matchEnergy + deformationEnergy + volumeEnergy;
    }

    public double[] getDisplacedCoords(double[] ctrlCoords)
    {
        double[] dispCoords = new double[source.length];
        for (int i = 0; i < dispCoords.length; i++) {
            dispCoords[i] = 0;
        }
        double pcoord;
        for (int k = 0; k < ctrlDims[0] - 1; k++) {
            int rx = density;
            if (k == ctrlDims[0] - 2) {
                rx = density + 1;
            }
            for (int r = 0, l = density * k; r < rx; r++, l++) {
                if(l >= dispCoords.length)
                    break;
                
                pcoord = 0;
                for (int kk = 0; kk < 4; kk++) {
                    int kx = k + kk - 1;
                    if (kx < 0) {
                        kx = 0;
                    }
                    if (kx >= ctrlDims[0]) {
                        kx = ctrlDims[0] - 1;
                    }
                    pcoord += ctrlCoords[kx] * coeffs[kk][r];
                }
                dispCoords[l] = pcoord;
            }
        }
        return dispCoords;
    }

    public byte[] getTransformedTarget(double[] ctrlCoords)
    {
        byte[] transTar = new byte[source.length];
        double pcoord;
        double[] tGradient = new double[1];
        for (int i = 0; i < transTar.length; i++) {
            pcoord = ctrlCoords[i];
            double tV = SplineValueGradientDP.getSplineValueGradient1D(target, tDims, pcoord, tGradient);
            transTar[i] = (byte) ((int) tV & 0xff);
        }
        return transTar;
    }

    public void setDeformationWeight(double deformationWeight)
    {
        this.deformationWeight = deformationWeight;
    }

    public void setVolumeWeight(double volumeWeight)
    {
        this.volumeWeight = volumeWeight;
    }

    public void setNThreads(int nThreads)
    {
    }

    public double getDeformationEnergy()
    {
        return deformationEnergy;
    }

    public double getVolumeEnergy()
    {
        return volumeEnergy;
    }

    public double getMatchEnergy()
    {
        return matchEnergy;
    }

    public void setGdebug(boolean gdebug)
    {
        this.gdebug = gdebug;
    }

    public int getEvaluations()
    {
        return evaluations;
    }

    public long getTotalMillis()
    {
        return totalMillis;
    }

    public void resetTimer()
    {
        totalMillis = 0;
        evaluations = 0;
    }

    public void setSpatialWeights(float[] weights)
    {
        if (weights == null || weights.length != ctrlDims[0] * ctrlDims[1]) {
            this.spatialWeights = null;
            return;
        }

        this.spatialWeights = weights;
    }
}
