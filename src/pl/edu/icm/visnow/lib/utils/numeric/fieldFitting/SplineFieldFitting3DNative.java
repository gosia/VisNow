/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.numeric.fieldFitting;

import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.lib.utils.numeric.splines.SplineValueGradient;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class SplineFieldFitting3DNative implements FieldFitting
{

    protected int[] ctrlDims;
    protected int[] srcDims;
    protected int density;
    protected float[] source;
    protected short[] mask = null;
    protected int[] tDims;
    protected DataArray tArr;
    protected float[][] coeffs = null;
    protected int nThreads = 2;
    protected float deformationWeight = 10;
    protected float volumeWeight = .1f;
    protected float cnfWeight = .1f;
    protected float isoWeight = .1f;
    protected int nCtrls, nSrcs;
    protected float[] matchGradient;
    protected float[] deformationGradient;
    protected double matchEnergy, deformationEnergy, volumeEnergy;
    protected boolean gdebug = false;
    protected long totalMillis = 0;
    protected int evaluations = 0;
    protected float shortNorm = 1.f / Short.MAX_VALUE;
    protected double vol0 = 1;
    protected int[][][] mBoxOff = new int[][][]{{{0, 2, 4, 6}, {1, 3, 5, 7}}, {{0, 1, 4, 5}, {2, 3, 6, 7}}, {{0, 1, 2, 3}, {4, 5, 6, 7}}};
    protected int[][][] boxOff = new int[3][2][4];
    protected int d0 = 0, d1 = 0, d2 = 0;
    protected float[] spatialWeights = null;

    /**
     * @param ctrlDims - dimensions of control points grid - ctrlDims.length must be 3
     * @param source   - scalar source data array
     * @param tDims    - dimensions of target grid - tDims.length must be 3
     * @param tArr     - target scalar data array
     * @param
     */
    public SplineFieldFitting3DNative(int[] ctrlDims,
                                      int[] srcDims, float[] source,
                                      int[] tDims, DataArray tArr,
                                      DataArray mask, int nThreads,
                                      float deformationWeight, float volumeWeight)
    {
        if (ctrlDims == null || ctrlDims.length != 3) {
            return;
        }
        if (source == null || source.length != srcDims[0] * srcDims[1] * srcDims[2]) {
            return;
        }
        if (tDims == null || tDims.length != 3 ||
            tArr == null || !tArr.isNumeric() || tArr.getNElements() != tDims[0] * tDims[1] * tDims[2] || tArr.getVectorLength() != 1) {
            return;
        }
        if (ceil((srcDims[0] - 1.) / (ctrlDims[0] - 1.)) != ceil((srcDims[1] - 1.) / (ctrlDims[1] - 1.)) ||
            ceil((srcDims[2] - 1.) / (ctrlDims[2] - 1.)) != ceil((srcDims[1] - 1.) / (ctrlDims[1] - 1.))) {
            return;
        }
        density = (int) (Math.ceil((srcDims[0] - 1.) / (ctrlDims[0] - 1.)));
        this.ctrlDims = ctrlDims;
        nCtrls = ctrlDims[0] * ctrlDims[1] * ctrlDims[2];

        this.srcDims = srcDims;
        nSrcs = srcDims[0] * srcDims[1] * srcDims[2];
        this.source = source;
        if (mask == null) {
            this.mask = new short[nSrcs];
            for (int i = 0; i < nSrcs; i++) {
                this.mask[i] = Short.MAX_VALUE;
            }
        } else {
            this.mask = mask.getRawShortArray().getData();
        }
        this.tDims = tDims;
        this.tArr = tArr;
        d0 = tDims[0];
        d1 = tDims[1];
        d2 = tDims[2];
        coeffs = new float[4][density + 1];
        for (int j = 0; j <= density; j++) {
            float x = (float) j / density;
            coeffs[3][j] = (.5f * x - .5f) * x * x;
            coeffs[2][j] = ((-1.5f * x + 2.f) * x + .5f) * x;
            coeffs[1][j] = (1.5f * x - 2.5f) * x * x + 1.f;
            coeffs[0][j] = ((-.5f * x + 1.0f) * x - .5f) * x;
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 4; k++) {
                    boxOff[i][j][k] = 0;
                    if ((mBoxOff[i][j][k] & 1) != 0) {
                        boxOff[i][j][k] += 3;
                    }
                    if ((mBoxOff[i][j][k] & 2) != 0) {
                        boxOff[i][j][k] += 3 * ctrlDims[0];
                    }
                    if ((mBoxOff[i][j][k] & 4) != 0) {
                        boxOff[i][j][k] += 3 * ctrlDims[0] * ctrlDims[1];
                    }
                }
            }
        }

        int gradSize = 3 * ctrlDims[0] * ctrlDims[1] * ctrlDims[2];
        matchGradient = new float[gradSize];
        deformationGradient = new float[gradSize];
        this.nThreads = nThreads;
        this.deformationWeight = deformationWeight;
        this.volumeWeight = volumeWeight;

        String libPath = "/home/staff/babor/java/projects/VNNativeFieldFitting/dist/libVNNativeFieldFitting.so";
        try {
            System.load(libPath);
        } catch (UnsatisfiedLinkError err) {
            System.out.println("ERROR");
            err.printStackTrace();
            System.exit(1);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        System.out.println("native library loaded");

        System.out.println("using native FF");
    }

    public SplineFieldFitting3DNative(int[] ctrlDims,
                                      int[] srcDims, float[] source,
                                      int[] tDims, DataArray tArr,
                                      DataArray mask, int nThreads,
                                      float deformationWeight, float volumeWeight, double vol0)
    {
        this(ctrlDims, srcDims, source, tDims, tArr, mask, nThreads, deformationWeight, volumeWeight);
        this.vol0 = vol0;
    }

    public String[] getVarNames()
    {
        return new String[]{"matchEnergy", "deformationEnergy", "volumeEnergy"};
    }

    public double[] getVariables()
    {
        return new double[]{matchEnergy, deformationEnergy, volumeEnergy};
    }

    public void setDeformationWeight(float deformationWeight)
    {
        this.deformationWeight = deformationWeight;
    }

    public void setVolumeWeight(float volumeWeight)
    {
        this.volumeWeight = volumeWeight;
    }

    public void setNThreads(int nThreads)
    {
        this.nThreads = nThreads;
    }

    public double getDeformationEnergy()
    {
        return deformationEnergy;
    }

    public float[] getDeformationGradient()
    {
        return deformationGradient;
    }

    public double getVolumeEnergy()
    {
        return volumeEnergy;
    }

    public double getMatchEnergy()
    {
        return matchEnergy;
    }

    public float[] getMatchGradient()
    {
        return matchGradient;
    }

    public void setGdebug(boolean gdebug)
    {
        this.gdebug = gdebug;
    }

    public int getEvaluations()
    {
        return evaluations;
    }

    public long getTotalMillis()
    {
        return totalMillis;
    }

    public void resetTimer()
    {
        totalMillis = 0;
        evaluations = 0;
    }

    public void setSpatialWeights(float[] weights)
    {
        if (weights == null || weights.length != ctrlDims[0] * ctrlDims[1] * ctrlDims[2]) {
            this.spatialWeights = null;
            return;
        }

        this.spatialWeights = weights;
    }

    private static native double native_computeDeformationValGrad(float[] point, float[] gradient,
                                                                  int[] ctrlDims, float deformationWeight, float[] spatialWeights, int nCtrls);

    private static native double native_computeVolumeDeformationValGrad(float[] point, float[] gradient,
                                                                        int[] ctrlDims, float volumeWeight, int nCtrls, int[] boxOff, float vol0);

    public static native double native_computeMatchValGrad(float[] point, float[] gradient, int[] ctrlDims,
                                                           float[] source, int[] srcDims, float[] targetArray, int[] tDims,
                                                           short[] mask, float shortNorm, float[] coeffsTmp, int density);

    public double computeValGrad(float[] point, float[] gradient)
    {
        float[] coeffsTmp = new float[4 * (density + 1)];
        for (int i = 0, l = 0; i < 4; i++) {
            for (int j = 0; j < (density + 1); j++, l++) {
                coeffsTmp[l] = coeffs[i][j];
            }
        }

        long t0_total = System.currentTimeMillis();
        matchEnergy = 0;
        deformationEnergy = 0;
        volumeEnergy = 0;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = matchGradient[i] = deformationGradient[i] = 0;
        }

        switch (tArr.getType()) {
            case FIELD_DATA_BYTE:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_DOUBLE:
            case FIELD_DATA_FLOAT:

                System.out.println("");
                System.out.println("start native_computeValGrad");
                long t0_computeValGrad = System.currentTimeMillis();
                matchEnergy = native_computeMatchValGrad(point, matchGradient, ctrlDims,
                                                         source, srcDims, tArr.getRawFloatArray().getData(), tDims,
                                                         mask, shortNorm, coeffsTmp, density);
                long t1_computeValGrad = System.currentTimeMillis();
                System.out.println("native_computeValGrad time (s): " + ((float) (t1_computeValGrad - t0_computeValGrad) / 1000.0f));

                System.out.println("start java_computeValGrad");
                t0_computeValGrad = System.currentTimeMillis();
                float[] jmatchGradient = new float[gradient.length];
                for (int i = 0; i < jmatchGradient.length; i++) {
                    jmatchGradient[i] = 0;
                }
                double jmatchEnergy = java_computeMatchValGrad(point, jmatchGradient, ctrlDims,
                                                               source, srcDims, tArr.getRawFloatArray().getData(), tDims,
                                                               mask, shortNorm, coeffsTmp, density);
                t1_computeValGrad = System.currentTimeMillis();
                System.out.println("java_computeValGrad time (s): " + ((float) (t1_computeValGrad - t0_computeValGrad) / 1000.0f));

                System.out.println("start computeValGrad comparison");
                System.out.println("native matchEnergy = " + matchEnergy);
                System.out.println("java matchEnergy = " + jmatchEnergy);
                //float eps = 0.0000000001f;
                //float eps = 0.0000001f;
                float eps = 0.0001f;
                int c = 0;
                for (int i = 0; i < jmatchGradient.length; i++) {
                    if (Math.abs(jmatchGradient[i] - matchGradient[i]) > eps) {
                        c++;
                    }
                }
                System.out.println("gradient error count = " + c + "(of " + gradient.length + ")");

        }

        //deformationValGrad
        long t0_computeDeformationValGrad = System.currentTimeMillis();
        if (spatialWeights == null) {
            float[] spatialWeightsDummy = new float[]{0};
            deformationEnergy = native_computeDeformationValGrad(point, deformationGradient, ctrlDims, deformationWeight, spatialWeightsDummy, nCtrls);
        } else {
            deformationEnergy = native_computeDeformationValGrad(point, deformationGradient, ctrlDims, deformationWeight, spatialWeights, nCtrls);
        }
        long t1_computeDeformationValGrad = System.currentTimeMillis();
        System.out.println("computeDeformationValGrad time (s): " + ((float) (t1_computeDeformationValGrad - t0_computeDeformationValGrad) / 1000.0f));

        //volumeDeformationValGrad
        int[] tmpBoxOff = new int[3 * 2 * 4];
        for (int i = 0, l = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 4; k++, l++) {
                    tmpBoxOff[l] = boxOff[i][j][k];
                }
            }
        }
        long t0_computeVolumeValGrad = System.currentTimeMillis();
        volumeEnergy = native_computeVolumeDeformationValGrad(point, deformationGradient, ctrlDims, volumeWeight, nCtrls, tmpBoxOff, (float) vol0);
        long t1_computeVolumeValGrad = System.currentTimeMillis();
        System.out.println("computeVolumeValGrad time (s): " + ((float) (t1_computeVolumeValGrad - t0_computeVolumeValGrad) / 1000.0f));

        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = deformationGradient[i] + matchGradient[i];
        }

        evaluations += 1;
        totalMillis += System.currentTimeMillis() - t0_total;
        System.out.println("total computeValGrad time (s): " + ((float) totalMillis / 1000.0f));

        return matchEnergy + deformationEnergy + volumeEnergy;
    }

    public float[] getDisplacedCoords(float[] ctrlCoords)
    {
        float[] dispCoords = new float[3 * source.length];
        for (int i = 0; i < dispCoords.length; i++) {
            dispCoords[i] = 0;
        }
        float[] pcoord = new float[3];
        for (int i = 0; i < ctrlDims[2] - 1; i++) {
            for (int j = 0; j < ctrlDims[1] - 1; j++) {
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int px = density;
                    if (i == ctrlDims[2] - 2) {
                        px = density + 1;
                    }
                    int qx = density;
                    if (j == ctrlDims[1] - 2) {
                        qx = density + 1;
                    }
                    int rx = density;
                    if (k == ctrlDims[0] - 2) {
                        rx = density + 1;
                    }
                    for (int p = 0; p < px; p++) {
                        for (int q = 0; q < qx; q++) {
                            for (int r = 0, l = ((density * i + p) * srcDims[1] + density * j + q) * srcDims[0] + density * k; r < rx; r++, l++) {
                                for (int m = 0; m < pcoord.length; m++) {
                                    pcoord[m] = 0;
                                }
                                for (int ii = 0; ii < 4; ii++) {
                                    int ix = i + ii - 1;
                                    if (ix < 0) {
                                        ix = 0;
                                    }
                                    if (ix >= ctrlDims[2]) {
                                        ix = ctrlDims[2] - 1;
                                    }
                                    for (int jj = 0; jj < 4; jj++) {
                                        int jx = j + jj - 1;
                                        if (jx < 0) {
                                            jx = 0;
                                        }
                                        if (jx >= ctrlDims[1]) {
                                            jx = ctrlDims[1] - 1;
                                        }
                                        int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                        for (int kk = 0; kk < 4; kk++) {
                                            for (int v = 0; v < 3; v++) {
                                                int kx = k + kk - 1;
                                                if (kx < 0) {
                                                    kx = 0;
                                                }
                                                if (kx >= ctrlDims[0]) {
                                                    kx = ctrlDims[0] - 1;
                                                }
                                                pcoord[v] += ctrlCoords[3 * (isrc + kx) + v] * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                            }
                                        }
                                    }
                                }
                                System.arraycopy(pcoord, 0, dispCoords, 3 * l, pcoord.length);
                            }
                        }
                    }
                }
            }
        }
        return dispCoords;
    }

    public byte[] getTransformedTarget(float[] ctrlCoords)
    {
        byte[] transTar = new byte[source.length];
        float[] pcoord = new float[3];
        float[] tGradient = new float[3];
        for (int i = 0; i < transTar.length; i++) {
            for (int j = 0; j < 3; j++) {
                pcoord[j] = ctrlCoords[3 * i + j];
            }
            double tV = SplineValueGradient.getSplineValueGradient(tArr, d0, d1, d2, pcoord, tGradient);
            transTar[i] = (byte) ((int) tV & 0xff);
        }
        return transTar;
    }

    //-----------------------------------------------
    //    public static void main(String[] args) {
    //
    //        String libPath = "/home/babor/java/projects/VNe4/tmp/libVNNativeFieldFitting.so";
    //        try {
    //            System.load(libPath);
    //        } catch (UnsatisfiedLinkError err) {
    //            System.out.println("ERROR");
    //            err.printStackTrace();
    //            System.exit(1);
    //        } catch (NullPointerException ex) {
    //            ex.printStackTrace();
    //            System.exit(1);
    //        } catch (Exception ex) {
    //            ex.printStackTrace();
    //            System.exit(1);
    //        }
    //        System.out.println("native library loaded");
    //
    //        int N = 10;
    //        int iters = 10;
    //
    //        int[] jctrlDims = new int[]{N, N, N};
    //        int jnCtrls = N * N * N;
    //        float[] jpoint = new float[jnCtrls * 3];
    //        float[] jgradient1 = new float[jnCtrls * 3];
    //        float[] jgradient2 = new float[jnCtrls * 3];
    //        float jdeformationWeight = 1.0f;
    //
    //        System.out.println("initializing");
    //        for (int i = 0; i < jgradient1.length; i++) {
    //            jgradient1[i] = 0.0f;
    //            jgradient2[i] = 0.0f;
    //        }
    //        int a;
    //        Random r = new Random();
    //        for (int i = 0; i < jctrlDims[0]; i++) {
    //            for (int j = 0; j < jctrlDims[1]; j++) {
    //                for (int k = 0; k < jctrlDims[2]; k++) {
    //                    a = (k * jctrlDims[0] * jctrlDims[1] + j * jctrlDims[0] + i);
    //                    jpoint[3 * a] = i + (r.nextFloat() - 0.5f);
    //                    jpoint[3 * a + 1] = j + (r.nextFloat() - 0.5f);
    //                    jpoint[3 * a + 2] = k + (r.nextFloat() - 0.5f);
    //                }
    //            }
    //        }
    //
    //        System.out.println("start java calcluation");
    //        long t0 = System.currentTimeMillis();
    //        for (int n = 0; n < iters; n++) {
    //            double v1 = java_computeDeformationValGrad(jpoint, jgradient1, jctrlDims, jdeformationWeight, null, jnCtrls);
    //        }
    //        long t1 = System.currentTimeMillis();
    //        float t = (float) (t1 - t0) / (iters * 1000.0f);
    //        System.out.println("java time (s): " + t);
    //
    //        System.out.println("start native calcluation");
    //        t0 = System.currentTimeMillis();
    //        for (int n = 0; n < iters; n++) {
    //            double v2 = native_computeDeformationValGrad(jpoint, jgradient2, jctrlDims, jdeformationWeight, jpoint, jnCtrls);
    //        }
    //        t1 = System.currentTimeMillis();
    //        t = (float) (t1 - t0) / (iters * 1000.0f);
    //        System.out.println("native time (s): " + t);
    //
    //        System.out.println("compare results");
    //        float eps = 0.0000000001f;
    //        int c = 0;
    //        for (int i = 0; i < jgradient2.length; i++) {
    //            if (Math.abs(jgradient1[i] - jgradient2[i]) > eps) {
    //                c++;
    //            }
    //        }
    //        System.out.println("gradient error count = " + c);
    //
    //
    //
    //
    //
    //    }
    private static double java_computeDeformationValGrad(float[] point, float[] gradient, int[] ctrlDims, float deformationWeight, float[] spatialWeights, int nCtrls)
    {
        if (deformationWeight == 0) {
            return 0;
        }
        float[] u = new float[3];
        float[] v = new float[3];
        float[] w = new float[3];
        int s0 = 3 * ctrlDims[0];
        int s1 = 3 * ctrlDims[0] * ctrlDims[1];
        float sw;
        double smoothEnergy = 0;

        // d2v/dx2
        for (int i = 0; i < ctrlDims[2]; i++) {
            for (int j = 0; j < ctrlDims[1]; j++) {
                for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0] - 1; k++, l += 3) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - 3 + m];
                        v[m] = point[l + 3 + m] - point[l + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + 3 + m] += 2 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - 3 + m] += 2 * w[m];
                        }
                    } else {
                        sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                        smoothEnergy += wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + 3 + m] += 2 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - 3 + m] += 2 * w[m] * sw;
                        }
                    }
                }
            }
        }
        // d2v/dxdy
        for (int i = 0; i < ctrlDims[2]; i++) {
            for (int j = 1; j < ctrlDims[1]; j++) {
                for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0]; k++, l += 3) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - 3 + m];
                        v[m] = point[l - s0 + m] - point[l - s0 - 3 + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }

                    if (spatialWeights == null) {
                        smoothEnergy += 2 * wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s0 + m] += 4 * w[m];
                            gradient[l - s0 - 3 + m] -= 4 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - 3 + m] += 4 * w[m];
                        }
                    } else {
                        sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                        smoothEnergy += 2 * wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s0 + m] += 4 * w[m] * sw;
                            gradient[l - s0 - 3 + m] -= 4 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - 3 + m] += 4 * w[m] * sw;
                        }
                    }
                }
            }
        }
        // d2v/dxdz
        for (int i = 1; i < ctrlDims[2]; i++) {
            for (int j = 0; j < ctrlDims[1]; j++) {
                for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0]; k++, l += 3) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - 3 + m];
                        v[m] = point[l - s1 + m] - point[l - s1 - 3 + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }

                    if (spatialWeights == null) {
                        smoothEnergy += 2 * wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s1 + m] += 4 * w[m];
                            gradient[l - s1 - 3 + m] -= 4 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - 3 + m] += 4 * w[m];
                        }
                    } else {
                        sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                        smoothEnergy += 2 * wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s1 + m] += 4 * w[m] * sw;
                            gradient[l - s1 - 3 + m] -= 4 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - 3 + m] += 4 * w[m] * sw;
                        }
                    }
                }
            }
        }
        // d2v/dy2
        for (int i = 0; i < ctrlDims[2]; i++) {
            for (int j = 0; j < ctrlDims[0]; j++) {
                for (int k = 1, l = ((i * ctrlDims[1] + 1) * ctrlDims[0] + j) * 3; k < ctrlDims[1] - 1; k++, l += s0) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - s0 + m];
                        v[m] = point[l + s0 + m] - point[l + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s0 + m] += 2 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - s0 + m] += 2 * w[m];
                        }
                    } else {
                        sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + k * ctrlDims[0] + j];
                        smoothEnergy += wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s0 + m] += 2 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - s0 + m] += 2 * w[m] * sw;
                        }
                    }
                }
            }
        }
        // d2v/dydz
        for (int i = 1; i < ctrlDims[2]; i++) {
            for (int j = 0; j < ctrlDims[0]; j++) {
                for (int k = 1, l = ((i * ctrlDims[1] + 1) * ctrlDims[0] + j) * 3; k < ctrlDims[1]; k++, l += s0) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - s1 + m];
                        v[m] = point[l - 3 + m] - point[l - s1 - 3 + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += 2 * wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - 3 + m] += 4 * w[m];
                            gradient[l - s1 - 3 + m] -= 4 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - s1 + m] += 4 * w[m];
                        }
                    } else {
                        sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + k * ctrlDims[0] + j];
                        smoothEnergy += 2 * wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - 3 + m] += 4 * w[m] * sw;
                            gradient[l - s1 - 3 + m] -= 4 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - s1 + m] += 4 * w[m] * sw;
                        }
                    }
                }
            }
        }

        // d2v/dz2
        for (int i = 0; i < ctrlDims[1]; i++) {
            for (int j = 0; j < ctrlDims[0]; j++) {
                for (int k = 1, l = ((i + ctrlDims[1]) * ctrlDims[0] + j) * 3; k < ctrlDims[2] - 1; k++, l += s1) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - s1 + m];
                        v[m] = point[l + s1 + m] - point[l + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s1 + m] += 2 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - s1 + m] += 2 * w[m];
                        }
                    } else {
                        sw = spatialWeights[k * ctrlDims[0] * ctrlDims[1] + i * ctrlDims[0] + j];
                        smoothEnergy += wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s1 + m] += 2 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - s1 + m] += 2 * w[m] * sw;
                        }
                    }
                }
            }
        }

        for (int i = 0; i < gradient.length; i++) {
            gradient[i] *= deformationWeight / nCtrls;
        }
        smoothEnergy *= deformationWeight / nCtrls;
        return smoothEnergy;
    }

    private static double java_computeVolumeDeformationValGrad(float[] point, float[] gradient, int[] ctrlDims, float volumeWeight, int nCtrls, int[][][] boxOff, float vol0)
    {
        if (volumeWeight == 0) {
            return 0;
        }
        double en = 0;
        float[] v0 = new float[3], v1 = new float[3], v2 = new float[3];
        float[] w0 = new float[3], w1 = new float[3], w2 = new float[3];
        for (int i = 0; i < ctrlDims[2] - 1; i++) {
            for (int j = 0; j < ctrlDims[1] - 1; j++) {
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int k0 = 3 * ((i * ctrlDims[1] + j) * ctrlDims[0] + k);
                    for (int l = 0; l < 3; l++) {
                        v0[l] = v1[l] = v2[l] = 0;
                        for (int m = 0; m < 4; m++) {
                            v0[l] += point[k0 + boxOff[0][1][m] + l] - point[k0 + boxOff[0][0][m] + l];
                            v1[l] += point[k0 + boxOff[1][1][m] + l] - point[k0 + boxOff[1][0][m] + l];
                            v2[l] += point[k0 + boxOff[2][1][m] + l] - point[k0 + boxOff[2][0][m] + l];
                        }
                        v0[l] *= .25f;
                        v1[l] *= .25f;
                        v2[l] *= .25f;
                    }
                    w0[0] = v1[1] * v2[2] - v1[2] * v2[1];
                    w0[1] = v1[2] * v2[0] - v1[0] * v2[2];
                    w0[2] = v1[0] * v2[1] - v1[1] * v2[0];
                    w1[0] = v2[1] * v0[2] - v2[2] * v0[1];
                    w1[1] = v2[2] * v0[0] - v2[0] * v0[2];
                    w1[2] = v2[0] * v0[1] - v2[1] * v0[0];
                    w2[0] = v0[1] * v1[2] - v0[2] * v1[1];
                    w2[1] = v0[2] * v1[0] - v0[0] * v1[2];
                    w2[2] = v0[0] * v1[1] - v0[1] * v1[0];
                    double v = v0[0] * w0[0] + v0[1] * w0[1] + v0[2] * w0[2];
                    en += (v - vol0) * (v - vol0);
                    double dif = .5f * volumeWeight * (v - vol0) / nCtrls;
                    for (int l = 0; l < 3; l++) {
                        for (int m = 0; m < 4; m++) {
                            gradient[k0 + boxOff[0][1][m] + l] += dif * w0[l];
                            gradient[k0 + boxOff[0][0][m] + l] -= dif * w0[l];
                            gradient[k0 + boxOff[1][1][m] + l] += dif * w1[l];
                            gradient[k0 + boxOff[1][0][m] + l] -= dif * w1[l];
                            gradient[k0 + boxOff[2][1][m] + l] += dif * w2[l];
                            gradient[k0 + boxOff[2][0][m] + l] -= dif * w2[l];
                        }
                    }
                }
            }
        }
        return volumeWeight * en / nCtrls;
    }

    public static double java_computeMatchValGrad(float[] point, float[] gradient, int[] ctrlDims, float[] source, int[] srcDims, float[] targetArray, int[] tDims, short[] mask, float shortNorm, float[] coeffsTmp, int density)
    {
        double matchEnergy = 0;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = 0;
        }

        double[][] coeffs = new double[4][density + 1];
        for (int i = 0, l = 0; i < 4; i++) {
            for (int j = 0; j < (density + 1); j++, l++) {
                coeffs[i][j] = (double) coeffsTmp[l];
            }
        }

        double[] pcoord = new double[3];

        for (int i = 0; i < ctrlDims[2] - 1; i++) {
            for (int j = 0; j < ctrlDims[1] - 1; j++) {
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int px = density;
                    if (i == ctrlDims[2] - 2) {
                        px = density + 1;
                    }
                    int qx = density;
                    if (j == ctrlDims[1] - 2) {
                        qx = density + 1;
                    }
                    int rx = density;
                    if (k == ctrlDims[0] - 2) {
                        rx = density + 1;
                    }
                    for (int p = 0; p < px; p++) {
                        for (int q = 0; q < qx; q++) {
                            for (int r = 0, l = ((density * i + p) * srcDims[1] + density * j + q) * srcDims[0] + density * k; r < rx; r++, l++) {
                                if (mask != null && mask[l] == 0) {
                                    continue;
                                }
                                for (int m = 0; m < pcoord.length; m++) {
                                    pcoord[m] = 0;
                                }
                                for (int ii = 0; ii < 4; ii++) {
                                    int ix = i + ii - 1;
                                    if (ix < 0) {
                                        ix = 0;
                                    }
                                    if (ix >= ctrlDims[2]) {
                                        ix = ctrlDims[2] - 1;
                                    }
                                    for (int jj = 0; jj < 4; jj++) {
                                        int jx = j + jj - 1;
                                        if (jx < 0) {
                                            jx = 0;
                                        }
                                        if (jx >= ctrlDims[1]) {
                                            jx = ctrlDims[1] - 1;
                                        }
                                        int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                        for (int kk = 0; kk < 4; kk++) {
                                            for (int v = 0; v < 3; v++) {
                                                int kx = k + kk - 1;
                                                if (kx < 0) {
                                                    kx = 0;
                                                }
                                                if (kx >= ctrlDims[0]) {
                                                    kx = ctrlDims[0] - 1;
                                                }
                                                pcoord[v] = pcoord[v] + (double) point[3 * (isrc + kx) + v] * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                            }
                                        }
                                    }
                                }
                                double maskCoeff = 1.0;
                                if (mask != null)
                                    maskCoeff = mask[l] * shortNorm;
                                double[] splineGradient = new double[3];

                                double splineVal = java_calculateSplineValueGradient(targetArray, tDims, pcoord, splineGradient);
                                //System.out.println("j splineVal="+splineVal+" splineGrad=["+splineGradient[0]+","+splineGradient[1]+","+splineGradient[2]+"]");
                                double sV = source[l];
                                matchEnergy = matchEnergy + (splineVal - sV) * (splineVal - sV);
                                for (int ii = 0; ii < 4; ii++) {
                                    int ix = min(max(i + ii - 1, 0), ctrlDims[2] - 1);
                                    for (int jj = 0; jj < 4; jj++) {
                                        int jx = min(max(j + jj - 1, 0), ctrlDims[1] - 1);
                                        int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                        for (int kk = 0; kk < 4; kk++) {
                                            for (int v = 0; v < 3; v++) {
                                                int kx = min(max(k + kk - 1, 0), ctrlDims[0] - 1);
                                                double c = maskCoeff * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                                int off = 3 * (isrc + kx) + v;
                                                gradient[off] = (float) ((double) gradient[off] + c * (splineVal - sV) * splineGradient[v]);
                                                //System.out.println("j["+ix+","+jx+","+kx+"]:"+c);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        double nSrcs = srcDims[0] * srcDims[1] * srcDims[2];
        matchEnergy /= nSrcs;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] /= nSrcs;
        }
        return matchEnergy;
    }

    private static double java_calculateSplineValueGradient(float[] target, int[] tDims, double[] coords, double[] gradient)
    {
        int i, j, k, off1, off2, il, jl, kl, iu, ju, ku, k1, j1, i1;
        double vu, vv, vw, du, dv, dw, t, tt;
        double u = coords[0];
        double v = coords[1];
        double w = coords[2];
        double val = 0;
        for (i = 0; i < 3; i++) {
            gradient[i] = 0;
        }

        off1 = tDims[0];
        off2 = tDims[0] * tDims[1];
        i = min(max((int) u, 0), tDims[0] - 1);
        u -= i;
        j = min(max((int) v, 0), tDims[1] - 1);
        v -= j;
        k = min(max((int) w, 0), tDims[2] - 1);
        w -= k;
        il = -1; // if (i == 0) il = 0;
        jl = -1; // if (j == 0) jl = 0;
        kl = -1; // if (k == 0) kl = 0;
        iu = 3; // if (i + iu > tDims[0])  iu = tDims[0] - i;
        ju = 3; // if (j + ju > tDims[1])  ju = tDims[1] - j;
        ku = 3; // if (k + ku > tDims[2])  ku = tDims[2] - k;
        vu = 0;
        vv = 0;
        vw = 0;
        du = 0;
        dv = 0;
        dw = 0;
        for (k1 = kl; k1 < ku; k1++) {
            switch (k1) {
                case -1:
                    vw = ((-.5 * w + 1.0) * w - .5) * w;
                    dw = (-1.5 * w + 2.0) * w - .5;
                    break;
                case 0:
                    vw = (1.5 * w - 2.5) * w * w + 1.0;
                    dw = (4.5 * w - 5.0) * w;
                    break;
                case 1:
                    vw = ((-1.5 * w + 2.0) * w + .5) * w;
                    dw = (-4.5 * w + 4.) * w + .5;
                    break;
                case 2:
                    vw = (.5 * w - .5) * w * w;
                    dw = (1.5 * w - 1.0) * w;
                    break;
            }
            for (j1 = jl; j1 < ju; j1++) {
                switch (j1) {
                    case -1:
                        vv = ((-.5 * v + 1.0) * v - .5) * v;
                        dv = (-1.5 * v + 2.0) * v - .5;
                        break;
                    case 0:
                        vv = (1.5 * v - 2.5) * v * v + 1.0;
                        dv = (4.5 * v - 5.0) * v;
                        break;
                    case 1:
                        vv = ((-1.5 * v + 2.) * v + .5) * v;
                        dv = (-4.5 * v + 4.) * v + .5;
                        break;
                    case 2:
                        vv = (.5 * v - .5) * v * v;
                        dv = (1.5 * v - 1.0) * v;
                        break;
                }
                for (i1 = il; i1 < iu; i1++) {
                    switch (i1) {
                        case -1:
                            vu = ((-.5 * u + 1.0) * u - .5) * u;
                            du = (-1.5 * u + 2.0) * u - .5;
                            break;
                        case 0:
                            vu = (1.5 * u - 2.5) * u * u + 1.0;
                            du = (4.5 * u - 5.0) * u;
                            break;
                        case 1:
                            vu = ((-1.5 * u + 2.0) * u + .5) * u;
                            du = (-4.5 * u + 4.0) * u + .5;
                            break;
                        case 2:
                            vu = (.5 * u - .5) * u * u;
                            du = (1.5 * u - 1.0) * u;
                            break;
                    }
                    t = (double) target[max(0, min(k + k1, tDims[2] - 1)) * off2 + max(0, min(j + j1, tDims[1] - 1)) * off1 + max(0, min(i + i1, tDims[0] - 1))];
                    //val += t * vu * vv * vw;
                    tt = t * vu * vv * vw;
                    val = val + tt;
                    gradient[0] = gradient[0] + 2.0 * t * du * vv * vw;
                    gradient[1] = gradient[1] + 2.0 * t * vu * dv * vw;
                    gradient[2] = gradient[2] + 2.0 * t * vu * vv * dw;

                    //                    if(coords[0] == 0 && coords[1] == 0 && coords[2] == 9.6f) {
                    //                        System.out.format("tt=%.8f t=%.8f vu=%.8f vv=%.8f vw=%.8f\n",tt,t,vu,vv,vw);
                    //                        System.out.flush();
                    //                    }
                }
            }
        }
        return val;
    }

    /*
     public float[] getDisplacedCoords(float[] ctrlCoords) {
     float[] dispCoords = new float[3 * source.length];
     for (int i = 0; i < dispCoords.length; i++) {
     dispCoords[i] = 0;
     }
     float[] pcoord = new float[3];
     for (int i = 0; i < ctrlDims[2] - 1; i++) {
     for (int j = 0; j < ctrlDims[1] - 1; j++) {
     for (int k = 0; k < ctrlDims[0] - 1; k++) {
     int px = density;
     if (i == ctrlDims[2] - 2) {
     px = density + 1;
     }
     int qx = density;
     if (j == ctrlDims[1] - 2) {
     qx = density + 1;
     }
     int rx = density;
     if (k == ctrlDims[0] - 2) {
     rx = density + 1;
     }
     for (int p = 0; p < px; p++) {
     for (int q = 0; q < qx; q++) {
     for (int r = 0,
     l = ((density * i + p) * srcDims[1] + density * j + q) * srcDims[0] + density * k;
     r < rx;
     r++, l++) {
     for (int m = 0; m < pcoord.length; m++) {
     pcoord[m] = 0;
     }
     for (int ii = 0; ii < 4; ii++) {
     int ix = i + ii - 1;
     if (ix < 0) {
     ix = 0;
     }
     if (ix >= ctrlDims[2]) {
     ix = ctrlDims[2] - 1;
     }
     for (int jj = 0; jj < 4; jj++) {
     int jx = j + jj - 1;
     if (jx < 0) {
     jx = 0;
     }
     if (jx >= ctrlDims[1]) {
     jx = ctrlDims[1] - 1;
     }
     int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
     for (int kk = 0; kk < 4; kk++) {
     for (int v = 0; v < 3; v++) {
     int kx = k + kk - 1;
     if (kx < 0) {
     kx = 0;
     }
     if (kx >= ctrlDims[0]) {
     kx = ctrlDims[0] - 1;
     }
     pcoord[v] += ctrlCoords[3 * (isrc + kx) + v] * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
     }
     }
     }
     }
     System.arraycopy(pcoord, 0, dispCoords, 3 * l, pcoord.length);
     }
     }
     }
     }
     }
     }
     return dispCoords;
     }

     public byte[] getTransformedTarget(float[] ctrlCoords) {
     byte[] transTar = new byte[source.length];
     float[] pcoord = new float[3];
     float[] tGradient = new float[3];
     for (int i = 0; i < transTar.length; i++) {
     for (int j = 0; j < 3; j++) {
     pcoord[j] = ctrlCoords[3 * i + j];
     }
     double tV = SplineValueGradient.getSplineValueGradient(tArr, d0, d1, d2, pcoord, tGradient);
     transTar[i] = (byte) ((int) tV & 0xff);
     }
     return transTar;
     }
     */
    public static void main(String[] args)
    {
        String libPath = "/home/staff/babor/java/projects/VNNativeFieldFitting/dist/libVNNativeFieldFitting.so";
        try {
            System.load(libPath);
        } catch (UnsatisfiedLinkError err) {
            System.out.println("ERROR");
            err.printStackTrace();
            System.exit(1);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
            System.exit(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        System.out.println("native library loaded");

        int density = 1;
        int s = 20;

        int[] srcDims = new int[]{8 * s, 8 * s, 8 * s};
        int nSrc = srcDims[0] * srcDims[1] * srcDims[2];
        float[] source = new float[nSrc];

        int[] tDims = new int[]{8 * s, 8 * s, 8 * s};
        int nTrg = tDims[0] * tDims[1] * tDims[2];
        float[] target = new float[nTrg];

        for (int i = 0; i < nTrg; i++) {
            target[i] = i;
            source[i] = i + 4 * s;
        }

        int[] ctrlDims = new int[]{3 * s, 3 * s, 3 * s};
        int nCtrls = ctrlDims[0] * ctrlDims[1] * ctrlDims[2];

        float[] point = new float[3 * nCtrls];
        for (int i = 0, l = 0; i < ctrlDims[0]; i++) {
            for (int j = 0; j < ctrlDims[1]; j++) {
                for (int k = 0; k < ctrlDims[2]; k++, l++) {
                    point[3 * l] = tDims[0] * (float) i / (float) (ctrlDims[0] - 1);
                    point[3 * l + 1] = tDims[1] * (float) j / (float) (ctrlDims[1] - 1);
                    point[3 * l + 2] = tDims[2] * (float) k / (float) (ctrlDims[2] - 1);
                }
            }
        }

        float[] gradient = new float[3 * point.length];
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = 0;
        }

        short[] mask = new short[nSrc];
        for (int i = 0; i < mask.length; i++) {
            mask[i] = Short.MAX_VALUE;
        }
        float shortNorm = 1.f / Short.MAX_VALUE;

        float[][] coeffs = new float[4][density + 1];
        for (int j = 0; j <= density; j++) {
            float x = (float) j / density;
            coeffs[3][j] = (.5f * x - .5f) * x * x;
            coeffs[2][j] = ((-1.5f * x + 2.f) * x + .5f) * x;
            coeffs[1][j] = (1.5f * x - 2.5f) * x * x + 1.f;
            coeffs[0][j] = ((-.5f * x + 1.0f) * x - .5f) * x;
        }
        float[] coeffsTmp = new float[4 * (density + 1)];
        for (int i = 0, l = 0; i < 4; i++) {
            for (int j = 0; j < (density + 1); j++, l++) {
                coeffsTmp[l] = coeffs[i][j];
            }
        }
        double matchEnergy = 0;

        System.out.println("");
        System.out.println("start native_computeValGrad");
        long t0_computeValGrad = System.currentTimeMillis();
        matchEnergy = native_computeMatchValGrad(point, gradient, ctrlDims,
                                                 source, srcDims, target, tDims,
                                                 mask, shortNorm, coeffsTmp, density);
        long t1_computeValGrad = System.currentTimeMillis();
        System.out.println("native_computeValGrad time (s): " + ((float) (t1_computeValGrad - t0_computeValGrad) / 1000.0f));

        System.out.println("start java_computeValGrad");
        t0_computeValGrad = System.currentTimeMillis();
        float[] jmatchGradient = new float[gradient.length];
        for (int i = 0; i < jmatchGradient.length; i++) {
            jmatchGradient[i] = 0;
        }
        double jmatchEnergy = java_computeMatchValGrad(point, jmatchGradient, ctrlDims,
                                                       source, srcDims, target, tDims,
                                                       mask, shortNorm, coeffsTmp, density);
        t1_computeValGrad = System.currentTimeMillis();
        System.out.println("java_computeValGrad time (s): " + ((float) (t1_computeValGrad - t0_computeValGrad) / 1000.0f));

        System.out.println("start computeValGrad comparison");
        System.out.println("native matchEnergy = " + matchEnergy);
        System.out.println("java matchEnergy = " + jmatchEnergy);
        float eps = 0.0000000001f;
        //float eps = 0.0000001f;
        int c = 0;
        for (int i = 0; i < jmatchGradient.length; i++) {
            if (Math.abs(jmatchGradient[i] - gradient[i]) > eps) {
                c++;
            }
        }
        System.out.println("gradient error count = " + c + "(of " + gradient.length + ")");

    }

}
