/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.numeric.fieldFitting;

import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.lib.utils.numeric.minimization.GradVal;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class AffineFieldFitting3D implements GradVal
{

    protected int[] ctrlDims;
    protected int[] srcDims;
    protected int density;
    protected float[] source;
    protected short[] mask = null;
    protected int[] tDims;
    protected DataArray tArr;
    protected float[][] coeffs = null;
    protected int nThreads = 2;
    protected float deformationWeight = 10;
    protected float volumeWeight = .1f;
    protected int nCtrls, nSrcs;
    protected float[] matchGradient;
    protected float[] deformationGradient;
    protected double matchEnergy, deformationEnergy, volumeEnergy;
    protected boolean gdebug = false;
    protected long totalMillis = 0;
    protected int evaluations = 0;
    protected float shortNorm = 1.f / Short.MAX_VALUE;
    protected double vol0 = 1;

    protected int[][][] mBoxOff = new int[][][]{{{0, 2, 4, 6}, {1, 3, 5, 7}}, {{0, 1, 4, 5}, {2, 3, 6, 7}}, {{0, 1, 2, 3}, {4, 5, 6, 7}}};
    protected int[][][] boxOff = new int[3][2][4];
    protected int d0 = 0, d1 = 0, d2 = 0;

    protected float[] spatialWeights = null;

    /**
     *
     * @param ctrlDims   - dimensions of control points grid - ctrlDims.length must be 3
     * @param ctrlCoords - target coordinates for control points in target index space (ctrlCoords.length = 3*ctrlDims[0]*ctrlDims[1]*ctrlDims[2]) - independent
     *                   variables
     * @param density    - density of source grid refinement w/r to the control points grid sourceDims[i] = density*(ctrlDims[i]-1)+1
     * @param source     - scalar source data array
     * @param tDims      - dimensions of target grid - tDims.length must be 3
     * @param tArr       - target scalar data array
     * @param gradient   - array for storing gradient - must be preallocated of length ctrlCoords.length.
     */
    public AffineFieldFitting3D(int[] ctrlDims,
                                int[] srcDims, float[] source,
                                int[] tDims, DataArray tArr,
                                DataArray mask)
    {
        if (ctrlDims == null || ctrlDims.length != 3)
            return;
        if (source == null || source.length != srcDims[0] * srcDims[1] * srcDims[2])
            return;
        if (tDims == null || tDims.length != 3 ||
            tArr == null || !tArr.isNumeric() || tArr.getNElements() != tDims[0] * tDims[1] * tDims[2] || tArr.getVectorLength() != 1)
            return;
        if (ceil((srcDims[0] - 1.) / (ctrlDims[0] - 1.)) != ceil((srcDims[1] - 1.) / (ctrlDims[1] - 1.)) ||
            ceil((srcDims[2] - 1.) / (ctrlDims[2] - 1.)) != ceil((srcDims[1] - 1.) / (ctrlDims[1] - 1.)))
            return;
        density = (int) (Math.ceil((srcDims[0] - 1.) / (ctrlDims[0] - 1.)));
        this.ctrlDims = ctrlDims;
        nCtrls = ctrlDims[0] * ctrlDims[1] * ctrlDims[2];

        this.srcDims = srcDims;
        nSrcs = srcDims[0] * srcDims[1] * srcDims[2];
        this.source = source;
        if (mask == null) {
            this.mask = new short[nSrcs];
            for (int i = 0; i < nSrcs; i++)
                this.mask[i] = Short.MAX_VALUE;
        } else
            this.mask = mask.getRawShortArray().getData();
        this.tDims = tDims;
        this.tArr = tArr;
        d0 = tDims[0];
        d1 = tDims[1];
        d2 = tDims[2];
        coeffs = new float[4][density + 1];
        for (int j = 0; j <= density; j++) {
            float x = (float) j / density;
            coeffs[3][j] = (.5f * x - .5f) * x * x;
            coeffs[2][j] = ((-1.5f * x + 2.f) * x + .5f) * x;
            coeffs[1][j] = (1.5f * x - 2.5f) * x * x + 1.f;
            coeffs[0][j] = ((-.5f * x + 1.0f) * x - .5f) * x;
        }

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 2; j++)
                for (int k = 0; k < 4; k++) {
                    boxOff[i][j][k] = 0;
                    if ((mBoxOff[i][j][k] & 1) != 0)
                        boxOff[i][j][k] += 3;
                    if ((mBoxOff[i][j][k] & 2) != 0)
                        boxOff[i][j][k] += 3 * ctrlDims[0];
                    if ((mBoxOff[i][j][k] & 4) != 0)
                        boxOff[i][j][k] += 3 * ctrlDims[0] * ctrlDims[1];
                }

        int gradSize = 3 * ctrlDims[0] * ctrlDims[1] * ctrlDims[2];
        matchGradient = new float[gradSize];
        deformationGradient = new float[gradSize];
    }

    public AffineFieldFitting3D(int[] ctrlDims,
                                int[] srcDims, float[] source,
                                int[] tDims, DataArray tArr,
                                DataArray mask, double vol0)
    {
        this(ctrlDims, srcDims, source, tDims, tArr, mask);
        this.vol0 = vol0;
    }

    private class ComputeValGrad implements Runnable
    {

        private float[] ctrlCoords;
        private float[] matchGradient;
        private int iThread = 0;
        private double matchValue;

        ComputeValGrad(float[] ctrlCoords, int nThreads, int iThread)
        {
            this.ctrlCoords = ctrlCoords;
            this.iThread = iThread;
            matchGradient = new float[ctrlCoords.length];
        }

        public float[] getMatchGradient()
        {
            return matchGradient;
        }

        public double getMatchValue()
        {
            return matchValue;
        }

        public void run()
        {
            float[] pcoord = new float[3];
            float[] pvect = new float[3];
            boolean debug = false;

            matchValue = 0;
            for (int i = 0; i < ctrlCoords.length; i++)
                matchGradient[i] = 0;

            for (int i = iThread; i < ctrlDims[2] - 1; i += nThreads)
                for (int j = 0; j < ctrlDims[1] - 1; j++)
                    for (int k = 0; k < ctrlDims[0] - 1; k++) {
                        debug = i >= ctrlDims[2] - 2 && (j < 2 || j >= ctrlDims[1] - 2) && (k < 2 || k >= ctrlDims[0] - 2);
                        debug = debug && gdebug;
                        int px = density;
                        if (i == ctrlDims[2] - 2)
                            px = density + 1;
                        int qx = density;
                        if (j == ctrlDims[1] - 2)
                            qx = density + 1;
                        int rx = density;
                        if (k == ctrlDims[0] - 2)
                            rx = density + 1;
                        for (int p = 0; p < px; p++)
                            for (int q = 0; q < qx; q++)
                                for (int r = 0, l = ((density * i + p) * srcDims[1] + density * j + q) * srcDims[0] + density * k; r < rx; r++, l++) {
                                    if (mask[l] == 0)
                                        continue;
                                    for (int m = 0; m < pvect.length; m++)
                                        pvect[m] = pcoord[m] = 0;
                                    for (int ii = 0; ii < 4; ii++) {
                                        int ix = i + ii - 1;
                                        if (ix < 0)
                                            ix = 0;
                                        if (ix >= ctrlDims[2])
                                            ix = ctrlDims[2] - 1;
                                        for (int jj = 0; jj < 4; jj++) {
                                            int jx = j + jj - 1;
                                            if (jx < 0)
                                                jx = 0;
                                            if (jx >= ctrlDims[1])
                                                jx = ctrlDims[1] - 1;
                                            int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                            for (int kk = 0; kk < 4; kk++) {
                                                for (int v = 0; v < 3; v++) {
                                                    int kx = k + kk - 1;
                                                    if (kx < 0)
                                                        kx = 0;
                                                    if (kx >= ctrlDims[0])
                                                        kx = ctrlDims[0] - 1;
                                                    pcoord[v] += ctrlCoords[3 * (isrc + kx) + v] * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                                }
                                            }
                                        }
                                    }
                                    double maskCoeff = mask[l] * shortNorm;
                                    float[] splineGradient = new float[3];
                                    double splineVal = getSplineValueGradient(pcoord, splineGradient);
                                    double sV = source[l];
                                    matchValue += (splineVal - sV) * (splineVal - sV);
                                    for (int ii = 0; ii < 4; ii++) {
                                        int ix = min(max(i + ii - 1, 0), ctrlDims[2] - 1);
                                        for (int jj = 0; jj < 4; jj++) {
                                            int jx = min(max(j + jj - 1, 0), ctrlDims[1] - 1);
                                            int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                            for (int kk = 0; kk < 4; kk++) {
                                                for (int v = 0; v < 3; v++) {
                                                    int kx = min(max(k + kk - 1, 0), ctrlDims[0] - 1);
                                                    double c = maskCoeff * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                                    matchGradient[3 * (isrc + kx) + v] += c * (splineVal - sV) * splineGradient[v];
                                                }
                                            }
                                        }
                                    }
                                }
                    }
        }
    }

    /**
     * computes energy (sum over source points of (t(F(x)-s(x))^2)) and energy gradient with respect to control points
     * <p>
     * @param ctrlCoords - target coordinates for control points in target index space
     *                   (ctrlCoords.length = 3*ctrlDims[0]*ctrlDims[1]*ctrlDims[2]) - independent variables
     * @param gradient   - array for storing gradient - must be preallocated of length ctrlCoords.length.
     * <p>
     * @return energy value (if negative - wrong input data)
     */
    double getSplineValueGradient(float[] coords, float[] gradient)
    {
        float u = coords[0];
        float v = coords[1];
        float w = coords[2];
        double val = 0;
        for (int i = 0; i < gradient.length; i++)
            gradient[i] = 0;
        int off1 = d0;
        int off2 = d0 * d1;
        int i = min(max((int) u, 0), d0 - 1);
        u -= i;
        int j = min(max((int) v, 0), d1 - 1);
        v -= j;
        int k = min(max((int) w, 0), d2 - 1);
        w -= k;
        int il = -1;// if (i == 0) il = 0;
        int jl = -1;// if (j == 0) jl = 0;
        int kl = -1;// if (k == 0) kl = 0;
        int iu = 3;// if (i + iu > tDims[0])  iu = tDims[0] - i;
        int ju = 3;// if (j + ju > tDims[1])  ju = tDims[1] - j;
        int ku = 3;// if (k + ku > tDims[2])  ku = tDims[2] - k;
        float vu = 0, vv = 0, vw = 0;
        float du = 0, dv = 0, dw = 0;
        for (int k1 = kl; k1 < ku; k1++) {
            switch (k1) {
                case -1:
                    vw = ((-.5f * w + 1.0f) * w - .5f) * w;
                    dw = (-1.5f * w + 2.0f) * w - .5f;
                    break;
                case 0:
                    vw = (1.5f * w - 2.5f) * w * w + 1.f;
                    dw = (4.5f * w - 5.0f) * w;
                    break;
                case 1:
                    vw = ((-1.5f * w + 2.f) * w + .5f) * w;
                    dw = (-4.5f * w + 4.f) * w + .5f;
                    break;
                case 2:
                    vw = (.5f * w - .5f) * w * w;
                    dw = (1.5f * w - 1.f) * w;
                    break;
            }
            for (int j1 = jl; j1 < ju; j1++) {
                switch (j1) {
                    case -1:
                        vv = ((-.5f * v + 1.0f) * v - .5f) * v;
                        dv = (-1.5f * v + 2.0f) * v - .5f;
                        break;
                    case 0:
                        vv = (1.5f * v - 2.5f) * v * v + 1.f;
                        dv = (4.5f * v - 5.0f) * v;
                        break;
                    case 1:
                        vv = ((-1.5f * v + 2.f) * v + .5f) * v;
                        dv = (-4.5f * v + 4.f) * v + .5f;
                        break;
                    case 2:
                        vv = (.5f * v - .5f) * v * v;
                        dv = (1.5f * v - 1.f) * v;
                        break;
                }
                for (int i1 = il; i1 < iu; i1++) {
                    switch (i1) {
                        case -1:
                            vu = ((-.5f * u + 1.0f) * u - .5f) * u;
                            du = (-1.5f * u + 2.0f) * u - .5f;
                            break;
                        case 0:
                            vu = (1.5f * u - 2.5f) * u * u + 1.f;
                            du = (4.5f * u - 5.0f) * u;
                            break;
                        case 1:
                            vu = ((-1.5f * u + 2.f) * u + .5f) * u;
                            du = (-4.5f * u + 4.f) * u + .5f;
                            break;
                        case 2:
                            vu = (.5f * u - .5f) * u * u;
                            du = (1.5f * u - 1.f) * u;
                            break;
                    }
                    float t = tArr.getFloatElement(max(0, min(k + k1, d2 - 1)) * off2 + max(0, min(j + j1, d1 - 1)) * off1 + max(0, min(i + i1, d0 - 1)))[0];
                    val += t * vu * vv * vw;
                    gradient[0] += 2 * t * du * vv * vw;
                    gradient[1] += 2 * t * vu * dv * vw;
                    gradient[2] += 2 * t * vu * vv * dw;
                }
            }
        }
        return val;
    }

    private double computeDeformationValGrad(float[] point, float[] gradient)
    {
        if (deformationWeight == 0)
            return 0;
        float[] u = new float[3];
        float[] v = new float[3];
        float[] w = new float[3];
        int s0 = 3 * ctrlDims[0];
        int s1 = 3 * ctrlDims[0] * ctrlDims[1];
        float sw;
        double smoothEnergy = 0;

        // d2v/dx2
        for (int i = 0; i < ctrlDims[2]; i++)
            for (int j = 0; j < ctrlDims[1]; j++)
                for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0] - 1; k++, l += 3) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - 3 + m];
                        v[m] = point[l + 3 + m] - point[l + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + 3 + m] += 2 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - 3 + m] += 2 * w[m];
                        }
                    } else {
                        sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                        smoothEnergy += wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + 3 + m] += 2 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - 3 + m] += 2 * w[m] * sw;
                        }
                    }
                }
        // d2v/dxdy
        for (int i = 0; i < ctrlDims[2]; i++)
            for (int j = 1; j < ctrlDims[1]; j++)
                for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0]; k++, l += 3) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - 3 + m];
                        v[m] = point[l - s0 + m] - point[l - s0 - 3 + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }

                    if (spatialWeights == null) {
                        smoothEnergy += 2 * wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s0 + m] += 4 * w[m];
                            gradient[l - s0 - 3 + m] -= 4 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - 3 + m] += 4 * w[m];
                        }
                    } else {
                        sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                        smoothEnergy += 2 * wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s0 + m] += 4 * w[m] * sw;
                            gradient[l - s0 - 3 + m] -= 4 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - 3 + m] += 4 * w[m] * sw;
                        }
                    }
                }
        // d2v/dxdz
        for (int i = 1; i < ctrlDims[2]; i++)
            for (int j = 0; j < ctrlDims[1]; j++)
                for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0]; k++, l += 3) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - 3 + m];
                        v[m] = point[l - s1 + m] - point[l - s1 - 3 + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }

                    if (spatialWeights == null) {
                        smoothEnergy += 2 * wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s1 + m] += 4 * w[m];
                            gradient[l - s1 - 3 + m] -= 4 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - 3 + m] += 4 * w[m];
                        }
                    } else {
                        sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                        smoothEnergy += 2 * wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s1 + m] += 4 * w[m] * sw;
                            gradient[l - s1 - 3 + m] -= 4 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - 3 + m] += 4 * w[m] * sw;
                        }
                    }
                }
        // d2v/dy2
        for (int i = 0; i < ctrlDims[2]; i++)
            for (int j = 0; j < ctrlDims[0]; j++)
                for (int k = 1, l = ((i * ctrlDims[1] + 1) * ctrlDims[0] + j) * 3; k < ctrlDims[1] - 1; k++, l += s0) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - s0 + m];
                        v[m] = point[l + s0 + m] - point[l + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s0 + m] += 2 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - s0 + m] += 2 * w[m];
                        }
                    } else {
                        sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + k * ctrlDims[0] + j];
                        smoothEnergy += wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s0 + m] += 2 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - s0 + m] += 2 * w[m] * sw;
                        }
                    }
                }
        // d2v/dydz
        for (int i = 1; i < ctrlDims[2]; i++)
            for (int j = 0; j < ctrlDims[0]; j++)
                for (int k = 1, l = ((i * ctrlDims[1] + 1) * ctrlDims[0] + j) * 3; k < ctrlDims[1]; k++, l += s0) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - s1 + m];
                        v[m] = point[l - 3 + m] - point[l - s1 - 3 + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += 2 * wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - 3 + m] += 4 * w[m];
                            gradient[l - s1 - 3 + m] -= 4 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - s1 + m] += 4 * w[m];
                        }
                    } else {
                        sw = spatialWeights[i * ctrlDims[0] * ctrlDims[1] + k * ctrlDims[0] + j];
                        smoothEnergy += 2 * wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - 3 + m] += 4 * w[m] * sw;
                            gradient[l - s1 - 3 + m] -= 4 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - s1 + m] += 4 * w[m] * sw;
                        }
                    }
                }

        // d2v/dz2
        for (int i = 0; i < ctrlDims[1]; i++)
            for (int j = 0; j < ctrlDims[0]; j++)
                for (int k = 1, l = ((i + ctrlDims[1]) * ctrlDims[0] + j) * 3; k < ctrlDims[2] - 1; k++, l += s1) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - s1 + m];
                        v[m] = point[l + s1 + m] - point[l + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s1 + m] += 2 * w[m];
                            gradient[l + m] -= 4 * w[m];
                            gradient[l - s1 + m] += 2 * w[m];
                        }
                    } else {
                        sw = spatialWeights[k * ctrlDims[0] * ctrlDims[1] + i * ctrlDims[0] + j];
                        smoothEnergy += wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s1 + m] += 2 * w[m] * sw;
                            gradient[l + m] -= 4 * w[m] * sw;
                            gradient[l - s1 + m] += 2 * w[m] * sw;
                        }
                    }
                }

        for (int i = 0; i < gradient.length; i++)
            gradient[i] *= deformationWeight / nCtrls;
        smoothEnergy *= deformationWeight / nCtrls;
        return smoothEnergy;
    }

    public double computeValGrad(float[] point, float[] gradient)
    {
        long t0 = System.currentTimeMillis();
        matchEnergy = 0;
        for (int i = 0; i < gradient.length; i++)
            gradient[i] = matchGradient[i] = deformationGradient[i] = 0;

        ComputeValGrad[] computeThreads = new ComputeValGrad[nThreads];
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            computeThreads[iThread] = new ComputeValGrad(point, nThreads, iThread);
            workThreads[iThread] = new Thread(computeThreads[iThread]);
            workThreads[iThread].start();
        }
        for (int iThread = 0; iThread < workThreads.length; iThread++)
            try {
                workThreads[iThread].join();
            } catch (Exception e) {
            }
        for (int iThread = 0; iThread < nThreads; iThread++) {
            matchEnergy += computeThreads[iThread].getMatchValue();
            float[] thrGr = computeThreads[iThread].getMatchGradient();
            for (int i = 0; i < thrGr.length; i++)
                matchGradient[i] += thrGr[i];
        }
        matchEnergy /= nSrcs;
        deformationEnergy = computeDeformationValGrad(point, deformationGradient);
        volumeEnergy = computeVolumeDeformationValGrad(point, deformationGradient);

        for (int i = 0; i < gradient.length; i++) {
            matchGradient[i] /= nSrcs;
            //gradient[i] = volumeGradient[i] + smoothnessGradient[i] + matchGradient[i];
            gradient[i] = deformationGradient[i] + matchGradient[i];
        }

        evaluations += 1;
        totalMillis += System.currentTimeMillis() - t0;
        //      System.out.printf("  matchEnergy = %8.3f   deformationEnergy = %8.3f    volumeEnergy = %8.3f     total energy = %8.3f%n",
        //                         matchEnergy, deformationEnergy, volumeEnergy, matchEnergy + deformationEnergy + volumeEnergy);
        return matchEnergy + deformationEnergy + volumeEnergy;
    }

    private double computeVolumeDeformationValGrad(float[] point, float[] gradient)
    {
        if (volumeWeight == 0)
            return 0;
        double en = 0;
        double avgvol = 0;
        float[] v0 = new float[3], v1 = new float[3], v2 = new float[3];
        float[] w0 = new float[3], w1 = new float[3], w2 = new float[3];
        for (int i = 0; i < ctrlDims[2] - 1; i++)
            for (int j = 0; j < ctrlDims[1] - 1; j++)
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int k0 = 3 * ((i * ctrlDims[1] + j) * ctrlDims[0] + k);
                    for (int l = 0; l < 3; l++) {
                        v0[l] = v1[l] = v2[l] = 0;
                        for (int m = 0; m < 4; m++) {
                            v0[l] += point[k0 + boxOff[0][1][m] + l] - point[k0 + boxOff[0][0][m] + l];
                            v1[l] += point[k0 + boxOff[1][1][m] + l] - point[k0 + boxOff[1][0][m] + l];
                            v2[l] += point[k0 + boxOff[2][1][m] + l] - point[k0 + boxOff[2][0][m] + l];
                        }
                        v0[l] *= .25f;
                        v1[l] *= .25f;
                        v2[l] *= .25f;
                    }
                    w0[0] = v1[1] * v2[2] - v1[2] * v2[1];
                    w0[1] = v1[2] * v2[0] - v1[0] * v2[2];
                    w0[2] = v1[0] * v2[1] - v1[1] * v2[0];
                    w1[0] = v2[1] * v0[2] - v2[2] * v0[1];
                    w1[1] = v2[2] * v0[0] - v2[0] * v0[2];
                    w1[2] = v2[0] * v0[1] - v2[1] * v0[0];
                    w2[0] = v0[1] * v1[2] - v0[2] * v1[1];
                    w2[1] = v0[2] * v1[0] - v0[0] * v1[2];
                    w2[2] = v0[0] * v1[1] - v0[1] * v1[0];
                    double v = v0[0] * w0[0] + v0[1] * w0[1] + v0[2] * w0[2];
                    avgvol += v;
                    en += (v - vol0) * (v - vol0);
                    double dif = .5f * volumeWeight * (v - vol0) / nCtrls;
                    for (int l = 0; l < 3; l++)
                        for (int m = 0; m < 4; m++) {
                            gradient[k0 + boxOff[0][1][m] + l] += dif * w0[l];
                            gradient[k0 + boxOff[0][0][m] + l] -= dif * w0[l];
                            gradient[k0 + boxOff[1][1][m] + l] += dif * w1[l];
                            gradient[k0 + boxOff[1][0][m] + l] -= dif * w1[l];
                            gradient[k0 + boxOff[2][1][m] + l] += dif * w2[l];
                            gradient[k0 + boxOff[2][0][m] + l] -= dif * w2[l];
                        }
                }
        avgvol /= (ctrlDims[2] - 1) * (ctrlDims[1] - 1) * (ctrlDims[0] - 1);
        return volumeWeight * en / nCtrls;
    }

    private class ComputeDisplacement implements Runnable
    {

        private float[] ctrlCoords;
        private float[] dispCoords;
        private int iThread = 0;

        ComputeDisplacement(float[] ctrlCoords, int nThreads, int iThread)
        {
            this.ctrlCoords = ctrlCoords;
            this.iThread = iThread;
            dispCoords = new float[3 * source.length];
        }

        public void run()
        {
            float[] pcoord = new float[3];
            for (int i = iThread; i < ctrlDims[2] - 1; i += nThreads)
                for (int j = 0; j < ctrlDims[1] - 1; j++)
                    for (int k = 0; k < ctrlDims[0] - 1; k++) {
                        int px = density;
                        if (i == ctrlDims[2] - 2)
                            px = density + 1;
                        int qx = density;
                        if (j == ctrlDims[1] - 2)
                            qx = density + 1;
                        int rx = density;
                        if (k == ctrlDims[0] - 2)
                            rx = density + 1;
                        for (int p = 0; p < px; p++)
                            for (int q = 0; q < qx; q++)
                                for (int r = 0, l = ((density * i + p) * srcDims[1] + density * j + q) * srcDims[0] + density * k; r < rx; r++, l++) {
                                    for (int m = 0; m < pcoord.length; m++)
                                        pcoord[m] = 0;
                                    for (int ii = 0; ii < 4; ii++) {
                                        int ix = i + ii - 1;
                                        if (ix < 0)
                                            ix = 0;
                                        if (ix >= ctrlDims[2])
                                            ix = ctrlDims[2] - 1;
                                        for (int jj = 0; jj < 4; jj++) {
                                            int jx = j + jj - 1;
                                            if (jx < 0)
                                                jx = 0;
                                            if (jx >= ctrlDims[1])
                                                jx = ctrlDims[1] - 1;
                                            int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                            for (int kk = 0; kk < 4; kk++) {
                                                for (int v = 0; v < 3; v++) {
                                                    int kx = k + kk - 1;
                                                    if (kx < 0)
                                                        kx = 0;
                                                    if (kx >= ctrlDims[0])
                                                        kx = ctrlDims[0] - 1;
                                                    pcoord[v] += ctrlCoords[3 * (isrc + kx) + v] * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                                }
                                            }
                                        }
                                    }
                                    System.arraycopy(pcoord, 0, dispCoords, 3 * l, pcoord.length);
                                }
                    }
        }

        public float[] getDispCoords()
        {
            return dispCoords;
        }
    }

    public float[] getThreadedDisplacedCoords(float[] ctrlCoords)
    {
        float[] dispCoords = new float[3 * source.length];
        for (int i = 0; i < dispCoords.length; i++)
            dispCoords[i] = 0;
        ComputeDisplacement[] computeThreads = new ComputeDisplacement[nThreads];
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            computeThreads[iThread] = new ComputeDisplacement(ctrlCoords, nThreads, iThread);
            workThreads[iThread] = new Thread(computeThreads[iThread]);
            workThreads[iThread].start();
        }
        for (int iThread = 0; iThread < workThreads.length; iThread++)
            try {
                workThreads[iThread].join();
            } catch (Exception e) {
            }
        for (int iThread = 0; iThread < nThreads; iThread++) {
            float[] dc = computeThreads[iThread].getDispCoords();
            for (int i = 0; i < dispCoords.length; i++)
                dispCoords[i] += dc[i];
        }
        return dispCoords;
    }

    public float[] getDisplacedCoords(float[] ctrlCoords)
    {
        float[] dispCoords = new float[3 * source.length];
        for (int i = 0; i < dispCoords.length; i++)
            dispCoords[i] = 0;
        float[] pcoord = new float[3];
        for (int i = 0; i < ctrlDims[2] - 1; i++)
            for (int j = 0; j < ctrlDims[1] - 1; j++)
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int px = density;
                    if (i == ctrlDims[2] - 2)
                        px = density + 1;
                    int qx = density;
                    if (j == ctrlDims[1] - 2)
                        qx = density + 1;
                    int rx = density;
                    if (k == ctrlDims[0] - 2)
                        rx = density + 1;
                    for (int p = 0; p < px; p++)
                        for (int q = 0; q < qx; q++)
                            for (int r = 0, l = ((density * i + p) * srcDims[1] + density * j + q) * srcDims[0] + density * k; r < rx; r++, l++) {
                                for (int m = 0; m < pcoord.length; m++)
                                    pcoord[m] = 0;
                                for (int ii = 0; ii < 4; ii++) {
                                    int ix = i + ii - 1;
                                    if (ix < 0)
                                        ix = 0;
                                    if (ix >= ctrlDims[2])
                                        ix = ctrlDims[2] - 1;
                                    for (int jj = 0; jj < 4; jj++) {
                                        int jx = j + jj - 1;
                                        if (jx < 0)
                                            jx = 0;
                                        if (jx >= ctrlDims[1])
                                            jx = ctrlDims[1] - 1;
                                        int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                        for (int kk = 0; kk < 4; kk++) {
                                            for (int v = 0; v < 3; v++) {
                                                int kx = k + kk - 1;
                                                if (kx < 0)
                                                    kx = 0;
                                                if (kx >= ctrlDims[0])
                                                    kx = ctrlDims[0] - 1;
                                                pcoord[v] += ctrlCoords[3 * (isrc + kx) + v] * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                            }
                                        }
                                    }
                                }
                                System.arraycopy(pcoord, 0, dispCoords, 3 * l, pcoord.length);
                            }
                }
        return dispCoords;
    }

    public byte[] getTransformedTarget(float[] ctrlCoords)
    {
        byte[] transTar = new byte[source.length];
        float[] pcoord = new float[3];
        float[] tGradient = new float[3];
        for (int i = 0; i < transTar.length; i++) {
            for (int j = 0; j < 3; j++)
                pcoord[j] = ctrlCoords[3 * i + j];
            double tV = getSplineValueGradient(pcoord, tGradient);
            transTar[i] = (byte) ((int) tV & 0xff);
        }
        //      for (int i = 0; i < ctrlDims[2]-1; i++)
        //         for (int j = 0; j < ctrlDims[1]-1; j++)
        //            for (int k = 0; k < ctrlDims[0]-1; k++)
        //            {
        //               int px = density; if (i == ctrlDims[2]-2) px = density + 1;
        //               int qx = density; if (j == ctrlDims[1]-2) qx = density + 1;
        //               int rx = density; if (k == ctrlDims[0]-2) rx = density + 1;
        //               for (int p = 0; p < px; p++)
        //                  for (int q = 0; q < qx; q++)
        //                     for (int r = 0,
        //                          l = ((density * i + p) * srcDims[1] + density * j + q)  * srcDims[0] + density * k;
        //                          r < rx;
        //                          r++, l++)
        //                     {
        //                        for (int m = 0; m < pcoord.length; m++)
        //                           pcoord[m] = 0;
        //                        for (int ii = 0; ii < 4; ii++)
        //                        {
        //                           int ix = i + ii - 1; if (ix < 0) ix = 0; if (ix >= ctrlDims[2]) ix = ctrlDims[2]-1;
        //                           for (int jj = 0; jj < 4; jj++)
        //                           {
        //                              int jx = j + jj - 1; if (jx < 0) jx = 0; if (jx >= ctrlDims[1]) jx = ctrlDims[1]-1;
        //                              int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
        //                              for (int kk = 0; kk < 4 ; kk++)
        //                              {
        //                                 for (int v = 0; v < 3; v++)
        //                                 {
        //                                    int kx = k + kk - 1; if (kx < 0) kx = 0; if (kx >= ctrlDims[0]) kx = ctrlDims[0]-1;
        //                                    pcoord[v] += ctrlCoords[3 * (isrc + kx) + v] * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
        //                                 }
        //                              }
        //                           }
        //                        }
        //                        float[] tGradient = new float[3];
        //                        double tV = getSplineValueGradient(pcoord, tGradient);
        //                        transTar[l] = (byte)((int)tV & 0xff);
        //                    }
        //            }
        return transTar;
    }

    public void setDeformationWeight(float deformationWeight)
    {
        this.deformationWeight = deformationWeight;
    }

    public void setVolumeWeight(float volumeWeight)
    {
        this.volumeWeight = volumeWeight;
    }

    public void setNThreads(int nThreads)
    {
        this.nThreads = nThreads;
    }

    public double getDeformationEnergy()
    {
        return deformationEnergy;
    }

    public float[] getDeformationGradient()
    {
        return deformationGradient;
    }

    public double getVolumeEnergy()
    {
        return volumeEnergy;
    }

    public double getMatchEnergy()
    {
        return matchEnergy;
    }

    public float[] getMatchGradient()
    {
        return matchGradient;
    }

    public void setGdebug(boolean gdebug)
    {
        this.gdebug = gdebug;
    }

    public int getEvaluations()
    {
        return evaluations;
    }

    public long getTotalMillis()
    {
        return totalMillis;
    }

    public void resetTimer()
    {
        totalMillis = 0;
        evaluations = 0;
    }

    public void setSpatialWeights(float[] weights)
    {
        if (weights == null || weights.length != ctrlDims[0] * ctrlDims[1] * ctrlDims[2]) {
            this.spatialWeights = null;
            return;
        }

        this.spatialWeights = weights;
    }

    public String[] getVarNames()
    {
        return new String[]{"matchEnergy", "deformationEnergy", "volumeEnergy"};
    }

    public double[] getVariables()
    {
        return new double[]{matchEnergy, deformationEnergy, volumeEnergy};
    }

}
