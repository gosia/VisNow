//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.numeric;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.Cell;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.cells.Hex;
import pl.edu.icm.jscic.cells.Prism;
import pl.edu.icm.jscic.cells.Pyramid;
import pl.edu.icm.jscic.cells.Quad;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import static pl.edu.icm.visnow.lib.utils.numeric.SliceLookupTable.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class IrregularFieldSplitter
{

    protected static final int CHUNK = 4096;

    class NewNode
    {

        public int p0, p1, index;
        public float ratio;

        public NewNode(int index, int p0, int p1, float ratio)
        {
            this.index = index;
            this.p0 = p0;
            this.p1 = p1;
            this.ratio = ratio;
        }

        public long getHash()
        {
            return (long) p1 << 32 | (long) p0;
        }
    }

    protected HashMap<Long, NewNode> newNodes = new HashMap<Long, NewNode>();

    protected IrregularField inField;
    protected int position = 0;

    protected boolean[] usedNodes;
    protected int nInNodes;
    protected int nOldNodes;
    protected int nNewNodes;
    protected int nOutNodes;
    protected int[] globalNewNodeIndices;

    protected int totalNewNodes = 0;

    protected int totalOutputCells = 0;

    protected ArrayList<ArrayList<int[]>[]> newCells = new ArrayList<ArrayList<int[]>[]>();
    protected ArrayList<ArrayList<int[]>[]> newCellDataIndices = new ArrayList<ArrayList<int[]>[]>();
    protected ArrayList<int[]> totalCellsInSets = new ArrayList<int[]>();

    protected int[] totalNewCells = new int[Cell.getNProperCellTypes()];
    protected int[] currentNewCells = new int[Cell.getNProperCellTypes()];
    protected ArrayList<int[]>[] newCellsInSet;
    protected ArrayList<int[]>[] newCellDataIndicesInSet;

    protected CellType cellType;
    protected int nCellNodes;
    protected int[] nodes;
    protected int[] dataIndices;

    protected NewNode[] newNodesArray;
    protected IrregularField outField;

    public IrregularFieldSplitter(IrregularField inField, int position)
    {
        this.inField = inField;
        this.position = position;
        nInNodes = (int) inField.getNNodes();
        usedNodes = new boolean[nInNodes];
        nOldNodes = nNewNodes = nOutNodes = 0;
        nNewNodes = 0;
        totalOutputCells = 0;
        totalCellsInSets.clear();
    }

    @SuppressWarnings("unchecked")
    public void initCellSetSplit(CellSet trCS)
    {
        Arrays.fill(totalNewCells, 0);
        Arrays.fill(currentNewCells, CHUNK);
        newCellsInSet = new ArrayList[Cell.getNProperCellTypes()];
        newCellDataIndicesInSet = new ArrayList[Cell.getNProperCellTypes()];
        for (int i = 0; i < Cell.getNProperCellTypes(); i++) {
            newCellsInSet[i] = new ArrayList<int[]>();
            newCellDataIndicesInSet[i] = new ArrayList<int[]>();
        }
        newCells.add(newCellsInSet);
        newCellDataIndices.add(newCellDataIndicesInSet);
        totalNewCells = new int[Cell.getNProperCellTypes()];
        totalNewCells = new int[Cell.getNProperCellTypes()];
        totalCellsInSets.add(totalNewCells);
    }

    public void initCellArraySplit(CellArray ca)
    {
        cellType = ca.getType();
        nCellNodes = cellType.getNVertices();
        nodes = ca.getNodes();
        dataIndices = ca.getDataIndices();
    }

    public void processSimplex(int iCell, float[] vals, int cellDataIndex)
    {
        int[] slicedCell = new int[nCellNodes];
        System.arraycopy(nodes, iCell * nCellNodes, slicedCell, 0, nCellNodes);
        processSimplex(slicedCell, vals, cellDataIndex);
    }

    public void processSimplex(int[] slicedSimplex, float[] vals, int cellDataIndex)
    {
        CellType subcellType;
        if (position != 0)
            subcellType = getSubcellType(cellType, vals, position > 0);
        else
            subcellType = getSliceType(cellType, vals);
        if (subcellType.getValue() < 0)
            return;
        CellType[] subcellNodes;
        if (position != 0)
            subcellNodes = getSubcellNodes(cellType, vals, position > 0);
        else
            subcellNodes = getSliceNodes(cellType, vals);
        int currentCell = currentNewCells[subcellType.getValue()];
        int[] currentCells;
        int[] currentDataIndices;
        if (currentCell >= CHUNK) {
            currentDataIndices = new int[CHUNK];
            currentCells = new int[subcellType.getNVertices() * CHUNK];
            newCellsInSet[subcellType.getValue()].add(currentCells);
            newCellDataIndicesInSet[subcellType.getValue()].add(currentDataIndices);
            currentCell = currentNewCells[subcellType.getValue()] = 0;
        } else {
            currentCells = newCellsInSet[subcellType.getValue()].get(newCellsInSet[subcellType.getValue()].size() - 1);
            currentDataIndices = newCellDataIndicesInSet[subcellType.getValue()].get(newCellsInSet[subcellType.getValue()].size() - 1);
        }
        int subcellSize = subcellNodes.length;
        for (int l = 0; l < subcellNodes.length; l++) {
            if (subcellNodes[l].getValue() < nCellNodes) {
                int node = slicedSimplex[subcellNodes[l].getValue()];
                currentCells[subcellSize * currentCell + l] = node;
                usedNodes[node] = true;
            } else {
                int n0 = addNodes[nCellNodes][subcellNodes[l].getValue()][0];
                int n1 = addNodes[nCellNodes][subcellNodes[l].getValue()][1];
                int p0 = slicedSimplex[n0];
                int p1 = slicedSimplex[n1];
                long key = p0 < p1 ? (long) p1 << 32 | (long) p0 : (long) p0 << 32 | (long) p1;

                NewNode node = newNodes.get(key);
                if (node == null) {
                    newNodes.put(key, new NewNode(nNewNodes, p0, p1,
                                                  vals[n1] / (vals[n1] - vals[n0])));
                    currentCells[subcellSize * currentCell + l] = nInNodes + nNewNodes;
                    nNewNodes += 1;
                } else {
                    currentCells[subcellSize * currentCell + l] = nInNodes + node.index;
                }
            }
        }
        currentDataIndices[currentCell] = cellDataIndex;
        totalNewCells[subcellType.getValue()] += 1;
        currentNewCells[subcellType.getValue()] += 1;
        totalOutputCells += 1;
    }

    public void processCell(int iCell, float[] vals)
    {
        int[] slicedCell = new int[nCellNodes];
        System.arraycopy(nodes, iCell * nCellNodes, slicedCell, 0, nCellNodes);
        if (dataIndices != null)
            processCell(slicedCell, vals, dataIndices[iCell]);
        else
            processCell(slicedCell, vals, -1);
    }

    public void processCell(int[] cell, float[] vals, int cellDataIndex)
    {
        if (cellType.isSimplex())
            processSimplex(cell, vals, cellDataIndex);
        else {
            for (int l = 0; l < cell.length; l++)
                usedNodes[cell[l]] = true;
            CellType newCellType;
            int[][] cellTriangulation;
            int[][] triangIndices;
            switch (cellType) {
                case QUAD:
                    newCellType = CellType.TRIANGLE;
                    cellTriangulation = Quad.triangulationVertices(cell);
                    triangIndices = Quad.triangulationIndices(cell);
                    break;
                case PYRAMID:
                    newCellType = CellType.TETRA;
                    cellTriangulation = Pyramid.triangulationVertices(cell);
                    triangIndices = Pyramid.triangulationIndices(cell);
                    break;
                case PRISM:
                    newCellType = CellType.TETRA;
                    cellTriangulation = Prism.triangulationVertices(cell);
                    triangIndices = Prism.triangulationIndices(cell);
                    break;
                case HEXAHEDRON:
                    newCellType = CellType.TETRA;
                    cellTriangulation = Hex.triangulationVertices(cell);
                    triangIndices = Hex.triangulationIndices(cell);
                    break;
                default:
                    return;
            }
            int nSimplexNodes = newCellType.getNVertices();
            for (int iSimplex = 0; iSimplex < cellTriangulation.length; iSimplex++) {
                int[] slicedSimplex = cellTriangulation[iSimplex];
                float[] slicedVals = new float[nSimplexNodes];
                for (int i = 0; i < nSimplexNodes; i++)
                    slicedVals[i] = vals[triangIndices[iSimplex][i]];
                CellType subcellType;
                if (position != 0)
                    subcellType = getSubcellType(newCellType, slicedVals, position > 0);
                else
                    subcellType = getSliceType(newCellType, slicedVals);
                if (subcellType.getValue() < 0)
                    continue;
                CellType[] subcellNodes;
                if (position != 0)
                    subcellNodes = getSubcellNodes(newCellType, slicedVals, position > 0);
                else
                    subcellNodes = getSliceNodes(newCellType, slicedVals);
                int currentCell = currentNewCells[subcellType.getValue()];
                int[] currentCells;
                int[] currentDataIndices;
                if (currentCell >= CHUNK) {
                    currentDataIndices = new int[CHUNK];
                    currentCells = new int[subcellType.getNVertices() * CHUNK];
                    newCellsInSet[subcellType.getValue()].add(currentCells);
                    newCellDataIndicesInSet[subcellType.getValue()].add(currentDataIndices);
                    currentCell = currentNewCells[subcellType.getValue()] = 0;
                } else {
                    currentCells = newCellsInSet[subcellType.getValue()].get(newCellsInSet[subcellType.getValue()].size() - 1);
                    currentDataIndices = newCellDataIndicesInSet[subcellType.getValue()].get(newCellsInSet[subcellType.getValue()].size() - 1);
                }
                int subcellSize = subcellNodes.length;
                for (int l = 0; l < subcellNodes.length; l++) {
                    if (subcellNodes[l].getValue() < nSimplexNodes) {
                        int node = slicedSimplex[subcellNodes[l].getValue()];
                        currentCells[subcellSize * currentCell + l] = node;
                        usedNodes[node] = true;
                    } else {
                        int n0 = addNodes[nSimplexNodes][subcellNodes[l].getValue()][0];
                        int n1 = addNodes[nSimplexNodes][subcellNodes[l].getValue()][1];
                        int p0 = slicedSimplex[n0];
                        int p1 = slicedSimplex[n1];
                        long key = p0 < p1 ? (long) p1 << 32 | (long) p0 : (long) p0 << 32 | (long) p1;

                        NewNode node = newNodes.get(key);
                        if (node == null) {
                            newNodes.put(key, new NewNode(nNewNodes, p0, p1,
                                                          slicedVals[n1] / (slicedVals[n1] - slicedVals[n0])));
                            currentCells[subcellSize * currentCell + l] = nInNodes + nNewNodes;
                            nNewNodes += 1;
                        } else {
                            currentCells[subcellSize * currentCell + l] = nInNodes + node.index;
                        }
                    }
                }
                currentDataIndices[currentCell] = cellDataIndex;
                totalNewCells[subcellType.getValue()] += 1;
                currentNewCells[subcellType.getValue()] += 1;
                totalOutputCells += 1;
            }
        }
    }

    public void addSimplex(int iCell)
    {
        int[] simplex = new int[nCellNodes];
        System.arraycopy(nodes, iCell * nCellNodes, simplex, 0, nCellNodes);
        if (dataIndices != null)
            addSimplex(simplex, dataIndices[iCell]);
        else
            addSimplex(simplex, -1);
    }

    public void addSimplex(int[] simplex, int cellDataIndex)
    {
        int currentCell = currentNewCells[cellType.getValue()];
        int[] currentCells;
        int[] currentDataIndices;
        if (currentCell >= CHUNK) {
            currentDataIndices = new int[CHUNK];
            currentCells = new int[cellType.getNVertices() * CHUNK];
            newCellsInSet[cellType.getValue()].add(currentCells);
            newCellDataIndicesInSet[cellType.getValue()].add(currentDataIndices);
            currentCell = currentNewCells[cellType.getValue()] = 0;
        } else {
            currentCells = newCellsInSet[cellType.getValue()].get(newCellsInSet[cellType.getValue()].size() - 1);
            currentDataIndices = newCellDataIndicesInSet[cellType.getValue()].get(newCellsInSet[cellType.getValue()].size() - 1);
        }
        for (int l = 0; l < simplex.length; l++)
            usedNodes[simplex[l]] = true;
        currentDataIndices[currentCell] = cellDataIndex;
        System.arraycopy(simplex, 0, currentCells, simplex.length * currentCell, simplex.length);
        totalNewCells[cellType.getValue()] += 1;
        currentNewCells[cellType.getValue()] += 1;
        totalOutputCells += 1;
    }

    public void addCellTriangulation(int iCell)
    {
        int nCellNodes = cellType.getNVertices();
        int[] cell = new int[nCellNodes];
        System.arraycopy(nodes, iCell * nCellNodes, cell, 0, nCellNodes);
        if (dataIndices != null)
            addCellTriangulation(cell, dataIndices[iCell]);
        else
            addCellTriangulation(cell, -1);
    }

    public void addCellTriangulation(int[] cell, int cellDataIndex)
    {
        if (cellType.isSimplex())
            addSimplex(cell, cellDataIndex);
        else {
            for (int l = 0; l < cell.length; l++)
                usedNodes[cell[l]] = true;
            CellType newCellType;
            int[][] cellTriangulation;
            switch (cellType) {
                case QUAD:
                    newCellType = CellType.TRIANGLE;
                    cellTriangulation = Quad.triangulationVertices(cell);
                    break;
                case PYRAMID:
                    newCellType = CellType.TETRA;
                    cellTriangulation = Pyramid.triangulationVertices(cell);
                    break;
                case PRISM:
                    newCellType = CellType.TETRA;
                    cellTriangulation = Prism.triangulationVertices(cell);
                    break;
                case HEXAHEDRON:
                    newCellType = CellType.TETRA;
                    cellTriangulation = Hex.triangulationVertices(cell);
                    break;
                default:
                    return;
            }
            for (int i = 0; i < cellTriangulation.length; i++) {
                int[] simplex = cellTriangulation[i];
                int currentCell = currentNewCells[newCellType.getValue()];
                int[] currentCells;
                int[] currentDataIndices;
                if (currentCell >= CHUNK) {
                    currentDataIndices = new int[CHUNK];
                    currentCells = new int[newCellType.getNVertices() * CHUNK];
                    newCellsInSet[newCellType.getValue()].add(currentCells);
                    newCellDataIndicesInSet[newCellType.getValue()].add(currentDataIndices);
                    currentCell = currentNewCells[newCellType.getValue()] = 0;
                } else {
                    currentCells = newCellsInSet[newCellType.getValue()].get(newCellsInSet[newCellType.getValue()].size() - 1);
                    currentDataIndices = newCellDataIndicesInSet[newCellType.getValue()].get(newCellsInSet[newCellType.getValue()].size() - 1);
                }
                currentDataIndices[currentCell] = cellDataIndex;
                System.arraycopy(simplex, 0, currentCells, simplex.length * currentCell, simplex.length);
                totalNewCells[newCellType.getValue()] += 1;
                currentNewCells[newCellType.getValue()] += 1;
                totalOutputCells += 1;
            }
        }
    }

    public IrregularField createOutField(float[] normal)
    {
        if (totalOutputCells == 0)
            return null;
        nOldNodes = 0;
        for (int i = 0; i < usedNodes.length; i++)
            if (usedNodes[i])
                nOldNodes += 1;

        nOutNodes = nOldNodes + nNewNodes;
        if (nOutNodes == 0)
            return null;
        newNodesArray = new NewNode[nNewNodes];
        for (NewNode newNode : newNodes.values())
            newNodesArray[newNode.index] = newNode;

        globalNewNodeIndices = new int[nInNodes + nNewNodes];
        Arrays.fill(globalNewNodeIndices, -1);
        int cNode = 0;
        for (int i = 0; i < usedNodes.length; i++)
            if (usedNodes[i]) {
                globalNewNodeIndices[i] = cNode;
                cNode += 1;
            }
        for (int i = 0; i < nNewNodes; i++, cNode++)
            globalNewNodeIndices[nInNodes + i] = cNode;
        outField = new IrregularField(nOutNodes);

        FloatLargeArray coords = inField.getCurrentCoords();
        FloatLargeArray outCoords = new FloatLargeArray(3 * (long)nOutNodes, false);
        int k = 0;
        for (int i = 0; i < usedNodes.length; i++)
            if (usedNodes[i]) {
                LargeArrayUtils.arraycopy(coords, 3 * i, outCoords, k, 3);
                k += 3;
            }
        for (int i = 0; i < nNewNodes; i++) {
            long k0 = newNodesArray[i].p0;
            long k1 = newNodesArray[i].p1;
            float r = newNodesArray[i].ratio;
            for (int j = 0; j < 3; j++, k++)
                outCoords.setFloat(k, r * coords.getFloat(3 * k0 + j) + (1 - r) * coords.getFloat(3 * k1 + j));
        }
        outField.setCurrentCoords(outCoords);

        //      boolean[] mask = inField.getCurrentMask();
        //      if(mask != null) {
        //            boolean[] outMask = new boolean[nOutNodes];
        //            k = 0;
        //            for (int i = 0; i < usedNodes.length; i++)
        //               if (usedNodes[i]) {
        //                  outMask[k] = mask[i];
        //                  k++;
        //               }   
        //            for (int i = 0; i < nNewNodes; i++)
        //            {
        //               int k0 = newNodesArray[i].p0;
        //               int k1 = newNodesArray[i].p1;
        //               outMask[k] = (mask[k0] && mask[k1]);
        //               k++;
        //            }
        //            outField.setMask(outMask);
        //      }
        interpolateData();

        for (int iSet = 0; iSet < inField.getNCellSets(); iSet++) {
            int dataIndex = 0;
            CellSet trCS = inField.getCellSet(iSet);
            CellSet outCS = new CellSet(trCS.getName());

            totalNewCells = totalCellsInSets.get(iSet);
            newCellsInSet = newCells.get(iSet);
            newCellDataIndicesInSet = newCellDataIndices.get(iSet);

            for (int i = 0; i < trCS.getNComponents(); i++)
                outCS.addComponent(trCS.getComponent(i).cloneShallow());

            for (int iArray = 0; iArray < Cell.getNProperCellTypes(); iArray++) {
                int nVertsInCell = CellType.getType(iArray).getNVertices();
                int nCellsInArray = totalNewCells[iArray];
                if (nCellsInArray < 1)
                    continue;
                ArrayList<int[]> cellsInArray = newCellsInSet[iArray];
                ArrayList<int[]> cellDataIndicesInArray = newCellDataIndicesInSet[iArray];
                int[] cellNodes = new int[nVertsInCell * nCellsInArray];
                int[] cellDataIndices = new int[nCellsInArray];
                byte[] orientations = new byte[nCellsInArray];
                k = 0;
                for (int i = 0; i < cellsInArray.size(); i++) {
                    int n = CHUNK;
                    if (i == cellsInArray.size() - 1)
                        n = nCellsInArray % CHUNK;
                    int[] nodesChunk = cellsInArray.get(i);
                    int[] dataIndicesChunk = cellDataIndicesInArray.get(i);
                    int[] cellVerts = new int[nVertsInCell];
                    for (int j = 0; j < n; j++, k++) {
                        cellDataIndices[k] = dataIndicesChunk[j];
                        for (int l = 0; l < cellVerts.length; l++)
                            cellVerts[l] = globalNewNodeIndices[nodesChunk[j * nVertsInCell + l]];
                        int orientation = 0;
                        Cell cell = Cell.createCell(CellType.getType(iArray), 3, cellVerts, (byte)1);
                        if (iArray <= CellType.QUAD.getValue())
                            orientation = cell.geomOrientation(outCoords, normal);
                        else
                            orientation = cell.geomOrientation(outCoords);
                        System.arraycopy(cell.getVertices(), 0, cellNodes, k * nVertsInCell, nVertsInCell);
                        orientations[k] = orientation > 0 ? 1 : (byte)0;
                    }
                }
                outCS.addCells(new CellArray(CellType.getType(iArray), cellNodes, orientations, cellDataIndices));
            }
            outCS.generateDisplayData(outCoords);
            outField.addCellSet(outCS);
        }
        return outField;
    }

    private void interpolateData()
    {
        for (int iData = 0; iData < inField.getNComponents(); iData++) {
            DataArray inDA = inField.getComponent(iData);
            if (!inDA.isNumeric())
                continue;
            int vlen = inDA.getVectorLength();
            DataArray outDA;
            int k = 0;
            switch (inDA.getType()) {
                case FIELD_DATA_BYTE:
                    break;
                case FIELD_DATA_INT:
                    int[] inID = (int[])inDA.getRawArray().getData();
                    int[] outID = new int[vlen * nOutNodes];
                    for (int i = 0; i < usedNodes.length; i++)
                        if (usedNodes[i]) {
                            System.arraycopy(inID, vlen * i, outID, k, vlen);
                            k += vlen;
                        }
                    for (int i = 0; i < nNewNodes; i++) {
                        int k0 = newNodesArray[i].p0;
                        int k1 = newNodesArray[i].p1;
                        float r = newNodesArray[i].ratio;
                        for (int j = 0; j < vlen; j++, k++)
                            outID[k] = (int) (r * inID[vlen * k0 + j] + (1 - r) * inID[vlen * k1 + j]);
                    }

                    outDA = DataArray.create(outID, vlen, inDA.getName());
                     outDA.setPreferredRanges(inDA.getPreferredMinValue(), inDA.getPreferredMaxValue(), inDA.getPreferredPhysMinValue(), inDA.getPreferredPhysMaxValue());
                    outField.addComponent(outDA);
                    break;
                case FIELD_DATA_FLOAT:
                    float[] inFD = (float[])inDA.getRawArray().getData();
                    float[] outFD = new float[vlen * nOutNodes];
                    for (int i = 0; i < nInNodes; i++)
                        if (usedNodes[i]) {
                            System.arraycopy(inFD, vlen * i, outFD, k, vlen);
                            k += vlen;
                        }
                    for (int i = 0; i < nNewNodes; i++) {
                        int k0 = newNodesArray[i].p0;
                        int k1 = newNodesArray[i].p1;
                        float r = newNodesArray[i].ratio;
                        for (int j = 0; j < vlen; j++, k++)
                            outFD[k] = r * inFD[vlen * k0 + j] + (1 - r) * inFD[vlen * k1 + j];
                    }
                    outDA = DataArray.create(outFD, vlen, inDA.getName());
                    outDA.setPreferredRanges(inDA.getPreferredMinValue(), inDA.getPreferredMaxValue(), inDA.getPreferredPhysMinValue(), inDA.getPreferredPhysMaxValue());
                    outField.addComponent(outDA);
                    break;
            }
        }
    }

}
