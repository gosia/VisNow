/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.numeric.fieldAveraging;

import pl.edu.icm.visnow.lib.utils.numeric.integration.GeometricVolumes;
import pl.edu.icm.visnow.lib.utils.numeric.splines.SplineValueGradientDP;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class TrilinearFieldAveraging3DDP implements FieldAveragingDP
{

    protected int[][] tDims;
    protected double[][] targets;
    protected int nThreads = 2;
    protected double deformationWeight = 10;
    protected double volumeWeight = 0;
    protected double objectVolumeWeight = 0;
    protected double objectVolumeThreshold = 128;
    protected int nCtrls = 8, nAvgs;
    protected double matchEnergy, deformationEnergy, volumeEnergy, objectVolumeEnergy;
    protected long totalMillis = 0;
    protected int evaluations = 0;
    protected double[] spatialWeights = null;
    protected double[] vol0 = null;

    protected int N = 0;
    protected int[] avgDims;
    protected double[] avgData;
    protected double[] nrgData;

    public TrilinearFieldAveraging3DDP(int[] avgDims,
                                       int[][] tDims, double[][] targets,
                                       int nThreads,
                                       float deformationWeight,
                                       float volumeWeight, double[] vol0)
    {

        if (avgDims == null) {
            return;
        }

        if (tDims == null || targets == null) {
            return;
        }

        N = targets.length;
        this.tDims = tDims;
        this.targets = targets;

        for (int n = 0; n < N; n++) {
            if (tDims[n] == null || tDims[n].length != 3 || targets[n] == null ||
                targets[n].length != tDims[n][0] * tDims[n][1] * tDims[n][2]) {
                return;
            }
        }

        this.deformationWeight = deformationWeight;
        this.volumeWeight = volumeWeight;
        this.vol0 = vol0;

        this.nThreads = nThreads;
        this.avgDims = avgDims;
        nAvgs = avgDims[0] * avgDims[1] * avgDims[2];
        avgData = new double[nAvgs];
        nrgData = new double[nAvgs];
    }

    public double[] getFullDisplacedCoords(double[] coords)
    {
        int[] dims = avgDims;
        int nData = dims[0] * dims[1] * dims[2];
        double[] dispCoords = new double[3 * N * nData];
        for (int n = 0, nOff = 0, nOff2 = 0; n < N; n++, nOff += 24, nOff2 += 3 * nData) {
            for (int i = 0, l = 0; i < dims[2]; i++) {
                double t = i / (dims[2] - 1.f);
                for (int j = 0; j < dims[1]; j++) {
                    double u = j / (dims[1] - 1.f);
                    for (int k = 0; k < dims[0]; k++, l++) {
                        double v = k / (dims[0] - 1.f);
                        for (int m = 0; m < 3; m++) {
                            dispCoords[nOff2 + 3 * l + m] = ((1 - t) * ((1 - u) * ((1 - v) * coords[nOff + m] + v * coords[nOff + 3 + m]) +
                                u * ((1 - v) * coords[nOff + 6 + m] + v * coords[nOff + 9 + m])) +
                                t * ((1 - u) * ((1 - v) * coords[nOff + 12 + m] + v * coords[nOff + 15 + m]) +
                                u * ((1 - v) * coords[nOff + 18 + m] + v * coords[nOff + 21 + m])));
                        }
                    }
                }
            }
        }
        return dispCoords;
    }

    public double[] getDisplacedCoords(int n, double[] coords, int scale)
    {
        int[] dims = avgDims;
        int nData = dims[0] * dims[1] * dims[2];
        int nOff = 24 * n;

        double[] dispCoords = new double[3 * nData];
        for (int i = 0, l = 0; i < dims[2]; i++) {
            double t = i / (dims[2] - 1.f);
            for (int j = 0; j < dims[1]; j++) {
                double u = j / (dims[1] - 1.f);
                for (int k = 0; k < dims[0]; k++, l++) {
                    double v = k / (dims[0] - 1.f);
                    for (int m = 0; m < 3; m++) {
                        dispCoords[3 * l + m] = ((1 - t) * ((1 - u) * ((1 - v) * coords[nOff + m] + v * coords[nOff + 3 + m]) +
                            u * ((1 - v) * coords[nOff + 6 + m] + v * coords[nOff + 9 + m])) +
                            t * ((1 - u) * ((1 - v) * coords[nOff + 12 + m] + v * coords[nOff + 15 + m]) +
                            u * ((1 - v) * coords[nOff + 18 + m] + v * coords[nOff + 21 + m]))) * scale;
                    }
                }
            }
        }
        return dispCoords;
    }

    public double[] getAverageData()
    {
        return avgData;
    }

    public double[] getEnergyData()
    {
        return nrgData;
    }

    public double[] getTargetData(int n)
    {
        return targets[n];
    }

    public int[] getTargetDataDims(int n)
    {
        return tDims[n];
    }

    private class ComputeValGrad implements Runnable
    {

        private double[] coords;
        private double[] matchGradient;
        private int iThread = 0;
        private double matchValue;

        ComputeValGrad(double[] coords, int nThreads, int iThread)
        {
            this.coords = coords;
            this.iThread = iThread;
            matchGradient = new double[coords.length];
        }

        public double[] getMatchGradient()
        {
            return matchGradient;
        }

        public double getMatchValue()
        {
            return matchValue;
        }

        public void run()
        {
            double[] pcoord = new double[3];
            double[] splineVals = new double[N];
            double meanVal = 0;
            double[][] splineGrads = new double[N][3];

            matchValue = 0;
            for (int i = 0; i < coords.length; i++) {
                matchGradient[i] = 0;
            }

            int di = (avgDims[2] - 1) / nThreads;
            int istart = iThread * di + Math.min(iThread, (avgDims[2] - 1) % nThreads);
            int iend = (iThread + 1) * di + Math.min(iThread + 1, (avgDims[2] - 1) % nThreads);
            for (int i = istart; i < iend; i++) {
                double t = i / (avgDims[2] - 1.f);
                for (int j = 0; j < avgDims[1]; j++) {
                    double u = j / (avgDims[1] - 1.f);
                    for (int k = 0; k < avgDims[0]; k++) {
                        double v = k / (avgDims[0] - 1.f);

                        meanVal = 0;
                        for (int n = 0, nOff = 0; n < N; n++, nOff += 24) {

                            for (int m = 0; m < 3; m++) {
                                pcoord[m] = (1 - t) * ((1 - u) * ((1 - v) * coords[nOff + m] + v * coords[nOff + 3 + m]) +
                                    u * ((1 - v) * coords[nOff + 6 + m] + v * coords[nOff + 9 + m])) +
                                    t * ((1 - u) * ((1 - v) * coords[nOff + 12 + m] + v * coords[nOff + 15 + m]) +
                                    u * ((1 - v) * coords[nOff + 18 + m] + v * coords[nOff + 21 + m]));
                            }

                            splineVals[n] = SplineValueGradientDP.getSplineValueGradient(targets[n], tDims[n], pcoord, splineGrads[n]);
                            meanVal += splineVals[n];
                        }
                        meanVal /= (double) N;
                        avgData[i * avgDims[0] * avgDims[1] + j * avgDims[0] + k] = meanVal;

                        double nrg = 0;
                        for (int n = 0, nOff = 0; n < N; n++, nOff += 24) {
                            nrg += (splineVals[n] - meanVal) * (splineVals[n] - meanVal) / (double) N;
                            for (int m = 0; m < 3; m++) {
                                //double s = 2 * (splineVals[n] - meanVal) * splineGrads[n][m] / (double)(N);
                                double s = (splineVals[n] - meanVal) * splineGrads[n][m] / (double) (N);
                                matchGradient[nOff + m] += (1 - t) * (1 - u) * (1 - v) * s;
                                matchGradient[nOff + m + 3] += (1 - t) * (1 - u) * v * s;
                                matchGradient[nOff + m + 6] += (1 - t) * u * (1 - v) * s;
                                matchGradient[nOff + m + 9] += (1 - t) * u * v * s;
                                matchGradient[nOff + m + 12] += t * (1 - u) * (1 - v) * s;
                                matchGradient[nOff + m + 15] += t * (1 - u) * v * s;
                                matchGradient[nOff + m + 18] += t * u * (1 - v) * s;
                                matchGradient[nOff + m + 21] += t * u * v * s;
                            }
                        }
                        nrgData[i * avgDims[0] * avgDims[1] + j * avgDims[0] + k] = nrg;
                        matchValue += nrg;
                    }
                }
            }
        }
    }

    private double computeDeformationValGrad(double[] point, double[] gradient)
    {
        if (deformationWeight == 0) {
            return 0;
        }
        double[] w = new double[3];
        double sw;
        double dw = deformationWeight / (double) nCtrls;
        double[] smoothEnergy = new double[N];

        for (int n = 0, cnOff = 0; n < N; n++, cnOff += 24) {
            smoothEnergy[n] = 0;

            for (int i = 0; i < 24; i += 12) {
                double wnorm = 0;
                /*          -======+
                 *        //|    //|
                 *      + ======-  |
                 *      |   |   |  |
                 *      |   -===|==+
                 *      | //    |//
                 *      +=======-
                 */
                for (int j = 0; j < 3; j++) {
                    w[j] = point[cnOff + i + 9 + j] - point[cnOff + i + 6 + j] - point[cnOff + i + 3 + j] + point[cnOff + i + j];
                    wnorm += w[j] * w[j];
                }
                sw = 2;
                if (spatialWeights != null) {
                    sw = 2 * spatialWeights[i / 3];
                }
                smoothEnergy[n] += sw * wnorm;
                sw *= 2;
                for (int j = 0; j < 3; j++) {
                    gradient[cnOff + i + 9 + j] += sw * w[j] * dw;
                    gradient[cnOff + i + 6 + j] -= sw * w[j] * dw;
                    gradient[cnOff + i + 3 + j] -= sw * w[j] * dw;
                    gradient[cnOff + i + j] += sw * w[j] * dw;
                }
            }
            /*         -======+
             *        / ||   / ||
             *      - ======+  ||
             *      ||  ||  || ||
             *      ||  +===||=-
             *      ||/     ||/
             *      +=======-
             */
            for (int i = 0; i < 12; i += 6) {
                double wnorm = 0;
                for (int j = 0; j < 3; j++) {
                    w[j] = point[cnOff + i + 15 + j] - point[cnOff + i + 12 + j] - point[cnOff + i + 3 + j] + point[cnOff + i + j];
                    wnorm += w[j] * w[j];
                }
                sw = 2;
                if (spatialWeights != null) {
                    sw = 2 * spatialWeights[i / 3];
                }
                smoothEnergy[n] += sw * wnorm;
                sw *= 2;
                for (int j = 0; j < 3; j++) {
                    gradient[cnOff + i + 15 + j] += sw * w[j] * dw;
                    gradient[cnOff + i + 12 + j] -= sw * w[j] * dw;
                    gradient[cnOff + i + 3 + j] -= sw * w[j] * dw;
                    gradient[cnOff + i + j] += sw * w[j] * dw;
                }
                /*         + ---- +
                 *       //||    //||
                 *      - ----- -  ||
                 *      || ||   || ||
                 *      ||  - --|| -
                 *      ||//    ||//
                 *      + ----- +
                 */
            }
            for (int i = 0; i < 6; i += 3) {
                double wnorm = 0;
                for (int j = 0; j < 3; j++) {
                    w[j] = point[cnOff + i + 18 + j] - point[cnOff + i + 12 + j] - point[cnOff + i + 6 + j] + point[cnOff + i + j];
                    wnorm += w[j] * w[j];
                }
                sw = 2;
                if (spatialWeights != null) {
                    sw = 2 * spatialWeights[i / 3];
                }
                smoothEnergy[n] += sw * wnorm;
                sw *= 2;
                for (int j = 0; j < 3; j++) {
                    gradient[cnOff + i + 18 + j] += sw * w[j] * dw;
                    gradient[cnOff + i + 12 + j] -= sw * w[j] * dw;
                    gradient[cnOff + i + 6 + j] -= sw * w[j] * dw;
                    gradient[cnOff + i + j] += sw * w[j] * dw;
                }
            }

            smoothEnergy[n] *= dw;
        }

        double outSmoothEnergy = 0;
        for (int n = 0; n < N; n++) {
            outSmoothEnergy += smoothEnergy[n];
        }
        return outSmoothEnergy;
    }

    private double computeVolumeDeformationValGrad(double[] point, double[] gradient)
    {
        if (volumeWeight == 0) {
            return 0;
        }
        double[] en = new double[N];
        double v;
        double[] x70 = new double[3];
        double[] x10 = new double[3];
        double[] x35 = new double[3];
        double[] x40 = new double[3];
        double[] x56 = new double[3];
        double[] x20 = new double[3];
        double[] x63 = new double[3];
        double[] dA0, dA1, dA2, dB0, dB1, dB2, dC0, dC1, dC2;
        double c;

        for (int n = 0, nOff = 0; n < N; n++, nOff += 3 * nCtrls) {
            v = computeTrilinearVolume(n, point);
            en[n] = (v - vol0[n]) * (v - vol0[n]) / nAvgs;

            //grad
            for (int i = 0; i < 3; i++) {
                x70[i] = point[nOff + 21 + i] - point[nOff + i]; //(x7-x0)
                x10[i] = point[nOff + 3 + i] - point[nOff + i]; //(x1-x0)
                x35[i] = point[nOff + 9 + i] - point[nOff + 15 + i]; //(x3-x5)
                x40[i] = point[nOff + 12 + i] - point[nOff + i]; //(x4-x0)
                x56[i] = point[nOff + 15 + i] - point[nOff + 18 + i]; //(x5-x6)
                x20[i] = point[nOff + 6 + i] - point[nOff + i]; //(x2-x0)
                x63[i] = point[nOff + 18 + i] - point[nOff + 9 + i]; //(x6-x3)
            }

            dA0 = dTripleProduct(x70, x10, x35, 0);
            dA1 = dTripleProduct(x70, x10, x35, 1);
            dA2 = dTripleProduct(x70, x10, x35, 2);

            dB0 = dTripleProduct(x70, x40, x56, 0);
            dB1 = dTripleProduct(x70, x40, x56, 1);
            dB2 = dTripleProduct(x70, x40, x56, 2);

            dC0 = dTripleProduct(x70, x20, x63, 0);
            dC1 = dTripleProduct(x70, x20, x63, 1);
            dC2 = dTripleProduct(x70, x20, x63, 2);

            c = volumeWeight * 2 * (v - vol0[n]) / (6.0 * nAvgs);
            for (int m = 0; m < 3; m++) {
                gradient[nOff + 3 * 0 + m] += c * (-dA0[m] - dA1[m] - dB0[m] - dB1[m] - dC0[m] - dC1[m]);
                gradient[nOff + 3 * 1 + m] += c * (dA1[m]);
                gradient[nOff + 3 * 2 + m] += c * (dC1[m]);
                gradient[nOff + 3 * 3 + m] += c * (dA2[m] - dC2[m]);
                gradient[nOff + 3 * 4 + m] += c * (dB1[m]);
                gradient[nOff + 3 * 5 + m] += c * (-dA2[m] + dB2[m]);
                gradient[nOff + 3 * 6 + m] += c * (-dB2[m] + dC2[m]);
                gradient[nOff + 3 * 7 + m] += c * (dA0[m] + dB0[m] + dC0[m]);
            }
        }

        double outEn = 0;
        for (int n = 0; n < N; n++) {
            outEn += en[n];
        }
        return volumeWeight * outEn;
    }

    private double computeMatchValGrad(double[] point, double[] gradient)
    {
        double en = 0;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = 0;
        }

        ComputeValGrad[] computeThreads = new ComputeValGrad[nThreads];
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            computeThreads[iThread] = new ComputeValGrad(point, nThreads, iThread);
            workThreads[iThread] = new Thread(computeThreads[iThread]);
            workThreads[iThread].start();
        }
        for (int iThread = 0; iThread < workThreads.length; iThread++) {
            try {
                workThreads[iThread].join();
            } catch (Exception e) {
            }
        }
        for (int iThread = 0; iThread < nThreads; iThread++) {
            en += computeThreads[iThread].getMatchValue();
            double[] thrGr = computeThreads[iThread].getMatchGradient();
            for (int i = 0; i < thrGr.length; i++) {
                gradient[i] += thrGr[i];
            }
        }

        en /= nAvgs;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] /= nAvgs;
        }

        return en;
    }

    public double computeValGrad(double[] point, double[] gradient)
    {
        long t0 = System.currentTimeMillis();
        matchEnergy = 0;
        deformationEnergy = 0;
        volumeEnergy = 0;
        objectVolumeEnergy = 0;

        matchEnergy = computeMatchValGrad(point, gradient);
        deformationEnergy = computeDeformationValGrad(point, gradient);
        volumeEnergy = computeVolumeDeformationValGrad(point, gradient);

        //        System.out.printf("  matchEnergy = %8.3f   deformationEnergy = %8.3f    volumeEnergy = %8.3f     total energy = %8.3f%n",
        //                         matchEnergy, deformationEnergy, volumeEnergy, matchEnergy + deformationEnergy + volumeEnergy);
        evaluations += 1;
        totalMillis += System.currentTimeMillis() - t0;

        return matchEnergy + deformationEnergy + volumeEnergy;
    }

    public void setDeformationWeight(double deformationWeight)
    {
        this.deformationWeight = deformationWeight;
    }

    public void setVolumeWeight(double volumeWeight)
    {
        this.volumeWeight = volumeWeight;
    }

    public void setNThreads(int nThreads)
    {
        this.nThreads = nThreads;
    }

    public double getDeformationEnergy()
    {
        return deformationEnergy;
    }

    public double getVolumeEnergy()
    {
        return volumeEnergy;
    }

    public double getMatchEnergy()
    {
        return matchEnergy;
    }

    public int getEvaluations()
    {
        return evaluations;
    }

    public long getTotalMillis()
    {
        return totalMillis;
    }

    public void resetTimer()
    {
        totalMillis = 0;
        evaluations = 0;
    }

    public static double computeTrilinearVolume(int n, double[] coords)
    {
        double[] tmp = new double[24];
        int nOff = n * 24;
        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = coords[nOff + i];
        }
        return GeometricVolumes.hexahedronVolume(tmp);
    }

    private double[] dTripleProduct(double[] a, double[] b, double[] c, int k)
    {
        double[] out = new double[3];
        switch (k) {
            case 0:
                out[0] = b[1] * c[2] - c[1] * b[2];
                out[1] = b[2] * c[0] - c[2] * b[0];
                out[2] = b[0] * c[1] - c[0] * b[1];
                break;
            case 1:
                out[0] = c[1] * a[2] - a[1] * c[2];
                out[1] = c[2] * a[0] - a[2] * c[0];
                out[2] = c[0] * a[1] - a[0] * c[1];
                break;
            case 2:
                out[0] = a[1] * b[2] - b[1] * a[2];
                out[1] = a[2] * b[0] - b[2] * a[0];
                out[2] = a[0] * b[1] - b[0] * a[1];
                break;
        }
        return out;
    }

    public String[] getVarNames()
    {
        return new String[]{"matchEnergy", "deformationEnergy", "volumeEnergy"};
    }

    public double[] getVariables()
    {
        return new double[]{matchEnergy, deformationEnergy, volumeEnergy};
    }

}
