/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.numeric.fieldAveraging;

import pl.edu.icm.visnow.lib.utils.numeric.splines.SplineValueGradientDP;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class DilinearFieldAveraging2DDP implements FieldAveragingDP
{

    protected int[][] tDims;
    protected double[][] targets;
    protected int nThreads = 2;
    protected double deformationWeight = 10;
    protected double volumeWeight = 0;
    protected double objectVolumeWeight = 0;
    protected double objectVolumeThreshold = 128;
    protected int nCtrls = 4, nAvgs;
    protected double matchEnergy, deformationEnergy, volumeEnergy, objectVolumeEnergy;
    protected long totalMillis = 0;
    protected int evaluations = 0;
    protected double[] spatialWeights = null;
    protected double[] vol0 = null;
    protected int N = 0;
    protected int[] avgDims;
    protected double[] avgData;
    protected double[] nrgData;

    public DilinearFieldAveraging2DDP(int[] avgDims,
                                      int[][] tDims, double[][] targets,
                                      int nThreads,
                                      float deformationWeight,
                                      float volumeWeight, double[] vol0)
    {

        if (avgDims == null) {
            return;
        }

        if (tDims == null || targets == null) {
            return;
        }

        N = targets.length;
        this.tDims = tDims;
        this.targets = targets;

        for (int n = 0; n < N; n++) {
            if (tDims[n] == null || tDims[n].length != 2 || targets[n] == null ||
                targets[n].length != tDims[n][0] * tDims[n][1]) {
                return;
            }
        }

        this.deformationWeight = deformationWeight;
        this.volumeWeight = volumeWeight;
        this.vol0 = vol0;

        this.nThreads = nThreads;
        this.avgDims = avgDims;
        nAvgs = avgDims[0] * avgDims[1];
        avgData = new double[nAvgs];
        nrgData = new double[nAvgs];
    }

    public double[] getFullDisplacedCoords(double[] coords)
    {
        int[] dims = avgDims;
        int nData = dims[0] * dims[1];
        double[] dispCoords = new double[2 * N * nData];
        for (int n = 0, nOff = 0, nOff2 = 0; n < N; n++, nOff += 8, nOff2 += 2 * nData) {
            for (int j = 0, l = 0; j < dims[1]; j++) {
                double u = j / (dims[1] - 1.f);
                for (int k = 0; k < dims[0]; k++, l++) {
                    double v = k / (dims[0] - 1.f);
                    for (int m = 0; m < 2; m++) {
                        dispCoords[nOff2 + 2 * l + m] = ((1 - u) * ((1 - v) * coords[nOff + m] + v * coords[nOff + 2 + m]) +
                            u * ((1 - v) * coords[nOff + 4 + m] + v * coords[nOff + 6 + m]));
                    }
                }
            }
        }
        return dispCoords;
    }

    public double[] getDisplacedCoords(int n, double[] coords, int scale)
    {
        int[] dims = avgDims;
        int nData = dims[0] * dims[1];
        int nOff = 8 * n;

        double[] dispCoords = new double[2 * nData];
        for (int j = 0, l = 0; j < dims[1]; j++) {
            double u = j / (dims[1] - 1.f);
            for (int k = 0; k < dims[0]; k++, l++) {
                double v = k / (dims[0] - 1.f);
                for (int m = 0; m < 2; m++) {
                    dispCoords[2 * l + m] = ((1 - u) * ((1 - v) * coords[nOff + m] + v * coords[nOff + 2 + m]) +
                        u * ((1 - v) * coords[nOff + 4 + m] + v * coords[nOff + 6 + m])) * scale;
                }
            }
        }
        return dispCoords;
    }

    public double[] getAverageData()
    {
        return avgData;
    }

    public double[] getEnergyData()
    {
        return nrgData;
    }

    public double[] getTargetData(int n)
    {
        return targets[n];
    }

    public int[] getTargetDataDims(int n)
    {
        return tDims[n];
    }

    private class ComputeValGrad implements Runnable
    {

        private double[] coords;
        private double[] matchGradient;
        private int iThread = 0;
        private double matchValue;

        ComputeValGrad(double[] coords, int nThreads, int iThread)
        {
            this.coords = coords;
            this.iThread = iThread;
            matchGradient = new double[coords.length];
        }

        public double[] getMatchGradient()
        {
            return matchGradient;
        }

        public double getMatchValue()
        {
            return matchValue;
        }

        public void run()
        {
            double[] pcoord = new double[2];
            double[] splineVals = new double[N];
            double meanVal = 0;
            double[][] splineGrads = new double[N][2];

            matchValue = 0;
            for (int i = 0; i < coords.length; i++) {
                matchGradient[i] = 0;
            }

            int dj = (avgDims[1] - 1) / nThreads;
            int jstart = iThread * dj + Math.min(iThread, (avgDims[1] - 1) % nThreads);
            int jend = (iThread + 1) * dj + Math.min(iThread + 1, (avgDims[1] - 1) % nThreads);
            for (int j = jstart; j < jend; j++) {
                double u = j / (avgDims[1] - 1.f);
                for (int k = 0; k < avgDims[0]; k++) {
                    double v = k / (avgDims[0] - 1.f);

                    meanVal = 0;
                    for (int n = 0, nOff = 0; n < N; n++, nOff += 8) {

                        for (int m = 0; m < 2; m++) {
                            pcoord[m] = (1 - u) * ((1 - v) * coords[nOff + m] + v * coords[nOff + 2 + m]) +
                                u * ((1 - v) * coords[nOff + 4 + m] + v * coords[nOff + 6 + m]);
                        }

                        splineVals[n] = SplineValueGradientDP.getSplineValueGradient2D(targets[n], tDims[n], pcoord, splineGrads[n]);
                        meanVal += splineVals[n];
                    }
                    meanVal /= (double) N;
                    avgData[j * avgDims[0] + k] = meanVal;

                    double nrg = 0;
                    for (int n = 0, nOff = 0; n < N; n++, nOff += 8) {
                        nrg += (splineVals[n] - meanVal) * (splineVals[n] - meanVal) / (double) N;
                        for (int m = 0; m < 2; m++) {
                            double s = (splineVals[n] - meanVal) * splineGrads[n][m] / (double) (N);
                            matchGradient[nOff + m] += (1 - u) * (1 - v) * s;
                            matchGradient[nOff + m + 2] += (1 - u) * v * s;
                            matchGradient[nOff + m + 4] += u * (1 - v) * s;
                            matchGradient[nOff + m + 6] += u * v * s;
                        }
                    }
                    nrgData[j * avgDims[0] + k] = nrg;
                    matchValue += nrg;
                }
            }
        }
    }

    private double computeDeformationValGrad(double[] point, double[] gradient)
    {
        if (deformationWeight == 0) {
            return 0;
        }
        double[] w = new double[2];
        double sw;
        double dw = deformationWeight / (double) nCtrls;
        double[] smoothEnergy = new double[N];

        for (int n = 0, cnOff = 0; n < N; n++, cnOff += 8) {
            smoothEnergy[n] = 0;

            double wnorm = 0;
            /* 
             * 
             *      -=======+
             *      |       |
             *      |       |
             *      |       |
             *      +=======-
             */
            for (int j = 0; j < 2; j++) {
                w[j] = point[cnOff + 6 + j] - point[cnOff + 4 + j] - point[cnOff + 2 + j] + point[cnOff + j];
                wnorm += w[j] * w[j];
            }
            sw = 2;
            if (spatialWeights != null) {
                sw = 2 * spatialWeights[0];
            }
            smoothEnergy[n] += sw * wnorm;
            sw *= 2;
            for (int j = 0; j < 2; j++) {
                gradient[cnOff + 6 + j] += sw * w[j] * dw;
                gradient[cnOff + 4 + j] -= sw * w[j] * dw;
                gradient[cnOff + 2 + j] -= sw * w[j] * dw;
                gradient[cnOff + j] += sw * w[j] * dw;
            }

            smoothEnergy[n] *= dw;
        }

        double outSmoothEnergy = 0;
        for (int n = 0; n < N; n++) {
            outSmoothEnergy += smoothEnergy[n];
        }
        return outSmoothEnergy;
    }

    private double computeVolumeDeformationValGrad(double[] point, double[] gradient)
    {
        if (volumeWeight == 0) {
            return 0;
        }
        double[] en = new double[N];
        double det, v;
        double c;

        for (int n = 0, nOff = 0; n < N; n++, nOff += 2 * nCtrls) {
            det = 0.5 * ((point[nOff + 6] - point[nOff + 0]) * (point[nOff + 5] - point[nOff + 3]) - (point[nOff + 7] - point[nOff + 1]) * (point[nOff + 4] - point[nOff + 2]));
            v = (det >= 0 ? det : (-det));
            en[n] = (v - vol0[n]) * (v - vol0[n]) / nAvgs;

            //grad
            c = volumeWeight * (v - vol0[n]) / nAvgs;
            c = (det >= 0 ? c : (-c));
            gradient[nOff] += c * (point[nOff + 3] - point[nOff + 5]);
            gradient[nOff + 1] += c * (point[nOff + 4] - point[nOff + 2]);
            gradient[nOff + 2] += c * (point[nOff + 7] - point[nOff + 1]);
            gradient[nOff + 3] += c * (point[nOff] - point[nOff + 6]);
            gradient[nOff + 4] += c * (point[nOff + 1] - point[nOff + 7]);
            gradient[nOff + 5] += c * (point[nOff + 6] - point[nOff]);
            gradient[nOff + 6] += c * (point[nOff + 5] - point[nOff + 3]);
            gradient[nOff + 7] += c * (point[nOff + 2] - point[nOff + 4]);
        }

        double outEn = 0;
        for (int n = 0; n < N; n++) {
            outEn += en[n];
        }
        return volumeWeight * outEn;
    }

    private double computeMatchValGrad(double[] point, double[] gradient)
    {
        double en = 0;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = 0;
        }

        ComputeValGrad[] computeThreads = new ComputeValGrad[nThreads];
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            computeThreads[iThread] = new ComputeValGrad(point, nThreads, iThread);
            workThreads[iThread] = new Thread(computeThreads[iThread]);
            workThreads[iThread].start();
        }
        for (int iThread = 0; iThread < workThreads.length; iThread++) {
            try {
                workThreads[iThread].join();
            } catch (Exception e) {
            }
        }
        for (int iThread = 0; iThread < nThreads; iThread++) {
            en += computeThreads[iThread].getMatchValue();
            double[] thrGr = computeThreads[iThread].getMatchGradient();
            for (int i = 0; i < thrGr.length; i++) {
                gradient[i] += thrGr[i];
            }
        }

        en /= nAvgs;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] /= nAvgs;
        }

        return en;
    }

    public double computeValGrad(double[] point, double[] gradient)
    {
        long t0 = System.currentTimeMillis();
        matchEnergy = 0;
        deformationEnergy = 0;
        volumeEnergy = 0;
        objectVolumeEnergy = 0;

        matchEnergy = computeMatchValGrad(point, gradient);
        deformationEnergy = computeDeformationValGrad(point, gradient);
        volumeEnergy = computeVolumeDeformationValGrad(point, gradient);
        //        System.out.printf("  matchEnergy = %8.3f   deformationEnergy = %8.3f    volumeEnergy = %8.3f     total energy = %8.3f%n",
        //                         matchEnergy, deformationEnergy, volumeEnergy, matchEnergy + deformationEnergy + volumeEnergy);

        //        //WITH TEST
        //        
        //        double[] mGradient = new double[gradient.length];
        //        double[] dGradient = new double[gradient.length];
        //        double[] vGradient = new double[gradient.length];
        //        double[] dummy = new double[gradient.length];
        //        double[] numGrad = new double[gradient.length];
        //        for (int i = 0; i < gradient.length; i++) {
        //            mGradient[i] = 0;
        //            dGradient[i] = 0;
        //            vGradient[i] = 0;
        //            numGrad[i] = 0;
        //        }
        //
        //        double[] pointUp = new double[point.length];
        //        double[] pointLo = new double[point.length];
        //        double d = 0.001;
        //        System.arraycopy(point, 0, pointUp, 0, point.length);
        //        System.arraycopy(point, 0, pointLo, 0, point.length);
        //
        //
        //        matchEnergy = computeMatchValGrad(point, mGradient);
        //
        //        for (int i = 0; i < gradient.length; i++) {
        //            numGrad[i] = 0;
        //        }
        //        for (int i = 0; i < point.length; i++) {
        //            pointLo[i] -= d;
        //            pointUp[i] += d;
        //            double enLo = computeMatchValGrad(pointLo, dummy);
        //            double enUp = computeMatchValGrad(pointUp, dummy);
        //            numGrad[i] = (enUp - enLo) / (2 * d);
        //            pointLo[i] = point[i];
        //            pointUp[i] = point[i];
        //        }
        //
        //        double eps = 0.001;
        //        boolean ok = true;
        //        for (int n = 0; n < N; n++) {
        //            for (int l = 0; l < numGrad.length; l++) {
        //                if (Math.abs(numGrad[l] - mGradient[l]) > eps) {
        //                    ok = false;
        //                    System.out.println("ERROR: numGrad["+ l + "] = " + numGrad[l] + " mGrad[" + l + "]=" + mGradient[l]);
        //                    //break;
        //                }
        //            }
        //        }
        //        System.out.println("match gradient test: "+(ok?"OK":"ERROR"));
        //
        //
        //        
        //        
        //        deformationEnergy = computeDeformationValGrad(point, dGradient);
        //        //TEST
        //        for (int i = 0; i < gradient.length; i++) {
        //            numGrad[i] = 0;
        //        }
        //        for (int i = 0; i < point.length; i++) {
        //            pointLo[i] -= d;
        //            pointUp[i] += d;
        //            double enLo = computeDeformationValGrad(pointLo, dummy);
        //            double enUp = computeDeformationValGrad(pointUp, dummy);
        //            numGrad[i] = (enUp - enLo) / (2 * d);
        //            pointLo[i] = point[i];
        //            pointUp[i] = point[i];
        //        }
        //
        //        ok = true;
        //        for (int n = 0; n < N; n++) {
        //            for (int l = 0; l < numGrad.length; l++) {
        //                if (Math.abs(numGrad[l] - dGradient[l]) > eps) {
        //                    ok = false;
        //                    System.out.println("ERROR: numGrad["+ l + "] = " + numGrad[l] + " dGrad[" + l + "]=" + dGradient[l]);
        //                    //break;
        //                }
        //            }
        //        }
        //        System.out.println("deformation gradient test: "+(ok?"OK":"ERROR"));
        //
        //
        //        
        //        
        //        volumeEnergy = computeVolumeDeformationValGrad(point, vGradient);
        //        //TEST
        //        for (int i = 0; i < gradient.length; i++) {
        //            numGrad[i] = 0;
        //        }
        //        for (int i = 0; i < point.length; i++) {
        //            pointLo[i] -= d;
        //            pointUp[i] += d;
        //            double enLo = computeVolumeDeformationValGrad(pointLo, dummy);
        //            double enUp = computeVolumeDeformationValGrad(pointUp, dummy);
        //            numGrad[i] = (enUp - enLo) / (2 * d);
        //            pointLo[i] = point[i];
        //            pointUp[i] = point[i];
        //        }
        //
        //        ok = true;
        //        for (int n = 0; n < N; n++) {
        //            for (int l = 0; l < numGrad.length; l++) {
        //                if (Math.abs(numGrad[l] - vGradient[l]) > eps) {
        //                    ok = false;
        //                    System.out.println("ERROR: numGrad["+ l + "] = " + numGrad[l] + " vGrad[" + l + "]=" + vGradient[l]);
        //                    //break;
        //                }
        //            }
        //        }
        //        System.out.println("volume gradient test: "+(ok?"OK":"ERROR"));
        //
        //
        //
        //        for (int i = 0; i < gradient.length; i++) {
        //            gradient[i] = mGradient[i] + dGradient[i] + vGradient[i];
        //        }
        evaluations += 1;
        totalMillis += System.currentTimeMillis() - t0;

        return matchEnergy + deformationEnergy + volumeEnergy;
    }

    public void setDeformationWeight(double deformationWeight)
    {
        this.deformationWeight = deformationWeight;
    }

    public void setVolumeWeight(double volumeWeight)
    {
        this.volumeWeight = volumeWeight;
    }

    public void setNThreads(int nThreads)
    {
        this.nThreads = nThreads;
    }

    public double getDeformationEnergy()
    {
        return deformationEnergy;
    }

    public double getVolumeEnergy()
    {
        return volumeEnergy;
    }

    public double getMatchEnergy()
    {
        return matchEnergy;
    }

    public int getEvaluations()
    {
        return evaluations;
    }

    public long getTotalMillis()
    {
        return totalMillis;
    }

    public void resetTimer()
    {
        totalMillis = 0;
        evaluations = 0;
    }

    public static double computeDilinearVolume(int n, double[] coords)
    {
        int nOff = n * 8;
        double det = 0.5 * ((coords[nOff + 6] - coords[nOff + 0]) * (coords[nOff + 5] - coords[nOff + 3]) - (coords[nOff + 7] - coords[nOff + 1]) * (coords[nOff + 4] - coords[nOff + 2]));
        return (det >= 0 ? det : (-det));
    }

    public String[] getVarNames()
    {
        return new String[]{"matchEnergy", "deformationEnergy", "volumeEnergy"};
    }

    public double[] getVariables()
    {
        return new double[]{matchEnergy, deformationEnergy, volumeEnergy};
    }
}
