/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.numeric.fieldFitting;

import pl.edu.icm.jscic.dataarrays.DataArray;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class SplineFieldFitting2D implements FieldFitting
{

    protected int[] ctrlDims;
    protected int[] srcDims;
    protected float[] ctrlCoords;
    protected int density;
    protected byte[] source;
    protected int[] tDims;
    protected DataArray tArr;
    protected float[] gradient;
    protected float[][] coeffs = null;
    protected int nThreads = 2;

    /**
     *
     * @param ctrlDims   - dimensions of control points grid - ctrlDims.length must be 3
     * @param ctrlCoords - target coordinates for control points in target index space (ctrlCoords.length = 3*ctrlDims[0]*ctrlDims[1]*ctrlDims[2]) - independent
     *                   variables
     * @param density    - density of source grid refinement w/r to the control points grid sourceDims[i] = density*(ctrlDims[i]-1)+1
     * @param source     - scalar source data array
     * @param tDims      - dimensions of target grid - tDims.length must be 3
     * @param tArr       - target scalar data array
     * @param gradient   - array for storing gradient - must be preallocated of length ctrlCoords.length.
     */
    public SplineFieldFitting2D(int[] ctrlDims, float[] ctrlCoords, int[] srcDims, byte[] source, int[] tDims, DataArray tArr, float[] gradient)
    {
        if (ctrlDims == null || ctrlDims.length != 2 || ctrlCoords == null || ctrlCoords.length != 2 * ctrlDims[0] * ctrlDims[1])
            return;
        if (source == null || source.length != srcDims[0] * srcDims[1] * srcDims[2])
            return;
        if (tDims == null || tDims.length != 2 ||
            tArr == null || !tArr.isNumeric() || tArr.getNElements() != tDims[0] * tDims[1] || tArr.getVectorLength() != 1)
            return;
        if (gradient == null || gradient.length != ctrlCoords.length)
            return;
        if (ceil((srcDims[0] - 1.) / (ctrlDims[0] - 1.)) != ceil((srcDims[1] - 1.) / (ctrlDims[1] - 1.)))
            return;
        density = (int) (Math.ceil((srcDims[0] - 1.) / (ctrlDims[0] - 1.)));
        this.ctrlDims = ctrlDims;
        this.ctrlCoords = ctrlCoords;
        this.srcDims = srcDims;
        this.source = source;
        this.tDims = tDims;
        this.tArr = tArr;
        this.gradient = gradient;
        coeffs = new float[4][density + 1];
        for (int j = 0; j <= density; j++) {
            float x = (float) j / density;
            coeffs[3][j] = (.5f * x - .5f) * x * x;
            coeffs[2][j] = ((-1.5f * x + 2.f) * x + .5f) * x;
            coeffs[1][j] = (1.5f * x - 2.5f) * x * x + 1.f;
            coeffs[0][j] = ((-.5f * x + 1.0f) * x - .5f) * x;
        }
    }

    public double getDeformationEnergy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public float[] getDeformationGradient()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public float[] getDisplacedCoords(float[] coords)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getEvaluations()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public double getMatchEnergy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public long getTotalMillis()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public double getVolumeEnergy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void resetTimer()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setDeformationWeight(float deformationWeight)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setVolumeWeight(float volumeWeight)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private class Compute implements Runnable
    {

        private float[] ctrlCoords;
        private float[] gradient;
        private int nThreads = 2, iThread = 0;
        private double value;

        Compute(float[] ctrlCoords, int nThreads, int iThread)
        {
            this.ctrlCoords = ctrlCoords;
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.gradient = new float[ctrlCoords.length];
        }

        public float[] getGradient()
        {
            return gradient;
        }

        public double getValue()
        {
            return value;
        }

        public void run()
        {
            float[] pcoord = new float[2];
            float[] pvect = new float[2];

            value = 0;
            for (int i = 0; i < gradient.length; i++)
                gradient[i] = 0;

            for (int j = iThread; j < ctrlDims[1] - 1; j += nThreads)
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int qx = density;
                    if (j == ctrlDims[1] - 2)
                        qx = density + 1;
                    int rx = density;
                    if (k == ctrlDims[0] - 2)
                        rx = density + 1;
                    for (int q = 0; q < qx; q++)
                        for (int r = 0, l = (density * j + q) * srcDims[0] + density * k; r < rx; r++, l++) {
                            for (int m = 0; m < pvect.length; m++)
                                pvect[m] = pcoord[m] = 0;
                            for (int jj = 0; jj < 4; jj++) {
                                int jx = j + jj - 1;
                                if (jx < 0)
                                    jx = 0;
                                if (jx >= ctrlDims[1])
                                    jx = ctrlDims[1] - 1;
                                int isrc = jx * ctrlDims[0];
                                for (int kk = 0; kk < 4; kk++)
                                    for (int v = 0; v < 3; v++) {
                                        int kx = k + kk - 1;
                                        if (kx < 0)
                                            kx = 0;
                                        if (kx >= ctrlDims[0])
                                            kx = ctrlDims[0] - 1;
                                        pcoord[v] += ctrlCoords[3 * (isrc + kx) + v] * coeffs[jj][q] * coeffs[kk][r];
                                    }

                            }

                            float[] tGradient = new float[3];
                            float tV = getSplineValueGradient(pcoord, tGradient);
                            float sV = (float) (0xFF & source[l]);
                            value += (tV - sV) * (tV - sV);
                            for (int jj = 0; jj < 4; jj++) {
                                int jx = min(max(j + jj - 1, 0), ctrlDims[1] - 1);
                                int isrc = jx * ctrlDims[0];
                                for (int kk = 0; kk < 4; kk++)
                                    for (int v = 0; v < 3; v++) {
                                        int kx = min(max(k + kk - 1, 0), ctrlDims[0] - 1);
                                        gradient[3 * (isrc + kx) + v] += 2 * (tV - sV) * coeffs[jj][q] * coeffs[kk][r] * tGradient[v];
                                    }
                            }
                        }
                }
        }
    }

    float getSplineValueGradient(float[] coords, float[] gradient)
    {
        float u = min(max(coords[0], 0), tDims[0] - 1), v = min(max(coords[1], 0), tDims[1] - 1);
        float val = 0;
        for (int i = 0; i < gradient.length; i++)
            gradient[i] = 0;
        int off1 = tDims[0];
        int i = (int) u;
        u -= i;
        int j = (int) v;
        v -= j;
        int m = j * tDims[0] + i;
        int il = -1;
        if (i == 0)
            il = 0;
        int jl = -1;
        if (j == 0)
            jl = 0;
        int iu = 3;
        if (i + iu > tDims[0])
            iu = tDims[0] - i;
        int ju = 3;
        if (j + ju > tDims[1])
            ju = tDims[1] - j;
        float vu = 0, vv = 0;
        float du = 0, dv = 0;
        for (int j1 = jl; j1 < ju; j1++) {
            switch (j1) {
                case -1:
                    vv = ((-.5f * v + 1.0f) * v - .5f) * v;
                    dv = (-1.5f * v + 2.0f) * v - .5f;
                    break;
                case 0:
                    vv = (1.5f * v - 2.5f) * v * v + 1.f;
                    dv = (4.5f * v - 5.0f) * v;
                    break;
                case 1:
                    vv = ((-1.5f * v + 2.f) * v + .5f) * v;
                    dv = (-4.5f * v + 4.f) * v + .5f;
                    break;
                case 2:
                    vv = (.5f * v - .5f) * v * v;
                    dv = (1.5f * v - 1.f) * v;
                    break;
            }
            for (int i1 = il; i1 < iu; i1++) {
                switch (i1) {
                    case -1:
                        vu = ((-.5f * u + 1.0f) * u - .5f) * u;
                        du = (-1.5f * u + 2.0f) * u - .5f;
                        break;
                    case 0:
                        vu = (1.5f * u - 2.5f) * u * u + 1.f;
                        du = (4.5f * u - 5.0f) * u;
                        break;
                    case 1:
                        vu = ((-1.5f * u + 2.f) * u + .5f) * u;
                        du = (-4.5f * u + 4.f) * u + .5f;
                        break;
                    case 2:
                        vu = (.5f * u - .5f) * u * u;
                        du = (1.5f * u - 1.f) * u;
                        break;
                }
                float t = tArr.getFloatElement(m + j1 * off1 + i1)[0];
                val += t * vu * vv;
                gradient[0] += t * du * vv;
                gradient[1] += t * vu * dv;

            }

        }
        return val;
    }

    /**
     * computes energy (sum over source points of (t(F(x)-s(x))^2)) and energy gradient with respect to control points
     * <p>
     * @param ctrlCoords - target coordinates for control points in target index space
     *                   (ctrlCoords.length = 3*ctrlDims[0]*ctrlDims[1]*ctrlDims[2]) - independent variables
     * @param gradient   - array for storing gradient - must be preallocated of length ctrlCoords.length.
     * <p>
     * @return energy value (if negative - wrong input data)
     */
    public double computeValGrad(float[] ctrlCoords, float[] gradient)
    {
        double energy = 0;
        Compute[] computeThreads = new Compute[nThreads];
        Thread[] workThreads = new Thread[nThreads];
        for (int i = 0; i < gradient.length; i++)
            gradient[i] = 0;
        for (int iThread = 0; iThread < nThreads; iThread++) {
            computeThreads[iThread] = new Compute(ctrlCoords, nThreads, iThread);
            workThreads[iThread] = new Thread(computeThreads[iThread]);
            workThreads[iThread].start();
        }
        for (int iThread = 0; iThread < workThreads.length; iThread++)
            try {
                workThreads[iThread].join();
            } catch (Exception e) {
            }
        for (int iThread = 0; iThread < nThreads; iThread++) {
            energy += computeThreads[iThread].getValue();
            float[] thrGr = computeThreads[iThread].getGradient();
            for (int i = 0; i < thrGr.length; i++)
                gradient[i] += thrGr[i];
        }
        return energy;
    }

    public void setNThreads(int nThreads)
    {
        this.nThreads = nThreads;
    }

    public String[] getVarNames()
    {
        return null;
    }

    public double[] getVariables()
    {
        return null;
    }

}
