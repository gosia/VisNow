/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.numeric.fieldAveraging;

import pl.edu.icm.visnow.lib.utils.numeric.splines.SplineValueGradientDP;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class SplineFieldAveraging2DDP implements FieldAveragingDP
{

    protected int[] ctrlDims;
    protected int[] avgDims;
    protected int density;
    //protected short[] mask = null;
    protected int[][] tDims;
    protected double[][] targets;
    protected double[][] coeffs = null;
    protected double[][] dCoeffs = null;
    protected double[][] d2Coeffs = null;
    protected int nThreads = 2;
    protected double deformationWeight = 10;
    protected double volumeWeight = .1f;
    protected int nCtrls, nCtrlsTotal;
    protected int nAvgs;
    protected double matchEnergy, deformationEnergy, volumeEnergy;
    protected boolean gdebug = false;
    protected long totalMillis = 0;
    protected int evaluations = 0;
    protected double shortNorm = 1.0 / Short.MAX_VALUE;
    protected double[] vol0 = null;
    protected int[][][] mBoxOff = new int[][][]{{{0, 2, 4, 6}, {1, 3, 5, 7}},
                                                {{0, 1, 4, 5}, {2, 3, 6, 7}},
                                                {{0, 1, 2, 3}, {4, 5, 6, 7}}};
    protected int[][][] boxOff = new int[3][2][4];
    protected double[] spatialWeights = null;
    protected int N = 0;
    protected double[] avgData;
    protected double[] stddevData;

    /**
     * @param ctrlDims - dimensions of control points grid - ctrlDims.length must be 3
     * @param source   - scalar source data array
     * @param tDims    - dimensions of target grid - tDims.length must be 3
     * @param tArr     - target scalar data array
     * @param
     */
    public SplineFieldAveraging2DDP(int[] ctrlDims, int[] avgDims,
                                    int[][] tDims, double[][] targets,
                                    int nThreads,
                                    float deformationWeight, float volumeWeight)
    {

        if (ctrlDims == null ||
            avgDims == null || avgDims.length != 2 ||
            tDims == null || targets == null ||
            ctrlDims == null || ctrlDims.length != 2) {
            return;
        }
        if (ceil((avgDims[0] - 1.) / (ctrlDims[0] - 1.)) != ceil((avgDims[1] - 1.) / (ctrlDims[1] - 1.))) {
            return;
        }

        N = targets.length;

        for (int n = 0; n < N; n++) {
            if (tDims[n] == null || tDims[n].length != 2 || targets[n] == null || targets[n].length != tDims[n][0] * tDims[n][1]) {
                return;
            }
        }

        vol0 = new double[N];
        for (int i = 0; i < N; i++) {
            vol0[i] = 1;
        }
        this.ctrlDims = ctrlDims;
        density = (int) (Math.ceil((avgDims[0] - 1.) / (ctrlDims[0] - 1.)));
        nCtrls = ctrlDims[0] * ctrlDims[1];
        nCtrlsTotal = N * nCtrls;
        this.avgDims = avgDims;
        nAvgs = avgDims[0] * avgDims[1];
        avgData = new double[nAvgs];
        stddevData = new double[nAvgs];

        this.tDims = tDims;
        this.targets = targets;
        coeffs = new double[4][density + 1];
        dCoeffs = new double[4][density + 1];
        d2Coeffs = new double[4][density + 1];
        for (int j = 0; j <= density; j++) {
            double x = (double) j / density;
            coeffs[3][j] = (.5 * x - .5) * x * x;
            coeffs[2][j] = ((-1.5 * x + 2.) * x + .5) * x;
            coeffs[1][j] = (1.5 * x - 2.5) * x * x + 1.;
            coeffs[0][j] = ((-.5 * x + 1.0) * x - .5) * x;
            dCoeffs[3][j] = (1.5 * x - 1.0) * x;
            dCoeffs[2][j] = (-4.5 * x + 4.0) * x + .5;
            dCoeffs[1][j] = (4.5 * x - 5.0) * x;
            dCoeffs[0][j] = (-1.5 * x + 2.0) * x - .5;
            d2Coeffs[3][j] = 3.0 * x - 1.0;
            d2Coeffs[2][j] = -9.0 * x + 4.0;
            d2Coeffs[1][j] = 9.0 * x - 5.0;
            d2Coeffs[0][j] = -3.0 * x + 2.0;
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 4; k++) {
                    boxOff[i][j][k] = 0;
                    if ((mBoxOff[i][j][k] & 1) != 0) {
                        boxOff[i][j][k] += 3;
                    }
                    if ((mBoxOff[i][j][k] & 2) != 0) {
                        boxOff[i][j][k] += 3 * ctrlDims[0];
                    }
                    if ((mBoxOff[i][j][k] & 4) != 0) {
                        boxOff[i][j][k] += 3 * ctrlDims[0] * ctrlDims[1];
                    }
                }
            }
        }

        this.nThreads = nThreads;
        this.deformationWeight = deformationWeight;
        this.volumeWeight = volumeWeight;
    }

    public SplineFieldAveraging2DDP(int[] ctrlDims,
                                    int[] avgDims,
                                    int[][] tDims, double[][] targets,
                                    int nThreads,
                                    float deformationWeight, float volumeWeight, double[] vol0)
    {
        this(ctrlDims, avgDims, tDims, targets, nThreads, deformationWeight, volumeWeight);
        this.vol0 = vol0;
    }

    private double computeMatchValGrad(double[] point, double[] gradient)
    {
        double out = 0;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = 0;
        }

        ComputeValGrad[] computeThreads = new ComputeValGrad[nThreads];
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            computeThreads[iThread] = new ComputeValGrad(point, nThreads, iThread);
            workThreads[iThread] = new Thread(computeThreads[iThread]);
            workThreads[iThread].start();
        }
        for (int iThread = 0; iThread < workThreads.length; iThread++) {
            try {
                workThreads[iThread].join();
            } catch (Exception e) {
            }
        }
        for (int iThread = 0; iThread < nThreads; iThread++) {
            out += computeThreads[iThread].getMatchValue();
            double[] thrGr = computeThreads[iThread].getMatchGradient();
            for (int i = 0; i < thrGr.length; i++) {
                gradient[i] += thrGr[i];
            }
        }
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] /= nAvgs;
        }
        out /= nAvgs;

        return out;
    }

    private class ComputeValGrad implements Runnable
    {

        private double[] ctrlCoords;
        private double[] matchGradient;
        private int iThread = 0;
        private double matchValue;

        ComputeValGrad(double[] ctrlCoords, int nThreads, int iThread)
        {
            this.ctrlCoords = ctrlCoords;
            this.iThread = iThread;
            matchGradient = new double[ctrlCoords.length];
        }

        public double[] getMatchGradient()
        {
            return matchGradient;
        }

        public double getMatchValue()
        {
            return matchValue;
        }

        public void run()
        {
            double[] pcoord = new double[2];
            double[] splineVals = new double[N];
            double meanVal = 0;
            double[][] splineGrads = new double[N][2];
            double[][][] jacobians = new double[N][2][2];

            matchValue = 0;
            for (int i = 0; i < ctrlCoords.length; i++) {
                matchGradient[i] = 0;
            }

            int dj = (ctrlDims[1] - 1) / nThreads;
            int jstart = iThread * dj + Math.min(iThread, (ctrlDims[1] - 1) % nThreads);
            int jend = (iThread + 1) * dj + Math.min(iThread + 1, (ctrlDims[1] - 1) % nThreads);
            for (int j = jstart; j < jend; j++) {
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int qx = density;
                    if (j == ctrlDims[1] - 2) {
                        qx = density + 1;
                    }
                    int rx = density;
                    if (k == ctrlDims[0] - 2) {
                        rx = density + 1;
                    }
                    for (int q = 0; q < qx; q++) {
                        for (int r = 0, l = (density * j + q) * avgDims[0] +
                            density * k; r < rx; r++, l++) {
                            meanVal = 0;
                            for (int n = 0, nOff = 0; n < N; n++, nOff += 2 * nCtrls) {
                                for (int m = 0; m < pcoord.length; m++) {
                                    pcoord[m] = 0;
                                    for (int im = 0; im < 2; im++) {
                                        for (int jm = 0; jm < 2; jm++) {
                                            jacobians[n][im][jm] = 0;
                                        }
                                    }
                                }
                                for (int jj = 0; jj < 4; jj++) {
                                    int jx = j + jj - 1;
                                    if (jx < 0) {
                                        jx = 0;
                                    }
                                    if (jx >= ctrlDims[1]) {
                                        jx = ctrlDims[1] - 1;
                                    }
                                    int isrc = jx * ctrlDims[0];
                                    for (int kk = 0; kk < 4; kk++) {
                                        for (int v = 0; v < 2; v++) {
                                            int kx = k + kk - 1;
                                            if (kx < 0) {
                                                kx = 0;
                                            }
                                            if (kx >= ctrlDims[0]) {
                                                kx = ctrlDims[0] - 1;
                                            }
                                            pcoord[v] += ctrlCoords[nOff + 2 * (isrc + kx) + v] *
                                                coeffs[jj][q] * coeffs[kk][r];
                                            jacobians[n][0][v] += ctrlCoords[nOff + 2 * (isrc + kx) + v] *
                                                dCoeffs[jj][q] * coeffs[kk][r];
                                            jacobians[n][1][v] += ctrlCoords[nOff + 2 * (isrc + kx) + v] *
                                                coeffs[jj][q] * dCoeffs[kk][r];
                                        }
                                    }
                                }
                                splineVals[n] = SplineValueGradientDP.getSplineValueGradient2D(targets[n], tDims[n], pcoord, splineGrads[n]);
                                meanVal += splineVals[n];
                            }
                            meanVal /= (double) N;
                            avgData[l] = meanVal;
                            double nrg = 0;
                            for (int n = 0, nOff = 0; n < N; n++, nOff += 2 * nCtrls) {
                                nrg += (splineVals[n] - meanVal) * (splineVals[n] - meanVal) / (double) N;
                                for (int jj = 0; jj < 4; jj++) {
                                    int jx = min(max(j + jj - 1, 0), ctrlDims[1] - 1);
                                    int isrc = jx * ctrlDims[0];
                                    for (int kk = 0; kk < 4; kk++) {
                                        for (int v = 0; v < 2; v++) {
                                            int kx = min(max(k + kk - 1, 0), ctrlDims[0] - 1);
                                            double c = coeffs[jj][q] * coeffs[kk][r];
                                            matchGradient[nOff + 2 * (isrc + kx) + v] += c * (splineVals[n] - meanVal) * splineGrads[n][v] / (double) N;
                                        }
                                    }
                                }
                            }
                            stddevData[l] = nrg;
                            matchValue += nrg;

                        }
                    }
                }
            }
        }
    }

    /**
     * computes energy (sum over source points of (t(F(x)-s(x))^2)) and energy gradient with respect to control points
     * <p>
     * @param ctrlCoords - target coordinates for control points in target index space
     *                   (ctrlCoords.length = 3*ctrlDims[0]*ctrlDims[1]*ctrlDims[2]) - independent variables
     * @param gradient   - array for storing gradient - must be preallocated of length ctrlCoords.length.
     * <p>
     * @return energy value (if negative - wrong input data)
     */
    private double computeDeformationValGrad(double[] point, double[] gradient)
    {
        if (deformationWeight == 0) {
            return 0;
        }
        double[] u = new double[2];
        double[] v = new double[2];
        double[] w = new double[2];
        int s0 = 2 * ctrlDims[0];
        double sw;
        double dw = deformationWeight / (double) nCtrls;
        double[] smoothEnergy = new double[N];

        for (int n = 0, cnOff = 0; n < N; n++, cnOff += 2 * nCtrls) {
            smoothEnergy[n] = 0;

            // d2v/dx2
            for (int j = 0; j < ctrlDims[1]; j++) {
                for (int k = 1, l = (j * ctrlDims[0] + 1) * 2; k < ctrlDims[0] - 1; k++, l += 2) {
                    double wnorm = 0;
                    for (int m = 0; m < 2; m++) {
                        u[m] = point[cnOff + l + m] - point[cnOff + l - 2 + m];
                        v[m] = point[cnOff + l + 2 + m] - point[cnOff + l + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy[n] += wnorm;
                        for (int m = 0; m < 2; m++) {
                            gradient[cnOff + l + 2 + m] += 2 * w[m] * dw;
                            gradient[cnOff + l + m] -= 4 * w[m] * dw;
                            gradient[cnOff + l - 2 + m] += 2 * w[m] * dw;
                        }
                    } else {
                        sw = spatialWeights[j * ctrlDims[0] + k];
                        smoothEnergy[n] += wnorm * sw;
                        for (int m = 0; m < 2; m++) {
                            gradient[cnOff + l + 2 + m] += 2 * w[m] * sw * dw;
                            gradient[cnOff + l + m] -= 4 * w[m] * sw * dw;
                            gradient[cnOff + l - 2 + m] += 2 * w[m] * sw * dw;
                        }
                    }
                }
            }
            // d2v/dxdy
            for (int j = 1; j < ctrlDims[1]; j++) {
                for (int k = 1, l = (j * ctrlDims[0] + 1) * 2; k < ctrlDims[0]; k++, l += 2) {
                    double wnorm = 0;
                    for (int m = 0; m < 2; m++) {
                        u[m] = point[cnOff + l + m] - point[cnOff + l - 2 + m];
                        v[m] = point[cnOff + l - s0 + m] - point[cnOff + l - s0 - 2 + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }

                    if (spatialWeights == null) {
                        smoothEnergy[n] += 2 * wnorm;
                        for (int m = 0; m < 2; m++) {
                            gradient[cnOff + l - s0 + m] += 4 * w[m] * dw;
                            gradient[cnOff + l - s0 - 2 + m] -= 4 * w[m] * dw;
                            gradient[cnOff + l + m] -= 4 * w[m] * dw;
                            gradient[cnOff + l - 2 + m] += 4 * w[m] * dw;
                        }
                    } else {
                        sw = spatialWeights[j * ctrlDims[0] + k];
                        smoothEnergy[n] += 2 * wnorm * sw;
                        for (int m = 0; m < 2; m++) {
                            gradient[cnOff + l - s0 + m] += 4 * w[m] * sw * dw;
                            gradient[cnOff + l - s0 - 2 + m] -= 4 * w[m] * sw * dw;
                            gradient[cnOff + l + m] -= 4 * w[m] * sw * dw;
                            gradient[cnOff + l - 2 + m] += 4 * w[m] * sw * dw;
                        }
                    }
                }
            }
            // d2v/dy2
            for (int j = 0; j < ctrlDims[0]; j++) {
                for (int k = 1, l = (ctrlDims[0] + j) * 2; k < ctrlDims[1] - 1; k++, l += s0) {
                    double wnorm = 0;
                    for (int m = 0; m < 2; m++) {
                        u[m] = point[cnOff + l + m] - point[cnOff + l - s0 + m];
                        v[m] = point[cnOff + l + s0 + m] - point[cnOff + l + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy[n] += wnorm;
                        for (int m = 0; m < 2; m++) {
                            gradient[cnOff + l + s0 + m] += 2 * w[m] * dw;
                            gradient[cnOff + l + m] -= 4 * w[m] * dw;
                            gradient[cnOff + l - s0 + m] += 2 * w[m] * dw;
                        }
                    } else {
                        sw = spatialWeights[k * ctrlDims[0] + j];
                        smoothEnergy[n] += wnorm * sw;
                        for (int m = 0; m < 2; m++) {
                            gradient[cnOff + l + s0 + m] += 2 * w[m] * sw * dw;
                            gradient[cnOff + l + m] -= 4 * w[m] * sw * dw;
                            gradient[cnOff + l - s0 + m] += 2 * w[m] * sw * dw;
                        }
                    }
                }
            }

            smoothEnergy[n] *= dw;
        }

        double outSmoothEnergy = 0;
        for (int n = 0; n < N; n++) {
            outSmoothEnergy += smoothEnergy[n];
        }

        return outSmoothEnergy;
    }

    public double computeValGrad(double[] point, double[] gradient)
    {
        long t0 = System.currentTimeMillis();

        matchEnergy = computeMatchValGrad(point, gradient);
        deformationEnergy = computeDeformationValGrad(point, gradient);
        volumeEnergy = computeVolumeDeformationValGrad(point, gradient);

        //        //WITH TEST
        //        
        //        double[] mGradient = new double[gradient.length];
        //        double[] dGradient = new double[gradient.length];
        //        double[] vGradient = new double[gradient.length];
        //        double[] dummy = new double[gradient.length];
        //        double[] numGrad = new double[gradient.length];
        //        for (int i = 0; i < gradient.length; i++) {
        //            mGradient[i] = 0;
        //            dGradient[i] = 0;
        //            vGradient[i] = 0;
        //            numGrad[i] = 0;
        //        }
        //
        //        double[] pointUp = new double[point.length];
        //        double[] pointLo = new double[point.length];
        //        double d = 0.001;
        //        System.arraycopy(point, 0, pointUp, 0, point.length);
        //        System.arraycopy(point, 0, pointLo, 0, point.length);
        //
        //
        //        matchEnergy = computeMatchValGrad(point, mGradient);
        //
        //        for (int i = 0; i < gradient.length; i++) {
        //            numGrad[i] = 0;
        //        }
        //        for (int i = 0; i < point.length; i++) {
        //            pointLo[i] -= d;
        //            pointUp[i] += d;
        //            double enLo = computeMatchValGrad(pointLo, dummy);
        //            double enUp = computeMatchValGrad(pointUp, dummy);
        //            numGrad[i] = (enUp - enLo) / (2 * d);
        //            pointLo[i] = point[i];
        //            pointUp[i] = point[i];
        //        }
        //
        //        double eps = 0.001;
        //        boolean ok = true;
        //        for (int n = 0; n < N; n++) {
        //            for (int l = 0; l < numGrad.length; l++) {
        //                if (Math.abs(numGrad[l] - mGradient[l]) > eps) {
        //                    ok = false;
        //                    System.out.println("ERROR: numGrad["+ l + "] = " + numGrad[l] + " mGrad[" + l + "]=" + mGradient[l]);
        //                    //break;
        //                }
        //            }
        //        }
        //        System.out.println("match gradient test: "+(ok?"OK":"ERROR"));
        //
        //
        //        
        //        
        //        deformationEnergy = computeDeformationValGrad(point, dGradient);
        //        //TEST
        //        for (int i = 0; i < gradient.length; i++) {
        //            numGrad[i] = 0;
        //        }
        //        for (int i = 0; i < point.length; i++) {
        //            pointLo[i] -= d;
        //            pointUp[i] += d;
        //            double enLo = computeDeformationValGrad(pointLo, dummy);
        //            double enUp = computeDeformationValGrad(pointUp, dummy);
        //            numGrad[i] = (enUp - enLo) / (2 * d);
        //            pointLo[i] = point[i];
        //            pointUp[i] = point[i];
        //        }
        //
        //        ok = true;
        //        for (int n = 0; n < N; n++) {
        //            for (int l = 0; l < numGrad.length; l++) {
        //                if (Math.abs(numGrad[l] - dGradient[l]) > eps) {
        //                    ok = false;
        //                    System.out.println("ERROR: numGrad["+ l + "] = " + numGrad[l] + " dGrad[" + l + "]=" + dGradient[l]);
        //                    //break;
        //                }
        //            }
        //        }
        //        System.out.println("deformation gradient test: "+(ok?"OK":"ERROR"));
        //
        //
        //        
        //        
        //        volumeEnergy = computeVolumeDeformationValGrad(point, vGradient);
        //        //TEST
        //        for (int i = 0; i < gradient.length; i++) {
        //            numGrad[i] = 0;
        //        }
        //        for (int i = 0; i < point.length; i++) {
        //            pointLo[i] -= d;
        //            pointUp[i] += d;
        //            double enLo = computeVolumeDeformationValGrad(pointLo, dummy);
        //            double enUp = computeVolumeDeformationValGrad(pointUp, dummy);
        //            numGrad[i] = (enUp - enLo) / (2 * d);
        //            pointLo[i] = point[i];
        //            pointUp[i] = point[i];
        //        }
        //
        //        ok = true;
        //        for (int n = 0; n < N; n++) {
        //            for (int l = 0; l < numGrad.length; l++) {
        //                if (Math.abs(numGrad[l] - vGradient[l]) > eps) {
        //                    ok = false;
        //                    System.out.println("ERROR: numGrad["+ l + "] = " + numGrad[l] + " vGrad[" + l + "]=" + vGradient[l]);
        //                    //break;
        //                }
        //            }
        //        }
        //        System.out.println("volume gradient test: "+(ok?"OK":"ERROR"));
        //
        //
        //
        //        for (int i = 0; i < gradient.length; i++) {
        //            gradient[i] = mGradient[i] + dGradient[i] + vGradient[i];
        //        }
        evaluations += 1;
        totalMillis += System.currentTimeMillis() - t0;
        //        System.out.printf("  matchEnergy = %8.3f   deformationEnergy = %8.3f    volumeEnergy = %8.3f     total energy = %8.3f%n",
        //                matchEnergy, deformationEnergy, volumeEnergy, matchEnergy + deformationEnergy + volumeEnergy);

        return matchEnergy + deformationEnergy + volumeEnergy;
    }

    private double computeVolumeDeformationValGrad(double[] point, double[] gradient)
    {
        if (volumeWeight == 0) {
            return 0;
        }
        double[] en = new double[N];
        double det, v;
        double dif;
        int l, l1, l2, l3;
        double[] w = new double[4];

        for (int n = 0, nOff = 0; n < N; n++, nOff += 2 * nCtrls) {
            en[n] = 0;
            for (int j = 0; j < ctrlDims[1] - 1; j++) {
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    l = (j * ctrlDims[0] + k) * 2;
                    l1 = (j * ctrlDims[0] + k + 1) * 2;
                    l2 = ((j + 1) * ctrlDims[0] + k) * 2;
                    l3 = ((j + 1) * ctrlDims[0] + k + 1) * 2;

                    w[0] = point[nOff + l3] - point[nOff + l];
                    w[1] = point[nOff + l2 + 1] - point[nOff + l1 + 1];
                    w[2] = point[nOff + l2] - point[nOff + l1];
                    w[3] = point[nOff + l3 + 1] - point[nOff + l + 1];

                    det = 0.5 * (w[0] * w[1] - w[2] * w[3]);
                    v = (det >= 0 ? det : (-det));
                    en[n] += (v - vol0[n]) * (v - vol0[n]) / nCtrls;

                    //grad
                    dif = volumeWeight * (v - vol0[n]) / nCtrls;
                    dif = (det >= 0 ? dif : (-dif));

                    gradient[nOff + l] -= dif * w[1];
                    gradient[nOff + l + 1] += dif * w[2];
                    gradient[nOff + l1] += dif * w[3];
                    gradient[nOff + l1 + 1] -= dif * w[0];
                    gradient[nOff + l2] -= dif * w[3];
                    gradient[nOff + l2 + 1] += dif * w[0];
                    gradient[nOff + l3] += dif * w[1];
                    gradient[nOff + l3 + 1] -= dif * w[2];
                }
            }
        }

        double outEn = 0;
        for (int n = 0; n < N; n++) {
            outEn += en[n];
        }
        return volumeWeight * outEn;
    }

    public double[] getFullDisplacedCoords(double[] ctrlCoords)
    {
        int nData = avgDims[0] * avgDims[1];
        double[] dispCoords = new double[N * 2 * nData];
        for (int i = 0; i < dispCoords.length; i++) {
            dispCoords[i] = 0;
        }
        double[] pcoord = new double[2];

        for (int n = 0, nOff = 0, nOff2 = 0; n < N; n++, nOff += 2 * nCtrls, nOff2 += 2 * nData) {
            for (int j = 0; j < ctrlDims[1] - 1; j++) {
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int qx = density;
                    if (j == ctrlDims[1] - 2) {
                        qx = density + 1;
                    }
                    int rx = density;
                    if (k == ctrlDims[0] - 2) {
                        rx = density + 1;
                    }
                    for (int q = 0; q < qx; q++) {
                        for (int r = 0, l = (density * j + q) * avgDims[0] + density * k; r < rx; r++, l++) {
                            for (int m = 0; m < pcoord.length; m++) {
                                pcoord[m] = 0;
                            }
                            for (int jj = 0; jj < 4; jj++) {
                                int jx = j + jj - 1;
                                if (jx < 0) {
                                    jx = 0;
                                }
                                if (jx >= ctrlDims[1]) {
                                    jx = ctrlDims[1] - 1;
                                }
                                int isrc = jx * ctrlDims[0];
                                for (int kk = 0; kk < 4; kk++) {
                                    for (int v = 0; v < 2; v++) {
                                        int kx = k + kk - 1;
                                        if (kx < 0) {
                                            kx = 0;
                                        }
                                        if (kx >= ctrlDims[0]) {
                                            kx = ctrlDims[0] - 1;
                                        }
                                        pcoord[v] += ctrlCoords[nOff + 2 * (isrc + kx) + v] * coeffs[jj][q] * coeffs[kk][r];
                                    }
                                }
                            }
                            System.arraycopy(pcoord, 0, dispCoords, nOff2 + 2 * l, pcoord.length);
                        }
                    }
                }
            }
        }
        return dispCoords;
    }

    public double[] getDisplacedCoords(int n, double[] ctrlCoords, int scale)
    {
        int nData = avgDims[0] * avgDims[1];
        double[] dispCoords = new double[2 * nData];
        for (int i = 0; i < dispCoords.length; i++) {
            dispCoords[i] = 0;
        }
        int nOff = 2 * n * nCtrls;
        double[] pcoord = new double[2];
        for (int j = 0; j < ctrlDims[1] - 1; j++) {
            for (int k = 0; k < ctrlDims[0] - 1; k++) {
                int qx = density;
                if (j == ctrlDims[1] - 2) {
                    qx = density + 1;
                }
                int rx = density;
                if (k == ctrlDims[0] - 2) {
                    rx = density + 1;
                }
                for (int q = 0; q < qx; q++) {
                    for (int r = 0, l = (density * j + q) * avgDims[0] + density * k; r < rx; r++, l++) {
                        for (int m = 0; m < pcoord.length; m++) {
                            pcoord[m] = 0;
                        }
                        for (int jj = 0; jj < 4; jj++) {
                            int jx = j + jj - 1;
                            if (jx < 0) {
                                jx = 0;
                            }
                            if (jx >= ctrlDims[1]) {
                                jx = ctrlDims[1] - 1;
                            }
                            int isrc = jx * ctrlDims[0];
                            for (int kk = 0; kk < 4; kk++) {
                                for (int v = 0; v < 2; v++) {
                                    int kx = k + kk - 1;
                                    if (kx < 0) {
                                        kx = 0;
                                    }
                                    if (kx >= ctrlDims[0]) {
                                        kx = ctrlDims[0] - 1;
                                    }
                                    pcoord[v] += ctrlCoords[nOff + 2 * (isrc + kx) + v] * coeffs[jj][q] * coeffs[kk][r];
                                }
                            }
                        }
                        System.arraycopy(pcoord, 0, dispCoords, 2 * l, pcoord.length);
                    }
                }
            }
        }
        return dispCoords;
    }

    public double[] getAverageData()
    {
        return avgData;
    }

    public double[] getEnergyData()
    {
        return stddevData;
    }

    public double[] getTargetData(int n)
    {
        return targets[n];
    }

    public int[] getTargetDataDims(int n)
    {
        return tDims[n];
    }

    public void setDeformationWeight(double deformationWeight)
    {
        this.deformationWeight = deformationWeight;
    }

    public void setVolumeWeight(double volumeWeight)
    {
        this.volumeWeight = volumeWeight;
    }

    public void setNThreads(int nThreads)
    {
        this.nThreads = nThreads;
    }

    public double getDeformationEnergy()
    {
        return deformationEnergy;
    }

    public double getVolumeEnergy()
    {
        return volumeEnergy;
    }

    public double getMatchEnergy()
    {
        return matchEnergy;
    }

    public void setGdebug(boolean gdebug)
    {
        this.gdebug = gdebug;
    }

    public int getEvaluations()
    {
        return evaluations;
    }

    public long getTotalMillis()
    {
        return totalMillis;
    }

    public void resetTimer()
    {
        totalMillis = 0;
        evaluations = 0;
    }

    public void setSpatialWeights(double[] weights)
    {
        if (weights == null || weights.length != ctrlDims[0] * ctrlDims[1]) {
            this.spatialWeights = null;
            return;
        }

        this.spatialWeights = weights;
    }

    public String[] getVarNames()
    {
        return new String[]{"matchEnergy", "deformationEnergy", "volumeEnergy"};
    }

    public double[] getVariables()
    {
        return new double[]{matchEnergy, deformationEnergy, volumeEnergy};
    }
}
