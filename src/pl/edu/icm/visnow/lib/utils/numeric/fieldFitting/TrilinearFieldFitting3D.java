/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.numeric.fieldFitting;

import pl.edu.icm.visnow.lib.utils.numeric.fieldFitting.FieldFitting;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.lib.utils.numeric.splines.SplineValueGradient;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class TrilinearFieldFitting3D implements FieldFitting
{

    protected int[] srcDims;
    protected float[] source;
    protected int[] tDims;
    protected DataArray tArr;
    protected int nThreads = 2;
    protected float deformationWeight = 10;
    protected float volumeWeight = .1f;
    protected int nCtrls = 8, nSrcs;
    protected float[] matchGradient;
    protected float[] deformationGradient;
    protected double matchEnergy, deformationEnergy, volumeEnergy;
    protected boolean gdebug = false;
    protected long totalMillis = 0;
    protected int evaluations = 0;
    protected int d0 = 0, d1 = 0, d2 = 0;

    protected float[] spatialWeights = null;

    public TrilinearFieldFitting3D(int[] srcDims, float[] source, int[] tDims, DataArray tArr, int nThreads, float deformationWeight)
    {
        if (source == null || source.length != srcDims[0] * srcDims[1] * srcDims[2])
            return;
        if (tDims == null || tDims.length != 3 ||
            tArr == null || !tArr.isNumeric() || tArr.getNElements() != tDims[0] * tDims[1] * tDims[2] || tArr.getVectorLength() != 1)
            return;
        this.srcDims = srcDims;
        nSrcs = srcDims[0] * srcDims[1] * srcDims[2];
        this.source = source;
        this.tDims = tDims;
        this.tArr = tArr;
        int gradSize = 3 * 8;
        matchGradient = new float[gradSize];
        deformationGradient = new float[gradSize];
        d0 = tDims[0];
        d1 = tDims[1];
        d2 = tDims[2];
        this.deformationWeight = deformationWeight;
        this.nThreads = nThreads;
    }

    public float[] getDisplacedCoords(float[] coords)
    {
        float[] dispCoords = new float[3 * source.length];
        for (int i = 0, l = 0; i < srcDims[2]; i++) {
            float t = i / (srcDims[2] - 1.f);
            for (int j = 0; j < srcDims[1]; j++) {
                float u = j / (srcDims[1] - 1.f);
                for (int k = 0; k < srcDims[0]; k++, l++) {
                    float v = k / (srcDims[0] - 1.f);
                    for (int m = 0; m < 3; m++)
                        dispCoords[3 * l + m] = (1 - t) * ((1 - u) * ((1 - v) * coords[m] + v * coords[3 + m]) +
                            u * ((1 - v) * coords[6 + m] + v * coords[9 + m])) +
                            t * ((1 - u) * ((1 - v) * coords[12 + m] + v * coords[15 + m]) +
                            u * ((1 - v) * coords[18 + m] + v * coords[21 + m]));
                }
            }
        }
        return dispCoords;
    }

    private class ComputeValGrad implements Runnable
    {

        private float[] coords;
        private float[] matchGradient;
        private int iThread = 0;
        private double matchValue;

        ComputeValGrad(float[] coords, int nThreads, int iThread)
        {
            this.coords = coords;
            this.iThread = iThread;
            matchGradient = new float[coords.length];
        }

        public float[] getMatchGradient()
        {
            return matchGradient;
        }

        public double getMatchValue()
        {
            return matchValue;
        }

        public void run()
        {
            float[] pcoord = new float[3];
            float[] splineGradient = new float[3];

            matchValue = 0;
            for (int i = 0; i < coords.length; i++)
                matchGradient[i] = 0;

            for (int i = iThread; i < srcDims[2]; i += nThreads) {
                float t = i / (srcDims[2] - 1.f);
                for (int j = 0; j < srcDims[1]; j++) {
                    float u = j / (srcDims[1] - 1.f);
                    for (int k = 0; k < srcDims[0]; k++) {
                        float v = k / (srcDims[0] - 1.f);
                        for (int m = 0; m < 3; m++) {
                            pcoord[m] = (1 - t) * ((1 - u) * ((1 - v) * coords[m] + v * coords[3 + m]) +
                                u * ((1 - v) * coords[6 + m] + v * coords[9 + m])) +
                                t * ((1 - u) * ((1 - v) * coords[12 + m] + v * coords[15 + m]) +
                                u * ((1 - v) * coords[18 + m] + v * coords[21 + m]));
                        }
                        double splineVal = SplineValueGradient.getSplineValueGradient(tArr, d0, d1, d2, pcoord, splineGradient);
                        double srcVal = source[(i * srcDims[1] + j) * srcDims[0] + k];
                        matchValue += (splineVal - srcVal) * (splineVal - srcVal);
                        for (int m = 0; m < 3; m++) {
                            double s = splineGradient[m] * (splineVal - srcVal);
                            matchGradient[m] += (1 - t) * (1 - u) * (1 - v) * s;
                            matchGradient[m + 3] += (1 - t) * (1 - u) * v * s;
                            matchGradient[m + 6] += (1 - t) * u * (1 - v) * s;
                            matchGradient[m + 9] += (1 - t) * u * v * s;
                            matchGradient[m + 12] += t * (1 - u) * (1 - v) * s;
                            matchGradient[m + 15] += t * (1 - u) * v * s;
                            matchGradient[m + 18] += t * u * (1 - v) * s;
                            matchGradient[m + 21] += t * u * v * s;
                        }
                    }
                }
            }
        }
    }

    /**
     * computes energy (sum over source points of (t(F(x)-s(x))^2)) and energy gradient with respect to control points
     * <p>
     * @param ctrlCoords - target coordinates for control points in target index space
     *                   (ctrlCoords.length = 3*srcDims[0]*srcDims[1]*srcDims[2]) - independent variables
     * @param gradient   - array for storing gradient - must be preallocated of length ctrlCoords.length.
     * <p>
     * @return energy value (if negative - wrong input data)
     */
    private double computeDeformationValGrad(float[] point, float[] gradient)
    {
        if (deformationWeight == 0)
            return 0;
        float[] u = new float[3];
        float[] v = new float[3];
        float[] w = new float[3];
        float sw;
        double smoothEnergy = 0;

        for (int i = 0; i < 24; i += 12) {
            double wnorm = 0;
            /*          -======+
             *        //|    //|
             *      + ======-  |
             *      |   |   |  |
             *      |   -===|==+
             *      | //    |//
             *      +=======-
             */
            for (int j = 0; j < 3; j++) {
                w[j] = point[i + 9 + j] - point[i + 6 + j] - point[i + 3 + j] + point[i + j];
                wnorm += w[j] * w[j];
            }
            sw = 2;
            if (spatialWeights != null)
                sw = 2 * spatialWeights[i / 3];
            smoothEnergy += sw * wnorm;
            sw *= 2;
            for (int j = 0; j < 3; j++) {
                gradient[i + 9 + j] += sw * w[j];
                gradient[i + 6 + j] -= sw * w[j];
                gradient[i + 3 + j] -= sw * w[j];
                gradient[i + j] += sw * w[j];
            }
        }
        /*         -======+
         *        / ||   / ||
         *      - ======+  ||
         *      ||  ||  || ||
         *      ||  +===||=-
         *      ||/     ||/
         *      +=======-
         */
        for (int i = 0; i < 12; i += 6) {
            double wnorm = 0;
            for (int j = 0; j < 3; j++) {
                w[j] = point[i + 15 + j] - point[i + 12 + j] - point[i + 3 + j] + point[i + j];
                wnorm += w[j] * w[j];
            }
            sw = 2;
            if (spatialWeights != null)
                sw = 2 * spatialWeights[i / 3];
            smoothEnergy += sw * wnorm;
            sw *= 2;
            for (int j = 0; j < 3; j++) {
                gradient[i + 15 + j] += sw * w[j];
                gradient[i + 12 + j] -= sw * w[j];
                gradient[i + 3 + j] -= sw * w[j];
                gradient[i + j] += sw * w[j];
            }
            /*         + ---- +
             *       //||    //||
             *      - ----- -  ||
             *      || ||   || ||
             *      ||  - --|| -
             *      ||//    ||//
             *      + ----- +
             */
        }
        for (int i = 0; i < 6; i += 3) {
            double wnorm = 0;
            for (int j = 0; j < 3; j++) {
                w[j] = point[i + 18 + j] - point[i + 12 + j] - point[i + 6 + j] + point[i + j];
                wnorm += w[j] * w[j];
            }
            sw = 2;
            if (spatialWeights != null)
                sw = 2 * spatialWeights[i / 3];
            smoothEnergy += sw * wnorm;
            sw *= 2;
            for (int j = 0; j < 3; j++) {
                gradient[i + 18 + j] += sw * w[j];
                gradient[i + 12 + j] -= sw * w[j];
                gradient[i + 6 + j] -= sw * w[j];
                gradient[i + j] += sw * w[j];
            }
        }

        for (int i = 0; i < gradient.length; i++)
            gradient[i] *= deformationWeight / nCtrls;
        smoothEnergy *= deformationWeight / nCtrls;
        return smoothEnergy;
    }

    public double computeValGrad(float[] point, float[] gradient)
    {
        long t0 = System.currentTimeMillis();
        matchEnergy = 0;
        for (int i = 0; i < gradient.length; i++)
            gradient[i] = matchGradient[i] = deformationGradient[i] = 0;

        ComputeValGrad[] computeThreads = new ComputeValGrad[nThreads];
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            computeThreads[iThread] = new ComputeValGrad(point, nThreads, iThread);
            workThreads[iThread] = new Thread(computeThreads[iThread]);
            workThreads[iThread].start();
        }
        for (int iThread = 0; iThread < workThreads.length; iThread++)
            try {
                workThreads[iThread].join();
            } catch (Exception e) {
            }
        for (int iThread = 0; iThread < nThreads; iThread++) {
            matchEnergy += computeThreads[iThread].getMatchValue();
            float[] thrGr = computeThreads[iThread].getMatchGradient();
            for (int i = 0; i < thrGr.length; i++)
                matchGradient[i] += thrGr[i];
        }
        matchEnergy /= nSrcs;
        deformationEnergy = computeDeformationValGrad(point, deformationGradient);

        for (int i = 0; i < gradient.length; i++) {
            matchGradient[i] /= nSrcs;
            gradient[i] = deformationGradient[i] + matchGradient[i];
        }

        //System.out.printf("  matchEnergy = %8.3f   deformationEnergy = %8.3f    volumeEnergy = %8.3f     total energy = %8.3f%n",
        //                 matchEnergy, deformationEnergy, volumeEnergy, matchEnergy + deformationEnergy + volumeEnergy);
        evaluations += 1;
        totalMillis += System.currentTimeMillis() - t0;
        return matchEnergy + deformationEnergy;
    }

    public void setDeformationWeight(float deformationWeight)
    {
        this.deformationWeight = deformationWeight;
    }

    public void setVolumeWeight(float volumeWeight)
    {
        this.volumeWeight = volumeWeight;
    }

    public void setNThreads(int nThreads)
    {
        this.nThreads = nThreads;
    }

    public double getDeformationEnergy()
    {
        return deformationEnergy;
    }

    public float[] getDeformationGradient()
    {
        return deformationGradient;
    }

    public double getVolumeEnergy()
    {
        return volumeEnergy;
    }

    public double getMatchEnergy()
    {
        return matchEnergy;
    }

    public int getEvaluations()
    {
        return evaluations;
    }

    public long getTotalMillis()
    {
        return totalMillis;
    }

    public void resetTimer()
    {
        totalMillis = 0;
        evaluations = 0;
    }

    public String[] getVarNames()
    {
        return new String[]{"matchEnergy", "deformationEnergy", "volumeEnergy"};
    }

    public double[] getVariables()
    {
        return new double[]{matchEnergy, deformationEnergy, volumeEnergy};
    }

}
