/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.numeric.fieldFitting;

import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.lib.utils.numeric.splines.SplineValueGradientDP;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class SplineFieldFitting3DDPNative implements FieldFittingDP
{

    protected int[] ctrlDims;
    protected int[] srcDims;
    protected int density;
    protected double[] source;
    protected double[] target;
    protected short[] mask = null;
    protected int[] tDims;
    protected double[][] coeffs = null;
    protected int nThreads = 2;
    protected double deformationWeight = 10;
    protected double volumeWeight = .1f;
    protected double cnfWeight = .1f;
    protected double isoWeight = .1f;
    protected int nCtrls, nSrcs;
    protected double matchEnergy, deformationEnergy, volumeEnergy;
    protected boolean gdebug = false;
    protected long totalMillis = 0;
    protected int evaluations = 0;
    protected double shortNorm = 1.f / Short.MAX_VALUE;
    protected double vol0 = 1;
    protected int[][][] mBoxOff = new int[][][]{{{0, 2, 4, 6}, {1, 3, 5, 7}}, {{0, 1, 4, 5}, {2, 3, 6, 7}}, {{0, 1, 2, 3}, {4, 5, 6, 7}}};
    protected int[][][] boxOff = new int[3][2][4];
    protected int d0 = 0, d1 = 0, d2 = 0;
    protected float[] spatialWeights = null;

    /**
     * @param ctrlDims - dimensions of control points grid - ctrlDims.length must be 3
     * @param source   - scalar source data array
     * @param tDims    - dimensions of target grid - tDims.length must be 3
     * @param target   - target scalar data
     * @param
     */
    public SplineFieldFitting3DDPNative(int[] ctrlDims,
                                        int[] srcDims, double[] source,
                                        int[] tDims, double[] target,
                                        DataArray mask, int nThreads,
                                        double deformationWeight, double volumeWeight)
    {
        if (ctrlDims == null || ctrlDims.length != 3) {
            return;
        }
        if (source == null || source.length != srcDims[0] * srcDims[1] * srcDims[2]) {
            return;
        }
        if (tDims == null || tDims.length != 3 ||
            target == null || target.length != tDims[0] * tDims[1] * tDims[2]) {
            return;
        }
        if (ceil((srcDims[0] - 1.) / (ctrlDims[0] - 1.)) != ceil((srcDims[1] - 1.) / (ctrlDims[1] - 1.)) ||
            ceil((srcDims[2] - 1.) / (ctrlDims[2] - 1.)) != ceil((srcDims[1] - 1.) / (ctrlDims[1] - 1.))) {
            return;
        }
        density = (int) (Math.ceil((srcDims[0] - 1.) / (ctrlDims[0] - 1.)));
        this.ctrlDims = ctrlDims;
        nCtrls = ctrlDims[0] * ctrlDims[1] * ctrlDims[2];

        this.srcDims = srcDims;
        nSrcs = srcDims[0] * srcDims[1] * srcDims[2];
        this.source = source;
        if (mask == null) {
            this.mask = new short[nSrcs];
            for (int i = 0; i < nSrcs; i++) {
                this.mask[i] = Short.MAX_VALUE;
            }
        } else {
            this.mask = mask.getRawShortArray().getData();
        }
        this.tDims = tDims;
        this.target = target;
        d0 = tDims[0];
        d1 = tDims[1];
        d2 = tDims[2];
        coeffs = new double[4][density + 1];
        for (int j = 0; j <= density; j++) {
            double x = (double) j / density;
            coeffs[3][j] = (.5 * x - .5) * x * x;
            coeffs[2][j] = ((-1.5 * x + 2.) * x + .5) * x;
            coeffs[1][j] = (1.5 * x - 2.5) * x * x + 1.;
            coeffs[0][j] = ((-.5 * x + 1.0) * x - .5) * x;
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 4; k++) {
                    boxOff[i][j][k] = 0;
                    if ((mBoxOff[i][j][k] & 1) != 0) {
                        boxOff[i][j][k] += 3;
                    }
                    if ((mBoxOff[i][j][k] & 2) != 0) {
                        boxOff[i][j][k] += 3 * ctrlDims[0];
                    }
                    if ((mBoxOff[i][j][k] & 4) != 0) {
                        boxOff[i][j][k] += 3 * ctrlDims[0] * ctrlDims[1];
                    }
                }
            }
        }

        this.nThreads = nThreads;
        this.deformationWeight = deformationWeight;
        this.volumeWeight = volumeWeight;

        //        String libPath = "/home/staff/babor/java/projects/VNNativeFieldFitting/dist/libVNNativeFieldFitting.so";
        //        try {
        //            System.load(libPath);
        //        } catch (UnsatisfiedLinkError err) {
        //            System.out.println("ERROR");
        //            err.printStackTrace();
        //            System.exit(1);
        //        } catch (NullPointerException ex) {
        //            ex.printStackTrace();
        //            System.exit(1);
        //        } catch (Exception ex) {
        //            ex.printStackTrace();
        //            System.exit(1);
        //        }
        //        System.out.println("native library loaded");
        System.out.println("using native SplineFieldFitting3DDP");

    }

    public SplineFieldFitting3DDPNative(int[] ctrlDims,
                                        int[] srcDims, double[] source,
                                        int[] tDims, double[] target,
                                        DataArray mask, int nThreads,
                                        double deformationWeight, double volumeWeight, double vol0)
    {
        this(ctrlDims, srcDims, source, tDims, target, mask, nThreads, deformationWeight, volumeWeight);
        this.vol0 = vol0;
    }

    public String[] getVarNames()
    {
        return new String[]{"matchEnergy", "deformationEnergy", "volumeEnergy"};
    }

    public double[] getVariables()
    {
        return new double[]{matchEnergy, deformationEnergy, volumeEnergy};
    }

    private static double java_computeMatchValGradDP(double[] point, double[] matchGradient, int[] ctrlDims, double[] source, int[] srcDims, double[] target, int[] tDims, short[] mask, double shortNorm, double[] coeffsTmp, int density)
    {
        double[] pcoord = new double[3];
        double matchValue = 0;
        double[][] coeffs = new double[4][density + 1];
        for (int i = 0, l = 0; i < 4; i++) {
            for (int j = 0; j < (density + 1); j++, l++) {
                coeffs[i][j] = (double) coeffsTmp[l];
            }
        }

        for (int i = 0; i < point.length; i++) {
            matchGradient[i] = 0;
        }

        for (int i = 0; i < ctrlDims[2] - 1; i++) {
            for (int j = 0; j < ctrlDims[1] - 1; j++) {
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int px = density;
                    if (i == ctrlDims[2] - 2) {
                        px = density + 1;
                    }
                    int qx = density;
                    if (j == ctrlDims[1] - 2) {
                        qx = density + 1;
                    }
                    int rx = density;
                    if (k == ctrlDims[0] - 2) {
                        rx = density + 1;
                    }
                    for (int p = 0; p < px; p++) {
                        for (int q = 0; q < qx; q++) {
                            for (int r = 0, l = ((density * i + p) * srcDims[1] + density * j + q) * srcDims[0] + density * k; r < rx; r++, l++) {
                                if (mask[l] == 0) {
                                    continue;
                                }
                                for (int m = 0; m < pcoord.length; m++) {
                                    pcoord[m] = 0;
                                }
                                for (int ii = 0; ii < 4; ii++) {
                                    int ix = i + ii - 1;
                                    if (ix < 0) {
                                        ix = 0;
                                    }
                                    if (ix >= ctrlDims[2]) {
                                        ix = ctrlDims[2] - 1;
                                    }
                                    for (int jj = 0; jj < 4; jj++) {
                                        int jx = j + jj - 1;
                                        if (jx < 0) {
                                            jx = 0;
                                        }
                                        if (jx >= ctrlDims[1]) {
                                            jx = ctrlDims[1] - 1;
                                        }
                                        int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                        for (int kk = 0; kk < 4; kk++) {
                                            for (int v = 0; v < 3; v++) {
                                                int kx = k + kk - 1;
                                                if (kx < 0) {
                                                    kx = 0;
                                                }
                                                if (kx >= ctrlDims[0]) {
                                                    kx = ctrlDims[0] - 1;
                                                }
                                                pcoord[v] += point[3 * (isrc + kx) + v] * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                            }
                                        }
                                    }
                                }
                                double maskCoeff = mask[l] * shortNorm;
                                double[] splineGradient = new double[3];
                                double splineVal = java_calculateSplineValueGradient(target, tDims, pcoord, splineGradient);
                                double sV = source[l];
                                matchValue += (splineVal - sV) * (splineVal - sV);
                                for (int ii = 0; ii < 4; ii++) {
                                    int ix = min(max(i + ii - 1, 0), ctrlDims[2] - 1);
                                    for (int jj = 0; jj < 4; jj++) {
                                        int jx = min(max(j + jj - 1, 0), ctrlDims[1] - 1);
                                        int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                        for (int kk = 0; kk < 4; kk++) {
                                            for (int v = 0; v < 3; v++) {
                                                int kx = min(max(k + kk - 1, 0), ctrlDims[0] - 1);
                                                double c = maskCoeff * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                                matchGradient[3 * (isrc + kx) + v] += c * (splineVal - sV) * splineGradient[v];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        double nSrcs = srcDims[0] * srcDims[1] * srcDims[2];
        matchValue /= nSrcs;
        for (int i = 0; i < matchGradient.length; i++) {
            matchGradient[i] /= nSrcs;
        }

        return matchValue;

    }

    public static double java_calculateSplineValueGradient(double[] target, int[] tDims, double[] coords, double[] gradient)
    {
        double u = coords[0];
        double v = coords[1];
        double w = coords[2];
        double val = 0;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = 0;
        }
        int off1 = tDims[0];
        int off2 = tDims[0] * tDims[1];
        int i = min(max((int) u, 0), tDims[0] - 1);
        u -= i;
        int j = min(max((int) v, 0), tDims[1] - 1);
        v -= j;
        int k = min(max((int) w, 0), tDims[2] - 1);
        w -= k;
        int il = -1;// if (i == 0) il = 0;
        int jl = -1;// if (j == 0) jl = 0;
        int kl = -1;// if (k == 0) kl = 0;
        int iu = 3;// if (i + iu > tDims[0])  iu = tDims[0] - i;
        int ju = 3;// if (j + ju > tDims[1])  ju = tDims[1] - j;
        int ku = 3;// if (k + ku > tDims[2])  ku = tDims[2] - k;
        double vu = 0, vv = 0, vw = 0;
        double du = 0, dv = 0, dw = 0;
        for (int k1 = kl; k1 < ku; k1++) {
            switch (k1) {
                case -1:
                    vw = ((-.5 * w + 1.0) * w - .5) * w;
                    dw = (-1.5 * w + 2.0) * w - .5;
                    break;
                case 0:
                    vw = (1.5 * w - 2.5) * w * w + 1.;
                    dw = (4.5 * w - 5.0) * w;
                    break;
                case 1:
                    vw = ((-1.5 * w + 2.) * w + .5) * w;
                    dw = (-4.5 * w + 4.) * w + .5;
                    break;
                case 2:
                    vw = (.5 * w - .5) * w * w;
                    dw = (1.5 * w - 1.) * w;
                    break;
            }
            for (int j1 = jl; j1 < ju; j1++) {
                switch (j1) {
                    case -1:
                        vv = ((-.5 * v + 1.0) * v - .5) * v;
                        dv = (-1.5 * v + 2.0) * v - .5;
                        break;
                    case 0:
                        vv = (1.5 * v - 2.5) * v * v + 1.;
                        dv = (4.5 * v - 5.0) * v;
                        break;
                    case 1:
                        vv = ((-1.5 * v + 2.) * v + .5) * v;
                        dv = (-4.5 * v + 4.) * v + .5;
                        break;
                    case 2:
                        vv = (.5 * v - .5) * v * v;
                        dv = (1.5 * v - 1.) * v;
                        break;
                }
                for (int i1 = il; i1 < iu; i1++) {
                    switch (i1) {
                        case -1:
                            vu = ((-.5 * u + 1.0) * u - .5) * u;
                            du = (-1.5 * u + 2.0) * u - .5;
                            break;
                        case 0:
                            vu = (1.5 * u - 2.5) * u * u + 1.;
                            du = (4.5 * u - 5.0) * u;
                            break;
                        case 1:
                            vu = ((-1.5 * u + 2.) * u + .5) * u;
                            du = (-4.5 * u + 4.) * u + .5;
                            break;
                        case 2:
                            vu = (.5 * u - .5) * u * u;
                            du = (1.5 * u - 1.) * u;
                            break;
                    }
                    double t = target[max(0, min(k + k1, tDims[2] - 1)) * off2 + max(0, min(j + j1, tDims[1] - 1)) * off1 + max(0, min(i + i1, tDims[0] - 1))];
                    val += (t * (vu * (vv * vw)));
                    gradient[0] += (2.0 * (t * (du * (vv * vw))));
                    gradient[1] += (2.0 * (t * (vu * (dv * vw))));
                    gradient[2] += (2.0 * (t * (vu * (vv * dw))));
                }
            }
        }
        return val;
    }

    private static double java_computeDeformationValGrad(double[] point, double[] gradient, double deformationWeight, int[] ctrlDims, float[] spatialWeights)
    {
        if (deformationWeight == 0) {
            return 0;
        }
        double[] u = new double[3];
        double[] v = new double[3];
        double[] w = new double[3];
        int s0 = 3 * ctrlDims[0];
        int s1 = 3 * ctrlDims[0] * ctrlDims[1];
        double sw;
        double smoothEnergy = 0;
        int nCtrls = ctrlDims[0] * ctrlDims[1] * ctrlDims[2];
        double dw = (double) deformationWeight / (double) nCtrls;

        // d2v/dx2
        for (int i = 0; i < ctrlDims[2]; i++) {
            for (int j = 0; j < ctrlDims[1]; j++) {
                for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0] - 1; k++, l += 3) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - 3 + m];
                        v[m] = point[l + 3 + m] - point[l + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + 3 + m] += 2 * w[m] * dw;
                            gradient[l + m] -= 4 * w[m] * dw;
                            gradient[l - 3 + m] += 2 * w[m] * dw;
                        }
                    } else {
                        sw = (double) spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                        smoothEnergy += wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + 3 + m] += 2 * w[m] * sw * dw;
                            gradient[l + m] -= 4 * w[m] * sw * dw;
                            gradient[l - 3 + m] += 2 * w[m] * sw * dw;
                        }
                    }
                }
            }
        }
        // d2v/dxdy
        for (int i = 0; i < ctrlDims[2]; i++) {
            for (int j = 1; j < ctrlDims[1]; j++) {
                for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0]; k++, l += 3) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - 3 + m];
                        v[m] = point[l - s0 + m] - point[l - s0 - 3 + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }

                    if (spatialWeights == null) {
                        smoothEnergy += 2 * wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s0 + m] += 4 * w[m] * dw;
                            gradient[l - s0 - 3 + m] -= 4 * w[m] * dw;
                            gradient[l + m] -= 4 * w[m] * dw;
                            gradient[l - 3 + m] += 4 * w[m] * dw;
                        }
                    } else {
                        sw = (double) spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                        smoothEnergy += 2 * wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s0 + m] += 4 * w[m] * sw * dw;
                            gradient[l - s0 - 3 + m] -= 4 * w[m] * sw * dw;
                            gradient[l + m] -= 4 * w[m] * sw * dw;
                            gradient[l - 3 + m] += 4 * w[m] * sw * dw;
                        }
                    }
                }
            }
        }
        // d2v/dxdz
        for (int i = 1; i < ctrlDims[2]; i++) {
            for (int j = 0; j < ctrlDims[1]; j++) {
                for (int k = 1, l = ((i * ctrlDims[1] + j) * ctrlDims[0] + 1) * 3; k < ctrlDims[0]; k++, l += 3) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - 3 + m];
                        v[m] = point[l - s1 + m] - point[l - s1 - 3 + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }

                    if (spatialWeights == null) {
                        smoothEnergy += 2 * wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s1 + m] += 4 * w[m] * dw;
                            gradient[l - s1 - 3 + m] -= 4 * w[m] * dw;
                            gradient[l + m] -= 4 * w[m] * dw;
                            gradient[l - 3 + m] += 4 * w[m] * dw;
                        }
                    } else {
                        sw = (double) spatialWeights[i * ctrlDims[0] * ctrlDims[1] + j * ctrlDims[0] + k];
                        smoothEnergy += 2 * wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - s1 + m] += 4 * w[m] * sw * dw;
                            gradient[l - s1 - 3 + m] -= 4 * w[m] * sw * dw;
                            gradient[l + m] -= 4 * w[m] * sw * dw;
                            gradient[l - 3 + m] += 4 * w[m] * sw * dw;
                        }
                    }
                }
            }
        }
        // d2v/dy2
        for (int i = 0; i < ctrlDims[2]; i++) {
            for (int j = 0; j < ctrlDims[0]; j++) {
                for (int k = 1, l = ((i * ctrlDims[1] + 1) * ctrlDims[0] + j) * 3; k < ctrlDims[1] - 1; k++, l += s0) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - s0 + m];
                        v[m] = point[l + s0 + m] - point[l + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s0 + m] += 2 * w[m] * dw;
                            gradient[l + m] -= 4 * w[m] * dw;
                            gradient[l - s0 + m] += 2 * w[m] * dw;
                        }
                    } else {
                        sw = (double) spatialWeights[i * ctrlDims[0] * ctrlDims[1] + k * ctrlDims[0] + j];
                        smoothEnergy += wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s0 + m] += 2 * w[m] * sw * dw;
                            gradient[l + m] -= 4 * w[m] * sw * dw;
                            gradient[l - s0 + m] += 2 * w[m] * sw * dw;
                        }
                    }
                }
            }
        }
        // d2v/dydz
        for (int i = 1; i < ctrlDims[2]; i++) {
            for (int j = 0; j < ctrlDims[0]; j++) {
                for (int k = 1, l = ((i * ctrlDims[1] + 1) * ctrlDims[0] + j) * 3; k < ctrlDims[1]; k++, l += s0) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - s1 + m];
                        v[m] = point[l - 3 + m] - point[l - s1 - 3 + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += 2 * wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - 3 + m] += 4 * w[m] * dw;
                            gradient[l - s1 - 3 + m] -= 4 * w[m] * dw;
                            gradient[l + m] -= 4 * w[m] * dw;
                            gradient[l - s1 + m] += 4 * w[m] * dw;
                        }
                    } else {
                        sw = (double) spatialWeights[i * ctrlDims[0] * ctrlDims[1] + k * ctrlDims[0] + j];
                        smoothEnergy += 2 * wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l - 3 + m] += 4 * w[m] * sw * dw;
                            gradient[l - s1 - 3 + m] -= 4 * w[m] * sw * dw;
                            gradient[l + m] -= 4 * w[m] * sw * dw;
                            gradient[l - s1 + m] += 4 * w[m] * sw * dw;
                        }
                    }
                }
            }
        }

        // d2v/dz2
        for (int i = 0; i < ctrlDims[1]; i++) {
            for (int j = 0; j < ctrlDims[0]; j++) {
                for (int k = 1, l = ((i + ctrlDims[1]) * ctrlDims[0] + j) * 3; k < ctrlDims[2] - 1; k++, l += s1) {
                    double wnorm = 0;
                    for (int m = 0; m < 3; m++) {
                        u[m] = point[l + m] - point[l - s1 + m];
                        v[m] = point[l + s1 + m] - point[l + m];
                        w[m] = v[m] - u[m];
                        wnorm += w[m] * w[m];
                    }
                    if (spatialWeights == null) {
                        smoothEnergy += wnorm;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s1 + m] += 2 * w[m] * dw;
                            gradient[l + m] -= 4 * w[m] * dw;
                            gradient[l - s1 + m] += 2 * w[m] * dw;
                        }
                    } else {
                        sw = (double) spatialWeights[k * ctrlDims[0] * ctrlDims[1] + i * ctrlDims[0] + j];
                        smoothEnergy += wnorm * sw;
                        for (int m = 0; m < 3; m++) {
                            gradient[l + s1 + m] += 2 * w[m] * sw * dw;
                            gradient[l + m] -= 4 * w[m] * sw * dw;
                            gradient[l - s1 + m] += 2 * w[m] * sw * dw;
                        }
                    }
                }
            }
        }

        return smoothEnergy * dw;
    }

    private static double java_computeVolumeDeformationValGrad(double[] point, double[] gradient, double volumeWeight, int[] ctrlDims, int[] boxOffTmp, double vol0)
    {
        if (volumeWeight == 0) {
            return 0;
        }

        int[][][] boxOff = new int[3][2][4];
        for (int i = 0, c = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 4; k++, c++) {
                    boxOff[i][j][k] = boxOffTmp[c];
                }
            }
        }

        double en = 0;
        double[] v0 = new double[3], v1 = new double[3], v2 = new double[3];
        double[] w0 = new double[3], w1 = new double[3], w2 = new double[3];
        int nCtrls = ctrlDims[0] * ctrlDims[1] * ctrlDims[2];
        for (int i = 0; i < ctrlDims[2] - 1; i++) {
            for (int j = 0; j < ctrlDims[1] - 1; j++) {
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int k0 = 3 * ((i * ctrlDims[1] + j) * ctrlDims[0] + k);
                    for (int l = 0; l < 3; l++) {
                        v0[l] = v1[l] = v2[l] = 0;
                        for (int m = 0; m < 4; m++) {
                            v0[l] += point[k0 + boxOff[0][1][m] + l] - point[k0 + boxOff[0][0][m] + l];
                            v1[l] += point[k0 + boxOff[1][1][m] + l] - point[k0 + boxOff[1][0][m] + l];
                            v2[l] += point[k0 + boxOff[2][1][m] + l] - point[k0 + boxOff[2][0][m] + l];
                        }
                        v0[l] *= .25;
                        v1[l] *= .25;
                        v2[l] *= .25;
                    }
                    w0[0] = v1[1] * v2[2] - v1[2] * v2[1];
                    w0[1] = v1[2] * v2[0] - v1[0] * v2[2];
                    w0[2] = v1[0] * v2[1] - v1[1] * v2[0];
                    w1[0] = v2[1] * v0[2] - v2[2] * v0[1];
                    w1[1] = v2[2] * v0[0] - v2[0] * v0[2];
                    w1[2] = v2[0] * v0[1] - v2[1] * v0[0];
                    w2[0] = v0[1] * v1[2] - v0[2] * v1[1];
                    w2[1] = v0[2] * v1[0] - v0[0] * v1[2];
                    w2[2] = v0[0] * v1[1] - v0[1] * v1[0];
                    double v = v0[0] * w0[0] + v0[1] * w0[1] + v0[2] * w0[2];
                    en += (v - vol0) * (v - vol0);
                    double dif = .5 * volumeWeight * (v - vol0) / nCtrls;
                    for (int l = 0; l < 3; l++) {
                        for (int m = 0; m < 4; m++) {
                            gradient[k0 + boxOff[0][1][m] + l] += dif * w0[l];
                            gradient[k0 + boxOff[0][0][m] + l] -= dif * w0[l];
                            gradient[k0 + boxOff[1][1][m] + l] += dif * w1[l];
                            gradient[k0 + boxOff[1][0][m] + l] -= dif * w1[l];
                            gradient[k0 + boxOff[2][1][m] + l] += dif * w2[l];
                            gradient[k0 + boxOff[2][0][m] + l] -= dif * w2[l];
                        }
                    }
                }
            }
        }
        return volumeWeight * en / (double) nCtrls;
    }

    private double computeConformalValGrad(double[] point, double[] gradient)
    {
        if (cnfWeight == 0) {
            return 0;
        }
        double en = 0;
        double[][] d = new double[3][3];
        double[][] v = new double[3][3];
        double[][] w = new double[3][3];
        for (int i = 0; i < ctrlDims[2] - 1; i++) {
            for (int j = 0; j < ctrlDims[1] - 1; j++) {
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int k0 = 3 * ((i * ctrlDims[1] + j) * ctrlDims[0] + k);
                    for (int l = 0; l < 3; l++) {
                        for (int m = 0; m < 3; m++) {
                            v[m][l] = d[m][l] = 0;
                        }
                        for (int m = 0; m < 4; m++) {
                            for (int n = 0; n < 3; n++) {
                                v[n][l] += point[k0 + boxOff[n][1][m] + l] - point[k0 + boxOff[n][0][m] + l];
                            }
                        }
                        for (int n = 0; n < 3; n++) {
                            v[n][l] *= .25f;
                        }
                    }
                    // v[n]  is the averaged n-th cell axis             

                    for (int l = 0; l < 3; l++) {
                        for (int m = 0; m < 3; m++) {
                            w[l][m] = v[l][0] * v[m][0] + v[l][1] * v[m][1] + v[l][2] * v[m][2];
                        }
                    }
                    // w is the metric form                

                    en += 2 * (w[1][0] * w[1][0] + w[2][0] * w[2][0] + w[2][1] * w[2][1]);
                    // penalty for nondiagonality of the metric form                
                    for (int l = 0; l < 3; l++) {
                        d[0][l] += 4 * (w[1][0] * v[1][l] + w[2][0] * v[2][l]);
                        d[1][l] += 4 * (w[2][1] * v[2][l] + w[0][1] * v[0][l]);
                        d[2][l] += 4 * (w[0][2] * v[0][l] + w[1][2] * v[1][l]);
                    }

                    en += (w[0][0] - w[1][1]) * (w[0][0] - w[1][1]) +
                        (w[1][1] - w[2][2]) * (w[1][1] - w[2][2]) +
                        (w[2][2] - w[0][0]) * (w[2][2] - w[0][0]);
                    // penelty for different diagonal entries               
                    for (int l = 0; l < 3; l++) {
                        d[0][l] += 2 * (2 * w[0][0] - w[1][1] - w[2][2]) * v[0][l];
                        d[1][l] += 2 * (2 * w[1][1] - w[2][2] - w[0][0]) * v[1][l];
                        d[2][l] += 2 * (2 * w[2][2] - w[0][0] - w[1][1]) * v[2][l];
                    }

                    double dif = .25 * cnfWeight / nCtrls;
                    for (int l = 0; l < 3; l++) {
                        for (int m = 0; m < 4; m++) {
                            for (int n = 0; n < 3; n++) {
                                gradient[k0 + boxOff[l][1][m] + n] += dif * d[l][n];
                                gradient[k0 + boxOff[l][0][m] + n] -= dif * d[l][n];
                            }
                        }
                    }
                }
            }
        }
        return cnfWeight * en / nCtrls;
    }

    private double computeConformalValGrad(double[] point, double[] gradient, double r0)
    {
        if (cnfWeight == 0) {
            return 0;
        }
        double en = 0;
        double[][] d = new double[3][3];
        double[][] v = new double[3][3];
        double[][] w = new double[3][3];
        for (int i = 0; i < ctrlDims[2] - 1; i++) {
            for (int j = 0; j < ctrlDims[1] - 1; j++) {
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int k0 = 3 * ((i * ctrlDims[1] + j) * ctrlDims[0] + k);
                    for (int l = 0; l < 3; l++) {
                        for (int m = 0; m < 3; m++) {
                            v[m][l] = d[m][l] = 0;
                        }
                        for (int m = 0; m < 4; m++) {
                            for (int n = 0; n < 3; n++) {
                                v[n][l] += point[k0 + boxOff[n][1][m] + l] - point[k0 + boxOff[n][0][m] + l];
                            }
                        }
                        for (int n = 0; n < 3; n++) {
                            v[n][l] *= .25;
                        }
                    }
                    // v[n]  is the averaged n-th cell axis             

                    for (int l = 0; l < 3; l++) {
                        for (int m = 0; m < 3; m++) {
                            w[l][m] = v[l][0] * v[m][0] + v[l][1] * v[m][1] + v[l][2] * v[m][2];
                        }
                    }
                    // w is the metric form                

                    en += 2 * (w[1][0] * w[1][0] + w[2][0] * w[2][0] + w[2][1] * w[2][1]);
                    // penalty for nondiagonality of the metric form                
                    for (int l = 0; l < 3; l++) {
                        d[0][l] += 4 * (w[1][0] * v[1][l] + w[2][0] * v[2][l]);
                        d[1][l] += 4 * (w[2][1] * v[2][l] + w[0][1] * v[0][l]);
                        d[2][l] += 4 * (w[0][2] * v[0][l] + w[1][2] * v[1][l]);
                    }

                    en += (w[0][0] - r0) * (w[0][0] - r0) +
                        (w[1][1] - r0) * (w[1][1] - r0) +
                        (w[2][2] - r0) * (w[2][2] - r0);
                    // penelty for diagonal entries               
                    for (int l = 0; l < 3; l++) {
                        d[0][l] += 2 * (w[0][0] - r0) * v[0][l];
                        d[1][l] += 2 * (w[1][1] - r0) * v[1][l];
                        d[2][l] += 2 * (w[2][2] - r0) * v[2][l];
                    }

                    double dif = .25 * cnfWeight / nCtrls;
                    for (int l = 0; l < 3; l++) {
                        for (int m = 0; m < 4; m++) {
                            for (int n = 0; n < 3; n++) {
                                gradient[k0 + boxOff[l][1][m] + n] += dif * d[l][n];
                                gradient[k0 + boxOff[l][0][m] + n] -= dif * d[l][n];
                            }
                        }
                    }
                }
            }
        }
        return cnfWeight * en / nCtrls;
    }

    public double computeValGrad(double[] point, double[] gradient)
    {
        double[] coeffsTmp = new double[4 * (density + 1)];
        for (int i = 0, l = 0; i < 4; i++) {
            for (int j = 0; j < (density + 1); j++, l++) {
                coeffsTmp[l] = coeffs[i][j];
            }
        }

        int[] boxOffTmp = new int[3 * 2 * 4];
        for (int i = 0, c = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 4; k++, c++) {
                    boxOffTmp[c] = boxOff[i][j][k];
                }
            }
        }

        long t0 = System.currentTimeMillis();
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = 0;
        }

        double[] energies = new double[]{0.0, 0.0, 0.0};

        native_doCompute(this.nThreads, energies, point, gradient, ctrlDims,
                         source, srcDims, target, tDims,
                         coeffsTmp, mask, shortNorm, density,
                         deformationWeight, spatialWeights,
                         volumeWeight, boxOffTmp, vol0);

        matchEnergy = energies[0];
        deformationEnergy = energies[1];
        volumeEnergy = energies[2];

        evaluations += 1;
        totalMillis += System.currentTimeMillis() - t0;
        return matchEnergy + deformationEnergy + volumeEnergy;
    }

    //    public double computeValGradTest(double[] point, double[] gradient) {
    //        double[] coeffsTmp = new double[4 * (density + 1)];
    //        for (int i = 0, l = 0; i < 4; i++) {
    //            for (int j = 0; j < (density + 1); j++, l++) {
    //                coeffsTmp[l] = coeffs[i][j];
    //            }
    //        }
    //
    //        long t0 = System.currentTimeMillis();
    //        matchEnergy = 0;
    //        deformationEnergy = 0;
    //        volumeEnergy = 0;
    //        for (int i = 0; i < gradient.length; i++) {
    //            gradient[i] = matchGradient[i] = deformationGradient[i] = 0;
    //        }
    //
    //        
    ////        matchEnergy = java_computeMatchValGradDP(point, matchGradient, ctrlDims,
    ////                source, srcDims, tArr.getFData().getData(), tDims,
    ////                mask, shortNorm, coeffsTmp, density);
    //
    ////        matchEnergy = native_computeMatchValGradDP(point, matchGradient, ctrlDims,
    ////                source, srcDims, tArr.getFData().getData(), tDims,
    ////                mask, shortNorm, coeffsTmp, density);
    ////        
    //        
    //        
    //        System.out.println("");
    //        System.out.println("start native_computeMatchValGradDP");
    //        long t0_computeValGrad = System.currentTimeMillis();
    //        matchEnergy = native_computeMatchValGradDP(point, matchGradient, ctrlDims,
    //                source, srcDims, target, tDims,
    //                mask, shortNorm, coeffsTmp, density);
    //        long t1_computeValGrad = System.currentTimeMillis();
    //        System.out.println("native_computeMatchValGradDP time (s): " + ((float) (t1_computeValGrad - t0_computeValGrad) / 1000.0f));
    //
    //        System.out.println("start java_computeMatchValGradDP");
    //        t0_computeValGrad = System.currentTimeMillis();
    //        double[] jmatchGradient = new double[gradient.length];
    //        for (int i = 0; i < jmatchGradient.length; i++) {
    //            jmatchGradient[i] = 0;
    //        }
    //        double jmatchEnergy = java_computeMatchValGradDP(point, jmatchGradient, ctrlDims,
    //                source, srcDims, target, tDims,
    //                mask, shortNorm, coeffsTmp, density);
    //        t1_computeValGrad = System.currentTimeMillis();
    //        System.out.println("java_computeMatchValGradDP time (s): " + ((float) (t1_computeValGrad - t0_computeValGrad) / 1000.0f));
    //
    //
    //        System.out.println("start computeMatchValGradDP comparison");
    //        System.out.println("native matchEnergy = "+matchEnergy);
    //        System.out.println("  java matchEnergy = "+jmatchEnergy);
    //        float eps = 0.0000000001f;
    //        //float eps = 0.0000001f;
    //        //float eps = 0.001f;
    //        int c = 0;
    //        for (int i = 0; i < jmatchGradient.length; i++) {
    //            if (Math.abs(jmatchGradient[i] - matchGradient[i]) > eps) {
    //                c++;
    //            }
    //        }
    //        System.out.println("gradient error count = " + c + "(of "+ gradient.length +")");
    //        
    //        
    //
    //        deformationEnergy = java_computeDeformationValGrad(point, deformationGradient);
    //        volumeEnergy = java_computeVolumeDeformationValGrad(point, deformationGradient);
    //
    //        for (int i = 0; i < gradient.length; i++) {
    //            gradient[i] = deformationGradient[i] + matchGradient[i];
    //        }
    //
    //        evaluations += 1;
    //        totalMillis += System.currentTimeMillis() - t0;
    //        return matchEnergy + deformationEnergy + volumeEnergy;
    //    }
    //TODO native
    public double[] getDisplacedCoords(double[] ctrlCoords)
    {
        double[] dispCoords = new double[3 * source.length];
        for (int i = 0; i < dispCoords.length; i++) {
            dispCoords[i] = 0;
        }
        double[] pcoord = new double[3];
        for (int i = 0; i < ctrlDims[2] - 1; i++) {
            for (int j = 0; j < ctrlDims[1] - 1; j++) {
                for (int k = 0; k < ctrlDims[0] - 1; k++) {
                    int px = density;
                    if (i == ctrlDims[2] - 2) {
                        px = density + 1;
                    }
                    int qx = density;
                    if (j == ctrlDims[1] - 2) {
                        qx = density + 1;
                    }
                    int rx = density;
                    if (k == ctrlDims[0] - 2) {
                        rx = density + 1;
                    }
                    for (int p = 0; p < px; p++) {
                        for (int q = 0; q < qx; q++) {
                            for (int r = 0, l = ((density * i + p) * srcDims[1] + density * j + q) * srcDims[0] + density * k; r < rx; r++, l++) {
                                for (int m = 0; m < pcoord.length; m++) {
                                    pcoord[m] = 0;
                                }
                                for (int ii = 0; ii < 4; ii++) {
                                    int ix = i + ii - 1;
                                    if (ix < 0) {
                                        ix = 0;
                                    }
                                    if (ix >= ctrlDims[2]) {
                                        ix = ctrlDims[2] - 1;
                                    }
                                    for (int jj = 0; jj < 4; jj++) {
                                        int jx = j + jj - 1;
                                        if (jx < 0) {
                                            jx = 0;
                                        }
                                        if (jx >= ctrlDims[1]) {
                                            jx = ctrlDims[1] - 1;
                                        }
                                        int isrc = (ix * ctrlDims[1] + jx) * ctrlDims[0];
                                        for (int kk = 0; kk < 4; kk++) {
                                            for (int v = 0; v < 3; v++) {
                                                int kx = k + kk - 1;
                                                if (kx < 0) {
                                                    kx = 0;
                                                }
                                                if (kx >= ctrlDims[0]) {
                                                    kx = ctrlDims[0] - 1;
                                                }
                                                pcoord[v] += ctrlCoords[3 * (isrc + kx) + v] * coeffs[ii][p] * coeffs[jj][q] * coeffs[kk][r];
                                            }
                                        }
                                    }
                                }
                                System.arraycopy(pcoord, 0, dispCoords, 3 * l, pcoord.length);
                            }
                        }
                    }
                }
            }
        }
        return dispCoords;
    }

    public byte[] getTransformedTarget(double[] ctrlCoords)
    {
        byte[] transTar = new byte[source.length];
        double[] pcoord = new double[3];
        double[] tGradient = new double[3];
        for (int i = 0; i < transTar.length; i++) {
            for (int j = 0; j < 3; j++) {
                pcoord[j] = ctrlCoords[3 * i + j];
            }
            double tV = SplineValueGradientDP.getSplineValueGradient(target, tDims, pcoord, tGradient);
            transTar[i] = (byte) ((int) tV & 0xff);
        }
        return transTar;
    }

    public void setDeformationWeight(double deformationWeight)
    {
        this.deformationWeight = deformationWeight;
    }

    public void setVolumeWeight(double volumeWeight)
    {
        this.volumeWeight = volumeWeight;
    }

    public void setNThreads(int nThreads)
    {
        this.nThreads = nThreads;
    }

    public double getDeformationEnergy()
    {
        return deformationEnergy;
    }

    public double getVolumeEnergy()
    {
        return volumeEnergy;
    }

    public double getMatchEnergy()
    {
        return matchEnergy;
    }

    public void setGdebug(boolean gdebug)
    {
        this.gdebug = gdebug;
    }

    public int getEvaluations()
    {
        return evaluations;
    }

    public long getTotalMillis()
    {
        return totalMillis;
    }

    public void resetTimer()
    {
        totalMillis = 0;
        evaluations = 0;
    }

    public void setSpatialWeights(float[] weights)
    {
        if (weights == null || weights.length != ctrlDims[0] * ctrlDims[1] * ctrlDims[2]) {
            this.spatialWeights = null;
            return;
        }

        this.spatialWeights = weights;
    }

    //    public static void main(String[] args) {
    //        String libPath = "/home/staff/babor/java/projects/VNNativeFieldFitting/dist/libVNNativeFieldFitting.so";
    //        try {
    //            System.load(libPath);
    //        } catch (UnsatisfiedLinkError err) {
    //            System.out.println("ERROR");
    //            err.printStackTrace();
    //            System.exit(1);
    //        } catch (NullPointerException ex) {
    //            ex.printStackTrace();
    //            System.exit(1);
    //        } catch (Exception ex) {
    //            ex.printStackTrace();
    //            System.exit(1);
    //        }
    //        System.out.println("native library loaded");
    //
    //        
    //        
    //        
    //        int density = 1;
    //        int s = 20;
    //        
    //        int[] srcDims = new int[]{8*s,8*s,8*s};
    //        int nSrc = srcDims[0]*srcDims[1]*srcDims[2];
    //        double[] source = new double[nSrc];
    //        
    //        
    //        int[] tDims = new int[]{8*s,8*s,8*s};
    //        int nTrg = tDims[0]*tDims[1]*tDims[2];
    //        double[] target = new double[nTrg];
    //        
    //        for (int i = 0; i < nTrg; i++) {
    //            target[i] = i;
    //            source[i] = i+4*s;
    //        }
    //        
    //        int[] ctrlDims = new int[]{3*s,3*s,3*s};
    //        int nCtrls = ctrlDims[0]*ctrlDims[1]*ctrlDims[2];
    //        
    //        double[] point = new double[3*nCtrls];
    //        for (int i = 0, l = 0; i < ctrlDims[0]; i++) {
    //            for (int j = 0; j < ctrlDims[1]; j++) {
    //                for (int k = 0; k < ctrlDims[2]; k++, l++) {
    //                    point[3*l] = tDims[0] * (double)i/(double)(ctrlDims[0]-1);
    //                    point[3*l + 1] = tDims[1] * (double)j/(double)(ctrlDims[1]-1);
    //                    point[3*l + 2] = tDims[2] * (double)k/(double)(ctrlDims[2]-1);
    //                }
    //            }
    //        }
    //        
    //        
    //        double[] gradient = new double[3*point.length];
    //        for (int i = 0; i < gradient.length; i++) {
    //            gradient[i] = 0;            
    //        }
    //        
    //        short[] mask = new short[nSrc];
    //        for (int i = 0; i < mask.length; i++) {
    //            mask[i] = Short.MAX_VALUE;            
    //        }
    //        float shortNorm = 1.f / Short.MAX_VALUE;
    //        
    //        double[][] coeffs = new double[4][density + 1];
    //        for (int j = 0; j <= density; j++) {
    //            float x = (float) j / density;
    //            coeffs[3][j] = (.5f * x - .5f) * x * x;
    //            coeffs[2][j] = ((-1.5f * x + 2.f) * x + .5f) * x;
    //            coeffs[1][j] = (1.5f * x - 2.5f) * x * x + 1.f;
    //            coeffs[0][j] = ((-.5f * x + 1.0f) * x - .5f) * x;
    //        }        
    //        double[] coeffsTmp = new double[4 * (density + 1)];
    //        for (int i = 0, l = 0; i < 4; i++) {
    //            for (int j = 0; j < (density + 1); j++, l++) {
    //                coeffsTmp[l] = coeffs[i][j];
    //            }
    //        }        
    //        double matchEnergy = 0;
    //        
    //        
    //        System.out.println("");
    //        System.out.println("start native_computeValGrad");
    //        long t0_computeValGrad = System.currentTimeMillis();
    //        matchEnergy = native_computeMatchValGradDP(point, gradient, ctrlDims,
    //                source, srcDims, target, tDims,
    //                mask, shortNorm, coeffsTmp, density);
    //        long t1_computeValGrad = System.currentTimeMillis();
    //        System.out.println("native_computeValGrad time (s): " + ((float) (t1_computeValGrad - t0_computeValGrad) / 1000.0f));
    //
    //        System.out.println("start java_computeValGrad");
    //        t0_computeValGrad = System.currentTimeMillis();
    //        double[] jmatchGradient = new double[gradient.length];
    //        for (int i = 0; i < jmatchGradient.length; i++) {
    //            jmatchGradient[i] = 0;
    //        }
    //        double jmatchEnergy = java_computeMatchValGradDP(point, jmatchGradient, ctrlDims,
    //                source, srcDims, target, tDims,
    //                mask, shortNorm, coeffsTmp, density);
    //        t1_computeValGrad = System.currentTimeMillis();
    //        System.out.println("java_computeValGrad time (s): " + ((float) (t1_computeValGrad - t0_computeValGrad) / 1000.0f));
    //
    //
    //        System.out.println("start computeValGrad comparison");
    //        System.out.println("native matchEnergy = "+matchEnergy);
    //        System.out.println("java matchEnergy = "+jmatchEnergy);
    //        //float eps = 0.0000000001f;
    //        float eps = 0.0000001f;
    //        int c = 0;
    //        for (int i = 0; i < jmatchGradient.length; i++) {
    //            if (Math.abs(jmatchGradient[i] - gradient[i]) > eps) {
    //                c++;
    //            }
    //        }
    //        System.out.println("gradient error count = " + c + "(of "+ gradient.length +")");
    //        
    //    }
    private static void java_doCompute(double[] energies, double[] point, double[] gradient, int[] ctrlDims,
                                       double[] source, int[] srcDims, double[] target, int[] tDims,
                                       double[] coeffsTmp, short[] mask, double shortNorm, int density,
                                       double deformationWeight, float[] spatialWeights,
                                       double volumeWeight, int[] boxOffTmp, double vol0)
    {

        energies[0] = java_computeMatchValGradDP(point, gradient, ctrlDims, source, srcDims, target, tDims, mask, shortNorm, coeffsTmp, density);
        if (deformationWeight != 0)
            energies[1] = java_computeDeformationValGrad(point, gradient, deformationWeight, ctrlDims, spatialWeights);
        if (volumeWeight != 0)
            energies[2] = java_computeVolumeDeformationValGrad(point, gradient, volumeWeight, ctrlDims, boxOffTmp, vol0);
    }

    private static native void native_doCompute(int nThreads, double[] energies, double[] point, double[] gradient, int[] ctrlDims,
                                                double[] source, int[] srcDims, double[] target, int[] tDims,
                                                double[] coeffsTmp, short[] mask, double shortNorm, int density,
                                                double deformationWeight, float[] spatialWeights,
                                                double volumeWeight, int[] boxOffTmp, double vol0);

}
