/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.numeric.fieldFitting;

import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.lib.utils.numeric.splines.SplineValueGradientDP;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class BilinearFieldFitting2DDP implements FieldFittingDP
{

    protected int[] srcDims;
    protected double[] source;
    protected int[] tDims;
    protected double[] target;
    protected int nThreads = 2;
    protected double deformationWeight = 10;
    protected double volumeWeight = .1f;
    protected int nCtrls = 4, nSrcs;
    protected double[] matchGradient;
    protected double[] deformationGradient;
    protected double matchEnergy, deformationEnergy, volumeEnergy;
    protected boolean gdebug = false;
    protected long totalMillis = 0;
    protected int evaluations = 0;
    protected int d0 = 0, d1 = 0;
    protected double[] spatialWeights = null;

    public BilinearFieldFitting2DDP(int[] srcDims, double[] source, int[] tDims, double[] target, int nThreads, double deformationWeight)
    {
        if (srcDims == null || srcDims.length != 2 ||
            source == null || source.length != srcDims[0] * srcDims[1]) {
            return;
        }
        if (tDims == null || tDims.length != 2 ||
            target == null || target.length != tDims[0] * tDims[1]) {
            return;
        }
        this.srcDims = srcDims;
        nSrcs = srcDims[0] * srcDims[1];
        this.source = source;
        this.tDims = tDims;
        this.target = target;
        int gradSize = 2 * 4;
        matchGradient = new double[gradSize];
        deformationGradient = new double[gradSize];
        d0 = tDims[0];
        d1 = tDims[1];
        this.deformationWeight = deformationWeight;
        this.nThreads = nThreads;
    }

    public double[] getDisplacedCoords(double[] coords)
    {
        //        double[] dispCoords = new double[3 * source.length];
        //        
        //        for (int j = 0, l = 0; j < srcDims[1]; j++) {
        //            double u = j / (srcDims[1] - 1.0);
        //            for (int k = 0; k < srcDims[0]; k++, l++) {
        //                double v = k / (srcDims[0] - 1.0);
        //                for (int m = 0; m < 3; m++) {
        //                    if(m < 2)
        //                        dispCoords[3 * l + m] = ((1 - u) * ((1 - v) * coords[m] + v * coords[ 2 + m])
        //                            + u * ((1 - v) * coords[ 4 + m] + v * coords[ 6 + m]));
        //                    else
        //                        dispCoords[3 * l + m] = 0.0;
        //                }
        //            }
        //        }
        double[] dispCoords = new double[2 * source.length];

        for (int j = 0, l = 0; j < srcDims[1]; j++) {
            double u = j / (srcDims[1] - 1.0);
            for (int k = 0; k < srcDims[0]; k++, l++) {
                double v = k / (srcDims[0] - 1.0);
                for (int m = 0; m < 2; m++) {
                    dispCoords[2 * l + m] = ((1 - u) * ((1 - v) * coords[m] + v * coords[2 + m]) +
                        u * ((1 - v) * coords[4 + m] + v * coords[6 + m]));
                }
            }
        }
        return dispCoords;
    }

    public void setSpatialWeights(float[] weights)
    {
    }

    private class ComputeValGrad implements Runnable
    {

        private double[] coords;
        private double[] matchGradient;
        private int iThread = 0;
        private double matchValue;

        ComputeValGrad(double[] coords, int nThreads, int iThread)
        {
            this.coords = coords;
            this.iThread = iThread;
            matchGradient = new double[coords.length];
        }

        public double[] getMatchGradient()
        {
            return matchGradient;
        }

        public double getMatchValue()
        {
            return matchValue;
        }

        public void run()
        {
            double[] pcoord = new double[2];
            double[] splineGradient = new double[2];

            matchValue = 0;
            for (int i = 0; i < coords.length; i++) {
                matchGradient[i] = 0;
            }

            int dj = (srcDims[1] - 1) / nThreads;
            int jstart = iThread * dj + Math.min(iThread, (srcDims[1] - 1) % nThreads);
            int jend = (iThread + 1) * dj + Math.min(iThread + 1, (srcDims[1] - 1) % nThreads);

            for (int j = jstart; j < jend; j++) {
                double u = j / (srcDims[1] - 1.0);
                for (int k = 0; k < srcDims[0]; k++) {
                    double v = k / (srcDims[0] - 1.0);
                    for (int m = 0; m < 2; m++) {
                        pcoord[m] = ((1 - u) * ((1 - v) * coords[m] + v * coords[2 + m]) +
                            u * ((1 - v) * coords[4 + m] + v * coords[6 + m]));
                    }
                    double splineVal = SplineValueGradientDP.getSplineValueGradient2D(target, tDims, pcoord, splineGradient);
                    double srcVal = source[j * srcDims[0] + k];
                    matchValue += (splineVal - srcVal) * (splineVal - srcVal);
                    for (int m = 0; m < 2; m++) {
                        double s = splineGradient[m] * (splineVal - srcVal);
                        matchGradient[m] += (1 - u) * (1 - v) * s;
                        matchGradient[m + 2] += (1 - u) * v * s;
                        matchGradient[m + 4] += u * (1 - v) * s;
                        matchGradient[m + 6] += u * v * s;
                    }
                }
            }
        }
    }

    /**
     * computes energy (sum over source points of (t(F(x)-s(x))^2)) and energy gradient with respect to control points
     * <p>
     * @param ctrlCoords - target coordinates for control points in target index space
     *                   (ctrlCoords.length = 3*srcDims[0]*srcDims[1]*srcDims[2]) - independent variables
     * @param gradient   - array for storing gradient - must be preallocated of length ctrlCoords.length.
     * <p>
     * @return energy value (if negative - wrong input data)
     */
    private double computeDeformationValGrad(double[] point, double[] gradient)
    {
        if (deformationWeight == 0) {
            return 0;
        }
        double[] w = new double[2];
        double sw;
        double dw = deformationWeight / (double) nCtrls;
        double smoothEnergy = 0;

        double wnorm = 0;
        /* 
         * 
         *      + ======-
         *      |       |
         *      |       |
         *      |       |
         *      +=======-
         */
        for (int j = 0; j < 2; j++) {
            w[j] = point[6 + j] - point[4 + j] - point[2 + j] + point[j];
            wnorm += w[j] * w[j];
        }
        sw = 2;
        if (spatialWeights != null) {
            sw = 2 * spatialWeights[0];
        }
        smoothEnergy += sw * wnorm;
        sw *= 2;
        for (int j = 0; j < 2; j++) {
            gradient[6 + j] += sw * w[j] * dw;
            gradient[4 + j] -= sw * w[j] * dw;
            gradient[2 + j] -= sw * w[j] * dw;
            gradient[j] += sw * w[j] * dw;
        }

        smoothEnergy *= dw;
        return smoothEnergy;
    }

    public double computeValGrad(double[] point, double[] gradient)
    {
        long t0 = System.currentTimeMillis();
        matchEnergy = 0;
        for (int i = 0; i < gradient.length; i++) {
            gradient[i] = matchGradient[i] = deformationGradient[i] = 0;
        }

        ComputeValGrad[] computeThreads = new ComputeValGrad[nThreads];
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            computeThreads[iThread] = new ComputeValGrad(point, nThreads, iThread);
            workThreads[iThread] = new Thread(computeThreads[iThread]);
            workThreads[iThread].start();
        }
        for (int iThread = 0; iThread < workThreads.length; iThread++) {
            try {
                workThreads[iThread].join();
            } catch (Exception e) {
            }
        }
        for (int iThread = 0; iThread < nThreads; iThread++) {
            matchEnergy += computeThreads[iThread].getMatchValue();
            double[] thrGr = computeThreads[iThread].getMatchGradient();
            for (int i = 0; i < thrGr.length; i++) {
                matchGradient[i] += thrGr[i];
            }
        }
        matchEnergy /= nSrcs;
        deformationEnergy = computeDeformationValGrad(point, deformationGradient);

        for (int i = 0; i < gradient.length; i++) {
            matchGradient[i] /= nSrcs;
            gradient[i] = deformationGradient[i] + matchGradient[i];
        }

        //System.out.printf("  matchEnergy = %8.3f   deformationEnergy = %8.3f    volumeEnergy = %8.3f     total energy = %8.3f%n",
        //                 matchEnergy, deformationEnergy, volumeEnergy, matchEnergy + deformationEnergy + volumeEnergy);
        evaluations += 1;
        totalMillis += System.currentTimeMillis() - t0;
        return matchEnergy + deformationEnergy;
    }

    public void setDeformationWeight(double deformationWeight)
    {
        this.deformationWeight = deformationWeight;
    }

    public void setVolumeWeight(double volumeWeight)
    {
        this.volumeWeight = volumeWeight;
    }

    public void setNThreads(int nThreads)
    {
        this.nThreads = nThreads;
    }

    public double getDeformationEnergy()
    {
        return deformationEnergy;
    }

    public double[] getDeformationGradient()
    {
        return deformationGradient;
    }

    public double getVolumeEnergy()
    {
        return volumeEnergy;
    }

    public double getMatchEnergy()
    {
        return matchEnergy;
    }

    public int getEvaluations()
    {
        return evaluations;
    }

    public long getTotalMillis()
    {
        return totalMillis;
    }

    public void resetTimer()
    {
        totalMillis = 0;
        evaluations = 0;
    }

    public String[] getVarNames()
    {
        return new String[]{"matchEnergy", "deformationEnergy", "volumeEnergy"};
    }

    public double[] getVariables()
    {
        return new double[]{matchEnergy, deformationEnergy, volumeEnergy};
    }
}
