package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1DNew;

import java.awt.Component;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1DNew.ExtendedChart.ExtendedChartPanel;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.plaf.IconUIResource;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1DNew.Resources.NodeIcon;

/**
 *
 * @author norkap
 */
public class Viewer1DFrame extends JFrame
{

    private final JMenuBar menuBar;
    private final JMenuItem addChartMenuItem;
    private final JButton addChartButton;
    private final JScrollPane scrollPane;
    private final JPanel mainPanel;

    public Viewer1DFrame()
    {
        setLayout(new GridBagLayout());
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(810, 200));
        setPreferredSize(new Dimension(810, 400));
        setIconImage(new ImageIcon(getClass().getResource("/pl/edu/icm/visnow/gui/icons/big/visnow.png")).getImage());
        
        menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");

        addChartMenuItem = new JMenuItem("Add Chart");

        JMenuItem menuItem3 = new JMenuItem("Exit");
        menuItem3.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                dispose();
            }
        });

        menu.add(addChartMenuItem);
        menu.addSeparator();
        menu.add(menuItem3);
        menuBar.add(menu);
        setJMenuBar(menuBar);

        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        scrollPane = new JScrollPane(mainPanel);
        scrollPane.setPreferredSize(new Dimension(1000, 400));

        GridBagConstraints gridBC = new GridBagConstraints();
        gridBC.weightx = 1;
        gridBC.weighty = 1;
        gridBC.fill = GridBagConstraints.BOTH;
        add(scrollPane, gridBC);
        
        
        addChartButton = new JButton("Add Chart"); 
        Icon icon = new IconUIResource(new NodeIcon('+'));
        addChartButton.setIcon(icon);
        
        gridBC = new GridBagConstraints();
        gridBC.gridy = 2;
        gridBC.weightx = 1;
        gridBC.weighty = 0;
        gridBC.fill = GridBagConstraints.HORIZONTAL;
        add(addChartButton, gridBC);
    }

    public int getComponentIndex(Component comp)
    {
        for (int i = 0; i < mainPanel.getComponentCount(); i++) {
            if (comp == mainPanel.getComponent(i))
                return i;
        }
        return -1;
    }

    public Component getComponent(String name)
    {
        for (int i = 0; i < getContentPane().getComponentCount(); i++) {
            if (name.equals(getContentPane().getComponent(i).getName()))
                return getContentPane().getComponent(i);
        }
        return null;
    }

    public void addChartToDisplay(ExtendedChartPanel chart)
    {
        GridBagConstraints gridBC = new GridBagConstraints();
        gridBC.weightx = 1;
        gridBC.weighty = 1;
        gridBC.fill = GridBagConstraints.BOTH;
        gridBC.gridx = 0;
        mainPanel.add(chart, gridBC);
        Dimension dims = getMinimumSize();
        mainPanel.setMinimumSize(new Dimension(800, dims.height + 200));

        validate();
    }

    public void removeChartFromDisplay(ExtendedChartPanel chart)
    {
        mainPanel.remove(chart);
        Dimension minDims = getMinimumSize();
        mainPanel.setMinimumSize(new Dimension(800, minDims.height - 200));
        validate();
        repaint();
    }

    public void addChartActionListener(ActionListener addChartAL)
    {
        addChartMenuItem.addActionListener(addChartAL);
        addChartButton.addActionListener(addChartAL);
    }

}
