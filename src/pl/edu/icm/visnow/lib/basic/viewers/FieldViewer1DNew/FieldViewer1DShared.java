package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1DNew;

import org.jfree.data.xy.XYSeriesCollection;
import pl.edu.icm.visnow.engine.core.ParameterName;

/**
 *
 * @author Norbert_2
 */


public class FieldViewer1DShared {
    public static final ParameterName<XYSeriesCollection> DATA_SET = new ParameterName("data set");

}
