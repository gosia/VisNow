package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer3D.Geometry;

import javax.swing.table.DefaultTableCellRenderer;

public class FloatValueTableCellRenderer extends DefaultTableCellRenderer
{

    private String format = "%.2f";
    
    public FloatValueTableCellRenderer()
    {
        super();
    }

    public FloatValueTableCellRenderer(String format)
    {
        this();
        this.format = format;
    }
    
    @Override
    public void setValue(Object value)
    {
        setText((value == null) ? "" : String.format(format, (Float)value));
    }
}
