/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.AbstractCellEditor;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import pl.edu.icm.visnow.geometries.viewer3d.Display3DPanel;

public class GraphsTable extends JTable
{

    protected Vector<Vector<GraphArea>> graphs = new Vector<Vector<GraphArea>>();
    protected Vector<String> colNames = new Vector<String>();

    public GraphsTable()
    {
        graphs.add(new Vector<GraphArea>());
        graphs.get(0).add(new GraphArea());
        graphs.add(new Vector<GraphArea>());
        graphs.get(1).add(new GraphArea());
        colNames.add(" ");
        setModel(new GraphsTableModel(graphs, colNames));
        DefaultTableColumnModel colModel = (DefaultTableColumnModel) getColumnModel();
        colModel.getColumn(0).setCellRenderer(new GraphRenderer());
        colModel.getColumn(0).setCellEditor(new GraphEditor());
        setRowHeight(200);
        setTableHeader(new JTableHeader(columnModel));
        sizeColumnsToFit(-1);
    }

    public GraphsTable(Display3DPanel panel)
    {
        if (panel == null)
            return;
        setRowHeight(200);
        setTableHeader(new JTableHeader(columnModel));
        sizeColumnsToFit(-1);
    }

    class GraphEditor
        extends AbstractCellEditor
        implements TableCellEditor,
        ActionListener
    {

        JToggleButton button;

        public GraphEditor()
        {
            //Set up the editor (from the table's point of view),
            //which is a button.
            //This button brings up the color chooser dialog,
            //which is the editor from the user's point of view.
            button = new JToggleButton();
            button.setToolTipText("<html>click to move light in 3D window<p>click again when done</html>");
            button.addActionListener(this);
            button.setBorderPainted(false);
        }

        public void actionPerformed(ActionEvent e)
        {
            if (!button.isSelected())
                fireEditingStopped();
        }

        //Implement the one CellEditor method that AbstractCellEditor doesn't.
        public Object getCellEditorValue()
        {
            return button.isSelected();
        }

        //Implement the one method defined by TableCellEditor.
        public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                     int column)
        {
            button.setText("editing ");
            return button;
        }
    }

    class GraphRenderer extends GraphArea implements TableCellRenderer
    {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
            setOpaque(true);
            return graphs.get(row).firstElement();
        }
    }

    class GraphsTableModel extends AbstractTableModel
    {

        protected int columns = 1;
        protected int selectedRow = -1;
        protected int selectedColumn = -1;
        protected Vector<Vector<GraphArea>> data;
        protected Vector<String> columnNames;

        public GraphsTableModel(Vector<Vector<GraphArea>> data, Vector<String> columnNames)
        {
            super();
            this.data = data;
            this.columnNames = columnNames;
        }

        public int getColumnCount()
        {
            return columnNames.size();
        }

        public int getRowCount()
        {
            return data.size();
        }

        @Override
        public String getColumnName(int col)
        {
            return columnNames.get(col);
        }

        public Object getValueAt(int row, int col)
        {
            return data.get(row).get(col);
        }

        @Override
        public Class getColumnClass(int c)
        {
            return GraphArea.class;
        }

        @Override
        public boolean isCellEditable(int row, int col)
        {
            return true;
        }
    }

    /**
     * Create the GUI and show it. For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public static void main(String[] args)
    {
        GraphsTable t = new GraphsTable();
        final JScrollPane p = new JScrollPane(t);
        p.setHorizontalScrollBarPolicy(p.HORIZONTAL_SCROLLBAR_NEVER);
        javax.swing.SwingUtilities.invokeLater(new Runnable()
        {

            public void run()
            {
                JOptionPane.showMessageDialog(null, p);
            }
        });
    }
}
