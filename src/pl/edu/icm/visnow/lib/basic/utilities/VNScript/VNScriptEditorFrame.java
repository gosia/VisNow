/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.utilities.VNScript;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.tree.DefaultMutableTreeNode;
import org.antlr.runtime.RecognitionException;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.VNData;
import pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets.*;
import pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets.LogMessage.LogMessageType;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.viscript.CompiledViscript;
import pl.edu.icm.visnow.viscript.Viscript2;
import pl.edu.icm.visnow.viscript.ViscriptCompilator;
import pl.edu.icm.visnow.viscript.types.VNScriptContext;
import pl.edu.icm.visnow.viscript.types.VNScriptType;
import pl.edu.icm.visnow.viscript.types.VNScriptVariable;

/**
 *
 * @author lyczek
 */
public class VNScriptEditorFrame extends javax.swing.JFrame
{

    private static final String exampleSource = "" +
        "// Przykladowy skrypt pokazujacy istotna czesc mozliwosci  \n" +
        "// jezyka. Do poprawnego dziala wymaga podpiecia Test Regular Field  \n" +
        "\n" +
        "// chwilowo  \n" +
        "// (docelowo odwolanie do dimsow bedzie przyjemniejsze: \n" +
        "// gaussians.dims[0]) \n" +
        "d0 := _gaussians_dims_0; // deklaracja skalara\n" +
        "d1 := _gaussians_dims_1; \n" +
        "d2 := _gaussians_dims_2;  \n" +
        "\n" +
        "// tworzymy dwa razy wieksze pole \n" +
        "tab := array(2 * d0,  2 * d0, 2 *d0);  // deklaracja tablicy\n" +
        "\n" +
        "// kilkukrotne kopiowanie gaussians \n" +
        "tab[:d0, :d1, :d2] = gaussians * gaussians_bits; \n" +
        "tab[d0:, :d1, :d2] = gaussians * gaussians_bits; \n" +
        "tab[:d0, d1:, :d2] = gaussians * gaussians_bits; \n" +
        "tab[:d0, :d1, d2:] = gaussians * gaussians_bits; \n" +
        "tab[d0:, d1:, :d2] = gaussians * gaussians_bits; \n" +
        "tab[:d0, d1:, d2:] = gaussians * gaussians_bits; \n" +
        "tab[d0:, :d1, d2:] = gaussians * gaussians_bits; \n" +
        "\n" +
        "// wyznaczenie kwadratu modulu pola wektorowego vortex \n" +
        "tab[d0:, d1:, d2:] = sqrt(vortex.x * vortex.x + vortex.y * vortex.y + vortex.z * vortex.z) / 6;  \n" +
        "\n" +
        "// proste progowanie (przy okazji wykorzystano adresowanie wzgledne)\n" +
        "tab[d0:, d1:, d2:] = tab[+d0, +d1, +d2] < 0.5 ? 0 : tab[+d0, +d1, +d2];\n" +
        "// bezposrednie odwolanie do elementu tablicy\n" +
        "tab[d0, d1, d2] = 1; \n" +
        "tab[8:12, 8:12, 8:12] = tab[d0, d1, d2] / 2; // i jeszcze jeden przyklad (taki komentarz tez dziala!)  \n" +
        "\n" +
        "/* oczywiscie taki komentarz   \n" +
        " * tez jest dozwolony :)  \n" +
        " */  \n" +
        "\n" +
        "// przekazujemy tab na wyjscie \n" +
        "output(tab);\n";
    private boolean vnScriptSourceChanged;
    private boolean javaSourceChanged;
    private boolean javaSourceAvailable;
    private boolean isCompiled;
    private boolean isScript;
    private Viscript2 viscript;
    private CompiledViscript compiledViscript;
    private VNScript vns;
    private InputFieldsTableTreeModel inputFieldsModel;
    private VNScriptLog log;

    public VNScriptEditorFrame()
    {
        this(null);
    }

    /**
     * Creates new form VNScriptEditorFrame
     */
    public VNScriptEditorFrame(VNScript vns)
    {
        this.javaSourceChanged = false;
        this.vnScriptSourceChanged = false;
        this.javaSourceAvailable = false;
        this.isCompiled = false;
        this.isScript = false;
        this.vns = vns;
        this.inputFieldsModel = new InputFieldsTableTreeModel();
        this.log = new VNScriptLog();

        initComponents();
        //        DefaultSyntaxKit.initKit();

        this.jTreeTable1.setDefaultRenderer(String.class, new StringRenderer());
        this.jTreeTable1.setDefaultRenderer(UsedState.class, new UsedStateRenderer());
        this.jTreeTable1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        this.jTreeTable1.getColumnModel().getColumn(0).setPreferredWidth(35);
        this.jTreeTable1.getColumnModel().getColumn(0).setResizable(false);
        this.jTreeTable1.getColumnModel().getColumn(1).setPreferredWidth(300);
        this.jTreeTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
        this.jTreeTable1.getColumnModel().getColumn(2).setResizable(false);
        this.jTreeTable1.getColumnModel().getColumn(3).setPreferredWidth(200);
        this.jTreeTable1.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
        this.jTreeTable1.setDragEnabled(false);

        this.jTable1.setRowHeight(20);
        this.jTable1.setDragEnabled(false);
        this.jTable1.setDefaultRenderer(LogMessageType.class, new LogMessageTypeRenderer());

        epJavaSource.setContentType("text/java");
        epJavaSource.getDocument().addDocumentListener(new DocumentListener()
        {

            public void insertUpdate(DocumentEvent e)
            {
                VNScriptEditorFrame.this.javaSourceChanged = true;
                VNScriptEditorFrame.this.tpSources.setTitleAt(1, "java source *");
            }

            public void removeUpdate(DocumentEvent e)
            {
                VNScriptEditorFrame.this.javaSourceChanged = true;
                VNScriptEditorFrame.this.tpSources.setTitleAt(1, "java source *");
            }

            public void changedUpdate(DocumentEvent e)
            {
                VNScriptEditorFrame.this.javaSourceChanged = true;
                VNScriptEditorFrame.this.tpSources.setTitleAt(1, "java source *");
            }
        });

        epVNScriptSource.setContentType("text/java");
        epVNScriptSource.setText(exampleSource);
        epVNScriptSource.getDocument().addDocumentListener(new DocumentListener()
        {

            public void insertUpdate(DocumentEvent e)
            {
                VNScriptEditorFrame.this.vnScriptSourceChanged = true;
                VNScriptEditorFrame.this.tpSources.setTitleAt(0, "vn script source *");
            }

            public void removeUpdate(DocumentEvent e)
            {
                VNScriptEditorFrame.this.vnScriptSourceChanged = true;
                VNScriptEditorFrame.this.tpSources.setTitleAt(0, "vn script source *");
            }

            public void changedUpdate(DocumentEvent e)
            {
                VNScriptEditorFrame.this.vnScriptSourceChanged = true;
                VNScriptEditorFrame.this.tpSources.setTitleAt(0, "vn script source *");
            }
        });

        tpSources.setEnabledAt(1, false);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        tpSources = new javax.swing.JTabbedPane();
        spVNScriptSource = new javax.swing.JScrollPane();
        epVNScriptSource = new javax.swing.JEditorPane();
        spJavaSource = new javax.swing.JScrollPane();
        epJavaSource = new javax.swing.JEditorPane();
        btnLoadVNScriptSource = new javax.swing.JButton();
        btnSaveVNScriptSource = new javax.swing.JButton();
        btnSaveJavaSource = new javax.swing.JButton();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTreeTable1 = new pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets.JTreeTable();
        btnRun = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        jButton1.setText("translate");
        jButton1.setName("jButton1"); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        tpSources.setName("tpSources"); // NOI18N

        spVNScriptSource.setName("spVNScriptSource"); // NOI18N

        epVNScriptSource.setName("epVNScriptSource"); // NOI18N
        spVNScriptSource.setViewportView(epVNScriptSource);

        tpSources.addTab("vn script source", spVNScriptSource);

        spJavaSource.setName("spJavaSource"); // NOI18N

        epJavaSource.setContentType("text/java");
        epJavaSource.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        epJavaSource.setName("epJavaSource"); // NOI18N
        spJavaSource.setViewportView(epJavaSource);

        tpSources.addTab("java source", spJavaSource);

        btnLoadVNScriptSource.setText("load");
        btnLoadVNScriptSource.setName("btnLoadVNScriptSource"); // NOI18N
        btnLoadVNScriptSource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadVNScriptSourceActionPerformed(evt);
            }
        });

        btnSaveVNScriptSource.setText("save");
        btnSaveVNScriptSource.setName("btnSaveVNScriptSource"); // NOI18N
        btnSaveVNScriptSource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveVNScriptSourceActionPerformed(evt);
            }
        });

        btnSaveJavaSource.setText("save java source");
        btnSaveJavaSource.setName("btnSaveJavaSource"); // NOI18N
        btnSaveJavaSource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveJavaSourceActionPerformed(evt);
            }
        });

        jTabbedPane2.setName("jTabbedPane2"); // NOI18N

        jScrollPane3.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane3.setName("jScrollPane3"); // NOI18N

        jTreeTable1.setModel(this.inputFieldsModel);
        jTreeTable1.setName("jTreeTable1"); // NOI18N
        jScrollPane3.setViewportView(jTreeTable1);

        jTabbedPane2.addTab("input fields", jScrollPane3);

        btnRun.setText("run");
        btnRun.setName("btnRun"); // NOI18N
        btnRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRunActionPerformed(evt);
            }
        });

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jTable1.setModel(log.getTableModel());
        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_LAST_COLUMN);
        jTable1.setName("jTable1"); // NOI18N
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getColumn(0).setResizable(false);
        jTable1.getColumnModel().getColumn(0).setPreferredWidth(32);
        jTable1.getColumnModel().getColumn(1).setResizable(false);
        jTable1.getColumnModel().getColumn(1).setPreferredWidth(100);
        jTable1.getColumnModel().getColumn(2).setResizable(false);
        jTable1.getColumnModel().getColumn(2).setPreferredWidth(1000);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(tpSources, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 799, Short.MAX_VALUE)
                                        .addComponent(jTabbedPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 799, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(btnLoadVNScriptSource)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(btnSaveVNScriptSource)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(btnSaveJavaSource)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 378, Short.MAX_VALUE)
                                                .addComponent(jButton1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(btnRun))
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 799, Short.MAX_VALUE))
                                .addContainerGap())
                );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tpSources, javax.swing.GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnRun)
                                        .addComponent(jButton1)
                                        .addComponent(btnLoadVNScriptSource, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnSaveVNScriptSource, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnSaveJavaSource, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRunActionPerformed
        runScript();
    }//GEN-LAST:event_btnRunActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton1ActionPerformed
    {//GEN-HEADEREND:event_jButton1ActionPerformed
        translateScript();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnLoadVNScriptSourceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnLoadVNScriptSourceActionPerformed
    {//GEN-HEADEREND:event_btnLoadVNScriptSourceActionPerformed
        loadScript();
    }//GEN-LAST:event_btnLoadVNScriptSourceActionPerformed

    private void btnSaveVNScriptSourceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnSaveVNScriptSourceActionPerformed
    {//GEN-HEADEREND:event_btnSaveVNScriptSourceActionPerformed
        saveScript();
    }//GEN-LAST:event_btnSaveVNScriptSourceActionPerformed

    private void btnSaveJavaSourceActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnSaveJavaSourceActionPerformed
    {//GEN-HEADEREND:event_btnSaveJavaSourceActionPerformed
        saveJavaSource();
    }//GEN-LAST:event_btnSaveJavaSourceActionPerformed

    public void setInField(Vector<VNData> inFields)
    {
        inputFieldsModel.updateModel(inFields);
        vnScriptSourceChanged = true;
    }

    public void saveScript()
    {
        // TODO:
    }

    public void saveJavaSource()
    {
        // TODO:
    }

    public void loadScript()
    {
        // TODO:
    }

    public void runScript()
    {
        log.clear();

        translateScript();
        compileScript();

        if (!log.isError()) {
            Map<String, Object> bidings = new HashMap<String, Object>();
            // loop over fields
            DefaultMutableTreeNode root = (DefaultMutableTreeNode) inputFieldsModel.getRoot();
            for (int i = 0; i < root.getChildCount(); i++) {
                FieldNode fn = (FieldNode) root.getChildAt(i);
                // loop over components
                for (int j = 0; j < fn.getChildCount(); j++) {
                    ComponentNode cn = (ComponentNode) fn.getChildAt(j);
                    bidings.put(cn.getAlias(), cn.getData());
                    bidings.put(cn.getAlias() + "_dims", fn.getDims());
                }
            }

            try {
                Map<String, Object> outputBidings = compiledViscript.run(bidings);

                if (outputBidings.containsKey("_dims")) {
                    RegularField rF = new RegularField((int[]) outputBidings.get("_dims"));

                    for (Entry<String, Object> e : outputBidings.entrySet()) {
                        if (!e.getKey().equals("_dims")) {
                            rF.addComponent(DataArray.create((float[]) e.getValue(), 1, e.getKey()));
                        }
                    }

                    vns.setOutputValue("outField", new VNRegularField(rF));
                }
            } catch (Throwable ex) {
                log.addMessage(new LogMessage("Error running script" + (ex.getMessage() != null ? ": " + ex.getMessage() : ""), LogMessageType.ERROR));
            }

            if (!log.isError()) {
                log.addMessage(new LogMessage("Script runned successfully", LogMessage.LogMessageType.INFO));
            }
        }
    }

    public void translateScript()
    {

        if (vnScriptSourceChanged || !isScript) {
            try {
                isScript = true;
                log.clear();
                VNScriptContext context = new VNScriptContext(null);
                // loop over fields
                DefaultMutableTreeNode root = (DefaultMutableTreeNode) inputFieldsModel.getRoot();

                for (int i = 0; i < root.getChildCount(); i++) {
                    FieldNode fn = (FieldNode) root.getChildAt(i);
                    // loop over components
                    for (int j = 0; j < fn.getChildCount(); j++) {
                        ComponentNode cn = (ComponentNode) fn.getChildAt(j);
                        context.putVariable(cn.getAlias(), new VNScriptVariable(VNScriptType.ARRAY, cn.getVectorLength(), fn.getDimensions(), true));
                    }
                }
                viscript = new Viscript2(epVNScriptSource.getText(), "MyScript", context);
                viscript.setLog(log);
                viscript.parse();
                epJavaSource.setText(viscript.getJavaSource());
                javaSourceChanged = false;
                vnScriptSourceChanged = false;
                isCompiled = false;
                tpSources.setTitleAt(1, "java source");
                tpSources.setTitleAt(0, "vn script source");
                javaSourceAvailable = true;
                btnSaveJavaSource.setEnabled(true);
                tpSources.setEnabledAt(1, true);
                inputFieldsModel.updateComponentUsage(context);
            } catch (IOException ex) {
                log.addMessage(new LogMessage("Error translating script: " + ex.getMessage(), LogMessage.LogMessageType.ERROR));
                //                ex.printStackTrace();
            } catch (RecognitionException ex) {
                log.addMessage(new LogMessage("Error translating script: " + ex.getMessage(), Integer.toString(ex.line), LogMessage.LogMessageType.ERROR));
                //                ex.printStackTrace();
            } catch (Exception ex) {
                log.addMessage(new LogMessage("Error translating script: " + ex.getMessage(), LogMessage.LogMessageType.ERROR));
                //                ex.printStackTrace();
            } catch (Throwable ex) {
                log.addMessage(new LogMessage("Error translating script: " + ex.getMessage(), LogMessage.LogMessageType.ERROR));
                //                ex.printStackTrace();
            }
            if (!log.isError()) {
                log.addMessage(new LogMessage("Script translated successfully", LogMessage.LogMessageType.INFO));
            }
        }
    }

    public void compileScript()
    {
        if (javaSourceChanged || !isCompiled) {
            try {
                isCompiled = true;
                ViscriptCompilator vc = new ViscriptCompilator();
                compiledViscript = vc.compile("MyScript", epJavaSource.getText());
            } catch (Exception ex) {
                log.addMessage(new LogMessage("Error compiling script", LogMessage.LogMessageType.ERROR));
                ex.printStackTrace();
            }
        }
        if (!log.isError()) {
            log.addMessage(new LogMessage("Script compiled successfully", LogMessage.LogMessageType.INFO));
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLoadVNScriptSource;
    private javax.swing.JButton btnRun;
    private javax.swing.JButton btnSaveJavaSource;
    private javax.swing.JButton btnSaveVNScriptSource;
    private javax.swing.JEditorPane epJavaSource;
    private javax.swing.JEditorPane epVNScriptSource;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable jTable1;
    private pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets.JTreeTable jTreeTable1;
    private javax.swing.JScrollPane spJavaSource;
    private javax.swing.JScrollPane spVNScriptSource;
    private javax.swing.JTabbedPane tpSources;
    // End of variables declaration//GEN-END:variables
}
