/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets;

import java.util.ArrayList;
import java.util.Collection;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.VNData;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.viscript.types.VNScriptContext;
import pl.edu.icm.visnow.viscript.types.VNScriptUtil;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class InputFieldsTableTreeModel implements TreeTableModel
{

    private DefaultMutableTreeNode root;
    private String[] columnNames = new String[]{"Used", "Name", "Size / Veclen", "Alias"};
    private Class[] columnClasses = new Class[]{UsedState.class, TreeTableModel.class, String.class, String.class};

    public InputFieldsTableTreeModel()
    {
        this.treeModelListeners = new ArrayList<TreeModelListener>();
        this.root = new DefaultMutableTreeNode();
    }

    public InputFieldsTableTreeModel(Collection<VNData> inputs)
    {
        this();

        updateModel(inputs);
    }

    public final void updateModel(Collection<VNData> inputs)
    {
        this.root.removeAllChildren();

        // TODO: fix input component list (add _x, for duplicates)
        for (VNData vnd : inputs) {
            if (vnd.getValue() instanceof VNRegularField) {
                VNRegularField vnrf = (VNRegularField) vnd.getValue();
                FieldNode fn = new FieldNode(vnd.getSourceModule().getName(), ((RegularField) vnrf.getField()).getDims());
                for (DataArray da : vnrf.getField().getComponents()) {
                    ComponentNode cn = new ComponentNode(da, da.getName(), VNScriptUtil.fixArrayName(da.getName()), da.getVectorLength(), false);
                    fn.add(cn);
                }
                this.root.add(fn);
            }
        }
        fireTreeModelChanged();
    }

    public void updateComponentUsage(VNScriptContext context)
    {
        for (int i = 0; i < root.getChildCount(); i++) {
            FieldNode fn = (FieldNode) root.getChildAt(i);
            // loop over components
            for (int j = 0; j < fn.getChildCount(); j++) {
                ComponentNode cn = (ComponentNode) fn.getChildAt(j);
                cn.setUsed(context.getVariable(cn.getAlias()).isUsed());
            }
        }
        fireTreeModelChanged();
    }

    @Override
    public int getColumnCount()
    {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }

    @Override
    public Class getColumnClass(int column)
    {
        return columnClasses[column];
    }

    @Override
    public Object getValueAt(Object node, int column)
    {
        if (node instanceof ComponentNode) {
            switch (column) {
                case 0:
                    return ((ComponentNode) node).isUsed();
                case 1:
                    return ((ComponentNode) node).getName();
                case 2:
                    return (Integer.toString(((ComponentNode) node).getVectorLength()));
                case 3:
                    return ((ComponentNode) node).getAlias();
                default:
                    return null;
            }
        } else if (node instanceof FieldNode) {
            switch (column) {
                case 0:
                    return ((FieldNode) node).isUsed();
                case 1:
                    return ((FieldNode) node).getName();
                case 2:
                    return ((FieldNode) node).getDimsString();
                case 3:
                    return "";
                default:
                    return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public boolean isCellEditable(Object node, int column)
    {
        return ((node instanceof ComponentNode) && column == 3) || (node instanceof FieldNode && column == 1);
    }

    @Override
    public void setValueAt(Object aValue, Object node, int column)
    {
        if (node instanceof ComponentNode) {
            if (column == 3) {
                ((ComponentNode) node).setAlias((String) aValue);
            }
        }
    }

    @Override
    public Object getRoot()
    {
        return root;
    }

    @Override
    public Object getChild(Object parent, int index)
    {
        return ((TreeNode) parent).getChildAt(index);
    }

    @Override
    public int getChildCount(Object parent)
    {
        return ((TreeNode) parent).getChildCount();
    }

    @Override
    public boolean isLeaf(Object node)
    {
        return (node instanceof ComponentNode);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue)
    {
    }

    @Override
    public int getIndexOfChild(Object parent, Object child)
    {
        return ((TreeNode) parent).getIndex((TreeNode) child);
    }

    private final ArrayList<TreeModelListener> treeModelListeners;

    @Override
    public void addTreeModelListener(TreeModelListener l)
    {
        treeModelListeners.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l)
    {
        treeModelListeners.remove(l);
    }

    public void fireTreeModelChanged()
    {
        for (TreeModelListener tml : treeModelListeners) {
            tml.treeStructureChanged(new TreeModelEvent(this, new TreePath(root)));
        }
    }
}
