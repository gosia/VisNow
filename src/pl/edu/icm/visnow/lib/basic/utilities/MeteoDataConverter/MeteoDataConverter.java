//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.utilities.MeteoDataConverter;

import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.ModuleCore;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.types.VNRegularField;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class MeteoDataConverter extends ModuleCore
{

    /**
     * Creates a new instance of CreateGrid
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected GUI gui = null;
    protected Field inField = null;
    protected Field outField = null;
    protected RegularField outRegularField = null;
    protected Params params;
    protected boolean fromUI = false;

    public MeteoDataConverter()
    {
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null || ((VNField) getInputFirstValue("inField")).getField() == null)
            return;
        inField = ((VNField) getInputFirstValue("inField")).getField();
        outField = inField.cloneShallow();
        outField.removeComponents();
        for (int iData = 0; iData < inField.getNComponents(); iData++) {
            DataArray da = inField.getComponent(iData);
            if (da.isNumeric() && "K".equalsIgnoreCase(da.getUnit())) {
                TimeData indta = da.getTimeData();
                TimeData outdta = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                float inCTime = da.getCurrentTime();
                for (int i = 0; i < indta.getNSteps(); i++) {
                    da.setCurrentTime(da.getTime(i));
                    float[] in = (float[])da.getRawArray().getData();
                    float[] out = new float[in.length];
                    for (int j = 0; j < out.length; j++)
                        out[j] = in[j] - 273.16f;
                    outdta.setValue(new FloatLargeArray(out), da.getTime(i));
                }
                da.setCurrentTime(inCTime);
                DataArray outda = DataArray.create(DataArrayType.FIELD_DATA_FLOAT, da.getNElements(), da.getVectorLength(), da.getName(), "C", da.getUserData());
                outda.setTimeData(outdta);
                outda.setCurrentTime(inCTime);
                outda.setPreferredRange(-50, 50);
                outField.addComponent(outda);
            } else if (da.isNumeric() && "Pa".equalsIgnoreCase(da.getUnit())) {
                TimeData indta = da.getTimeData();
                TimeData outdta = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                float inCTime = da.getCurrentTime();
                for (int i = 0; i < indta.getNSteps(); i++) {
                    da.setCurrentTime(da.getTime(i));
                    float[] in = (float[])da.getRawArray().getData();
                    float[] out = new float[in.length];
                    for (int j = 0; j < out.length; j++)
                        out[j] = in[j] / 100;
                    outdta.setValue(new FloatLargeArray(out), da.getTime(i));
                }
                da.setCurrentTime(inCTime);
                DataArray outda = DataArray.create(DataArrayType.FIELD_DATA_FLOAT, da.getNElements(), da.getVectorLength(), da.getName(), "hPa", da.getUserData());
                outda.setTimeData(outdta);
                outda.setCurrentTime(inCTime);
                outda.setPreferredRange(950, 1050);
                outField.addComponent(outda);
            } else {
                outField.addComponent(da);
                continue;
            }
        }

        if (outField != null && outField instanceof RegularField) {
            outRegularField = (RegularField) outField;
            setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
            setOutputValue("outIrregularField", null);
        } else if (outField != null && outField instanceof IrregularField) {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
        } else {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
        }

    }

}
