//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.testdata.TestRegularField;

import pl.edu.icm.visnow.engine.core.ProgressAgent;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.system.main.VisNow;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import static pl.edu.icm.visnow.lib.basic.testdata.TestRegularField.TestRegularFieldShared.*;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.geometries.parameters.RenderingParams;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class TestRegularField extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(TestRegularField.class);
    //module elements
    public static OutputEgg[] outputEggs = null;
    protected GUI computeUI = null;
    protected long randomSeed = 0;
    protected double[][] randoms = new double[256][5];

    public TestRegularField()
    {
        super();
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameterProxy(parameters);
                ui.addComputeGUI(computeUI);
            }
        });

        Random random = new Random(randomSeed);

        for (int i = 0; i < randoms.length; i++) {
            randoms[i][0] = 2 * random.nextDouble() - 1;
            randoms[i][1] = 2 * random.nextDouble() - 1;
            randoms[i][2] = 20 * (random.nextDouble() + .1) / (i + 50);
            randoms[i][3] = 20 * (random.nextDouble() + .1) / (i + 50);
            randoms[i][4] = 100 * (random.nextDouble() - .5) / (i + 50);

        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(NUMBER_OF_DIMENSIONS, 2),
            new Parameter<>(DIMENSION_LENGTH, 50),
            new Parameter<>(SELECTED_COMPONENTS, new int[]{0}),
            new Parameter<>(META_RANDOM_SEED, System.currentTimeMillis())
        };
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    private void createTestRegularField(int dimNum, int resolution, int[] components, int nThreads, long randomSeed, ProgressAgent progressAgent)
    {
        float[][] extents = {{0, 0, 0}, {0, 0, 0}};
        int[] dims = new int[dimNum];
        switch (dimNum) {
            case 1:
                dims[0] = resolution;
                break;
            case 2:
                dims[0] = Math.max(2, (int) (.8 * resolution));
                dims[1] = (int) (1.25 * resolution);
                break;
            case 3:
                dims[0] = Math.max(2, (int) (.8 * resolution));
                dims[1] = resolution;
                dims[2] = (int) (1.25 * resolution);

                break;
            default:
                Arrays.fill(dims, resolution);
                break;
        }

        outField = outRegularField = new RegularField(dims);
        outRegularField.setName("Test regular field");
        outRegularField.setNSpace(3);
        if (dimNum > 1) {
            for (int i = 0; i < dimNum; i++) {
                extents[0][i] = (-.5f * dims[i]) / resolution;
                extents[1][i] = (.5f * dims[i]) / resolution;
            }
            outRegularField.setExtents(extents);
        }

        List<DataArray> dataArrays = null;
        switch (dimNum) {
            case 3:
                dataArrays = TestRegularField3DCore.createDataArrays(nThreads, dims, components, progressAgent);

                break;
            case 2:
                dataArrays = TestRegularField2DCore.createDataArrays(nThreads, dims, components, randoms, progressAgent);
                break;
            case 1:
                dataArrays = TestRegularField1DCore.createDataArrays(resolution, components, randoms, progressAgent);
                FloatLargeArray coords = new FloatLargeArray(3 * resolution);
                for (long i = 0; i < resolution; i++) {
                    double s = 0;
                    float dd = 5.f / resolution;
                    coords.set(3 * i, (float)(cos(3 * i * dd + s) + .1 * cos(10 * i * dd + s)));
                    coords.set(3 * i + 1, (float) sin(3 * i * dd + s));
                    coords.set(3 * i + 2, (float)(abs(i - resolution / 2) * dd) * (float)(abs(i - resolution / 2) * dd));
                }
                outRegularField.setCurrentCoords(coords);
                break;
            default:
        }
        for (DataArray dataArray : dataArrays)
            outRegularField.addComponent(dataArray);

        outRegularField.setCurrentTime(0);
        outRegularField.setCurrentTime(outRegularField.getStartTime());
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (resetParameters)
            parameters.set(META_RANDOM_SEED, System.currentTimeMillis());
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(final pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("isFromVNA = " + isFromVNA());

        Parameters p;
        synchronized (parameters) {
            validateParamsAndSetSmart(!isFromVNA());
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, isFromVNA(), false);

        int resolution = p.get(DIMENSION_LENGTH);
        int[] selectedComponents = p.get(SELECTED_COMPONENTS);
        ProgressAgent progressAgent = getProgressAgent(resolution + resolution / 2);
        //long time;
        if (selectedComponents.length == 0) {
            setOutputValue("outField", null);
            outRegularField = null;
            outField = null;
        } else {
            createTestRegularField(p.get(NUMBER_OF_DIMENSIONS), resolution, selectedComponents,
                                   VisNow.availableProcessors(), 0, progressAgent);
            setOutputValue("outField", new VNRegularField(outRegularField));
        }
        if (p.get(NUMBER_OF_DIMENSIONS) == 2)
            presentationParams.getRenderingParams().setShadingMode(RenderingParams.UNSHADED);
        presentationParams.getRenderingParams().setDisplayMode(RenderingParams.SURFACE | RenderingParams.IMAGE);
        prepareOutputGeometry();
        progressAgent.increase(resolution / 5);
        show();
        progressAgent.setProgress(1.0f);
    }
}
