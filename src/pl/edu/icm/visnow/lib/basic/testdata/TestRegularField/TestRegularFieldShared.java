//<editor-fold defaultstate="collapsed" desc=" License ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.testdata.TestRegularField;

import pl.edu.icm.visnow.engine.core.ParameterName;

/**
 * Shared static methods and fields between GUI and logic.
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class TestRegularFieldShared
{
    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //
    //Specification:
    //integer: 1,2 or 3
    public static final ParameterName<Integer> NUMBER_OF_DIMENSIONS = new ParameterName("Number of dimensions");
    //integer >= 2
    public static final ParameterName<Integer> DIMENSION_LENGTH = new ParameterName("Length of single dimension");
    //not null list of unique integers within range 0 .. <number_of_components>-1 
    //where <number_of_components> equals FIELD_NAMES_3D.length, FIELD_NAMES_2D.length or FIELD_NAMES_1D.length depends on NUMBER_OF_DIMENSIONS
    public static final ParameterName<int[]> SELECTED_COMPONENTS = new ParameterName("Selected components");
    
    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)    
    public static final ParameterName<Long> META_RANDOM_SEED = new ParameterName("META Random seed");

    //outfield component names (mainly 3D)
    public final static String GAUSSIANS = "Gaussians";
    public final static String TRIG_FUNCTION = "Trig function";
    public final static String SEMIELIPSOID = "Semielipsoid";
    public final static String TIME_FIELD = "Time field";
    public final static String PIPE = "Pipe";
    public final static String HOPF = "Hopf";
    public final static String VORTEX = "Vortex";
    public final static String TIME_VECTOR_FIELD = "Time vector field";
    public final static String CONST = "Constant field";

    //outfield component names (2D)
    public final static String GAUSSIANS1 = "Gaussians 1";
    public final static String GAUSSIANS_SWIRLS = "Gaussians swirls";
    public final static String GAUSSIANS_GRADIENT = "Gaussians gradient";
    public final static String BYTE_GAUSSIANS = "Byte gaussians";
    public final static String SHORT_GAUSSIANS = "Short gaussians";
    public final static String INT_GAUSSIANS = "Int gaussians";
    public final static String COMPLEX_GAUSSIANS = "Complex gaussians";
    public final static String STRING_GAUSSIANS = "String gaussians";
    public final static String LOGIC_GAUSSIANS = "Logic gaussians";

    //outfield component names (1D)
    final static String VECTOR_TRIG = "Vector trig";
    
    public static final String[] FIELD_NAMES_3D = {
        TRIG_FUNCTION,
        GAUSSIANS,
        SEMIELIPSOID,
        HOPF,
        CONST,
        VORTEX,
        TIME_VECTOR_FIELD,
        TIME_FIELD,
        PIPE
    };

    public static final String[] FIELD_NAMES_2D = {
        GAUSSIANS,
        GAUSSIANS1,
        GAUSSIANS_SWIRLS,
        GAUSSIANS_GRADIENT,
        BYTE_GAUSSIANS,
        SHORT_GAUSSIANS,
        INT_GAUSSIANS,
        COMPLEX_GAUSSIANS,
        STRING_GAUSSIANS,
        LOGIC_GAUSSIANS,
        TIME_VECTOR_FIELD,
        TIME_FIELD
    };

    public static final String[] FIELD_NAMES_1D = {
        GAUSSIANS,
        TRIG_FUNCTION,
        VECTOR_TRIG,
        TIME_FIELD
    };
}
