package pl.edu.icm.visnow.lib.basic.readers.medreaders.ReadDICOM;

import java.util.Iterator;
import java.util.Locale;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.spi.ImageWriterSpi;

public class TestImageIO {

    public static void main(String[] args) {
        System.out.println("---- Java Image I/O Readers ----");
        
        String names[] = ImageIO.getReaderFormatNames();
        for (int i = 0; i < names.length; ++i) {
            System.out.println("" + names[i] + ":");
            Iterator<ImageReader> it = ImageIO.getImageReadersByFormatName(names[i]);
            while(it.hasNext()) {
                ImageReader reader = it.next();
                ImageReaderSpi spi = reader.getOriginatingProvider();
                System.out.println("    - "+spi.getDescription(Locale.US)+" "+spi.getVendorName()+" v"+spi.getVersion()+" ("+spi.getPluginClassName()+")");
            }
        }

/*        
        System.out.println("");
        System.out.println(" ----Java Image I/O Writers ----");
        names = ImageIO.getWriterFormatNames();
        for (int i = 0; i < names.length; ++i) {
            System.out.println("" + names[i] + ":");
            Iterator<ImageWriter> it = ImageIO.getImageWritersByFormatName(names[i]);
            while(it.hasNext()) {
                ImageWriter writer = it.next();
                ImageWriterSpi spi = writer.getOriginatingProvider();
                System.out.println("      "+spi.getDescription(Locale.US)+" "+spi.getVendorName()+" v"+spi.getVersion()+" ("+spi.getPluginClassName()+")");
            }
        }
*/

    }
}
