package pl.edu.icm.visnow.lib.basic.readers.ReadEnSightGoldCase;

import java.nio.ByteOrder;
import pl.edu.icm.visnow.engine.core.ParameterName;

/**
 *
 * @author Norbert_2
 */


public class ReadEnSightGoldCaseShared {
            
        static final ParameterName<String> FILENAME = new ParameterName("file name");
        static final ParameterName<Boolean> MATERIALS_AS_SETS = new ParameterName("materials as sets");
        static final ParameterName<Boolean> SHOW = new ParameterName("show");
        static final ParameterName<Integer> INPUT_SOURCE = new ParameterName("input source");
}
