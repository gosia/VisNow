package pl.edu.icm.visnow.lib.basic.readers.ExtendedReadGADGET2;

import java.util.HashMap;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.system.main.VisNow;
import static pl.edu.icm.visnow.lib.basic.readers.ExtendedReadGADGET2.ExtendedGadgetReaderShared.*;


//TODO: proper java doc 
//TODO: common "Core" interface
//TODO: Core classes should have also full knowledge about input/output port names/types - if they're going to be used as "batch" version of module.
/**
 * Core that provides main computational functionality: processing input
 * parameters and creating output fields.
 *
 * @author szpak
 */
public class ExtendedGadgetReaderCore
{

    final private ExtendedReadGadgetData extendedReadGadgetData;

    final private Parameters params;

    private RegularField outDensityField;
    private IrregularField outField;

    //FIXME: no field schema testing ... modules may be incorrectly connected now (in Unicore wizard).
    public final static String OUT_IFIELD_MAIN = "outField";
    public final static String OUT_RFIELD_DENSITY = "outDensityField";

    public ExtendedGadgetReaderCore(Parameters params)
    {
        this.params = params;
        extendedReadGadgetData = new ExtendedReadGadgetData();
    }

    public static boolean[] getReadMask(Parameters params)
    {
        boolean[] out = new boolean[7];
        out[0] = params.get(READ_VELOCITY);
        out[1] = params.get(READ_ID);
        out[2] = params.get(READ_TYPE);
        out[3] = params.get(READ_MASS);
        out[4] = params.get(READ_ENERGY);
        //out[5] = isReadDensity();
        out[5] = false;
        out[6] = params.get(READ_TEMPERATURE);
        return out;
    }


    //TODO: boolean flag can be used to mark failures
    /**
     * Processes params to create outField and outDensityField. On failure, null
     * fields are set.
     */
    public void recalculate()
    {
        //reset out fields
        outDensityField = null;
        outField = null;

        //if file exists
        if (params.get(FILE_PATHS).length > 0) {
            extendedReadGadgetData.read(
                params.get(FILE_PATHS),
                getReadMask(params),
                params.get(DOWNSIZE),
                params.get(DENSITY_FIELD_DIMS),
                params.get(DENSITY_FIELD_LOG),
                null,
                VisNow.getMemoryFree() / 3
            );

            outField = extendedReadGadgetData.getField();
            if (outField != null) {
                outDensityField = extendedReadGadgetData.getDensityField();
            }
        }
    }

    public static String[] getInFieldNames()
    {
        return new String[]{};
    }

    public static String[] getOutFieldNames()
    {
        return new String[]{OUT_IFIELD_MAIN, OUT_RFIELD_DENSITY};
    }

    public Field getOutField(String name)
    {
        if (name.equals(OUT_IFIELD_MAIN))
            return outField;
        else if (name.equals(OUT_RFIELD_DENSITY))
            return outDensityField;
        else
            throw new IllegalArgumentException("Incorrect field name: " + name);
    }
}
