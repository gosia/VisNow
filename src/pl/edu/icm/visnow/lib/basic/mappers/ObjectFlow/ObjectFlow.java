//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.ObjectFlow;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.render.FrameRenderedListener;
import static pl.edu.icm.visnow.lib.basic.mappers.ObjectFlow.ObjectFlowShared.*;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.types.VNGeometryObject;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;

/**
 * Main class of time select/animate module
 * <p>
 * @author Krzysztof S. Nowinski (know@icm.edu.pl), University of Warsaw ICM
 * @author Jędrzej M. Nowosielski (jnow@icm.edu.pl), University of Warsaw ICM
 */
public class ObjectFlow extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected Field inField;
    protected ObjectFlowGUI computeUI = null;
    protected ObjectFlowShared params;
    protected RegularField inRegularField = null;
    protected IrregularField inIrregularField = null;

    /**
     * Creates a new instance of the module main class
     */
    public ObjectFlow()
    {

        outObj.setName("displacement");
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {

                if (outField == null)
                    return;
                outField.setCurrentTime(parameters.get(TIME_FRAME));
                if (outField.hasMask()) {
                    show();
                } else {
                    if (outField.getCurrentCoords() != null) {
                        fieldGeometry.updateCoords(outField.getCurrentCoords());
                    }
                    fieldGeometry.updateDataMap();
                }

                if (outObj.getCurrentViewer() != null) {
                    outObj.getCurrentViewer().setWaitForExternalTrigger(parameters.get(CONTINUOUS_UPDATE));
                }

                if (!parameters.get(ADJUSTING) || parameters.get(CONTINUOUS_UPDATE)) {

                    startAction();
                }
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new ObjectFlowGUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });

    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(NUMBER_OF_FRAMES, 2),
            new Parameter<>(START_TIME, 0.0f),
            new Parameter<>(END_TIME, 1.0f),
            new Parameter<>(FRAME, 0),
            new Parameter<>(TIME_FRAME, 0.0f),
            new Parameter<>(CONTINUOUS_UPDATE, false),
            new Parameter<>(ADJUSTING, false),
            new Parameter<>(META_START_TIME, 0.0f),
            new Parameter<>(META_END_TIME, 1.0f),
            new Parameter<>(META_TIME_UNIT, "")

        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);

        int nFrames = parameters.get(NUMBER_OF_FRAMES);
        int frame = parameters.get(FRAME);
        float startTime = parameters.get(START_TIME);
        float endTime = parameters.get(END_TIME);
        float timeFrame = parameters.get(TIME_FRAME);

        // 1.  validate parameters
        parameters.set(META_TIME_UNIT, inField.getTimeUnit());
        parameters.set(META_START_TIME, inField.getStartTime());
        parameters.set(META_END_TIME, inField.getEndTime());

        if (startTime < inField.getStartTime()) {
            parameters.set(START_TIME, inField.getStartTime());
        }
        if (endTime > inField.getEndTime()) {
            parameters.set(END_TIME, inField.getEndTime());
        }

        if (getInputFirstValue("frame") != null) {
            int frameFromModulePort = (Integer) getInputFirstValue("frame");
            if (frameFromModulePort != frame) {
                frame = frameFromModulePort;
                parameters.set(FRAME, frame);
                float dt = (parameters.get(END_TIME) - parameters.get(START_TIME)) / (nFrames - 1);
                timeFrame = parameters.get(START_TIME) + frame * dt;
                parameters.set(TIME_FRAME, timeFrame);
            }
        }

        if (frame >= nFrames || timeFrame >= parameters.get(END_TIME)) {
            parameters.set(FRAME, nFrames - 1);
            parameters.set(TIME_FRAME, parameters.get(END_TIME));
        }

        if (frame < 0 || timeFrame < parameters.get(START_TIME)) {
            parameters.set(FRAME, 0);
            parameters.set(TIME_FRAME, parameters.get(START_TIME));
        }

        // 2. reset parameters / set smart values
        if (resetParameters) {
            parameters.set(NUMBER_OF_FRAMES, inField.getNFrames());
            parameters.set(START_TIME, inField.getStartTime());
            parameters.set(END_TIME, inField.getEndTime());
            parameters.set(TIME_FRAME, inField.getCurrentTime());
            parameters.set(FRAME, inField.getCurrentFrame());

            if (getInputFirstValue("frame") != null && (Integer) getInputFirstValue("frame") < parameters.get(NUMBER_OF_FRAMES)) {
                int frameFromModulePort = (Integer) getInputFirstValue("frame");
                if (frameFromModulePort != parameters.get(FRAME)) {
                    frame = frameFromModulePort;
                    parameters.set(FRAME, frame);
                    float dt = (parameters.get(END_TIME) - parameters.get(START_TIME)) / (parameters.get(NUMBER_OF_FRAMES) - 1);
                    parameters.set(TIME_FRAME, parameters.get(START_TIME) + frame * dt);
                }

            }

        }

        parameters.setParameterActive(true);

    }

    @Override
    public void onInitFinishedLocal()
    {
        outObj.setCreator(this);
        setOutputValue("outObj", new VNGeometryObject(outObj));
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {

            //1. get new field
            Field newInField = ((VNField) getInputFirstValue("inField")).getField();

            //1a. set "different Field" flag
            boolean isDifferentField = !isFromVNA() && (inField == null || newInField != inField);
            boolean isNewField = (newInField != inField);
            inField = newInField;

            Parameters p;
            synchronized (parameters) {
                //2. validate params             
                validateParamsAndSetSmart(isDifferentField);
                //2b. clone param (local read-only copy)
                p = parameters.getReadOnlyClone();
            }

            //3. update gui (ObjectFlowGUI doesn't change parameters !!!!!!!!!!!!! - assuming correct set of parameters)
            notifyGUIs(p, isDifferentField || isFromVNA(), false);

            //4. run computation and propagate
            if (inField != null) {

                if (getInputFirstValue("frame") != null && outField != null) {

                    outField.setCurrentTime(parameters.get(TIME_FRAME));
                    if (outField.hasMask()) {
                        show();
                    } else {
                        if (outField.getCurrentCoords() != null) {
                            fieldGeometry.updateCoords(outField.getCurrentCoords());
                        }
                        fieldGeometry.updateDataMap();
                    }

                    if (outObj.getCurrentViewer() != null) {
                        outObj.getCurrentViewer().setWaitForExternalTrigger(false);
                    }
                }

                if (isNewField || isFromVNA() || outField == null) {
                    outField = inField.cloneShallow();
                    prepareOutputGeometry();
                }
                outField.setCurrentTime(parameters.get(TIME_FRAME));

                if (outField instanceof RegularField) {
                    setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
                    setOutputValue("outIrregularField", null);
                } else {
                    setOutputValue("outRegularField", null);
                    setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
                }

            } else {
                outField = null;
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", null);
            }

            //create default presentation of outField
            show(); //and send it to output 

        } else {
            this.switchPanelToDummy();
        }

    }

    @Override
    public FrameRenderedListener getFrameRenderedListener()
    {
        return computeUI.getFrameRenderedListener();
    }

//    @Override
//    public void onWaveFinalizing()
//    {
//        //      if (params.isAdjusting())
//        //         computeUI.getFrameRenderedListener().frameRendered(null);
//    }

    /**
     * Computation ObjectFlowGUI setter.
     * Set of a panel displayed in the "Computation" tab.
     * 
     * @param computeUI alternative ObjectFlowGUI (alternative to default one instantiated in constructor)
     */
    public void setComputeUI(ObjectFlowGUI computeUI) {
        this.computeUI = computeUI;
    }
}
