//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.basic.mappers.LineSlice;

import java.util.Arrays;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import static pl.edu.icm.visnow.lib.basic.mappers.LineSlice.LineSliceShared.*;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class LineSlice extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(LineSlice.class);
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private LineSliceGUI computeUI = null;
    protected RegularField inField = null;
    protected int dim;
    protected int[] dims;
    protected int nComps;
    protected DataArray data;

    public LineSlice()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startIfNotInQueue();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new LineSliceGUI();
            }
        });
        computeUI.setParameters(parameters);
        ui.addComputeGUI(computeUI);
        setPanel(ui);
    }
    
    private class SliceArray implements Runnable
    {
        private int iThread;
        private int nThreads;
        long n, start, step;

        public SliceArray(int iThread, int nThreads, 
                          long n, long start, long step) 
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            
            this.n = n;
            this.start = start;
            this.step = step;
        }
        
        @Override
        public void run() 
        {
            int nData = inField.getNComponents();
           for (int idata =  (iThread * nData) / nThreads; 
                    idata < ((iThread + 1) * nData) / nThreads; 
                    idata++) {
                data = inField.getComponent(idata);
                if (data == null)
                    continue;
                TimeData timeData = data.getTimeData();
                int veclen = data.getVectorLength();
                DataArray outDa = DataArray.create(data.getType(), n, veclen, data.getName(), data.getUnit(), data.getUserData());
                for (int iStep = 0; iStep < timeData.getNSteps(); iStep++) {
                    LargeArray inBData = timeData.getValue(timeData.getTime(iStep));
                    LargeArray outBData = LargeArrayUtils.create(inBData.getType(), n * veclen, false);
                    for (long i = 0, j = start; i < n; i++, j += step) {
                        for (int k = 0; k < veclen; k++) {
                            outBData.set(i * veclen + k, inBData.get(j * veclen + k));
                        }
                    }
                    outDa.addRawArray(outBData, timeData.getTime(iStep));
                }

                if (outDa != null) {
                    outDa.setPreferredRanges(data.getPreferredMinValue(), data.getPreferredMaxValue(), data.getPreferredPhysMinValue(), data.getPreferredPhysMaxValue());
                    outRegularField.addComponent(outDa);
                }
            }
        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(I_AXIS_VALUE, -1),
            new Parameter<>(J_AXIS_VALUE, -1),
            new Parameter<>(K_AXIS_VALUE, -1),
            new Parameter<>(SELECTED_AXIS, Axis.I),
            new Parameter<>(RECALCULATE_MIN_MAX, false),
            new Parameter<>(META_FIELD_DIMENSION_LENGTHS, new int[]{2, 2, 2})
        };
    }

    public void update()
    {
        LOGGER.debug("");

        Axis axis = parameters.get(SELECTED_AXIS);
        int ax = axis == Axis.I ? 0 : axis == Axis.J ? 1 : 2;
        int x = parameters.get(I_AXIS_VALUE);
        int y = parameters.get(J_AXIS_VALUE);
        int z = parameters.get(K_AXIS_VALUE);

        int dimNum = inField.getDimNum();

        //validate sliders
        boolean validSlice = true;
        if (ax != 0 && (x < 0 || x >= dims[0]))
            validSlice = false;
        if (ax != 1 && dimNum >= 2 && (y < 0 || y >= dims[1]))
            validSlice = false;
        if (ax != 2 && dimNum >= 3 && (z < 0 || z >= dims[2]))
            validSlice = false;

        //invalid slice - remove output field
        if (!validSlice) {
            outField = null;
            setOutputValue("outField", null);
        } else {
            int[] outDims = new int[1];
            outDims[0] = dims[ax];
            outField = new RegularField(outDims);
            outRegularField = (RegularField) outField;
            outRegularField.setNSpace(inField.getNSpace());
            float[][] affine = inField.getAffine();
            float[][] sliceAffine = new float[4][3];
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 3; j++) {
                    sliceAffine[i][j] = 0;
                }
            }
            for (int i = 0; i < 3; i++) {
                sliceAffine[0][i] = affine[ax][i];
                if (inField.getDims().length == 2)
                    if (ax == 0)
                        sliceAffine[3][i] = affine[3][i] + y * affine[1][i];
                    else
                        sliceAffine[3][i] = affine[3][i] + x * affine[0][i];
                else if (ax == 0)
                    sliceAffine[3][i] = affine[3][i] + y * affine[1][i] + z * affine[2][i];
                else if (ax == 1)
                    sliceAffine[3][i] = affine[3][i] + x * affine[0][i] + z * affine[2][i];
                else
                    sliceAffine[3][i] = affine[3][i] + x * affine[0][i] + y * affine[1][i];
            }
            outRegularField.setAffine(sliceAffine);

            // now we are rewriting values from input data to output array
            // input data is 1D even when there are many dimensions, therefore
            // step variable must be used
            long n = dims[ax];
            long start = 0, step = 1;
            if (inField.getDims().length == 2) {
                if (ax == 0) {
                    start = y * dims[0];
                    step = 1;
                } else if (ax == 1) {
                    start = x;
                    step = dims[0];
                }
            } else if (ax == 0) {
                start = (z * dims[1] + y) * dims[0];
                step = 1;
            } else if (ax == 1) {
                start = z * dims[1] * dims[0] + x;
                step = dims[0];
            } else {
                start = y * dims[0] + x;
                step = dims[0] * dims[1];
            }
            if (inField.getCurrentCoords() != null) {
                int veclen = inField.getNSpace();
                FloatLargeArray inCoords = inField.getCurrentCoords();
                FloatLargeArray outCoords = new FloatLargeArray(n * veclen);
                for (long i = 0, j = start; i < n; i++, j += step) {
                    for (int k = 0; k < veclen; k++) {
                        outCoords.set(i * veclen + k, inCoords.get(j * veclen + k));
                    }
                }
                outRegularField.setCoords(outCoords, 0);
            }
            
            int nThreads = 
                   pl.edu.icm.visnow.system.main.VisNow.availableProcessors();
            if (nThreads > inField.getNComponents())
                nThreads = inField.getNComponents();
            Thread[] workThreads = new Thread[nThreads];
            for (int iThread = 0; iThread < nThreads; iThread++) {
                    workThreads[iThread] = 
                            new Thread(new SliceArray(iThread, nThreads,
                                                      n, start, step));
                workThreads[iThread].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        
            updateMinMax();
            setOutputValue("outField", new VNRegularField(outRegularField));
        }

        prepareOutputGeometry();
        show();
    }

    private void validateParamsAndSetSmart()
    {
        parameters.setParameterActive(false);
        parameters.set(META_FIELD_DIMENSION_LENGTHS, dims);

        if (parameters.get(SELECTED_AXIS) == Axis.K && dims.length < 3)
            parameters.set(SELECTED_AXIS, Axis.J);

        if (parameters.get(I_AXIS_VALUE) > dims[0] || parameters.get(I_AXIS_VALUE) < 0)
            parameters.set(I_AXIS_VALUE, dims[0] / 2);
        if (parameters.get(J_AXIS_VALUE) > dims[1] || parameters.get(J_AXIS_VALUE) < 0)
            parameters.set(J_AXIS_VALUE, dims[1] / 2);
        if (dims.length > 2 && (parameters.get(K_AXIS_VALUE) > dims[2] || parameters.get(K_AXIS_VALUE) < 0))
            parameters.set(K_AXIS_VALUE, dims[2] / 2);

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean dimChanged = !isFromVNA() && (inField == null || !Arrays.equals(inField.getDims(), newField.getDims()));
            inField = newField;
            dims = inField.getDims();
            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart();
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || dimChanged, false);

            update();
            prepareOutputGeometry();
            show();
        }
    }

    private void updateMinMax()
    {
        if (parameters.get(RECALCULATE_MIN_MAX) && outField != null) {
            for (int i = 0; i < outField.getNComponents(); i++) {
                outField.getComponent(i).recomputeStatistics();
            }
        }
    }
}
