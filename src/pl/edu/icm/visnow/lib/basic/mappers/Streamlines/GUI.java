//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.Streamlines;

import java.awt.CardLayout;
import pl.edu.icm.visnow.engine.core.ParameterProxy;
import pl.edu.icm.visnow.gui.widgets.RunButton;
import static pl.edu.icm.visnow.lib.basic.mappers.Streamlines.StreamlinesShared.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 * <p>
 * Revisions above 564 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class GUI extends javax.swing.JPanel
{
    private ParameterProxy parameterProxy = null;

    public GUI()
    {
        initComponents();
    }

    /**
     * Bind parameterproxy to this gui
     *
     * @param targetParameterProxy proxy to target parameters
     */
    public void setParameterProxy(ParameterProxy targetParameterProxy)
    {
        this.parameterProxy = runButton.getPlainProxy(targetParameterProxy, RUNNING_MESSAGE);
        regularFieldDownsizeUI.setParameterProxy(parameterProxy);
    }

    void update(ParameterProxy p, boolean setRunButtonPending)
    {
        regularFieldDownsizeUI.updateGUI(p);
        
        vectorComponentSelector.setListData(p.get(META_VECTOR_COMPONENT_NAMES));
        vectorComponentSelector.setSelectedItem(p.get(COMPONENT));

        stepSlider.setValue(parameterProxy.get(STEP));

        downsizeSlider.setValue(parameterProxy.get(DOWNSIZE_IRREGULAR) + 1);
        ((CardLayout) (downsizePanel.getLayout())).show(downsizePanel, parameterProxy.get(META_IS_START_POINT_FIELD_REGULAR) ? "regular" : "irregular");

        forwardStepsSlider.setValue(parameterProxy.get(NUM_STEPS_FORWARD));
        backwardStepsSlider.setValue(parameterProxy.get(NUM_STEPS_BACKWARD));

        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        vectorComponentSelector = new pl.edu.icm.visnow.gui.widgets.WidePopupComboBox();
        stepSlider = new pl.edu.icm.visnow.gui.widgets.ExtendedSlider();
        downsizePanel = new javax.swing.JPanel();
        regularFieldDownsizeUI = new pl.edu.icm.visnow.lib.gui.DownsizeUI();
        downsizeSlider = new pl.edu.icm.visnow.gui.widgets.ExtendedSlider();
        forwardStepsSlider = new pl.edu.icm.visnow.gui.widgets.ExtendedSlider();
        backwardStepsSlider = new pl.edu.icm.visnow.gui.widgets.ExtendedSlider();
        runButton = new pl.edu.icm.visnow.gui.widgets.RunButton();
        filler = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setLayout(new java.awt.GridBagLayout());

        vectorComponentSelector.setBorder(javax.swing.BorderFactory.createTitledBorder("Vector component"));
        vectorComponentSelector.addUserActionListener(new pl.edu.icm.visnow.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
                vectorComponentSelectorUserChangeAction(evt);
            }
            public void userAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(vectorComponentSelector, gridBagConstraints);

        stepSlider.setFieldType(pl.edu.icm.visnow.gui.components.NumericTextField.FieldType.FLOAT);
        stepSlider.setMax(0.1);
        stepSlider.setMin(1e-4);
        stepSlider.setScaleType(pl.edu.icm.visnow.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        stepSlider.setSubmitOnAdjusting(false);
        stepSlider.setValue(2e-3);
        stepSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Integration step"));
        stepSlider.addUserActionListener(new pl.edu.icm.visnow.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
                stepSliderUserChangeAction(evt);
            }
            public void userAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(stepSlider, gridBagConstraints);

        downsizePanel.setMinimumSize(new java.awt.Dimension(10, 65));
        downsizePanel.setOpaque(false);
        downsizePanel.setLayout(new java.awt.CardLayout());

        regularFieldDownsizeUI.setDynamicButtonVisible(false);
        downsizePanel.add(regularFieldDownsizeUI, "regular");

        downsizeSlider.setFieldType(pl.edu.icm.visnow.gui.components.NumericTextField.FieldType.INT);
        downsizeSlider.setGlobalMin(1);
        downsizeSlider.setMax(100000);
        downsizeSlider.setMin(1);
        downsizeSlider.setScaleType(pl.edu.icm.visnow.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        downsizeSlider.setSubmitOnAdjusting(false);
        downsizeSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Downsize init points set"));
        downsizeSlider.addUserActionListener(new pl.edu.icm.visnow.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
                downsizeSliderUserChangeAction(evt);
            }
            public void userAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        downsizePanel.add(downsizeSlider, "irregular");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(downsizePanel, gridBagConstraints);

        forwardStepsSlider.setFieldType(pl.edu.icm.visnow.gui.components.NumericTextField.FieldType.INT);
        forwardStepsSlider.setGlobalMin(0);
        forwardStepsSlider.setMax(1000);
        forwardStepsSlider.setMin(0
        );
        forwardStepsSlider.setScaleType(pl.edu.icm.visnow.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        forwardStepsSlider.setSubmitOnAdjusting(false);
        forwardStepsSlider.setValue(100);
        forwardStepsSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Steps forward"));
        forwardStepsSlider.addUserActionListener(new pl.edu.icm.visnow.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
                forwardStepsSliderUserChangeAction(evt);
            }
            public void userAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(forwardStepsSlider, gridBagConstraints);

        backwardStepsSlider.setFieldType(pl.edu.icm.visnow.gui.components.NumericTextField.FieldType.INT);
        backwardStepsSlider.setGlobalMin(0);
        backwardStepsSlider.setMax(1000);
        backwardStepsSlider.setMin(0);
        backwardStepsSlider.setScaleType(pl.edu.icm.visnow.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        backwardStepsSlider.setSubmitOnAdjusting(false);
        backwardStepsSlider.setValue(1);
        backwardStepsSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Steps backward"));
        backwardStepsSlider.addUserActionListener(new pl.edu.icm.visnow.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
                backwardStepsSliderUserChangeAction(evt);
            }
            public void userAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(backwardStepsSlider, gridBagConstraints);

        runButton.setAutoCB(true);
        runButton.addUserActionListener(new pl.edu.icm.visnow.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
                runButtonUserChangeAction(evt);
            }
            public void userAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(runButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void stepSliderUserChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_stepSliderUserChangeAction
    {//GEN-HEADEREND:event_stepSliderUserChangeAction
        parameterProxy.set(STEP, (Float) stepSlider.getValue());
    }//GEN-LAST:event_stepSliderUserChangeAction

    private void forwardStepsSliderUserChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_forwardStepsSliderUserChangeAction
    {//GEN-HEADEREND:event_forwardStepsSliderUserChangeAction
        parameterProxy.set(NUM_STEPS_FORWARD, (Integer) forwardStepsSlider.getValue());
    }//GEN-LAST:event_forwardStepsSliderUserChangeAction

    private void backwardStepsSliderUserChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_backwardStepsSliderUserChangeAction
    {//GEN-HEADEREND:event_backwardStepsSliderUserChangeAction
        parameterProxy.set(NUM_STEPS_BACKWARD, (Integer) backwardStepsSlider.getValue());
    }//GEN-LAST:event_backwardStepsSliderUserChangeAction

    private void runButtonUserChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButtonUserChangeAction
    {//GEN-HEADEREND:event_runButtonUserChangeAction
        parameterProxy.set(RUNNING_MESSAGE, (RunButton.RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction

    private void downsizeSliderUserChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_downsizeSliderUserChangeAction
    {//GEN-HEADEREND:event_downsizeSliderUserChangeAction
        parameterProxy.set(DOWNSIZE_IRREGULAR, (Integer) downsizeSlider.getValue());
    }//GEN-LAST:event_downsizeSliderUserChangeAction

    private void vectorComponentSelectorUserChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_vectorComponentSelectorUserChangeAction
    {//GEN-HEADEREND:event_vectorComponentSelectorUserChangeAction
        stepSlider.setValue(parameterProxy.get(META_PREFERRED_STEPS)[vectorComponentSelector.getSelectedIndex()]);
        parameterProxy.set(COMPONENT, (String) vectorComponentSelector.getSelectedItem(),
                           STEP, stepSlider.getValue().floatValue());

    }//GEN-LAST:event_vectorComponentSelectorUserChangeAction

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private pl.edu.icm.visnow.gui.widgets.ExtendedSlider backwardStepsSlider;
    private javax.swing.JPanel downsizePanel;
    private pl.edu.icm.visnow.gui.widgets.ExtendedSlider downsizeSlider;
    private javax.swing.Box.Filler filler;
    private pl.edu.icm.visnow.gui.widgets.ExtendedSlider forwardStepsSlider;
    private pl.edu.icm.visnow.lib.gui.DownsizeUI regularFieldDownsizeUI;
    private pl.edu.icm.visnow.gui.widgets.RunButton runButton;
    private pl.edu.icm.visnow.gui.widgets.ExtendedSlider stepSlider;
    private pl.edu.icm.visnow.gui.widgets.WidePopupComboBox vectorComponentSelector;
    // End of variables declaration//GEN-END:variables
}
