/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.mappers.Trajectories;

import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import java.util.ArrayList;
import pl.edu.icm.jscic.cells.Cell;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Core
{

    private Field inField;
    private IrregularField outField = null;
    private RegularField mappedField = null;
    private Params params;

    public Core()
    {
    }

    /**
     * @param inField the inField to set
     */
    public void setInField(Field inField)
    {
        this.inField = inField;
    }

    /**
     * @return the outField
     */
    public IrregularField getOutField()
    {
        return outField;
    }

    public RegularField getMappedField()
    {
        return mappedField;
    }

    /**
     * @param params the params to set
     */
    public void setParams(Params params)
    {
        this.params = params;
    }
    
    public LogicLargeArray getMaskTimeSlice(Field field, int node)
    {
        if (!field.hasMask())
            return null;
        int nFrames = field.getNFrames();
        LogicLargeArray out = new LogicLargeArray(nFrames);
        for (int i = 0; i < out.length(); i++) {
            out.setByte(i, field.getMask().getValues().get(i).getByte(node));
        }
        return out;
    }

    public void update()
    {
        if (inField == null || inField.getNFrames() < 2 || inField.getCoords() == null || inField.getCoords().isEmpty()) {
            outField = null;
            return;
        }

        int inNFrames = inField.getNFrames();
        int startFrame = params.getStartFrame();
        int endFrame = params.getEndFrame();
        int nFrames = endFrame - startFrame + 1;

        int inNNodes = (int) inField.getNNodes();
        int outNNodes = nFrames * inNNodes;
        int outNCells = (nFrames - 1) * inNNodes;
        int inNSpace = inField.getNSpace();

        ArrayList<DataArray> comps = new ArrayList<DataArray>();

        for (int c = 0; c < inField.getNComponents(); c++) {
            int veclen = inField.getComponent(c).getVectorLength();
            LargeArray bData = LargeArrayUtils.create(inField.getComponent(c).getRawArray().getType(), nFrames * inNNodes * veclen, false);
            for (int n = 0; n < nFrames; n++) {
                inField.getComponent(c).setCurrentTime(inField.getComponent(c).getTime(n));
                LargeArray inDataB = inField.getComponent(c).getRawArray();
                for (int i = 0; i < inNNodes; i++) {
                    if (veclen == 1) {
                        bData.set(i * nFrames + n, inDataB.get(i));
                    } else {
                        for (int m = 0; m < veclen; m++) {
                            bData.set(i * nFrames * veclen + n * veclen + m, inDataB.get(i * veclen + m));
                        }
                    }
                }
            }
            comps.add(DataArray.create(bData, veclen, inField.getComponent(c).getName()));
            inField.getComponent(c).setCurrentTime(inField.getComponent(c).getStartTime());
        }

        float[] coords = new float[inNNodes * nFrames * 3];
        for (int i = 0; i < inNNodes; i++) {

            float[] trj = inField.getTrajectory(i).getData();

            if (inNSpace == 3 && nFrames == inNFrames) {

                System.arraycopy(trj, 0, coords, nFrames * 3 * i, trj.length);

            } else {

                for (int n = 0; n < nFrames; n++) {
                    for (int m = 0; m < inNSpace; m++) {
                        coords[i * nFrames * 3 + 3 * n + m] = trj[(n + startFrame - 1) * inNSpace + m];
                    }
                    for (int m = inNSpace; m < 3; m++) {
                        coords[i * nFrames * 3 + 3 * n + m] = 0;
                    }
                }
            }

        }

        int[] cells = new int[2 * outNCells]; //2 * (nFrames-1) * inNNodes
        for (int i = 0, l = 0; i < inNNodes; i++) {
            for (int n = 0; n < nFrames - 1; n++, l += 2) {
                cells[l] = i * nFrames + n;
                cells[l + 1] = i * nFrames + n + 1;
            }
        }

        outField = new IrregularField(outNNodes);
        outField.setNSpace(3);
        outField.setCurrentCoords(new FloatLargeArray(coords));
        CellSet cellSet = new CellSet(inField.getName() + "_trajectories");
        for (int i = 0; i < comps.size(); i++) {
            outField.addComponent(comps.get(i));
        }
        CellArray segments = new CellArray(CellType.SEGMENT, cells, null, null);
        cellSet.setBoundaryCellArray(segments);
        cellSet.setCellArray(segments);

        outField.addCellSet(cellSet);

        if (inField.getCurrentMask() != null) {
            LogicLargeArray valid = new LogicLargeArray(inNNodes * nFrames);
            for (int i = 0; i < inNNodes; i++) {
                LogicLargeArray mask = getMaskTimeSlice(inField, i);
                for (int j = 0; j < nFrames; j++) {
                    valid.setByte(nFrames * i + j, mask.getByte(startFrame - 1 + j));
                }
            }
            outField.setCurrentMask(valid);
        }

        //TODO invalid aware trajectories
        mappedField = MapCoordsToData.MapCoordsToData(inField);
    }
}
