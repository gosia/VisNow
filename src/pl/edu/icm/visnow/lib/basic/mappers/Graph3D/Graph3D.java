//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>

package pl.edu.icm.visnow.lib.basic.mappers.Graph3D;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.LinkFace;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.geometries.objects.RegularField2DGeometry;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.utils.VectorMath;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Graph3D extends OutFieldVisualizationModule
{

    private GUI computeUI = null;
    private RegularField inField = null;
    private Params params = null;
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected boolean ignoreUI = false;
    protected boolean fromUI = false;

    public Graph3D()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                fromUI = true;
                if (ignoreUI)
                    return;
                if (params.isAdjusting() &&
                        regularFieldGeometry != null) {
                    update();
                    if (regularFieldGeometry instanceof RegularField2DGeometry)
                        ((RegularField2DGeometry) regularFieldGeometry).updateCoords();
                } else
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new GUI();
            }
        });
        computeUI.setParams(params);
        outObj.setName("graph3D");
        ui.addComputeGUI(computeUI);
        setPanel(ui);
    }

    public void update()
    {
        boolean newCoords = false;
        float[] coords;
        if (inField == null || inField.getDims() == null || inField.getDims().length != 2)
            return;
        int mapComponent = params.getComponent();
        if (mapComponent < 0 || mapComponent >= inField.getNComponents())
            return;
        if (inField.getComponent(mapComponent).getVectorLength() != 1)
            return;
        int[] dims = inField.getDims();
        if (outRegularField != null && outRegularField.getCurrentCoords() != null)
            for (int i = 0; i < dims.length; i++)
                if (dims[i] != outRegularField.getDims()[i]) {
                    newCoords = true;
                    break;
                }
        if (outRegularField == null || outRegularField.getCurrentCoords() == null || newCoords) {
            outRegularField = new RegularField(dims);
            outRegularField.setNSpace(3);
            coords = new float[3 * dims[0] * dims[1]];
            outRegularField.setCurrentCoords(new FloatLargeArray(coords));
        } else
            coords = outRegularField.getCurrentCoords() == null ? null : outRegularField.getCurrentCoords().getData();
        for (int i = 0; i < inField.getNComponents(); i++)
            outRegularField.setComponent(inField.getComponent(i).cloneShallow(), i);
        outRegularField.setCurrentMask(inField.getCurrentMask());
        if (inField.getCurrentCoords() == null) {
            float[] h = new float[3];
            float[][] affine = inField.getAffine();
            h[0] = affine[0][1] * affine[1][2] - affine[0][2] * affine[1][1];
            h[1] = affine[0][2] * affine[1][0] - affine[0][0] * affine[1][2];
            h[2] = affine[0][0] * affine[1][1] - affine[0][1] * affine[1][0];
            float r = (float) (sqrt(h[0] * h[0] + h[1] * h[1] + h[2] * h[2]));
            for (int i = 0; i < h.length; i++)
                h[i] *= params.getScale() / r;
            for (int i = 0, k = 0; i < dims[1]; i++) {
                float[] d = inField.getComponent(mapComponent).getCurrent1DFloatSlice(i * dims[0], dims[0], 1).getData();
                for (int j = 0; j < dims[0]; j++)
                    for (int l = 0; l < 3; l++, k++)
                        coords[k] = affine[3][l] + i * affine[1][l] + j * affine[0][l] + h[l] * d[j];
            }
        } else {
            float[] c = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
            float[] z = inField.getNormals() == null ? null : inField.getNormals().getData();
            for (int i = 0, k = 0; i < dims[1]; i++) {
                float[] d = inField.getComponent(mapComponent).getCurrent1DFloatSlice(i * dims[0], dims[0], 1).getData();
                for (int j = 0; j < dims[0]; j++)
                    for (int l = 0; l < 3; l++, k++)
                        coords[k] = c[k] + params.getScale() * z[k] * d[j];
            }
        }
        outRegularField.updateExtents();
        
        if (params.isZFromData() && inField.isAffineXYZ(false)) {
            //find which axis is "3D"?
            float[] v0 = inField.getAffine()[0];
            float[] v1 = inField.getAffine()[1];
            float[] v2 = VectorMath.crossProduct(v0, v1, false);
            float ax = VectorMath.vectorAngle(v2, new float[]{1.0f, 0.0f, 0.0f}, false);
            if(ax >= Math.PI/2.0) {
                ax = (float) (ax - Math.PI);
            }
            ax = Math.abs(ax);            
            
            float ay = VectorMath.vectorAngle(v2, new float[]{0.0f, 1.0f, 0.0f}, false);
            if(ay >= Math.PI/2.0) {
                ay = (float) (ay - Math.PI);
            }
            ay = Math.abs(ay);            
            
            float az = VectorMath.vectorAngle(v2, new float[]{0.0f, 0.0f, 1.0f}, false);            
            if(az >= Math.PI/2.0) {
                az = (float) (az - Math.PI);
            }
            az = Math.abs(az);            
            
            
            int axis3 = 2;
            if(ax < ay && ax < az) {
                axis3 = 0;
            } else if(ay < ax && ay < az) {
                axis3 = 1;
            }
            
            
            
            
            
            float[][] phExts = new float[2][3];
            float[][] exts = outRegularField.getExtents();
            for (int i = 0; i < 2; i++)
                System.arraycopy(exts[i], 0, phExts[i], 0, 3);
            phExts[0][axis3] = (float)inField.getComponent(mapComponent).getPreferredPhysMinValue();
            phExts[1][axis3] = (float)inField.getComponent(mapComponent).getPreferredPhysMaxValue();
            outRegularField.setPhysExtents(phExts);
            String[] inCoordNames  = inField.getAxesNames();
            String[] outCoordNames = new String[3];
            
            outCoordNames[0] = (inCoordNames == null || inCoordNames.length < 1 || inCoordNames[0] == null || 
                                inCoordNames[0].startsWith("__coord")) ?
                 "y" : inCoordNames[0];
            outCoordNames[1] = (inCoordNames == null || inCoordNames.length < 2 ||inCoordNames[1] == null || 
                                inCoordNames[1].startsWith("__coord")) ?
                 "y" : inCoordNames[1];
            outCoordNames[2] = (inCoordNames == null || inCoordNames.length < 3 ||inCoordNames[2] == null || 
                                inCoordNames[2].startsWith("__coord")) ?
                 "z" : inCoordNames[2];
            outCoordNames[axis3] = inField.getComponent(mapComponent).getName();
                        
            String u = inField.getComponent(mapComponent).getUnit();
            if (u != null && !u.isEmpty()) 
                outCoordNames[axis3] = inField.getComponent(mapComponent).getName() + "[" + u + "]";
            outRegularField.setAxesNames(outCoordNames);
        } else {
            outRegularField.setAxesNames(null);
        }
    }

    /**
     * Returns smart values for scale parameter: preferredMinValue, val, preferredMaxValue.
     */
    public static float[] getSmartScale(RegularField field, int componentNumber) 
    {
    float min = (float)field.getComponent(componentNumber).getPreferredMinValue();
        float max = (float)field.getComponent(componentNumber).getPreferredMaxValue();
        if (max <= min)
            max = min + .001f;
        float ext = field.getExtents()[1][0] - field.getExtents()[0][0];
        for (int i = 1; i < field.getNSpace(); i++)
            ext = max(ext, field.getExtents()[1][i] - field.getExtents()[0][i]);
        int k = (int) round(log10(ext / (max - min)));
        float smax = (float) pow(10., k);
        return new float[]{-smax, 0.2f * smax, smax};
    }
    
    private void validateParamsAndSetSmart()
    {
        //validate selected component
        params.setComponent(Math.min(params.getComponent(), inField.getNComponents() - 1));
                
        //scale value
        float scale = params.getScale();
        float[] smartValues = Graph3D.getSmartScale(inField, params.getComponent());
        //validate
        boolean validate = scale < smartValues[0] || scale > smartValues[2];                
        //threshold used when graph scale is too close to scale range
        float smartThreshold = 0.1f;
        float position = (scale - smartValues[0]) / (smartValues[2] - smartValues[0]);
        boolean smartUpdate = false;
        //smart
        if (position < smartThreshold || 1 - position < smartThreshold || scale == 0) smartUpdate = true;
        if (validate  || smartUpdate) params.setScale(smartValues[1]);
    }

    @Override
    public void onActive()
    {
        if (fromUI && outField != null) {
            update();
            if (regularFieldGeometry instanceof RegularField2DGeometry)
                ((RegularField2DGeometry) regularFieldGeometry).updateCoords();
            setOutputValue("graph3DField", new VNRegularField(outRegularField));
            return;
        }
        fromUI = false;
        boolean newField = false;
        RegularField in;

        if (getInputFirstValue("inField") == null)
            return;
        in = ((VNRegularField) getInputFirstValue("inField")).getField();
        if (in == null)
            return;
        if (!in.isDataCompatibleWith(inField)) {
            inField = in;
            newField = true;
            computeUI.setInField(inField);
        } else if (!in.isStructureCompatibleWith(inField))
            newField = true;
        inField = in;
        validateParamsAndSetSmart();
        computeUI.updateGUI();
        update();
        setOutputValue("outField", new VNRegularField(outRegularField));
        outField = outRegularField;
        if (newField) prepareOutputGeometry();
        newField = false;
        show();
        fromUI = false;
    }

    @Override
    public void onInputDetach(LinkFace link)
    {
        inField = null;
    }
} 
