/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.mappers.DiffusionStream;

import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.CellArray;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.gui.events.FloatValueModificationEvent;
import pl.edu.icm.visnow.gui.events.FloatValueModificationListener;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import static pl.edu.icm.jscic.utils.CropDownUtils.*;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.cells.CellType;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Streamlines extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private ComputeStreamlines streamlines;
    protected GUI computeUI = null;
    private Params params;
    private Object lastIn = null;
    private Object lastPts = null;
    private Field inField = null;
    private RegularField inRegularField;
    private IrregularField inIrregularField;
    private Field pts;
    private IrregularField startPts;
    private int[] ptsDims = null;
    private int[] down = null;
    private IrregularField flowField;
    private boolean fromParams = false;
    private boolean onInput = true;
    private int nSteps = 100;
    private FloatValueModificationListener progressListener
        = new FloatValueModificationListener()
        {
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                setProgress(e.getVal());
            }
        };

    public Streamlines()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                if (onInput)
                    return;
                fromParams = true;
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new GUI();
            }
        });
        computeUI.setParams(params);
        ui.addComputeGUI(computeUI);
        setPanel(ui);
        outObj.setName("Streamlines");
    }

    private void checkAndDownsizeInPts()
    {
        down = params.getDown();
        int downsize = 1;
        int nDownsizedNodes = 1;
        int nStartNodes;
        int nSpace = pts.getNSpace();

        if (pts instanceof IrregularField) {
            int dSize = params.getDownsize();
            if (dSize < 1)
                dSize = 1;
            nDownsizedNodes = (int) pts.getNNodes() / dSize;
        } else {
            nDownsizedNodes = 1;
            int[] downsizedDims = new int[ptsDims.length];
            for (int i = 0; i < downsizedDims.length; i++) {
                downsizedDims[i] = ptsDims[i] / down[i] + 1;
                nDownsizedNodes *= downsizedDims[i];
            }
        }
        int multiplicity = params.getMultiplicity();
        if (!params.computeDiffusion())
            multiplicity = 1;
        nStartNodes = nDownsizedNodes * multiplicity;
        float[] downsizedCoords = new float[nSpace * nDownsizedNodes];
        if (pts instanceof IrregularField) {
            float[] ptsCoords = pts.getCurrentCoords() == null ? null : pts.getCurrentCoords().getData();
            for (int i = 0, j = 0, k = 0; i < nDownsizedNodes; i++, k += downsize * pts.getNSpace())
                for (int l = 0; l < nSpace; l++, j++)
                    downsizedCoords[j] = ptsCoords[k + l];
        } else {
            if (pts.getCurrentCoords() != null)
                downsizedCoords = ((FloatLargeArray)downArray(pts.getCurrentCoords(), pts.getNSpace(), ptsDims, down)).getData();
            else {
                downsizedCoords = ((FloatLargeArray)downArray(((RegularField) pts).getCoordsFromAffine(), pts.getNSpace(), ptsDims, down)).getData();
            }
        }
        float[] startCoords = new float[nSpace * nStartNodes];
        startPts = new IrregularField(nStartNodes);
        startPts.setNSpace(3);
        for (int i = 0; i < multiplicity; i++)
            System.arraycopy(downsizedCoords, 0, startCoords, i * downsizedCoords.length, downsizedCoords.length);
        startPts.setCurrentCoords(new FloatLargeArray(startCoords));
        int[] nodes = new int[nStartNodes];
        for (int i = 0; i < nodes.length; i++)
            nodes[i] = i;
        CellArray nodesArray = new CellArray(CellType.POINT, nodes, null, nodes);
        CellSet ptSet = new CellSet();
        ptSet.setCellArray(nodesArray);
        ptSet.setBoundaryCellArray(nodesArray);
        ptSet.setCellArray(nodesArray);
        startPts.addCellSet(ptSet);
        startPts.addComponent(DataArray.create(nodes, 1, "dummy"));
    }

    private class Remap implements Runnable
    {

        private final int nNodes, nSp;
        private final int iThr;
        private final int nThreads;
        private final float[][] inCoords;
        private final float[][] outCoords;

        public Remap(int nNodes, int nSp, int nThread, float[][] inCoords, float[][] outCoords)
        {
            this.nNodes = nNodes;
            this.nSp = nSp;
            this.iThr = nThread;
            nThreads = params.getNThreads();
            this.inCoords = inCoords;
            this.outCoords = outCoords;
        }

        @Override
        public void run()
        {
            int n = nSteps / nThreads;
            int low = iThr * n;
            for (int i = low; i < low + n; i++) {
                float[] stepCoords = outCoords[i];
                for (int j = 0; j < nNodes; j++)
                    System.arraycopy(inCoords[j], i * nSp, stepCoords, j * nSp, nSp);
            }
        }
    }

    @Override
    public void onActive()
    {
        boolean inChanged = false;
        boolean ptsChanged = false;
        if (!fromParams) {
            onInput = true;
            if (getInputFirstValue("inField") == null ||
                ((VNField) getInputFirstValue("inField")).getField() == null ||
                getInputFirstValue("inField") == lastIn && getInputFirstValue("startPoints") == lastPts) {
                onInput = false;
                return;
            }
            inChanged = ((VNField) getInputFirstValue("inField")).getField() != inField;
            lastIn = getInputFirstValue("inField");
            inField = ((VNField) getInputFirstValue("inField")).getField();
            ptsChanged = getInputFirstValue("startPoints") != lastPts;
            lastPts = getInputFirstValue("startPoints");
            if (getInputFirstValue("startPoints") == null ||
                ((VNField) getInputFirstValue("startPoints")).getField() == null)
                pts = inField;
            else {
                ptsChanged = pts != ((VNField) getInputFirstValue("startPoints")).getField();
                pts = ((VNField) getInputFirstValue("startPoints")).getField();
            }
            if (!inChanged && !ptsChanged) {
                onInput = false;
                return;
            }

            if (pts instanceof RegularField) {
                RegularField regPts = (RegularField) pts;
                down = params.getDown();
                if (regPts.getDims() == null)
                    return;
                ptsDims = regPts.getDims();
                int n = (int) (pow(300., 1. / ptsDims.length));
                for (int i = 0; i < ptsDims.length; i++)
                    down[i] = (ptsDims[i] + n - 1) / n;
                params.setDownsize(1);
            } else
                params.setDownsize((int) pts.getNNodes() / 3000);
            computeUI.setInPts(pts);
            params.setVectorComponent(-1);
            for (int i = 0; i < inField.getNComponents(); i++)
                if (inField.getComponent(i).isNumeric() &&
                    (inField.getComponent(i).getVectorLength() == inField.getNSpace() ||
                    (inField instanceof RegularField &&
                    inField.getComponent(i).getVectorLength() == ((RegularField) inField).getDims().length))) {
                    params.setVectorComponent(i);
                    break;
                }
            if (params.getVectorComponent() == -1) {
                onInput = false;
                return;
            }
            if (inField instanceof RegularField) {
                inIrregularField = null;
                inRegularField = (RegularField) inField;
                if (inChanged)
                    computeUI.setInField(inRegularField);
                if (streamlines != null)
                    streamlines.clearFloatValueModificationListener();
                streamlines = new ComputeRegularFieldStreamlines(inRegularField, params);
                streamlines.addFloatValueModificationListener(progressListener);
                params.setDownsizeChanged(true);
            } else {
                inRegularField = null;
                inIrregularField = (IrregularField) inField;
                if (inChanged)
                    computeUI.setInField(inIrregularField);
                if (streamlines != null)
                    streamlines.clearFloatValueModificationListener();
                streamlines = new ComputeIrregularFieldStreamlines(inIrregularField, params);
                streamlines.addFloatValueModificationListener(progressListener);
                params.setDownsizeChanged(true);
            }
            onInput = false;
        }
        fromParams = false;
        if (streamlines != null) {
            checkAndDownsizeInPts();
            streamlines.setStartPoints(startPts);
            streamlines.updateStreamlines();
            if (params.computeDiffusion()) {
                int nFlowNodes = (int) startPts.getNNodes();
                int nSp = startPts.getNSpace();
                nSteps = params.getNThreads() * (params.getNForwardSteps() / params.getNThreads());
                flowField = new IrregularField(nFlowNodes);
                flowField.setNSpace(nSp);
                float[][] flowCoords = new float[nSteps][];
                for (int i = 0; i < nSteps; i++)
                    flowCoords[i] = flowField.produceCoords(i).getData();
                float[][] streamlinesCoords = streamlines.getStreamlineCoords();

                Thread[] workThreads = new Thread[params.getNThreads()];
                for (int iThread = 0; iThread < params.getNThreads(); iThread++) {
                    workThreads[iThread] = new Thread(new Remap(nFlowNodes, nSp, iThread, streamlinesCoords, flowCoords));
                    workThreads[iThread].start();
                }
                for (Thread workThread : workThreads)
                    try {
                        workThread.join();
                    }catch (Exception e) {
                    }
                flowField.setExtents(inField.getExtents());
                int[] nodes = new int[(int) startPts.getNNodes()];
                for (int i = 0; i < nodes.length; i++)
                    nodes[i] = i;
                CellArray ptsArray = new CellArray(CellType.POINT, nodes, null, null);
                CellSet ptsSet = new CellSet();
                ptsSet.addCells(ptsArray);
                ptsSet.setBoundaryCellArray(ptsArray);
                flowField.addCellSet(ptsSet);
                flowField.addComponent(DataArray.create(nodes, 1, "index"));
                setOutputValue("flowField", new VNIrregularField(flowField));
            } else {
                outIrregularField = streamlines.getOutField();
                setOutputValue("streamlinesField", new VNIrregularField(outIrregularField));
                outField = outIrregularField;
                prepareOutputGeometry();
                show();
            }
        }
    }
}
