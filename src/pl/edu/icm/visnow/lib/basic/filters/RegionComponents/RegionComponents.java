//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>

package pl.edu.icm.visnow.lib.basic.filters.RegionComponents;

import java.util.Arrays;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.utils.RegularFieldNeighbors;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.field.FloodFill;
import pl.edu.icm.visnow.lib.utils.numeric.HeapSort;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class RegionComponents extends OutFieldVisualizationModule
{

    private GUI computeUI = null;
    protected Params params;
    protected RegularField inField = null;
    protected boolean fromGUI = false;
    private static final int CHUNK = 2048;
    private RegularField outField = null;
    private int[] dims = null;
    private int[] xDims = null;
    private int xSize = 1;
    private int[] out = null;   // result array (-1 when out of region, otherwise the component number)
    private int[] xOut = null;                      // temporary result array with margins of the width 1
    private boolean[] region = null;            // true if inside region (extended with margins of the width 1)
    private int[] neighbors;
    private int[][] counts = null;
    private float[][] elongations = null;
    private int[] outCounts = null;
    private float[] outElongations = null;


    public RegionComponents()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                fromGUI = true;
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new GUI();
            }
        });
        computeUI.setParams(params);
        ui.addComputeGUI(computeUI);
        setPanel(ui);
    }

    public void update()
    {
        if (inField == null || params == null)
            return;
        outField = inField.cloneShallow();
        outField.removeComponents();
        computeUI.setWorkDesc(
                params.getComponentRange().getComponentName() + " in " +
                params.getComponentRange().getPhysicalLow()+ ":" +
                params.getComponentRange().getPhysicalUp());
        
        Arrays.fill(xOut, -1);
        counts = new int[1][CHUNK];
        elongations = new float[1][CHUNK];

        region = FloodFill.createExtendedRegion(inField.getComponent(params.getComponentRange().getComponentName()), 
                                                inField.getDims(), 
                                                params.getComponentRange().getLow(), 
                                                params.getComponentRange().getUp());

        if (inField.hasMask())
            FloodFill.maskRegion(inField.getCurrentMask(), dims, region);

        int regionCount = 0;
        
        switch (dims.length) {
            case 3:
                for (int i = 0; i < dims[2]; i++) {
                    setProgress((float)i / dims[2]);
                    for (int j = 0; j < dims[1]; j++)
                        for (int k = 0,
                                m = ((i + 1) * xDims[1] + j + 1) * xDims[0] + 1;
                                k < dims[0]; k++, m++)
                            if (region[m] && xOut[m] < 0) {
                                FloodFill.fill(regionCount, region, new int[]{m}, false, dims, neighbors, xOut, counts, elongations);
                                regionCount += 1;
                            }
                }
                break;
            case 2:
                for (int j = 0; j < dims[1]; j++)
                    for (int k = 0,
                            m = (j + 1) * xDims[0] + 1;
                            k < dims[0]; k++, m++)
                        if (region[m] && xOut[m] < 0) {
                            FloodFill.fill(regionCount, region, new int[]{m}, false, dims, neighbors, xOut, counts, elongations);
                            regionCount += 1;
                        }
                break;
            case 1:
                for (int k = 0; k < dims[0]; k++)
                    if (region[k + 1] && xOut[k + 1] < 0) {
                        FloodFill.fill(regionCount, region, new int[]{k + 1}, false, dims, neighbors, xOut, counts, elongations);
                        regionCount += 1;
                    }
                break;
        }
        
        out = FloodFill.restoreResultDimensions(dims, xOut);
        
        float[] el = new float[out.length];
        float[] elTable = elongations[0];
        for (int i = 0; i < out.length; i++) 
            if (out[i] >= 0)
                el[i] = elTable[out[i]];

        int[] cnts = counts[0];
        int[] count = new int[regionCount];
        int[] ind = new int[regionCount];
        int[] pind = new int[regionCount];
        for (int i = 0; i < ind.length; i++) {
            count[i] = -cnts[i];
            ind[i] = i;
        } // we use negative counts to obtain decreasing ordering
        HeapSort.heapsortInt(regionCount, 1, count, ind, 0, 0);
        for (int i = 0; i < ind.length; i++) {
            pind[ind[i]] = i;
            count[i] = -count[i];
        }
        int outSize = Math.min(pind.length, params.getMaxComps());
        outCounts = new int[outSize];
        outElongations = new float[outSize];
        for (int i = 0; i < outSize; i++) {
            int j = ind[i];
            outCounts[i] = cnts[j];
            outElongations[i] = elTable[j];
        }
        if (outSize < 254) {
            byte[] outB = new byte[out.length];
            Arrays.fill(outB, (byte)0);
            for (int i = 0; i < out.length; i++)
                if (out[i] >= 0) {
                    int k = pind[out[i]];
                    if (k < outSize)
                        outB[i] = (byte) (0xff & (k + 1));
                }
            String[] dataMap = new String[outSize + 2];
            dataMap[0] = "MAP";
            dataMap[1] = "-1: background";
            for (int i = 0; i < outSize ; i++) {
                dataMap[i + 2] = "" + i + ": component " + i;
            }
            outField.addComponent(DataArray.create(outB, 1, "regions").userData(dataMap));
        } 
        else {
            short[] outS = new short[out.length];
            Arrays.fill(outS, (short)0);
            for (int i = 0; i < out.length; i++)
                if (out[i] >= 0) {
                    int k = pind[out[i]];
                    if (k < outSize)
                        outS[i] = (short)(k + 1);
                }
            String[] dataMap = new String[outSize + 2];
            dataMap[0] = "MAP";
            dataMap[1] = "-1: background";
            for (int i = 0; i < outSize ; i++) {
                dataMap[i + 2] = "" + i + ": component " + i;
            }
            outField.addComponent(DataArray.create(outS, 1, "regions").userData(dataMap));
        }
        outField.addComponent(DataArray.create(el, 1, "elongation"));
        for (DataArray inDa : inField.getComponents())
            outField.addComponent(inDa.cloneShallow());
        outRegularField = (RegularField) outField;        
    }


    @Override
    public void onActive()
    {
        if (!fromGUI)
        {
            if (getInputFirstValue("inField") == null)
                return;
            RegularField inFld = ((VNRegularField) getInputFirstValue("inField")).getField();
            if (inFld == null)
                return;
            if (inFld != inField) {
                inField = inFld;
                params.setField(inField);

                dims = inField.getDims();
                xDims = new int[dims.length];
                for (int i = 0; i < xDims.length; i++)
                    xDims[i] = dims[i] + 2;
                switch (dims.length) {
                    case 3:
                        xSize = xDims[2] * xDims[1] * xDims[0];
                        neighbors = RegularFieldNeighbors.neighbors(xDims)[1];
                        break;
                    case 2:
                        xSize = xDims[1] * xDims[0];
                        neighbors = RegularFieldNeighbors.neighbors(xDims)[1];
                        break;
                    default:
                        xSize = xDims[0];
                        neighbors = RegularFieldNeighbors.neighbors(xDims)[0];
                }
                out = new int[(int) inField.getNNodes()];   // result array (-1 when out of region, otherwise the component number)
                xOut = new int[xSize];                      // temporary result array with margins of the width 1
            }
            return;
        }
        fromGUI = false;
        update();
        computeUI.setResults(outCounts, outElongations);
        setOutputValue("outField", new VNRegularField(outRegularField));
        prepareOutputGeometry();
        show();
    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

}
