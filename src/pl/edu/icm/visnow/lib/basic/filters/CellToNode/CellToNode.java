//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.CellToNode;

import java.util.ArrayList;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.Cell;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import static pl.edu.icm.visnow.lib.basic.filters.CellToNode.CellToNodeShared.DROP;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class CellToNode extends OutFieldVisualizationModule
{

    /**
     * Creates a new instance of SurfaceSmoother
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected IrregularField inField = null;
    protected GUI computeUI = null;

    public CellToNode()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(DROP, true)
        };
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    public void compute(Parameters p)
    {
        if (p.get(DROP))
            outIrregularField.getCellSets().clear();
        int nNodes = (int) inField.getNNodes();
        int[] counts = new int[nNodes];
        int csId = 0;
        for (CellSet cs : inField.getCellSets()) {
            ArrayList<DataArray> cellData = cs.getComponents();
            for (DataArray inDA : cellData) {
                float[] inD = inDA.getRawFloatArray().getData();
                int vlen = inDA.getVectorLength();
                float[] outD = new float[nNodes * vlen];
                System.out.println("in len "+inD.length+" out len "+outD.length);
                java.util.Arrays.fill(counts, 0);
                java.util.Arrays.fill(outD, 0.f);
                for (int i = 0; i < Cell.getNProperCellTypes(); i++) {
                    int nv = CellType.getType(i).getNVertices();
                    CellArray ca = cs.getCellArray(CellType.getType(i));
                    if (ca == null)
                        continue;
                    int[] nodes = ca.getNodes();
                    int[] indices = ca.getDataIndices();
                    for (int j = 0; j < ca.getNCells(); j++) {
                        int iin = vlen * indices[j];
                        for (int l = nv * j; l < nv * (j + 1); l++) {
                            int iout = vlen * nodes[l];
                            counts[nodes[l]] += 1;
                            for (int k = 0; k < vlen; k++)
                                try {
                                    outD[iout + k] += inD[iin + k];
                                } catch (Exception e) {
                                    System.out.printf("%s cell array %d cell nnodes %d  cell %d ",
                                            cs.getName(), i, nv, j);
                                    System.out.printf(" node %d:%d ",
                                            l, nodes[l]);
                                    System.out.printf("in idx %d out idx %d %n",
                                            iin, iout);
                                }
                        }
                    }
                }
                for (int i = 0; i < nNodes; i++) {
                    if (counts[i] == 0)
                        continue;
                    float f = 1.f / counts[i];
                    for (int j = i * vlen; j < (i + 1) * vlen; j++)
                        outD[j] *= f;
                }
                String name = inDA.getName();
                if (outIrregularField.getComponent(name) != null)
                    name += csId;
                outIrregularField.addComponent(DataArray.create(outD, vlen, name).unit(inDA.getUnit()).userData(inDA.getUserData()));
            }
            if (p.get(DROP)) {
                CellSet outCS = new CellSet(cs.getName());
                outCS.setCellArrays(cs.getCellArrays());
                outCS.setBoundaryCellArrays(cs.getBoundaryCellArrays());
                outIrregularField.addCellSet(outCS);
            }
            csId += 1;
        }
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            inField = ((VNIrregularField) getInputFirstValue("inField")).getField();
            outIrregularField = inField.cloneShallow();

            Parameters p = parameters.getReadOnlyClone();

            notifyGUIs(p, true, true);

            compute(p);

            setOutputValue("outField", new VNIrregularField(outIrregularField));
            outField = outIrregularField;
            prepareOutputGeometry();
            show();
        }
    }
}
