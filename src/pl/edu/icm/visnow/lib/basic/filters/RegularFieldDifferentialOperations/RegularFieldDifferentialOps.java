//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.ProgressAgent;
import pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations.RegularFieldDifferentialOperationsShared.ScalarOperation;
import static pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations.RegularFieldDifferentialOperationsShared.ScalarOperation.*;
import pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations.RegularFieldDifferentialOperationsShared.VectorOperation;
import static pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations.RegularFieldDifferentialOperationsShared.VectorOperation.*;
import static pl.edu.icm.visnow.lib.utils.numeric.FiniteDifferences.Derivatives.*;
import pl.edu.icm.visnow.lib.utils.numeric.FiniteDifferences.InvertedJacobian;
import pl.edu.icm.visnow.lib.utils.numeric.PointwiseLinearAlgebra2D;
import pl.edu.icm.visnow.lib.utils.numeric.PointwiseLinearAlgebra3D;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class RegularFieldDifferentialOps
{

    private static final Logger LOGGER = Logger.getLogger(RegularFieldDifferentialOps.class);

    public static RegularField compute(RegularField inField, HashMap<String, ScalarOperation[]> scalarOps, HashMap<String, VectorOperation[]> vectorOps, int nThreads) {
        return compute(inField, scalarOps, vectorOps, null, nThreads);
    }
    
    public static RegularField compute(RegularField inField, HashMap<String, ScalarOperation[]> scalarOps, HashMap<String, VectorOperation[]> vectorOps, ProgressAgent progressAgent, int nThreads)
    {
        RegularField outField = null;

        int[] dims = null;
        int dim = 0;
        int nData = 0;

        float[] invJacobian = null;

        dims = inField.getDims();
        dim = dims.length;
        nData = 1;
        for (int i = 0; i < dims.length; i++)
            nData *= dims[i];
        invJacobian = InvertedJacobian.computeInvertedJacobian(nThreads, inField);

        outField = inField.cloneShallow();

        if(scalarOps != null)
            for (Map.Entry<String, ScalarOperation[]> entry : scalarOps.entrySet()) {
                List<ScalarOperation> scalarOperations = Arrays.asList((ScalarOperation[]) entry.getValue());
                if (scalarOperations.size() > 0) {
                    DataArray da = inField.getComponent(entry.getKey());
                    float[] fda = da.getRawFloatArray().getData();

                    //first derivative must be computed and stored
                    float[] grad = computeDerivatives(nThreads, inField.getDims(), fda, invJacobian);

                    if (scalarOperations.contains(GRADIENT)) {
                        outField.addComponent(DataArray.create(grad, dim, da.getName() + "_grad"));
                        if(progressAgent != null) progressAgent.increase();
                    }

                    if (scalarOperations.contains(GRADIENT_NORM)) {
                        float[] gnorm = new float[nData];
                        for (int k = 0, l = 0; k < nData; k++) {
                            double s = 0;
                            for (int m = 0; m < dim; m++, l++)
                                s += grad[l] * grad[l];
                            gnorm[k] = (float) sqrt(s);
                        }
                        outField.addComponent(DataArray.create(gnorm, 1, da.getName() + "_gradnorm"));
                        if(progressAgent != null) progressAgent.increase();

                    }

                    if (scalarOperations.contains(NORMALIZED_GRADIENT)) {
                        float[] normg = new float[dim * nData];
                        for (int k = 0, l = 0, n = 0; k < nData; k++) {
                            float s = 0;
                            for (int m = 0; m < dim; m++, l++)
                                s += grad[l] * grad[l];
                            if (s == 0)
                                for (int m = 0; m < dim; m++, n++)
                                    normg[n] = 0;
                            else {
                                s = 1 / (float) sqrt(s);
                                for (int m = 0; m < dim; m++, n++)
                                    normg[n] = grad[n] * s;
                            }
                        }
                        outField.addComponent(DataArray.create(normg, dim, da.getName() + "_normgrad"));
                        if(progressAgent != null) progressAgent.increase();
                    }

                    if (scalarOperations.contains(GRADIENT_COMPONENTS)) 
                        for (int coord = 0; coord < dim; coord++) {
                            float[] cmp = new float[nData];
                            for (int k = 0, l = coord; k < nData; k++, l += dim) 
                                cmp[k] = grad[l];
                            outField.addComponent(DataArray.create(cmp, 1, da.getName() + "_d" + coord));
                            if(progressAgent != null) progressAgent.increase();
                        }

                    //second derivatives
                    if (scalarOperations.contains(LAPLACIAN) || scalarOperations.contains(HESSIAN) || scalarOperations.contains(HESSIAN_EIGEN)) {
                        float[] d2 = computeDerivatives(nThreads, inField.getDims(), grad, invJacobian);
                        float[] h = symmetrize(dim, d2);

                        if (scalarOperations.contains(LAPLACIAN)) {
                            float[] lapl = new float[nData];
                            if (dim == 3)
                                for (int k = 0, l = 0; k < nData; k++, l += 6)
                                    lapl[k] = h[l] + h[l + 3] + h[l + 5];
                            else if (dim == 2)
                                for (int k = 0, l = 0; k < nData; k++, l += 3)
                                    lapl[k] = h[l] + h[l + 2];
                            outField.addComponent(DataArray.create(lapl, 1, da.getName() + "_lap"));
                            if(progressAgent != null) progressAgent.increase();
                        }

                        if (scalarOperations.contains(HESSIAN)) {
                            try {
                                outField.addComponent(DataArray.create(h, (dim * (dim + 1)) / 2, da.getName() + "_hessian")).
                                        setMatrixProperties(new int[]{dim, dim}, true);
                            } catch (Exception e) {
                                LOGGER.warn("Could not evaluate hessian", e);
                            }
                            if(progressAgent != null) progressAgent.increase();
                        }

                        if (scalarOperations.contains(HESSIAN_EIGEN)) {
                            float[][] hEigV = new float[dim][nData];
                            float[][] hEigR = new float[dim][nData * dim];
                            if (dim == 3)
                                PointwiseLinearAlgebra3D.symEigen(nThreads, h, hEigV, hEigR);
                            else
                                PointwiseLinearAlgebra2D.symEigen(nThreads, h, hEigV, hEigR);
                            for (int k = 0; k < dim; k++)
                                try {
                                    outField.addComponent(DataArray.create(hEigV[k], 1, da.getName() + "H_eigval_" + k));
                                    outField.addComponent(DataArray.create(hEigR[k], dim, da.getName() + "H_eigvec_" + k));
                                } catch (Exception e) {
                                    LOGGER.warn("Could not evaluate hessian eigenvalues", e);
                                }
                            if(progressAgent != null) progressAgent.increase();
                        }

                    }
                }
            }

        if(vectorOps != null)
            for (Map.Entry<String, VectorOperation[]> entry : vectorOps.entrySet()) {
                List<VectorOperation> vectorOperations = Arrays.asList((VectorOperation[]) entry.getValue());
                if (vectorOperations.size() > 0) {
                    DataArray da = inField.getComponent(entry.getKey());
                    float[] fda = da.getRawFloatArray().getData();

                    float[] h = computeDerivatives(nThreads, inField.getDims(), fda, invJacobian);
                    if (vectorOperations.contains(DERIV)) {
                        for (int k = 0; k < dim; k++) {
                            float[] dta = new float[dim * nData];
                            for (int l = 0, m = 0; l < nData; l++)
                                for (int p = 0, n = dim * (dim * l + k); p < dim; p++, m++, n++)
                                    dta[m] = h[n];
                            outField.addComponent(DataArray.create(dta, dim, da.getName() + "_deriv_" + k));
                        }
                        if(progressAgent != null) progressAgent.increase();
                    }

                    if (vectorOperations.contains(DIV)) {

                        float[] div = new float[nData];
                        for (int k = 0, l = 0; k < nData; k++, l += dim * dim) {
                            float s = 0;
                            for (int m = 0; m < dim; m++)
                                s += h[l + m * (dim + 1)];
                            div[k] = s;
                        }
                        outField.addComponent(DataArray.create(div, 1, da.getName() + "_div"));
                        if(progressAgent != null) progressAgent.increase();
                    }

                    if (vectorOperations.contains(ROT)) {
                        if (dim == 3) {
                            float[] rot = new float[dim * nData];
                            for (int k = 0, l = 0, n = 0; k < nData; k++, l += 3, n += 9) {
                                rot[l] = h[n + 7] - h[n + 5];
                                rot[l + 1] = h[n + 2] - h[n + 6];
                                rot[l + 2] = h[n + 3] - h[n + 1];
                            }
                            outField.addComponent(DataArray.create(rot, dim, da.getName() + "_rot"));
                        } else {
                            float[] rot = new float[nData];
                            for (int k = 0, n = 0; k < nData; k++, n += 4)
                                rot[k] = h[n + 2] - h[n + 1];
                            outField.addComponent(DataArray.create(rot, 1, da.getName() + "_rot"));
                        }
                        if(progressAgent != null) progressAgent.increase();
                    }
                }
            }

        return outField;
    }
}
