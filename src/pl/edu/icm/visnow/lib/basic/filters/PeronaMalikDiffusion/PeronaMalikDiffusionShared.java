/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.icm.visnow.lib.basic.filters.PeronaMalikDiffusion;

import pl.edu.icm.visnow.engine.core.ParameterName;

/**
 *
 * @author jnow
 */
public class PeronaMalikDiffusionShared
{
    // PARAMETERS
    static final ParameterName<Integer> ITERATION_NUMBER = new ParameterName("iterationNumber");
    static final ParameterName<Float> LAMBDA_FOR_ITERATION_SCHEME = new ParameterName("lambdaForIterationScheme");
    static final ParameterName<Float> PARAMETER_K = new ParameterName("parameterK");
    static final ParameterName<Float> NABLA_WEIGHT_INDEX_I = new ParameterName("nablaWeightIndexI");
    static final ParameterName<Float> NABLA_WEIGHT_INDEX_J = new ParameterName("nablaWeightIndexJ");
    static final ParameterName<Float> NABLA_WEIGHT_INDEX_K = new ParameterName("nablaWeightIndexK");

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)    
    //integer: 1,2 or 3
    static final ParameterName<Integer> META_NUMBER_OF_DIMENSIONS = new ParameterName("META Number of dimensions");

}
