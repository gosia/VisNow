/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
import pl.edu.icm.visnow.engine.core.ParameterProxy;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.gui.widgets.RunButton.RunState;
import static pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations.RegularFieldDifferentialOperationsShared.*;
import static pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations.RegularFieldDifferentialOperationsShared.ScalarOperation.*;
import static pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations.RegularFieldDifferentialOperationsShared.VectorOperation.*;
import pl.edu.icm.visnow.system.swing.FixedGridBagLayoutPanel;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class GUI extends FixedGridBagLayoutPanel
{
    private static final Logger LOGGER = Logger.getLogger(GUI.class);

    //null for name column
    private final static ScalarOperation[] SCALAR_TABLE_COLUMN_ORDER = {null, GRADIENT, GRADIENT_COMPONENTS, GRADIENT_NORM, NORMALIZED_GRADIENT, LAPLACIAN, HESSIAN, HESSIAN_EIGEN};
    private final static VectorOperation[] VECTOR_TABLE_COLUMN_ORDER = {null, DERIV, ROT, DIV};

    private Parameters parameters = null;
    //unfortunatelly this flag is necessary to distinguish user action on jTable from setter actions.
    private boolean tableActive = false;

    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        int[] preferredWidth = new int[]{60, 15, 20, 15, 25, 15, 15, 30};
        for (int i = 0; i < 8; i++) scalarComponentTable.getColumnModel().getColumn(i).setPreferredWidth(preferredWidth[i]);
        preferredWidth = new int[]{100, 20, 20, 20};
        for (int i = 0; i < 4; i++) vectorComponentTable.getColumnModel().getColumn(i).setPreferredWidth(preferredWidth[i]);

        scalarComponentTable.getTableHeader().setReorderingAllowed(false);
        vectorComponentTable.getTableHeader().setReorderingAllowed(false);

        scalarTable.add(scalarComponentTable.getTableHeader(), BorderLayout.NORTH);
        vectorTable.add(vectorComponentTable.getTableHeader(), BorderLayout.NORTH);

        TableModelListener scalarTableListener = new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {

                if (scalarComponentTable.getModel().getRowCount() == parameters.get(META_SCALAR_COMPONENT_NAMES).length) {
                    ScalarOperation[][] scalarOperations = new ScalarOperation[parameters.get(META_SCALAR_COMPONENT_NAMES).length][];
                    for (int row = 0; row < scalarOperations.length; row++) {
                        List<ScalarOperation> operations = new ArrayList<>();
                        for (int column = 1; column < SCALAR_TABLE_COLUMN_ORDER.length; column++)
                            if ((Boolean) scalarComponentTable.getModel().getValueAt(row, column))
                                operations.add(SCALAR_TABLE_COLUMN_ORDER[column]);

                        scalarOperations[row] = operations.toArray(new ScalarOperation[operations.size()]);
                    }

                    if (tableActive) runButton.setPendingIfNoAuto();
                    parameters.set(SCALAR_OPERATIONS, scalarOperations);
                }
            }
        };

        TableModelListener vectorTableListener = new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                if (vectorComponentTable.getModel().getRowCount() == parameters.get(META_VECTOR_COMPONENT_NAMES).length) {
                    VectorOperation[][] vectorOperations = new VectorOperation[parameters.get(META_VECTOR_COMPONENT_NAMES).length][];
                    for (int row = 0; row < vectorOperations.length; row++) {
                        List<VectorOperation> operations = new ArrayList<>();
                        for (int column = 1; column < VECTOR_TABLE_COLUMN_ORDER.length; column++)
                            if ((Boolean) vectorComponentTable.getModel().getValueAt(row, column))
                                operations.add(VECTOR_TABLE_COLUMN_ORDER[column]);

                        vectorOperations[row] = operations.toArray(new VectorOperation[operations.size()]);
                    }

                    if (tableActive) runButton.setPendingIfNoAuto();
                    parameters.set(VECTOR_OPERATIONS, vectorOperations);
                }
            }
        };

        ((DefaultTableModel) scalarComponentTable.getModel()).addTableModelListener(scalarTableListener);
        ((DefaultTableModel) vectorComponentTable.getModel()).addTableModelListener(vectorTableListener);

        runButton.setPendingIfNoAuto();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        scalarPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        scalarTable = new javax.swing.JPanel();
        scalarComponentTable = new javax.swing.JTable();
        vectorPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        vectorTable = new javax.swing.JPanel();
        vectorComponentTable = new javax.swing.JTable();
        runButton = new pl.edu.icm.visnow.gui.widgets.RunButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        scalarPanel.setLayout(new java.awt.GridBagLayout());

        jLabel2.setText("Scalar components:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        scalarPanel.add(jLabel2, gridBagConstraints);

        scalarTable.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 0, 0, javax.swing.UIManager.getDefaults().getColor("ScrollBar.darkShadow")));
        scalarTable.setLayout(new java.awt.BorderLayout());

        scalarComponentTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null, null, null, null, null, null, null}
            },
            new String []
            {
                "", "<html>\u2207</html>", "\u2202/\u2202i", "<html>|\u2207|</html>", "<html>\u2207/|\u2207|</html>", "\u0394", "<html>D<sup>2</sup></html>", "<html>D<sup>2</sup>eig</html>"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true, true, true, true, true, true, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        scalarComponentTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        scalarTable.add(scalarComponentTable, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 4, 0);
        scalarPanel.add(scalarTable, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(scalarPanel, gridBagConstraints);

        vectorPanel.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Vector components:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        vectorPanel.add(jLabel1, gridBagConstraints);

        vectorTable.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 0, 0, javax.swing.UIManager.getDefaults().getColor("ScrollBar.darkShadow")));
        vectorTable.setLayout(new java.awt.BorderLayout());

        vectorComponentTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null, null, null}
            },
            new String []
            {
                "", "D", "rot", "div"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }
        });
        vectorTable.add(vectorComponentTable, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 4, 0);
        vectorPanel.add(vectorTable, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(vectorPanel, gridBagConstraints);

        runButton.addUserActionListener(new pl.edu.icm.visnow.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
                runButtonUserChangeAction(evt);
            }
            public void userAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(runButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void runButtonUserChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButtonUserChangeAction
    {//GEN-HEADEREND:event_runButtonUserChangeAction
        parameters.set(RUNNING_MESSAGE, (RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction

    /**
     * @param p cloned parameters
     */
    void updateGUI(final ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {

        tableActive = false;

        scalarPanel.setVisible(p.get(META_SCALAR_COMPONENT_NAMES).length > 0);

        ((DefaultTableModel) scalarComponentTable.getModel()).setRowCount(0);

        Object[] row = new Object[SCALAR_TABLE_COLUMN_ORDER.length];

        for (int i = 0; i < p.get(META_SCALAR_COMPONENT_NAMES).length; i++) {
            Arrays.fill(row, Boolean.FALSE);
            row[0] = p.get(META_SCALAR_COMPONENT_NAMES)[i];

            for (ScalarOperation scalarOperation : p.get(SCALAR_OPERATIONS)[i])
                row[Arrays.asList(SCALAR_TABLE_COLUMN_ORDER).indexOf(scalarOperation)] = true;
            ((DefaultTableModel) scalarComponentTable.getModel()).addRow(row);

        }

        vectorPanel.setVisible(p.get(META_VECTOR_COMPONENT_NAMES).length > 0);

        ((DefaultTableModel) vectorComponentTable.getModel()).setRowCount(0);

        row = new Object[VECTOR_TABLE_COLUMN_ORDER.length];

        for (int i = 0; i < p.get(META_VECTOR_COMPONENT_NAMES).length; i++) {
            Arrays.fill(row, Boolean.FALSE);
            row[0] = p.get(META_VECTOR_COMPONENT_NAMES)[i];

            for (VectorOperation vectorOperation : p.get(VECTOR_OPERATIONS)[i])
                row[Arrays.asList(VECTOR_TABLE_COLUMN_ORDER).indexOf(vectorOperation)] = true;
            ((DefaultTableModel) vectorComponentTable.getModel()).addRow(row);

        }

        
        tableActive = true;
        
        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    public void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private pl.edu.icm.visnow.gui.widgets.RunButton runButton;
    private javax.swing.JTable scalarComponentTable;
    private javax.swing.JPanel scalarPanel;
    private javax.swing.JPanel scalarTable;
    private javax.swing.JTable vectorComponentTable;
    private javax.swing.JPanel vectorPanel;
    private javax.swing.JPanel vectorTable;
    // End of variables declaration//GEN-END:variables

    public static void main(String[] args)
    {
        JFrame f = new JFrame();
        f.add(new GUI());
        f.pack();
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
}
