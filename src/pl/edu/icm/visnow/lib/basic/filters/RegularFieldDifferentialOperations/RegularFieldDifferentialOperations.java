//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.engine.core.ProgressAgent;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.*;
import static pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations.RegularFieldDifferentialOperationsShared.*;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class RegularFieldDifferentialOperations extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(RegularFieldDifferentialOperations.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private RegularField inField = null;
    private GUI computeUI = null;

    private int runQueue = 0;

    /**
     * Creates a new instance of CreateGrid
     */
    public RegularFieldDifferentialOperations()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startIfNotInQueue();

            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(META_SCALAR_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(META_VECTOR_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(SCALAR_OPERATIONS, new ScalarOperation[][]{}),
            new Parameter<>(VECTOR_OPERATIONS, new VectorOperation[][]{}),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);

        String[] newScalarComponentNames = inField.getScalarComponentNames();//getScalarComponentNames(inField);
        String[] newVectorComponentNames = inField.getVectorComponentNames();//getVectorComponentNames(inField);

        ScalarOperation[][] newSO = new ScalarOperation[newScalarComponentNames.length][0];
        VectorOperation[][] newVO = new VectorOperation[newVectorComponentNames.length][0];

        if (!resetParameters) { //try to restore vector/scalar operations (based on component names)
            List<String> scalarComponentNames = Arrays.asList(parameters.get(META_SCALAR_COMPONENT_NAMES));
            List<String> vectorComponentNames = Arrays.asList(parameters.get(META_VECTOR_COMPONENT_NAMES));

            ScalarOperation[][] sO = parameters.get(SCALAR_OPERATIONS);
            VectorOperation[][] vO = parameters.get(VECTOR_OPERATIONS);

            //if just only one component then use the same operations
            if (newScalarComponentNames.length == 1 && scalarComponentNames.size() == newScalarComponentNames.length)
                newSO = parameters.get(SCALAR_OPERATIONS);
            //otherwise restore by component name
            else {
                for (int i = 0; i < newScalarComponentNames.length; i++) {
                    String componentName = newScalarComponentNames[i];
                    int oldPosition = scalarComponentNames.indexOf(componentName);
                    if (oldPosition != -1) newSO[i] = sO[oldPosition];
                }
            }

            //if just only one component then use the same operations
            if (newVectorComponentNames.length == 1 && vectorComponentNames.size() == newVectorComponentNames.length)
                newVO = parameters.get(VECTOR_OPERATIONS);
            //otherwise restore by component names
            else {
                for (int i = 0; i < newVectorComponentNames.length; i++) {
                    String componentName = newVectorComponentNames[i];
                    int oldPosition = vectorComponentNames.indexOf(componentName);
                    if (oldPosition != -1) newVO[i] = vO[oldPosition];
                }
            }
        }

        parameters.set(VECTOR_OPERATIONS, newVO);
        parameters.set(SCALAR_OPERATIONS, newSO);

        parameters.set(META_SCALAR_COMPONENT_NAMES, newScalarComponentNames);
        parameters.set(META_VECTOR_COMPONENT_NAMES, newVectorComponentNames);

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentDimNum = !isFromVNA() && (inField == null || inField.getDimNum() != newField.getDimNum());
            boolean isNewField = !isFromVNA() && newField != inField;

            inField = newField;

            Parameters p;

            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentDimNum);
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentDimNum, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data

                HashMap<String, ScalarOperation[]> scalarOps = new HashMap<>();
                String[] scn = p.get(META_SCALAR_COMPONENT_NAMES);
                for (int i = 0; i < scn.length; i++) scalarOps.put(scn[i], p.get(SCALAR_OPERATIONS)[i]);

                HashMap<String, VectorOperation[]> vectorOps = new HashMap<>();
                String[] vcn = p.get(META_VECTOR_COMPONENT_NAMES);
                for (int i = 0; i < vcn.length; i++) vectorOps.put(vcn[i], p.get(VECTOR_OPERATIONS)[i]);

                int totalSteps = 1; // 1 for prepare geometry :)
                for (ScalarOperation[] scalarOperations : scalarOps.values()) totalSteps += scalarOperations.length;
                for (VectorOperation[] vectorOperations : vectorOps.values()) totalSteps += vectorOperations.length;

                ProgressAgent progressAgent = getProgressAgent(totalSteps);

                outField = RegularFieldDifferentialOps.compute(inField, scalarOps, vectorOps, progressAgent, VisNow.availableProcessors());
                outRegularField = (RegularField) outField;
                setOutputValue("outField", new VNRegularField(outRegularField));
                prepareOutputGeometry();
                show();
                progressAgent.increase();
            }
        }
    }
}
