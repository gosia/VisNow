package pl.edu.icm.visnow.lib.basic.writers.FieldWriter;

import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.lib.utils.io.AVSRegularFieldWriterCore;
import pl.edu.icm.visnow.lib.utils.io.VisNowRegularFieldWriterCore;

/**
 *
 * @author szpak
 */
public class FieldWriterCore
{

    private final Params params;

    private RegularField inField;

    //FIXME: no field schema testing ... modules may be incorrectly connected now (in Unicore wizard).
    //TODO: inField is of type Field in module.xml - fix it!
    public final static String IN_RFIELD_MAIN = "inField";

    public FieldWriterCore(Params params)
    {
        this.params = params;
    }

    public static FieldWriterCore createCore()
    {
        return new FieldWriterCore(new Params());
    }

    public static FieldWriterCore createCore(Params params)
    {
        return new FieldWriterCore(params);
    }

    //    //FIXME: classCastException may occure here - resolve this somehow...
    //    public static FieldWriterCore createCore(Parameters parameters) {
    //        return new FieldWriterCore((Params) parameters);
    //    }
    public Params getParams()
    {
        return params;
    }

    public void setInField(RegularField inField)
    {
        this.inField = inField;
    }

    public void recalculate()
    {
        boolean result;
        if (params.isAVS()) {
            result = new AVSRegularFieldWriterCore((RegularField) inField, params).writeField();
        } else {
            result = new VisNowRegularFieldWriterCore((RegularField) inField, params).writeField();
        }

        //TODO: return result by exception, by inner status, by return value
    }

    public static String[] getInFieldNames()
    {
        return new String[]{IN_RFIELD_MAIN};
    }

    public static String[] getOutFieldNames()
    {
        return new String[]{};
    }

    public void setInField(String name, Field inField)
    {
        if (name.equals(IN_RFIELD_MAIN))
            this.inField = (RegularField) inField;
        else
            throw new IllegalArgumentException("Incorrect field name: " + name);
    }

}
