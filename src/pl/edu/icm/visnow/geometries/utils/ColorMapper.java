//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.geometries.utils;

import static org.apache.commons.math3.util.FastMath.*;
import javax.vecmath.Color3f;
import pl.edu.icm.visnow.datamaps.ColorMapManager;
import pl.edu.icm.jscic.DataContainer;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.dataarrays.ByteDataArray;
import pl.edu.icm.jscic.dataarrays.ComplexDataArray;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jscic.dataarrays.DataObjectInterface;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.ObjectLargeArray;
import pl.edu.icm.visnow.geometries.parameters.ComponentColorMap;
import pl.edu.icm.visnow.geometries.parameters.ColorComponentParams;
import pl.edu.icm.visnow.geometries.parameters.DataMappingParams;
import pl.edu.icm.visnow.geometries.parameters.TransparencyParams;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ColorMapper
{

    private static void hsvtorgb(byte[] colors)
    {
        float hue, sat, val, f, p, q, t;
        int m;
        for (int i = 0; i < colors.length; i += 4) {
            hue = (0xff & colors[i]) / 42.6f;
            sat = (0xff & colors[i + 1]) / 255f;
            val = (0xff & colors[i + 2]) / 255f;
            m = (int) (floor(hue));
            f = hue - m;
            p = val * (1 - sat);
            q = val * (1 - (sat * f));
            t = val * (1 - (sat * (1 - f)));
            switch (m) {
                case 0:
                    colors[i] = (byte) (0xff & (int) (255 * val));
                    colors[i + 1] = (byte) (0xff & (int) (255 * t));
                    colors[i + 2] = (byte) (0xff & (int) (255 * p));
                    break;
                case 1:
                    colors[i] = (byte) (0xff & (int) (255 * q));
                    colors[i + 1] = (byte) (0xff & (int) (255 * val));
                    colors[i + 2] = (byte) (0xff & (int) (255 * p));
                    break;
                case 2:
                    colors[i] = (byte) (0xff & (int) (255 * p));
                    colors[i + 1] = (byte) (0xff & (int) (255 * val));
                    colors[i + 2] = (byte) (0xff & (int) (255 * t));
                    break;
                case 3:
                    colors[i] = (byte) (0xff & (int) (255 * p));
                    colors[i + 1] = (byte) (0xff & (int) (255 * q));
                    colors[i + 2] = (byte) (0xff & (int) (255 * val));
                    break;
                case 4:
                    colors[i] = (byte) (0xff & (int) (255 * t));
                    colors[i + 1] = (byte) (0xff & (int) (255 * p));
                    colors[i + 2] = (byte) (0xff & (int) (255 * val));
                    break;
                case 5:
                    colors[i] = (byte) (0xff & (int) (255 * val));
                    colors[i + 1] = (byte) (0xff & (int) (255 * p));
                    colors[i + 2] = (byte) (0xff & (int) (255 * q));
                    break;
            }
        }
    }

    public static byte[] mapColorsIndexed(DataContainer data, DataMappingParams dataMappingParams,
                                          long[] indices, Color3f col, byte[] colors)
    {
        int nInd = indices.length;
        if (colors == null || colors.length != 4 * nInd)
            colors = new byte[4 * nInd];
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                colors = mapColorsIndexedColormapped(data, dataMappingParams, indices, col, colors);
                break;
            case DataMappingParams.RGB:
                //            colors = mapColorsIndexedRGB(data, dataMappingParams, indices, col, colors);
                break;
        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;
        if (data instanceof Field && ((Field) data).hasMask()) {
            LogicLargeArray valid = ((Field) data).getCurrentMask();
            float[] c = new float[3];
            col.get(c);
            byte[] bc = new byte[4];
            for (int i = 0; i < c.length; i++)
                bc[i] = (byte) (0xff & (int) (255 * c[i]));
            for (int i = 0; i < nInd; i++)
                if (valid.getByte(indices[i]) == 0) {
                    colors[4 * i] = bc[0];
                    colors[4 * i + 1] = bc[1];
                    colors[4 * i + 2] = bc[2];
                    colors[4 * i + 3] = (byte) 127;
                }
        }
        return colors;
    }

    private static byte[] mapColorsIndexedColormapped(DataContainer data, DataMappingParams dataMappingParams,
                                                      long[] indices, Color3f col, byte[] colors)
    {
        int nInd = indices.length;
        ComponentColorMap cMap = dataMappingParams.getColorMap0();
        ComponentColorMap modMap = null;
        ColorComponentParams modParams = null;
        if (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION)
            modParams = dataMappingParams.getSatParams();
        else if (dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION)
            modParams = dataMappingParams.getValParams();
        boolean wrapCMap = cMap.isWrap();
        DataArray colData = data.getComponent(cMap.getDataComponentName());
        if (colData == null || !colData.isNumeric()) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue()),
                (byte) 100
            };
            for (int i = 0; i < colors.length;)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            return colors;
        }
        if (colData.getUserData(0).equalsIgnoreCase("colors") &&
            colData.getType() == DataArrayType.FIELD_DATA_BYTE &&
            colData.getVectorLength() >= 3) {
            int vlen = colData.getVectorLength();
            for (int i = 0; i < nInd; i++) {
                float[] lc = colData.getFloatElement(indices[i]);
                for (int j = 0, k = 4 * i; j < vlen; j++, k++)
                    colors[k] = (byte) ((int) lc[j] & 0xff);
            }
            if (vlen == 3)
                for (int i = 3; i < colors.length; i += 4)
                    colors[i] = (byte) 255;
            return colors;
        }

        byte[] colorMapLUT = cMap.getRGBByteColorTable();
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = cMap.getDataMin();
        float d = nColors / (cMap.getDataMax() - low);
        int cIndex;
        double v;
        int vl = colData.getVectorLength();

        for (int i = 0; i < nInd; i++) {
            float[] lc = colData.getFloatElement(indices[i]);
            if (vl == 1)
                v = lc[0];
            else {
                v = 0;
                for (int j = 0; j < vl; j++)
                    v += lc[j] * lc[j];
                v = sqrt(v);
            }
            if (!wrapCMap)
                cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
            else
                cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
            for (int j = 0, l = 4 * i; j < 3; j++, l++)
                colors[l] = colorMapLUT[cIndex + j];
            for (int j = 0, l = 4 * i; j < 3; j++, l++)
                colors[l] = colorMapLUT[cIndex + j];
        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;

        if (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION ||
            dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION) {
            boolean modSat = (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION);
            colData = data.getComponent(modParams.getDataComponentName());
            if (colData == null)
                return colors;
            vl = colData.getVectorLength();
            if (vl == 1)
                low = modParams.getDataMin();
            else
                low = 0;
            float mlow = modParams.getColorComponentMin();
            float mup = modSat ? 1 : modParams.getColorComponentMax();
            d = (mup - mlow) / (modParams.getDataMax() - low);
            int r, g, b;
            double u;
            for (int i = 0, l = 0; i < nInd; i++, l += 4) {
                float[] lc = colData.getFloatElement(indices[i]);
                if (vl == 1)
                    u = lc[0];
                else {
                    u = 0;
                    for (int j = 0; j < vl; j++)
                        u += lc[j] * lc[j];
                    u = mlow + d * sqrt(u);
                }
                u = max(mlow, min(mup, u));
                r = 0xff & colors[l];
                g = 0xff & colors[l + 1];
                b = 0xff & colors[l + 2];
                for (int j = 0; j < 3; j++)
                    if (modSat) {
                        int m = r;
                        if (g > m)
                            m = g;
                        if (b > m)
                            m = b;
                        colors[l] = (byte) (0xff & (int) (m - u * (m - r)));
                        colors[l + 1] = (byte) (0xff & (int) (m - u * (m - g)));
                        colors[l + 2] = (byte) (0xff & (int) (m - u * (m - b)));
                    } else if (u <= 1) {
                        colors[l] = (byte) (0xff & (int) (u * r));
                        colors[l + 1] = (byte) (0xff & (int) (u * g));
                        colors[l + 2] = (byte) (0xff & (int) (u * b));
                    } else {
                        double w = 2 - u;
                        colors[l] = (byte) (0xff & (int) (255 - u * (255 - r)));
                        colors[l + 1] = (byte) (0xff & (int) (255 - u * (255 - g)));
                        colors[l + 2] = (byte) (0xff & (int) (255 - u * (255 - b)));
                    }
            }
        }
        if (dataMappingParams.getColorMapModification() == DataMappingParams.BLEND_MAP_MODIFICATION) {
            float blendRatio = dataMappingParams.getBlendRatio();
            cMap = dataMappingParams.getColorMap1();
            colData = data.getComponent(cMap.getDataComponentName());
            wrapCMap = cMap.isWrap();
            colorMapLUT = cMap.getRGBByteColorTable();
            nColors = ColorMapManager.SAMPLING_TABLE - 1;
            low = cMap.getDataMin();
            d = nColors / (cMap.getDataMax() - low);
            cIndex = 0;
            vl = colData.getVectorLength();
            for (int i = 0; i < nInd; i++) {
                float[] lc = colData.getFloatElement(indices[i]);
                if (vl == 1)
                    v = lc[0];
                else {
                    v = 0;
                    for (int j = 0; j < vl; j++)
                        v += lc[j] * lc[j];
                    v = sqrt(v);
                }
                if (!wrapCMap)
                    if (!wrapCMap)
                        cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                    else
                        cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                for (int j = 0, l = 4 * i; j < 3; j++, l++)
                    colors[l] = (byte) (0xff & (int) ((1 - blendRatio) * (0xff & colors[l]) +
                        blendRatio * (0xff & colorMapLUT[cIndex + j])));
            }
        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;
        return colors;
    }

    public static byte[] mapColorsIndexed(DataContainer data, DataMappingParams dataMappingParams,
                                          int[] indices, Color3f col, byte[] colors)
    {
        int nInd = indices.length;
        if (colors == null || colors.length != 4 * nInd)
            colors = new byte[4 * nInd];
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                colors = mapColorsIndexedColormapped(data, dataMappingParams, indices, col, colors);
                break;
            case DataMappingParams.RGB:
                colors = mapColorsIndexedRGB(data, dataMappingParams, indices, col, colors);
                break;
        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;
        if (data instanceof Field && ((Field) data).hasMask()) {
            LogicLargeArray valid = ((Field) data).getCurrentMask();
            float[] c = new float[3];
            col.get(c);
            byte[] bc = new byte[4];
            for (int i = 0; i < c.length; i++)
                bc[i] = (byte) (0xff & (int) (255 * c[i]));
            for (int i = 0; i < nInd; i++)
                if (valid.getByte(indices[i]) == 0) {
                    colors[4 * i] = bc[0];
                    colors[4 * i + 1] = bc[1];
                    colors[4 * i + 2] = bc[2];
                    colors[4 * i + 3] = (byte) 127;
                }
        }
        return colors;
    }

    private static byte[] mapColorsIndexedColormapped(DataContainer data, DataMappingParams dataMappingParams,
                                                      int[] indices, Color3f col, byte[] colors)
    {
        int nInd = indices.length;
        ComponentColorMap cMap = dataMappingParams.getColorMap0();
        ComponentColorMap modMap = null;
        ColorComponentParams modParams = null;
        if (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION)
            modParams = dataMappingParams.getSatParams();
        else if (dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION)
            modParams = dataMappingParams.getValParams();
        boolean wrapCMap = cMap.isWrap();
        DataArray colData = data.getComponent(cMap.getDataComponentName());
        if (colData == null || !colData.isNumeric()) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue()),
                (byte) 100
            };
            for (int i = 0; i < colors.length;)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            return colors;
        }
        if (colData.getUserData(0).equalsIgnoreCase("colors") &&
            colData.getType() == DataArrayType.FIELD_DATA_BYTE &&
            colData.getVectorLength() >= 3) {
            byte[] cData = (byte[]) colData.getRawArray().getData();
            int vlen = colData.getVectorLength();
            for (int i = 0; i < nInd; i++)
                for (int j = 0, k = 4 * i, l = vlen * indices[i]; j < vlen; j++, k++, l++)
                    colors[k] = cData[l];
            if (vlen == 3)
                for (int i = 3; i < colors.length; i += 4)
                    colors[i] = (byte) 255;
            return colors;
        }

        byte[] colorMapLUT = cMap.getRGBByteColorTable();
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = cMap.getDataMin();
        float d = nColors / (cMap.getDataMax() - low);
        int cIndex;
        double v;
        int vl = colData.getVectorLength();
        switch (colData.getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = ((ByteDataArray) colData).getRawArray().getData();
                for (int i = 0; i < nInd; i++) {
                    if (vl == 1)
                        v = 0xff & bData[indices[i]];
                    else {
                        v = 0;
                        for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                            v += (0xff & bData[k]) * (0xff & bData[k]);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                    else
                        cIndex = 3 * (int) (d * (v - low));
                    for (int j = 0, l = 4 * i; j < 3; j++, l++)
                        colors[l] = colorMapLUT[cIndex + j];
                }
                break;
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_LONG:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
            case FIELD_DATA_STRING:
                LargeArray dData = colData.getRawArray();
                for (int i = 0; i < nInd; i++) {
                    if (vl == 1)
                        v = dData.getDouble(indices[i]);
                    else {
                        v = 0;
                        for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                            v += dData.getDouble(k) * dData.getDouble(k);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                    else
                        cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    for (int j = 0, l = 4 * i; j < 3; j++, l++)
                        colors[l] = colorMapLUT[cIndex + j];
                    for (int j = 0, l = 4 * i; j < 3; j++, l++)
                        colors[l] = colorMapLUT[cIndex + j];
                }
                break;
            case FIELD_DATA_COMPLEX:
                float[] reData = (float[]) ((ComplexDataArray) colData).getFloatRealArray().getData();
                float[] imData = (float[]) ((ComplexDataArray) colData).getFloatImaginaryArray().getData();
                float v1,
                 v2;
                for (int i = 0; i < nInd; i++) {
                    if (vl == 1) {
                        v1 = reData[indices[i]];
                        v2 = imData[indices[i]];
                        v = sqrt(v1 * v1 + v2 * v2);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * indices[i]; j < vl; j++, k++) {
                            v1 = reData[k];
                            v2 = imData[k];
                            v += sqrt(v1 * v2 + v1 * v2);
                        }
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                    else
                        cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    for (int j = 0, l = 4 * i; j < 3; j++, l++)
                        colors[l] = colorMapLUT[cIndex + j];
                    for (int j = 0, l = 4 * i; j < 3; j++, l++)
                        colors[l] = colorMapLUT[cIndex + j];
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                for (int i = 0; i < nInd; i++) {
                    if (vl == 1)
                        v = ((DataObjectInterface) oData.get(indices[i])).toFloat();
                    else {
                        v = 0;
                        for (int j = 0, k = vl * indices[i]; j < vl; j++, k++) {
                            double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                            v += tmp * tmp;
                        }
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                    else
                        cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    for (int j = 0, l = 4 * i; j < 3; j++, l++)
                        colors[l] = colorMapLUT[cIndex + j];
                    for (int j = 0, l = 4 * i; j < 3; j++, l++)
                        colors[l] = colorMapLUT[cIndex + j];
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported array type");

        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;

        if ((dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION ||
            dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION) && modParams != null) {
            boolean modSat = (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION);
            colData = data.getComponent(modParams.getDataComponentName());
            if (colData == null)
                return colors;
            vl = colData.getVectorLength();
            if (vl == 1)
                low = modParams.getDataMin();
            else
                low = 0;
            float mlow = modParams.getColorComponentMin();
            float mup = modSat ? 1 : modParams.getColorComponentMax();
            d = (mup - mlow) / (modParams.getDataMax() - low);
            int r, g, b;
            double u;
            switch (colData.getType()) {
                case FIELD_DATA_BYTE:
                    byte[] bData = ((ByteDataArray) colData).getRawArray().getData();
                    for (int i = 0, l = 0; i < nInd; i++, l += 4) {
                        if (vl == 1)
                            u = mlow + d * ((0xff & bData[indices[i]]) - low);
                        else {
                            u = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                u += (0xff & bData[k]) * (0xff & bData[k]);
                            u = mlow + d * sqrt(u);
                        }
                        u = max(mlow, min(mup, u));
                        r = 0xff & colors[l];
                        g = 0xff & colors[l + 1];
                        b = 0xff & colors[l + 2];
                        for (int j = 0; j < 3; j++)
                            if (modSat) {
                                int m = r;
                                if (g > m)
                                    m = g;
                                if (b > m)
                                    m = b;
                                colors[l] = (byte) (0xff & (int) (m - u * (m - r)));
                                colors[l + 1] = (byte) (0xff & (int) (m - u * (m - g)));
                                colors[l + 2] = (byte) (0xff & (int) (m - u * (m - b)));
                            } else if (u <= 1) {
                                colors[l] = (byte) (0xff & (int) (u * r));
                                colors[l + 1] = (byte) (0xff & (int) (u * g));
                                colors[l + 2] = (byte) (0xff & (int) (u * b));
                            } else {
                                double w = 2 - u;
                                colors[l] = (byte) (0xff & (int) (255 - u * (255 - r)));
                                colors[l + 1] = (byte) (0xff & (int) (255 - u * (255 - g)));
                                colors[l + 2] = (byte) (0xff & (int) (255 - u * (255 - b)));
                            }
                    }
                    break;
                case FIELD_DATA_LOGIC:
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_LONG:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                case FIELD_DATA_STRING:
                    LargeArray dData = colData.getRawArray();
                    for (int i = 0, l = 0; i < nInd; i++, l += 4) {
                        if (vl == 1)
                            u = mlow + d * (dData.getDouble(indices[i]) - low);
                        else {
                            u = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                u += dData.getDouble(k) * dData.getDouble(k);
                            u = mlow + d * sqrt(u);
                        }
                        u = max(mlow, min(mup, u));
                        r = 0xff & colors[l];
                        g = 0xff & colors[l + 1];
                        b = 0xff & colors[l + 2];
                        for (int j = 0; j < 3; j++)
                            if (modSat) {
                                int m = r;
                                if (g > m)
                                    m = g;
                                if (b > m)
                                    m = b;
                                colors[l] = (byte) (0xff & (int) (m - u * (m - r)));
                                colors[l + 1] = (byte) (0xff & (int) (m - u * (m - g)));
                                colors[l + 2] = (byte) (0xff & (int) (m - u * (m - b)));
                            } else if (u <= 1) {
                                colors[l] = (byte) (0xff & (int) (u * r));
                                colors[l + 1] = (byte) (0xff & (int) (u * g));
                                colors[l + 2] = (byte) (0xff & (int) (u * b));
                            } else {
                                double w = 2 - u;
                                colors[l] = (byte) (0xff & (int) (255 - u * (255 - r)));
                                colors[l + 1] = (byte) (0xff & (int) (255 - u * (255 - g)));
                                colors[l + 2] = (byte) (0xff & (int) (255 - u * (255 - b)));
                            }
                    }
                    break;
                case FIELD_DATA_COMPLEX:
                    float[] reData = (float[]) ((ComplexDataArray) colData).getFloatRealArray().getData();
                    float[] imData = (float[]) ((ComplexDataArray) colData).getFloatImaginaryArray().getData();
                    float v1,
                     v2;
                    for (int i = 0, l = 0; i < nInd; i++, l += 4) {
                        if (vl == 1) {
                            v1 = reData[indices[i]];
                            v2 = imData[indices[i]];
                            v = sqrt(v1 * v1 + v2 * v2);
                            u = mlow + d * (v - low);
                        } else {
                            u = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++) {
                                v1 = reData[k];
                                v2 = imData[k];
                                u += sqrt(v1 * v1 + v2 * v2);
                            }
                            u = mlow + d * sqrt(u);
                        }
                        u = max(mlow, min(mup, u));
                        r = 0xff & colors[l];
                        g = 0xff & colors[l + 1];
                        b = 0xff & colors[l + 2];
                        for (int j = 0; j < 3; j++)
                            if (modSat) {
                                int m = r;
                                if (g > m)
                                    m = g;
                                if (b > m)
                                    m = b;
                                colors[l] = (byte) (0xff & (int) (m - u * (m - r)));
                                colors[l + 1] = (byte) (0xff & (int) (m - u * (m - g)));
                                colors[l + 2] = (byte) (0xff & (int) (m - u * (m - b)));
                            } else if (u <= 1) {
                                colors[l] = (byte) (0xff & (int) (u * r));
                                colors[l + 1] = (byte) (0xff & (int) (u * g));
                                colors[l + 2] = (byte) (0xff & (int) (u * b));
                            } else {
                                double w = 2 - u;
                                colors[l] = (byte) (0xff & (int) (255 - u * (255 - r)));
                                colors[l + 1] = (byte) (0xff & (int) (255 - u * (255 - g)));
                                colors[l + 2] = (byte) (0xff & (int) (255 - u * (255 - b)));
                            }
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                    for (int i = 0, l = 0; i < nInd; i++, l += 4) {
                        if (vl == 1)
                            u = mlow + d * (((DataObjectInterface) oData.get(indices[i])).toFloat() - low);
                        else {
                            u = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++) {
                                double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                                u += tmp + tmp;
                            }
                            u = mlow + d * sqrt(u);
                        }
                        u = max(mlow, min(mup, u));
                        r = 0xff & colors[l];
                        g = 0xff & colors[l + 1];
                        b = 0xff & colors[l + 2];
                        for (int j = 0; j < 3; j++)
                            if (modSat) {
                                int m = r;
                                if (g > m)
                                    m = g;
                                if (b > m)
                                    m = b;
                                colors[l] = (byte) (0xff & (int) (m - u * (m - r)));
                                colors[l + 1] = (byte) (0xff & (int) (m - u * (m - g)));
                                colors[l + 2] = (byte) (0xff & (int) (m - u * (m - b)));
                            } else if (u <= 1) {
                                colors[l] = (byte) (0xff & (int) (u * r));
                                colors[l + 1] = (byte) (0xff & (int) (u * g));
                                colors[l + 2] = (byte) (0xff & (int) (u * b));
                            } else {
                                double w = 2 - u;
                                colors[l] = (byte) (0xff & (int) (255 - u * (255 - r)));
                                colors[l + 1] = (byte) (0xff & (int) (255 - u * (255 - g)));
                                colors[l + 2] = (byte) (0xff & (int) (255 - u * (255 - b)));
                            }
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported array type");

            }
        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;
        if (dataMappingParams.getColorMapModification() == DataMappingParams.BLEND_MAP_MODIFICATION) {
            float blendRatio = dataMappingParams.getBlendRatio();
            cMap = dataMappingParams.getColorMap1();
            colData = data.getComponent(cMap.getDataComponentName());
            if (colData == null) colData = data.getComponent(0);
            wrapCMap = cMap.isWrap();
            colorMapLUT = cMap.getRGBByteColorTable();
            nColors = ColorMapManager.SAMPLING_TABLE - 1;
            low = cMap.getDataMin();
            d = nColors / (cMap.getDataMax() - low);
            cIndex = 0;
            vl = colData.getVectorLength();
            switch (colData.getType()) {
                case FIELD_DATA_BYTE:
                    byte[] bData = ((ByteDataArray) colData).getRawArray().getData();
                    for (int i = 0; i < nInd; i++) {
                        if (vl == 1)
                            v = 0xff & bData[indices[i]];
                        else {
                            v = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                v += (0xff & bData[k]) * (0xff & bData[k]);
                            v = sqrt(v);
                        }
                        if (!wrapCMap)
                            cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                        else
                            cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                        for (int j = 0, l = 4 * i; j < 3; j++, l++)
                            colors[l] = (byte) (0xff & (int) ((1 - blendRatio) * (0xff & colors[l]) +
                                blendRatio * (0xff & colorMapLUT[cIndex + j])));
                    }
                    break;
                case FIELD_DATA_LOGIC:
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_LONG:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                case FIELD_DATA_STRING:
                    LargeArray dData = colData.getRawArray();
                    for (int i = 0; i < nInd; i++) {
                        if (vl == 1)
                            v = dData.getDouble(indices[i]);
                        else {
                            v = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                v += dData.getDouble(k) * dData.getDouble(k);
                            v = sqrt(v);
                        }
                        if (!wrapCMap)
                            cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                        else
                            cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                        for (int j = 0, l = 4 * i; j < 3; j++, l++)
                            colors[l] = (byte) (0xff & (int) ((1 - blendRatio) * (0xff & colors[l]) +
                                blendRatio * (0xff & colorMapLUT[cIndex + j])));
                    }
                    break;
                case FIELD_DATA_COMPLEX:
                    float[] reData = (float[]) ((ComplexDataArray) colData).getFloatRealArray().getData();
                    float[] imData = (float[]) ((ComplexDataArray) colData).getFloatImaginaryArray().getData();
                    float v1,
                     v2;
                    for (int i = 0; i < nInd; i++) {
                        if (vl == 1) {
                            v1 = reData[indices[i]];
                            v2 = imData[indices[i]];
                            v = sqrt(v1 * v1 + v2 * v2);
                        } else {
                            v = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                v += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                            v = sqrt(v);
                        }
                        if (!wrapCMap)
                            cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                        else
                            cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                        for (int j = 0, l = 4 * i; j < 3; j++, l++)
                            colors[l] = (byte) (0xff & (int) ((1 - blendRatio) * (0xff & colors[l]) +
                                blendRatio * (0xff & colorMapLUT[cIndex + j])));
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                    for (int i = 0; i < nInd; i++) {
                        if (vl == 1)
                            v = ((DataObjectInterface) oData.get(indices[i])).toFloat();
                        else {
                            v = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++) {
                                double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                                v += tmp * tmp;
                            }
                            v = sqrt(v);
                        }
                        if (!wrapCMap)
                            cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                        else
                            cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                        for (int j = 0, l = 4 * i; j < 3; j++, l++)
                            colors[l] = (byte) (0xff & (int) ((1 - blendRatio) * (0xff & colors[l]) +
                                blendRatio * (0xff & colorMapLUT[cIndex + j])));
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported array type");

            }
        }
        return colors;
    }

    private static byte[] mapColorsIndexedRGB(DataContainer data, DataMappingParams dataMappingParams,
                                              int[] indices, Color3f col, byte[] colors)
    {
        int nInd = indices.length;
        float minData = 0, maxData = 0, minC = 0, scale = 1;
        ColorComponentParams[] rgbParams = new ColorComponentParams[]{
            dataMappingParams.getRedParams(),
            dataMappingParams.getGreenParams(),
            dataMappingParams.getBlueParams()
        };
        DataArray[] components = new DataArray[3];
        DataArray colData;
        double v;
        int vl;
        for (int i = 0; i < rgbParams.length; i++)
            components[i] = data.getComponent(rgbParams[i].getDataComponentName());
        if ((components[0] == null || !components[0].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric())) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue())
            };
            for (int i = 0; i < colors.length;)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            for (int i = 3; i < colors.length; i += 4)
                colors[i] = (byte) 255;
            return colors;
        }
        for (int ncomp = 0; ncomp < 3; ncomp++) {
            colData = components[ncomp];
            if (colData == null || !colData.isNumeric()) {
                byte c = (byte) (0xff & (int) (255 * rgbParams[ncomp].getColorComponentMin()));
                for (int i = ncomp; i < colors.length; i += 3)
                    colors[i] = c;
                continue;
            }

            minC = 255 * rgbParams[ncomp].getColorComponentMin();
            minData = rgbParams[ncomp].getDataMin();
            maxData = rgbParams[ncomp].getDataMax();
            scale = (255 - minC) / (maxData - minData);
            vl = colData.getVectorLength();
            switch (colData.getType()) {
                case FIELD_DATA_BYTE:
                    byte[] bData = ((ByteDataArray) colData).getRawArray().getData();
                    for (int i = 0, l = ncomp; i < nInd; i++, l += 4) {
                        if (vl == 1)
                            v = 0xff & bData[indices[i]];
                        else {
                            v = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                v += (0xff & bData[k]) * (0xff & bData[k]);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;

                case FIELD_DATA_LOGIC:
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_LONG:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                case FIELD_DATA_STRING:
                    LargeArray dData = colData.getRawArray();
                    for (int i = 0, l = ncomp; i < nInd; i++, l += 4) {
                        if (vl == 1)
                            v = dData.getDouble(indices[i]);
                        else {
                            v = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                v += dData.getDouble(k) * dData.getDouble(k);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                case FIELD_DATA_COMPLEX:
                    float[] reData = (float[]) ((ComplexDataArray) colData).getFloatRealArray().getData();
                    float[] imData = (float[]) ((ComplexDataArray) colData).getFloatImaginaryArray().getData();
                    float v1,
                     v2;
                    for (int i = 0, l = ncomp; i < nInd; i++, l += 4) {
                        if (vl == 1) {
                            v1 = reData[indices[i]];
                            v2 = imData[indices[i]];
                            v = sqrt(v1 * v1 + v2 * v2);
                        } else {
                            v = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                v += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                    for (int i = 0, l = ncomp; i < nInd; i++, l += 4) {
                        if (vl == 1)
                            v = ((DataObjectInterface) oData.get(indices[i])).toFloat();
                        else {
                            v = 0;
                            for (int j = 0, k = vl * indices[i]; j < vl; j++, k++) {
                                double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                                v += tmp * tmp;
                            }
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported array type");

            }
        }
        return colors;
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, int start, int end, int tStart, Color3f col, byte[] colors)
    {
        if (start < 0 || start >= end || end > data.getNElements())
            return null;
        if (colors == null || colors.length < 4 * (tStart + end - start))
            colors = new byte[4 * (tStart + end - start)];
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                colors = mapColormapped(data, dataMappingParams, start, end, tStart, col, colors);
                break;
            case DataMappingParams.RGB:
                colors = mapRGB(data, dataMappingParams, start, end, tStart, col, colors);
                break;
        }
        if (data instanceof Field && ((Field) data).hasMask()) {
            LogicLargeArray valid = ((Field) data).getCurrentMask();
            float[] c = new float[3];
            col.get(c);
            byte[] bc = new byte[4];
            for (int i = 0; i < c.length; i++)
                bc[i] = (byte) (0xff & (int) (255 * c[i]));
            for (int i = start, l = tStart; i < end; i++, l++)
                if (valid.getByte(i) == 0) {
                    colors[4 * l] = bc[0];
                    colors[4 * l + 1] = bc[1];
                    colors[4 * l + 2] = bc[2];
                    colors[4 * l + 3] = (byte) 127;
                }
        }
        return colors;
    }

    private static byte[] mapColormapped(DataContainer data, DataMappingParams dataMappingParams, int start, int end, int tStart, Color3f col, byte[] colors)
    {
        ComponentColorMap cMap = dataMappingParams.getColorMap0();
        ComponentColorMap modMap = dataMappingParams.getColorMap0();
        ColorComponentParams modParams = null;
        switch (dataMappingParams.getColorMapModification()) {
            case DataMappingParams.SAT_MAP_MODIFICATION:
                modParams = dataMappingParams.getSatParams();
                break;
            case DataMappingParams.VAL_MAP_MODIFICATION:
                modParams = dataMappingParams.getValParams();
                break;
            case DataMappingParams.BLEND_MAP_MODIFICATION:
                modMap = dataMappingParams.getColorMap1();
                break;
            default:
        }
        boolean wrapCMap = cMap.isWrap();
        DataArray colData = null;
        if (cMap.getDataComponentName() != null)
            colData = data.getComponent(cMap.getDataComponentName());
        if (colData == null) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue()),
                (byte) 255
            };
            for (int i = 4 * tStart; i < 4 * (tStart + end - start);)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            return colors;
        }
        if (colData.getUserData(0).equalsIgnoreCase("colors") &&
            colData.getType() == DataArrayType.FIELD_DATA_BYTE &&
            colData.getVectorLength() == 3) {
            byte[] cData = (byte[]) colData.getRawArray().getData();
            for (int i = start, k = 4 * tStart, l = 3 * start; i < end; i++) {
                for (int j = 0; j < 3; j++, k++, l++)
                    colors[k] = cData[l];
                colors[k] = (byte) 255;
                k += 1;
            }
            return colors;
        }
        if (colData.getUserData(0).equalsIgnoreCase("colors") &&
            colData.getType() == DataArrayType.FIELD_DATA_BYTE &&
            colData.getVectorLength() == 4) {
            byte[] cData = (byte[]) colData.getRawArray().getData();
            System.arraycopy(cData, 4 * start, colors, 4 * tStart, 4 * (end - start));
            return colors;
        }
        byte[] colorMapLUT = cMap.getRGBByteColorTable();
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = cMap.getDataMin();
        float d = nColors / (cMap.getDataMax() - low);
        int cIndex = 0;
        double v;
        int vl = colData.getVectorLength();
        switch (colData.getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = ((ByteDataArray) colData).getRawArray().getData();
                if (bData.length < end * vl)
                    break;
                for (int i = start, it = tStart; i < end; i++, it++) {
                    if (vl == 1)
                        v = 0xff & bData[i];
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += (0xff & bData[k]) * (0xff & bData[k]);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                    else
                        cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    for (int j = 0, l = 4 * it; j < 3; j++, l++)
                        colors[l] = colorMapLUT[cIndex + j];
                }
                break;
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_LONG:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
            case FIELD_DATA_STRING:
                LargeArray dData = colData.getRawArray();
                if (dData.length() < end * vl)
                    break;
                for (int i = start, it = tStart; i < end; i++, it++) {
                    if (vl == 1)
                        v = dData.getDouble(i);
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += dData.getDouble(k) * dData.getDouble(k);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                    else
                        cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    for (int j = 0, l = 4 * it; j < 3; j++, l++)
                        colors[l] = colorMapLUT[cIndex + j];
                }
                break;
            case FIELD_DATA_COMPLEX:
                float[] reData = (float[]) ((ComplexDataArray) colData).getFloatRealArray().getData();
                float[] imData = (float[]) ((ComplexDataArray) colData).getFloatImaginaryArray().getData();
                float v1,
                 v2;
                if (reData.length < end * vl)
                    break;
                for (int i = start, it = tStart; i < end; i++, it++) {
                    if (vl == 1) {
                        v1 = reData[i];
                        v2 = imData[i];
                        v = sqrt(v1 * v1 + v2 * v2);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                    else
                        cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    for (int j = 0, l = 4 * it; j < 3; j++, l++)
                        colors[l] = colorMapLUT[cIndex + j];
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                if (oData.length() < end * vl)
                    break;
                for (int i = start, it = tStart; i < end; i++, it++) {
                    if (vl == 1)
                        v = ((DataObjectInterface) oData.get(i)).toFloat();
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                            v += tmp * tmp;
                        }
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
                    else
                        cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    for (int j = 0, l = 4 * it; j < 3; j++, l++)
                        colors[l] = colorMapLUT[cIndex + j];
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported array type");

        }
        for (int i = 4 * tStart + 3; i < 4 * (tStart + end - start); i += 4)
            colors[i] = (byte) 255;

        if (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION ||
            dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION) {
            boolean modSat = (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION);
            DataArray modData = null;
            if (modParams != null && modParams.getDataComponentName() != null)
                modData = data.getComponent(modParams.getDataComponentName());
            if (modData == null)
                return colors;
            vl = modData.getVectorLength();
            if (vl == 1)
                low = modParams.getDataMin();
            else
                low = 0;
            float mlow = modParams.getColorComponentMin();
            d = (1 - mlow) / (modParams.getDataMax() - low);
            int r, g, b;
            double u;
            switch (modData.getType()) {
                case FIELD_DATA_BYTE:
                    byte[] bData = ((ByteDataArray) modData).getRawArray().getData();
                    for (int i = start, l = 4 * tStart; i < end; i++, l += 4) {
                        if (vl == 1)
                            u = mlow + d * ((0xff & bData[i]) - low);
                        else {
                            u = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                u += (0xff & bData[k]) * (0xff & bData[k]);
                            u = mlow + d * sqrt(u);
                        }
                        u = max(mlow, min(1, u));
                        r = 0xff & colors[l];
                        g = 0xff & colors[l + 1];
                        b = 0xff & colors[l + 2];
                        if (modSat) {
                            int m = r;
                            if (g > m)
                                m = g;
                            if (b > m)
                                m = b;
                            colors[l] = (byte) (0xff & (int) (m - u * (m - r)));
                            colors[l + 1] = (byte) (0xff & (int) (m - u * (m - g)));
                            colors[l + 2] = (byte) (0xff & (int) (m - u * (m - b)));
                        } else {
                            colors[l] = (byte) (0xff & (int) (u * r));
                            colors[l + 1] = (byte) (0xff & (int) (u * g));
                            colors[l + 2] = (byte) (0xff & (int) (u * b));
                        }
                    }
                    break;

                case FIELD_DATA_LOGIC:
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_LONG:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                case FIELD_DATA_STRING:
                    LargeArray dData = modData.getRawArray();
                    for (int i = start, l = 4 * tStart; i < end; i++, l += 4) {
                        if (vl == 1)
                            u = mlow + d * (dData.getDouble(i) - low);
                        else {
                            u = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                u += dData.getDouble(k) * dData.getDouble(k);
                            u = mlow + d * sqrt(u);
                        }
                        u = max(mlow, min(1, u));
                        r = 0xff & colors[l];
                        g = 0xff & colors[l + 1];
                        b = 0xff & colors[l + 2];
                        if (modSat) {
                            int m = r;
                            if (g > m)
                                m = g;
                            if (b > m)
                                m = b;
                            colors[l] = (byte) (0xff & (int) (m - u * (m - r)));
                            colors[l + 1] = (byte) (0xff & (int) (m - u * (m - g)));
                            colors[l + 2] = (byte) (0xff & (int) (m - u * (m - b)));
                        } else {
                            colors[l] = (byte) (0xff & (int) (u * r));
                            colors[l + 1] = (byte) (0xff & (int) (u * g));
                            colors[l + 2] = (byte) (0xff & (int) (u * b));
                        }
                    }
                    break;
                case FIELD_DATA_COMPLEX:
                    float[] reData = (float[]) ((ComplexDataArray) colData).getFloatRealArray().getData();
                    float[] imData = (float[]) ((ComplexDataArray) colData).getFloatImaginaryArray().getData();
                    float v1,
                     v2;
                    for (int i = start, l = 4 * tStart; i < end; i++, l += 4) {
                        if (vl == 1) {
                            v1 = reData[i];
                            v2 = imData[i];
                            v = sqrt(v1 * v1 + v2 * v2);
                            u = mlow + d * (v - low);
                        } else {
                            u = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                u += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                            u = mlow + d * sqrt(u);
                        }
                        u = max(mlow, min(1, u));
                        r = 0xff & colors[l];
                        g = 0xff & colors[l + 1];
                        b = 0xff & colors[l + 2];
                        if (modSat) {
                            int m = r;
                            if (g > m)
                                m = g;
                            if (b > m)
                                m = b;
                            colors[l] = (byte) (0xff & (int) (m - u * (m - r)));
                            colors[l + 1] = (byte) (0xff & (int) (m - u * (m - g)));
                            colors[l + 2] = (byte) (0xff & (int) (m - u * (m - b)));
                        } else {
                            colors[l] = (byte) (0xff & (int) (u * r));
                            colors[l + 1] = (byte) (0xff & (int) (u * g));
                            colors[l + 2] = (byte) (0xff & (int) (u * b));
                        }
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) modData.getRawArray();
                    for (int i = start, l = 4 * tStart; i < end; i++, l += 4) {
                        if (vl == 1)
                            u = mlow + d * (((DataObjectInterface) oData.get(i)).toFloat() - low);
                        else {
                            u = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++) {
                                double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                                u += tmp * tmp;
                            }
                            u = mlow + d * sqrt(u);
                        }
                        u = max(mlow, min(1, u));
                        r = 0xff & colors[l];
                        g = 0xff & colors[l + 1];
                        b = 0xff & colors[l + 2];
                        if (modSat) {
                            int m = r;
                            if (g > m)
                                m = g;
                            if (b > m)
                                m = b;
                            colors[l] = (byte) (0xff & (int) (m - u * (m - r)));
                            colors[l + 1] = (byte) (0xff & (int) (m - u * (m - g)));
                            colors[l + 2] = (byte) (0xff & (int) (m - u * (m - b)));
                        } else {
                            colors[l] = (byte) (0xff & (int) (u * r));
                            colors[l + 1] = (byte) (0xff & (int) (u * g));
                            colors[l + 2] = (byte) (0xff & (int) (u * b));
                        }
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported array type");

            }
        }
        if (dataMappingParams.getColorMapModification() == DataMappingParams.BLEND_MAP_MODIFICATION) {
            float blendRatio = dataMappingParams.getBlendRatio();
            cMap = dataMappingParams.getColorMap1();
            colData = data.getComponent(cMap.getDataComponentName());
            if (colData != null) {
                wrapCMap = cMap.isWrap();
                colorMapLUT = cMap.getRGBByteColorTable();
                nColors = ColorMapManager.SAMPLING_TABLE - 1;
                low = cMap.getDataMin();
                d = nColors / (cMap.getDataMax() - low);
                cIndex = 0;
                vl = colData.getVectorLength();
                switch (colData.getType()) {
                    case FIELD_DATA_BYTE:
                        byte[] bData = ((ByteDataArray) colData).getRawArray().getData();
                        for (int i = start, l = 4 * tStart; i < end; i++, l++) //l is incrementad here to skip alpha entry
                        {
                            if (vl == 1)
                                v = 0xff & bData[i];
                            else {
                                v = 0;
                                for (int j = 0, k = vl * i; j < vl; j++, k++)
                                    v += (0xff & bData[k]) * (0xff & bData[k]);
                                v = sqrt(v);
                            }
                            if (!wrapCMap) {
                                cIndex = 3 * (int) (d * (v - low));
                                if (cIndex < 0)
                                    cIndex = 0;
                                if (cIndex > 3 * nColors)
                                    cIndex = 3 * nColors;
                            } else
                                cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                            for (int j = 0; j < 3; j++, l++)
                                colors[l] = (byte) (0xff & (int) ((1 - blendRatio) * (0xff & colors[l]) +
                                    blendRatio * (0xff & colorMapLUT[cIndex + j])));
                        }
                        break;

                    case FIELD_DATA_LOGIC:
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_LONG:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                    case FIELD_DATA_STRING:
                        LargeArray dData = colData.getRawArray();
                        for (int i = start, l = 4 * tStart; i < end; i++, l++) {
                            if (vl == 1)
                                v = dData.getDouble(i);
                            else {
                                v = 0;
                                for (int j = 0, k = vl * i; j < vl; j++, k++)
                                    v += dData.getDouble(k) * dData.getDouble(k);
                                v = sqrt(v);
                            }
                            if (!wrapCMap) {
                                cIndex = 3 * (int) (d * (v - low));
                                if (cIndex < 0)
                                    cIndex = 0;
                                if (cIndex > 3 * nColors)
                                    cIndex = 3 * nColors;
                            } else
                                cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                            for (int j = 0; j < 3; j++, l++)
                                colors[l] = (byte) (0xff & (int) ((1 - blendRatio) * (0xff & colors[l]) +
                                    blendRatio * (0xff & colorMapLUT[cIndex + j])));
                        }
                        break;
                    case FIELD_DATA_COMPLEX:
                        float[] reData = (float[]) ((ComplexDataArray) colData).getFloatRealArray().getData();
                        float[] imData = (float[]) ((ComplexDataArray) colData).getFloatImaginaryArray().getData();
                        float v1,
                         v2;
                        for (int i = start, l = 4 * tStart; i < end; i++, l++) {
                            if (vl == 1) {
                                v1 = reData[i];
                                v2 = imData[i];
                                v = sqrt(v1 * v1 + v2 * v2);
                            } else {
                                v = 0;
                                for (int j = 0, k = vl * i; j < vl; j++, k++)
                                    v += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                                v = sqrt(v);
                            }
                            if (!wrapCMap) {
                                cIndex = 3 * (int) (d * (v - low));
                                if (cIndex < 0)
                                    cIndex = 0;
                                if (cIndex > 3 * nColors)
                                    cIndex = 3 * nColors;
                            } else
                                cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                            for (int j = 0; j < 3; j++, l++)
                                colors[l] = (byte) (0xff & (int) ((1 - blendRatio) * (0xff & colors[l]) +
                                    blendRatio * (0xff & colorMapLUT[cIndex + j])));
                        }
                        break;
                    case FIELD_DATA_OBJECT:
                        ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                        for (int i = start, l = 4 * tStart; i < end; i++, l++) {
                            if (vl == 1)
                                v = ((DataObjectInterface) oData.get(i)).toFloat();
                            else {
                                v = 0;
                                for (int j = 0, k = vl * i; j < vl; j++, k++) {
                                    double tmp = ((DataObjectInterface) oData.get(k)).toFloat();;
                                    v += tmp * tmp;
                                }
                                v = sqrt(v);
                            }
                            if (!wrapCMap) {
                                cIndex = 3 * (int) (d * (v - low));
                                if (cIndex < 0)
                                    cIndex = 0;
                                if (cIndex > 3 * nColors)
                                    cIndex = 3 * nColors;
                            } else
                                cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                            for (int j = 0; j < 3; j++, l++)
                                colors[l] = (byte) (0xff & (int) ((1 - blendRatio) * (0xff & colors[l]) +
                                    blendRatio * (0xff & colorMapLUT[cIndex + j])));
                        }
                        break;
                    default:
                        throw new IllegalArgumentException("Unsupported array type");

                }
            }
        }
        return colors;
    }

    private static byte[] mapRGB(DataContainer data, DataMappingParams dataMappingParams, int start, int end, int tStart, Color3f col, byte[] colors)
    {
        float minData = 0, maxData = 0, minC = 0, scale = 1;
        ColorComponentParams[] rgbParams = new ColorComponentParams[]{
            dataMappingParams.getRedParams(),
            dataMappingParams.getGreenParams(),
            dataMappingParams.getBlueParams()
        };
        DataArray[] components = new DataArray[3];
        DataArray colData;
        int vl;
        double v;
        for (int i = 0; i < rgbParams.length; i++)
            components[i] = data.getComponent(rgbParams[i].getDataComponentName());
        if ((components[0] == null || !components[0].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric())) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue())
            };
            for (int i = 0; i < colors.length;)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            return colors;
        }
        for (int ncomp = 0; ncomp < 3; ncomp++) {
            colData = components[ncomp];
            if (colData == null || !colData.isNumeric()) {
                byte c = (byte) (0xff & (int) (255 * rgbParams[ncomp].getColorComponentMin()));
                for (int i = ncomp; i < colors.length; i += 4)
                    colors[i] = c;
                continue;
            }

            minC = 200 * rgbParams[ncomp].getColorComponentMin();
            minData = rgbParams[ncomp].getDataMin();
            maxData = rgbParams[ncomp].getDataMax();
            scale = (200 - minC) / (maxData - minData);
            vl = colData.getVectorLength();
            switch (colData.getType()) {
                case FIELD_DATA_BYTE:
                    byte[] bData = ((ByteDataArray) colData).getRawArray().getData();
                    for (int i = start, l = 4 * tStart + ncomp; i < end; i++, l += 4) {
                        if (vl == 1)
                            v = 0xff & bData[i];
                        else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                v += (0xff & bData[k]) * (0xff & bData[k]);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;

                case FIELD_DATA_LOGIC:
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_LONG:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                case FIELD_DATA_STRING:
                    LargeArray dData = colData.getRawArray();
                    for (int i = start, l = 4 * tStart + ncomp; i < end; i++, l += 4) {
                        if (vl == 1)
                            v = dData.getDouble(i);
                        else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                v += dData.getDouble(k) * dData.getDouble(k);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                case FIELD_DATA_COMPLEX:
                    float[] reData = (float[]) ((ComplexDataArray) colData).getFloatRealArray().getData();
                    float[] imData = (float[]) ((ComplexDataArray) colData).getFloatImaginaryArray().getData();
                    float v1,
                     v2;
                    for (int i = start, l = 4 * tStart + ncomp; i < end; i++, l += 4) {
                        if (vl == 1) {
                            v1 = reData[i];
                            v2 = imData[i];
                            v = sqrt(v1 * v1 + v2 * v2);
                        } else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                v += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                    for (int i = start, l = 4 * tStart + ncomp; i < end; i++, l += 4) {
                        if (vl == 1)
                            v = ((DataObjectInterface) oData.get(i)).toFloat();
                        else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++) {
                                double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                                v += tmp * tmp;
                            }
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported array type");

            }
        }
        return colors;
    }

    public static int[] map(DataContainer data, DataMappingParams dataMappingParams,
                            Color3f col, int[] colors,
                            boolean transpose, int n0, int n1)
    {
        int n = (int) data.getNElements();
        if (colors == null || colors.length < n)
            colors = new int[n];
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                colors = mapColormapped(data, dataMappingParams, col, colors);
                break;
            case DataMappingParams.RGB:
                colors = mapRGB(data, dataMappingParams, col, colors);
                break;
        }
        if (data instanceof Field && ((Field) data).hasMask()) {
            LogicLargeArray valid = ((Field) data).getCurrentMask();
            float[] c = new float[3];
            col.get(c);
            byte[] bc = new byte[4];
            for (int i = 0; i < c.length; i++)
                bc[i] = (byte) (0xff & (int) (255 * c[i]));
            for (int i = 0, l = 0; i < n; i++, l++)
                if (valid.getByte(i) == 0)
                    colors[l] = bc[0] << 16 | bc[1] << 8 | bc[2] | 0x7f << 24;
        }
        if (transpose && n0 * n1 == n) {
            int[] tcolors = new int[n];
            for (int i = 0; i < n1; i++)
                for (int j = 0; j < n0; j++)
                    tcolors[j * n1 + i] = colors[i * n0 + j];
            System.arraycopy(tcolors, 0, colors, 0, n);
        }
        return colors;
    }

    private static int intModifySaturation(int original, double saturation)
    {
        int r = 0xff & (original >> 16);
        int g = 0xff & (original >> 8);
        int b = 0xff & original;
        int m = r;
        if (g > m)
            m = g;
        if (b > m)
            m = b;
        int res = (0xff << 24) |
            ((0xff & (int) (m - saturation * (m - r))) << 16) |
            ((0xff & (int) (m - saturation * (m - g))) << 8) |
            (0xff & (int) (m - saturation * (m - b)));
        return res;
    }

    private static int intModifyValue(int original, double value)
    {
        int r = 0xff & (original >> 16);
        int g = 0xff & (original >> 8);
        int b = 0xff & original;
        int m = r;
        if (g > m)
            m = g;
        if (b > m)
            m = b;
        int res = (0xff << 24) |
            ((0xff & (int) (value * r)) << 16) |
            ((0xff & (int) (value * g)) << 8) |
            (0xff & (int) (value * b));
        return res;
    }

    private static void intModifyValueSaturation(DataContainer data, DataMappingParams dataMappingParams,
                                                 Color3f col, int[] colors)
    {
        boolean modVal = (dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION);
        int n = (int) data.getNElements();
        float low = 0;
        ColorComponentParams modMapParams = dataMappingParams.getSatParams();
        DataArray modData = data.getComponent(modMapParams.getDataComponentName());
        if (modData == null)
            return;
        int vl = modData.getVectorLength();
        if (vl == 1)
            low = modMapParams.getDataMin();
        float mlow = modMapParams.getColorComponentMin();
        float d = (1 - mlow) / (modMapParams.getDataMax() - low);
        int r, g, b;
        double u;
        switch (modData.getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = ((ByteDataArray) modData).getRawArray().getData();
                for (int i = 0, l = 0; i < n; i++, l++) {
                    if (vl == 1)
                        u = mlow + d * ((0xff & bData[i]) - low);
                    else {
                        u = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            u += (0xff & bData[k]) * (0xff & bData[k]);
                        u = mlow + d * sqrt(u);
                    }
                    u = max(mlow, min(1, u));
                    if (modVal)
                        colors[l] = intModifyValue(colors[l], u);
                    else
                        colors[l] = intModifySaturation(colors[l], u);
                }
                break;
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_LONG:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
            case FIELD_DATA_STRING:
                LargeArray dData = modData.getRawArray();
                for (int i = 0, l = 0; i < n; i++, l++) {
                    if (vl == 1)
                        u = mlow + d * (dData.getDouble(i) - low);
                    else {
                        u = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            u += dData.getDouble(k) * dData.getDouble(k);
                        u = mlow + d * sqrt(u);
                    }
                    u = max(mlow, min(1, u));
                    if (modVal)
                        colors[l] = intModifyValue(colors[l], u);
                    else
                        colors[l] = intModifySaturation(colors[l], u);
                }
                break;
            case FIELD_DATA_COMPLEX:
                float[] reData = (float[]) ((ComplexDataArray) modData).getFloatRealArray().getData();
                float[] imData = (float[]) ((ComplexDataArray) modData).getFloatImaginaryArray().getData();
                float v1,
                 v2;
                for (int i = 0, l = 0; i < n; i++, l++) {
                    if (vl == 1) {
                        v1 = reData[i];
                        v2 = imData[i];
                        u = mlow + d * (sqrt(v1 * v1 + v2 * v2) - low);
                    } else {
                        u = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            u += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                        u = mlow + d * sqrt(u);
                    }
                    u = max(mlow, min(1, u));
                    r = 0xff & (colors[l] >> 16);
                    g = 0xff & (colors[l] >> 9);
                    b = 0xff & colors[l];
                    colors[l] &= 0xff << 24;
                    if (modVal)
                        colors[l] = intModifyValue(colors[l], u);
                    else
                        colors[l] = intModifySaturation(colors[l], u);
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) modData.getRawArray();
                for (int i = 0, l = 0; i < n; i++, l++) {
                    if (vl == 1)
                        u = mlow + d * (((DataObjectInterface) oData.get(i)).toFloat() - low);
                    else {
                        u = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                            u += tmp * tmp;
                        }
                        u = mlow + d * sqrt(u);
                    }
                    u = max(mlow, min(1, u));
                    if (modVal)
                        colors[l] = intModifyValue(colors[l], u);
                    else
                        colors[l] = intModifySaturation(colors[l], u);
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported array type");

        }
    }

    private static int intBlend(int c0, int c1, float value)
    {
        int r0 = 0xff & (c0 >> 16);
        int g0 = 0xff & (c0 >> 8);
        int b0 = 0xff & c0;
        int r1 = 0xff & (c1 >> 16);
        int g1 = 0xff & (c1 >> 8);
        int b1 = 0xff & c1;
        int res = (0xff << 24) |
            ((0xff & (int) ((1 - value) * r0 + value * r1)) << 16) |
            ((0xff & (int) ((1 - value) * g0 + value * g1)) << 8) |
            (0xff & (int) ((1 - value) * b0 + value * b1));
        return res;
    }

    private static void intBlend(DataContainer data, DataMappingParams dataMappingParams,
                                 Color3f col, int[] colors)
    {
        int n = (int) data.getNElements();
        ComponentColorMap modMap = dataMappingParams.getColorMap1();
        boolean wrapCMap = modMap.isWrap();
        DataArray colData = data.getComponent(modMap.getDataComponentName());
        float blendRatio = dataMappingParams.getBlendRatio();
        if (colData == null)
            return;
        int[] colorMapILUT = modMap.getARGBColorTable();
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = modMap.getDataMin();
        float d = nColors / (modMap.getDataMax() - low);
        int cIndex;
        double v;
        int vl = colData.getVectorLength();
        switch (colData.getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = ((ByteDataArray) colData).getRawArray().getData();
                if (bData.length < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1)
                        v = 0xff & bData[i];
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += (0xff & bData[k]) * (0xff & bData[k]);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = intBlend(colors[it], colorMapILUT[cIndex], blendRatio);
                }
                break;
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_LONG:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
            case FIELD_DATA_STRING:
                LargeArray dData = colData.getRawArray();
                if (dData.length() < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1)
                        v = dData.getDouble(i);
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += dData.getDouble(k) * dData.getDouble(k);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = intBlend(colors[it], colorMapILUT[cIndex], blendRatio);
                }
                break;
            case FIELD_DATA_COMPLEX:
                float[] reData = (float[]) ((ComplexDataArray) colData).getFloatRealArray().getData();
                float[] imData = (float[]) ((ComplexDataArray) colData).getFloatImaginaryArray().getData();
                float v1,
                 v2;
                if (reData.length < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1) {
                        v1 = reData[i];
                        v2 = imData[i];
                        v = sqrt(v1 * v1 + v2 * v2);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = intBlend(colors[it], colorMapILUT[cIndex], blendRatio);
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                if (oData.length() < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1)
                        v = ((DataObjectInterface) oData.get(i)).toFloat();
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                            v += tmp * tmp;
                        }
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = intBlend(colors[it], colorMapILUT[cIndex], blendRatio);
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported array type");

        }
    }

    private static int[] mapColormapped(DataContainer data, DataMappingParams dataMappingParams,
                                        Color3f col, int[] colors)
    {
        int n = (int) data.getNElements();
        ComponentColorMap cMap = dataMappingParams.getColorMap0();
        ColorComponentParams modMapParams = null;
        boolean wrapCMap = cMap.isWrap();
        DataArray colData = data.getComponent(cMap.getDataComponentName());
        if (colData == null) {
            for (int i = 0; i < n; i++)
                colors[i] = 0xff << 24 |
                    ((0xff & col.get().getRed()) << 16) |
                    ((0xff & col.get().getGreen()) << 8) |
                    (0xff & col.get().getBlue());
            return colors;
        }
        if (colData.getUserData(0).equalsIgnoreCase("colors") &&
            colData.getType() == DataArrayType.FIELD_DATA_BYTE) {
            byte[] cData = (byte[]) colData.getRawArray().getData();
            if (colData.getVectorLength() == 3) {
                for (int i = 0; i < n; i++)
                    colors[i] = 0xff << 24 |
                        ((0xff & cData[3 * i + 2]) << 16) |
                        ((0xff & cData[3 * i + 1]) << 8) |
                        (0xff & cData[3 * i]);
                return colors;
            } else if (colData.getVectorLength() == 4) {
                for (int i = 0; i < n; i++)
                    colors[i] = ((0xff & cData[4 * i + 3]) << 24) |
                        ((0xff & cData[4 * i + 2]) << 16) |
                        ((0xff & cData[4 * i + 1]) << 8) |
                        (0xff & cData[4 * i]);
                return colors;
            }
        }
        int[] colorMapILUT = cMap.getARGBColorTable();
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = cMap.getDataMin();
        float d = nColors / (cMap.getDataMax() - low);
        int cIndex;
        double v;
        int vl = colData.getVectorLength();
        switch (colData.getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = ((ByteDataArray) colData).getRawArray().getData();
                if (bData.length < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1)
                        v = 0xff & bData[i];
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += (0xff & bData[k]) * (0xff & bData[k]);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = colorMapILUT[cIndex];
                }
                break;
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_LONG:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
            case FIELD_DATA_STRING:
                LargeArray dData = colData.getRawArray();
                if (dData.length() < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1)
                        v = dData.getDouble(i);
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += dData.getDouble(k) * dData.getDouble(k);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = colorMapILUT[cIndex];
                }
                break;
            case FIELD_DATA_COMPLEX:
                float[] reData = (float[]) ((ComplexDataArray) colData).getFloatRealArray().getData();
                float[] imData = (float[]) ((ComplexDataArray) colData).getFloatImaginaryArray().getData();
                float v1,
                 v2;
                if (reData.length < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1) {
                        v1 = reData[i];
                        v2 = imData[i];
                        v = sqrt(v1 * v1 + v2 * v2);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = colorMapILUT[cIndex];
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                if (oData.length() < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1)
                        v = ((DataObjectInterface) oData.get(i)).toFloat();
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                            v += tmp * tmp;
                        }
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = colorMapILUT[cIndex];
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported array type");

        }
        switch (dataMappingParams.getColorMapModification()) {
            case DataMappingParams.SAT_MAP_MODIFICATION:
                modMapParams = dataMappingParams.getSatParams();
                if (modMapParams != null && modMapParams.getDataComponentName() != null)
                    intModifyValueSaturation(data, dataMappingParams, col, colors);
                break;
            case DataMappingParams.VAL_MAP_MODIFICATION:
                modMapParams = dataMappingParams.getValParams();
                if (modMapParams != null && modMapParams.getDataComponentName() != null)
                    intModifyValueSaturation(data, dataMappingParams, col, colors);
                break;
            case DataMappingParams.BLEND_MAP_MODIFICATION:
                cMap = dataMappingParams.getColorMap1();
                if (cMap != null && cMap.getDataComponentName() != null)
                    intBlend(data, dataMappingParams, col, colors);
                break;
            default:
                break;
        }

        return colors;
    }

    private static int[] mapRGB(DataContainer data, DataMappingParams dataMappingParams,
                                Color3f col, int[] colors)
    {
        int n = (int) data.getNElements();
        float minData, maxData, minC, scale;
        ColorComponentParams[] rgbParams = new ColorComponentParams[]{
            dataMappingParams.getRedParams(),
            dataMappingParams.getGreenParams(),
            dataMappingParams.getBlueParams()
        };
        DataArray[] components = new DataArray[3];
        DataArray colData;
        double v;
        int vl;
        for (int i = 0; i < rgbParams.length; i++)
            components[i] = data.getComponent(rgbParams[i].getDataComponentName());

        if ((components[0] == null || !components[0].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric())) {
            int bc = (0xff << 24) |
                ((0xff & col.get().getRed()) << 16) |
                ((0xff & col.get().getGreen()) << 8) |
                ((0xff & col.get().getBlue()));
            for (int i = 0; i < colors.length; i++)
                colors[i] = bc;
            return colors;
        }

        for (int i = 0; i < colors.length; i++)
            colors[i] = 0xff << 24;
        for (int ncomp = 0; ncomp < 3; ncomp++) {
            int shift = 0;
            if (ncomp == 0)
                shift = 16;
            if (ncomp == 1)
                shift = 8;
            colData = components[ncomp];
            if (colData == null || !colData.isNumeric()) {
                int c = (int) (255 * rgbParams[ncomp].getColorComponentMin()) << shift;
                for (int i = ncomp; i < colors.length; i += 4)
                    colors[i] |= c;
                continue;
            }

            minC = 200 * rgbParams[ncomp].getColorComponentMin();
            minData = rgbParams[ncomp].getDataMin();
            maxData = rgbParams[ncomp].getDataMax();
            scale = (200 - minC) / (maxData - minData);
            vl = colData.getVectorLength();
            switch (colData.getType()) {
                case FIELD_DATA_BYTE:
                    byte[] bData = ((ByteDataArray) colData).getRawArray().getData();
                    for (int i = 0, l = 4 * 0 + ncomp; i < n; i++, l += 4) {
                        if (vl == 1)
                            v = 0xff & bData[i];
                        else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                v += (0xff & bData[k]) * (0xff & bData[k]);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] |= (0xff & (int) (minC + scale * (v - minData))) << shift;
                    }
                    break;
                case FIELD_DATA_LOGIC:
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_LONG:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                case FIELD_DATA_STRING:
                    LargeArray dData = colData.getRawArray();
                    for (int i = 0, l = 4 * 0 + ncomp; i < n; i++, l += 4) {
                        if (vl == 1)
                            v = dData.getDouble(i);
                        else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                v += dData.getDouble(k) * dData.getDouble(k);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] |= (0xff & (int) (minC + scale * (v - minData))) << shift;
                    }
                    break;
                case FIELD_DATA_COMPLEX:
                    float[] reData = (float[]) ((ComplexDataArray) colData).getFloatRealArray().getData();
                    float[] imData = (float[]) ((ComplexDataArray) colData).getFloatImaginaryArray().getData();
                    float v1,
                     v2;
                    for (int i = 0, l = 4 * 0 + ncomp; i < n; i++, l += 4) {
                        if (vl == 1) {
                            v1 = reData[i];
                            v2 = imData[i];
                            v = sqrt(v1 * v1 + v2 * v2);
                        } else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                v += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] |= (0xff & (int) (minC + scale * (v - minData))) << shift;
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                    for (int i = 0, l = 4 * 0 + ncomp; i < n; i++, l += 4) {
                        if (vl == 1)
                            v = ((DataObjectInterface) oData.get(i)).toFloat();
                        else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++) {
                                double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                                v += tmp * tmp;
                            }
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] |= (0xff & (int) (minC + scale * (v - minData))) << shift;
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported array type");

            }
        }
        return colors;
    }

    public static byte[] mapTransparencyIndexed(DataContainer data, TransparencyParams params,
                                                int[] indices, byte[] colors)
    {
        DataArray trData = data.getComponent(params.getComponentRange().getComponentName());
        if (trData == null) {
            for (int i = 3; i < colors.length; i += 4)
                colors[i] = (byte) (0xff);
            return colors;
        }
        int nInd = indices.length;
        if (colors == null || colors.length != 4 * nInd)
            colors = new byte[4 * nInd];
        int[] transparencyMap = params.getMap();
        byte[] tMap = new byte[transparencyMap.length];
        for (int i = 0; i < tMap.length; i++)
            tMap[i] = (byte) (0xff & transparencyMap[i]);
        float low = params.getComponentRange().getLow();
        float up = params.getComponentRange().getUp();
        float d = 255 / (up - low);
        int tIndex;
        int vl = trData.getVectorLength();
        double v;
        switch (trData.getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = ((ByteDataArray) trData).getRawArray().getData();
                for (int i = 0; i < nInd; i++) {
                    if (vl == 1)
                        v = 0xff & bData[indices[i]];
                    else {
                        v = 0;
                        for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                            v += (0xff & bData[k]) * (0xff & bData[k]);
                        v = sqrt(v);
                    }
                    tIndex = (int) (d * (v - low));
                    if (tIndex < 0)
                        tIndex = 0;
                    if (tIndex > 255)
                        tIndex = 255;
                    colors[4 * i + 3] = tMap[tIndex];
                }
                break;
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_LONG:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
            case FIELD_DATA_STRING:
                LargeArray dData = trData.getRawArray();
                for (int i = 0; i < nInd; i++) {
                    if (vl == 1)
                        v = dData.getDouble(indices[i]);
                    else {
                        v = 0;
                        for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                            v += dData.getDouble(k) * dData.getDouble(k);
                        v = sqrt(v);
                    }
                    tIndex = (int) (d * (v - low));
                    if (tIndex < 0)
                        tIndex = 0;
                    if (tIndex > 255)
                        tIndex = 255;
                    colors[4 * i + 3] = tMap[tIndex];
                }
                break;
            case FIELD_DATA_COMPLEX:
                float[] reData = (float[]) ((ComplexDataArray) trData).getFloatRealArray().getData();
                float[] imData = (float[]) ((ComplexDataArray) trData).getFloatImaginaryArray().getData();
                float v1,
                 v2;
                for (int i = 0; i < nInd; i++) {
                    if (vl == 1) {
                        v1 = reData[indices[i]];
                        v2 = imData[indices[i]];
                        v = sqrt(v1 * v1 + v2 * v2);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * indices[i]; j < vl; j++, k++)
                            v += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                        v = sqrt(v);
                    }
                    tIndex = (int) (d * (v - low));
                    if (tIndex < 0)
                        tIndex = 0;
                    if (tIndex > 255)
                        tIndex = 255;
                    colors[4 * i + 3] = tMap[tIndex];
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) trData.getRawArray();
                for (int i = 0; i < nInd; i++) {
                    if (vl == 1)
                        v = ((DataObjectInterface) oData.get(indices[i])).toFloat();
                    else {
                        v = 0;
                        for (int j = 0, k = vl * indices[i]; j < vl; j++, k++) {
                            double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                            v += tmp * tmp;
                        }
                        v = sqrt(v);
                    }
                    tIndex = (int) (d * (v - low));
                    if (tIndex < 0)
                        tIndex = 0;
                    if (tIndex > 255)
                        tIndex = 255;
                    colors[4 * i + 3] = tMap[tIndex];
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported array type");

        }
        return colors;
    }

    public static byte[] mapTransparency(DataContainer data, TransparencyParams params, int start, int end, int tStart, byte[] colors)
    {
//        VisNowCallTrace.trace();
        if (start < 0 || start >= end || end > data.getNElements() || colors == null || colors.length < 4 * end)
            return null;
        DataArray trData = data.getComponent(params.getComponentRange().getComponentName());
        if (trData == null) {
            for (int i = 4 * tStart + 3; i < 4 * (tStart + end - start); i += 4)
                colors[i] = (byte) (0xff);
            return colors;
        }
        int[] transparencyMap = params.getMap();
        byte[] tMap = new byte[transparencyMap.length];
        for (int i = 0; i < tMap.length; i++)
            tMap[i] = (byte) (0xff & transparencyMap[i]);
        float low = params.getComponentRange().getLow();
        float up = params.getComponentRange().getUp();
        float d = 255 / (up - low);
        int tIndex;
        double v;
        int vl = trData.getVectorLength();
        switch (trData.getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = ((ByteDataArray) trData).getRawArray().getData();
                if (bData.length < end * vl)
                    break;
                for (int i = start, it = tStart; i < end; i++, it++) {
                    if (vl == 1)
                        v = 0xff & bData[i];
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += (0xff & bData[k]) * (0xff & bData[k]);
                        v = sqrt(v);
                    }
                    tIndex = (int) (d * (v - low));
                    if (tIndex < 0)
                        tIndex = 0;
                    if (tIndex > 255)
                        tIndex = 255;
                    colors[4 * it + 3] = tMap[tIndex];
                }
                break;
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_LONG:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
            case FIELD_DATA_STRING:
                LargeArray dData = trData.getRawArray();
                if (dData.length() < end * vl)
                    break;
                for (int i = start, it = tStart; i < end; i++, it++) {
                    if (vl == 1)
                        v = dData.getDouble(i);
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += dData.getDouble(k) * dData.getDouble(k);
                        v = sqrt(v);
                    }
                    tIndex = (int) (d * (v - low));
                    if (tIndex < 0)
                        tIndex = 0;
                    if (tIndex > 255)
                        tIndex = 255;
                    colors[4 * it + 3] = tMap[tIndex];
                }
                break;
            case FIELD_DATA_COMPLEX:
                float[] reData = (float[]) ((ComplexDataArray) trData).getFloatRealArray().getData();
                float[] imData = (float[]) ((ComplexDataArray) trData).getFloatImaginaryArray().getData();
                float v1,
                 v2;
                if (reData.length < end * vl)
                    break;
                for (int i = start, it = tStart; i < end; i++, it++) {
                    if (vl == 1) {
                        v1 = reData[i];
                        v2 = imData[i];
                        v = sqrt(v1 * v1 + v2 * v2);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += sqrt(reData[k] * reData[k] + imData[k] * imData[k]);
                        v = sqrt(v);
                    }
                    tIndex = (int) (d * (v - low));
                    if (tIndex < 0)
                        tIndex = 0;
                    if (tIndex > 255)
                        tIndex = 255;
                    colors[4 * it + 3] = tMap[tIndex];
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) trData.getRawArray();
                if (oData.length() < end * vl)
                    break;
                for (int i = start, it = tStart; i < end; i++, it++) {
                    if (vl == 1)
                        v = ((DataObjectInterface) oData.get(i)).toFloat();
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                            v += tmp * tmp;
                        }
                        v = sqrt(v);
                    }
                    tIndex = (int) (d * (v - low));
                    if (tIndex < 0)
                        tIndex = 0;
                    if (tIndex > 255)
                        tIndex = 255;
                    colors[4 * it + 3] = tMap[tIndex];
                }
                break;
            default:
                throw new IllegalArgumentException("Unsupported array type");

        }
        return colors;
    }

    public static byte[] mapTimeValidityTransparency(int[] timeRange, float currentTime, int start, int end, int tStart, byte[] colors)
    {
        if (start < 0 || start >= end || end > timeRange.length / 2 || colors == null || colors.length < 4 * end)
            return null;
        for (int i = 2 * start, it = 4 * tStart + 3; i < 2 * end; i += 2, it += 4)
            if (currentTime < timeRange[i] || timeRange[i + 1] < currentTime)
                colors[it] = 0;
        return colors;
    }

    public static void setTransparencyMask(byte[] colors, LogicLargeArray mask, int start, int end, int tStart)
    {
        if (colors == null || colors.length < 4 * end)
            return;
        for (int i = start, j = 4 * tStart + 3; i < end; i++, j += 4)
            if (mask.getByte(i) == 0)
                colors[j] = 0;
    }

    public static void setTransparencyMask(byte[] colors, LogicLargeArray mask, int start, int end)
    {
        if (colors == null || colors.length < 4 * end)
            return;
        setTransparencyMask(colors, mask, start, end, 0);
    }

    public static void setTransparencyMask(byte[] colors, LogicLargeArray mask)
    {
        if (colors == null || mask == null || colors.length != 4 * mask.length())
            return;
        setTransparencyMask(colors, mask, 0, (int) mask.length());
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, int start, int end, Color3f col, byte[] colors)
    {
        return map(data, dataMappingParams, start, end, start, col, colors);
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, int start, int end, byte[] colors)
    {
        return map(data, dataMappingParams, start, end, new Color3f(1, 1, 1), colors);
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, int nData, Color3f col, byte[] colors)
    {
        return map(data, dataMappingParams, 0, (int) data.getNElements(), col, colors);
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, Color3f col, byte[] colors)
    {
        return map(data, dataMappingParams, (int) data.getNElements(), col, colors);
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, byte[] colors)
    {
        return map(data, dataMappingParams, (int) data.getNElements(), new Color3f(1, 1, 1), colors);
    }

    public static byte[] mapTransparency(DataContainer data, TransparencyParams params, int start, int end, byte[] colors)
    {
        if (colors == null || colors.length < 4 * end)
            return null;
        return mapTransparency(data, params, start, end, start, colors);
    }

    public static byte[] mapTransparency(DataContainer data, TransparencyParams params, int nData, byte[] colors)
    {
        return mapTransparency(data, params, 0, (int) data.getNElements(), colors);
    }

    public static byte[] mapTransparency(DataContainer data, TransparencyParams params, byte[] colors)
    {
        return mapTransparency(data, params, (int) data.getNElements(), colors);
    }

    private ColorMapper()
    {
    }
}
