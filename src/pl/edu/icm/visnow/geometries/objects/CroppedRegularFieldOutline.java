//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.geometries.objects;

import java.util.Arrays;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedLineStripArray;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.geometries.objects.generics.OpenAppearance;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.objects.generics.OpenColoringAttributes;
import pl.edu.icm.visnow.geometries.objects.generics.OpenLineAttributes;
import pl.edu.icm.visnow.geometries.objects.generics.OpenShape3D;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class CroppedRegularFieldOutline extends OpenBranchGroup
{
    protected static final int nSegments = 10;
    protected static final int nSegmentVerts = nSegments + 1;
    protected RegularField field;
    protected OpenBranchGroup geometry = new OpenBranchGroup();
    protected IndexedLineStripArray box = null;
    protected OpenLineAttributes boxLineAttr = new OpenLineAttributes();
    protected OpenColoringAttributes boxColorAttr = new OpenColoringAttributes();
    protected OpenAppearance boxApp = new OpenAppearance();
    protected OpenShape3D boxShape = new OpenShape3D();

    protected int nLineStrips = 0;
    protected int[] dims = null;
    protected int[] lastDims = null;
    protected int[] up = null;
    protected int[] low = null;
    protected float[][] af = null;
    protected float[] coords = null;
    protected int[] lineStripCounts = null;
    protected float[] boxVerts = null;
    protected int nDims = 3;

    static Logger logger = Logger.getLogger(CroppedRegularFieldOutline.class);

    public CroppedRegularFieldOutline()
    {
        super();
        geometry.addChild(boxShape);
        this.addChild(geometry);
    }

    /**
     * Set the value of field
     *
     * @param inField new value of field
     * @return true if field is a 3D regular field
     */
    public boolean setField(RegularField inField)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length < 2)
            return false;
        dims = inField.getDims();
        if (!Arrays.equals(lastDims, dims)) {
            nDims = dims.length;
            low = new int[nDims];
            up  = new int[nDims];
            for (int i = 0; i < nDims; i++) {
                low[i] = 0;
                up[i]  = dims[i];
            }
        }
        coords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
        af = inField.getAffine();
        this.field = inField;
        makeOutlineBox();
        return true;
    }

    /**
     * Set the crop range
     *
     * @param low new value of lower crop extents
     * @param up new value of upper crop extents
     * @return true low and up form valid crop range
     */    
    public boolean setCrop(int[] low, int[] up)
    {
        if (low == null || low.length < nDims || up == null || up.length < nDims)
            return false;
        for (int i = 0; i < nDims; i++) 
            if (low[i] < 0 || up[i] <= low[i] || up[i] > dims[i])
                return false;
        this.low = low;
        this.up  = up;
        updateBoxCoords();
        return true;
    }
    
    private int getInd(int i, int j, int k)
    {
        return ((i * dims[1] + j) * dims[0] + k) * 3;
    }
    private int getInd( int j, int k)
    {
        return (j * dims[0] + k) * 3;
    }

    private void updateBoxCoords()
    {
        switch (nDims)
        {
        case 2:
            if (coords == null) {
                for (int i = 0; i < 3; i++) {
                    boxVerts[i]      = af[3][i] + low[0]      * af[0][i] + low[1]      * af[1][i];
                    boxVerts[i + 3]  = af[3][i] + (up[0] - 1) * af[0][i] + low[1]      * af[1][i];
                    boxVerts[i + 6]  = af[3][i] + low[0]      * af[0][i] + (up[1] - 1) * af[1][i];
                    boxVerts[i + 9]  = af[3][i] + (up[0] - 1) * af[0][i] + (up[1] - 1) * af[1][i];
                }
            } else {
                int d;
                d = up[0] - low[0];
                // i0 axis edges
                for (int i = 0; i < nSegmentVerts; i++) {
                    int ii = Math.min(low[0] + (i * d) / nSegments, up[0] - 1);
                    System.arraycopy(coords, getInd(low[1]    , ii), boxVerts, 3 *                       i,  3);
                    System.arraycopy(coords, getInd( up[1] - 1, ii), boxVerts, 3 * (     nSegmentVerts + i), 3);
                }
                d = up[1] - low[1];
                for (int i = 0; i < nSegmentVerts; i++) {
                    int ii = Math.min(low[1] + (i * d) / nSegments, up[1] - 1);
                    System.arraycopy(coords, getInd(ii, low[0]), boxVerts, 3 * ( 2 * nSegmentVerts + i), 3);
                    System.arraycopy(coords, getInd(ii,  up[0] - 1), boxVerts, 3 * ( 3 * nSegmentVerts + i), 3);
                }
            }
            break;
        case 3:
            if (coords == null) {
                for (int i = 0; i < 3; i++) {
                    boxVerts[i]      = af[3][i] + low[0]      * af[0][i] + low[1]      * af[1][i] + low[2]      * af[2][i];
                    boxVerts[i + 3]  = af[3][i] + (up[0] - 1) * af[0][i] + low[1]      * af[1][i] + low[2]      * af[2][i];
                    boxVerts[i + 6]  = af[3][i] + low[0]      * af[0][i] + (up[1] - 1) * af[1][i] + low[2]      * af[2][i];
                    boxVerts[i + 9]  = af[3][i] + (up[0] - 1) * af[0][i] + (up[1] - 1) * af[1][i] + low[2]      * af[2][i];
                    boxVerts[i + 12] = af[3][i] + low[0]      * af[0][i] + low[1]      * af[1][i] + (up[2] - 1) * af[2][i];
                    boxVerts[i + 15] = af[3][i] + (up[0] - 1) * af[0][i] + low[1]      * af[1][i] + (up[2] - 1) * af[2][i];
                    boxVerts[i + 18] = af[3][i] + low[0]      * af[0][i] + (up[1] - 1) * af[1][i] + (up[2] - 1) * af[2][i];
                    boxVerts[i + 21] = af[3][i] + (up[0] - 1) * af[0][i] + (up[1] - 1) * af[1][i] + (up[2] - 1) * af[2][i];
                }
            } else {
                int d;
                d = up[0] - low[0];
                // i0 axis edges
                for (int i = 0; i < nSegmentVerts; i++) {
                    int ii = Math.min(low[0] + (i * d) / nSegments, up[0] - 1);
                    System.arraycopy(coords, getInd(low[2],     low[1],     ii), boxVerts, 3 *                       i,  3);
                    System.arraycopy(coords, getInd(low[2],      up[1] - 1, ii), boxVerts, 3 * (     nSegmentVerts + i), 3);
                    System.arraycopy(coords, getInd( up[2] - 1, low[1],     ii), boxVerts, 3 * ( 2 * nSegmentVerts + i), 3);
                    System.arraycopy(coords, getInd( up[2] - 1,  up[1] - 1, ii), boxVerts, 3 * ( 3 * nSegmentVerts + i), 3);
                }
                d = up[1] - low[1];
                for (int i = 0; i < nSegmentVerts; i++) {
                    int ii = Math.min(low[1] + (i * d) / nSegments, up[1] - 1);
                    System.arraycopy(coords, getInd(low[2],     ii, low[0]),     boxVerts, 3 * ( 4 * nSegmentVerts + i), 3);
                    System.arraycopy(coords, getInd(low[2],     ii,  up[0] - 1), boxVerts, 3 * ( 5 * nSegmentVerts + i), 3);
                    System.arraycopy(coords, getInd( up[2] - 1, ii, low[0]),     boxVerts, 3 * ( 6 * nSegmentVerts + i), 3);
                    System.arraycopy(coords, getInd( up[2] - 1, ii,  up[0] - 1), boxVerts, 3 * ( 7 * nSegmentVerts + i), 3);
                }
                d = up[2] - low[2];
                for (int i = 0; i < nSegmentVerts; i++) {
                    int ii = Math.min(low[2] + (i * d) / nSegments, up[2] - 1);
                    System.arraycopy(coords, getInd(ii, low[1],     low[0]),     boxVerts, 3 * ( 8 * nSegmentVerts + i), 3);
                    System.arraycopy(coords, getInd(ii, low[1],      up[0] - 1), boxVerts, 3 * ( 9 * nSegmentVerts + i), 3);
                    System.arraycopy(coords, getInd(ii,  up[1] - 1, low[0]),     boxVerts, 3 * (10 * nSegmentVerts + i), 3);
                    System.arraycopy(coords, getInd(ii,  up[1] - 1,  up[0] - 1), boxVerts, 3 * (11 * nSegmentVerts + i), 3);
                }
            }
            break;
        }
        box.setCoordinates(0, boxVerts);
    }

    private void makeOutlineBox()
    {
        boolean detach = postdetach();
        boxShape.removeAllGeometries();
        if (field == null) {
            if (detach)
                postattach();
            return;
        }
        int[] cInd = null;
        switch (nDims)
        {
        case 2:
            if (coords == null) {
                boxVerts = new float[12];
                box = new IndexedLineStripArray(4, GeometryArray.COORDINATES, 8, new int[]{2, 2, 2, 2});

                cInd = new int[]{0, 1, 2, 3, 0, 2, 1, 3};
            } else {
                boxVerts = new float[12 * nSegmentVerts];
                box = new IndexedLineStripArray(4 * nSegmentVerts,
                                                GeometryArray.COORDINATES,
                                                4 * nSegmentVerts,
                                                new int[]{
                                                    nSegmentVerts, nSegmentVerts, nSegmentVerts, nSegmentVerts
                                                });
                cInd = new int[4 * nSegmentVerts];
                for (int i = 0; i < cInd.length; i++)
                    cInd[i] = i;
            }
            break;
        case 3:
            if (coords == null) {
                boxVerts = new float[24];
                box = new IndexedLineStripArray(8, GeometryArray.COORDINATES, 24, new int[]{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2});

                cInd = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 0, 2, 1, 3, 4, 6, 5, 7, 0, 4, 1, 5, 2, 6, 3, 7};
            } else {
                boxVerts = new float[36 * nSegmentVerts];
                box = new IndexedLineStripArray(12 * nSegmentVerts,
                                                GeometryArray.COORDINATES,
                                                12 * nSegmentVerts,
                                                new int[]{
                                                    nSegmentVerts, nSegmentVerts, nSegmentVerts, nSegmentVerts, 
                                                    nSegmentVerts, nSegmentVerts, nSegmentVerts, nSegmentVerts, 
                                                    nSegmentVerts, nSegmentVerts, nSegmentVerts, nSegmentVerts 
                                                });
                cInd = new int[12 * nSegmentVerts];
                for (int i = 0; i < cInd.length; i++)
                    cInd[i] = i;
                break;
            }
        }
        box.setCoordinateIndices(0, cInd);
        updateBoxCoords();
        boxLineAttr.setLineAntialiasingEnable(true);
        boxLineAttr.setLineWidth(1.5f);
        boxApp.setLineAttributes(boxLineAttr);
        boxColorAttr.setColor(0, 1, 0);
        boxApp.setColoringAttributes(boxColorAttr);
        box.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        box.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        box.setCoordinates(0, boxVerts);
        boxShape.setAppearance(boxApp);
        updateBoxCoords();
        boxShape.addGeometry(box);
        if (detach)
            postattach();
    }
}
