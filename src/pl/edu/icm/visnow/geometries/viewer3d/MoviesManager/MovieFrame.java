package pl.edu.icm.visnow.geometries.viewer3d.MoviesManager;

import java.text.DecimalFormat;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedLineStripArray;
import javax.media.j3d.Transform3D;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import pl.edu.icm.visnow.geometries.geometryTemplates.Glyph;
import pl.edu.icm.visnow.geometries.geometryTemplates.Templates;
import pl.edu.icm.visnow.geometries.objects.GeometryObject;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.objects.generics.OpenShape3D;

/**
 * 
 * This class holds information about a single movie frame.
 * 
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 */


public class MovieFrame {

    private final Point3d cameraPosition;
    private final Transform3D cameraTransform;
    private final GeometryObject marker;
    private final String name;
    protected Glyph gt = new Templates.DiamondTemplate(0);
    protected int nvertl, nvertp, nindl, nindp;
    protected int[] stripsl, stripsp, pIndexl, pIndexp, cIndexl, cIndexp;
    protected double[] vertsl, vertsp;
    protected float scale = .1f;
    protected float[] resultColors = {.3f, .6f, 1.f};
    
    
    /**
     * creates movie frame
     * @param cameraTransform camera position and rotation in a current frame
     */
    public MovieFrame(Transform3D cameraTransform){
        initArrays();
        Vector3d p = new Vector3d();
        cameraTransform.get(p);
        DecimalFormat df = new DecimalFormat("0.00##");
        String resultX = df.format(p.x);
        String resultY = df.format(p.y);
        String resultZ = df.format(p.z);
        name = "("+resultX+", "+resultY+", "+resultZ+")";
        marker = new GeometryObject("frame marker - "+name);
        this.cameraPosition = new Point3d(p.x, p.y, p.z);
        this.cameraTransform = cameraTransform;
        createPickPointGeometry();
    }
    
    /**
     * Camera position.
     * @return camera position
     */

    public Point3d getCameraPosition() {
        return cameraPosition;
    }
    
    protected void createPickPointGeometry()
    {
        marker.clearAllGeometry();
        IndexedLineStripArray glyph = new IndexedLineStripArray(nvertp,
                                          GeometryArray.COORDINATES | GeometryArray.COLOR_3,
                                          nindp, stripsp);

            for (int i = 0; i < gt.getNverts(); i++) {
                vertsp[3 * i] = cameraPosition.x + scale * gt.getVerts()[3 * i];
                vertsp[3 * i + 1] = cameraPosition.y + scale * gt.getVerts()[3 * i + 1];
                vertsp[3 * i + 2] = cameraPosition.z + scale * gt.getVerts()[3 * i + 2];
            }
            
        glyph.setColors(0, resultColors);
        glyph.setCoordinates(0, vertsp);
        glyph.setCoordinateIndices(0, pIndexp);
        glyph.setColorIndices(0, cIndexp);
        OpenShape3D pickLine = new OpenShape3D();
        pickLine.addGeometry(glyph);
        OpenBranchGroup pLine = new OpenBranchGroup();
        pLine.addChild(pickLine);
        marker.addNode(pLine);
    }
        
    private void initArrays()
    {
        nvertl = 2 * gt.getNverts() + 2;
        int nstripl = 2 * gt.getNstrips() + 1;
        nindl = 2 * gt.getNinds() + 2;
        stripsl = new int[nstripl];
        for (int i = 0; i < gt.getNstrips(); i++) {
            stripsl[i] = stripsl[i + gt.getNstrips()] = gt.getStrips()[i];
        }
        stripsl[2 * gt.getNstrips()] = 2;
        pIndexl = new int[nindl];
        cIndexl = new int[nindl];
        for (int i = 0; i < gt.getNinds(); i++) {
            pIndexl[i] = gt.getPntsIndex()[i];
            pIndexl[i + gt.getNinds()] = gt.getPntsIndex()[i] + gt.getNverts();
        }
        pIndexl[2 * gt.getNinds()] = 2 * gt.getNverts();
        pIndexl[2 * gt.getNinds() + 1] = 2 * gt.getNverts() + 1;
        for (int i = 0; i < nindl; i++) {
            cIndexl[i] = 0;
        }
        vertsl = new double[3 * nvertl];

        nvertp = gt.getNverts();
        int nstripp = gt.getNstrips();
        nindp = gt.getNinds();
        stripsp = new int[nstripp];
        System.arraycopy(gt.getStrips(), 0, stripsp, 0, gt.getNstrips());
        pIndexp = new int[nindp];
        cIndexp = new int[nindp];
        System.arraycopy(gt.getPntsIndex(), 0, pIndexp, 0, gt.getNinds());
        for (int i = 0; i < nindp; i++) {
            cIndexp[i] = 0;
        }
        vertsp = new double[3 * nvertp];
    }
    
    /**
     * Geometry object to mark camera position in the current frame.
     * @return geometry object that represents camera position in the scene 
     */
    
    public GeometryObject getOutObject()
    {
        return marker;
    }
    
    /**
     * Overrides the toString method.
     * @return camera position in 3D coords.
     */
    @Override
    public String toString(){
        return name;
    }
}
