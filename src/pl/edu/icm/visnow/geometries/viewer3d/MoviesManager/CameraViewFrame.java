package pl.edu.icm.visnow.geometries.viewer3d.MoviesManager;

import java.awt.BorderLayout;
import javax.media.j3d.Canvas3D;

/**
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * 
 * 
 * 
 */


public class CameraViewFrame extends javax.swing.JFrame {

    private CameraWindowCloseListener cameraWindowCloseListener;
    /**
     * Creates new form CameraViewFrame
     */
    public CameraViewFrame() {
        initComponents();
    }
    
    /**
     * 
     * @param canvas show canvas3D in frame 
     */
    
    public void showCanvas(Canvas3D canvas){
        getContentPane().add(canvas, BorderLayout.CENTER);
    }
    
    /**
     * 
     * @param listener window close listener 
     */
    
    public void addCameraWindowCloseListener(CameraWindowCloseListener listener){
        cameraWindowCloseListener = listener;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("camera view");
        setPreferredSize(new java.awt.Dimension(640, 480));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        cameraWindowCloseListener.onCameraWindowClose();
    }//GEN-LAST:event_formWindowClosing


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
