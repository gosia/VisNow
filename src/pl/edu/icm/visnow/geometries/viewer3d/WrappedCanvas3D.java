package pl.edu.icm.visnow.geometries.viewer3d;

import java.awt.Container;
import java.awt.GraphicsConfiguration;
import java.awt.Window;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.lang.reflect.Method;
import javax.media.j3d.Canvas3D;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * This calss is an override to keep Java3D stero rendering working on newer JREs.
 * 
 * For Java3D 1.5.2:
 * - on Java versions 1.6 or older stereo rendering was working fine.
 * - in Java 1.7 due to some GraphicsConfiguration hierarchy changes the wrapper below is required. With the wrapper enabled stereo is working fine.
 * - in Java 1.8 so far no solution was found to properly run stereo.
 * 
 * For Java3D 1.6-pre:
 * 
 */
public class WrappedCanvas3D extends Canvas3D {
    
    

    private GraphicsConfiguration initialGC;

    public WrappedCanvas3D(GraphicsConfiguration graphicsConfiguration) {
        super(graphicsConfiguration);
        initialGC = this.getGraphicsConfiguration();
        if ((VisNow.getJava3DVersion() == 5 && VisNow.getJavaVersion() == 7) || (VisNow.getJava3DVersion() == 6 && VisNow.getJavaVersion() >= 7)) {
            this.addHierarchyListener(new HierarchyListener() {
                @Override
                public void hierarchyChanged(HierarchyEvent e) {
                    if (e.getChangeFlags() == HierarchyEvent.PARENT_CHANGED) {
                        Container parent = e.getChangedParent();
                        try {
                            Class<?> c = java.awt.Component.class;
                            Method method = c.getDeclaredMethod("setGraphicsConfiguration", GraphicsConfiguration.class);
                            if (method == null) {
                                return;
                            }
                            method.setAccessible(true);
                            if (parent == null) {
                                return;
                            }
                            Window parentWindow = SwingUtilities.getWindowAncestor(parent);
                            if (parentWindow != null && initialGC != null) {
                                method.invoke(parentWindow, initialGC);
                            }
                        } catch (NoSuchMethodException ex) {
                            //in case of Java 6 ignore this exception
                        } catch (Exception ex) {
                            throw new RuntimeException(ex.getMessage());
                        }
                        if (parent instanceof JComponent) {
                            ((JComponent) parent).revalidate();
                        }
                    }
                }
            });
        }
    }
    
    @Override
    public boolean getStereoAvailable() {    
        return (super.getStereoAvailable() && ((VisNow.getJava3DVersion() == 5 && VisNow.getJavaVersion() < 8) || VisNow.getJava3DVersion() == 6));        
    }
}
