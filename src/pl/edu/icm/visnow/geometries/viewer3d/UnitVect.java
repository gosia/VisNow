package pl.edu.icm.visnow.geometries.viewer3d;

/**
 *
 * @author Norbert
 */


public class UnitVect
{
    public float x;
    public float y;
    public float z;
    
    
    public UnitVect()
    {
        x=0; y=0; z=0;
    }
    
    public void setVals(float x, float y, float z)
    {
        double l = Math.sqrt(x*x+y*y+z*z);
        if(l == 0){
            this.x = 1; this.y = 0; this.z = 0;
        }
        else{
            this.x = (float)(x/l);
            this.y = (float)(y/l);
            this.z = (float)(z/l);
        }
    }
    
    
    @Override
    public String toString(){
        return "X: "+x+", Y:"+y+", Z: "+z;
    }
}
