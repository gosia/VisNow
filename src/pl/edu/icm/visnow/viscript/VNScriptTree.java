/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript;

import java.io.PrintStream;
import pl.edu.icm.visnow.viscript.types.VNScriptContext;
import pl.edu.icm.visnow.viscript.types.VNScriptExpr;
import pl.edu.icm.visnow.viscript.types.VNScriptType;
import pl.edu.icm.visnow.viscript.types.VNScriptRange;
import pl.edu.icm.visnow.viscript.types.VNScriptRangeType;
import pl.edu.icm.visnow.viscript.types.VNScriptRanges;

import pl.edu.icm.visnow.viscript.types.VNScriptArray;
import pl.edu.icm.visnow.viscript.types.VNScriptSubArray;
import pl.edu.icm.visnow.viscript.types.VNScriptScalar;
import pl.edu.icm.visnow.viscript.types.VNScriptVariable;
import pl.edu.icm.visnow.viscript.types.VNScriptUndefined;
import pl.edu.icm.visnow.viscript.types.VNScriptMethodCall;
import pl.edu.icm.visnow.viscript.types.VNScriptLiteral;
import pl.edu.icm.visnow.viscript.types.VNScriptAssignExpr;
import pl.edu.icm.visnow.viscript.types.VNScriptQ;
import pl.edu.icm.visnow.viscript.types.VNScriptDeclaration;

import pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets.VNScriptLog;
import pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets.LogMessage;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class VNScriptTree extends TreeParser
{

    public static final String[] tokenNames = new String[]{
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "AND_ASSIGN", "ASSIGN", "AT", "BIT_SHIFT_RIGHT", "BIT_SHIFT_RIGHT_ASSIGN", "COLON", "COMMA", "DEC", "DIV", "DIV_ASSIGN", "DOT", "DOTSTAR", "ELLIPSIS", "EQUAL", "GREATER_OR_EQUAL", "GREATER_THAN", "INC", "LBRACK", "LCURLY", "LESS_OR_EQUAL", "LESS_THAN", "LOGICAL_AND", "LOGICAL_NOT", "LOGICAL_OR", "LPAREN", "MINUS", "MINUS_ASSIGN", "MOD", "MOD_ASSIGN", "NOT", "NOT_EQUAL", "OR", "OR_ASSIGN", "PLUS", "PLUS_ASSIGN", "QUESTION", "RBRACK", "RCURLY", "RPAREN", "SEMI", "SHIFT_LEFT", "SHIFT_LEFT_ASSIGN", "SHIFT_RIGHT", "SHIFT_RIGHT_ASSIGN", "STAR", "STAR_ASSIGN", "XOR", "XOR_ASSIGN", "ABSTRACT", "ASSERT", "BOOLEAN", "BREAK", "BYTE", "CASE", "CATCH", "CHAR", "CLASS", "CONTINUE", "DEFAULT", "DO", "DOUBLE", "ELSE", "ENUM", "EXTENDS", "FALSE", "FINAL", "FINALLY", "FLOAT", "FOR", "IF", "IMPLEMENTS", "INSTANCEOF", "INTERFACE", "IMPORT", "INT", "LONG", "NATIVE", "NEW", "NULL", "PACKAGE", "PRIVATE", "PROTECTED", "PUBLIC", "RETURN", "SHORT", "STATIC", "STRICTFP", "SUPER", "SWITCH", "SYNCHRONIZED", "THIS", "THROW", "THROWS", "TRANSIENT", "TRUE", "TRY", "VOID", "VOLATILE", "WHILE", "ANNOTATION_INIT_ARRAY_ELEMENT", "ANNOTATION_INIT_BLOCK", "ANNOTATION_INIT_DEFAULT_KEY", "ANNOTATION_INIT_KEY_LIST", "ANNOTATION_LIST", "ANNOTATION_METHOD_DECL", "ANNOTATION_SCOPE", "ANNOTATION_TOP_LEVEL_SCOPE", "ARGUMENT_LIST", "ARRAY_DECLARATOR", "ARRAY_DECLARATOR_LIST", "ARRAY_ELEMENT_ACCESS", "ARRAY_INITIALIZER", "BLOCK_SCOPE", "CAST_EXPR", "CATCH_CLAUSE_LIST", "CLASS_CONSTRUCTOR_CALL", "CLASS_INSTANCE_INITIALIZER", "CLASS_STATIC_INITIALIZER", "CLASS_TOP_LEVEL_SCOPE", "CONSTRUCTOR_DECL", "ENUM_TOP_LEVEL_SCOPE", "EXPR", "EXTENDS_BOUND_LIST", "EXTENDS_CLAUSE", "FOR_CONDITION", "FOR_EACH", "FOR_INIT", "FOR_UPDATE", "FORMAL_PARAM_LIST", "FORMAL_PARAM_STD_DECL", "FORMAL_PARAM_VARARG_DECL", "FUNCTION_METHOD_DECL", "GENERIC_TYPE_ARG_LIST", "GENERIC_TYPE_PARAM_LIST", "INTERFACE_TOP_LEVEL_SCOPE", "IMPLEMENTS_CLAUSE", "LABELED_STATEMENT", "LOCAL_MODIFIER_LIST", "JAVA_SOURCE", "METHOD_CALL", "MODIFIER_LIST", "PARENTESIZED_EXPR", "POST_DEC", "POST_INC", "PRE_DEC", "PRE_INC", "QUALIFIED_TYPE_IDENT", "STATIC_ARRAY_CREATOR", "SUPER_CONSTRUCTOR_CALL", "SWITCH_BLOCK_LABEL_LIST", "THIS_CONSTRUCTOR_CALL", "THROWS_CLAUSE", "TYPE", "UNARY_MINUS", "UNARY_PLUS", "VAR_DECLARATION", "VAR_DECLARATOR", "VAR_DECLARATOR_LIST", "VOID_METHOD_DECL", "RANGE_LEFT", "RANGE_RIGHT", "RANGE_ALL", "RANGE_OFFSET", "RANGE_SINGLE", "DECLARATION", "IDENT", "HEX_LITERAL", "OCTAL_LITERAL", "DECIMAL_LITERAL", "FLOATING_POINT_LITERAL", "CHARACTER_LITERAL", "STRING_LITERAL", "HEX_DIGIT", "INTEGER_TYPE_SUFFIX", "EXPONENT", "FLOAT_TYPE_SUFFIX", "ESCAPE_SEQUENCE", "UNICODE_ESCAPE", "OCTAL_ESCAPE", "JAVA_ID_START", "JAVA_ID_PART", "WS", "COMMENT", "LINE_COMMENT", "':='"
    };
    public static final int PACKAGE = 84;
    public static final int STAR = 49;
    public static final int MOD = 32;
    public static final int DO = 64;
    public static final int GENERIC_TYPE_PARAM_LIST = 138;
    public static final int NOT = 34;
    public static final int ANNOTATION_METHOD_DECL = 109;
    public static final int EOF = -1;
    public static final int UNARY_PLUS = 159;
    public static final int BIT_SHIFT_RIGHT_ASSIGN = 9;
    public static final int TYPE = 157;
    public static final int RANGE_LEFT = 164;
    public static final int INC = 21;
    public static final int RPAREN = 43;
    public static final int FINAL = 70;
    public static final int IMPORT = 78;
    public static final int STRING_LITERAL = 176;
    public static final int CAST_EXPR = 118;
    public static final int NOT_EQUAL = 35;
    public static final int RETURN = 88;
    public static final int THIS = 95;
    public static final int ENUM_TOP_LEVEL_SCOPE = 125;
    public static final int ANNOTATION_INIT_KEY_LIST = 107;
    public static final int RBRACK = 41;
    public static final int PRE_DEC = 149;
    public static final int SWITCH_BLOCK_LABEL_LIST = 154;
    public static final int STATIC = 90;
    public static final int ELSE = 66;
    public static final int MINUS_ASSIGN = 31;
    public static final int STRICTFP = 91;
    public static final int NATIVE = 81;
    public static final int ELLIPSIS = 17;
    public static final int PRE_INC = 150;
    public static final int CHARACTER_LITERAL = 175;
    public static final int LCURLY = 23;
    public static final int UNARY_MINUS = 158;
    public static final int OCTAL_ESCAPE = 183;
    public static final int INT = 79;
    public static final int FORMAL_PARAM_VARARG_DECL = 135;
    public static final int INTERFACE_TOP_LEVEL_SCOPE = 139;
    public static final int WS = 186;
    public static final int LOCAL_MODIFIER_LIST = 142;
    public static final int LESS_THAN = 25;
    public static final int EXTENDS_BOUND_LIST = 127;
    public static final int RANGE_OFFSET = 167;
    public static final int DECIMAL_LITERAL = 173;
    public static final int FOR_INIT = 131;
    public static final int PROTECTED = 86;
    public static final int LBRACK = 22;
    public static final int THIS_CONSTRUCTOR_CALL = 155;
    public static final int FLOAT = 72;
    public static final int POST_DEC = 147;
    public static final int DECLARATION = 169;
    public static final int STATIC_ARRAY_CREATOR = 152;
    public static final int ANNOTATION_SCOPE = 110;
    public static final int LPAREN = 29;
    public static final int AT = 7;
    public static final int IMPLEMENTS = 75;
    public static final int XOR_ASSIGN = 52;
    public static final int LOGICAL_OR = 28;
    public static final int IDENT = 170;
    public static final int PLUS = 38;
    public static final int ANNOTATION_INIT_BLOCK = 105;
    public static final int GENERIC_TYPE_ARG_LIST = 137;
    public static final int JAVA_ID_PART = 185;
    public static final int GREATER_THAN = 20;
    public static final int CLASS_STATIC_INITIALIZER = 122;
    public static final int LESS_OR_EQUAL = 24;
    public static final int HEX_DIGIT = 177;
    public static final int SHORT = 89;
    public static final int INSTANCEOF = 76;
    public static final int MINUS = 30;
    public static final int RANGE_SINGLE = 168;
    public static final int SEMI = 44;
    public static final int STAR_ASSIGN = 50;
    public static final int VAR_DECLARATOR_LIST = 162;
    public static final int COLON = 10;
    public static final int OR_ASSIGN = 37;
    public static final int ENUM = 67;
    public static final int RCURLY = 42;
    public static final int PLUS_ASSIGN = 39;
    public static final int FUNCTION_METHOD_DECL = 136;
    public static final int INTERFACE = 77;
    public static final int POST_INC = 148;
    public static final int DIV = 13;
    public static final int CLASS_CONSTRUCTOR_CALL = 120;
    public static final int LONG = 80;
    public static final int PUBLIC = 87;
    public static final int ARRAY_INITIALIZER = 116;
    public static final int CATCH_CLAUSE_LIST = 119;
    public static final int SUPER_CONSTRUCTOR_CALL = 153;
    public static final int EXPONENT = 179;
    public static final int WHILE = 103;
    public static final int MOD_ASSIGN = 33;
    public static final int CASE = 58;
    public static final int NEW = 82;
    public static final int CHAR = 60;
    public static final int CLASS_INSTANCE_INITIALIZER = 121;
    public static final int ARRAY_ELEMENT_ACCESS = 115;
    public static final int FOR_CONDITION = 129;
    public static final int VAR_DECLARATION = 160;
    public static final int DIV_ASSIGN = 14;
    public static final int BREAK = 56;
    public static final int LOGICAL_AND = 26;
    public static final int FOR_UPDATE = 132;
    public static final int FLOATING_POINT_LITERAL = 174;
    public static final int VOID_METHOD_DECL = 163;
    public static final int DOUBLE = 65;
    public static final int VOID = 101;
    public static final int SUPER = 92;
    public static final int COMMENT = 187;
    public static final int FLOAT_TYPE_SUFFIX = 180;
    public static final int JAVA_ID_START = 184;
    public static final int IMPLEMENTS_CLAUSE = 140;
    public static final int LINE_COMMENT = 188;
    public static final int PRIVATE = 85;
    public static final int BLOCK_SCOPE = 117;
    public static final int SWITCH = 93;
    public static final int ANNOTATION_INIT_DEFAULT_KEY = 106;
    public static final int NULL = 83;
    public static final int VAR_DECLARATOR = 161;
    public static final int ANNOTATION_LIST = 108;
    public static final int THROWS = 97;
    public static final int ASSERT = 54;
    public static final int METHOD_CALL = 144;
    public static final int TRY = 100;
    public static final int SHIFT_LEFT = 45;
    public static final int SHIFT_RIGHT = 47;
    public static final int FORMAL_PARAM_STD_DECL = 134;
    public static final int RANGE_RIGHT = 165;
    public static final int OR = 36;
    public static final int SHIFT_RIGHT_ASSIGN = 48;
    public static final int JAVA_SOURCE = 143;
    public static final int CATCH = 59;
    public static final int FALSE = 69;
    public static final int RANGE_ALL = 166;
    public static final int INTEGER_TYPE_SUFFIX = 178;
    public static final int THROW = 96;
    public static final int DEC = 12;
    public static final int CLASS = 61;
    public static final int BIT_SHIFT_RIGHT = 8;
    public static final int THROWS_CLAUSE = 156;
    public static final int GREATER_OR_EQUAL = 19;
    public static final int FOR = 73;
    public static final int LOGICAL_NOT = 27;
    public static final int ABSTRACT = 53;
    public static final int AND = 4;
    public static final int AND_ASSIGN = 5;
    public static final int MODIFIER_LIST = 145;
    public static final int IF = 74;
    public static final int CONSTRUCTOR_DECL = 124;
    public static final int ESCAPE_SEQUENCE = 181;
    public static final int LABELED_STATEMENT = 141;
    public static final int UNICODE_ESCAPE = 182;
    public static final int BOOLEAN = 55;
    public static final int SYNCHRONIZED = 94;
    public static final int EXPR = 126;
    public static final int CLASS_TOP_LEVEL_SCOPE = 123;
    public static final int CONTINUE = 62;
    public static final int COMMA = 11;
    public static final int TRANSIENT = 98;
    public static final int EQUAL = 18;
    public static final int ARGUMENT_LIST = 112;
    public static final int QUALIFIED_TYPE_IDENT = 151;
    public static final int HEX_LITERAL = 171;
    public static final int DOT = 15;
    public static final int SHIFT_LEFT_ASSIGN = 46;
    public static final int FORMAL_PARAM_LIST = 133;
    public static final int DOTSTAR = 16;
    public static final int ANNOTATION_TOP_LEVEL_SCOPE = 111;
    public static final int BYTE = 57;
    public static final int XOR = 51;
    public static final int T__189 = 189;
    public static final int VOLATILE = 102;
    public static final int PARENTESIZED_EXPR = 146;
    public static final int ARRAY_DECLARATOR_LIST = 114;
    public static final int DEFAULT = 63;
    public static final int OCTAL_LITERAL = 172;
    public static final int TRUE = 99;
    public static final int EXTENDS_CLAUSE = 128;
    public static final int ARRAY_DECLARATOR = 113;
    public static final int QUESTION = 40;
    public static final int FINALLY = 71;
    public static final int ASSIGN = 6;
    public static final int ANNOTATION_INIT_ARRAY_ELEMENT = 104;
    public static final int FOR_EACH = 130;
    public static final int EXTENDS = 68;

    // delegates
    // delegators
    public VNScriptTree(TreeNodeStream input)
    {
        this(input, new RecognizerSharedState());
    }

    public VNScriptTree(TreeNodeStream input, RecognizerSharedState state)
    {
        super(input, state);
        this.state.ruleMemo = new HashMap[97 + 1];

    }

    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor)
    {
        this.adaptor = adaptor;
    }

    public TreeAdaptor getTreeAdaptor()
    {
        return adaptor;
    }

    public String[] getTokenNames()
    {
        return VNScriptTree.tokenNames;
    }

    public String getGrammarFileName()
    {
        return "/home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g";
    }

    private PrintStream ps;
    private VNScriptContext activeContext;
    private String activeVec;

    public void setPrintStream(PrintStream ps)
    {
        this.ps = ps;
    }

    public void setContext(VNScriptContext activeContext)
    {
        this.activeContext = activeContext;
    }

    public VNScriptContext getContext()
    {
        return this.activeContext;
    }

    private VNScriptRanges ranges;
    private ArrayList<VNScriptExpr> args;

    private VNScriptLog log;

    public VNScriptLog getLog()
    {
        return log;
    }

    public void setLog(VNScriptLog log)
    {
        this.log = log;
    }

    @Override
    public void reportError(RecognitionException e)
    {
        super.reportError(e);
        log.addMessage(new LogMessage(e.toString(), Integer.toString(e.line), LogMessage.LogMessageType.ERROR));
    }

    private void warning(String s)
    {
        log.addMessage(new LogMessage(s, LogMessage.LogMessageType.WARNING));
    }

    private void warning(String s, int line)
    {
        log.addMessage(new LogMessage(s, Integer.toString(line), LogMessage.LogMessageType.WARNING));
    }

    public static class program_return extends TreeRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "program"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:88:1: program : ^( BLOCK_SCOPE ( statement )* ) ;
    public final VNScriptTree.program_return program() throws RecognitionException
    {
        VNScriptTree.program_return retval = new VNScriptTree.program_return();
        retval.start = input.LT(1);
        int program_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree BLOCK_SCOPE1 = null;
        VNScriptTree.statement_return statement2 = null;

        CommonTree BLOCK_SCOPE1_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 1)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:89:5: ( ^( BLOCK_SCOPE ( statement )* ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:89:13: ^( BLOCK_SCOPE ( statement )* )
            {
                root_0 = (CommonTree) adaptor.nil();

                _last = (CommonTree) input.LT(1);
                {
                    CommonTree _save_last_1 = _last;
                    CommonTree _first_1 = null;
                    CommonTree root_1 = (CommonTree) adaptor.nil();
                    _last = (CommonTree) input.LT(1);
                    BLOCK_SCOPE1 = (CommonTree) match(input, BLOCK_SCOPE, FOLLOW_BLOCK_SCOPE_in_program97);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        BLOCK_SCOPE1_tree = (CommonTree) adaptor.dupNode(BLOCK_SCOPE1);

                        root_1 = (CommonTree) adaptor.becomeRoot(BLOCK_SCOPE1_tree, root_1);
                    }

                    if (input.LA(1) == Token.DOWN) {
                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:89:27: ( statement )*
                        loop1:
                        do {
                            int alt1 = 2;
                            int LA1_0 = input.LA(1);

                            if ((LA1_0 == SEMI || LA1_0 == DO || (LA1_0 >= FOR && LA1_0 <= IF) || LA1_0 == WHILE || LA1_0 == BLOCK_SCOPE || LA1_0 == EXPR || LA1_0 == DECLARATION)) {
                                alt1 = 1;
                            }

                            switch (alt1) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:0:0: statement
                                {
                                    _last = (CommonTree) input.LT(1);
                                    pushFollow(FOLLOW_statement_in_program99);
                                    statement2 = statement();

                                    state._fsp--;
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        adaptor.addChild(root_1, statement2.getTree());

                                    if (state.backtracking == 0) {
                                    }
                                }
                                break;

                                default:
                                    break loop1;
                            }
                        } while (true);

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                    }
                    adaptor.addChild(root_0, root_1);
                    _last = _save_last_1;
                }

                if (state.backtracking == 0) {
                }
            }

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 1, program_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "program"
    public static class block_return extends TreeRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "block"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:92:1: block : ^( BLOCK_SCOPE ( statement )* ) ;
    public final VNScriptTree.block_return block() throws RecognitionException
    {
        VNScriptTree.block_return retval = new VNScriptTree.block_return();
        retval.start = input.LT(1);
        int block_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree BLOCK_SCOPE3 = null;
        VNScriptTree.statement_return statement4 = null;

        CommonTree BLOCK_SCOPE3_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 2)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:93:5: ( ^( BLOCK_SCOPE ( statement )* ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:93:13: ^( BLOCK_SCOPE ( statement )* )
            {
                root_0 = (CommonTree) adaptor.nil();

                _last = (CommonTree) input.LT(1);
                {
                    CommonTree _save_last_1 = _last;
                    CommonTree _first_1 = null;
                    CommonTree root_1 = (CommonTree) adaptor.nil();
                    _last = (CommonTree) input.LT(1);
                    BLOCK_SCOPE3 = (CommonTree) match(input, BLOCK_SCOPE, FOLLOW_BLOCK_SCOPE_in_block130);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        BLOCK_SCOPE3_tree = (CommonTree) adaptor.dupNode(BLOCK_SCOPE3);

                        root_1 = (CommonTree) adaptor.becomeRoot(BLOCK_SCOPE3_tree, root_1);
                    }

                    if (state.backtracking == 0) {
                        ps.println("{");
                    }

                    if (input.LA(1) == Token.DOWN) {
                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:96:5: ( statement )*
                        loop2:
                        do {
                            int alt2 = 2;
                            int LA2_0 = input.LA(1);

                            if ((LA2_0 == SEMI || LA2_0 == DO || (LA2_0 >= FOR && LA2_0 <= IF) || LA2_0 == WHILE || LA2_0 == BLOCK_SCOPE || LA2_0 == EXPR || LA2_0 == DECLARATION)) {
                                alt2 = 1;
                            }

                            switch (alt2) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:0:0: statement
                                {
                                    _last = (CommonTree) input.LT(1);
                                    pushFollow(FOLLOW_statement_in_block147);
                                    statement4 = statement();

                                    state._fsp--;
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        adaptor.addChild(root_1, statement4.getTree());

                                    if (state.backtracking == 0) {
                                    }
                                }
                                break;

                                default:
                                    break loop2;
                            }
                        } while (true);

                        if (state.backtracking == 0) {
                            ps.println("}");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                    }
                    adaptor.addChild(root_0, root_1);
                    _last = _save_last_1;
                }

                if (state.backtracking == 0) {
                }
            }

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 2, block_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "block"
    public static class statement_return extends TreeRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "statement"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:101:1: statement : ( block | ^( IF a= parenthesizedExpression statement ( statement )? ) | ^( FOR forInit forCondition forUpdater statement ) | ^( WHILE d= parenthesizedExpression statement ) | ^( DO statement b= parenthesizedExpression ) | c= expression | SEMI | ^( DECLARATION ident= IDENT e= expression ) );
    public final VNScriptTree.statement_return statement() throws RecognitionException
    {
        VNScriptTree.statement_return retval = new VNScriptTree.statement_return();
        retval.start = input.LT(1);
        int statement_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree ident = null;
        CommonTree IF6 = null;
        CommonTree FOR9 = null;
        CommonTree WHILE14 = null;
        CommonTree DO16 = null;
        CommonTree SEMI18 = null;
        CommonTree DECLARATION19 = null;
        VNScriptTree.parenthesizedExpression_return a = null;

        VNScriptTree.parenthesizedExpression_return d = null;

        VNScriptTree.parenthesizedExpression_return b = null;

        VNScriptTree.expression_return c = null;

        VNScriptTree.expression_return e = null;

        VNScriptTree.block_return block5 = null;

        VNScriptTree.statement_return statement7 = null;

        VNScriptTree.statement_return statement8 = null;

        VNScriptTree.forInit_return forInit10 = null;

        VNScriptTree.forCondition_return forCondition11 = null;

        VNScriptTree.forUpdater_return forUpdater12 = null;

        VNScriptTree.statement_return statement13 = null;

        VNScriptTree.statement_return statement15 = null;

        VNScriptTree.statement_return statement17 = null;

        CommonTree ident_tree = null;
        CommonTree IF6_tree = null;
        CommonTree FOR9_tree = null;
        CommonTree WHILE14_tree = null;
        CommonTree DO16_tree = null;
        CommonTree SEMI18_tree = null;
        CommonTree DECLARATION19_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 3)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:102:5: ( block | ^( IF a= parenthesizedExpression statement ( statement )? ) | ^( FOR forInit forCondition forUpdater statement ) | ^( WHILE d= parenthesizedExpression statement ) | ^( DO statement b= parenthesizedExpression ) | c= expression | SEMI | ^( DECLARATION ident= IDENT e= expression ) )
            int alt4 = 8;
            switch (input.LA(1)) {
                case BLOCK_SCOPE: {
                    alt4 = 1;
                }
                break;
                case IF: {
                    alt4 = 2;
                }
                break;
                case FOR: {
                    alt4 = 3;
                }
                break;
                case WHILE: {
                    alt4 = 4;
                }
                break;
                case DO: {
                    alt4 = 5;
                }
                break;
                case EXPR: {
                    alt4 = 6;
                }
                break;
                case SEMI: {
                    alt4 = 7;
                }
                break;
                case DECLARATION: {
                    alt4 = 8;
                }
                break;
                default:
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 4, 0, input);

                    throw nvae;
            }

            switch (alt4) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:102:10: block
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    pushFollow(FOLLOW_block_in_statement184);
                    block5 = block();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, block5.getTree());

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:103:9: ^( IF a= parenthesizedExpression statement ( statement )? )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        IF6 = (CommonTree) match(input, IF, FOLLOW_IF_in_statement199);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            IF6_tree = (CommonTree) adaptor.dupNode(IF6);

                            root_1 = (CommonTree) adaptor.becomeRoot(IF6_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_parenthesizedExpression_in_statement203);
                        a = parenthesizedExpression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        if (state.backtracking == 0) {
                            ps.println("if " + (a != null ? a.r : null) + " ");
                        }
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_statement_in_statement216);
                        statement7 = statement();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, statement7.getTree());
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:105:15: ( statement )?
                        int alt3 = 2;
                        int LA3_0 = input.LA(1);

                        if ((LA3_0 == SEMI || LA3_0 == DO || (LA3_0 >= FOR && LA3_0 <= IF) || LA3_0 == WHILE || LA3_0 == BLOCK_SCOPE || LA3_0 == EXPR || LA3_0 == DECLARATION)) {
                            alt3 = 1;
                        }
                        switch (alt3) {
                            case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:106:1: statement
                            {
                                if (state.backtracking == 0) {
                                    ps.println("else ");
                                }
                                _last = (CommonTree) input.LT(1);
                                pushFollow(FOLLOW_statement_in_statement234);
                                statement8 = statement();

                                state._fsp--;
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    adaptor.addChild(root_1, statement8.getTree());

                                if (state.backtracking == 0) {
                                }
                            }
                            break;

                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:109:9: ^( FOR forInit forCondition forUpdater statement )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        FOR9 = (CommonTree) match(input, FOR, FOLLOW_FOR_in_statement253);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            FOR9_tree = (CommonTree) adaptor.dupNode(FOR9);

                            root_1 = (CommonTree) adaptor.becomeRoot(FOR9_tree, root_1);
                        }

                        if (state.backtracking == 0) {
                            ps.print("for ( ");
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_forInit_in_statement266);
                        forInit10 = forInit();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, forInit10.getTree());
                        if (state.backtracking == 0) {
                            ps.print("; ");
                        }
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_forCondition_in_statement283);
                        forCondition11 = forCondition();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, forCondition11.getTree());
                        if (state.backtracking == 0) {
                            ps.print("; ");
                        }
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_forUpdater_in_statement301);
                        forUpdater12 = forUpdater();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, forUpdater12.getTree());
                        if (state.backtracking == 0) {
                            ps.println(") ");
                        }
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_statement_in_statement314);
                        statement13 = statement();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, statement13.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:119:9: ^( WHILE d= parenthesizedExpression statement )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        WHILE14 = (CommonTree) match(input, WHILE, FOLLOW_WHILE_in_statement339);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            WHILE14_tree = (CommonTree) adaptor.dupNode(WHILE14);

                            root_1 = (CommonTree) adaptor.becomeRoot(WHILE14_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_parenthesizedExpression_in_statement349);
                        d = parenthesizedExpression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, d.getTree());
                        if (state.backtracking == 0) {
                            ps.println("while " + (d != null ? d.r : null));
                        }
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_statement_in_statement366);
                        statement15 = statement();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, statement15.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 5: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:125:9: ^( DO statement b= parenthesizedExpression )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        DO16 = (CommonTree) match(input, DO, FOLLOW_DO_in_statement379);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            DO16_tree = (CommonTree) adaptor.dupNode(DO16);

                            root_1 = (CommonTree) adaptor.becomeRoot(DO16_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_statement_in_statement386);
                        statement17 = statement();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, statement17.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_parenthesizedExpression_in_statement396);
                        b = parenthesizedExpression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            ps.println("do " + (b != null ? b.r : null) + ";");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 6: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:131:9: c= expression
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    pushFollow(FOLLOW_expression_in_statement424);
                    c = expression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, c.getTree());
                    if (state.backtracking == 0) {
                        ps.println((c != null ? c.r : null) + ";");
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 7: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:133:9: SEMI
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    SEMI18 = (CommonTree) match(input, SEMI, FOLLOW_SEMI_in_statement444);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        SEMI18_tree = (CommonTree) adaptor.dupNode(SEMI18);

                        adaptor.addChild(root_0, SEMI18_tree);
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 8: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:134:11: ^( DECLARATION ident= IDENT e= expression )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        DECLARATION19 = (CommonTree) match(input, DECLARATION, FOLLOW_DECLARATION_in_statement458);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            DECLARATION19_tree = (CommonTree) adaptor.dupNode(DECLARATION19);

                            root_1 = (CommonTree) adaptor.becomeRoot(DECLARATION19_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        ident = (CommonTree) match(input, IDENT, FOLLOW_IDENT_in_statement462);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            ident_tree = (CommonTree) adaptor.dupNode(ident);

                            adaptor.addChild(root_1, ident_tree);
                        }
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expression_in_statement466);
                        e = expression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, e.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                        ps.println(new VNScriptDeclaration((ident != null ? ident.getText() : null), (e != null ? e.r : null), activeContext) + ";");
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;

            }
            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 3, statement_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "statement"
    public static class forInit_return extends TreeRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "forInit"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:138:1: forInit : ( ^( FOR_INIT ( (a= expression )* )? ) | ^( FOR_INIT ^( DECLARATION ident= IDENT e= expression ) ) );
    public final VNScriptTree.forInit_return forInit() throws RecognitionException
    {
        VNScriptTree.forInit_return retval = new VNScriptTree.forInit_return();
        retval.start = input.LT(1);
        int forInit_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree ident = null;
        CommonTree FOR_INIT20 = null;
        CommonTree FOR_INIT21 = null;
        CommonTree DECLARATION22 = null;
        VNScriptTree.expression_return a = null;

        VNScriptTree.expression_return e = null;

        CommonTree ident_tree = null;
        CommonTree FOR_INIT20_tree = null;
        CommonTree FOR_INIT21_tree = null;
        CommonTree DECLARATION22_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 4)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:139:5: ( ^( FOR_INIT ( (a= expression )* )? ) | ^( FOR_INIT ^( DECLARATION ident= IDENT e= expression ) ) )
            int alt7 = 2;
            int LA7_0 = input.LA(1);

            if ((LA7_0 == FOR_INIT)) {
                int LA7_1 = input.LA(2);

                if ((LA7_1 == DOWN)) {
                    int LA7_2 = input.LA(3);

                    if ((LA7_2 == DECLARATION)) {
                        alt7 = 2;
                    } else if ((LA7_2 == UP || LA7_2 == EXPR)) {
                        alt7 = 1;
                    } else {
                        if (state.backtracking > 0) {
                            state.failed = true;
                            return retval;
                        }
                        NoViableAltException nvae
                            = new NoViableAltException("", 7, 2, input);

                        throw nvae;
                    }
                } else {
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 7, 1, input);

                    throw nvae;
                }
            } else {
                if (state.backtracking > 0) {
                    state.failed = true;
                    return retval;
                }
                NoViableAltException nvae
                    = new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:139:9: ^( FOR_INIT ( (a= expression )* )? )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        FOR_INIT20 = (CommonTree) match(input, FOR_INIT, FOLLOW_FOR_INIT_in_forInit492);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            FOR_INIT20_tree = (CommonTree) adaptor.dupNode(FOR_INIT20);

                            root_1 = (CommonTree) adaptor.becomeRoot(FOR_INIT20_tree, root_1);
                        }

                        if (input.LA(1) == Token.DOWN) {
                            match(input, Token.DOWN, null);
                            if (state.failed)
                                return retval;
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:139:20: ( (a= expression )* )?
                            int alt6 = 2;
                            int LA6_0 = input.LA(1);

                            if ((LA6_0 == EXPR)) {
                                alt6 = 1;
                            } else if ((LA6_0 == UP)) {
                                int LA6_2 = input.LA(2);

                                if ((synpred12_VNScriptTree())) {
                                    alt6 = 1;
                                }
                            }
                            switch (alt6) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:139:21: (a= expression )*
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:139:22: (a= expression )*
                                    loop5:
                                    do {
                                        int alt5 = 2;
                                        int LA5_0 = input.LA(1);

                                        if ((LA5_0 == EXPR)) {
                                            alt5 = 1;
                                        }

                                        switch (alt5) {
                                            case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:0:0: a= expression
                                            {
                                                _last = (CommonTree) input.LT(1);
                                                pushFollow(FOLLOW_expression_in_forInit497);
                                                a = expression();

                                                state._fsp--;
                                                if (state.failed)
                                                    return retval;
                                                if (state.backtracking == 0)
                                                    adaptor.addChild(root_1, a.getTree());

                                                if (state.backtracking == 0) {
                                                }
                                            }
                                            break;

                                            default:
                                                break loop5;
                                        }
                                    } while (true);

                                    if (state.backtracking == 0) {
                                        ps.print((a != null ? a.r : null));
                                    }

                                    if (state.backtracking == 0) {
                                    }
                                }
                                break;

                            }

                            match(input, Token.UP, null);
                            if (state.failed)
                                return retval;
                        }
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:142:9: ^( FOR_INIT ^( DECLARATION ident= IDENT e= expression ) )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        FOR_INIT21 = (CommonTree) match(input, FOR_INIT, FOLLOW_FOR_INIT_in_forInit531);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            FOR_INIT21_tree = (CommonTree) adaptor.dupNode(FOR_INIT21);

                            root_1 = (CommonTree) adaptor.becomeRoot(FOR_INIT21_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        {
                            CommonTree _save_last_2 = _last;
                            CommonTree _first_2 = null;
                            CommonTree root_2 = (CommonTree) adaptor.nil();
                            _last = (CommonTree) input.LT(1);
                            DECLARATION22 = (CommonTree) match(input, DECLARATION, FOLLOW_DECLARATION_in_forInit534);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0) {
                                DECLARATION22_tree = (CommonTree) adaptor.dupNode(DECLARATION22);

                                root_2 = (CommonTree) adaptor.becomeRoot(DECLARATION22_tree, root_2);
                            }

                            match(input, Token.DOWN, null);
                            if (state.failed)
                                return retval;
                            _last = (CommonTree) input.LT(1);
                            ident = (CommonTree) match(input, IDENT, FOLLOW_IDENT_in_forInit538);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0) {
                                ident_tree = (CommonTree) adaptor.dupNode(ident);

                                adaptor.addChild(root_2, ident_tree);
                            }
                            _last = (CommonTree) input.LT(1);
                            pushFollow(FOLLOW_expression_in_forInit542);
                            e = expression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_2, e.getTree());

                            match(input, Token.UP, null);
                            if (state.failed)
                                return retval;
                            adaptor.addChild(root_1, root_2);
                            _last = _save_last_2;
                        }

                        if (state.backtracking == 0) {
                            ps.print(new VNScriptDeclaration((ident != null ? ident.getText() : null), (e != null ? e.r : null), activeContext));
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;

            }
            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 4, forInit_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "forInit"
    public static class forCondition_return extends TreeRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "forCondition"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:147:1: forCondition : ^( FOR_CONDITION (a= expression )? ) ;
    public final VNScriptTree.forCondition_return forCondition() throws RecognitionException
    {
        VNScriptTree.forCondition_return retval = new VNScriptTree.forCondition_return();
        retval.start = input.LT(1);
        int forCondition_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree FOR_CONDITION23 = null;
        VNScriptTree.expression_return a = null;

        CommonTree FOR_CONDITION23_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 5)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:148:5: ( ^( FOR_CONDITION (a= expression )? ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:148:9: ^( FOR_CONDITION (a= expression )? )
            {
                root_0 = (CommonTree) adaptor.nil();

                _last = (CommonTree) input.LT(1);
                {
                    CommonTree _save_last_1 = _last;
                    CommonTree _first_1 = null;
                    CommonTree root_1 = (CommonTree) adaptor.nil();
                    _last = (CommonTree) input.LT(1);
                    FOR_CONDITION23 = (CommonTree) match(input, FOR_CONDITION, FOLLOW_FOR_CONDITION_in_forCondition575);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        FOR_CONDITION23_tree = (CommonTree) adaptor.dupNode(FOR_CONDITION23);

                        root_1 = (CommonTree) adaptor.becomeRoot(FOR_CONDITION23_tree, root_1);
                    }

                    if (input.LA(1) == Token.DOWN) {
                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:148:25: (a= expression )?
                        int alt8 = 2;
                        int LA8_0 = input.LA(1);

                        if ((LA8_0 == EXPR)) {
                            alt8 = 1;
                        }
                        switch (alt8) {
                            case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:148:26: a= expression
                            {
                                _last = (CommonTree) input.LT(1);
                                pushFollow(FOLLOW_expression_in_forCondition580);
                                a = expression();

                                state._fsp--;
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    adaptor.addChild(root_1, a.getTree());
                                if (state.backtracking == 0) {
                                    ps.print((a != null ? a.r : null));
                                }

                                if (state.backtracking == 0) {
                                }
                            }
                            break;

                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                    }
                    adaptor.addChild(root_0, root_1);
                    _last = _save_last_1;
                }

                if (state.backtracking == 0) {
                }
            }

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 5, forCondition_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "forCondition"
    public static class forUpdater_return extends TreeRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "forUpdater"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:153:1: forUpdater : ^( FOR_UPDATE (a= expression )* ) ;
    public final VNScriptTree.forUpdater_return forUpdater() throws RecognitionException
    {
        VNScriptTree.forUpdater_return retval = new VNScriptTree.forUpdater_return();
        retval.start = input.LT(1);
        int forUpdater_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree FOR_UPDATE24 = null;
        VNScriptTree.expression_return a = null;

        CommonTree FOR_UPDATE24_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 6)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:154:5: ( ^( FOR_UPDATE (a= expression )* ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:154:9: ^( FOR_UPDATE (a= expression )* )
            {
                root_0 = (CommonTree) adaptor.nil();

                _last = (CommonTree) input.LT(1);
                {
                    CommonTree _save_last_1 = _last;
                    CommonTree _first_1 = null;
                    CommonTree root_1 = (CommonTree) adaptor.nil();
                    _last = (CommonTree) input.LT(1);
                    FOR_UPDATE24 = (CommonTree) match(input, FOR_UPDATE, FOLLOW_FOR_UPDATE_in_forUpdater626);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        FOR_UPDATE24_tree = (CommonTree) adaptor.dupNode(FOR_UPDATE24);

                        root_1 = (CommonTree) adaptor.becomeRoot(FOR_UPDATE24_tree, root_1);
                    }

                    if (input.LA(1) == Token.DOWN) {
                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:154:22: (a= expression )*
                        loop9:
                        do {
                            int alt9 = 2;
                            int LA9_0 = input.LA(1);

                            if ((LA9_0 == EXPR)) {
                                alt9 = 1;
                            }

                            switch (alt9) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:154:23: a= expression
                                {
                                    _last = (CommonTree) input.LT(1);
                                    pushFollow(FOLLOW_expression_in_forUpdater631);
                                    a = expression();

                                    state._fsp--;
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        adaptor.addChild(root_1, a.getTree());
                                    if (state.backtracking == 0) {
                                        ps.print((a != null ? a.r : null));
                                    }

                                    if (state.backtracking == 0) {
                                    }
                                }
                                break;

                                default:
                                    break loop9;
                            }
                        } while (true);

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                    }
                    adaptor.addChild(root_0, root_1);
                    _last = _save_last_1;
                }

                if (state.backtracking == 0) {
                }
            }

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 6, forUpdater_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "forUpdater"
    public static class parenthesizedExpression_return extends TreeRuleReturnScope
    {

        public VNScriptExpr r;
        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "parenthesizedExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:161:1: parenthesizedExpression returns [ VNScriptExpr r ] : a= expression ;
    public final VNScriptTree.parenthesizedExpression_return parenthesizedExpression() throws RecognitionException
    {
        VNScriptTree.parenthesizedExpression_return retval = new VNScriptTree.parenthesizedExpression_return();
        retval.start = input.LT(1);
        int parenthesizedExpression_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        VNScriptTree.expression_return a = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 7)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:162:5: (a= expression )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:162:9: a= expression
            {
                root_0 = (CommonTree) adaptor.nil();

                _last = (CommonTree) input.LT(1);
                pushFollow(FOLLOW_expression_in_parenthesizedExpression680);
                a = expression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, a.getTree());
                if (state.backtracking == 0) {
                    retval.r = (a != null ? a.r : null);
                }

                if (state.backtracking == 0) {
                }
            }

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 7, parenthesizedExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "parenthesizedExpression"
    public static class expression_return extends TreeRuleReturnScope
    {

        public VNScriptExpr r;
        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "expression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:166:1: expression returns [ VNScriptExpr r ] : ^( EXPR a= expr ) ;
    public final VNScriptTree.expression_return expression() throws RecognitionException
    {
        VNScriptTree.expression_return retval = new VNScriptTree.expression_return();
        retval.start = input.LT(1);
        int expression_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree EXPR25 = null;
        VNScriptTree.expr_return a = null;

        CommonTree EXPR25_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 8)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:167:5: ( ^( EXPR a= expr ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:168:5: ^( EXPR a= expr )
            {
                root_0 = (CommonTree) adaptor.nil();

                _last = (CommonTree) input.LT(1);
                {
                    CommonTree _save_last_1 = _last;
                    CommonTree _first_1 = null;
                    CommonTree root_1 = (CommonTree) adaptor.nil();
                    _last = (CommonTree) input.LT(1);
                    EXPR25 = (CommonTree) match(input, EXPR, FOLLOW_EXPR_in_expression720);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        EXPR25_tree = (CommonTree) adaptor.dupNode(EXPR25);

                        root_1 = (CommonTree) adaptor.becomeRoot(EXPR25_tree, root_1);
                    }

                    match(input, Token.DOWN, null);
                    if (state.failed)
                        return retval;
                    _last = (CommonTree) input.LT(1);
                    pushFollow(FOLLOW_expr_in_expression724);
                    a = expr();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_1, a.getTree());
                    if (state.backtracking == 0) {
                        retval.r = (a != null ? a.r : null);
                    }

                    match(input, Token.UP, null);
                    if (state.failed)
                        return retval;
                    adaptor.addChild(root_0, root_1);
                    _last = _save_last_1;
                }

                if (state.backtracking == 0) {
                }
            }

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 8, expression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "expression"
    public static class expr_return extends TreeRuleReturnScope
    {

        public VNScriptExpr r;
        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "expr"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:173:1: expr returns [ VNScriptExpr r] : ( ^( ASSIGN a= expr b= expr ) | ^( PLUS_ASSIGN a= expr b= expr ) | ^( MINUS_ASSIGN a= expr b= expr ) | ^( STAR_ASSIGN a= expr b= expr ) | ^( DIV_ASSIGN a= expr b= expr ) | ^( AND_ASSIGN expr expr ) | ^( OR_ASSIGN expr expr ) | ^( XOR_ASSIGN expr expr ) | ^( MOD_ASSIGN a= expr b= expr ) | ^( BIT_SHIFT_RIGHT_ASSIGN expr expr ) | ^( SHIFT_RIGHT_ASSIGN expr expr ) | ^( SHIFT_LEFT_ASSIGN expr expr ) | ^( QUESTION a= expr b= expr c= expr ) | ^( LOGICAL_OR a= expr b= expr ) | ^( LOGICAL_AND a= expr b= expr ) | ^( OR expr expr ) | ^( XOR expr expr ) | ^( AND expr expr ) | ^( EQUAL a= expr b= expr ) | ^( NOT_EQUAL a= expr b= expr ) | ^( LESS_OR_EQUAL a= expr b= expr ) | ^( GREATER_OR_EQUAL a= expr b= expr ) | ^( BIT_SHIFT_RIGHT expr expr ) | ^( SHIFT_RIGHT expr expr ) | ^( GREATER_THAN a= expr b= expr ) | ^( SHIFT_LEFT expr expr ) | ^( LESS_THAN a= expr b= expr ) | ^( PLUS a= expr b= expr ) | ^( MINUS a= expr b= expr ) | ^( STAR a= expr b= expr ) | ^( DIV a= expr b= expr ) | ^( MOD a= expr b= expr ) | ^( UNARY_PLUS a= expr ) | ^( UNARY_MINUS a= expr ) | ^( PRE_INC a= expr ) | ^( PRE_DEC a= expr ) | ^( POST_INC a= expr ) | ^( POST_DEC a= expr ) | ^( NOT expr ) | ^( LOGICAL_NOT expr ) | p= primaryExpression );
    public final VNScriptTree.expr_return expr() throws RecognitionException
    {
        VNScriptTree.expr_return retval = new VNScriptTree.expr_return();
        retval.start = input.LT(1);
        int expr_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree ASSIGN26 = null;
        CommonTree PLUS_ASSIGN27 = null;
        CommonTree MINUS_ASSIGN28 = null;
        CommonTree STAR_ASSIGN29 = null;
        CommonTree DIV_ASSIGN30 = null;
        CommonTree AND_ASSIGN31 = null;
        CommonTree OR_ASSIGN34 = null;
        CommonTree XOR_ASSIGN37 = null;
        CommonTree MOD_ASSIGN40 = null;
        CommonTree BIT_SHIFT_RIGHT_ASSIGN41 = null;
        CommonTree SHIFT_RIGHT_ASSIGN44 = null;
        CommonTree SHIFT_LEFT_ASSIGN47 = null;
        CommonTree QUESTION50 = null;
        CommonTree LOGICAL_OR51 = null;
        CommonTree LOGICAL_AND52 = null;
        CommonTree OR53 = null;
        CommonTree XOR56 = null;
        CommonTree AND59 = null;
        CommonTree EQUAL62 = null;
        CommonTree NOT_EQUAL63 = null;
        CommonTree LESS_OR_EQUAL64 = null;
        CommonTree GREATER_OR_EQUAL65 = null;
        CommonTree BIT_SHIFT_RIGHT66 = null;
        CommonTree SHIFT_RIGHT69 = null;
        CommonTree GREATER_THAN72 = null;
        CommonTree SHIFT_LEFT73 = null;
        CommonTree LESS_THAN76 = null;
        CommonTree PLUS77 = null;
        CommonTree MINUS78 = null;
        CommonTree STAR79 = null;
        CommonTree DIV80 = null;
        CommonTree MOD81 = null;
        CommonTree UNARY_PLUS82 = null;
        CommonTree UNARY_MINUS83 = null;
        CommonTree PRE_INC84 = null;
        CommonTree PRE_DEC85 = null;
        CommonTree POST_INC86 = null;
        CommonTree POST_DEC87 = null;
        CommonTree NOT88 = null;
        CommonTree LOGICAL_NOT90 = null;
        VNScriptTree.expr_return a = null;

        VNScriptTree.expr_return b = null;

        VNScriptTree.expr_return c = null;

        VNScriptTree.primaryExpression_return p = null;

        VNScriptTree.expr_return expr32 = null;

        VNScriptTree.expr_return expr33 = null;

        VNScriptTree.expr_return expr35 = null;

        VNScriptTree.expr_return expr36 = null;

        VNScriptTree.expr_return expr38 = null;

        VNScriptTree.expr_return expr39 = null;

        VNScriptTree.expr_return expr42 = null;

        VNScriptTree.expr_return expr43 = null;

        VNScriptTree.expr_return expr45 = null;

        VNScriptTree.expr_return expr46 = null;

        VNScriptTree.expr_return expr48 = null;

        VNScriptTree.expr_return expr49 = null;

        VNScriptTree.expr_return expr54 = null;

        VNScriptTree.expr_return expr55 = null;

        VNScriptTree.expr_return expr57 = null;

        VNScriptTree.expr_return expr58 = null;

        VNScriptTree.expr_return expr60 = null;

        VNScriptTree.expr_return expr61 = null;

        VNScriptTree.expr_return expr67 = null;

        VNScriptTree.expr_return expr68 = null;

        VNScriptTree.expr_return expr70 = null;

        VNScriptTree.expr_return expr71 = null;

        VNScriptTree.expr_return expr74 = null;

        VNScriptTree.expr_return expr75 = null;

        VNScriptTree.expr_return expr89 = null;

        VNScriptTree.expr_return expr91 = null;

        CommonTree ASSIGN26_tree = null;
        CommonTree PLUS_ASSIGN27_tree = null;
        CommonTree MINUS_ASSIGN28_tree = null;
        CommonTree STAR_ASSIGN29_tree = null;
        CommonTree DIV_ASSIGN30_tree = null;
        CommonTree AND_ASSIGN31_tree = null;
        CommonTree OR_ASSIGN34_tree = null;
        CommonTree XOR_ASSIGN37_tree = null;
        CommonTree MOD_ASSIGN40_tree = null;
        CommonTree BIT_SHIFT_RIGHT_ASSIGN41_tree = null;
        CommonTree SHIFT_RIGHT_ASSIGN44_tree = null;
        CommonTree SHIFT_LEFT_ASSIGN47_tree = null;
        CommonTree QUESTION50_tree = null;
        CommonTree LOGICAL_OR51_tree = null;
        CommonTree LOGICAL_AND52_tree = null;
        CommonTree OR53_tree = null;
        CommonTree XOR56_tree = null;
        CommonTree AND59_tree = null;
        CommonTree EQUAL62_tree = null;
        CommonTree NOT_EQUAL63_tree = null;
        CommonTree LESS_OR_EQUAL64_tree = null;
        CommonTree GREATER_OR_EQUAL65_tree = null;
        CommonTree BIT_SHIFT_RIGHT66_tree = null;
        CommonTree SHIFT_RIGHT69_tree = null;
        CommonTree GREATER_THAN72_tree = null;
        CommonTree SHIFT_LEFT73_tree = null;
        CommonTree LESS_THAN76_tree = null;
        CommonTree PLUS77_tree = null;
        CommonTree MINUS78_tree = null;
        CommonTree STAR79_tree = null;
        CommonTree DIV80_tree = null;
        CommonTree MOD81_tree = null;
        CommonTree UNARY_PLUS82_tree = null;
        CommonTree UNARY_MINUS83_tree = null;
        CommonTree PRE_INC84_tree = null;
        CommonTree PRE_DEC85_tree = null;
        CommonTree POST_INC86_tree = null;
        CommonTree POST_DEC87_tree = null;
        CommonTree NOT88_tree = null;
        CommonTree LOGICAL_NOT90_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 9)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:174:5: ( ^( ASSIGN a= expr b= expr ) | ^( PLUS_ASSIGN a= expr b= expr ) | ^( MINUS_ASSIGN a= expr b= expr ) | ^( STAR_ASSIGN a= expr b= expr ) | ^( DIV_ASSIGN a= expr b= expr ) | ^( AND_ASSIGN expr expr ) | ^( OR_ASSIGN expr expr ) | ^( XOR_ASSIGN expr expr ) | ^( MOD_ASSIGN a= expr b= expr ) | ^( BIT_SHIFT_RIGHT_ASSIGN expr expr ) | ^( SHIFT_RIGHT_ASSIGN expr expr ) | ^( SHIFT_LEFT_ASSIGN expr expr ) | ^( QUESTION a= expr b= expr c= expr ) | ^( LOGICAL_OR a= expr b= expr ) | ^( LOGICAL_AND a= expr b= expr ) | ^( OR expr expr ) | ^( XOR expr expr ) | ^( AND expr expr ) | ^( EQUAL a= expr b= expr ) | ^( NOT_EQUAL a= expr b= expr ) | ^( LESS_OR_EQUAL a= expr b= expr ) | ^( GREATER_OR_EQUAL a= expr b= expr ) | ^( BIT_SHIFT_RIGHT expr expr ) | ^( SHIFT_RIGHT expr expr ) | ^( GREATER_THAN a= expr b= expr ) | ^( SHIFT_LEFT expr expr ) | ^( LESS_THAN a= expr b= expr ) | ^( PLUS a= expr b= expr ) | ^( MINUS a= expr b= expr ) | ^( STAR a= expr b= expr ) | ^( DIV a= expr b= expr ) | ^( MOD a= expr b= expr ) | ^( UNARY_PLUS a= expr ) | ^( UNARY_MINUS a= expr ) | ^( PRE_INC a= expr ) | ^( PRE_DEC a= expr ) | ^( POST_INC a= expr ) | ^( POST_DEC a= expr ) | ^( NOT expr ) | ^( LOGICAL_NOT expr ) | p= primaryExpression )
            int alt10 = 41;
            switch (input.LA(1)) {
                case ASSIGN: {
                    alt10 = 1;
                }
                break;
                case PLUS_ASSIGN: {
                    alt10 = 2;
                }
                break;
                case MINUS_ASSIGN: {
                    alt10 = 3;
                }
                break;
                case STAR_ASSIGN: {
                    alt10 = 4;
                }
                break;
                case DIV_ASSIGN: {
                    alt10 = 5;
                }
                break;
                case AND_ASSIGN: {
                    alt10 = 6;
                }
                break;
                case OR_ASSIGN: {
                    alt10 = 7;
                }
                break;
                case XOR_ASSIGN: {
                    alt10 = 8;
                }
                break;
                case MOD_ASSIGN: {
                    alt10 = 9;
                }
                break;
                case BIT_SHIFT_RIGHT_ASSIGN: {
                    alt10 = 10;
                }
                break;
                case SHIFT_RIGHT_ASSIGN: {
                    alt10 = 11;
                }
                break;
                case SHIFT_LEFT_ASSIGN: {
                    alt10 = 12;
                }
                break;
                case QUESTION: {
                    alt10 = 13;
                }
                break;
                case LOGICAL_OR: {
                    alt10 = 14;
                }
                break;
                case LOGICAL_AND: {
                    alt10 = 15;
                }
                break;
                case OR: {
                    alt10 = 16;
                }
                break;
                case XOR: {
                    alt10 = 17;
                }
                break;
                case AND: {
                    alt10 = 18;
                }
                break;
                case EQUAL: {
                    alt10 = 19;
                }
                break;
                case NOT_EQUAL: {
                    alt10 = 20;
                }
                break;
                case LESS_OR_EQUAL: {
                    alt10 = 21;
                }
                break;
                case GREATER_OR_EQUAL: {
                    alt10 = 22;
                }
                break;
                case BIT_SHIFT_RIGHT: {
                    alt10 = 23;
                }
                break;
                case SHIFT_RIGHT: {
                    alt10 = 24;
                }
                break;
                case GREATER_THAN: {
                    alt10 = 25;
                }
                break;
                case SHIFT_LEFT: {
                    alt10 = 26;
                }
                break;
                case LESS_THAN: {
                    alt10 = 27;
                }
                break;
                case PLUS: {
                    alt10 = 28;
                }
                break;
                case MINUS: {
                    alt10 = 29;
                }
                break;
                case STAR: {
                    alt10 = 30;
                }
                break;
                case DIV: {
                    alt10 = 31;
                }
                break;
                case MOD: {
                    alt10 = 32;
                }
                break;
                case UNARY_PLUS: {
                    alt10 = 33;
                }
                break;
                case UNARY_MINUS: {
                    alt10 = 34;
                }
                break;
                case PRE_INC: {
                    alt10 = 35;
                }
                break;
                case PRE_DEC: {
                    alt10 = 36;
                }
                break;
                case POST_INC: {
                    alt10 = 37;
                }
                break;
                case POST_DEC: {
                    alt10 = 38;
                }
                break;
                case NOT: {
                    alt10 = 39;
                }
                break;
                case LOGICAL_NOT: {
                    alt10 = 40;
                }
                break;
                case DOT:
                case FALSE:
                case NULL:
                case TRUE:
                case ARRAY_ELEMENT_ACCESS:
                case EXPR:
                case METHOD_CALL:
                case IDENT:
                case HEX_LITERAL:
                case OCTAL_LITERAL:
                case DECIMAL_LITERAL:
                case FLOATING_POINT_LITERAL:
                case CHARACTER_LITERAL:
                case STRING_LITERAL: {
                    alt10 = 41;
                }
                break;
                default:
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 10, 0, input);

                    throw nvae;
            }

            switch (alt10) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:175:5: ^( ASSIGN a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        ASSIGN26 = (CommonTree) match(input, ASSIGN, FOLLOW_ASSIGN_in_expr768);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            ASSIGN26_tree = (CommonTree) adaptor.dupNode(ASSIGN26);

                            root_1 = (CommonTree) adaptor.becomeRoot(ASSIGN26_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr772);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr776);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptAssignExpr(a.r, b.r, "=", activeContext);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:178:9: ^( PLUS_ASSIGN a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        PLUS_ASSIGN27 = (CommonTree) match(input, PLUS_ASSIGN, FOLLOW_PLUS_ASSIGN_in_expr795);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            PLUS_ASSIGN27_tree = (CommonTree) adaptor.dupNode(PLUS_ASSIGN27);

                            root_1 = (CommonTree) adaptor.becomeRoot(PLUS_ASSIGN27_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr799);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr803);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptAssignExpr(a.r, b.r, "+=", activeContext);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:181:9: ^( MINUS_ASSIGN a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        MINUS_ASSIGN28 = (CommonTree) match(input, MINUS_ASSIGN, FOLLOW_MINUS_ASSIGN_in_expr822);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            MINUS_ASSIGN28_tree = (CommonTree) adaptor.dupNode(MINUS_ASSIGN28);

                            root_1 = (CommonTree) adaptor.becomeRoot(MINUS_ASSIGN28_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr826);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr830);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptAssignExpr(a.r, b.r, "-=", activeContext);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:184:9: ^( STAR_ASSIGN a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        STAR_ASSIGN29 = (CommonTree) match(input, STAR_ASSIGN, FOLLOW_STAR_ASSIGN_in_expr849);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            STAR_ASSIGN29_tree = (CommonTree) adaptor.dupNode(STAR_ASSIGN29);

                            root_1 = (CommonTree) adaptor.becomeRoot(STAR_ASSIGN29_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr853);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr857);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptAssignExpr(a.r, b.r, "*=", activeContext);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 5: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:187:9: ^( DIV_ASSIGN a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        DIV_ASSIGN30 = (CommonTree) match(input, DIV_ASSIGN, FOLLOW_DIV_ASSIGN_in_expr876);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            DIV_ASSIGN30_tree = (CommonTree) adaptor.dupNode(DIV_ASSIGN30);

                            root_1 = (CommonTree) adaptor.becomeRoot(DIV_ASSIGN30_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr880);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr884);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptAssignExpr(a.r, b.r, "/=", activeContext);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 6: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:190:9: ^( AND_ASSIGN expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        AND_ASSIGN31 = (CommonTree) match(input, AND_ASSIGN, FOLLOW_AND_ASSIGN_in_expr903);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            AND_ASSIGN31_tree = (CommonTree) adaptor.dupNode(AND_ASSIGN31);

                            root_1 = (CommonTree) adaptor.becomeRoot(AND_ASSIGN31_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr905);
                        expr32 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr32.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr907);
                        expr33 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr33.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 7: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:191:9: ^( OR_ASSIGN expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        OR_ASSIGN34 = (CommonTree) match(input, OR_ASSIGN, FOLLOW_OR_ASSIGN_in_expr919);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            OR_ASSIGN34_tree = (CommonTree) adaptor.dupNode(OR_ASSIGN34);

                            root_1 = (CommonTree) adaptor.becomeRoot(OR_ASSIGN34_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr921);
                        expr35 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr35.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr923);
                        expr36 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr36.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 8: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:192:9: ^( XOR_ASSIGN expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        XOR_ASSIGN37 = (CommonTree) match(input, XOR_ASSIGN, FOLLOW_XOR_ASSIGN_in_expr935);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            XOR_ASSIGN37_tree = (CommonTree) adaptor.dupNode(XOR_ASSIGN37);

                            root_1 = (CommonTree) adaptor.becomeRoot(XOR_ASSIGN37_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr937);
                        expr38 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr38.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr939);
                        expr39 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr39.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 9: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:193:9: ^( MOD_ASSIGN a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        MOD_ASSIGN40 = (CommonTree) match(input, MOD_ASSIGN, FOLLOW_MOD_ASSIGN_in_expr951);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            MOD_ASSIGN40_tree = (CommonTree) adaptor.dupNode(MOD_ASSIGN40);

                            root_1 = (CommonTree) adaptor.becomeRoot(MOD_ASSIGN40_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr955);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr959);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptAssignExpr(a.r, b.r, "%=", activeContext);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 10: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:196:9: ^( BIT_SHIFT_RIGHT_ASSIGN expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        BIT_SHIFT_RIGHT_ASSIGN41 = (CommonTree) match(input, BIT_SHIFT_RIGHT_ASSIGN, FOLLOW_BIT_SHIFT_RIGHT_ASSIGN_in_expr978);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            BIT_SHIFT_RIGHT_ASSIGN41_tree = (CommonTree) adaptor.dupNode(BIT_SHIFT_RIGHT_ASSIGN41);

                            root_1 = (CommonTree) adaptor.becomeRoot(BIT_SHIFT_RIGHT_ASSIGN41_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr980);
                        expr42 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr42.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr982);
                        expr43 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr43.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 11: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:197:9: ^( SHIFT_RIGHT_ASSIGN expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        SHIFT_RIGHT_ASSIGN44 = (CommonTree) match(input, SHIFT_RIGHT_ASSIGN, FOLLOW_SHIFT_RIGHT_ASSIGN_in_expr994);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            SHIFT_RIGHT_ASSIGN44_tree = (CommonTree) adaptor.dupNode(SHIFT_RIGHT_ASSIGN44);

                            root_1 = (CommonTree) adaptor.becomeRoot(SHIFT_RIGHT_ASSIGN44_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr996);
                        expr45 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr45.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr998);
                        expr46 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr46.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 12: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:198:9: ^( SHIFT_LEFT_ASSIGN expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        SHIFT_LEFT_ASSIGN47 = (CommonTree) match(input, SHIFT_LEFT_ASSIGN, FOLLOW_SHIFT_LEFT_ASSIGN_in_expr1010);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            SHIFT_LEFT_ASSIGN47_tree = (CommonTree) adaptor.dupNode(SHIFT_LEFT_ASSIGN47);

                            root_1 = (CommonTree) adaptor.becomeRoot(SHIFT_LEFT_ASSIGN47_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1012);
                        expr48 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr48.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1014);
                        expr49 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr49.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 13: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:199:9: ^( QUESTION a= expr b= expr c= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        QUESTION50 = (CommonTree) match(input, QUESTION, FOLLOW_QUESTION_in_expr1026);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            QUESTION50_tree = (CommonTree) adaptor.dupNode(QUESTION50);

                            root_1 = (CommonTree) adaptor.becomeRoot(QUESTION50_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1030);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1034);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1038);
                        c = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, c.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptQ(a.r, b.r, c.r);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 14: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:202:9: ^( LOGICAL_OR a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        LOGICAL_OR51 = (CommonTree) match(input, LOGICAL_OR, FOLLOW_LOGICAL_OR_in_expr1065);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            LOGICAL_OR51_tree = (CommonTree) adaptor.dupNode(LOGICAL_OR51);

                            root_1 = (CommonTree) adaptor.becomeRoot(LOGICAL_OR51_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1070);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1074);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, "||");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 15: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:205:9: ^( LOGICAL_AND a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        LOGICAL_AND52 = (CommonTree) match(input, LOGICAL_AND, FOLLOW_LOGICAL_AND_in_expr1101);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            LOGICAL_AND52_tree = (CommonTree) adaptor.dupNode(LOGICAL_AND52);

                            root_1 = (CommonTree) adaptor.becomeRoot(LOGICAL_AND52_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1105);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1109);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, "&&");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 16: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:208:9: ^( OR expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        OR53 = (CommonTree) match(input, OR, FOLLOW_OR_in_expr1132);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            OR53_tree = (CommonTree) adaptor.dupNode(OR53);

                            root_1 = (CommonTree) adaptor.becomeRoot(OR53_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1134);
                        expr54 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr54.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1136);
                        expr55 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr55.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 17: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:209:9: ^( XOR expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        XOR56 = (CommonTree) match(input, XOR, FOLLOW_XOR_in_expr1148);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            XOR56_tree = (CommonTree) adaptor.dupNode(XOR56);

                            root_1 = (CommonTree) adaptor.becomeRoot(XOR56_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1150);
                        expr57 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr57.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1152);
                        expr58 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr58.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 18: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:210:9: ^( AND expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        AND59 = (CommonTree) match(input, AND, FOLLOW_AND_in_expr1164);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            AND59_tree = (CommonTree) adaptor.dupNode(AND59);

                            root_1 = (CommonTree) adaptor.becomeRoot(AND59_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1166);
                        expr60 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr60.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1168);
                        expr61 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr61.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 19: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:211:9: ^( EQUAL a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        EQUAL62 = (CommonTree) match(input, EQUAL, FOLLOW_EQUAL_in_expr1180);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            EQUAL62_tree = (CommonTree) adaptor.dupNode(EQUAL62);

                            root_1 = (CommonTree) adaptor.becomeRoot(EQUAL62_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1184);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1188);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, "==");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 20: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:214:9: ^( NOT_EQUAL a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        NOT_EQUAL63 = (CommonTree) match(input, NOT_EQUAL, FOLLOW_NOT_EQUAL_in_expr1211);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            NOT_EQUAL63_tree = (CommonTree) adaptor.dupNode(NOT_EQUAL63);

                            root_1 = (CommonTree) adaptor.becomeRoot(NOT_EQUAL63_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1215);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1219);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, "!=");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 21: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:217:9: ^( LESS_OR_EQUAL a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        LESS_OR_EQUAL64 = (CommonTree) match(input, LESS_OR_EQUAL, FOLLOW_LESS_OR_EQUAL_in_expr1242);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            LESS_OR_EQUAL64_tree = (CommonTree) adaptor.dupNode(LESS_OR_EQUAL64);

                            root_1 = (CommonTree) adaptor.becomeRoot(LESS_OR_EQUAL64_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1246);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1250);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, "<=");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 22: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:220:9: ^( GREATER_OR_EQUAL a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        GREATER_OR_EQUAL65 = (CommonTree) match(input, GREATER_OR_EQUAL, FOLLOW_GREATER_OR_EQUAL_in_expr1273);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            GREATER_OR_EQUAL65_tree = (CommonTree) adaptor.dupNode(GREATER_OR_EQUAL65);

                            root_1 = (CommonTree) adaptor.becomeRoot(GREATER_OR_EQUAL65_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1277);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1281);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, ">=");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 23: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:223:9: ^( BIT_SHIFT_RIGHT expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        BIT_SHIFT_RIGHT66 = (CommonTree) match(input, BIT_SHIFT_RIGHT, FOLLOW_BIT_SHIFT_RIGHT_in_expr1304);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            BIT_SHIFT_RIGHT66_tree = (CommonTree) adaptor.dupNode(BIT_SHIFT_RIGHT66);

                            root_1 = (CommonTree) adaptor.becomeRoot(BIT_SHIFT_RIGHT66_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1306);
                        expr67 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr67.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1308);
                        expr68 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr68.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 24: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:224:9: ^( SHIFT_RIGHT expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        SHIFT_RIGHT69 = (CommonTree) match(input, SHIFT_RIGHT, FOLLOW_SHIFT_RIGHT_in_expr1320);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            SHIFT_RIGHT69_tree = (CommonTree) adaptor.dupNode(SHIFT_RIGHT69);

                            root_1 = (CommonTree) adaptor.becomeRoot(SHIFT_RIGHT69_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1322);
                        expr70 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr70.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1324);
                        expr71 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr71.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 25: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:225:9: ^( GREATER_THAN a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        GREATER_THAN72 = (CommonTree) match(input, GREATER_THAN, FOLLOW_GREATER_THAN_in_expr1336);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            GREATER_THAN72_tree = (CommonTree) adaptor.dupNode(GREATER_THAN72);

                            root_1 = (CommonTree) adaptor.becomeRoot(GREATER_THAN72_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1340);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1344);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, ">");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 26: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:228:9: ^( SHIFT_LEFT expr expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        SHIFT_LEFT73 = (CommonTree) match(input, SHIFT_LEFT, FOLLOW_SHIFT_LEFT_in_expr1367);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            SHIFT_LEFT73_tree = (CommonTree) adaptor.dupNode(SHIFT_LEFT73);

                            root_1 = (CommonTree) adaptor.becomeRoot(SHIFT_LEFT73_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1369);
                        expr74 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr74.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1371);
                        expr75 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr75.getTree());

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 27: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:229:9: ^( LESS_THAN a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        LESS_THAN76 = (CommonTree) match(input, LESS_THAN, FOLLOW_LESS_THAN_in_expr1383);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            LESS_THAN76_tree = (CommonTree) adaptor.dupNode(LESS_THAN76);

                            root_1 = (CommonTree) adaptor.becomeRoot(LESS_THAN76_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1387);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1391);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, "<");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 28: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:232:9: ^( PLUS a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        PLUS77 = (CommonTree) match(input, PLUS, FOLLOW_PLUS_in_expr1414);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            PLUS77_tree = (CommonTree) adaptor.dupNode(PLUS77);

                            root_1 = (CommonTree) adaptor.becomeRoot(PLUS77_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1418);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1422);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, "+");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 29: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:235:9: ^( MINUS a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        MINUS78 = (CommonTree) match(input, MINUS, FOLLOW_MINUS_in_expr1447);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            MINUS78_tree = (CommonTree) adaptor.dupNode(MINUS78);

                            root_1 = (CommonTree) adaptor.becomeRoot(MINUS78_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1451);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1455);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, "-");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 30: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:238:9: ^( STAR a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        STAR79 = (CommonTree) match(input, STAR, FOLLOW_STAR_in_expr1478);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            STAR79_tree = (CommonTree) adaptor.dupNode(STAR79);

                            root_1 = (CommonTree) adaptor.becomeRoot(STAR79_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1482);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1486);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, "*");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 31: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:241:9: ^( DIV a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        DIV80 = (CommonTree) match(input, DIV, FOLLOW_DIV_in_expr1509);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            DIV80_tree = (CommonTree) adaptor.dupNode(DIV80);

                            root_1 = (CommonTree) adaptor.becomeRoot(DIV80_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1513);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1517);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, "/");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 32: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:244:9: ^( MOD a= expr b= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        MOD81 = (CommonTree) match(input, MOD, FOLLOW_MOD_in_expr1540);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            MOD81_tree = (CommonTree) adaptor.dupNode(MOD81);

                            root_1 = (CommonTree) adaptor.becomeRoot(MOD81_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1544);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1548);
                        b = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, b.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, b.r, "/");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 33: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:247:9: ^( UNARY_PLUS a= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        UNARY_PLUS82 = (CommonTree) match(input, UNARY_PLUS, FOLLOW_UNARY_PLUS_in_expr1571);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            UNARY_PLUS82_tree = (CommonTree) adaptor.dupNode(UNARY_PLUS82);

                            root_1 = (CommonTree) adaptor.becomeRoot(UNARY_PLUS82_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1575);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr("+", a.r);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 34: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:250:9: ^( UNARY_MINUS a= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        UNARY_MINUS83 = (CommonTree) match(input, UNARY_MINUS, FOLLOW_UNARY_MINUS_in_expr1598);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            UNARY_MINUS83_tree = (CommonTree) adaptor.dupNode(UNARY_MINUS83);

                            root_1 = (CommonTree) adaptor.becomeRoot(UNARY_MINUS83_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1602);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr("-", a.r);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 35: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:253:9: ^( PRE_INC a= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        PRE_INC84 = (CommonTree) match(input, PRE_INC, FOLLOW_PRE_INC_in_expr1625);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            PRE_INC84_tree = (CommonTree) adaptor.dupNode(PRE_INC84);

                            root_1 = (CommonTree) adaptor.becomeRoot(PRE_INC84_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1629);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr("++", a.r);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 36: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:256:9: ^( PRE_DEC a= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        PRE_DEC85 = (CommonTree) match(input, PRE_DEC, FOLLOW_PRE_DEC_in_expr1652);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            PRE_DEC85_tree = (CommonTree) adaptor.dupNode(PRE_DEC85);

                            root_1 = (CommonTree) adaptor.becomeRoot(PRE_DEC85_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1656);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr("--", a.r);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 37: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:259:9: ^( POST_INC a= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        POST_INC86 = (CommonTree) match(input, POST_INC, FOLLOW_POST_INC_in_expr1679);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            POST_INC86_tree = (CommonTree) adaptor.dupNode(POST_INC86);

                            root_1 = (CommonTree) adaptor.becomeRoot(POST_INC86_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1683);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, "++");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 38: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:262:9: ^( POST_DEC a= expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        POST_DEC87 = (CommonTree) match(input, POST_DEC, FOLLOW_POST_DEC_in_expr1706);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            POST_DEC87_tree = (CommonTree) adaptor.dupNode(POST_DEC87);

                            root_1 = (CommonTree) adaptor.becomeRoot(POST_DEC87_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1710);
                        a = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr(a.r, "--");
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 39: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:265:9: ^( NOT expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        NOT88 = (CommonTree) match(input, NOT, FOLLOW_NOT_in_expr1733);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            NOT88_tree = (CommonTree) adaptor.dupNode(NOT88);

                            root_1 = (CommonTree) adaptor.becomeRoot(NOT88_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1735);
                        expr89 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr89.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr("!", a.r);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 40: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:268:9: ^( LOGICAL_NOT expr )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        LOGICAL_NOT90 = (CommonTree) match(input, LOGICAL_NOT, FOLLOW_LOGICAL_NOT_in_expr1758);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            LOGICAL_NOT90_tree = (CommonTree) adaptor.dupNode(LOGICAL_NOT90);

                            root_1 = (CommonTree) adaptor.becomeRoot(LOGICAL_NOT90_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expr_in_expr1760);
                        expr91 = expr();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, expr91.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptExpr("!", a.r);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 41: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:271:9: p= primaryExpression
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    pushFollow(FOLLOW_primaryExpression_in_expr1784);
                    p = primaryExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, p.getTree());
                    if (state.backtracking == 0) {
                        retval.r = p.r;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;

            }
            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 9, expr_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "expr"
    public static class primaryExpression_return extends TreeRuleReturnScope
    {

        public VNScriptExpr r;
        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "primaryExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:275:1: primaryExpression returns [ VNScriptExpr r ] : ( ^( DOT (d= primaryExpression (ident= IDENT ) ) ) | p= parenthesizedExpression | ident= IDENT | ^( METHOD_CALL e= IDENT z= arguments ) | ^( ARRAY_ELEMENT_ACCESS e= IDENT (range_r= range )* (f= IDENT )? ) | l= literal );
    public final VNScriptTree.primaryExpression_return primaryExpression() throws RecognitionException
    {
        VNScriptTree.primaryExpression_return retval = new VNScriptTree.primaryExpression_return();
        retval.start = input.LT(1);
        int primaryExpression_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree ident = null;
        CommonTree e = null;
        CommonTree f = null;
        CommonTree DOT92 = null;
        CommonTree METHOD_CALL93 = null;
        CommonTree ARRAY_ELEMENT_ACCESS94 = null;
        VNScriptTree.primaryExpression_return d = null;

        VNScriptTree.parenthesizedExpression_return p = null;

        VNScriptTree.arguments_return z = null;

        VNScriptTree.range_return range_r = null;

        VNScriptTree.literal_return l = null;

        CommonTree ident_tree = null;
        CommonTree e_tree = null;
        CommonTree f_tree = null;
        CommonTree DOT92_tree = null;
        CommonTree METHOD_CALL93_tree = null;
        CommonTree ARRAY_ELEMENT_ACCESS94_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 10)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:276:5: ( ^( DOT (d= primaryExpression (ident= IDENT ) ) ) | p= parenthesizedExpression | ident= IDENT | ^( METHOD_CALL e= IDENT z= arguments ) | ^( ARRAY_ELEMENT_ACCESS e= IDENT (range_r= range )* (f= IDENT )? ) | l= literal )
            int alt13 = 6;
            switch (input.LA(1)) {
                case DOT: {
                    alt13 = 1;
                }
                break;
                case EXPR: {
                    alt13 = 2;
                }
                break;
                case IDENT: {
                    alt13 = 3;
                }
                break;
                case METHOD_CALL: {
                    alt13 = 4;
                }
                break;
                case ARRAY_ELEMENT_ACCESS: {
                    alt13 = 5;
                }
                break;
                case FALSE:
                case NULL:
                case TRUE:
                case HEX_LITERAL:
                case OCTAL_LITERAL:
                case DECIMAL_LITERAL:
                case FLOATING_POINT_LITERAL:
                case CHARACTER_LITERAL:
                case STRING_LITERAL: {
                    alt13 = 6;
                }
                break;
                default:
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 13, 0, input);

                    throw nvae;
            }

            switch (alt13) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:276:9: ^( DOT (d= primaryExpression (ident= IDENT ) ) )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        DOT92 = (CommonTree) match(input, DOT, FOLLOW_DOT_in_primaryExpression1829);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            DOT92_tree = (CommonTree) adaptor.dupNode(DOT92);

                            root_1 = (CommonTree) adaptor.becomeRoot(DOT92_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:277:13: (d= primaryExpression (ident= IDENT ) )
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:277:17: d= primaryExpression (ident= IDENT )
                        {
                            _last = (CommonTree) input.LT(1);
                            pushFollow(FOLLOW_primaryExpression_in_primaryExpression1849);
                            d = primaryExpression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_1, d.getTree());
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:278:17: (ident= IDENT )
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:278:20: ident= IDENT
                            {
                                _last = (CommonTree) input.LT(1);
                                ident = (CommonTree) match(input, IDENT, FOLLOW_IDENT_in_primaryExpression1872);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    ident_tree = (CommonTree) adaptor.dupNode(ident);

                                    adaptor.addChild(root_1, ident_tree);
                                }
                                if (state.backtracking == 0) {

                                    VNScriptVariable v = activeContext.getVariable((d != null ? d.r : null).toString(true));
                                    ranges = new VNScriptRanges();
                                    for (int i = 0; i < v.dimensions; i++) {
                                        ranges.ranges.add(new VNScriptRange(VNScriptRangeType.ALL));
                                    }
                                    retval.r = new VNScriptSubArray((d != null ? d.r : null).toString(true), ranges, v.veclen, v.dimensions, (ident != null ? ident.getText() : null));

                                }

                                if (state.backtracking == 0) {
                                }
                            }

                            if (state.backtracking == 0) {
                            }
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:291:9: p= parenthesizedExpression
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    pushFollow(FOLLOW_parenthesizedExpression_in_primaryExpression1928);
                    p = parenthesizedExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, p.getTree());
                    if (state.backtracking == 0) {
                        retval.r = p.r;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:293:9: ident= IDENT
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    ident = (CommonTree) match(input, IDENT, FOLLOW_IDENT_in_primaryExpression1946);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        ident_tree = (CommonTree) adaptor.dupNode(ident);

                        adaptor.addChild(root_0, ident_tree);
                    }
                    if (state.backtracking == 0) {

                        VNScriptVariable v = activeContext.getVariable((ident != null ? ident.getText() : null));
                        if (v != null) {
                            v.setUsed(true);
                            if (v.type == VNScriptType.ARRAY) {
                                retval.r = new VNScriptArray((ident != null ? ident.getText() : null), v.veclen, v.dimensions);
                            } else {
                                retval.r = new VNScriptScalar((ident != null ? ident.getText() : null));
                            }
                        } else {
                            retval.r = new VNScriptUndefined((ident != null ? ident.getText() : null));
                            if (!(ident != null ? ident.getText() : null).startsWith("_")) {
                                warning("Variable '" + (ident != null ? ident.getText() : null) + "' not defined", (ident != null ? ident.getLine() : 0));
                            }
                        }

                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:310:9: ^( METHOD_CALL e= IDENT z= arguments )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        METHOD_CALL93 = (CommonTree) match(input, METHOD_CALL, FOLLOW_METHOD_CALL_in_primaryExpression1967);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            METHOD_CALL93_tree = (CommonTree) adaptor.dupNode(METHOD_CALL93);

                            root_1 = (CommonTree) adaptor.becomeRoot(METHOD_CALL93_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        e = (CommonTree) match(input, IDENT, FOLLOW_IDENT_in_primaryExpression1971);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            e_tree = (CommonTree) adaptor.dupNode(e);

                            adaptor.addChild(root_1, e_tree);
                        }
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_arguments_in_primaryExpression1975);
                        z = arguments();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, z.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptMethodCall((e != null ? e.getText() : null), activeContext, z.r);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 5: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:314:5: ^( ARRAY_ELEMENT_ACCESS e= IDENT (range_r= range )* (f= IDENT )? )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        ARRAY_ELEMENT_ACCESS94 = (CommonTree) match(input, ARRAY_ELEMENT_ACCESS, FOLLOW_ARRAY_ELEMENT_ACCESS_in_primaryExpression2006);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            ARRAY_ELEMENT_ACCESS94_tree = (CommonTree) adaptor.dupNode(ARRAY_ELEMENT_ACCESS94);

                            root_1 = (CommonTree) adaptor.becomeRoot(ARRAY_ELEMENT_ACCESS94_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        e = (CommonTree) match(input, IDENT, FOLLOW_IDENT_in_primaryExpression2010);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            e_tree = (CommonTree) adaptor.dupNode(e);

                            adaptor.addChild(root_1, e_tree);
                        }
                        if (state.backtracking == 0) {
                            ranges = new VNScriptRanges();
                        }
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:316:5: (range_r= range )*
                        loop11:
                        do {
                            int alt11 = 2;
                            int LA11_0 = input.LA(1);

                            if (((LA11_0 >= RANGE_LEFT && LA11_0 <= RANGE_SINGLE))) {
                                alt11 = 1;
                            }

                            switch (alt11) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:316:7: range_r= range
                                {
                                    _last = (CommonTree) input.LT(1);
                                    pushFollow(FOLLOW_range_in_primaryExpression2030);
                                    range_r = range();

                                    state._fsp--;
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        adaptor.addChild(root_1, range_r.getTree());
                                    if (state.backtracking == 0) {
                                        ranges.ranges.add(range_r.r);
                                    }

                                    if (state.backtracking == 0) {
                                    }
                                }
                                break;

                                default:
                                    break loop11;
                            }
                        } while (true);

                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:317:8: (f= IDENT )?
                        int alt12 = 2;
                        int LA12_0 = input.LA(1);

                        if ((LA12_0 == IDENT)) {
                            alt12 = 1;
                        }
                        switch (alt12) {
                            case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:317:9: f= IDENT
                            {
                                _last = (CommonTree) input.LT(1);
                                f = (CommonTree) match(input, IDENT, FOLLOW_IDENT_in_primaryExpression2047);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    f_tree = (CommonTree) adaptor.dupNode(f);

                                    adaptor.addChild(root_1, f_tree);
                                }
                                if (state.backtracking == 0) {
                                    activeVec = (f != null ? f.getText() : null);
                                }

                                if (state.backtracking == 0) {
                                }
                            }
                            break;

                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {

                        VNScriptVariable v = activeContext.getVariable((e != null ? e.getText() : null));
                        v.setUsed(true);
                        retval.r = new VNScriptSubArray((e != null ? e.getText() : null), ranges, v.veclen, v.dimensions, activeVec);

                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 6: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:326:8: l= literal
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    pushFollow(FOLLOW_literal_in_primaryExpression2098);
                    l = literal();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, l.getTree());
                    if (state.backtracking == 0) {
                        retval.r = l.r;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;

            }
            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 10, primaryExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "primaryExpression"
    public static class range_return extends TreeRuleReturnScope
    {

        public VNScriptRange r;
        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "range"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:330:1: range returns [VNScriptRange r] : ( ^( RANGE_OFFSET a= expression ) | ^( RANGE_SINGLE a= expression ) | ^( RANGE_LEFT a= expression (b= expression )? ) | ^( RANGE_RIGHT a= expression ) | ^( RANGE_ALL ) );
    public final VNScriptTree.range_return range() throws RecognitionException
    {
        VNScriptTree.range_return retval = new VNScriptTree.range_return();
        retval.start = input.LT(1);
        int range_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree RANGE_OFFSET95 = null;
        CommonTree RANGE_SINGLE96 = null;
        CommonTree RANGE_LEFT97 = null;
        CommonTree RANGE_RIGHT98 = null;
        CommonTree RANGE_ALL99 = null;
        VNScriptTree.expression_return a = null;

        VNScriptTree.expression_return b = null;

        CommonTree RANGE_OFFSET95_tree = null;
        CommonTree RANGE_SINGLE96_tree = null;
        CommonTree RANGE_LEFT97_tree = null;
        CommonTree RANGE_RIGHT98_tree = null;
        CommonTree RANGE_ALL99_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 11)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:331:3: ( ^( RANGE_OFFSET a= expression ) | ^( RANGE_SINGLE a= expression ) | ^( RANGE_LEFT a= expression (b= expression )? ) | ^( RANGE_RIGHT a= expression ) | ^( RANGE_ALL ) )
            int alt15 = 5;
            switch (input.LA(1)) {
                case RANGE_OFFSET: {
                    alt15 = 1;
                }
                break;
                case RANGE_SINGLE: {
                    alt15 = 2;
                }
                break;
                case RANGE_LEFT: {
                    alt15 = 3;
                }
                break;
                case RANGE_RIGHT: {
                    alt15 = 4;
                }
                break;
                case RANGE_ALL: {
                    alt15 = 5;
                }
                break;
                default:
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 15, 0, input);

                    throw nvae;
            }

            switch (alt15) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:332:4: ^( RANGE_OFFSET a= expression )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        RANGE_OFFSET95 = (CommonTree) match(input, RANGE_OFFSET, FOLLOW_RANGE_OFFSET_in_range2133);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            RANGE_OFFSET95_tree = (CommonTree) adaptor.dupNode(RANGE_OFFSET95);

                            root_1 = (CommonTree) adaptor.becomeRoot(RANGE_OFFSET95_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expression_in_range2137);
                        a = expression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptRange(VNScriptRangeType.OFFSET, a.r);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:335:5: ^( RANGE_SINGLE a= expression )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        RANGE_SINGLE96 = (CommonTree) match(input, RANGE_SINGLE, FOLLOW_RANGE_SINGLE_in_range2150);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            RANGE_SINGLE96_tree = (CommonTree) adaptor.dupNode(RANGE_SINGLE96);

                            root_1 = (CommonTree) adaptor.becomeRoot(RANGE_SINGLE96_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expression_in_range2154);
                        a = expression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptRange(VNScriptRangeType.SINGLE, a.r);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:338:6: ^( RANGE_LEFT a= expression (b= expression )? )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        RANGE_LEFT97 = (CommonTree) match(input, RANGE_LEFT, FOLLOW_RANGE_LEFT_in_range2170);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            RANGE_LEFT97_tree = (CommonTree) adaptor.dupNode(RANGE_LEFT97);

                            root_1 = (CommonTree) adaptor.becomeRoot(RANGE_LEFT97_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expression_in_range2174);
                        a = expression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptRange(VNScriptRangeType.LEFT, a.r);
                        }
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:340:3: (b= expression )?
                        int alt14 = 2;
                        int LA14_0 = input.LA(1);

                        if ((LA14_0 == EXPR)) {
                            alt14 = 1;
                        }
                        switch (alt14) {
                            case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:340:4: b= expression
                            {
                                _last = (CommonTree) input.LT(1);
                                pushFollow(FOLLOW_expression_in_range2184);
                                b = expression();

                                state._fsp--;
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    adaptor.addChild(root_1, b.getTree());
                                if (state.backtracking == 0) {
                                    retval.r = new VNScriptRange(VNScriptRangeType.BOTH, a.r, b.r);
                                }

                                if (state.backtracking == 0) {
                                }
                            }
                            break;

                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:344:5: ^( RANGE_RIGHT a= expression )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        RANGE_RIGHT98 = (CommonTree) match(input, RANGE_RIGHT, FOLLOW_RANGE_RIGHT_in_range2204);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            RANGE_RIGHT98_tree = (CommonTree) adaptor.dupNode(RANGE_RIGHT98);

                            root_1 = (CommonTree) adaptor.becomeRoot(RANGE_RIGHT98_tree, root_1);
                        }

                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        _last = (CommonTree) input.LT(1);
                        pushFollow(FOLLOW_expression_in_range2208);
                        a = expression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_1, a.getTree());
                        if (state.backtracking == 0) {
                            retval.r = new VNScriptRange(VNScriptRangeType.RIGHT, a.r);
                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 5: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:347:5: ^( RANGE_ALL )
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    {
                        CommonTree _save_last_1 = _last;
                        CommonTree _first_1 = null;
                        CommonTree root_1 = (CommonTree) adaptor.nil();
                        _last = (CommonTree) input.LT(1);
                        RANGE_ALL99 = (CommonTree) match(input, RANGE_ALL, FOLLOW_RANGE_ALL_in_range2221);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            RANGE_ALL99_tree = (CommonTree) adaptor.dupNode(RANGE_ALL99);

                            root_1 = (CommonTree) adaptor.becomeRoot(RANGE_ALL99_tree, root_1);
                        }

                        if (state.backtracking == 0) {
                            retval.r = new VNScriptRange(VNScriptRangeType.ALL);
                        }

                        if (input.LA(1) == Token.DOWN) {
                            match(input, Token.DOWN, null);
                            if (state.failed)
                                return retval;
                            match(input, Token.UP, null);
                            if (state.failed)
                                return retval;
                        }
                        adaptor.addChild(root_0, root_1);
                        _last = _save_last_1;
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;

            }
            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 11, range_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "range"
    public static class arguments_return extends TreeRuleReturnScope
    {

        public VNScriptExpr[] r;
        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "arguments"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:352:1: arguments returns [ VNScriptExpr[] r ] : ^( ARGUMENT_LIST (e= expression )* ) ;
    public final VNScriptTree.arguments_return arguments() throws RecognitionException
    {
        VNScriptTree.arguments_return retval = new VNScriptTree.arguments_return();
        retval.start = input.LT(1);
        int arguments_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree ARGUMENT_LIST100 = null;
        VNScriptTree.expression_return e = null;

        CommonTree ARGUMENT_LIST100_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 12)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:353:5: ( ^( ARGUMENT_LIST (e= expression )* ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:353:9: ^( ARGUMENT_LIST (e= expression )* )
            {
                root_0 = (CommonTree) adaptor.nil();

                _last = (CommonTree) input.LT(1);
                {
                    CommonTree _save_last_1 = _last;
                    CommonTree _first_1 = null;
                    CommonTree root_1 = (CommonTree) adaptor.nil();
                    _last = (CommonTree) input.LT(1);
                    ARGUMENT_LIST100 = (CommonTree) match(input, ARGUMENT_LIST, FOLLOW_ARGUMENT_LIST_in_arguments2258);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        ARGUMENT_LIST100_tree = (CommonTree) adaptor.dupNode(ARGUMENT_LIST100);

                        root_1 = (CommonTree) adaptor.becomeRoot(ARGUMENT_LIST100_tree, root_1);
                    }

                    if (state.backtracking == 0) {
                        args = new ArrayList<VNScriptExpr>();
                    }

                    if (input.LA(1) == Token.DOWN) {
                        match(input, Token.DOWN, null);
                        if (state.failed)
                            return retval;
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:356:5: (e= expression )*
                        loop16:
                        do {
                            int alt16 = 2;
                            int LA16_0 = input.LA(1);

                            if ((LA16_0 == EXPR)) {
                                alt16 = 1;
                            }

                            switch (alt16) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:358:5: e= expression
                                {
                                    _last = (CommonTree) input.LT(1);
                                    pushFollow(FOLLOW_expression_in_arguments2289);
                                    e = expression();

                                    state._fsp--;
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        adaptor.addChild(root_1, e.getTree());
                                    if (state.backtracking == 0) {
                                        args.add((e != null ? e.r : null));
                                    }

                                    if (state.backtracking == 0) {
                                    }
                                }
                                break;

                                default:
                                    break loop16;
                            }
                        } while (true);

                        if (state.backtracking == 0) {
                            VNScriptExpr[] result = new VNScriptExpr[args.size()];
                            retval.r = args.toArray(result);

                        }

                        match(input, Token.UP, null);
                        if (state.failed)
                            return retval;
                    }
                    adaptor.addChild(root_0, root_1);
                    _last = _save_last_1;
                }

                if (state.backtracking == 0) {
                }
            }

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 12, arguments_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "arguments"
    public static class literal_return extends TreeRuleReturnScope
    {

        public VNScriptExpr r;
        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "literal"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:371:1: literal returns [ VNScriptExpr r ] : ( HEX_LITERAL | OCTAL_LITERAL | a= DECIMAL_LITERAL | b= FLOATING_POINT_LITERAL | CHARACTER_LITERAL | STRING_LITERAL | TRUE | FALSE | NULL );
    public final VNScriptTree.literal_return literal() throws RecognitionException
    {
        VNScriptTree.literal_return retval = new VNScriptTree.literal_return();
        retval.start = input.LT(1);
        int literal_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree a = null;
        CommonTree b = null;
        CommonTree HEX_LITERAL101 = null;
        CommonTree OCTAL_LITERAL102 = null;
        CommonTree CHARACTER_LITERAL103 = null;
        CommonTree STRING_LITERAL104 = null;
        CommonTree TRUE105 = null;
        CommonTree FALSE106 = null;
        CommonTree NULL107 = null;

        CommonTree a_tree = null;
        CommonTree b_tree = null;
        CommonTree HEX_LITERAL101_tree = null;
        CommonTree OCTAL_LITERAL102_tree = null;
        CommonTree CHARACTER_LITERAL103_tree = null;
        CommonTree STRING_LITERAL104_tree = null;
        CommonTree TRUE105_tree = null;
        CommonTree FALSE106_tree = null;
        CommonTree NULL107_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 13)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:372:5: ( HEX_LITERAL | OCTAL_LITERAL | a= DECIMAL_LITERAL | b= FLOATING_POINT_LITERAL | CHARACTER_LITERAL | STRING_LITERAL | TRUE | FALSE | NULL )
            int alt17 = 9;
            switch (input.LA(1)) {
                case HEX_LITERAL: {
                    alt17 = 1;
                }
                break;
                case OCTAL_LITERAL: {
                    alt17 = 2;
                }
                break;
                case DECIMAL_LITERAL: {
                    alt17 = 3;
                }
                break;
                case FLOATING_POINT_LITERAL: {
                    alt17 = 4;
                }
                break;
                case CHARACTER_LITERAL: {
                    alt17 = 5;
                }
                break;
                case STRING_LITERAL: {
                    alt17 = 6;
                }
                break;
                case TRUE: {
                    alt17 = 7;
                }
                break;
                case FALSE: {
                    alt17 = 8;
                }
                break;
                case NULL: {
                    alt17 = 9;
                }
                break;
                default:
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 17, 0, input);

                    throw nvae;
            }

            switch (alt17) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:372:9: HEX_LITERAL
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    HEX_LITERAL101 = (CommonTree) match(input, HEX_LITERAL, FOLLOW_HEX_LITERAL_in_literal2362);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        HEX_LITERAL101_tree = (CommonTree) adaptor.dupNode(HEX_LITERAL101);

                        adaptor.addChild(root_0, HEX_LITERAL101_tree);
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:373:9: OCTAL_LITERAL
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    OCTAL_LITERAL102 = (CommonTree) match(input, OCTAL_LITERAL, FOLLOW_OCTAL_LITERAL_in_literal2372);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        OCTAL_LITERAL102_tree = (CommonTree) adaptor.dupNode(OCTAL_LITERAL102);

                        adaptor.addChild(root_0, OCTAL_LITERAL102_tree);
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:374:9: a= DECIMAL_LITERAL
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    a = (CommonTree) match(input, DECIMAL_LITERAL, FOLLOW_DECIMAL_LITERAL_in_literal2384);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        a_tree = (CommonTree) adaptor.dupNode(a);

                        adaptor.addChild(root_0, a_tree);
                    }
                    if (state.backtracking == 0) {
                        retval.r = new VNScriptLiteral((a != null ? a.getText() : null));
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:376:9: b= FLOATING_POINT_LITERAL
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    b = (CommonTree) match(input, FLOATING_POINT_LITERAL, FOLLOW_FLOATING_POINT_LITERAL_in_literal2402);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        b_tree = (CommonTree) adaptor.dupNode(b);

                        adaptor.addChild(root_0, b_tree);
                    }
                    if (state.backtracking == 0) {
                        retval.r = new VNScriptLiteral((b != null ? b.getText() : null));
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 5: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:378:9: CHARACTER_LITERAL
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    CHARACTER_LITERAL103 = (CommonTree) match(input, CHARACTER_LITERAL, FOLLOW_CHARACTER_LITERAL_in_literal2418);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        CHARACTER_LITERAL103_tree = (CommonTree) adaptor.dupNode(CHARACTER_LITERAL103);

                        adaptor.addChild(root_0, CHARACTER_LITERAL103_tree);
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 6: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:379:9: STRING_LITERAL
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    STRING_LITERAL104 = (CommonTree) match(input, STRING_LITERAL, FOLLOW_STRING_LITERAL_in_literal2428);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        STRING_LITERAL104_tree = (CommonTree) adaptor.dupNode(STRING_LITERAL104);

                        adaptor.addChild(root_0, STRING_LITERAL104_tree);
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 7: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:380:9: TRUE
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    TRUE105 = (CommonTree) match(input, TRUE, FOLLOW_TRUE_in_literal2438);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        TRUE105_tree = (CommonTree) adaptor.dupNode(TRUE105);

                        adaptor.addChild(root_0, TRUE105_tree);
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 8: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:381:9: FALSE
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    FALSE106 = (CommonTree) match(input, FALSE, FOLLOW_FALSE_in_literal2448);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        FALSE106_tree = (CommonTree) adaptor.dupNode(FALSE106);

                        adaptor.addChild(root_0, FALSE106_tree);
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;
                case 9: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:382:9: NULL
                {
                    root_0 = (CommonTree) adaptor.nil();

                    _last = (CommonTree) input.LT(1);
                    NULL107 = (CommonTree) match(input, NULL, FOLLOW_NULL_in_literal2458);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        NULL107_tree = (CommonTree) adaptor.dupNode(NULL107);

                        adaptor.addChild(root_0, NULL107_tree);
                    }

                    if (state.backtracking == 0) {
                    }
                }
                break;

            }
            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 13, literal_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "literal"
    public static class primitiveType_return extends TreeRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "primitiveType"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:385:1: primitiveType : ( BOOLEAN | CHAR | BYTE | SHORT | INT | LONG | FLOAT | DOUBLE );
    public final VNScriptTree.primitiveType_return primitiveType() throws RecognitionException
    {
        VNScriptTree.primitiveType_return retval = new VNScriptTree.primitiveType_return();
        retval.start = input.LT(1);
        int primitiveType_StartIndex = input.index();
        CommonTree root_0 = null;

        CommonTree _first_0 = null;
        CommonTree _last = null;

        CommonTree set108 = null;

        CommonTree set108_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 14)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:386:5: ( BOOLEAN | CHAR | BYTE | SHORT | INT | LONG | FLOAT | DOUBLE )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:
            {
                root_0 = (CommonTree) adaptor.nil();

                _last = (CommonTree) input.LT(1);
                set108 = (CommonTree) input.LT(1);
                if (input.LA(1) == BOOLEAN || input.LA(1) == BYTE || input.LA(1) == CHAR || input.LA(1) == DOUBLE || input.LA(1) == FLOAT || (input.LA(1) >= INT && input.LA(1) <= LONG) || input.LA(1) == SHORT) {
                    input.consume();

                    if (state.backtracking == 0) {
                        set108_tree = (CommonTree) adaptor.dupNode(set108);

                        adaptor.addChild(root_0, set108_tree);
                    }
                    state.errorRecovery = false;
                    state.failed = false;
                } else {
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    throw mse;
                }

                if (state.backtracking == 0) {
                }

            }

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 14, primitiveType_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "primitiveType"
    // $ANTLR start synpred12_VNScriptTree
    public final void synpred12_VNScriptTree_fragment() throws RecognitionException
    {
        VNScriptTree.expression_return a = null;

        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:139:21: ( (a= expression )* )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:139:21: (a= expression )*
        {
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:139:22: (a= expression )*
            loop19:
            do {
                int alt19 = 2;
                int LA19_0 = input.LA(1);

                if ((LA19_0 == EXPR)) {
                    alt19 = 1;
                }

                switch (alt19) {
                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScriptTree.g:0:0: a= expression
                    {
                        pushFollow(FOLLOW_expression_in_synpred12_VNScriptTree497);
                        a = expression();

                        state._fsp--;
                        if (state.failed)
                            return;

                    }
                    break;

                    default:
                        break loop19;
                }
            } while (true);

        }
    }

    // $ANTLR end synpred12_VNScriptTree
    // Delegated rules
    public final boolean synpred12_VNScriptTree()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred12_VNScriptTree_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public static final BitSet FOLLOW_BLOCK_SCOPE_in_program97 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_statement_in_program99 = new BitSet(new long[]{0x0000100000000008L, 0x4020008000000601L, 0x0000020000000000L});
    public static final BitSet FOLLOW_BLOCK_SCOPE_in_block130 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_statement_in_block147 = new BitSet(new long[]{0x0000100000000008L, 0x4020008000000601L, 0x0000020000000000L});
    public static final BitSet FOLLOW_block_in_statement184 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IF_in_statement199 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_parenthesizedExpression_in_statement203 = new BitSet(new long[]{0x0000100000000008L, 0x4020008000000601L, 0x0000020000000000L});
    public static final BitSet FOLLOW_statement_in_statement216 = new BitSet(new long[]{0x0000100000000008L, 0x4020008000000601L, 0x0000020000000000L});
    public static final BitSet FOLLOW_statement_in_statement234 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_FOR_in_statement253 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_forInit_in_statement266 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000000000000002L});
    public static final BitSet FOLLOW_forCondition_in_statement283 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000000000000010L});
    public static final BitSet FOLLOW_forUpdater_in_statement301 = new BitSet(new long[]{0x0000100000000008L, 0x4020008000000601L, 0x0000020000000000L});
    public static final BitSet FOLLOW_statement_in_statement314 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_WHILE_in_statement339 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_parenthesizedExpression_in_statement349 = new BitSet(new long[]{0x0000100000000008L, 0x4020008000000601L, 0x0000020000000000L});
    public static final BitSet FOLLOW_statement_in_statement366 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_DO_in_statement379 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_statement_in_statement386 = new BitSet(new long[]{0x0000000000000000L, 0x4000000000000000L});
    public static final BitSet FOLLOW_parenthesizedExpression_in_statement396 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_expression_in_statement424 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SEMI_in_statement444 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DECLARATION_in_statement458 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_IDENT_in_statement462 = new BitSet(new long[]{0x0000000000000000L, 0x4000000000000000L});
    public static final BitSet FOLLOW_expression_in_statement466 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_FOR_INIT_in_forInit492 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_forInit497 = new BitSet(new long[]{0x0000000000000008L, 0x4000000000000000L});
    public static final BitSet FOLLOW_FOR_INIT_in_forInit531 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_DECLARATION_in_forInit534 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_IDENT_in_forInit538 = new BitSet(new long[]{0x0000000000000000L, 0x4000000000000000L});
    public static final BitSet FOLLOW_expression_in_forInit542 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_FOR_CONDITION_in_forCondition575 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_forCondition580 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_FOR_UPDATE_in_forUpdater626 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_forUpdater631 = new BitSet(new long[]{0x0000000000000008L, 0x4000000000000000L});
    public static final BitSet FOLLOW_expression_in_parenthesizedExpression680 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_EXPR_in_expression720 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expression724 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_ASSIGN_in_expr768 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr772 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr776 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PLUS_ASSIGN_in_expr795 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr799 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr803 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MINUS_ASSIGN_in_expr822 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr826 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr830 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_STAR_ASSIGN_in_expr849 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr853 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr857 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_DIV_ASSIGN_in_expr876 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr880 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr884 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_AND_ASSIGN_in_expr903 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr905 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr907 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_OR_ASSIGN_in_expr919 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr921 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr923 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_XOR_ASSIGN_in_expr935 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr937 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr939 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MOD_ASSIGN_in_expr951 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr955 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr959 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_BIT_SHIFT_RIGHT_ASSIGN_in_expr978 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr980 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr982 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_SHIFT_RIGHT_ASSIGN_in_expr994 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr996 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr998 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_SHIFT_LEFT_ASSIGN_in_expr1010 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1012 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1014 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_QUESTION_in_expr1026 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1030 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1034 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1038 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LOGICAL_OR_in_expr1065 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1070 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1074 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LOGICAL_AND_in_expr1101 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1105 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1109 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_OR_in_expr1132 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1134 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1136 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_XOR_in_expr1148 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1150 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1152 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_AND_in_expr1164 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1166 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1168 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_EQUAL_in_expr1180 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1184 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1188 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NOT_EQUAL_in_expr1211 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1215 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1219 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LESS_OR_EQUAL_in_expr1242 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1246 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1250 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_GREATER_OR_EQUAL_in_expr1273 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1277 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1281 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_BIT_SHIFT_RIGHT_in_expr1304 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1306 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1308 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_SHIFT_RIGHT_in_expr1320 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1322 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1324 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_GREATER_THAN_in_expr1336 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1340 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1344 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_SHIFT_LEFT_in_expr1367 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1369 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1371 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LESS_THAN_in_expr1383 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1387 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1391 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PLUS_in_expr1414 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1418 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1422 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MINUS_in_expr1447 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1451 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1455 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_STAR_in_expr1478 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1482 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1486 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_DIV_in_expr1509 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1513 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1517 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MOD_in_expr1540 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1544 = new BitSet(new long[]{0x001FE1FFDF1CE370L, 0x4008000800080020L, 0x0001FC00C0790000L});
    public static final BitSet FOLLOW_expr_in_expr1548 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_UNARY_PLUS_in_expr1571 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1575 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_UNARY_MINUS_in_expr1598 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1602 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PRE_INC_in_expr1625 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1629 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PRE_DEC_in_expr1652 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1656 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_POST_INC_in_expr1679 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1683 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_POST_DEC_in_expr1706 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1710 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NOT_in_expr1733 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1735 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LOGICAL_NOT_in_expr1758 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr1760 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_primaryExpression_in_expr1784 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DOT_in_primaryExpression1829 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_primaryExpression_in_primaryExpression1849 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_IDENT_in_primaryExpression1872 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_parenthesizedExpression_in_primaryExpression1928 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_primaryExpression1946 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_METHOD_CALL_in_primaryExpression1967 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_IDENT_in_primaryExpression1971 = new BitSet(new long[]{0x0000000000000000L, 0x0001000000000000L});
    public static final BitSet FOLLOW_arguments_in_primaryExpression1975 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_ARRAY_ELEMENT_ACCESS_in_primaryExpression2006 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_IDENT_in_primaryExpression2010 = new BitSet(new long[]{0x0000000000000008L, 0x0000000000000000L, 0x000005F000000000L});
    public static final BitSet FOLLOW_range_in_primaryExpression2030 = new BitSet(new long[]{0x0000000000000008L, 0x0000000000000000L, 0x000005F000000000L});
    public static final BitSet FOLLOW_IDENT_in_primaryExpression2047 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_literal_in_primaryExpression2098 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RANGE_OFFSET_in_range2133 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_range2137 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_RANGE_SINGLE_in_range2150 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_range2154 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_RANGE_LEFT_in_range2170 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_range2174 = new BitSet(new long[]{0x0000000000000008L, 0x4000000000000000L});
    public static final BitSet FOLLOW_expression_in_range2184 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_RANGE_RIGHT_in_range2204 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_range2208 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_RANGE_ALL_in_range2221 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ARGUMENT_LIST_in_arguments2258 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_arguments2289 = new BitSet(new long[]{0x0000000000000008L, 0x4000000000000000L});
    public static final BitSet FOLLOW_HEX_LITERAL_in_literal2362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OCTAL_LITERAL_in_literal2372 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DECIMAL_LITERAL_in_literal2384 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FLOATING_POINT_LITERAL_in_literal2402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CHARACTER_LITERAL_in_literal2418 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STRING_LITERAL_in_literal2428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TRUE_in_literal2438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FALSE_in_literal2448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NULL_in_literal2458 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_primitiveType0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_synpred12_VNScriptTree497 = new BitSet(new long[]{0x0000000000000002L, 0x4000000000000000L});

}
