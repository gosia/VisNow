/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript;

import pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets.VNScriptLog;
import pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets.LogMessage;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;

public class VNScriptParser extends Parser
{

    public static final String[] tokenNames = new String[]{
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "AND_ASSIGN", "ASSIGN", "AT", "BIT_SHIFT_RIGHT", "BIT_SHIFT_RIGHT_ASSIGN", "COLON", "COMMA", "DEC", "DIV", "DIV_ASSIGN", "DOT", "DOTSTAR", "ELLIPSIS", "EQUAL", "GREATER_OR_EQUAL", "GREATER_THAN", "INC", "LBRACK", "LCURLY", "LESS_OR_EQUAL", "LESS_THAN", "LOGICAL_AND", "LOGICAL_NOT", "LOGICAL_OR", "LPAREN", "MINUS", "MINUS_ASSIGN", "MOD", "MOD_ASSIGN", "NOT", "NOT_EQUAL", "OR", "OR_ASSIGN", "PLUS", "PLUS_ASSIGN", "QUESTION", "RBRACK", "RCURLY", "RPAREN", "SEMI", "SHIFT_LEFT", "SHIFT_LEFT_ASSIGN", "SHIFT_RIGHT", "SHIFT_RIGHT_ASSIGN", "STAR", "STAR_ASSIGN", "XOR", "XOR_ASSIGN", "ABSTRACT", "ASSERT", "BOOLEAN", "BREAK", "BYTE", "CASE", "CATCH", "CHAR", "CLASS", "CONTINUE", "DEFAULT", "DO", "DOUBLE", "ELSE", "ENUM", "EXTENDS", "FALSE", "FINAL", "FINALLY", "FLOAT", "FOR", "IF", "IMPLEMENTS", "INSTANCEOF", "INTERFACE", "IMPORT", "INT", "LONG", "NATIVE", "NEW", "NULL", "PACKAGE", "PRIVATE", "PROTECTED", "PUBLIC", "RETURN", "SHORT", "STATIC", "STRICTFP", "SUPER", "SWITCH", "SYNCHRONIZED", "THIS", "THROW", "THROWS", "TRANSIENT", "TRUE", "TRY", "VOID", "VOLATILE", "WHILE", "ANNOTATION_INIT_ARRAY_ELEMENT", "ANNOTATION_INIT_BLOCK", "ANNOTATION_INIT_DEFAULT_KEY", "ANNOTATION_INIT_KEY_LIST", "ANNOTATION_LIST", "ANNOTATION_METHOD_DECL", "ANNOTATION_SCOPE", "ANNOTATION_TOP_LEVEL_SCOPE", "ARGUMENT_LIST", "ARRAY_DECLARATOR", "ARRAY_DECLARATOR_LIST", "ARRAY_ELEMENT_ACCESS", "ARRAY_INITIALIZER", "BLOCK_SCOPE", "CAST_EXPR", "CATCH_CLAUSE_LIST", "CLASS_CONSTRUCTOR_CALL", "CLASS_INSTANCE_INITIALIZER", "CLASS_STATIC_INITIALIZER", "CLASS_TOP_LEVEL_SCOPE", "CONSTRUCTOR_DECL", "ENUM_TOP_LEVEL_SCOPE", "EXPR", "EXTENDS_BOUND_LIST", "EXTENDS_CLAUSE", "FOR_CONDITION", "FOR_EACH", "FOR_INIT", "FOR_UPDATE", "FORMAL_PARAM_LIST", "FORMAL_PARAM_STD_DECL", "FORMAL_PARAM_VARARG_DECL", "FUNCTION_METHOD_DECL", "GENERIC_TYPE_ARG_LIST", "GENERIC_TYPE_PARAM_LIST", "INTERFACE_TOP_LEVEL_SCOPE", "IMPLEMENTS_CLAUSE", "LABELED_STATEMENT", "LOCAL_MODIFIER_LIST", "JAVA_SOURCE", "METHOD_CALL", "MODIFIER_LIST", "PARENTESIZED_EXPR", "POST_DEC", "POST_INC", "PRE_DEC", "PRE_INC", "QUALIFIED_TYPE_IDENT", "STATIC_ARRAY_CREATOR", "SUPER_CONSTRUCTOR_CALL", "SWITCH_BLOCK_LABEL_LIST", "THIS_CONSTRUCTOR_CALL", "THROWS_CLAUSE", "TYPE", "UNARY_MINUS", "UNARY_PLUS", "VAR_DECLARATION", "VAR_DECLARATOR", "VAR_DECLARATOR_LIST", "VOID_METHOD_DECL", "RANGE_LEFT", "RANGE_RIGHT", "RANGE_ALL", "RANGE_OFFSET", "RANGE_SINGLE", "DECLARATION", "IDENT", "HEX_LITERAL", "OCTAL_LITERAL", "DECIMAL_LITERAL", "FLOATING_POINT_LITERAL", "CHARACTER_LITERAL", "STRING_LITERAL", "HEX_DIGIT", "INTEGER_TYPE_SUFFIX", "EXPONENT", "FLOAT_TYPE_SUFFIX", "ESCAPE_SEQUENCE", "UNICODE_ESCAPE", "OCTAL_ESCAPE", "JAVA_ID_START", "JAVA_ID_PART", "WS", "COMMENT", "LINE_COMMENT", "':='"
    };
    public static final int PACKAGE = 84;
    public static final int STAR = 49;
    public static final int MOD = 32;
    public static final int DO = 64;
    public static final int GENERIC_TYPE_PARAM_LIST = 138;
    public static final int NOT = 34;
    public static final int EOF = -1;
    public static final int ANNOTATION_METHOD_DECL = 109;
    public static final int BIT_SHIFT_RIGHT_ASSIGN = 9;
    public static final int UNARY_PLUS = 159;
    public static final int TYPE = 157;
    public static final int FINAL = 70;
    public static final int RPAREN = 43;
    public static final int INC = 21;
    public static final int RANGE_LEFT = 164;
    public static final int IMPORT = 78;
    public static final int STRING_LITERAL = 176;
    public static final int CAST_EXPR = 118;
    public static final int NOT_EQUAL = 35;
    public static final int RETURN = 88;
    public static final int THIS = 95;
    public static final int ENUM_TOP_LEVEL_SCOPE = 125;
    public static final int ANNOTATION_INIT_KEY_LIST = 107;
    public static final int RBRACK = 41;
    public static final int PRE_DEC = 149;
    public static final int SWITCH_BLOCK_LABEL_LIST = 154;
    public static final int STATIC = 90;
    public static final int ELSE = 66;
    public static final int MINUS_ASSIGN = 31;
    public static final int STRICTFP = 91;
    public static final int NATIVE = 81;
    public static final int ELLIPSIS = 17;
    public static final int PRE_INC = 150;
    public static final int CHARACTER_LITERAL = 175;
    public static final int LCURLY = 23;
    public static final int UNARY_MINUS = 158;
    public static final int OCTAL_ESCAPE = 183;
    public static final int INT = 79;
    public static final int FORMAL_PARAM_VARARG_DECL = 135;
    public static final int INTERFACE_TOP_LEVEL_SCOPE = 139;
    public static final int WS = 186;
    public static final int LOCAL_MODIFIER_LIST = 142;
    public static final int LESS_THAN = 25;
    public static final int EXTENDS_BOUND_LIST = 127;
    public static final int RANGE_OFFSET = 167;
    public static final int DECIMAL_LITERAL = 173;
    public static final int FOR_INIT = 131;
    public static final int PROTECTED = 86;
    public static final int LBRACK = 22;
    public static final int THIS_CONSTRUCTOR_CALL = 155;
    public static final int FLOAT = 72;
    public static final int POST_DEC = 147;
    public static final int DECLARATION = 169;
    public static final int ANNOTATION_SCOPE = 110;
    public static final int STATIC_ARRAY_CREATOR = 152;
    public static final int LPAREN = 29;
    public static final int AT = 7;
    public static final int IMPLEMENTS = 75;
    public static final int XOR_ASSIGN = 52;
    public static final int LOGICAL_OR = 28;
    public static final int IDENT = 170;
    public static final int PLUS = 38;
    public static final int ANNOTATION_INIT_BLOCK = 105;
    public static final int GENERIC_TYPE_ARG_LIST = 137;
    public static final int JAVA_ID_PART = 185;
    public static final int GREATER_THAN = 20;
    public static final int LESS_OR_EQUAL = 24;
    public static final int CLASS_STATIC_INITIALIZER = 122;
    public static final int HEX_DIGIT = 177;
    public static final int SHORT = 89;
    public static final int INSTANCEOF = 76;
    public static final int MINUS = 30;
    public static final int RANGE_SINGLE = 168;
    public static final int SEMI = 44;
    public static final int STAR_ASSIGN = 50;
    public static final int VAR_DECLARATOR_LIST = 162;
    public static final int COLON = 10;
    public static final int OR_ASSIGN = 37;
    public static final int ENUM = 67;
    public static final int RCURLY = 42;
    public static final int PLUS_ASSIGN = 39;
    public static final int FUNCTION_METHOD_DECL = 136;
    public static final int INTERFACE = 77;
    public static final int DIV = 13;
    public static final int POST_INC = 148;
    public static final int LONG = 80;
    public static final int CLASS_CONSTRUCTOR_CALL = 120;
    public static final int PUBLIC = 87;
    public static final int ARRAY_INITIALIZER = 116;
    public static final int CATCH_CLAUSE_LIST = 119;
    public static final int SUPER_CONSTRUCTOR_CALL = 153;
    public static final int EXPONENT = 179;
    public static final int WHILE = 103;
    public static final int MOD_ASSIGN = 33;
    public static final int CASE = 58;
    public static final int NEW = 82;
    public static final int CHAR = 60;
    public static final int CLASS_INSTANCE_INITIALIZER = 121;
    public static final int ARRAY_ELEMENT_ACCESS = 115;
    public static final int FOR_CONDITION = 129;
    public static final int VAR_DECLARATION = 160;
    public static final int DIV_ASSIGN = 14;
    public static final int BREAK = 56;
    public static final int LOGICAL_AND = 26;
    public static final int FOR_UPDATE = 132;
    public static final int FLOATING_POINT_LITERAL = 174;
    public static final int VOID_METHOD_DECL = 163;
    public static final int DOUBLE = 65;
    public static final int VOID = 101;
    public static final int SUPER = 92;
    public static final int COMMENT = 187;
    public static final int FLOAT_TYPE_SUFFIX = 180;
    public static final int JAVA_ID_START = 184;
    public static final int IMPLEMENTS_CLAUSE = 140;
    public static final int LINE_COMMENT = 188;
    public static final int PRIVATE = 85;
    public static final int BLOCK_SCOPE = 117;
    public static final int SWITCH = 93;
    public static final int ANNOTATION_INIT_DEFAULT_KEY = 106;
    public static final int NULL = 83;
    public static final int VAR_DECLARATOR = 161;
    public static final int ANNOTATION_LIST = 108;
    public static final int THROWS = 97;
    public static final int ASSERT = 54;
    public static final int METHOD_CALL = 144;
    public static final int TRY = 100;
    public static final int SHIFT_LEFT = 45;
    public static final int SHIFT_RIGHT = 47;
    public static final int FORMAL_PARAM_STD_DECL = 134;
    public static final int RANGE_RIGHT = 165;
    public static final int OR = 36;
    public static final int SHIFT_RIGHT_ASSIGN = 48;
    public static final int JAVA_SOURCE = 143;
    public static final int CATCH = 59;
    public static final int FALSE = 69;
    public static final int RANGE_ALL = 166;
    public static final int INTEGER_TYPE_SUFFIX = 178;
    public static final int THROW = 96;
    public static final int DEC = 12;
    public static final int CLASS = 61;
    public static final int BIT_SHIFT_RIGHT = 8;
    public static final int THROWS_CLAUSE = 156;
    public static final int GREATER_OR_EQUAL = 19;
    public static final int FOR = 73;
    public static final int LOGICAL_NOT = 27;
    public static final int ABSTRACT = 53;
    public static final int AND = 4;
    public static final int AND_ASSIGN = 5;
    public static final int MODIFIER_LIST = 145;
    public static final int IF = 74;
    public static final int CONSTRUCTOR_DECL = 124;
    public static final int ESCAPE_SEQUENCE = 181;
    public static final int LABELED_STATEMENT = 141;
    public static final int UNICODE_ESCAPE = 182;
    public static final int BOOLEAN = 55;
    public static final int SYNCHRONIZED = 94;
    public static final int EXPR = 126;
    public static final int CLASS_TOP_LEVEL_SCOPE = 123;
    public static final int CONTINUE = 62;
    public static final int COMMA = 11;
    public static final int TRANSIENT = 98;
    public static final int EQUAL = 18;
    public static final int ARGUMENT_LIST = 112;
    public static final int QUALIFIED_TYPE_IDENT = 151;
    public static final int HEX_LITERAL = 171;
    public static final int DOT = 15;
    public static final int SHIFT_LEFT_ASSIGN = 46;
    public static final int FORMAL_PARAM_LIST = 133;
    public static final int DOTSTAR = 16;
    public static final int ANNOTATION_TOP_LEVEL_SCOPE = 111;
    public static final int BYTE = 57;
    public static final int XOR = 51;
    public static final int T__189 = 189;
    public static final int VOLATILE = 102;
    public static final int PARENTESIZED_EXPR = 146;
    public static final int ARRAY_DECLARATOR_LIST = 114;
    public static final int DEFAULT = 63;
    public static final int OCTAL_LITERAL = 172;
    public static final int TRUE = 99;
    public static final int EXTENDS_CLAUSE = 128;
    public static final int ARRAY_DECLARATOR = 113;
    public static final int QUESTION = 40;
    public static final int FINALLY = 71;
    public static final int ASSIGN = 6;
    public static final int ANNOTATION_INIT_ARRAY_ELEMENT = 104;
    public static final int EXTENDS = 68;
    public static final int FOR_EACH = 130;

    // delegates
    // delegators
    public VNScriptParser(TokenStream input)
    {
        this(input, new RecognizerSharedState());
    }

    public VNScriptParser(TokenStream input, RecognizerSharedState state)
    {
        super(input, state);
        this.state.ruleMemo = new HashMap[162 + 1];

    }

    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor)
    {
        this.adaptor = adaptor;
    }

    public TreeAdaptor getTreeAdaptor()
    {
        return adaptor;
    }

    public String[] getTokenNames()
    {
        return VNScriptParser.tokenNames;
    }

    public String getGrammarFileName()
    {
        return "/home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g";
    }

    private VNScriptLog log;

    public VNScriptLog getLog()
    {
        return log;
    }

    public void setLog(VNScriptLog log)
    {
        this.log = log;
    }

    @Override
    public void reportError(RecognitionException e)
    {
        super.reportError(e);
        log.addMessage(new LogMessage(e.toString(), Integer.toString(e.line), LogMessage.LogMessageType.ERROR));
    }

    public static class program_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "program"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:222:1: program : ( statement )* -> ^( BLOCK_SCOPE ( statement )* ) ;
    public final VNScriptParser.program_return program() throws RecognitionException
    {
        VNScriptParser.program_return retval = new VNScriptParser.program_return();
        retval.start = input.LT(1);
        int program_StartIndex = input.index();
        Tree root_0 = null;

        VNScriptParser.statement_return statement1 = null;

        RewriteRuleSubtreeStream stream_statement = new RewriteRuleSubtreeStream(adaptor, "rule statement");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 1)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:222:9: ( ( statement )* -> ^( BLOCK_SCOPE ( statement )* ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:222:12: ( statement )*
            {
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:222:12: ( statement )*
                loop1:
                do {
                    int alt1 = 2;
                    int LA1_0 = input.LA(1);

                    if ((LA1_0 == DEC || LA1_0 == INC || LA1_0 == LCURLY || LA1_0 == LESS_THAN || LA1_0 == LOGICAL_NOT || (LA1_0 >= LPAREN && LA1_0 <= MINUS) || LA1_0 == NOT || LA1_0 == PLUS || LA1_0 == SEMI || LA1_0 == BOOLEAN || LA1_0 == BYTE || LA1_0 == CHAR || (LA1_0 >= DO && LA1_0 <= DOUBLE) || LA1_0 == FALSE || (LA1_0 >= FLOAT && LA1_0 <= IF) || (LA1_0 >= INT && LA1_0 <= LONG) || LA1_0 == NULL || LA1_0 == SHORT || LA1_0 == SUPER || LA1_0 == THIS || LA1_0 == TRUE || LA1_0 == VOID || LA1_0 == WHILE || (LA1_0 >= IDENT && LA1_0 <= STRING_LITERAL))) {
                        alt1 = 1;
                    }

                    switch (alt1) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:0:0: statement
                        {
                            pushFollow(FOLLOW_statement_in_program4523);
                            statement1 = statement();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_statement.add(statement1.getTree());

                        }
                        break;

                        default:
                            break loop1;
                    }
                } while (true);

                // AST REWRITE
                // elements: statement
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (Tree) adaptor.nil();
                    // 223:9: -> ^( BLOCK_SCOPE ( statement )* )
                    {
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:223:13: ^( BLOCK_SCOPE ( statement )* )
                        {
                            Tree root_1 = (Tree) adaptor.nil();
                            root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(BLOCK_SCOPE, "BLOCK_SCOPE"), root_1);

                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:223:27: ( statement )*
                            while (stream_statement.hasNext()) {
                                adaptor.addChild(root_1, stream_statement.nextTree());

                            }
                            stream_statement.reset();

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 1, program_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "program"
    public static class block_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "block"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:225:1: block : LCURLY ( statement )* RCURLY -> ^( BLOCK_SCOPE[$LCURLY, \"BLOCK_SCOPE\"] ( statement )* ) ;
    public final VNScriptParser.block_return block() throws RecognitionException
    {
        VNScriptParser.block_return retval = new VNScriptParser.block_return();
        retval.start = input.LT(1);
        int block_StartIndex = input.index();
        Tree root_0 = null;

        Token LCURLY2 = null;
        Token RCURLY4 = null;
        VNScriptParser.statement_return statement3 = null;

        Tree LCURLY2_tree = null;
        Tree RCURLY4_tree = null;
        RewriteRuleTokenStream stream_LCURLY = new RewriteRuleTokenStream(adaptor, "token LCURLY");
        RewriteRuleTokenStream stream_RCURLY = new RewriteRuleTokenStream(adaptor, "token RCURLY");
        RewriteRuleSubtreeStream stream_statement = new RewriteRuleSubtreeStream(adaptor, "rule statement");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 2)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:226:5: ( LCURLY ( statement )* RCURLY -> ^( BLOCK_SCOPE[$LCURLY, \"BLOCK_SCOPE\"] ( statement )* ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:226:9: LCURLY ( statement )* RCURLY
            {
                LCURLY2 = (Token) match(input, LCURLY, FOLLOW_LCURLY_in_block4557);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_LCURLY.add(LCURLY2);

                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:226:16: ( statement )*
                loop2:
                do {
                    int alt2 = 2;
                    int LA2_0 = input.LA(1);

                    if ((LA2_0 == DEC || LA2_0 == INC || LA2_0 == LCURLY || LA2_0 == LESS_THAN || LA2_0 == LOGICAL_NOT || (LA2_0 >= LPAREN && LA2_0 <= MINUS) || LA2_0 == NOT || LA2_0 == PLUS || LA2_0 == SEMI || LA2_0 == BOOLEAN || LA2_0 == BYTE || LA2_0 == CHAR || (LA2_0 >= DO && LA2_0 <= DOUBLE) || LA2_0 == FALSE || (LA2_0 >= FLOAT && LA2_0 <= IF) || (LA2_0 >= INT && LA2_0 <= LONG) || LA2_0 == NULL || LA2_0 == SHORT || LA2_0 == SUPER || LA2_0 == THIS || LA2_0 == TRUE || LA2_0 == VOID || LA2_0 == WHILE || (LA2_0 >= IDENT && LA2_0 <= STRING_LITERAL))) {
                        alt2 = 1;
                    }

                    switch (alt2) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:0:0: statement
                        {
                            pushFollow(FOLLOW_statement_in_block4559);
                            statement3 = statement();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_statement.add(statement3.getTree());

                        }
                        break;

                        default:
                            break loop2;
                    }
                } while (true);

                RCURLY4 = (Token) match(input, RCURLY, FOLLOW_RCURLY_in_block4562);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_RCURLY.add(RCURLY4);

                // AST REWRITE
                // elements: statement
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (Tree) adaptor.nil();
                    // 227:9: -> ^( BLOCK_SCOPE[$LCURLY, \"BLOCK_SCOPE\"] ( statement )* )
                    {
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:227:13: ^( BLOCK_SCOPE[$LCURLY, \"BLOCK_SCOPE\"] ( statement )* )
                        {
                            Tree root_1 = (Tree) adaptor.nil();
                            root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(BLOCK_SCOPE, LCURLY2, "BLOCK_SCOPE"), root_1);

                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:227:51: ( statement )*
                            while (stream_statement.hasNext()) {
                                adaptor.addChild(root_1, stream_statement.nextTree());

                            }
                            stream_statement.reset();

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 2, block_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "block"
    public static class statement_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "statement"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:230:1: statement : ( block | IF parenthesizedExpression ifStat= statement ( ELSE elseStat= statement -> ^( IF parenthesizedExpression $ifStat $elseStat) | -> ^( IF parenthesizedExpression $ifStat) ) | FOR LPAREN forInit SEMI forCondition SEMI forUpdater RPAREN statement -> ^( FOR forInit forCondition forUpdater statement ) | WHILE parenthesizedExpression statement -> ^( WHILE parenthesizedExpression statement ) | DO statement WHILE parenthesizedExpression SEMI -> ^( DO statement parenthesizedExpression ) | expression SEMI | SEMI | declaration SEMI -> ^( DECLARATION declaration ) );
    public final VNScriptParser.statement_return statement() throws RecognitionException
    {
        VNScriptParser.statement_return retval = new VNScriptParser.statement_return();
        retval.start = input.LT(1);
        int statement_StartIndex = input.index();
        Tree root_0 = null;

        Token IF6 = null;
        Token ELSE8 = null;
        Token FOR9 = null;
        Token LPAREN10 = null;
        Token SEMI12 = null;
        Token SEMI14 = null;
        Token RPAREN16 = null;
        Token WHILE18 = null;
        Token DO21 = null;
        Token WHILE23 = null;
        Token SEMI25 = null;
        Token SEMI27 = null;
        Token SEMI28 = null;
        Token SEMI30 = null;
        VNScriptParser.statement_return ifStat = null;

        VNScriptParser.statement_return elseStat = null;

        VNScriptParser.block_return block5 = null;

        VNScriptParser.parenthesizedExpression_return parenthesizedExpression7 = null;

        VNScriptParser.forInit_return forInit11 = null;

        VNScriptParser.forCondition_return forCondition13 = null;

        VNScriptParser.forUpdater_return forUpdater15 = null;

        VNScriptParser.statement_return statement17 = null;

        VNScriptParser.parenthesizedExpression_return parenthesizedExpression19 = null;

        VNScriptParser.statement_return statement20 = null;

        VNScriptParser.statement_return statement22 = null;

        VNScriptParser.parenthesizedExpression_return parenthesizedExpression24 = null;

        VNScriptParser.expression_return expression26 = null;

        VNScriptParser.declaration_return declaration29 = null;

        Tree IF6_tree = null;
        Tree ELSE8_tree = null;
        Tree FOR9_tree = null;
        Tree LPAREN10_tree = null;
        Tree SEMI12_tree = null;
        Tree SEMI14_tree = null;
        Tree RPAREN16_tree = null;
        Tree WHILE18_tree = null;
        Tree DO21_tree = null;
        Tree WHILE23_tree = null;
        Tree SEMI25_tree = null;
        Tree SEMI27_tree = null;
        Tree SEMI28_tree = null;
        Tree SEMI30_tree = null;
        RewriteRuleTokenStream stream_DO = new RewriteRuleTokenStream(adaptor, "token DO");
        RewriteRuleTokenStream stream_FOR = new RewriteRuleTokenStream(adaptor, "token FOR");
        RewriteRuleTokenStream stream_RPAREN = new RewriteRuleTokenStream(adaptor, "token RPAREN");
        RewriteRuleTokenStream stream_WHILE = new RewriteRuleTokenStream(adaptor, "token WHILE");
        RewriteRuleTokenStream stream_SEMI = new RewriteRuleTokenStream(adaptor, "token SEMI");
        RewriteRuleTokenStream stream_LPAREN = new RewriteRuleTokenStream(adaptor, "token LPAREN");
        RewriteRuleTokenStream stream_IF = new RewriteRuleTokenStream(adaptor, "token IF");
        RewriteRuleTokenStream stream_ELSE = new RewriteRuleTokenStream(adaptor, "token ELSE");
        RewriteRuleSubtreeStream stream_statement = new RewriteRuleSubtreeStream(adaptor, "rule statement");
        RewriteRuleSubtreeStream stream_declaration = new RewriteRuleSubtreeStream(adaptor, "rule declaration");
        RewriteRuleSubtreeStream stream_forUpdater = new RewriteRuleSubtreeStream(adaptor, "rule forUpdater");
        RewriteRuleSubtreeStream stream_forCondition = new RewriteRuleSubtreeStream(adaptor, "rule forCondition");
        RewriteRuleSubtreeStream stream_forInit = new RewriteRuleSubtreeStream(adaptor, "rule forInit");
        RewriteRuleSubtreeStream stream_parenthesizedExpression = new RewriteRuleSubtreeStream(adaptor, "rule parenthesizedExpression");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 3)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:230:11: ( block | IF parenthesizedExpression ifStat= statement ( ELSE elseStat= statement -> ^( IF parenthesizedExpression $ifStat $elseStat) | -> ^( IF parenthesizedExpression $ifStat) ) | FOR LPAREN forInit SEMI forCondition SEMI forUpdater RPAREN statement -> ^( FOR forInit forCondition forUpdater statement ) | WHILE parenthesizedExpression statement -> ^( WHILE parenthesizedExpression statement ) | DO statement WHILE parenthesizedExpression SEMI -> ^( DO statement parenthesizedExpression ) | expression SEMI | SEMI | declaration SEMI -> ^( DECLARATION declaration ) )
            int alt4 = 8;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:230:12: block
                {
                    root_0 = (Tree) adaptor.nil();

                    pushFollow(FOLLOW_block_in_statement4597);
                    block5 = block();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, block5.getTree());

                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:231:6: IF parenthesizedExpression ifStat= statement ( ELSE elseStat= statement -> ^( IF parenthesizedExpression $ifStat $elseStat) | -> ^( IF parenthesizedExpression $ifStat) )
                {
                    IF6 = (Token) match(input, IF, FOLLOW_IF_in_statement4604);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_IF.add(IF6);

                    pushFollow(FOLLOW_parenthesizedExpression_in_statement4606);
                    parenthesizedExpression7 = parenthesizedExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_parenthesizedExpression.add(parenthesizedExpression7.getTree());
                    pushFollow(FOLLOW_statement_in_statement4610);
                    ifStat = statement();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_statement.add(ifStat.getTree());
                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:232:9: ( ELSE elseStat= statement -> ^( IF parenthesizedExpression $ifStat $elseStat) | -> ^( IF parenthesizedExpression $ifStat) )
                    int alt3 = 2;
                    int LA3_0 = input.LA(1);

                    if ((LA3_0 == ELSE)) {
                        int LA3_1 = input.LA(2);

                        if ((synpred4_VNScript())) {
                            alt3 = 1;
                        } else if ((true)) {
                            alt3 = 2;
                        } else {
                            if (state.backtracking > 0) {
                                state.failed = true;
                                return retval;
                            }
                            NoViableAltException nvae
                                = new NoViableAltException("", 3, 1, input);

                            throw nvae;
                        }
                    } else if ((LA3_0 == EOF || LA3_0 == DEC || LA3_0 == INC || LA3_0 == LCURLY || LA3_0 == LESS_THAN || LA3_0 == LOGICAL_NOT || (LA3_0 >= LPAREN && LA3_0 <= MINUS) || LA3_0 == NOT || LA3_0 == PLUS || LA3_0 == RCURLY || LA3_0 == SEMI || LA3_0 == BOOLEAN || LA3_0 == BYTE || LA3_0 == CHAR || (LA3_0 >= DO && LA3_0 <= DOUBLE) || LA3_0 == FALSE || (LA3_0 >= FLOAT && LA3_0 <= IF) || (LA3_0 >= INT && LA3_0 <= LONG) || LA3_0 == NULL || LA3_0 == SHORT || LA3_0 == SUPER || LA3_0 == THIS || LA3_0 == TRUE || LA3_0 == VOID || LA3_0 == WHILE || (LA3_0 >= IDENT && LA3_0 <= STRING_LITERAL))) {
                        alt3 = 2;
                    } else {
                        if (state.backtracking > 0) {
                            state.failed = true;
                            return retval;
                        }
                        NoViableAltException nvae
                            = new NoViableAltException("", 3, 0, input);

                        throw nvae;
                    }
                    switch (alt3) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:232:13: ELSE elseStat= statement
                        {
                            ELSE8 = (Token) match(input, ELSE, FOLLOW_ELSE_in_statement4625);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_ELSE.add(ELSE8);

                            pushFollow(FOLLOW_statement_in_statement4629);
                            elseStat = statement();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_statement.add(elseStat.getTree());

                            // AST REWRITE
                            // elements: IF, ifStat, parenthesizedExpression, elseStat
                            // token labels: 
                            // rule labels: retval, ifStat, elseStat
                            // token list labels: 
                            // rule list labels: 
                            // wildcard labels: 
                            if (state.backtracking == 0) {
                                retval.tree = root_0;
                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);
                                RewriteRuleSubtreeStream stream_ifStat = new RewriteRuleSubtreeStream(adaptor, "rule ifStat", ifStat != null ? ifStat.tree : null);
                                RewriteRuleSubtreeStream stream_elseStat = new RewriteRuleSubtreeStream(adaptor, "rule elseStat", elseStat != null ? elseStat.tree : null);

                                root_0 = (Tree) adaptor.nil();
                                // 232:77: -> ^( IF parenthesizedExpression $ifStat $elseStat)
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:232:81: ^( IF parenthesizedExpression $ifStat $elseStat)
                                    {
                                        Tree root_1 = (Tree) adaptor.nil();
                                        root_1 = (Tree) adaptor.becomeRoot(stream_IF.nextNode(), root_1);

                                        adaptor.addChild(root_1, stream_parenthesizedExpression.nextTree());
                                        adaptor.addChild(root_1, stream_ifStat.nextTree());
                                        adaptor.addChild(root_1, stream_elseStat.nextTree());

                                        adaptor.addChild(root_0, root_1);
                                    }

                                }

                                retval.tree = root_0;
                            }
                        }
                        break;
                        case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:233:77: 
                        {

                            // AST REWRITE
                            // elements: IF, parenthesizedExpression, ifStat
                            // token labels: 
                            // rule labels: retval, ifStat
                            // token list labels: 
                            // rule list labels: 
                            // wildcard labels: 
                            if (state.backtracking == 0) {
                                retval.tree = root_0;
                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);
                                RewriteRuleSubtreeStream stream_ifStat = new RewriteRuleSubtreeStream(adaptor, "rule ifStat", ifStat != null ? ifStat.tree : null);

                                root_0 = (Tree) adaptor.nil();
                                // 233:77: -> ^( IF parenthesizedExpression $ifStat)
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:233:81: ^( IF parenthesizedExpression $ifStat)
                                    {
                                        Tree root_1 = (Tree) adaptor.nil();
                                        root_1 = (Tree) adaptor.becomeRoot(stream_IF.nextNode(), root_1);

                                        adaptor.addChild(root_1, stream_parenthesizedExpression.nextTree());
                                        adaptor.addChild(root_1, stream_ifStat.nextTree());

                                        adaptor.addChild(root_0, root_1);
                                    }

                                }

                                retval.tree = root_0;
                            }
                        }
                        break;

                    }

                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:235:9: FOR LPAREN forInit SEMI forCondition SEMI forUpdater RPAREN statement
                {
                    FOR9 = (Token) match(input, FOR, FOLLOW_FOR_in_statement4795);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_FOR.add(FOR9);

                    LPAREN10 = (Token) match(input, LPAREN, FOLLOW_LPAREN_in_statement4797);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_LPAREN.add(LPAREN10);

                    pushFollow(FOLLOW_forInit_in_statement4811);
                    forInit11 = forInit();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_forInit.add(forInit11.getTree());
                    SEMI12 = (Token) match(input, SEMI, FOLLOW_SEMI_in_statement4813);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_SEMI.add(SEMI12);

                    pushFollow(FOLLOW_forCondition_in_statement4815);
                    forCondition13 = forCondition();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_forCondition.add(forCondition13.getTree());
                    SEMI14 = (Token) match(input, SEMI, FOLLOW_SEMI_in_statement4817);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_SEMI.add(SEMI14);

                    pushFollow(FOLLOW_forUpdater_in_statement4819);
                    forUpdater15 = forUpdater();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_forUpdater.add(forUpdater15.getTree());
                    RPAREN16 = (Token) match(input, RPAREN, FOLLOW_RPAREN_in_statement4821);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_RPAREN.add(RPAREN16);

                    pushFollow(FOLLOW_statement_in_statement4823);
                    statement17 = statement();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_statement.add(statement17.getTree());

                    // AST REWRITE
                    // elements: forInit, statement, forUpdater, forCondition, FOR
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 236:76: -> ^( FOR forInit forCondition forUpdater statement )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:236:80: ^( FOR forInit forCondition forUpdater statement )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot(stream_FOR.nextNode(), root_1);

                                adaptor.addChild(root_1, stream_forInit.nextTree());
                                adaptor.addChild(root_1, stream_forCondition.nextTree());
                                adaptor.addChild(root_1, stream_forUpdater.nextTree());
                                adaptor.addChild(root_1, stream_statement.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:237:9: WHILE parenthesizedExpression statement
                {
                    WHILE18 = (Token) match(input, WHILE, FOLLOW_WHILE_in_statement4854);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_WHILE.add(WHILE18);

                    pushFollow(FOLLOW_parenthesizedExpression_in_statement4856);
                    parenthesizedExpression19 = parenthesizedExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_parenthesizedExpression.add(parenthesizedExpression19.getTree());
                    pushFollow(FOLLOW_statement_in_statement4858);
                    statement20 = statement();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_statement.add(statement20.getTree());

                    // AST REWRITE
                    // elements: parenthesizedExpression, statement, WHILE
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 237:77: -> ^( WHILE parenthesizedExpression statement )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:237:81: ^( WHILE parenthesizedExpression statement )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot(stream_WHILE.nextNode(), root_1);

                                adaptor.addChild(root_1, stream_parenthesizedExpression.nextTree());
                                adaptor.addChild(root_1, stream_statement.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 5: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:238:9: DO statement WHILE parenthesizedExpression SEMI
                {
                    DO21 = (Token) match(input, DO, FOLLOW_DO_in_statement4907);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_DO.add(DO21);

                    pushFollow(FOLLOW_statement_in_statement4909);
                    statement22 = statement();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_statement.add(statement22.getTree());
                    WHILE23 = (Token) match(input, WHILE, FOLLOW_WHILE_in_statement4911);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_WHILE.add(WHILE23);

                    pushFollow(FOLLOW_parenthesizedExpression_in_statement4913);
                    parenthesizedExpression24 = parenthesizedExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_parenthesizedExpression.add(parenthesizedExpression24.getTree());
                    SEMI25 = (Token) match(input, SEMI, FOLLOW_SEMI_in_statement4915);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_SEMI.add(SEMI25);

                    // AST REWRITE
                    // elements: statement, parenthesizedExpression, DO
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 238:77: -> ^( DO statement parenthesizedExpression )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:238:81: ^( DO statement parenthesizedExpression )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot(stream_DO.nextNode(), root_1);

                                adaptor.addChild(root_1, stream_statement.nextTree());
                                adaptor.addChild(root_1, stream_parenthesizedExpression.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 6: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:239:9: expression SEMI
                {
                    root_0 = (Tree) adaptor.nil();

                    pushFollow(FOLLOW_expression_in_statement4956);
                    expression26 = expression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, expression26.getTree());
                    SEMI27 = (Token) match(input, SEMI, FOLLOW_SEMI_in_statement4958);
                    if (state.failed)
                        return retval;

                }
                break;
                case 7: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:240:9: SEMI
                {
                    root_0 = (Tree) adaptor.nil();

                    SEMI28 = (Token) match(input, SEMI, FOLLOW_SEMI_in_statement4969);
                    if (state.failed)
                        return retval;

                }
                break;
                case 8: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:241:9: declaration SEMI
                {
                    pushFollow(FOLLOW_declaration_in_statement4981);
                    declaration29 = declaration();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_declaration.add(declaration29.getTree());
                    SEMI30 = (Token) match(input, SEMI, FOLLOW_SEMI_in_statement4983);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_SEMI.add(SEMI30);

                    // AST REWRITE
                    // elements: declaration
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 241:26: -> ^( DECLARATION declaration )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:241:29: ^( DECLARATION declaration )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(DECLARATION, "DECLARATION"), root_1);

                                adaptor.addChild(root_1, stream_declaration.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 3, statement_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "statement"
    public static class declaration_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "declaration"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:244:1: declaration : IDENT ':=' expression ;
    public final VNScriptParser.declaration_return declaration() throws RecognitionException
    {
        VNScriptParser.declaration_return retval = new VNScriptParser.declaration_return();
        retval.start = input.LT(1);
        int declaration_StartIndex = input.index();
        Tree root_0 = null;

        Token IDENT31 = null;
        Token string_literal32 = null;
        VNScriptParser.expression_return expression33 = null;

        Tree IDENT31_tree = null;
        Tree string_literal32_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 4)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:245:2: ( IDENT ':=' expression )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:245:4: IDENT ':=' expression
            {
                root_0 = (Tree) adaptor.nil();

                IDENT31 = (Token) match(input, IDENT, FOLLOW_IDENT_in_declaration5006);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0) {
                    IDENT31_tree = (Tree) adaptor.create(IDENT31);
                    adaptor.addChild(root_0, IDENT31_tree);
                }
                string_literal32 = (Token) match(input, 189, FOLLOW_189_in_declaration5008);
                if (state.failed)
                    return retval;
                pushFollow(FOLLOW_expression_in_declaration5011);
                expression33 = expression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, expression33.getTree());

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 4, declaration_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "declaration"
    public static class forInit_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "forInit"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:248:1: forInit : ( expression -> ^( FOR_INIT expression ) | declaration -> ^( FOR_INIT ^( DECLARATION declaration ) ) | -> ^( FOR_INIT ) );
    public final VNScriptParser.forInit_return forInit() throws RecognitionException
    {
        VNScriptParser.forInit_return retval = new VNScriptParser.forInit_return();
        retval.start = input.LT(1);
        int forInit_StartIndex = input.index();
        Tree root_0 = null;

        VNScriptParser.expression_return expression34 = null;

        VNScriptParser.declaration_return declaration35 = null;

        RewriteRuleSubtreeStream stream_expression = new RewriteRuleSubtreeStream(adaptor, "rule expression");
        RewriteRuleSubtreeStream stream_declaration = new RewriteRuleSubtreeStream(adaptor, "rule declaration");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 5)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:249:5: ( expression -> ^( FOR_INIT expression ) | declaration -> ^( FOR_INIT ^( DECLARATION declaration ) ) | -> ^( FOR_INIT ) )
            int alt5 = 3;
            switch (input.LA(1)) {
                case DEC:
                case INC:
                case LESS_THAN:
                case LOGICAL_NOT:
                case LPAREN:
                case MINUS:
                case NOT:
                case PLUS:
                case BOOLEAN:
                case BYTE:
                case CHAR:
                case DOUBLE:
                case FALSE:
                case FLOAT:
                case INT:
                case LONG:
                case NULL:
                case SHORT:
                case SUPER:
                case THIS:
                case TRUE:
                case VOID:
                case HEX_LITERAL:
                case OCTAL_LITERAL:
                case DECIMAL_LITERAL:
                case FLOATING_POINT_LITERAL:
                case CHARACTER_LITERAL:
                case STRING_LITERAL: {
                    alt5 = 1;
                }
                break;
                case IDENT: {
                    int LA5_2 = input.LA(2);

                    if ((LA5_2 == 189)) {
                        alt5 = 2;
                    } else if (((LA5_2 >= AND && LA5_2 <= ASSIGN) || (LA5_2 >= BIT_SHIFT_RIGHT && LA5_2 <= BIT_SHIFT_RIGHT_ASSIGN) || (LA5_2 >= DEC && LA5_2 <= DOT) || (LA5_2 >= EQUAL && LA5_2 <= LBRACK) || (LA5_2 >= LESS_OR_EQUAL && LA5_2 <= LOGICAL_AND) || (LA5_2 >= LOGICAL_OR && LA5_2 <= MOD_ASSIGN) || (LA5_2 >= NOT_EQUAL && LA5_2 <= QUESTION) || (LA5_2 >= SEMI && LA5_2 <= XOR_ASSIGN) || LA5_2 == INSTANCEOF)) {
                        alt5 = 1;
                    } else {
                        if (state.backtracking > 0) {
                            state.failed = true;
                            return retval;
                        }
                        NoViableAltException nvae
                            = new NoViableAltException("", 5, 2, input);

                        throw nvae;
                    }
                }
                break;
                case SEMI: {
                    alt5 = 3;
                }
                break;
                default:
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 5, 0, input);

                    throw nvae;
            }

            switch (alt5) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:249:9: expression
                {
                    pushFollow(FOLLOW_expression_in_forInit5027);
                    expression34 = expression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_expression.add(expression34.getTree());

                    // AST REWRITE
                    // elements: expression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 249:33: -> ^( FOR_INIT expression )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:249:37: ^( FOR_INIT expression )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(FOR_INIT, "FOR_INIT"), root_1);

                                adaptor.addChild(root_1, stream_expression.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:250:9: declaration
                {
                    pushFollow(FOLLOW_declaration_in_forInit5059);
                    declaration35 = declaration();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_declaration.add(declaration35.getTree());

                    // AST REWRITE
                    // elements: declaration
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 250:34: -> ^( FOR_INIT ^( DECLARATION declaration ) )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:250:38: ^( FOR_INIT ^( DECLARATION declaration ) )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(FOR_INIT, "FOR_INIT"), root_1);

                                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:250:49: ^( DECLARATION declaration )
                                {
                                    Tree root_2 = (Tree) adaptor.nil();
                                    root_2 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(DECLARATION, "DECLARATION"), root_2);

                                    adaptor.addChild(root_2, stream_declaration.nextTree());

                                    adaptor.addChild(root_1, root_2);
                                }

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:251:37: 
                {

                    // AST REWRITE
                    // elements: 
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 251:37: -> ^( FOR_INIT )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:251:41: ^( FOR_INIT )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(FOR_INIT, "FOR_INIT"), root_1);

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 5, forInit_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "forInit"
    public static class forCondition_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "forCondition"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:254:1: forCondition : ( expression )? -> ^( FOR_CONDITION ( expression )? ) ;
    public final VNScriptParser.forCondition_return forCondition() throws RecognitionException
    {
        VNScriptParser.forCondition_return retval = new VNScriptParser.forCondition_return();
        retval.start = input.LT(1);
        int forCondition_StartIndex = input.index();
        Tree root_0 = null;

        VNScriptParser.expression_return expression36 = null;

        RewriteRuleSubtreeStream stream_expression = new RewriteRuleSubtreeStream(adaptor, "rule expression");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 6)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:255:5: ( ( expression )? -> ^( FOR_CONDITION ( expression )? ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:255:9: ( expression )?
            {
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:255:9: ( expression )?
                int alt6 = 2;
                int LA6_0 = input.LA(1);

                if ((LA6_0 == DEC || LA6_0 == INC || LA6_0 == LESS_THAN || LA6_0 == LOGICAL_NOT || (LA6_0 >= LPAREN && LA6_0 <= MINUS) || LA6_0 == NOT || LA6_0 == PLUS || LA6_0 == BOOLEAN || LA6_0 == BYTE || LA6_0 == CHAR || LA6_0 == DOUBLE || LA6_0 == FALSE || LA6_0 == FLOAT || (LA6_0 >= INT && LA6_0 <= LONG) || LA6_0 == NULL || LA6_0 == SHORT || LA6_0 == SUPER || LA6_0 == THIS || LA6_0 == TRUE || LA6_0 == VOID || (LA6_0 >= IDENT && LA6_0 <= STRING_LITERAL))) {
                    alt6 = 1;
                }
                switch (alt6) {
                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:0:0: expression
                    {
                        pushFollow(FOLLOW_expression_in_forCondition5151);
                        expression36 = expression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_expression.add(expression36.getTree());

                    }
                    break;

                }

                // AST REWRITE
                // elements: expression
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (Tree) adaptor.nil();
                    // 256:9: -> ^( FOR_CONDITION ( expression )? )
                    {
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:256:13: ^( FOR_CONDITION ( expression )? )
                        {
                            Tree root_1 = (Tree) adaptor.nil();
                            root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(FOR_CONDITION, "FOR_CONDITION"), root_1);

                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:256:29: ( expression )?
                            if (stream_expression.hasNext()) {
                                adaptor.addChild(root_1, stream_expression.nextTree());

                            }
                            stream_expression.reset();

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 6, forCondition_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "forCondition"
    public static class forUpdater_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "forUpdater"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:259:1: forUpdater : ( expressionList )? -> ^( FOR_UPDATE ( expressionList )? ) ;
    public final VNScriptParser.forUpdater_return forUpdater() throws RecognitionException
    {
        VNScriptParser.forUpdater_return retval = new VNScriptParser.forUpdater_return();
        retval.start = input.LT(1);
        int forUpdater_StartIndex = input.index();
        Tree root_0 = null;

        VNScriptParser.expressionList_return expressionList37 = null;

        RewriteRuleSubtreeStream stream_expressionList = new RewriteRuleSubtreeStream(adaptor, "rule expressionList");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 7)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:260:5: ( ( expressionList )? -> ^( FOR_UPDATE ( expressionList )? ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:260:9: ( expressionList )?
            {
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:260:9: ( expressionList )?
                int alt7 = 2;
                int LA7_0 = input.LA(1);

                if ((LA7_0 == DEC || LA7_0 == INC || LA7_0 == LESS_THAN || LA7_0 == LOGICAL_NOT || (LA7_0 >= LPAREN && LA7_0 <= MINUS) || LA7_0 == NOT || LA7_0 == PLUS || LA7_0 == BOOLEAN || LA7_0 == BYTE || LA7_0 == CHAR || LA7_0 == DOUBLE || LA7_0 == FALSE || LA7_0 == FLOAT || (LA7_0 >= INT && LA7_0 <= LONG) || LA7_0 == NULL || LA7_0 == SHORT || LA7_0 == SUPER || LA7_0 == THIS || LA7_0 == TRUE || LA7_0 == VOID || (LA7_0 >= IDENT && LA7_0 <= STRING_LITERAL))) {
                    alt7 = 1;
                }
                switch (alt7) {
                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:0:0: expressionList
                    {
                        pushFollow(FOLLOW_expressionList_in_forUpdater5193);
                        expressionList37 = expressionList();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_expressionList.add(expressionList37.getTree());

                    }
                    break;

                }

                // AST REWRITE
                // elements: expressionList
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (Tree) adaptor.nil();
                    // 261:9: -> ^( FOR_UPDATE ( expressionList )? )
                    {
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:261:13: ^( FOR_UPDATE ( expressionList )? )
                        {
                            Tree root_1 = (Tree) adaptor.nil();
                            root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(FOR_UPDATE, "FOR_UPDATE"), root_1);

                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:261:26: ( expressionList )?
                            if (stream_expressionList.hasNext()) {
                                adaptor.addChild(root_1, stream_expressionList.nextTree());

                            }
                            stream_expressionList.reset();

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 7, forUpdater_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "forUpdater"
    public static class parenthesizedExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "parenthesizedExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:267:1: parenthesizedExpression : LPAREN expression RPAREN -> expression ;
    public final VNScriptParser.parenthesizedExpression_return parenthesizedExpression() throws RecognitionException
    {
        VNScriptParser.parenthesizedExpression_return retval = new VNScriptParser.parenthesizedExpression_return();
        retval.start = input.LT(1);
        int parenthesizedExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token LPAREN38 = null;
        Token RPAREN40 = null;
        VNScriptParser.expression_return expression39 = null;

        Tree LPAREN38_tree = null;
        Tree RPAREN40_tree = null;
        RewriteRuleTokenStream stream_RPAREN = new RewriteRuleTokenStream(adaptor, "token RPAREN");
        RewriteRuleTokenStream stream_LPAREN = new RewriteRuleTokenStream(adaptor, "token LPAREN");
        RewriteRuleSubtreeStream stream_expression = new RewriteRuleSubtreeStream(adaptor, "rule expression");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 8)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:268:5: ( LPAREN expression RPAREN -> expression )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:268:9: LPAREN expression RPAREN
            {
                LPAREN38 = (Token) match(input, LPAREN, FOLLOW_LPAREN_in_parenthesizedExpression5234);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_LPAREN.add(LPAREN38);

                pushFollow(FOLLOW_expression_in_parenthesizedExpression5236);
                expression39 = expression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_expression.add(expression39.getTree());
                RPAREN40 = (Token) match(input, RPAREN, FOLLOW_RPAREN_in_parenthesizedExpression5238);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_RPAREN.add(RPAREN40);

                // AST REWRITE
                // elements: expression
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (Tree) adaptor.nil();
                    // 270:9: -> expression
                    {
                        adaptor.addChild(root_0, stream_expression.nextTree());

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 8, parenthesizedExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "parenthesizedExpression"
    public static class expressionList_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "expressionList"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:273:1: expressionList : expression ( COMMA expression )* ;
    public final VNScriptParser.expressionList_return expressionList() throws RecognitionException
    {
        VNScriptParser.expressionList_return retval = new VNScriptParser.expressionList_return();
        retval.start = input.LT(1);
        int expressionList_StartIndex = input.index();
        Tree root_0 = null;

        Token COMMA42 = null;
        VNScriptParser.expression_return expression41 = null;

        VNScriptParser.expression_return expression43 = null;

        Tree COMMA42_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 9)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:274:5: ( expression ( COMMA expression )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:274:9: expression ( COMMA expression )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_expression_in_expressionList5272);
                expression41 = expression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, expression41.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:274:20: ( COMMA expression )*
                loop8:
                do {
                    int alt8 = 2;
                    int LA8_0 = input.LA(1);

                    if ((LA8_0 == COMMA)) {
                        alt8 = 1;
                    }

                    switch (alt8) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:274:21: COMMA expression
                        {
                            COMMA42 = (Token) match(input, COMMA, FOLLOW_COMMA_in_expressionList5275);
                            if (state.failed)
                                return retval;
                            pushFollow(FOLLOW_expression_in_expressionList5278);
                            expression43 = expression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, expression43.getTree());

                        }
                        break;

                        default:
                            break loop8;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 9, expressionList_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "expressionList"
    public static class range_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "range"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:277:1: range : ( range_offset -> ^( RANGE_OFFSET range_offset ) | range_left -> ^( RANGE_LEFT range_left ) | range_single -> ^( RANGE_SINGLE range_single ) | range_right -> ^( RANGE_RIGHT range_right ) | range_all -> ^( RANGE_ALL ) );
    public final VNScriptParser.range_return range() throws RecognitionException
    {
        VNScriptParser.range_return retval = new VNScriptParser.range_return();
        retval.start = input.LT(1);
        int range_StartIndex = input.index();
        Tree root_0 = null;

        VNScriptParser.range_offset_return range_offset44 = null;

        VNScriptParser.range_left_return range_left45 = null;

        VNScriptParser.range_single_return range_single46 = null;

        VNScriptParser.range_right_return range_right47 = null;

        VNScriptParser.range_all_return range_all48 = null;

        RewriteRuleSubtreeStream stream_range_left = new RewriteRuleSubtreeStream(adaptor, "rule range_left");
        RewriteRuleSubtreeStream stream_range_all = new RewriteRuleSubtreeStream(adaptor, "rule range_all");
        RewriteRuleSubtreeStream stream_range_offset = new RewriteRuleSubtreeStream(adaptor, "rule range_offset");
        RewriteRuleSubtreeStream stream_range_right = new RewriteRuleSubtreeStream(adaptor, "rule range_right");
        RewriteRuleSubtreeStream stream_range_single = new RewriteRuleSubtreeStream(adaptor, "rule range_single");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 10)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:277:7: ( range_offset -> ^( RANGE_OFFSET range_offset ) | range_left -> ^( RANGE_LEFT range_left ) | range_single -> ^( RANGE_SINGLE range_single ) | range_right -> ^( RANGE_RIGHT range_right ) | range_all -> ^( RANGE_ALL ) )
            int alt9 = 5;
            alt9 = dfa9.predict(input);
            switch (alt9) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:278:3: range_offset
                {
                    pushFollow(FOLLOW_range_offset_in_range5299);
                    range_offset44 = range_offset();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_range_offset.add(range_offset44.getTree());

                    // AST REWRITE
                    // elements: range_offset
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 278:16: -> ^( RANGE_OFFSET range_offset )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:278:19: ^( RANGE_OFFSET range_offset )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(RANGE_OFFSET, "RANGE_OFFSET"), root_1);

                                adaptor.addChild(root_1, stream_range_offset.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:279:4: range_left
                {
                    pushFollow(FOLLOW_range_left_in_range5312);
                    range_left45 = range_left();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_range_left.add(range_left45.getTree());

                    // AST REWRITE
                    // elements: range_left
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 279:15: -> ^( RANGE_LEFT range_left )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:279:18: ^( RANGE_LEFT range_left )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(RANGE_LEFT, "RANGE_LEFT"), root_1);

                                adaptor.addChild(root_1, stream_range_left.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:280:4: range_single
                {
                    pushFollow(FOLLOW_range_single_in_range5325);
                    range_single46 = range_single();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_range_single.add(range_single46.getTree());

                    // AST REWRITE
                    // elements: range_single
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 280:17: -> ^( RANGE_SINGLE range_single )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:280:20: ^( RANGE_SINGLE range_single )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(RANGE_SINGLE, "RANGE_SINGLE"), root_1);

                                adaptor.addChild(root_1, stream_range_single.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:281:4: range_right
                {
                    pushFollow(FOLLOW_range_right_in_range5338);
                    range_right47 = range_right();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_range_right.add(range_right47.getTree());

                    // AST REWRITE
                    // elements: range_right
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 281:16: -> ^( RANGE_RIGHT range_right )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:281:19: ^( RANGE_RIGHT range_right )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(RANGE_RIGHT, "RANGE_RIGHT"), root_1);

                                adaptor.addChild(root_1, stream_range_right.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 5: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:282:4: range_all
                {
                    pushFollow(FOLLOW_range_all_in_range5351);
                    range_all48 = range_all();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_range_all.add(range_all48.getTree());

                    // AST REWRITE
                    // elements: 
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 282:14: -> ^( RANGE_ALL )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:282:17: ^( RANGE_ALL )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(RANGE_ALL, "RANGE_ALL"), root_1);

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 10, range_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "range"
    public static class range_single_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "range_single"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:285:1: range_single : expression ;
    public final VNScriptParser.range_single_return range_single() throws RecognitionException
    {
        VNScriptParser.range_single_return retval = new VNScriptParser.range_single_return();
        retval.start = input.LT(1);
        int range_single_StartIndex = input.index();
        Tree root_0 = null;

        VNScriptParser.expression_return expression49 = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 11)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:285:14: ( expression )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:285:16: expression
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_expression_in_range_single5367);
                expression49 = expression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, expression49.getTree());

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 11, range_single_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "range_single"
    public static class range_offset_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "range_offset"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:288:1: range_offset : ( '+' | '-' ) expression ;
    public final VNScriptParser.range_offset_return range_offset() throws RecognitionException
    {
        VNScriptParser.range_offset_return retval = new VNScriptParser.range_offset_return();
        retval.start = input.LT(1);
        int range_offset_StartIndex = input.index();
        Tree root_0 = null;

        Token set50 = null;
        VNScriptParser.expression_return expression51 = null;

        Tree set50_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 12)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:288:14: ( ( '+' | '-' ) expression )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:288:16: ( '+' | '-' ) expression
            {
                root_0 = (Tree) adaptor.nil();

                set50 = (Token) input.LT(1);
                if (input.LA(1) == MINUS || input.LA(1) == PLUS) {
                    input.consume();
                    state.errorRecovery = false;
                    state.failed = false;
                } else {
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    throw mse;
                }

                pushFollow(FOLLOW_expression_in_range_offset5384);
                expression51 = expression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, expression51.getTree());

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 12, range_offset_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "range_offset"
    public static class range_left_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "range_left"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:291:1: range_left : expression COLON ( expression )? ;
    public final VNScriptParser.range_left_return range_left() throws RecognitionException
    {
        VNScriptParser.range_left_return retval = new VNScriptParser.range_left_return();
        retval.start = input.LT(1);
        int range_left_StartIndex = input.index();
        Tree root_0 = null;

        Token COLON53 = null;
        VNScriptParser.expression_return expression52 = null;

        VNScriptParser.expression_return expression54 = null;

        Tree COLON53_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 13)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:291:12: ( expression COLON ( expression )? )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:291:14: expression COLON ( expression )?
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_expression_in_range_left5394);
                expression52 = expression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, expression52.getTree());
                COLON53 = (Token) match(input, COLON, FOLLOW_COLON_in_range_left5396);
                if (state.failed)
                    return retval;
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:291:32: ( expression )?
                int alt10 = 2;
                int LA10_0 = input.LA(1);

                if ((LA10_0 == DEC || LA10_0 == INC || LA10_0 == LESS_THAN || LA10_0 == LOGICAL_NOT || (LA10_0 >= LPAREN && LA10_0 <= MINUS) || LA10_0 == NOT || LA10_0 == PLUS || LA10_0 == BOOLEAN || LA10_0 == BYTE || LA10_0 == CHAR || LA10_0 == DOUBLE || LA10_0 == FALSE || LA10_0 == FLOAT || (LA10_0 >= INT && LA10_0 <= LONG) || LA10_0 == NULL || LA10_0 == SHORT || LA10_0 == SUPER || LA10_0 == THIS || LA10_0 == TRUE || LA10_0 == VOID || (LA10_0 >= IDENT && LA10_0 <= STRING_LITERAL))) {
                    alt10 = 1;
                }
                switch (alt10) {
                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:0:0: expression
                    {
                        pushFollow(FOLLOW_expression_in_range_left5399);
                        expression54 = expression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_0, expression54.getTree());

                    }
                    break;

                }

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 13, range_left_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "range_left"
    public static class range_right_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "range_right"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:293:1: range_right : COLON expression ;
    public final VNScriptParser.range_right_return range_right() throws RecognitionException
    {
        VNScriptParser.range_right_return retval = new VNScriptParser.range_right_return();
        retval.start = input.LT(1);
        int range_right_StartIndex = input.index();
        Tree root_0 = null;

        Token COLON55 = null;
        VNScriptParser.expression_return expression56 = null;

        Tree COLON55_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 14)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:293:13: ( COLON expression )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:293:15: COLON expression
            {
                root_0 = (Tree) adaptor.nil();

                COLON55 = (Token) match(input, COLON, FOLLOW_COLON_in_range_right5409);
                if (state.failed)
                    return retval;
                pushFollow(FOLLOW_expression_in_range_right5412);
                expression56 = expression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, expression56.getTree());

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 14, range_right_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "range_right"
    public static class range_all_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "range_all"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:295:1: range_all : COLON ;
    public final VNScriptParser.range_all_return range_all() throws RecognitionException
    {
        VNScriptParser.range_all_return retval = new VNScriptParser.range_all_return();
        retval.start = input.LT(1);
        int range_all_StartIndex = input.index();
        Tree root_0 = null;

        Token COLON57 = null;

        Tree COLON57_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 15)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:295:11: ( COLON )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:295:13: COLON
            {
                root_0 = (Tree) adaptor.nil();

                COLON57 = (Token) match(input, COLON, FOLLOW_COLON_in_range_all5421);
                if (state.failed)
                    return retval;

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 15, range_all_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "range_all"
    public static class rangeList_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "rangeList"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:298:1: rangeList : range ( COMMA range )* ;
    public final VNScriptParser.rangeList_return rangeList() throws RecognitionException
    {
        VNScriptParser.rangeList_return retval = new VNScriptParser.rangeList_return();
        retval.start = input.LT(1);
        int rangeList_StartIndex = input.index();
        Tree root_0 = null;

        Token COMMA59 = null;
        VNScriptParser.range_return range58 = null;

        VNScriptParser.range_return range60 = null;

        Tree COMMA59_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 16)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:299:3: ( range ( COMMA range )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:299:5: range ( COMMA range )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_range_in_rangeList5434);
                range58 = range();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, range58.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:299:11: ( COMMA range )*
                loop11:
                do {
                    int alt11 = 2;
                    int LA11_0 = input.LA(1);

                    if ((LA11_0 == COMMA)) {
                        alt11 = 1;
                    }

                    switch (alt11) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:299:12: COMMA range
                        {
                            COMMA59 = (Token) match(input, COMMA, FOLLOW_COMMA_in_rangeList5437);
                            if (state.failed)
                                return retval;
                            pushFollow(FOLLOW_range_in_rangeList5440);
                            range60 = range();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, range60.getTree());

                        }
                        break;

                        default:
                            break loop11;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 16, rangeList_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "rangeList"
    public static class expression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "expression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:302:1: expression : assignmentExpression -> ^( EXPR assignmentExpression ) ;
    public final VNScriptParser.expression_return expression() throws RecognitionException
    {
        VNScriptParser.expression_return retval = new VNScriptParser.expression_return();
        retval.start = input.LT(1);
        int expression_StartIndex = input.index();
        Tree root_0 = null;

        VNScriptParser.assignmentExpression_return assignmentExpression61 = null;

        RewriteRuleSubtreeStream stream_assignmentExpression = new RewriteRuleSubtreeStream(adaptor, "rule assignmentExpression");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 17)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:303:5: ( assignmentExpression -> ^( EXPR assignmentExpression ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:303:9: assignmentExpression
            {
                pushFollow(FOLLOW_assignmentExpression_in_expression5459);
                assignmentExpression61 = assignmentExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_assignmentExpression.add(assignmentExpression61.getTree());

                // AST REWRITE
                // elements: assignmentExpression
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (Tree) adaptor.nil();
                    // 304:9: -> ^( EXPR assignmentExpression )
                    {
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:304:13: ^( EXPR assignmentExpression )
                        {
                            Tree root_1 = (Tree) adaptor.nil();
                            root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(EXPR, "EXPR"), root_1);

                            adaptor.addChild(root_1, stream_assignmentExpression.nextTree());

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 17, expression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "expression"
    public static class assignmentExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "assignmentExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:307:1: assignmentExpression : conditionalExpression ( ( ASSIGN | PLUS_ASSIGN | MINUS_ASSIGN | STAR_ASSIGN | DIV_ASSIGN | AND_ASSIGN | OR_ASSIGN | XOR_ASSIGN | MOD_ASSIGN | SHIFT_LEFT_ASSIGN | SHIFT_RIGHT_ASSIGN | BIT_SHIFT_RIGHT_ASSIGN ) assignmentExpression )? ;
    public final VNScriptParser.assignmentExpression_return assignmentExpression() throws RecognitionException
    {
        VNScriptParser.assignmentExpression_return retval = new VNScriptParser.assignmentExpression_return();
        retval.start = input.LT(1);
        int assignmentExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token ASSIGN63 = null;
        Token PLUS_ASSIGN64 = null;
        Token MINUS_ASSIGN65 = null;
        Token STAR_ASSIGN66 = null;
        Token DIV_ASSIGN67 = null;
        Token AND_ASSIGN68 = null;
        Token OR_ASSIGN69 = null;
        Token XOR_ASSIGN70 = null;
        Token MOD_ASSIGN71 = null;
        Token SHIFT_LEFT_ASSIGN72 = null;
        Token SHIFT_RIGHT_ASSIGN73 = null;
        Token BIT_SHIFT_RIGHT_ASSIGN74 = null;
        VNScriptParser.conditionalExpression_return conditionalExpression62 = null;

        VNScriptParser.assignmentExpression_return assignmentExpression75 = null;

        Tree ASSIGN63_tree = null;
        Tree PLUS_ASSIGN64_tree = null;
        Tree MINUS_ASSIGN65_tree = null;
        Tree STAR_ASSIGN66_tree = null;
        Tree DIV_ASSIGN67_tree = null;
        Tree AND_ASSIGN68_tree = null;
        Tree OR_ASSIGN69_tree = null;
        Tree XOR_ASSIGN70_tree = null;
        Tree MOD_ASSIGN71_tree = null;
        Tree SHIFT_LEFT_ASSIGN72_tree = null;
        Tree SHIFT_RIGHT_ASSIGN73_tree = null;
        Tree BIT_SHIFT_RIGHT_ASSIGN74_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 18)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:308:5: ( conditionalExpression ( ( ASSIGN | PLUS_ASSIGN | MINUS_ASSIGN | STAR_ASSIGN | DIV_ASSIGN | AND_ASSIGN | OR_ASSIGN | XOR_ASSIGN | MOD_ASSIGN | SHIFT_LEFT_ASSIGN | SHIFT_RIGHT_ASSIGN | BIT_SHIFT_RIGHT_ASSIGN ) assignmentExpression )? )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:308:9: conditionalExpression ( ( ASSIGN | PLUS_ASSIGN | MINUS_ASSIGN | STAR_ASSIGN | DIV_ASSIGN | AND_ASSIGN | OR_ASSIGN | XOR_ASSIGN | MOD_ASSIGN | SHIFT_LEFT_ASSIGN | SHIFT_RIGHT_ASSIGN | BIT_SHIFT_RIGHT_ASSIGN ) assignmentExpression )?
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_conditionalExpression_in_assignmentExpression5495);
                conditionalExpression62 = conditionalExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, conditionalExpression62.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:309:9: ( ( ASSIGN | PLUS_ASSIGN | MINUS_ASSIGN | STAR_ASSIGN | DIV_ASSIGN | AND_ASSIGN | OR_ASSIGN | XOR_ASSIGN | MOD_ASSIGN | SHIFT_LEFT_ASSIGN | SHIFT_RIGHT_ASSIGN | BIT_SHIFT_RIGHT_ASSIGN ) assignmentExpression )?
                int alt13 = 2;
                int LA13_0 = input.LA(1);

                if (((LA13_0 >= AND_ASSIGN && LA13_0 <= ASSIGN) || LA13_0 == BIT_SHIFT_RIGHT_ASSIGN || LA13_0 == DIV_ASSIGN || LA13_0 == MINUS_ASSIGN || LA13_0 == MOD_ASSIGN || LA13_0 == OR_ASSIGN || LA13_0 == PLUS_ASSIGN || LA13_0 == SHIFT_LEFT_ASSIGN || LA13_0 == SHIFT_RIGHT_ASSIGN || LA13_0 == STAR_ASSIGN || LA13_0 == XOR_ASSIGN)) {
                    alt13 = 1;
                }
                switch (alt13) {
                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:309:13: ( ASSIGN | PLUS_ASSIGN | MINUS_ASSIGN | STAR_ASSIGN | DIV_ASSIGN | AND_ASSIGN | OR_ASSIGN | XOR_ASSIGN | MOD_ASSIGN | SHIFT_LEFT_ASSIGN | SHIFT_RIGHT_ASSIGN | BIT_SHIFT_RIGHT_ASSIGN ) assignmentExpression
                    {
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:309:13: ( ASSIGN | PLUS_ASSIGN | MINUS_ASSIGN | STAR_ASSIGN | DIV_ASSIGN | AND_ASSIGN | OR_ASSIGN | XOR_ASSIGN | MOD_ASSIGN | SHIFT_LEFT_ASSIGN | SHIFT_RIGHT_ASSIGN | BIT_SHIFT_RIGHT_ASSIGN )
                        int alt12 = 12;
                        switch (input.LA(1)) {
                            case ASSIGN: {
                                alt12 = 1;
                            }
                            break;
                            case PLUS_ASSIGN: {
                                alt12 = 2;
                            }
                            break;
                            case MINUS_ASSIGN: {
                                alt12 = 3;
                            }
                            break;
                            case STAR_ASSIGN: {
                                alt12 = 4;
                            }
                            break;
                            case DIV_ASSIGN: {
                                alt12 = 5;
                            }
                            break;
                            case AND_ASSIGN: {
                                alt12 = 6;
                            }
                            break;
                            case OR_ASSIGN: {
                                alt12 = 7;
                            }
                            break;
                            case XOR_ASSIGN: {
                                alt12 = 8;
                            }
                            break;
                            case MOD_ASSIGN: {
                                alt12 = 9;
                            }
                            break;
                            case SHIFT_LEFT_ASSIGN: {
                                alt12 = 10;
                            }
                            break;
                            case SHIFT_RIGHT_ASSIGN: {
                                alt12 = 11;
                            }
                            break;
                            case BIT_SHIFT_RIGHT_ASSIGN: {
                                alt12 = 12;
                            }
                            break;
                            default:
                                if (state.backtracking > 0) {
                                    state.failed = true;
                                    return retval;
                                }
                                NoViableAltException nvae
                                    = new NoViableAltException("", 12, 0, input);

                                throw nvae;
                        }

                        switch (alt12) {
                            case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:309:17: ASSIGN
                            {
                                ASSIGN63 = (Token) match(input, ASSIGN, FOLLOW_ASSIGN_in_assignmentExpression5514);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    ASSIGN63_tree = (Tree) adaptor.create(ASSIGN63);
                                    root_0 = (Tree) adaptor.becomeRoot(ASSIGN63_tree, root_0);
                                }

                            }
                            break;
                            case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:310:17: PLUS_ASSIGN
                            {
                                PLUS_ASSIGN64 = (Token) match(input, PLUS_ASSIGN, FOLLOW_PLUS_ASSIGN_in_assignmentExpression5533);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    PLUS_ASSIGN64_tree = (Tree) adaptor.create(PLUS_ASSIGN64);
                                    root_0 = (Tree) adaptor.becomeRoot(PLUS_ASSIGN64_tree, root_0);
                                }

                            }
                            break;
                            case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:311:17: MINUS_ASSIGN
                            {
                                MINUS_ASSIGN65 = (Token) match(input, MINUS_ASSIGN, FOLLOW_MINUS_ASSIGN_in_assignmentExpression5552);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    MINUS_ASSIGN65_tree = (Tree) adaptor.create(MINUS_ASSIGN65);
                                    root_0 = (Tree) adaptor.becomeRoot(MINUS_ASSIGN65_tree, root_0);
                                }

                            }
                            break;
                            case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:312:17: STAR_ASSIGN
                            {
                                STAR_ASSIGN66 = (Token) match(input, STAR_ASSIGN, FOLLOW_STAR_ASSIGN_in_assignmentExpression5571);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    STAR_ASSIGN66_tree = (Tree) adaptor.create(STAR_ASSIGN66);
                                    root_0 = (Tree) adaptor.becomeRoot(STAR_ASSIGN66_tree, root_0);
                                }

                            }
                            break;
                            case 5: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:313:17: DIV_ASSIGN
                            {
                                DIV_ASSIGN67 = (Token) match(input, DIV_ASSIGN, FOLLOW_DIV_ASSIGN_in_assignmentExpression5590);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    DIV_ASSIGN67_tree = (Tree) adaptor.create(DIV_ASSIGN67);
                                    root_0 = (Tree) adaptor.becomeRoot(DIV_ASSIGN67_tree, root_0);
                                }

                            }
                            break;
                            case 6: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:314:17: AND_ASSIGN
                            {
                                AND_ASSIGN68 = (Token) match(input, AND_ASSIGN, FOLLOW_AND_ASSIGN_in_assignmentExpression5609);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    AND_ASSIGN68_tree = (Tree) adaptor.create(AND_ASSIGN68);
                                    root_0 = (Tree) adaptor.becomeRoot(AND_ASSIGN68_tree, root_0);
                                }

                            }
                            break;
                            case 7: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:315:17: OR_ASSIGN
                            {
                                OR_ASSIGN69 = (Token) match(input, OR_ASSIGN, FOLLOW_OR_ASSIGN_in_assignmentExpression5628);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    OR_ASSIGN69_tree = (Tree) adaptor.create(OR_ASSIGN69);
                                    root_0 = (Tree) adaptor.becomeRoot(OR_ASSIGN69_tree, root_0);
                                }

                            }
                            break;
                            case 8: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:316:17: XOR_ASSIGN
                            {
                                XOR_ASSIGN70 = (Token) match(input, XOR_ASSIGN, FOLLOW_XOR_ASSIGN_in_assignmentExpression5647);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    XOR_ASSIGN70_tree = (Tree) adaptor.create(XOR_ASSIGN70);
                                    root_0 = (Tree) adaptor.becomeRoot(XOR_ASSIGN70_tree, root_0);
                                }

                            }
                            break;
                            case 9: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:317:17: MOD_ASSIGN
                            {
                                MOD_ASSIGN71 = (Token) match(input, MOD_ASSIGN, FOLLOW_MOD_ASSIGN_in_assignmentExpression5666);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    MOD_ASSIGN71_tree = (Tree) adaptor.create(MOD_ASSIGN71);
                                    root_0 = (Tree) adaptor.becomeRoot(MOD_ASSIGN71_tree, root_0);
                                }

                            }
                            break;
                            case 10: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:318:17: SHIFT_LEFT_ASSIGN
                            {
                                SHIFT_LEFT_ASSIGN72 = (Token) match(input, SHIFT_LEFT_ASSIGN, FOLLOW_SHIFT_LEFT_ASSIGN_in_assignmentExpression5685);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    SHIFT_LEFT_ASSIGN72_tree = (Tree) adaptor.create(SHIFT_LEFT_ASSIGN72);
                                    root_0 = (Tree) adaptor.becomeRoot(SHIFT_LEFT_ASSIGN72_tree, root_0);
                                }

                            }
                            break;
                            case 11: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:319:17: SHIFT_RIGHT_ASSIGN
                            {
                                SHIFT_RIGHT_ASSIGN73 = (Token) match(input, SHIFT_RIGHT_ASSIGN, FOLLOW_SHIFT_RIGHT_ASSIGN_in_assignmentExpression5704);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    SHIFT_RIGHT_ASSIGN73_tree = (Tree) adaptor.create(SHIFT_RIGHT_ASSIGN73);
                                    root_0 = (Tree) adaptor.becomeRoot(SHIFT_RIGHT_ASSIGN73_tree, root_0);
                                }

                            }
                            break;
                            case 12: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:320:17: BIT_SHIFT_RIGHT_ASSIGN
                            {
                                BIT_SHIFT_RIGHT_ASSIGN74 = (Token) match(input, BIT_SHIFT_RIGHT_ASSIGN, FOLLOW_BIT_SHIFT_RIGHT_ASSIGN_in_assignmentExpression5723);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0) {
                                    BIT_SHIFT_RIGHT_ASSIGN74_tree = (Tree) adaptor.create(BIT_SHIFT_RIGHT_ASSIGN74);
                                    root_0 = (Tree) adaptor.becomeRoot(BIT_SHIFT_RIGHT_ASSIGN74_tree, root_0);
                                }

                            }
                            break;

                        }

                        pushFollow(FOLLOW_assignmentExpression_in_assignmentExpression5745);
                        assignmentExpression75 = assignmentExpression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_0, assignmentExpression75.getTree());

                    }
                    break;

                }

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 18, assignmentExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "assignmentExpression"
    public static class conditionalExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "conditionalExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:325:1: conditionalExpression : logicalOrExpression ( QUESTION assignmentExpression COLON conditionalExpression )? ;
    public final VNScriptParser.conditionalExpression_return conditionalExpression() throws RecognitionException
    {
        VNScriptParser.conditionalExpression_return retval = new VNScriptParser.conditionalExpression_return();
        retval.start = input.LT(1);
        int conditionalExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token QUESTION77 = null;
        Token COLON79 = null;
        VNScriptParser.logicalOrExpression_return logicalOrExpression76 = null;

        VNScriptParser.assignmentExpression_return assignmentExpression78 = null;

        VNScriptParser.conditionalExpression_return conditionalExpression80 = null;

        Tree QUESTION77_tree = null;
        Tree COLON79_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 19)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:326:5: ( logicalOrExpression ( QUESTION assignmentExpression COLON conditionalExpression )? )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:326:9: logicalOrExpression ( QUESTION assignmentExpression COLON conditionalExpression )?
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_logicalOrExpression_in_conditionalExpression5770);
                logicalOrExpression76 = logicalOrExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, logicalOrExpression76.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:326:29: ( QUESTION assignmentExpression COLON conditionalExpression )?
                int alt14 = 2;
                int LA14_0 = input.LA(1);

                if ((LA14_0 == QUESTION)) {
                    alt14 = 1;
                }
                switch (alt14) {
                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:326:30: QUESTION assignmentExpression COLON conditionalExpression
                    {
                        QUESTION77 = (Token) match(input, QUESTION, FOLLOW_QUESTION_in_conditionalExpression5773);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            QUESTION77_tree = (Tree) adaptor.create(QUESTION77);
                            root_0 = (Tree) adaptor.becomeRoot(QUESTION77_tree, root_0);
                        }
                        pushFollow(FOLLOW_assignmentExpression_in_conditionalExpression5776);
                        assignmentExpression78 = assignmentExpression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_0, assignmentExpression78.getTree());
                        COLON79 = (Token) match(input, COLON, FOLLOW_COLON_in_conditionalExpression5778);
                        if (state.failed)
                            return retval;
                        pushFollow(FOLLOW_conditionalExpression_in_conditionalExpression5781);
                        conditionalExpression80 = conditionalExpression();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_0, conditionalExpression80.getTree());

                    }
                    break;

                }

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 19, conditionalExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "conditionalExpression"
    public static class logicalOrExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "logicalOrExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:329:1: logicalOrExpression : logicalAndExpression ( LOGICAL_OR logicalAndExpression )* ;
    public final VNScriptParser.logicalOrExpression_return logicalOrExpression() throws RecognitionException
    {
        VNScriptParser.logicalOrExpression_return retval = new VNScriptParser.logicalOrExpression_return();
        retval.start = input.LT(1);
        int logicalOrExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token LOGICAL_OR82 = null;
        VNScriptParser.logicalAndExpression_return logicalAndExpression81 = null;

        VNScriptParser.logicalAndExpression_return logicalAndExpression83 = null;

        Tree LOGICAL_OR82_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 20)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:330:5: ( logicalAndExpression ( LOGICAL_OR logicalAndExpression )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:330:9: logicalAndExpression ( LOGICAL_OR logicalAndExpression )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_logicalAndExpression_in_logicalOrExpression5802);
                logicalAndExpression81 = logicalAndExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, logicalAndExpression81.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:330:30: ( LOGICAL_OR logicalAndExpression )*
                loop15:
                do {
                    int alt15 = 2;
                    int LA15_0 = input.LA(1);

                    if ((LA15_0 == LOGICAL_OR)) {
                        alt15 = 1;
                    }

                    switch (alt15) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:330:31: LOGICAL_OR logicalAndExpression
                        {
                            LOGICAL_OR82 = (Token) match(input, LOGICAL_OR, FOLLOW_LOGICAL_OR_in_logicalOrExpression5805);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0) {
                                LOGICAL_OR82_tree = (Tree) adaptor.create(LOGICAL_OR82);
                                root_0 = (Tree) adaptor.becomeRoot(LOGICAL_OR82_tree, root_0);
                            }
                            pushFollow(FOLLOW_logicalAndExpression_in_logicalOrExpression5808);
                            logicalAndExpression83 = logicalAndExpression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, logicalAndExpression83.getTree());

                        }
                        break;

                        default:
                            break loop15;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 20, logicalOrExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "logicalOrExpression"
    public static class logicalAndExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "logicalAndExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:333:1: logicalAndExpression : inclusiveOrExpression ( LOGICAL_AND inclusiveOrExpression )* ;
    public final VNScriptParser.logicalAndExpression_return logicalAndExpression() throws RecognitionException
    {
        VNScriptParser.logicalAndExpression_return retval = new VNScriptParser.logicalAndExpression_return();
        retval.start = input.LT(1);
        int logicalAndExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token LOGICAL_AND85 = null;
        VNScriptParser.inclusiveOrExpression_return inclusiveOrExpression84 = null;

        VNScriptParser.inclusiveOrExpression_return inclusiveOrExpression86 = null;

        Tree LOGICAL_AND85_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 21)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:334:5: ( inclusiveOrExpression ( LOGICAL_AND inclusiveOrExpression )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:334:9: inclusiveOrExpression ( LOGICAL_AND inclusiveOrExpression )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_inclusiveOrExpression_in_logicalAndExpression5829);
                inclusiveOrExpression84 = inclusiveOrExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, inclusiveOrExpression84.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:334:31: ( LOGICAL_AND inclusiveOrExpression )*
                loop16:
                do {
                    int alt16 = 2;
                    int LA16_0 = input.LA(1);

                    if ((LA16_0 == LOGICAL_AND)) {
                        alt16 = 1;
                    }

                    switch (alt16) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:334:32: LOGICAL_AND inclusiveOrExpression
                        {
                            LOGICAL_AND85 = (Token) match(input, LOGICAL_AND, FOLLOW_LOGICAL_AND_in_logicalAndExpression5832);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0) {
                                LOGICAL_AND85_tree = (Tree) adaptor.create(LOGICAL_AND85);
                                root_0 = (Tree) adaptor.becomeRoot(LOGICAL_AND85_tree, root_0);
                            }
                            pushFollow(FOLLOW_inclusiveOrExpression_in_logicalAndExpression5835);
                            inclusiveOrExpression86 = inclusiveOrExpression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, inclusiveOrExpression86.getTree());

                        }
                        break;

                        default:
                            break loop16;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 21, logicalAndExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "logicalAndExpression"
    public static class inclusiveOrExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "inclusiveOrExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:337:1: inclusiveOrExpression : exclusiveOrExpression ( OR exclusiveOrExpression )* ;
    public final VNScriptParser.inclusiveOrExpression_return inclusiveOrExpression() throws RecognitionException
    {
        VNScriptParser.inclusiveOrExpression_return retval = new VNScriptParser.inclusiveOrExpression_return();
        retval.start = input.LT(1);
        int inclusiveOrExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token OR88 = null;
        VNScriptParser.exclusiveOrExpression_return exclusiveOrExpression87 = null;

        VNScriptParser.exclusiveOrExpression_return exclusiveOrExpression89 = null;

        Tree OR88_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 22)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:338:5: ( exclusiveOrExpression ( OR exclusiveOrExpression )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:338:9: exclusiveOrExpression ( OR exclusiveOrExpression )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression5856);
                exclusiveOrExpression87 = exclusiveOrExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, exclusiveOrExpression87.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:338:31: ( OR exclusiveOrExpression )*
                loop17:
                do {
                    int alt17 = 2;
                    int LA17_0 = input.LA(1);

                    if ((LA17_0 == OR)) {
                        alt17 = 1;
                    }

                    switch (alt17) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:338:32: OR exclusiveOrExpression
                        {
                            OR88 = (Token) match(input, OR, FOLLOW_OR_in_inclusiveOrExpression5859);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0) {
                                OR88_tree = (Tree) adaptor.create(OR88);
                                root_0 = (Tree) adaptor.becomeRoot(OR88_tree, root_0);
                            }
                            pushFollow(FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression5862);
                            exclusiveOrExpression89 = exclusiveOrExpression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, exclusiveOrExpression89.getTree());

                        }
                        break;

                        default:
                            break loop17;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 22, inclusiveOrExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "inclusiveOrExpression"
    public static class exclusiveOrExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "exclusiveOrExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:341:1: exclusiveOrExpression : andExpression ( XOR andExpression )* ;
    public final VNScriptParser.exclusiveOrExpression_return exclusiveOrExpression() throws RecognitionException
    {
        VNScriptParser.exclusiveOrExpression_return retval = new VNScriptParser.exclusiveOrExpression_return();
        retval.start = input.LT(1);
        int exclusiveOrExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token XOR91 = null;
        VNScriptParser.andExpression_return andExpression90 = null;

        VNScriptParser.andExpression_return andExpression92 = null;

        Tree XOR91_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 23)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:342:5: ( andExpression ( XOR andExpression )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:342:9: andExpression ( XOR andExpression )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_andExpression_in_exclusiveOrExpression5883);
                andExpression90 = andExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, andExpression90.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:342:23: ( XOR andExpression )*
                loop18:
                do {
                    int alt18 = 2;
                    int LA18_0 = input.LA(1);

                    if ((LA18_0 == XOR)) {
                        alt18 = 1;
                    }

                    switch (alt18) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:342:24: XOR andExpression
                        {
                            XOR91 = (Token) match(input, XOR, FOLLOW_XOR_in_exclusiveOrExpression5886);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0) {
                                XOR91_tree = (Tree) adaptor.create(XOR91);
                                root_0 = (Tree) adaptor.becomeRoot(XOR91_tree, root_0);
                            }
                            pushFollow(FOLLOW_andExpression_in_exclusiveOrExpression5889);
                            andExpression92 = andExpression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, andExpression92.getTree());

                        }
                        break;

                        default:
                            break loop18;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 23, exclusiveOrExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "exclusiveOrExpression"
    public static class andExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "andExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:345:1: andExpression : equalityExpression ( AND equalityExpression )* ;
    public final VNScriptParser.andExpression_return andExpression() throws RecognitionException
    {
        VNScriptParser.andExpression_return retval = new VNScriptParser.andExpression_return();
        retval.start = input.LT(1);
        int andExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token AND94 = null;
        VNScriptParser.equalityExpression_return equalityExpression93 = null;

        VNScriptParser.equalityExpression_return equalityExpression95 = null;

        Tree AND94_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 24)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:346:5: ( equalityExpression ( AND equalityExpression )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:346:9: equalityExpression ( AND equalityExpression )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_equalityExpression_in_andExpression5910);
                equalityExpression93 = equalityExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, equalityExpression93.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:346:28: ( AND equalityExpression )*
                loop19:
                do {
                    int alt19 = 2;
                    int LA19_0 = input.LA(1);

                    if ((LA19_0 == AND)) {
                        alt19 = 1;
                    }

                    switch (alt19) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:346:29: AND equalityExpression
                        {
                            AND94 = (Token) match(input, AND, FOLLOW_AND_in_andExpression5913);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0) {
                                AND94_tree = (Tree) adaptor.create(AND94);
                                root_0 = (Tree) adaptor.becomeRoot(AND94_tree, root_0);
                            }
                            pushFollow(FOLLOW_equalityExpression_in_andExpression5916);
                            equalityExpression95 = equalityExpression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, equalityExpression95.getTree());

                        }
                        break;

                        default:
                            break loop19;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 24, andExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "andExpression"
    public static class equalityExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "equalityExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:349:1: equalityExpression : instanceOfExpression ( ( EQUAL | NOT_EQUAL ) instanceOfExpression )* ;
    public final VNScriptParser.equalityExpression_return equalityExpression() throws RecognitionException
    {
        VNScriptParser.equalityExpression_return retval = new VNScriptParser.equalityExpression_return();
        retval.start = input.LT(1);
        int equalityExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token EQUAL97 = null;
        Token NOT_EQUAL98 = null;
        VNScriptParser.instanceOfExpression_return instanceOfExpression96 = null;

        VNScriptParser.instanceOfExpression_return instanceOfExpression99 = null;

        Tree EQUAL97_tree = null;
        Tree NOT_EQUAL98_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 25)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:350:5: ( instanceOfExpression ( ( EQUAL | NOT_EQUAL ) instanceOfExpression )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:350:9: instanceOfExpression ( ( EQUAL | NOT_EQUAL ) instanceOfExpression )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_instanceOfExpression_in_equalityExpression5937);
                instanceOfExpression96 = instanceOfExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, instanceOfExpression96.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:351:9: ( ( EQUAL | NOT_EQUAL ) instanceOfExpression )*
                loop21:
                do {
                    int alt21 = 2;
                    int LA21_0 = input.LA(1);

                    if ((LA21_0 == EQUAL || LA21_0 == NOT_EQUAL)) {
                        alt21 = 1;
                    }

                    switch (alt21) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:351:13: ( EQUAL | NOT_EQUAL ) instanceOfExpression
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:351:13: ( EQUAL | NOT_EQUAL )
                            int alt20 = 2;
                            int LA20_0 = input.LA(1);

                            if ((LA20_0 == EQUAL)) {
                                alt20 = 1;
                            } else if ((LA20_0 == NOT_EQUAL)) {
                                alt20 = 2;
                            } else {
                                if (state.backtracking > 0) {
                                    state.failed = true;
                                    return retval;
                                }
                                NoViableAltException nvae
                                    = new NoViableAltException("", 20, 0, input);

                                throw nvae;
                            }
                            switch (alt20) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:351:17: EQUAL
                                {
                                    EQUAL97 = (Token) match(input, EQUAL, FOLLOW_EQUAL_in_equalityExpression5956);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        EQUAL97_tree = (Tree) adaptor.create(EQUAL97);
                                        root_0 = (Tree) adaptor.becomeRoot(EQUAL97_tree, root_0);
                                    }

                                }
                                break;
                                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:352:17: NOT_EQUAL
                                {
                                    NOT_EQUAL98 = (Token) match(input, NOT_EQUAL, FOLLOW_NOT_EQUAL_in_equalityExpression5975);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        NOT_EQUAL98_tree = (Tree) adaptor.create(NOT_EQUAL98);
                                        root_0 = (Tree) adaptor.becomeRoot(NOT_EQUAL98_tree, root_0);
                                    }

                                }
                                break;

                            }

                            pushFollow(FOLLOW_instanceOfExpression_in_equalityExpression6005);
                            instanceOfExpression99 = instanceOfExpression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, instanceOfExpression99.getTree());

                        }
                        break;

                        default:
                            break loop21;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 25, equalityExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "equalityExpression"
    public static class type_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "type"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:358:1: type : IDENT ;
    public final VNScriptParser.type_return type() throws RecognitionException
    {
        VNScriptParser.type_return retval = new VNScriptParser.type_return();
        retval.start = input.LT(1);
        int type_StartIndex = input.index();
        Tree root_0 = null;

        Token IDENT100 = null;

        Tree IDENT100_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 26)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:358:5: ( IDENT )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:358:7: IDENT
            {
                root_0 = (Tree) adaptor.nil();

                IDENT100 = (Token) match(input, IDENT, FOLLOW_IDENT_in_type6028);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0) {
                    IDENT100_tree = (Tree) adaptor.create(IDENT100);
                    adaptor.addChild(root_0, IDENT100_tree);
                }

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 26, type_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "type"
    public static class instanceOfExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "instanceOfExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:360:1: instanceOfExpression : relationalExpression ( INSTANCEOF type )? ;
    public final VNScriptParser.instanceOfExpression_return instanceOfExpression() throws RecognitionException
    {
        VNScriptParser.instanceOfExpression_return retval = new VNScriptParser.instanceOfExpression_return();
        retval.start = input.LT(1);
        int instanceOfExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token INSTANCEOF102 = null;
        VNScriptParser.relationalExpression_return relationalExpression101 = null;

        VNScriptParser.type_return type103 = null;

        Tree INSTANCEOF102_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 27)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:361:5: ( relationalExpression ( INSTANCEOF type )? )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:361:9: relationalExpression ( INSTANCEOF type )?
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_relationalExpression_in_instanceOfExpression6042);
                relationalExpression101 = relationalExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, relationalExpression101.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:361:30: ( INSTANCEOF type )?
                int alt22 = 2;
                int LA22_0 = input.LA(1);

                if ((LA22_0 == INSTANCEOF)) {
                    alt22 = 1;
                }
                switch (alt22) {
                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:361:31: INSTANCEOF type
                    {
                        INSTANCEOF102 = (Token) match(input, INSTANCEOF, FOLLOW_INSTANCEOF_in_instanceOfExpression6045);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0) {
                            INSTANCEOF102_tree = (Tree) adaptor.create(INSTANCEOF102);
                            root_0 = (Tree) adaptor.becomeRoot(INSTANCEOF102_tree, root_0);
                        }
                        pushFollow(FOLLOW_type_in_instanceOfExpression6048);
                        type103 = type();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            adaptor.addChild(root_0, type103.getTree());

                    }
                    break;

                }

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 27, instanceOfExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "instanceOfExpression"
    public static class relationalExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "relationalExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:364:1: relationalExpression : shiftExpression ( ( LESS_OR_EQUAL | GREATER_OR_EQUAL | LESS_THAN | GREATER_THAN ) shiftExpression )* ;
    public final VNScriptParser.relationalExpression_return relationalExpression() throws RecognitionException
    {
        VNScriptParser.relationalExpression_return retval = new VNScriptParser.relationalExpression_return();
        retval.start = input.LT(1);
        int relationalExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token LESS_OR_EQUAL105 = null;
        Token GREATER_OR_EQUAL106 = null;
        Token LESS_THAN107 = null;
        Token GREATER_THAN108 = null;
        VNScriptParser.shiftExpression_return shiftExpression104 = null;

        VNScriptParser.shiftExpression_return shiftExpression109 = null;

        Tree LESS_OR_EQUAL105_tree = null;
        Tree GREATER_OR_EQUAL106_tree = null;
        Tree LESS_THAN107_tree = null;
        Tree GREATER_THAN108_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 28)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:365:5: ( shiftExpression ( ( LESS_OR_EQUAL | GREATER_OR_EQUAL | LESS_THAN | GREATER_THAN ) shiftExpression )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:365:9: shiftExpression ( ( LESS_OR_EQUAL | GREATER_OR_EQUAL | LESS_THAN | GREATER_THAN ) shiftExpression )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_shiftExpression_in_relationalExpression6069);
                shiftExpression104 = shiftExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, shiftExpression104.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:366:9: ( ( LESS_OR_EQUAL | GREATER_OR_EQUAL | LESS_THAN | GREATER_THAN ) shiftExpression )*
                loop24:
                do {
                    int alt24 = 2;
                    int LA24_0 = input.LA(1);

                    if (((LA24_0 >= GREATER_OR_EQUAL && LA24_0 <= GREATER_THAN) || (LA24_0 >= LESS_OR_EQUAL && LA24_0 <= LESS_THAN))) {
                        alt24 = 1;
                    }

                    switch (alt24) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:366:13: ( LESS_OR_EQUAL | GREATER_OR_EQUAL | LESS_THAN | GREATER_THAN ) shiftExpression
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:366:13: ( LESS_OR_EQUAL | GREATER_OR_EQUAL | LESS_THAN | GREATER_THAN )
                            int alt23 = 4;
                            switch (input.LA(1)) {
                                case LESS_OR_EQUAL: {
                                    alt23 = 1;
                                }
                                break;
                                case GREATER_OR_EQUAL: {
                                    alt23 = 2;
                                }
                                break;
                                case LESS_THAN: {
                                    alt23 = 3;
                                }
                                break;
                                case GREATER_THAN: {
                                    alt23 = 4;
                                }
                                break;
                                default:
                                    if (state.backtracking > 0) {
                                        state.failed = true;
                                        return retval;
                                    }
                                    NoViableAltException nvae
                                        = new NoViableAltException("", 23, 0, input);

                                    throw nvae;
                            }

                            switch (alt23) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:366:17: LESS_OR_EQUAL
                                {
                                    LESS_OR_EQUAL105 = (Token) match(input, LESS_OR_EQUAL, FOLLOW_LESS_OR_EQUAL_in_relationalExpression6088);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        LESS_OR_EQUAL105_tree = (Tree) adaptor.create(LESS_OR_EQUAL105);
                                        root_0 = (Tree) adaptor.becomeRoot(LESS_OR_EQUAL105_tree, root_0);
                                    }

                                }
                                break;
                                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:367:17: GREATER_OR_EQUAL
                                {
                                    GREATER_OR_EQUAL106 = (Token) match(input, GREATER_OR_EQUAL, FOLLOW_GREATER_OR_EQUAL_in_relationalExpression6107);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        GREATER_OR_EQUAL106_tree = (Tree) adaptor.create(GREATER_OR_EQUAL106);
                                        root_0 = (Tree) adaptor.becomeRoot(GREATER_OR_EQUAL106_tree, root_0);
                                    }

                                }
                                break;
                                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:368:17: LESS_THAN
                                {
                                    LESS_THAN107 = (Token) match(input, LESS_THAN, FOLLOW_LESS_THAN_in_relationalExpression6126);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        LESS_THAN107_tree = (Tree) adaptor.create(LESS_THAN107);
                                        root_0 = (Tree) adaptor.becomeRoot(LESS_THAN107_tree, root_0);
                                    }

                                }
                                break;
                                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:369:17: GREATER_THAN
                                {
                                    GREATER_THAN108 = (Token) match(input, GREATER_THAN, FOLLOW_GREATER_THAN_in_relationalExpression6145);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        GREATER_THAN108_tree = (Tree) adaptor.create(GREATER_THAN108);
                                        root_0 = (Tree) adaptor.becomeRoot(GREATER_THAN108_tree, root_0);
                                    }

                                }
                                break;

                            }

                            pushFollow(FOLLOW_shiftExpression_in_relationalExpression6174);
                            shiftExpression109 = shiftExpression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, shiftExpression109.getTree());

                        }
                        break;

                        default:
                            break loop24;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 28, relationalExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "relationalExpression"
    public static class shiftExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "shiftExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:375:1: shiftExpression : additiveExpression ( ( BIT_SHIFT_RIGHT | SHIFT_RIGHT | SHIFT_LEFT ) additiveExpression )* ;
    public final VNScriptParser.shiftExpression_return shiftExpression() throws RecognitionException
    {
        VNScriptParser.shiftExpression_return retval = new VNScriptParser.shiftExpression_return();
        retval.start = input.LT(1);
        int shiftExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token BIT_SHIFT_RIGHT111 = null;
        Token SHIFT_RIGHT112 = null;
        Token SHIFT_LEFT113 = null;
        VNScriptParser.additiveExpression_return additiveExpression110 = null;

        VNScriptParser.additiveExpression_return additiveExpression114 = null;

        Tree BIT_SHIFT_RIGHT111_tree = null;
        Tree SHIFT_RIGHT112_tree = null;
        Tree SHIFT_LEFT113_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 29)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:376:5: ( additiveExpression ( ( BIT_SHIFT_RIGHT | SHIFT_RIGHT | SHIFT_LEFT ) additiveExpression )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:376:9: additiveExpression ( ( BIT_SHIFT_RIGHT | SHIFT_RIGHT | SHIFT_LEFT ) additiveExpression )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_additiveExpression_in_shiftExpression6208);
                additiveExpression110 = additiveExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, additiveExpression110.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:377:9: ( ( BIT_SHIFT_RIGHT | SHIFT_RIGHT | SHIFT_LEFT ) additiveExpression )*
                loop26:
                do {
                    int alt26 = 2;
                    int LA26_0 = input.LA(1);

                    if ((LA26_0 == BIT_SHIFT_RIGHT || LA26_0 == SHIFT_LEFT || LA26_0 == SHIFT_RIGHT)) {
                        alt26 = 1;
                    }

                    switch (alt26) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:377:13: ( BIT_SHIFT_RIGHT | SHIFT_RIGHT | SHIFT_LEFT ) additiveExpression
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:377:13: ( BIT_SHIFT_RIGHT | SHIFT_RIGHT | SHIFT_LEFT )
                            int alt25 = 3;
                            switch (input.LA(1)) {
                                case BIT_SHIFT_RIGHT: {
                                    alt25 = 1;
                                }
                                break;
                                case SHIFT_RIGHT: {
                                    alt25 = 2;
                                }
                                break;
                                case SHIFT_LEFT: {
                                    alt25 = 3;
                                }
                                break;
                                default:
                                    if (state.backtracking > 0) {
                                        state.failed = true;
                                        return retval;
                                    }
                                    NoViableAltException nvae
                                        = new NoViableAltException("", 25, 0, input);

                                    throw nvae;
                            }

                            switch (alt25) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:377:17: BIT_SHIFT_RIGHT
                                {
                                    BIT_SHIFT_RIGHT111 = (Token) match(input, BIT_SHIFT_RIGHT, FOLLOW_BIT_SHIFT_RIGHT_in_shiftExpression6226);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        BIT_SHIFT_RIGHT111_tree = (Tree) adaptor.create(BIT_SHIFT_RIGHT111);
                                        root_0 = (Tree) adaptor.becomeRoot(BIT_SHIFT_RIGHT111_tree, root_0);
                                    }

                                }
                                break;
                                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:378:17: SHIFT_RIGHT
                                {
                                    SHIFT_RIGHT112 = (Token) match(input, SHIFT_RIGHT, FOLLOW_SHIFT_RIGHT_in_shiftExpression6245);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        SHIFT_RIGHT112_tree = (Tree) adaptor.create(SHIFT_RIGHT112);
                                        root_0 = (Tree) adaptor.becomeRoot(SHIFT_RIGHT112_tree, root_0);
                                    }

                                }
                                break;
                                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:379:17: SHIFT_LEFT
                                {
                                    SHIFT_LEFT113 = (Token) match(input, SHIFT_LEFT, FOLLOW_SHIFT_LEFT_in_shiftExpression6264);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        SHIFT_LEFT113_tree = (Tree) adaptor.create(SHIFT_LEFT113);
                                        root_0 = (Tree) adaptor.becomeRoot(SHIFT_LEFT113_tree, root_0);
                                    }

                                }
                                break;

                            }

                            pushFollow(FOLLOW_additiveExpression_in_shiftExpression6293);
                            additiveExpression114 = additiveExpression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, additiveExpression114.getTree());

                        }
                        break;

                        default:
                            break loop26;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 29, shiftExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "shiftExpression"
    public static class additiveExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "additiveExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:385:1: additiveExpression : multiplicativeExpression ( ( PLUS | MINUS ) multiplicativeExpression )* ;
    public final VNScriptParser.additiveExpression_return additiveExpression() throws RecognitionException
    {
        VNScriptParser.additiveExpression_return retval = new VNScriptParser.additiveExpression_return();
        retval.start = input.LT(1);
        int additiveExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token PLUS116 = null;
        Token MINUS117 = null;
        VNScriptParser.multiplicativeExpression_return multiplicativeExpression115 = null;

        VNScriptParser.multiplicativeExpression_return multiplicativeExpression118 = null;

        Tree PLUS116_tree = null;
        Tree MINUS117_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 30)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:386:5: ( multiplicativeExpression ( ( PLUS | MINUS ) multiplicativeExpression )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:386:9: multiplicativeExpression ( ( PLUS | MINUS ) multiplicativeExpression )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression6323);
                multiplicativeExpression115 = multiplicativeExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, multiplicativeExpression115.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:387:9: ( ( PLUS | MINUS ) multiplicativeExpression )*
                loop28:
                do {
                    int alt28 = 2;
                    int LA28_0 = input.LA(1);

                    if ((LA28_0 == MINUS || LA28_0 == PLUS)) {
                        alt28 = 1;
                    }

                    switch (alt28) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:387:13: ( PLUS | MINUS ) multiplicativeExpression
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:387:13: ( PLUS | MINUS )
                            int alt27 = 2;
                            int LA27_0 = input.LA(1);

                            if ((LA27_0 == PLUS)) {
                                alt27 = 1;
                            } else if ((LA27_0 == MINUS)) {
                                alt27 = 2;
                            } else {
                                if (state.backtracking > 0) {
                                    state.failed = true;
                                    return retval;
                                }
                                NoViableAltException nvae
                                    = new NoViableAltException("", 27, 0, input);

                                throw nvae;
                            }
                            switch (alt27) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:387:17: PLUS
                                {
                                    PLUS116 = (Token) match(input, PLUS, FOLLOW_PLUS_in_additiveExpression6341);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        PLUS116_tree = (Tree) adaptor.create(PLUS116);
                                        root_0 = (Tree) adaptor.becomeRoot(PLUS116_tree, root_0);
                                    }

                                }
                                break;
                                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:388:17: MINUS
                                {
                                    MINUS117 = (Token) match(input, MINUS, FOLLOW_MINUS_in_additiveExpression6360);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        MINUS117_tree = (Tree) adaptor.create(MINUS117);
                                        root_0 = (Tree) adaptor.becomeRoot(MINUS117_tree, root_0);
                                    }

                                }
                                break;

                            }

                            pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression6389);
                            multiplicativeExpression118 = multiplicativeExpression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, multiplicativeExpression118.getTree());

                        }
                        break;

                        default:
                            break loop28;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 30, additiveExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "additiveExpression"
    public static class multiplicativeExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "multiplicativeExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:394:1: multiplicativeExpression : unaryExpression ( ( STAR | DIV | MOD ) unaryExpression )* ;
    public final VNScriptParser.multiplicativeExpression_return multiplicativeExpression() throws RecognitionException
    {
        VNScriptParser.multiplicativeExpression_return retval = new VNScriptParser.multiplicativeExpression_return();
        retval.start = input.LT(1);
        int multiplicativeExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token STAR120 = null;
        Token DIV121 = null;
        Token MOD122 = null;
        VNScriptParser.unaryExpression_return unaryExpression119 = null;

        VNScriptParser.unaryExpression_return unaryExpression123 = null;

        Tree STAR120_tree = null;
        Tree DIV121_tree = null;
        Tree MOD122_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 31)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:395:5: ( unaryExpression ( ( STAR | DIV | MOD ) unaryExpression )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:395:9: unaryExpression ( ( STAR | DIV | MOD ) unaryExpression )*
            {
                root_0 = (Tree) adaptor.nil();

                pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression6419);
                unaryExpression119 = unaryExpression();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, unaryExpression119.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:396:9: ( ( STAR | DIV | MOD ) unaryExpression )*
                loop30:
                do {
                    int alt30 = 2;
                    int LA30_0 = input.LA(1);

                    if ((LA30_0 == DIV || LA30_0 == MOD || LA30_0 == STAR)) {
                        alt30 = 1;
                    }

                    switch (alt30) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:396:13: ( STAR | DIV | MOD ) unaryExpression
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:396:13: ( STAR | DIV | MOD )
                            int alt29 = 3;
                            switch (input.LA(1)) {
                                case STAR: {
                                    alt29 = 1;
                                }
                                break;
                                case DIV: {
                                    alt29 = 2;
                                }
                                break;
                                case MOD: {
                                    alt29 = 3;
                                }
                                break;
                                default:
                                    if (state.backtracking > 0) {
                                        state.failed = true;
                                        return retval;
                                    }
                                    NoViableAltException nvae
                                        = new NoViableAltException("", 29, 0, input);

                                    throw nvae;
                            }

                            switch (alt29) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:396:17: STAR
                                {
                                    STAR120 = (Token) match(input, STAR, FOLLOW_STAR_in_multiplicativeExpression6438);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        STAR120_tree = (Tree) adaptor.create(STAR120);
                                        root_0 = (Tree) adaptor.becomeRoot(STAR120_tree, root_0);
                                    }

                                }
                                break;
                                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:397:17: DIV
                                {
                                    DIV121 = (Token) match(input, DIV, FOLLOW_DIV_in_multiplicativeExpression6457);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        DIV121_tree = (Tree) adaptor.create(DIV121);
                                        root_0 = (Tree) adaptor.becomeRoot(DIV121_tree, root_0);
                                    }

                                }
                                break;
                                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:398:17: MOD
                                {
                                    MOD122 = (Token) match(input, MOD, FOLLOW_MOD_in_multiplicativeExpression6476);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0) {
                                        MOD122_tree = (Tree) adaptor.create(MOD122);
                                        root_0 = (Tree) adaptor.becomeRoot(MOD122_tree, root_0);
                                    }

                                }
                                break;

                            }

                            pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression6505);
                            unaryExpression123 = unaryExpression();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, unaryExpression123.getTree());

                        }
                        break;

                        default:
                            break loop30;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 31, multiplicativeExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "multiplicativeExpression"
    public static class unaryExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "unaryExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:404:1: unaryExpression : ( PLUS unaryExpression -> ^( UNARY_PLUS[$PLUS, \"UNARY_PLUS\"] unaryExpression ) | MINUS unaryExpression -> ^( UNARY_MINUS[$MINUS, \"UNARY_MINUS\"] unaryExpression ) | INC postfixedExpression -> ^( PRE_INC[$INC, \"PRE_INC\"] postfixedExpression ) | DEC postfixedExpression -> ^( PRE_DEC[$DEC, \"PRE_DEC\"] postfixedExpression ) | unaryExpressionNotPlusMinus );
    public final VNScriptParser.unaryExpression_return unaryExpression() throws RecognitionException
    {
        VNScriptParser.unaryExpression_return retval = new VNScriptParser.unaryExpression_return();
        retval.start = input.LT(1);
        int unaryExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token PLUS124 = null;
        Token MINUS126 = null;
        Token INC128 = null;
        Token DEC130 = null;
        VNScriptParser.unaryExpression_return unaryExpression125 = null;

        VNScriptParser.unaryExpression_return unaryExpression127 = null;

        VNScriptParser.postfixedExpression_return postfixedExpression129 = null;

        VNScriptParser.postfixedExpression_return postfixedExpression131 = null;

        VNScriptParser.unaryExpressionNotPlusMinus_return unaryExpressionNotPlusMinus132 = null;

        Tree PLUS124_tree = null;
        Tree MINUS126_tree = null;
        Tree INC128_tree = null;
        Tree DEC130_tree = null;
        RewriteRuleTokenStream stream_DEC = new RewriteRuleTokenStream(adaptor, "token DEC");
        RewriteRuleTokenStream stream_INC = new RewriteRuleTokenStream(adaptor, "token INC");
        RewriteRuleTokenStream stream_PLUS = new RewriteRuleTokenStream(adaptor, "token PLUS");
        RewriteRuleTokenStream stream_MINUS = new RewriteRuleTokenStream(adaptor, "token MINUS");
        RewriteRuleSubtreeStream stream_postfixedExpression = new RewriteRuleSubtreeStream(adaptor, "rule postfixedExpression");
        RewriteRuleSubtreeStream stream_unaryExpression = new RewriteRuleSubtreeStream(adaptor, "rule unaryExpression");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 32)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:405:5: ( PLUS unaryExpression -> ^( UNARY_PLUS[$PLUS, \"UNARY_PLUS\"] unaryExpression ) | MINUS unaryExpression -> ^( UNARY_MINUS[$MINUS, \"UNARY_MINUS\"] unaryExpression ) | INC postfixedExpression -> ^( PRE_INC[$INC, \"PRE_INC\"] postfixedExpression ) | DEC postfixedExpression -> ^( PRE_DEC[$DEC, \"PRE_DEC\"] postfixedExpression ) | unaryExpressionNotPlusMinus )
            int alt31 = 5;
            switch (input.LA(1)) {
                case PLUS: {
                    alt31 = 1;
                }
                break;
                case MINUS: {
                    alt31 = 2;
                }
                break;
                case INC: {
                    alt31 = 3;
                }
                break;
                case DEC: {
                    alt31 = 4;
                }
                break;
                case LESS_THAN:
                case LOGICAL_NOT:
                case LPAREN:
                case NOT:
                case BOOLEAN:
                case BYTE:
                case CHAR:
                case DOUBLE:
                case FALSE:
                case FLOAT:
                case INT:
                case LONG:
                case NULL:
                case SHORT:
                case SUPER:
                case THIS:
                case TRUE:
                case VOID:
                case IDENT:
                case HEX_LITERAL:
                case OCTAL_LITERAL:
                case DECIMAL_LITERAL:
                case FLOATING_POINT_LITERAL:
                case CHARACTER_LITERAL:
                case STRING_LITERAL: {
                    alt31 = 5;
                }
                break;
                default:
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 31, 0, input);

                    throw nvae;
            }

            switch (alt31) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:405:9: PLUS unaryExpression
                {
                    PLUS124 = (Token) match(input, PLUS, FOLLOW_PLUS_in_unaryExpression6539);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_PLUS.add(PLUS124);

                    pushFollow(FOLLOW_unaryExpression_in_unaryExpression6541);
                    unaryExpression125 = unaryExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_unaryExpression.add(unaryExpression125.getTree());

                    // AST REWRITE
                    // elements: unaryExpression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 405:37: -> ^( UNARY_PLUS[$PLUS, \"UNARY_PLUS\"] unaryExpression )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:405:41: ^( UNARY_PLUS[$PLUS, \"UNARY_PLUS\"] unaryExpression )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(UNARY_PLUS, PLUS124, "UNARY_PLUS"), root_1);

                                adaptor.addChild(root_1, stream_unaryExpression.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:406:9: MINUS unaryExpression
                {
                    MINUS126 = (Token) match(input, MINUS, FOLLOW_MINUS_in_unaryExpression6568);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_MINUS.add(MINUS126);

                    pushFollow(FOLLOW_unaryExpression_in_unaryExpression6570);
                    unaryExpression127 = unaryExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_unaryExpression.add(unaryExpression127.getTree());

                    // AST REWRITE
                    // elements: unaryExpression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 406:37: -> ^( UNARY_MINUS[$MINUS, \"UNARY_MINUS\"] unaryExpression )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:406:41: ^( UNARY_MINUS[$MINUS, \"UNARY_MINUS\"] unaryExpression )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(UNARY_MINUS, MINUS126, "UNARY_MINUS"), root_1);

                                adaptor.addChild(root_1, stream_unaryExpression.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:407:9: INC postfixedExpression
                {
                    INC128 = (Token) match(input, INC, FOLLOW_INC_in_unaryExpression6596);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_INC.add(INC128);

                    pushFollow(FOLLOW_postfixedExpression_in_unaryExpression6598);
                    postfixedExpression129 = postfixedExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_postfixedExpression.add(postfixedExpression129.getTree());

                    // AST REWRITE
                    // elements: postfixedExpression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 407:37: -> ^( PRE_INC[$INC, \"PRE_INC\"] postfixedExpression )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:407:41: ^( PRE_INC[$INC, \"PRE_INC\"] postfixedExpression )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(PRE_INC, INC128, "PRE_INC"), root_1);

                                adaptor.addChild(root_1, stream_postfixedExpression.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:408:9: DEC postfixedExpression
                {
                    DEC130 = (Token) match(input, DEC, FOLLOW_DEC_in_unaryExpression6622);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_DEC.add(DEC130);

                    pushFollow(FOLLOW_postfixedExpression_in_unaryExpression6624);
                    postfixedExpression131 = postfixedExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_postfixedExpression.add(postfixedExpression131.getTree());

                    // AST REWRITE
                    // elements: postfixedExpression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 408:37: -> ^( PRE_DEC[$DEC, \"PRE_DEC\"] postfixedExpression )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:408:41: ^( PRE_DEC[$DEC, \"PRE_DEC\"] postfixedExpression )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(PRE_DEC, DEC130, "PRE_DEC"), root_1);

                                adaptor.addChild(root_1, stream_postfixedExpression.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 5: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:409:9: unaryExpressionNotPlusMinus
                {
                    root_0 = (Tree) adaptor.nil();

                    pushFollow(FOLLOW_unaryExpressionNotPlusMinus_in_unaryExpression6648);
                    unaryExpressionNotPlusMinus132 = unaryExpressionNotPlusMinus();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, unaryExpressionNotPlusMinus132.getTree());

                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 32, unaryExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "unaryExpression"
    public static class unaryExpressionNotPlusMinus_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "unaryExpressionNotPlusMinus"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:412:1: unaryExpressionNotPlusMinus : ( NOT unaryExpression -> ^( NOT unaryExpression ) | LOGICAL_NOT unaryExpression -> ^( LOGICAL_NOT unaryExpression ) | LPAREN type RPAREN unaryExpression -> ^( CAST_EXPR[$LPAREN, \"CAST_EXPR\"] type unaryExpression ) | postfixedExpression );
    public final VNScriptParser.unaryExpressionNotPlusMinus_return unaryExpressionNotPlusMinus() throws RecognitionException
    {
        VNScriptParser.unaryExpressionNotPlusMinus_return retval = new VNScriptParser.unaryExpressionNotPlusMinus_return();
        retval.start = input.LT(1);
        int unaryExpressionNotPlusMinus_StartIndex = input.index();
        Tree root_0 = null;

        Token NOT133 = null;
        Token LOGICAL_NOT135 = null;
        Token LPAREN137 = null;
        Token RPAREN139 = null;
        VNScriptParser.unaryExpression_return unaryExpression134 = null;

        VNScriptParser.unaryExpression_return unaryExpression136 = null;

        VNScriptParser.type_return type138 = null;

        VNScriptParser.unaryExpression_return unaryExpression140 = null;

        VNScriptParser.postfixedExpression_return postfixedExpression141 = null;

        Tree NOT133_tree = null;
        Tree LOGICAL_NOT135_tree = null;
        Tree LPAREN137_tree = null;
        Tree RPAREN139_tree = null;
        RewriteRuleTokenStream stream_RPAREN = new RewriteRuleTokenStream(adaptor, "token RPAREN");
        RewriteRuleTokenStream stream_LOGICAL_NOT = new RewriteRuleTokenStream(adaptor, "token LOGICAL_NOT");
        RewriteRuleTokenStream stream_NOT = new RewriteRuleTokenStream(adaptor, "token NOT");
        RewriteRuleTokenStream stream_LPAREN = new RewriteRuleTokenStream(adaptor, "token LPAREN");
        RewriteRuleSubtreeStream stream_unaryExpression = new RewriteRuleSubtreeStream(adaptor, "rule unaryExpression");
        RewriteRuleSubtreeStream stream_type = new RewriteRuleSubtreeStream(adaptor, "rule type");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 33)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:413:5: ( NOT unaryExpression -> ^( NOT unaryExpression ) | LOGICAL_NOT unaryExpression -> ^( LOGICAL_NOT unaryExpression ) | LPAREN type RPAREN unaryExpression -> ^( CAST_EXPR[$LPAREN, \"CAST_EXPR\"] type unaryExpression ) | postfixedExpression )
            int alt32 = 4;
            alt32 = dfa32.predict(input);
            switch (alt32) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:413:9: NOT unaryExpression
                {
                    NOT133 = (Token) match(input, NOT, FOLLOW_NOT_in_unaryExpressionNotPlusMinus6667);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_NOT.add(NOT133);

                    pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus6669);
                    unaryExpression134 = unaryExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_unaryExpression.add(unaryExpression134.getTree());

                    // AST REWRITE
                    // elements: NOT, unaryExpression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 413:57: -> ^( NOT unaryExpression )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:413:61: ^( NOT unaryExpression )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot(stream_NOT.nextNode(), root_1);

                                adaptor.addChild(root_1, stream_unaryExpression.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:414:9: LOGICAL_NOT unaryExpression
                {
                    LOGICAL_NOT135 = (Token) match(input, LOGICAL_NOT, FOLLOW_LOGICAL_NOT_in_unaryExpressionNotPlusMinus6716);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_LOGICAL_NOT.add(LOGICAL_NOT135);

                    pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus6718);
                    unaryExpression136 = unaryExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_unaryExpression.add(unaryExpression136.getTree());

                    // AST REWRITE
                    // elements: unaryExpression, LOGICAL_NOT
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 414:57: -> ^( LOGICAL_NOT unaryExpression )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:414:61: ^( LOGICAL_NOT unaryExpression )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot(stream_LOGICAL_NOT.nextNode(), root_1);

                                adaptor.addChild(root_1, stream_unaryExpression.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:415:9: LPAREN type RPAREN unaryExpression
                {
                    LPAREN137 = (Token) match(input, LPAREN, FOLLOW_LPAREN_in_unaryExpressionNotPlusMinus6757);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_LPAREN.add(LPAREN137);

                    pushFollow(FOLLOW_type_in_unaryExpressionNotPlusMinus6759);
                    type138 = type();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_type.add(type138.getTree());
                    RPAREN139 = (Token) match(input, RPAREN, FOLLOW_RPAREN_in_unaryExpressionNotPlusMinus6761);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_RPAREN.add(RPAREN139);

                    pushFollow(FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus6763);
                    unaryExpression140 = unaryExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_unaryExpression.add(unaryExpression140.getTree());

                    // AST REWRITE
                    // elements: type, unaryExpression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 415:57: -> ^( CAST_EXPR[$LPAREN, \"CAST_EXPR\"] type unaryExpression )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:415:61: ^( CAST_EXPR[$LPAREN, \"CAST_EXPR\"] type unaryExpression )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(CAST_EXPR, LPAREN137, "CAST_EXPR"), root_1);

                                adaptor.addChild(root_1, stream_type.nextTree());
                                adaptor.addChild(root_1, stream_unaryExpression.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:416:9: postfixedExpression
                {
                    root_0 = (Tree) adaptor.nil();

                    pushFollow(FOLLOW_postfixedExpression_in_unaryExpressionNotPlusMinus6798);
                    postfixedExpression141 = postfixedExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, postfixedExpression141.getTree());

                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 33, unaryExpressionNotPlusMinus_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "unaryExpressionNotPlusMinus"
    public static class postfixedExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "postfixedExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:421:1: postfixedExpression : ( primaryExpression -> primaryExpression ) (outerDot= DOT ( ( ( genericTypeArgumentListSimplified )? -> ^( DOT $postfixedExpression IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression ( genericTypeArgumentListSimplified )? arguments ) )? | THIS -> ^( DOT $postfixedExpression THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] $postfixedExpression arguments ) | ( SUPER innerDot= DOT IDENT -> ^( $innerDot ^( $outerDot $postfixedExpression SUPER ) IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression) )? ) | LBRACK rangeList RBRACK ( DOT IDENT )? -> ^( ARRAY_ELEMENT_ACCESS $postfixedExpression rangeList ( IDENT )? ) )* ( INC -> ^( POST_INC[$INC, \"POST_INC\"] $postfixedExpression) | DEC -> ^( POST_DEC[$DEC, \"POST_DEC\"] $postfixedExpression) )? ;
    public final VNScriptParser.postfixedExpression_return postfixedExpression() throws RecognitionException
    {
        VNScriptParser.postfixedExpression_return retval = new VNScriptParser.postfixedExpression_return();
        retval.start = input.LT(1);
        int postfixedExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token outerDot = null;
        Token Super = null;
        Token innerDot = null;
        Token THIS145 = null;
        Token SUPER147 = null;
        Token IDENT148 = null;
        Token LBRACK150 = null;
        Token RBRACK152 = null;
        Token DOT153 = null;
        Token IDENT154 = null;
        Token INC155 = null;
        Token DEC156 = null;
        VNScriptParser.primaryExpression_return primaryExpression142 = null;

        VNScriptParser.genericTypeArgumentListSimplified_return genericTypeArgumentListSimplified143 = null;

        VNScriptParser.arguments_return arguments144 = null;

        VNScriptParser.arguments_return arguments146 = null;

        VNScriptParser.arguments_return arguments149 = null;

        VNScriptParser.rangeList_return rangeList151 = null;

        Tree outerDot_tree = null;
        Tree Super_tree = null;
        Tree innerDot_tree = null;
        Tree THIS145_tree = null;
        Tree SUPER147_tree = null;
        Tree IDENT148_tree = null;
        Tree LBRACK150_tree = null;
        Tree RBRACK152_tree = null;
        Tree DOT153_tree = null;
        Tree IDENT154_tree = null;
        Tree INC155_tree = null;
        Tree DEC156_tree = null;
        RewriteRuleTokenStream stream_RBRACK = new RewriteRuleTokenStream(adaptor, "token RBRACK");
        RewriteRuleTokenStream stream_IDENT = new RewriteRuleTokenStream(adaptor, "token IDENT");
        RewriteRuleTokenStream stream_INC = new RewriteRuleTokenStream(adaptor, "token INC");
        RewriteRuleTokenStream stream_DEC = new RewriteRuleTokenStream(adaptor, "token DEC");
        RewriteRuleTokenStream stream_LBRACK = new RewriteRuleTokenStream(adaptor, "token LBRACK");
        RewriteRuleTokenStream stream_SUPER = new RewriteRuleTokenStream(adaptor, "token SUPER");
        RewriteRuleTokenStream stream_DOT = new RewriteRuleTokenStream(adaptor, "token DOT");
        RewriteRuleTokenStream stream_THIS = new RewriteRuleTokenStream(adaptor, "token THIS");
        RewriteRuleSubtreeStream stream_arguments = new RewriteRuleSubtreeStream(adaptor, "rule arguments");
        RewriteRuleSubtreeStream stream_rangeList = new RewriteRuleSubtreeStream(adaptor, "rule rangeList");
        RewriteRuleSubtreeStream stream_primaryExpression = new RewriteRuleSubtreeStream(adaptor, "rule primaryExpression");
        RewriteRuleSubtreeStream stream_genericTypeArgumentListSimplified = new RewriteRuleSubtreeStream(adaptor, "rule genericTypeArgumentListSimplified");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 34)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:423:5: ( ( primaryExpression -> primaryExpression ) (outerDot= DOT ( ( ( genericTypeArgumentListSimplified )? -> ^( DOT $postfixedExpression IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression ( genericTypeArgumentListSimplified )? arguments ) )? | THIS -> ^( DOT $postfixedExpression THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] $postfixedExpression arguments ) | ( SUPER innerDot= DOT IDENT -> ^( $innerDot ^( $outerDot $postfixedExpression SUPER ) IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression) )? ) | LBRACK rangeList RBRACK ( DOT IDENT )? -> ^( ARRAY_ELEMENT_ACCESS $postfixedExpression rangeList ( IDENT )? ) )* ( INC -> ^( POST_INC[$INC, \"POST_INC\"] $postfixedExpression) | DEC -> ^( POST_DEC[$DEC, \"POST_DEC\"] $postfixedExpression) )? )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:423:9: ( primaryExpression -> primaryExpression ) (outerDot= DOT ( ( ( genericTypeArgumentListSimplified )? -> ^( DOT $postfixedExpression IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression ( genericTypeArgumentListSimplified )? arguments ) )? | THIS -> ^( DOT $postfixedExpression THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] $postfixedExpression arguments ) | ( SUPER innerDot= DOT IDENT -> ^( $innerDot ^( $outerDot $postfixedExpression SUPER ) IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression) )? ) | LBRACK rangeList RBRACK ( DOT IDENT )? -> ^( ARRAY_ELEMENT_ACCESS $postfixedExpression rangeList ( IDENT )? ) )* ( INC -> ^( POST_INC[$INC, \"POST_INC\"] $postfixedExpression) | DEC -> ^( POST_DEC[$DEC, \"POST_DEC\"] $postfixedExpression) )?
            {
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:423:9: ( primaryExpression -> primaryExpression )
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:423:13: primaryExpression
                {
                    pushFollow(FOLLOW_primaryExpression_in_postfixedExpression6844);
                    primaryExpression142 = primaryExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_primaryExpression.add(primaryExpression142.getTree());

                    // AST REWRITE
                    // elements: primaryExpression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 423:53: -> primaryExpression
                        {
                            adaptor.addChild(root_0, stream_primaryExpression.nextTree());

                        }

                        retval.tree = root_0;
                    }
                }

                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:426:9: (outerDot= DOT ( ( ( genericTypeArgumentListSimplified )? -> ^( DOT $postfixedExpression IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression ( genericTypeArgumentListSimplified )? arguments ) )? | THIS -> ^( DOT $postfixedExpression THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] $postfixedExpression arguments ) | ( SUPER innerDot= DOT IDENT -> ^( $innerDot ^( $outerDot $postfixedExpression SUPER ) IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression) )? ) | LBRACK rangeList RBRACK ( DOT IDENT )? -> ^( ARRAY_ELEMENT_ACCESS $postfixedExpression rangeList ( IDENT )? ) )*
                loop38:
                do {
                    int alt38 = 3;
                    int LA38_0 = input.LA(1);

                    if ((LA38_0 == DOT)) {
                        alt38 = 1;
                    } else if ((LA38_0 == LBRACK)) {
                        alt38 = 2;
                    }

                    switch (alt38) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:426:13: outerDot= DOT ( ( ( genericTypeArgumentListSimplified )? -> ^( DOT $postfixedExpression IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression ( genericTypeArgumentListSimplified )? arguments ) )? | THIS -> ^( DOT $postfixedExpression THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] $postfixedExpression arguments ) | ( SUPER innerDot= DOT IDENT -> ^( $innerDot ^( $outerDot $postfixedExpression SUPER ) IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression) )? )
                        {
                            outerDot = (Token) match(input, DOT, FOLLOW_DOT_in_postfixedExpression6906);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_DOT.add(outerDot);

                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:427:13: ( ( ( genericTypeArgumentListSimplified )? -> ^( DOT $postfixedExpression IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression ( genericTypeArgumentListSimplified )? arguments ) )? | THIS -> ^( DOT $postfixedExpression THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] $postfixedExpression arguments ) | ( SUPER innerDot= DOT IDENT -> ^( $innerDot ^( $outerDot $postfixedExpression SUPER ) IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression) )? )
                            int alt36 = 4;
                            switch (input.LA(1)) {
                                case EOF:
                                case AND:
                                case AND_ASSIGN:
                                case ASSIGN:
                                case BIT_SHIFT_RIGHT:
                                case BIT_SHIFT_RIGHT_ASSIGN:
                                case COLON:
                                case COMMA:
                                case DEC:
                                case DIV:
                                case DIV_ASSIGN:
                                case DOT:
                                case EQUAL:
                                case GREATER_OR_EQUAL:
                                case GREATER_THAN:
                                case INC:
                                case LBRACK:
                                case LESS_OR_EQUAL:
                                case LESS_THAN:
                                case LOGICAL_AND:
                                case LOGICAL_OR:
                                case LPAREN:
                                case MINUS:
                                case MINUS_ASSIGN:
                                case MOD:
                                case MOD_ASSIGN:
                                case NOT_EQUAL:
                                case OR:
                                case OR_ASSIGN:
                                case PLUS:
                                case PLUS_ASSIGN:
                                case QUESTION:
                                case RBRACK:
                                case RPAREN:
                                case SEMI:
                                case SHIFT_LEFT:
                                case SHIFT_LEFT_ASSIGN:
                                case SHIFT_RIGHT:
                                case SHIFT_RIGHT_ASSIGN:
                                case STAR:
                                case STAR_ASSIGN:
                                case XOR:
                                case XOR_ASSIGN:
                                case INSTANCEOF: {
                                    alt36 = 1;
                                }
                                break;
                                case THIS: {
                                    alt36 = 2;
                                }
                                break;
                                case SUPER: {
                                    int LA36_3 = input.LA(2);

                                    if ((LA36_3 == DOT)) {
                                        alt36 = 4;
                                    } else if ((LA36_3 == LPAREN)) {
                                        alt36 = 3;
                                    } else {
                                        if (state.backtracking > 0) {
                                            state.failed = true;
                                            return retval;
                                        }
                                        NoViableAltException nvae
                                            = new NoViableAltException("", 36, 3, input);

                                        throw nvae;
                                    }
                                }
                                break;
                                default:
                                    if (state.backtracking > 0) {
                                        state.failed = true;
                                        return retval;
                                    }
                                    NoViableAltException nvae
                                        = new NoViableAltException("", 36, 0, input);

                                    throw nvae;
                            }

                            switch (alt36) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:427:17: ( ( genericTypeArgumentListSimplified )? -> ^( DOT $postfixedExpression IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression ( genericTypeArgumentListSimplified )? arguments ) )?
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:427:17: ( ( genericTypeArgumentListSimplified )? -> ^( DOT $postfixedExpression IDENT ) )
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:427:21: ( genericTypeArgumentListSimplified )?
                                    {
                                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:427:21: ( genericTypeArgumentListSimplified )?
                                        int alt33 = 2;
                                        int LA33_0 = input.LA(1);

                                        if ((LA33_0 == LESS_THAN)) {
                                            int LA33_1 = input.LA(2);

                                            if ((LA33_1 == IDENT)) {
                                                int LA33_3 = input.LA(3);

                                                if ((synpred63_VNScript())) {
                                                    alt33 = 1;
                                                }
                                            } else if ((LA33_1 == QUESTION)) {
                                                alt33 = 1;
                                            }
                                        }
                                        switch (alt33) {
                                            case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:0:0: genericTypeArgumentListSimplified
                                            {
                                                pushFollow(FOLLOW_genericTypeArgumentListSimplified_in_postfixedExpression6956);
                                                genericTypeArgumentListSimplified143 = genericTypeArgumentListSimplified();

                                                state._fsp--;
                                                if (state.failed)
                                                    return retval;
                                                if (state.backtracking == 0)
                                                    stream_genericTypeArgumentListSimplified.add(genericTypeArgumentListSimplified143.getTree());

                                            }
                                            break;

                                        }

                                        // AST REWRITE
                                        // elements: DOT, postfixedExpression
                                        // token labels: 
                                        // rule labels: retval
                                        // token list labels: 
                                        // rule list labels: 
                                        // wildcard labels: 
                                        if (state.backtracking == 0) {
                                            retval.tree = root_0;
                                            RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                            root_0 = (Tree) adaptor.nil();
                                            // 429:48: -> ^( DOT $postfixedExpression IDENT )
                                            {
                                                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:429:52: ^( DOT $postfixedExpression IDENT )
                                                {
                                                    Tree root_1 = (Tree) adaptor.nil();
                                                    root_1 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_1);

                                                    adaptor.addChild(root_1, stream_retval.nextTree());
                                                    adaptor.addChild(root_1, stream_IDENT.nextNode());

                                                    adaptor.addChild(root_0, root_1);
                                                }

                                            }

                                            retval.tree = root_0;
                                        }
                                    }

                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:431:17: ( arguments -> ^( METHOD_CALL $postfixedExpression ( genericTypeArgumentListSimplified )? arguments ) )?
                                    int alt34 = 2;
                                    int LA34_0 = input.LA(1);

                                    if ((LA34_0 == LPAREN)) {
                                        alt34 = 1;
                                    }
                                    switch (alt34) {
                                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:431:21: arguments
                                        {
                                            pushFollow(FOLLOW_arguments_in_postfixedExpression7116);
                                            arguments144 = arguments();

                                            state._fsp--;
                                            if (state.failed)
                                                return retval;
                                            if (state.backtracking == 0)
                                                stream_arguments.add(arguments144.getTree());

                                            // AST REWRITE
                                            // elements: genericTypeArgumentListSimplified, postfixedExpression, arguments
                                            // token labels: 
                                            // rule labels: retval
                                            // token list labels: 
                                            // rule list labels: 
                                            // wildcard labels: 
                                            if (state.backtracking == 0) {
                                                retval.tree = root_0;
                                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                                root_0 = (Tree) adaptor.nil();
                                                // 431:53: -> ^( METHOD_CALL $postfixedExpression ( genericTypeArgumentListSimplified )? arguments )
                                                {
                                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:431:57: ^( METHOD_CALL $postfixedExpression ( genericTypeArgumentListSimplified )? arguments )
                                                    {
                                                        Tree root_1 = (Tree) adaptor.nil();
                                                        root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(METHOD_CALL, "METHOD_CALL"), root_1);

                                                        adaptor.addChild(root_1, stream_retval.nextTree());
                                                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:431:92: ( genericTypeArgumentListSimplified )?
                                                        if (stream_genericTypeArgumentListSimplified.hasNext()) {
                                                            adaptor.addChild(root_1, stream_genericTypeArgumentListSimplified.nextTree());

                                                        }
                                                        stream_genericTypeArgumentListSimplified.reset();
                                                        adaptor.addChild(root_1, stream_arguments.nextTree());

                                                        adaptor.addChild(root_0, root_1);
                                                    }

                                                }

                                                retval.tree = root_0;
                                            }
                                        }
                                        break;

                                    }

                                }
                                break;
                                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:433:17: THIS
                                {
                                    THIS145 = (Token) match(input, THIS, FOLLOW_THIS_in_postfixedExpression7190);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        stream_THIS.add(THIS145);

                                    // AST REWRITE
                                    // elements: THIS, DOT, postfixedExpression
                                    // token labels: 
                                    // rule labels: retval
                                    // token list labels: 
                                    // rule list labels: 
                                    // wildcard labels: 
                                    if (state.backtracking == 0) {
                                        retval.tree = root_0;
                                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                        root_0 = (Tree) adaptor.nil();
                                        // 433:53: -> ^( DOT $postfixedExpression THIS )
                                        {
                                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:433:57: ^( DOT $postfixedExpression THIS )
                                            {
                                                Tree root_1 = (Tree) adaptor.nil();
                                                root_1 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_1);

                                                adaptor.addChild(root_1, stream_retval.nextTree());
                                                adaptor.addChild(root_1, stream_THIS.nextNode());

                                                adaptor.addChild(root_0, root_1);
                                            }

                                        }

                                        retval.tree = root_0;
                                    }
                                }
                                break;
                                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:434:17: Super= SUPER arguments
                                {
                                    Super = (Token) match(input, SUPER, FOLLOW_SUPER_in_postfixedExpression7253);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        stream_SUPER.add(Super);

                                    pushFollow(FOLLOW_arguments_in_postfixedExpression7255);
                                    arguments146 = arguments();

                                    state._fsp--;
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        stream_arguments.add(arguments146.getTree());

                                    // AST REWRITE
                                    // elements: arguments, postfixedExpression
                                    // token labels: 
                                    // rule labels: retval
                                    // token list labels: 
                                    // rule list labels: 
                                    // wildcard labels: 
                                    if (state.backtracking == 0) {
                                        retval.tree = root_0;
                                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                        root_0 = (Tree) adaptor.nil();
                                        // 434:57: -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] $postfixedExpression arguments )
                                        {
                                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:434:61: ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] $postfixedExpression arguments )
                                            {
                                                Tree root_1 = (Tree) adaptor.nil();
                                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(SUPER_CONSTRUCTOR_CALL, Super, "SUPER_CONSTRUCTOR_CALL"), root_1);

                                                adaptor.addChild(root_1, stream_retval.nextTree());
                                                adaptor.addChild(root_1, stream_arguments.nextTree());

                                                adaptor.addChild(root_0, root_1);
                                            }

                                        }

                                        retval.tree = root_0;
                                    }
                                }
                                break;
                                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:435:17: ( SUPER innerDot= DOT IDENT -> ^( $innerDot ^( $outerDot $postfixedExpression SUPER ) IDENT ) ) ( arguments -> ^( METHOD_CALL $postfixedExpression) )?
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:435:17: ( SUPER innerDot= DOT IDENT -> ^( $innerDot ^( $outerDot $postfixedExpression SUPER ) IDENT ) )
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:435:21: SUPER innerDot= DOT IDENT
                                    {
                                        SUPER147 = (Token) match(input, SUPER, FOLLOW_SUPER_in_postfixedExpression7308);
                                        if (state.failed)
                                            return retval;
                                        if (state.backtracking == 0)
                                            stream_SUPER.add(SUPER147);

                                        innerDot = (Token) match(input, DOT, FOLLOW_DOT_in_postfixedExpression7312);
                                        if (state.failed)
                                            return retval;
                                        if (state.backtracking == 0)
                                            stream_DOT.add(innerDot);

                                        IDENT148 = (Token) match(input, IDENT, FOLLOW_IDENT_in_postfixedExpression7314);
                                        if (state.failed)
                                            return retval;
                                        if (state.backtracking == 0)
                                            stream_IDENT.add(IDENT148);

                                        // AST REWRITE
                                        // elements: innerDot, postfixedExpression, outerDot, IDENT, SUPER
                                        // token labels: outerDot, innerDot
                                        // rule labels: retval
                                        // token list labels: 
                                        // rule list labels: 
                                        // wildcard labels: 
                                        if (state.backtracking == 0) {
                                            retval.tree = root_0;
                                            RewriteRuleTokenStream stream_outerDot = new RewriteRuleTokenStream(adaptor, "token outerDot", outerDot);
                                            RewriteRuleTokenStream stream_innerDot = new RewriteRuleTokenStream(adaptor, "token innerDot", innerDot);
                                            RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                            root_0 = (Tree) adaptor.nil();
                                            // 435:53: -> ^( $innerDot ^( $outerDot $postfixedExpression SUPER ) IDENT )
                                            {
                                                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:435:57: ^( $innerDot ^( $outerDot $postfixedExpression SUPER ) IDENT )
                                                {
                                                    Tree root_1 = (Tree) adaptor.nil();
                                                    root_1 = (Tree) adaptor.becomeRoot(stream_innerDot.nextNode(), root_1);

                                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:435:69: ^( $outerDot $postfixedExpression SUPER )
                                                    {
                                                        Tree root_2 = (Tree) adaptor.nil();
                                                        root_2 = (Tree) adaptor.becomeRoot(stream_outerDot.nextNode(), root_2);

                                                        adaptor.addChild(root_2, stream_retval.nextTree());
                                                        adaptor.addChild(root_2, stream_SUPER.nextNode());

                                                        adaptor.addChild(root_1, root_2);
                                                    }
                                                    adaptor.addChild(root_1, stream_IDENT.nextNode());

                                                    adaptor.addChild(root_0, root_1);
                                                }

                                            }

                                            retval.tree = root_0;
                                        }
                                    }

                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:437:17: ( arguments -> ^( METHOD_CALL $postfixedExpression) )?
                                    int alt35 = 2;
                                    int LA35_0 = input.LA(1);

                                    if ((LA35_0 == LPAREN)) {
                                        alt35 = 1;
                                    }
                                    switch (alt35) {
                                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:437:21: arguments
                                        {
                                            pushFollow(FOLLOW_arguments_in_postfixedExpression7381);
                                            arguments149 = arguments();

                                            state._fsp--;
                                            if (state.failed)
                                                return retval;
                                            if (state.backtracking == 0)
                                                stream_arguments.add(arguments149.getTree());

                                            // AST REWRITE
                                            // elements: postfixedExpression
                                            // token labels: 
                                            // rule labels: retval
                                            // token list labels: 
                                            // rule list labels: 
                                            // wildcard labels: 
                                            if (state.backtracking == 0) {
                                                retval.tree = root_0;
                                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                                root_0 = (Tree) adaptor.nil();
                                                // 437:53: -> ^( METHOD_CALL $postfixedExpression)
                                                {
                                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:437:57: ^( METHOD_CALL $postfixedExpression)
                                                    {
                                                        Tree root_1 = (Tree) adaptor.nil();
                                                        root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(METHOD_CALL, "METHOD_CALL"), root_1);

                                                        adaptor.addChild(root_1, stream_retval.nextTree());

                                                        adaptor.addChild(root_0, root_1);
                                                    }

                                                }

                                                retval.tree = root_0;
                                            }
                                        }
                                        break;

                                    }

                                }
                                break;

                            }

                        }
                        break;
                        case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:440:13: LBRACK rangeList RBRACK ( DOT IDENT )?
                        {
                            LBRACK150 = (Token) match(input, LBRACK, FOLLOW_LBRACK_in_postfixedExpression7461);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_LBRACK.add(LBRACK150);

                            pushFollow(FOLLOW_rangeList_in_postfixedExpression7463);
                            rangeList151 = rangeList();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_rangeList.add(rangeList151.getTree());
                            RBRACK152 = (Token) match(input, RBRACK, FOLLOW_RBRACK_in_postfixedExpression7465);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_RBRACK.add(RBRACK152);

                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:440:37: ( DOT IDENT )?
                            int alt37 = 2;
                            int LA37_0 = input.LA(1);

                            if ((LA37_0 == DOT)) {
                                int LA37_1 = input.LA(2);

                                if ((LA37_1 == IDENT)) {
                                    alt37 = 1;
                                }
                            }
                            switch (alt37) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:440:38: DOT IDENT
                                {
                                    DOT153 = (Token) match(input, DOT, FOLLOW_DOT_in_postfixedExpression7468);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        stream_DOT.add(DOT153);

                                    IDENT154 = (Token) match(input, IDENT, FOLLOW_IDENT_in_postfixedExpression7470);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        stream_IDENT.add(IDENT154);

                                }
                                break;

                            }

                            // AST REWRITE
                            // elements: rangeList, postfixedExpression, IDENT
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            // wildcard labels: 
                            if (state.backtracking == 0) {
                                retval.tree = root_0;
                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                root_0 = (Tree) adaptor.nil();
                                // 440:66: -> ^( ARRAY_ELEMENT_ACCESS $postfixedExpression rangeList ( IDENT )? )
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:440:70: ^( ARRAY_ELEMENT_ACCESS $postfixedExpression rangeList ( IDENT )? )
                                    {
                                        Tree root_1 = (Tree) adaptor.nil();
                                        root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(ARRAY_ELEMENT_ACCESS, "ARRAY_ELEMENT_ACCESS"), root_1);

                                        adaptor.addChild(root_1, stream_retval.nextTree());
                                        adaptor.addChild(root_1, stream_rangeList.nextTree());
                                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:440:124: ( IDENT )?
                                        if (stream_IDENT.hasNext()) {
                                            adaptor.addChild(root_1, stream_IDENT.nextNode());

                                        }
                                        stream_IDENT.reset();

                                        adaptor.addChild(root_0, root_1);
                                    }

                                }

                                retval.tree = root_0;
                            }
                        }
                        break;

                        default:
                            break loop38;
                    }
                } while (true);

                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:443:9: ( INC -> ^( POST_INC[$INC, \"POST_INC\"] $postfixedExpression) | DEC -> ^( POST_DEC[$DEC, \"POST_DEC\"] $postfixedExpression) )?
                int alt39 = 3;
                int LA39_0 = input.LA(1);

                if ((LA39_0 == INC)) {
                    alt39 = 1;
                } else if ((LA39_0 == DEC)) {
                    alt39 = 2;
                }
                switch (alt39) {
                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:443:13: INC
                    {
                        INC155 = (Token) match(input, INC, FOLLOW_INC_in_postfixedExpression7537);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_INC.add(INC155);

                        // AST REWRITE
                        // elements: postfixedExpression
                        // token labels: 
                        // rule labels: retval
                        // token list labels: 
                        // rule list labels: 
                        // wildcard labels: 
                        if (state.backtracking == 0) {
                            retval.tree = root_0;
                            RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                            root_0 = (Tree) adaptor.nil();
                            // 443:17: -> ^( POST_INC[$INC, \"POST_INC\"] $postfixedExpression)
                            {
                                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:443:20: ^( POST_INC[$INC, \"POST_INC\"] $postfixedExpression)
                                {
                                    Tree root_1 = (Tree) adaptor.nil();
                                    root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(POST_INC, INC155, "POST_INC"), root_1);

                                    adaptor.addChild(root_1, stream_retval.nextTree());

                                    adaptor.addChild(root_0, root_1);
                                }

                            }

                            retval.tree = root_0;
                        }
                    }
                    break;
                    case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:444:13: DEC
                    {
                        DEC156 = (Token) match(input, DEC, FOLLOW_DEC_in_postfixedExpression7561);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_DEC.add(DEC156);

                        // AST REWRITE
                        // elements: postfixedExpression
                        // token labels: 
                        // rule labels: retval
                        // token list labels: 
                        // rule list labels: 
                        // wildcard labels: 
                        if (state.backtracking == 0) {
                            retval.tree = root_0;
                            RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                            root_0 = (Tree) adaptor.nil();
                            // 444:17: -> ^( POST_DEC[$DEC, \"POST_DEC\"] $postfixedExpression)
                            {
                                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:444:20: ^( POST_DEC[$DEC, \"POST_DEC\"] $postfixedExpression)
                                {
                                    Tree root_1 = (Tree) adaptor.nil();
                                    root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(POST_DEC, DEC156, "POST_DEC"), root_1);

                                    adaptor.addChild(root_1, stream_retval.nextTree());

                                    adaptor.addChild(root_0, root_1);
                                }

                            }

                            retval.tree = root_0;
                        }
                    }
                    break;

                }

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 34, postfixedExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "postfixedExpression"
    public static class arrayDeclarator_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "arrayDeclarator"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:448:1: arrayDeclarator : LBRACK RBRACK -> ^( ARRAY_DECLARATOR ) ;
    public final VNScriptParser.arrayDeclarator_return arrayDeclarator() throws RecognitionException
    {
        VNScriptParser.arrayDeclarator_return retval = new VNScriptParser.arrayDeclarator_return();
        retval.start = input.LT(1);
        int arrayDeclarator_StartIndex = input.index();
        Tree root_0 = null;

        Token LBRACK157 = null;
        Token RBRACK158 = null;

        Tree LBRACK157_tree = null;
        Tree RBRACK158_tree = null;
        RewriteRuleTokenStream stream_RBRACK = new RewriteRuleTokenStream(adaptor, "token RBRACK");
        RewriteRuleTokenStream stream_LBRACK = new RewriteRuleTokenStream(adaptor, "token LBRACK");

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 35)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:449:5: ( LBRACK RBRACK -> ^( ARRAY_DECLARATOR ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:449:9: LBRACK RBRACK
            {
                LBRACK157 = (Token) match(input, LBRACK, FOLLOW_LBRACK_in_arrayDeclarator7605);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_LBRACK.add(LBRACK157);

                RBRACK158 = (Token) match(input, RBRACK, FOLLOW_RBRACK_in_arrayDeclarator7607);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_RBRACK.add(RBRACK158);

                // AST REWRITE
                // elements: 
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (Tree) adaptor.nil();
                    // 450:9: -> ^( ARRAY_DECLARATOR )
                    {
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:450:13: ^( ARRAY_DECLARATOR )
                        {
                            Tree root_1 = (Tree) adaptor.nil();
                            root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(ARRAY_DECLARATOR, "ARRAY_DECLARATOR"), root_1);

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 35, arrayDeclarator_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "arrayDeclarator"
    public static class primitiveType_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "primitiveType"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:453:1: primitiveType : ( BOOLEAN | CHAR | BYTE | SHORT | INT | LONG | FLOAT | DOUBLE );
    public final VNScriptParser.primitiveType_return primitiveType() throws RecognitionException
    {
        VNScriptParser.primitiveType_return retval = new VNScriptParser.primitiveType_return();
        retval.start = input.LT(1);
        int primitiveType_StartIndex = input.index();
        Tree root_0 = null;

        Token set159 = null;

        Tree set159_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 36)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:454:5: ( BOOLEAN | CHAR | BYTE | SHORT | INT | LONG | FLOAT | DOUBLE )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:
            {
                root_0 = (Tree) adaptor.nil();

                set159 = (Token) input.LT(1);
                if (input.LA(1) == BOOLEAN || input.LA(1) == BYTE || input.LA(1) == CHAR || input.LA(1) == DOUBLE || input.LA(1) == FLOAT || (input.LA(1) >= INT && input.LA(1) <= LONG) || input.LA(1) == SHORT) {
                    input.consume();
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, (Tree) adaptor.create(set159));
                    state.errorRecovery = false;
                    state.failed = false;
                } else {
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    throw mse;
                }

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 36, primitiveType_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "primitiveType"
    public static class primaryExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "primaryExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:464:1: primaryExpression : ( parenthesizedExpression | literal | qualifiedIdentExpression | genericTypeArgumentListSimplified ( SUPER ( arguments -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) | DOT IDENT arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) genericTypeArgumentListSimplified arguments ) ) | IDENT arguments -> ^( METHOD_CALL IDENT genericTypeArgumentListSimplified arguments ) | THIS arguments -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) ) | ( THIS -> THIS ) ( arguments -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] arguments ) )? | SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] arguments ) | ( SUPER DOT IDENT ) ( arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) arguments ) | -> ^( DOT SUPER IDENT ) ) | ( primitiveType -> primitiveType ) ( arrayDeclarator -> ^( arrayDeclarator $primaryExpression) )* DOT CLASS -> ^( DOT $primaryExpression CLASS ) | VOID DOT CLASS -> ^( DOT VOID CLASS ) );
    public final VNScriptParser.primaryExpression_return primaryExpression() throws RecognitionException
    {
        VNScriptParser.primaryExpression_return retval = new VNScriptParser.primaryExpression_return();
        retval.start = input.LT(1);
        int primaryExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token SUPER164 = null;
        Token DOT166 = null;
        Token IDENT167 = null;
        Token IDENT169 = null;
        Token THIS171 = null;
        Token THIS173 = null;
        Token SUPER175 = null;
        Token SUPER177 = null;
        Token DOT178 = null;
        Token IDENT179 = null;
        Token DOT183 = null;
        Token CLASS184 = null;
        Token VOID185 = null;
        Token DOT186 = null;
        Token CLASS187 = null;
        VNScriptParser.parenthesizedExpression_return parenthesizedExpression160 = null;

        VNScriptParser.literal_return literal161 = null;

        VNScriptParser.qualifiedIdentExpression_return qualifiedIdentExpression162 = null;

        VNScriptParser.genericTypeArgumentListSimplified_return genericTypeArgumentListSimplified163 = null;

        VNScriptParser.arguments_return arguments165 = null;

        VNScriptParser.arguments_return arguments168 = null;

        VNScriptParser.arguments_return arguments170 = null;

        VNScriptParser.arguments_return arguments172 = null;

        VNScriptParser.arguments_return arguments174 = null;

        VNScriptParser.arguments_return arguments176 = null;

        VNScriptParser.arguments_return arguments180 = null;

        VNScriptParser.primitiveType_return primitiveType181 = null;

        VNScriptParser.arrayDeclarator_return arrayDeclarator182 = null;

        Tree SUPER164_tree = null;
        Tree DOT166_tree = null;
        Tree IDENT167_tree = null;
        Tree IDENT169_tree = null;
        Tree THIS171_tree = null;
        Tree THIS173_tree = null;
        Tree SUPER175_tree = null;
        Tree SUPER177_tree = null;
        Tree DOT178_tree = null;
        Tree IDENT179_tree = null;
        Tree DOT183_tree = null;
        Tree CLASS184_tree = null;
        Tree VOID185_tree = null;
        Tree DOT186_tree = null;
        Tree CLASS187_tree = null;
        RewriteRuleTokenStream stream_IDENT = new RewriteRuleTokenStream(adaptor, "token IDENT");
        RewriteRuleTokenStream stream_CLASS = new RewriteRuleTokenStream(adaptor, "token CLASS");
        RewriteRuleTokenStream stream_VOID = new RewriteRuleTokenStream(adaptor, "token VOID");
        RewriteRuleTokenStream stream_SUPER = new RewriteRuleTokenStream(adaptor, "token SUPER");
        RewriteRuleTokenStream stream_DOT = new RewriteRuleTokenStream(adaptor, "token DOT");
        RewriteRuleTokenStream stream_THIS = new RewriteRuleTokenStream(adaptor, "token THIS");
        RewriteRuleSubtreeStream stream_arrayDeclarator = new RewriteRuleSubtreeStream(adaptor, "rule arrayDeclarator");
        RewriteRuleSubtreeStream stream_arguments = new RewriteRuleSubtreeStream(adaptor, "rule arguments");
        RewriteRuleSubtreeStream stream_primitiveType = new RewriteRuleSubtreeStream(adaptor, "rule primitiveType");
        RewriteRuleSubtreeStream stream_genericTypeArgumentListSimplified = new RewriteRuleSubtreeStream(adaptor, "rule genericTypeArgumentListSimplified");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 37)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:465:5: ( parenthesizedExpression | literal | qualifiedIdentExpression | genericTypeArgumentListSimplified ( SUPER ( arguments -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) | DOT IDENT arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) genericTypeArgumentListSimplified arguments ) ) | IDENT arguments -> ^( METHOD_CALL IDENT genericTypeArgumentListSimplified arguments ) | THIS arguments -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) ) | ( THIS -> THIS ) ( arguments -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] arguments ) )? | SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] arguments ) | ( SUPER DOT IDENT ) ( arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) arguments ) | -> ^( DOT SUPER IDENT ) ) | ( primitiveType -> primitiveType ) ( arrayDeclarator -> ^( arrayDeclarator $primaryExpression) )* DOT CLASS -> ^( DOT $primaryExpression CLASS ) | VOID DOT CLASS -> ^( DOT VOID CLASS ) )
            int alt45 = 9;
            alt45 = dfa45.predict(input);
            switch (alt45) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:465:9: parenthesizedExpression
                {
                    root_0 = (Tree) adaptor.nil();

                    pushFollow(FOLLOW_parenthesizedExpression_in_primaryExpression7734);
                    parenthesizedExpression160 = parenthesizedExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, parenthesizedExpression160.getTree());

                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:466:9: literal
                {
                    root_0 = (Tree) adaptor.nil();

                    pushFollow(FOLLOW_literal_in_primaryExpression7744);
                    literal161 = literal();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, literal161.getTree());

                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:467:9: qualifiedIdentExpression
                {
                    root_0 = (Tree) adaptor.nil();

                    pushFollow(FOLLOW_qualifiedIdentExpression_in_primaryExpression7754);
                    qualifiedIdentExpression162 = qualifiedIdentExpression();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, qualifiedIdentExpression162.getTree());

                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:468:9: genericTypeArgumentListSimplified ( SUPER ( arguments -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) | DOT IDENT arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) genericTypeArgumentListSimplified arguments ) ) | IDENT arguments -> ^( METHOD_CALL IDENT genericTypeArgumentListSimplified arguments ) | THIS arguments -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) )
                {
                    pushFollow(FOLLOW_genericTypeArgumentListSimplified_in_primaryExpression7764);
                    genericTypeArgumentListSimplified163 = genericTypeArgumentListSimplified();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_genericTypeArgumentListSimplified.add(genericTypeArgumentListSimplified163.getTree());
                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:469:9: ( SUPER ( arguments -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) | DOT IDENT arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) genericTypeArgumentListSimplified arguments ) ) | IDENT arguments -> ^( METHOD_CALL IDENT genericTypeArgumentListSimplified arguments ) | THIS arguments -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) )
                    int alt41 = 3;
                    switch (input.LA(1)) {
                        case SUPER: {
                            alt41 = 1;
                        }
                        break;
                        case IDENT: {
                            alt41 = 2;
                        }
                        break;
                        case THIS: {
                            alt41 = 3;
                        }
                        break;
                        default:
                            if (state.backtracking > 0) {
                                state.failed = true;
                                return retval;
                            }
                            NoViableAltException nvae
                                = new NoViableAltException("", 41, 0, input);

                            throw nvae;
                    }

                    switch (alt41) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:469:13: SUPER ( arguments -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) | DOT IDENT arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) genericTypeArgumentListSimplified arguments ) )
                        {
                            SUPER164 = (Token) match(input, SUPER, FOLLOW_SUPER_in_primaryExpression7779);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_SUPER.add(SUPER164);

                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:470:13: ( arguments -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) | DOT IDENT arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) genericTypeArgumentListSimplified arguments ) )
                            int alt40 = 2;
                            int LA40_0 = input.LA(1);

                            if ((LA40_0 == LPAREN)) {
                                alt40 = 1;
                            } else if ((LA40_0 == DOT)) {
                                alt40 = 2;
                            } else {
                                if (state.backtracking > 0) {
                                    state.failed = true;
                                    return retval;
                                }
                                NoViableAltException nvae
                                    = new NoViableAltException("", 40, 0, input);

                                throw nvae;
                            }
                            switch (alt40) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:470:17: arguments
                                {
                                    pushFollow(FOLLOW_arguments_in_primaryExpression7797);
                                    arguments165 = arguments();

                                    state._fsp--;
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        stream_arguments.add(arguments165.getTree());

                                    // AST REWRITE
                                    // elements: arguments, genericTypeArgumentListSimplified
                                    // token labels: 
                                    // rule labels: retval
                                    // token list labels: 
                                    // rule list labels: 
                                    // wildcard labels: 
                                    if (state.backtracking == 0) {
                                        retval.tree = root_0;
                                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                        root_0 = (Tree) adaptor.nil();
                                        // 470:57: -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments )
                                        {
                                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:470:61: ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments )
                                            {
                                                Tree root_1 = (Tree) adaptor.nil();
                                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(SUPER_CONSTRUCTOR_CALL, SUPER164, "SUPER_CONSTRUCTOR_CALL"), root_1);

                                                adaptor.addChild(root_1, stream_genericTypeArgumentListSimplified.nextTree());
                                                adaptor.addChild(root_1, stream_arguments.nextTree());

                                                adaptor.addChild(root_0, root_1);
                                            }

                                        }

                                        retval.tree = root_0;
                                    }
                                }
                                break;
                                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:471:17: DOT IDENT arguments
                                {
                                    DOT166 = (Token) match(input, DOT, FOLLOW_DOT_in_primaryExpression7857);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        stream_DOT.add(DOT166);

                                    IDENT167 = (Token) match(input, IDENT, FOLLOW_IDENT_in_primaryExpression7859);
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        stream_IDENT.add(IDENT167);

                                    pushFollow(FOLLOW_arguments_in_primaryExpression7861);
                                    arguments168 = arguments();

                                    state._fsp--;
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        stream_arguments.add(arguments168.getTree());

                                    // AST REWRITE
                                    // elements: arguments, genericTypeArgumentListSimplified, SUPER, DOT, IDENT
                                    // token labels: 
                                    // rule labels: retval
                                    // token list labels: 
                                    // rule list labels: 
                                    // wildcard labels: 
                                    if (state.backtracking == 0) {
                                        retval.tree = root_0;
                                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                        root_0 = (Tree) adaptor.nil();
                                        // 471:57: -> ^( METHOD_CALL ^( DOT SUPER IDENT ) genericTypeArgumentListSimplified arguments )
                                        {
                                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:471:61: ^( METHOD_CALL ^( DOT SUPER IDENT ) genericTypeArgumentListSimplified arguments )
                                            {
                                                Tree root_1 = (Tree) adaptor.nil();
                                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(METHOD_CALL, "METHOD_CALL"), root_1);

                                                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:471:75: ^( DOT SUPER IDENT )
                                                {
                                                    Tree root_2 = (Tree) adaptor.nil();
                                                    root_2 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_2);

                                                    adaptor.addChild(root_2, stream_SUPER.nextNode());
                                                    adaptor.addChild(root_2, stream_IDENT.nextNode());

                                                    adaptor.addChild(root_1, root_2);
                                                }
                                                adaptor.addChild(root_1, stream_genericTypeArgumentListSimplified.nextTree());
                                                adaptor.addChild(root_1, stream_arguments.nextTree());

                                                adaptor.addChild(root_0, root_1);
                                            }

                                        }

                                        retval.tree = root_0;
                                    }
                                }
                                break;

                            }

                        }
                        break;
                        case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:473:13: IDENT arguments
                        {
                            IDENT169 = (Token) match(input, IDENT, FOLLOW_IDENT_in_primaryExpression7928);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_IDENT.add(IDENT169);

                            pushFollow(FOLLOW_arguments_in_primaryExpression7930);
                            arguments170 = arguments();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_arguments.add(arguments170.getTree());

                            // AST REWRITE
                            // elements: IDENT, arguments, genericTypeArgumentListSimplified
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            // wildcard labels: 
                            if (state.backtracking == 0) {
                                retval.tree = root_0;
                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                root_0 = (Tree) adaptor.nil();
                                // 473:57: -> ^( METHOD_CALL IDENT genericTypeArgumentListSimplified arguments )
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:473:61: ^( METHOD_CALL IDENT genericTypeArgumentListSimplified arguments )
                                    {
                                        Tree root_1 = (Tree) adaptor.nil();
                                        root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(METHOD_CALL, "METHOD_CALL"), root_1);

                                        adaptor.addChild(root_1, stream_IDENT.nextNode());
                                        adaptor.addChild(root_1, stream_genericTypeArgumentListSimplified.nextTree());
                                        adaptor.addChild(root_1, stream_arguments.nextTree());

                                        adaptor.addChild(root_0, root_1);
                                    }

                                }

                                retval.tree = root_0;
                            }
                        }
                        break;
                        case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:474:13: THIS arguments
                        {
                            THIS171 = (Token) match(input, THIS, FOLLOW_THIS_in_primaryExpression7985);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_THIS.add(THIS171);

                            pushFollow(FOLLOW_arguments_in_primaryExpression7987);
                            arguments172 = arguments();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_arguments.add(arguments172.getTree());

                            // AST REWRITE
                            // elements: genericTypeArgumentListSimplified, arguments
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            // wildcard labels: 
                            if (state.backtracking == 0) {
                                retval.tree = root_0;
                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                root_0 = (Tree) adaptor.nil();
                                // 474:57: -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments )
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:474:61: ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments )
                                    {
                                        Tree root_1 = (Tree) adaptor.nil();
                                        root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(THIS_CONSTRUCTOR_CALL, THIS171, "THIS_CONSTRUCTOR_CALL"), root_1);

                                        adaptor.addChild(root_1, stream_genericTypeArgumentListSimplified.nextTree());
                                        adaptor.addChild(root_1, stream_arguments.nextTree());

                                        adaptor.addChild(root_0, root_1);
                                    }

                                }

                                retval.tree = root_0;
                            }
                        }
                        break;

                    }

                }
                break;
                case 5: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:476:9: ( THIS -> THIS ) ( arguments -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] arguments ) )?
                {
                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:476:9: ( THIS -> THIS )
                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:476:13: THIS
                    {
                        THIS173 = (Token) match(input, THIS, FOLLOW_THIS_in_primaryExpression8052);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_THIS.add(THIS173);

                        // AST REWRITE
                        // elements: THIS
                        // token labels: 
                        // rule labels: retval
                        // token list labels: 
                        // rule list labels: 
                        // wildcard labels: 
                        if (state.backtracking == 0) {
                            retval.tree = root_0;
                            RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                            root_0 = (Tree) adaptor.nil();
                            // 476:57: -> THIS
                            {
                                adaptor.addChild(root_0, stream_THIS.nextNode());

                            }

                            retval.tree = root_0;
                        }
                    }

                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:478:9: ( arguments -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] arguments ) )?
                    int alt42 = 2;
                    int LA42_0 = input.LA(1);

                    if ((LA42_0 == LPAREN)) {
                        alt42 = 1;
                    }
                    switch (alt42) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:478:13: arguments
                        {
                            pushFollow(FOLLOW_arguments_in_primaryExpression8120);
                            arguments174 = arguments();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_arguments.add(arguments174.getTree());

                            // AST REWRITE
                            // elements: arguments
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            // wildcard labels: 
                            if (state.backtracking == 0) {
                                retval.tree = root_0;
                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                root_0 = (Tree) adaptor.nil();
                                // 478:57: -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] arguments )
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:478:61: ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] arguments )
                                    {
                                        Tree root_1 = (Tree) adaptor.nil();
                                        root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(THIS_CONSTRUCTOR_CALL, THIS173, "THIS_CONSTRUCTOR_CALL"), root_1);

                                        adaptor.addChild(root_1, stream_arguments.nextTree());

                                        adaptor.addChild(root_0, root_1);
                                    }

                                }

                                retval.tree = root_0;
                            }
                        }
                        break;

                    }

                }
                break;
                case 6: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:480:9: SUPER arguments
                {
                    SUPER175 = (Token) match(input, SUPER, FOLLOW_SUPER_in_primaryExpression8185);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_SUPER.add(SUPER175);

                    pushFollow(FOLLOW_arguments_in_primaryExpression8187);
                    arguments176 = arguments();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_arguments.add(arguments176.getTree());

                    // AST REWRITE
                    // elements: arguments
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 480:57: -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] arguments )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:480:61: ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] arguments )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(SUPER_CONSTRUCTOR_CALL, SUPER175, "SUPER_CONSTRUCTOR_CALL"), root_1);

                                adaptor.addChild(root_1, stream_arguments.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 7: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:481:9: ( SUPER DOT IDENT ) ( arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) arguments ) | -> ^( DOT SUPER IDENT ) )
                {
                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:481:9: ( SUPER DOT IDENT )
                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:481:13: SUPER DOT IDENT
                    {
                        SUPER177 = (Token) match(input, SUPER, FOLLOW_SUPER_in_primaryExpression8243);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_SUPER.add(SUPER177);

                        DOT178 = (Token) match(input, DOT, FOLLOW_DOT_in_primaryExpression8245);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_DOT.add(DOT178);

                        IDENT179 = (Token) match(input, IDENT, FOLLOW_IDENT_in_primaryExpression8247);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_IDENT.add(IDENT179);

                    }

                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:483:9: ( arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) arguments ) | -> ^( DOT SUPER IDENT ) )
                    int alt43 = 2;
                    int LA43_0 = input.LA(1);

                    if ((LA43_0 == LPAREN)) {
                        alt43 = 1;
                    } else if ((LA43_0 == EOF || (LA43_0 >= AND && LA43_0 <= ASSIGN) || (LA43_0 >= BIT_SHIFT_RIGHT && LA43_0 <= DOT) || (LA43_0 >= EQUAL && LA43_0 <= LBRACK) || (LA43_0 >= LESS_OR_EQUAL && LA43_0 <= LOGICAL_AND) || LA43_0 == LOGICAL_OR || (LA43_0 >= MINUS && LA43_0 <= MOD_ASSIGN) || (LA43_0 >= NOT_EQUAL && LA43_0 <= RBRACK) || (LA43_0 >= RPAREN && LA43_0 <= XOR_ASSIGN) || LA43_0 == INSTANCEOF)) {
                        alt43 = 2;
                    } else {
                        if (state.backtracking > 0) {
                            state.failed = true;
                            return retval;
                        }
                        NoViableAltException nvae
                            = new NoViableAltException("", 43, 0, input);

                        throw nvae;
                    }
                    switch (alt43) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:483:13: arguments
                        {
                            pushFollow(FOLLOW_arguments_in_primaryExpression8271);
                            arguments180 = arguments();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_arguments.add(arguments180.getTree());

                            // AST REWRITE
                            // elements: SUPER, IDENT, DOT, arguments
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            // wildcard labels: 
                            if (state.backtracking == 0) {
                                retval.tree = root_0;
                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                root_0 = (Tree) adaptor.nil();
                                // 483:57: -> ^( METHOD_CALL ^( DOT SUPER IDENT ) arguments )
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:483:61: ^( METHOD_CALL ^( DOT SUPER IDENT ) arguments )
                                    {
                                        Tree root_1 = (Tree) adaptor.nil();
                                        root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(METHOD_CALL, "METHOD_CALL"), root_1);

                                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:483:75: ^( DOT SUPER IDENT )
                                        {
                                            Tree root_2 = (Tree) adaptor.nil();
                                            root_2 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_2);

                                            adaptor.addChild(root_2, stream_SUPER.nextNode());
                                            adaptor.addChild(root_2, stream_IDENT.nextNode());

                                            adaptor.addChild(root_1, root_2);
                                        }
                                        adaptor.addChild(root_1, stream_arguments.nextTree());

                                        adaptor.addChild(root_0, root_1);
                                    }

                                }

                                retval.tree = root_0;
                            }
                        }
                        break;
                        case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:484:57: 
                        {

                            // AST REWRITE
                            // elements: DOT, SUPER, IDENT
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            // wildcard labels: 
                            if (state.backtracking == 0) {
                                retval.tree = root_0;
                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                root_0 = (Tree) adaptor.nil();
                                // 484:57: -> ^( DOT SUPER IDENT )
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:484:61: ^( DOT SUPER IDENT )
                                    {
                                        Tree root_1 = (Tree) adaptor.nil();
                                        root_1 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_1);

                                        adaptor.addChild(root_1, stream_SUPER.nextNode());
                                        adaptor.addChild(root_1, stream_IDENT.nextNode());

                                        adaptor.addChild(root_0, root_1);
                                    }

                                }

                                retval.tree = root_0;
                            }
                        }
                        break;

                    }

                }
                break;
                case 8: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:486:9: ( primitiveType -> primitiveType ) ( arrayDeclarator -> ^( arrayDeclarator $primaryExpression) )* DOT CLASS
                {
                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:486:9: ( primitiveType -> primitiveType )
                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:486:13: primitiveType
                    {
                        pushFollow(FOLLOW_primitiveType_in_primaryExpression8413);
                        primitiveType181 = primitiveType();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_primitiveType.add(primitiveType181.getTree());

                        // AST REWRITE
                        // elements: primitiveType
                        // token labels: 
                        // rule labels: retval
                        // token list labels: 
                        // rule list labels: 
                        // wildcard labels: 
                        if (state.backtracking == 0) {
                            retval.tree = root_0;
                            RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                            root_0 = (Tree) adaptor.nil();
                            // 486:57: -> primitiveType
                            {
                                adaptor.addChild(root_0, stream_primitiveType.nextTree());

                            }

                            retval.tree = root_0;
                        }
                    }

                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:488:9: ( arrayDeclarator -> ^( arrayDeclarator $primaryExpression) )*
                    loop44:
                    do {
                        int alt44 = 2;
                        int LA44_0 = input.LA(1);

                        if ((LA44_0 == LBRACK)) {
                            alt44 = 1;
                        }

                        switch (alt44) {
                            case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:488:13: arrayDeclarator
                            {
                                pushFollow(FOLLOW_arrayDeclarator_in_primaryExpression8472);
                                arrayDeclarator182 = arrayDeclarator();

                                state._fsp--;
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    stream_arrayDeclarator.add(arrayDeclarator182.getTree());

                                // AST REWRITE
                                // elements: arrayDeclarator, primaryExpression
                                // token labels: 
                                // rule labels: retval
                                // token list labels: 
                                // rule list labels: 
                                // wildcard labels: 
                                if (state.backtracking == 0) {
                                    retval.tree = root_0;
                                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                    root_0 = (Tree) adaptor.nil();
                                    // 488:57: -> ^( arrayDeclarator $primaryExpression)
                                    {
                                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:488:61: ^( arrayDeclarator $primaryExpression)
                                        {
                                            Tree root_1 = (Tree) adaptor.nil();
                                            root_1 = (Tree) adaptor.becomeRoot(stream_arrayDeclarator.nextNode(), root_1);

                                            adaptor.addChild(root_1, stream_retval.nextTree());

                                            adaptor.addChild(root_0, root_1);
                                        }

                                    }

                                    retval.tree = root_0;
                                }
                            }
                            break;

                            default:
                                break loop44;
                        }
                    } while (true);

                    DOT183 = (Token) match(input, DOT, FOLLOW_DOT_in_primaryExpression8535);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_DOT.add(DOT183);

                    CLASS184 = (Token) match(input, CLASS, FOLLOW_CLASS_in_primaryExpression8537);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_CLASS.add(CLASS184);

                    // AST REWRITE
                    // elements: DOT, CLASS, primaryExpression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 490:57: -> ^( DOT $primaryExpression CLASS )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:490:61: ^( DOT $primaryExpression CLASS )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_1);

                                adaptor.addChild(root_1, stream_retval.nextTree());
                                adaptor.addChild(root_1, stream_CLASS.nextNode());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 9: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:491:9: VOID DOT CLASS
                {
                    VOID185 = (Token) match(input, VOID, FOLLOW_VOID_in_primaryExpression8597);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_VOID.add(VOID185);

                    DOT186 = (Token) match(input, DOT, FOLLOW_DOT_in_primaryExpression8599);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_DOT.add(DOT186);

                    CLASS187 = (Token) match(input, CLASS, FOLLOW_CLASS_in_primaryExpression8601);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_CLASS.add(CLASS187);

                    // AST REWRITE
                    // elements: VOID, DOT, CLASS
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 491:57: -> ^( DOT VOID CLASS )
                        {
                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:491:61: ^( DOT VOID CLASS )
                            {
                                Tree root_1 = (Tree) adaptor.nil();
                                root_1 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_1);

                                adaptor.addChild(root_1, stream_VOID.nextNode());
                                adaptor.addChild(root_1, stream_CLASS.nextNode());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 37, primaryExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "primaryExpression"
    public static class qualifiedIdentifier_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "qualifiedIdentifier"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:494:5: qualifiedIdentifier : ( IDENT -> IDENT ) ( DOT ident= IDENT -> ^( DOT $qualifiedIdentifier $ident) )* ;
    public final VNScriptParser.qualifiedIdentifier_return qualifiedIdentifier() throws RecognitionException
    {
        VNScriptParser.qualifiedIdentifier_return retval = new VNScriptParser.qualifiedIdentifier_return();
        retval.start = input.LT(1);
        int qualifiedIdentifier_StartIndex = input.index();
        Tree root_0 = null;

        Token ident = null;
        Token IDENT188 = null;
        Token DOT189 = null;

        Tree ident_tree = null;
        Tree IDENT188_tree = null;
        Tree DOT189_tree = null;
        RewriteRuleTokenStream stream_IDENT = new RewriteRuleTokenStream(adaptor, "token IDENT");
        RewriteRuleTokenStream stream_DOT = new RewriteRuleTokenStream(adaptor, "token DOT");

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 38)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:495:5: ( ( IDENT -> IDENT ) ( DOT ident= IDENT -> ^( DOT $qualifiedIdentifier $ident) )* )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:495:9: ( IDENT -> IDENT ) ( DOT ident= IDENT -> ^( DOT $qualifiedIdentifier $ident) )*
            {
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:495:9: ( IDENT -> IDENT )
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:495:13: IDENT
                {
                    IDENT188 = (Token) match(input, IDENT, FOLLOW_IDENT_in_qualifiedIdentifier8676);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_IDENT.add(IDENT188);

                    // AST REWRITE
                    // elements: IDENT
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 495:33: -> IDENT
                        {
                            adaptor.addChild(root_0, stream_IDENT.nextNode());

                        }

                        retval.tree = root_0;
                    }
                }

                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:497:9: ( DOT ident= IDENT -> ^( DOT $qualifiedIdentifier $ident) )*
                loop46:
                do {
                    int alt46 = 2;
                    int LA46_0 = input.LA(1);

                    if ((LA46_0 == DOT)) {
                        int LA46_2 = input.LA(2);

                        if ((LA46_2 == IDENT)) {
                            alt46 = 1;
                        }

                    }

                    switch (alt46) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:497:13: DOT ident= IDENT
                        {
                            DOT189 = (Token) match(input, DOT, FOLLOW_DOT_in_qualifiedIdentifier8719);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_DOT.add(DOT189);

                            ident = (Token) match(input, IDENT, FOLLOW_IDENT_in_qualifiedIdentifier8723);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_IDENT.add(ident);

                            // AST REWRITE
                            // elements: DOT, qualifiedIdentifier, ident
                            // token labels: ident
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            // wildcard labels: 
                            if (state.backtracking == 0) {
                                retval.tree = root_0;
                                RewriteRuleTokenStream stream_ident = new RewriteRuleTokenStream(adaptor, "token ident", ident);
                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                root_0 = (Tree) adaptor.nil();
                                // 497:33: -> ^( DOT $qualifiedIdentifier $ident)
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:497:37: ^( DOT $qualifiedIdentifier $ident)
                                    {
                                        Tree root_1 = (Tree) adaptor.nil();
                                        root_1 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_1);

                                        adaptor.addChild(root_1, stream_retval.nextTree());
                                        adaptor.addChild(root_1, stream_ident.nextNode());

                                        adaptor.addChild(root_0, root_1);
                                    }

                                }

                                retval.tree = root_0;
                            }
                        }
                        break;

                        default:
                            break loop46;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 38, qualifiedIdentifier_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "qualifiedIdentifier"
    public static class genericTypeListClosing_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "genericTypeListClosing"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:501:1: genericTypeListClosing : ( GREATER_THAN | SHIFT_RIGHT | BIT_SHIFT_RIGHT | );
    public final VNScriptParser.genericTypeListClosing_return genericTypeListClosing() throws RecognitionException
    {
        VNScriptParser.genericTypeListClosing_return retval = new VNScriptParser.genericTypeListClosing_return();
        retval.start = input.LT(1);
        int genericTypeListClosing_StartIndex = input.index();
        Tree root_0 = null;

        Token GREATER_THAN190 = null;
        Token SHIFT_RIGHT191 = null;
        Token BIT_SHIFT_RIGHT192 = null;

        Tree GREATER_THAN190_tree = null;
        Tree SHIFT_RIGHT191_tree = null;
        Tree BIT_SHIFT_RIGHT192_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 39)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:505:5: ( GREATER_THAN | SHIFT_RIGHT | BIT_SHIFT_RIGHT | )
            int alt47 = 4;
            alt47 = dfa47.predict(input);
            switch (alt47) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:505:9: GREATER_THAN
                {
                    root_0 = (Tree) adaptor.nil();

                    GREATER_THAN190 = (Token) match(input, GREATER_THAN, FOLLOW_GREATER_THAN_in_genericTypeListClosing8847);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        GREATER_THAN190_tree = (Tree) adaptor.create(GREATER_THAN190);
                        adaptor.addChild(root_0, GREATER_THAN190_tree);
                    }

                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:506:9: SHIFT_RIGHT
                {
                    root_0 = (Tree) adaptor.nil();

                    SHIFT_RIGHT191 = (Token) match(input, SHIFT_RIGHT, FOLLOW_SHIFT_RIGHT_in_genericTypeListClosing8857);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        SHIFT_RIGHT191_tree = (Tree) adaptor.create(SHIFT_RIGHT191);
                        adaptor.addChild(root_0, SHIFT_RIGHT191_tree);
                    }

                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:507:9: BIT_SHIFT_RIGHT
                {
                    root_0 = (Tree) adaptor.nil();

                    BIT_SHIFT_RIGHT192 = (Token) match(input, BIT_SHIFT_RIGHT, FOLLOW_BIT_SHIFT_RIGHT_in_genericTypeListClosing8867);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        BIT_SHIFT_RIGHT192_tree = (Tree) adaptor.create(BIT_SHIFT_RIGHT192);
                        adaptor.addChild(root_0, BIT_SHIFT_RIGHT192_tree);
                    }

                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:509:5: 
                {
                    root_0 = (Tree) adaptor.nil();

                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 39, genericTypeListClosing_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "genericTypeListClosing"
    public static class genericTypeArgumentListSimplified_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "genericTypeArgumentListSimplified"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:511:1: genericTypeArgumentListSimplified : LESS_THAN genericTypeArgumentSimplified ( COMMA genericTypeArgumentSimplified )* genericTypeListClosing -> ^( GENERIC_TYPE_ARG_LIST[$LESS_THAN, \"GENERIC_TYPE_ARG_LIST\"] ( genericTypeArgumentSimplified )+ ) ;
    public final VNScriptParser.genericTypeArgumentListSimplified_return genericTypeArgumentListSimplified() throws RecognitionException
    {
        VNScriptParser.genericTypeArgumentListSimplified_return retval = new VNScriptParser.genericTypeArgumentListSimplified_return();
        retval.start = input.LT(1);
        int genericTypeArgumentListSimplified_StartIndex = input.index();
        Tree root_0 = null;

        Token LESS_THAN193 = null;
        Token COMMA195 = null;
        VNScriptParser.genericTypeArgumentSimplified_return genericTypeArgumentSimplified194 = null;

        VNScriptParser.genericTypeArgumentSimplified_return genericTypeArgumentSimplified196 = null;

        VNScriptParser.genericTypeListClosing_return genericTypeListClosing197 = null;

        Tree LESS_THAN193_tree = null;
        Tree COMMA195_tree = null;
        RewriteRuleTokenStream stream_COMMA = new RewriteRuleTokenStream(adaptor, "token COMMA");
        RewriteRuleTokenStream stream_LESS_THAN = new RewriteRuleTokenStream(adaptor, "token LESS_THAN");
        RewriteRuleSubtreeStream stream_genericTypeArgumentSimplified = new RewriteRuleSubtreeStream(adaptor, "rule genericTypeArgumentSimplified");
        RewriteRuleSubtreeStream stream_genericTypeListClosing = new RewriteRuleSubtreeStream(adaptor, "rule genericTypeListClosing");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 40)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:512:5: ( LESS_THAN genericTypeArgumentSimplified ( COMMA genericTypeArgumentSimplified )* genericTypeListClosing -> ^( GENERIC_TYPE_ARG_LIST[$LESS_THAN, \"GENERIC_TYPE_ARG_LIST\"] ( genericTypeArgumentSimplified )+ ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:512:9: LESS_THAN genericTypeArgumentSimplified ( COMMA genericTypeArgumentSimplified )* genericTypeListClosing
            {
                LESS_THAN193 = (Token) match(input, LESS_THAN, FOLLOW_LESS_THAN_in_genericTypeArgumentListSimplified8895);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_LESS_THAN.add(LESS_THAN193);

                pushFollow(FOLLOW_genericTypeArgumentSimplified_in_genericTypeArgumentListSimplified8897);
                genericTypeArgumentSimplified194 = genericTypeArgumentSimplified();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_genericTypeArgumentSimplified.add(genericTypeArgumentSimplified194.getTree());
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:512:49: ( COMMA genericTypeArgumentSimplified )*
                loop48:
                do {
                    int alt48 = 2;
                    int LA48_0 = input.LA(1);

                    if ((LA48_0 == COMMA)) {
                        int LA48_2 = input.LA(2);

                        if ((LA48_2 == IDENT)) {
                            int LA48_3 = input.LA(3);

                            if ((synpred99_VNScript())) {
                                alt48 = 1;
                            }

                        } else if ((LA48_2 == QUESTION)) {
                            alt48 = 1;
                        }

                    }

                    switch (alt48) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:512:50: COMMA genericTypeArgumentSimplified
                        {
                            COMMA195 = (Token) match(input, COMMA, FOLLOW_COMMA_in_genericTypeArgumentListSimplified8900);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_COMMA.add(COMMA195);

                            pushFollow(FOLLOW_genericTypeArgumentSimplified_in_genericTypeArgumentListSimplified8902);
                            genericTypeArgumentSimplified196 = genericTypeArgumentSimplified();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_genericTypeArgumentSimplified.add(genericTypeArgumentSimplified196.getTree());

                        }
                        break;

                        default:
                            break loop48;
                    }
                } while (true);

                pushFollow(FOLLOW_genericTypeListClosing_in_genericTypeArgumentListSimplified8906);
                genericTypeListClosing197 = genericTypeListClosing();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_genericTypeListClosing.add(genericTypeListClosing197.getTree());

                // AST REWRITE
                // elements: genericTypeArgumentSimplified
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (Tree) adaptor.nil();
                    // 513:9: -> ^( GENERIC_TYPE_ARG_LIST[$LESS_THAN, \"GENERIC_TYPE_ARG_LIST\"] ( genericTypeArgumentSimplified )+ )
                    {
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:513:13: ^( GENERIC_TYPE_ARG_LIST[$LESS_THAN, \"GENERIC_TYPE_ARG_LIST\"] ( genericTypeArgumentSimplified )+ )
                        {
                            Tree root_1 = (Tree) adaptor.nil();
                            root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(GENERIC_TYPE_ARG_LIST, LESS_THAN193, "GENERIC_TYPE_ARG_LIST"), root_1);

                            if (!(stream_genericTypeArgumentSimplified.hasNext())) {
                                throw new RewriteEarlyExitException();
                            }
                            while (stream_genericTypeArgumentSimplified.hasNext()) {
                                adaptor.addChild(root_1, stream_genericTypeArgumentSimplified.nextTree());

                            }
                            stream_genericTypeArgumentSimplified.reset();

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 40, genericTypeArgumentListSimplified_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "genericTypeArgumentListSimplified"
    public static class genericTypeArgumentSimplified_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "genericTypeArgumentSimplified"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:516:1: genericTypeArgumentSimplified : ( type | QUESTION );
    public final VNScriptParser.genericTypeArgumentSimplified_return genericTypeArgumentSimplified() throws RecognitionException
    {
        VNScriptParser.genericTypeArgumentSimplified_return retval = new VNScriptParser.genericTypeArgumentSimplified_return();
        retval.start = input.LT(1);
        int genericTypeArgumentSimplified_StartIndex = input.index();
        Tree root_0 = null;

        Token QUESTION199 = null;
        VNScriptParser.type_return type198 = null;

        Tree QUESTION199_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 41)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:517:5: ( type | QUESTION )
            int alt49 = 2;
            int LA49_0 = input.LA(1);

            if ((LA49_0 == IDENT)) {
                alt49 = 1;
            } else if ((LA49_0 == QUESTION)) {
                alt49 = 2;
            } else {
                if (state.backtracking > 0) {
                    state.failed = true;
                    return retval;
                }
                NoViableAltException nvae
                    = new NoViableAltException("", 49, 0, input);

                throw nvae;
            }
            switch (alt49) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:517:9: type
                {
                    root_0 = (Tree) adaptor.nil();

                    pushFollow(FOLLOW_type_in_genericTypeArgumentSimplified8948);
                    type198 = type();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, type198.getTree());

                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:518:9: QUESTION
                {
                    root_0 = (Tree) adaptor.nil();

                    QUESTION199 = (Token) match(input, QUESTION, FOLLOW_QUESTION_in_genericTypeArgumentSimplified8958);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        QUESTION199_tree = (Tree) adaptor.create(QUESTION199);
                        adaptor.addChild(root_0, QUESTION199_tree);
                    }

                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 41, genericTypeArgumentSimplified_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "genericTypeArgumentSimplified"
    public static class qualifiedIdentExpression_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "qualifiedIdentExpression"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:522:1: qualifiedIdentExpression : ( qualifiedIdentifier -> qualifiedIdentifier ) ( ( arrayDeclarator -> ^( arrayDeclarator $qualifiedIdentExpression) )+ ( DOT CLASS -> ^( DOT $qualifiedIdentExpression CLASS ) ) | arguments -> ^( METHOD_CALL qualifiedIdentifier arguments ) | outerDot= DOT ( CLASS -> ^( DOT qualifiedIdentifier CLASS ) | genericTypeArgumentListSimplified (Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier genericTypeArgumentListSimplified arguments ) | SUPER innerDot= DOT IDENT arguments -> ^( METHOD_CALL ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT ) genericTypeArgumentListSimplified arguments ) | IDENT arguments -> ^( METHOD_CALL ^( DOT qualifiedIdentifier IDENT ) genericTypeArgumentListSimplified arguments ) ) | THIS -> ^( DOT qualifiedIdentifier THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier arguments ) ) )? ;
    public final VNScriptParser.qualifiedIdentExpression_return qualifiedIdentExpression() throws RecognitionException
    {
        VNScriptParser.qualifiedIdentExpression_return retval = new VNScriptParser.qualifiedIdentExpression_return();
        retval.start = input.LT(1);
        int qualifiedIdentExpression_StartIndex = input.index();
        Tree root_0 = null;

        Token outerDot = null;
        Token Super = null;
        Token innerDot = null;
        Token DOT202 = null;
        Token CLASS203 = null;
        Token CLASS205 = null;
        Token SUPER208 = null;
        Token IDENT209 = null;
        Token IDENT211 = null;
        Token THIS213 = null;
        VNScriptParser.qualifiedIdentifier_return qualifiedIdentifier200 = null;

        VNScriptParser.arrayDeclarator_return arrayDeclarator201 = null;

        VNScriptParser.arguments_return arguments204 = null;

        VNScriptParser.genericTypeArgumentListSimplified_return genericTypeArgumentListSimplified206 = null;

        VNScriptParser.arguments_return arguments207 = null;

        VNScriptParser.arguments_return arguments210 = null;

        VNScriptParser.arguments_return arguments212 = null;

        VNScriptParser.arguments_return arguments214 = null;

        Tree outerDot_tree = null;
        Tree Super_tree = null;
        Tree innerDot_tree = null;
        Tree DOT202_tree = null;
        Tree CLASS203_tree = null;
        Tree CLASS205_tree = null;
        Tree SUPER208_tree = null;
        Tree IDENT209_tree = null;
        Tree IDENT211_tree = null;
        Tree THIS213_tree = null;
        RewriteRuleTokenStream stream_IDENT = new RewriteRuleTokenStream(adaptor, "token IDENT");
        RewriteRuleTokenStream stream_CLASS = new RewriteRuleTokenStream(adaptor, "token CLASS");
        RewriteRuleTokenStream stream_SUPER = new RewriteRuleTokenStream(adaptor, "token SUPER");
        RewriteRuleTokenStream stream_DOT = new RewriteRuleTokenStream(adaptor, "token DOT");
        RewriteRuleTokenStream stream_THIS = new RewriteRuleTokenStream(adaptor, "token THIS");
        RewriteRuleSubtreeStream stream_arrayDeclarator = new RewriteRuleSubtreeStream(adaptor, "rule arrayDeclarator");
        RewriteRuleSubtreeStream stream_arguments = new RewriteRuleSubtreeStream(adaptor, "rule arguments");
        RewriteRuleSubtreeStream stream_qualifiedIdentifier = new RewriteRuleSubtreeStream(adaptor, "rule qualifiedIdentifier");
        RewriteRuleSubtreeStream stream_genericTypeArgumentListSimplified = new RewriteRuleSubtreeStream(adaptor, "rule genericTypeArgumentListSimplified");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 42)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:524:5: ( ( qualifiedIdentifier -> qualifiedIdentifier ) ( ( arrayDeclarator -> ^( arrayDeclarator $qualifiedIdentExpression) )+ ( DOT CLASS -> ^( DOT $qualifiedIdentExpression CLASS ) ) | arguments -> ^( METHOD_CALL qualifiedIdentifier arguments ) | outerDot= DOT ( CLASS -> ^( DOT qualifiedIdentifier CLASS ) | genericTypeArgumentListSimplified (Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier genericTypeArgumentListSimplified arguments ) | SUPER innerDot= DOT IDENT arguments -> ^( METHOD_CALL ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT ) genericTypeArgumentListSimplified arguments ) | IDENT arguments -> ^( METHOD_CALL ^( DOT qualifiedIdentifier IDENT ) genericTypeArgumentListSimplified arguments ) ) | THIS -> ^( DOT qualifiedIdentifier THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier arguments ) ) )? )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:524:9: ( qualifiedIdentifier -> qualifiedIdentifier ) ( ( arrayDeclarator -> ^( arrayDeclarator $qualifiedIdentExpression) )+ ( DOT CLASS -> ^( DOT $qualifiedIdentExpression CLASS ) ) | arguments -> ^( METHOD_CALL qualifiedIdentifier arguments ) | outerDot= DOT ( CLASS -> ^( DOT qualifiedIdentifier CLASS ) | genericTypeArgumentListSimplified (Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier genericTypeArgumentListSimplified arguments ) | SUPER innerDot= DOT IDENT arguments -> ^( METHOD_CALL ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT ) genericTypeArgumentListSimplified arguments ) | IDENT arguments -> ^( METHOD_CALL ^( DOT qualifiedIdentifier IDENT ) genericTypeArgumentListSimplified arguments ) ) | THIS -> ^( DOT qualifiedIdentifier THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier arguments ) ) )?
            {
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:524:9: ( qualifiedIdentifier -> qualifiedIdentifier )
                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:524:13: qualifiedIdentifier
                {
                    pushFollow(FOLLOW_qualifiedIdentifier_in_qualifiedIdentExpression8995);
                    qualifiedIdentifier200 = qualifiedIdentifier();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_qualifiedIdentifier.add(qualifiedIdentifier200.getTree());

                    // AST REWRITE
                    // elements: qualifiedIdentifier
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (Tree) adaptor.nil();
                        // 524:61: -> qualifiedIdentifier
                        {
                            adaptor.addChild(root_0, stream_qualifiedIdentifier.nextTree());

                        }

                        retval.tree = root_0;
                    }
                }

                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:527:9: ( ( arrayDeclarator -> ^( arrayDeclarator $qualifiedIdentExpression) )+ ( DOT CLASS -> ^( DOT $qualifiedIdentExpression CLASS ) ) | arguments -> ^( METHOD_CALL qualifiedIdentifier arguments ) | outerDot= DOT ( CLASS -> ^( DOT qualifiedIdentifier CLASS ) | genericTypeArgumentListSimplified (Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier genericTypeArgumentListSimplified arguments ) | SUPER innerDot= DOT IDENT arguments -> ^( METHOD_CALL ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT ) genericTypeArgumentListSimplified arguments ) | IDENT arguments -> ^( METHOD_CALL ^( DOT qualifiedIdentifier IDENT ) genericTypeArgumentListSimplified arguments ) ) | THIS -> ^( DOT qualifiedIdentifier THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier arguments ) ) )?
                int alt53 = 4;
                alt53 = dfa53.predict(input);
                switch (alt53) {
                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:527:13: ( arrayDeclarator -> ^( arrayDeclarator $qualifiedIdentExpression) )+ ( DOT CLASS -> ^( DOT $qualifiedIdentExpression CLASS ) )
                    {
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:527:13: ( arrayDeclarator -> ^( arrayDeclarator $qualifiedIdentExpression) )+
                        int cnt50 = 0;
                        loop50:
                        do {
                            int alt50 = 2;
                            int LA50_0 = input.LA(1);

                            if ((LA50_0 == LBRACK)) {
                                alt50 = 1;
                            }

                            switch (alt50) {
                                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:527:17: arrayDeclarator
                                {
                                    pushFollow(FOLLOW_arrayDeclarator_in_qualifiedIdentExpression9065);
                                    arrayDeclarator201 = arrayDeclarator();

                                    state._fsp--;
                                    if (state.failed)
                                        return retval;
                                    if (state.backtracking == 0)
                                        stream_arrayDeclarator.add(arrayDeclarator201.getTree());

                                    // AST REWRITE
                                    // elements: arrayDeclarator, qualifiedIdentExpression
                                    // token labels: 
                                    // rule labels: retval
                                    // token list labels: 
                                    // rule list labels: 
                                    // wildcard labels: 
                                    if (state.backtracking == 0) {
                                        retval.tree = root_0;
                                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                        root_0 = (Tree) adaptor.nil();
                                        // 527:57: -> ^( arrayDeclarator $qualifiedIdentExpression)
                                        {
                                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:527:61: ^( arrayDeclarator $qualifiedIdentExpression)
                                            {
                                                Tree root_1 = (Tree) adaptor.nil();
                                                root_1 = (Tree) adaptor.becomeRoot(stream_arrayDeclarator.nextNode(), root_1);

                                                adaptor.addChild(root_1, stream_retval.nextTree());

                                                adaptor.addChild(root_0, root_1);
                                            }

                                        }

                                        retval.tree = root_0;
                                    }
                                }
                                break;

                                default:
                                    if (cnt50 >= 1)
                                        break loop50;
                                    if (state.backtracking > 0) {
                                        state.failed = true;
                                        return retval;
                                    }
                                    EarlyExitException eee
                                        = new EarlyExitException(50, input);
                                    throw eee;
                            }
                            cnt50++;
                        } while (true);

                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:529:13: ( DOT CLASS -> ^( DOT $qualifiedIdentExpression CLASS ) )
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:529:17: DOT CLASS
                        {
                            DOT202 = (Token) match(input, DOT, FOLLOW_DOT_in_qualifiedIdentExpression9133);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_DOT.add(DOT202);

                            CLASS203 = (Token) match(input, CLASS, FOLLOW_CLASS_in_qualifiedIdentExpression9135);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_CLASS.add(CLASS203);

                            // AST REWRITE
                            // elements: CLASS, qualifiedIdentExpression, DOT
                            // token labels: 
                            // rule labels: retval
                            // token list labels: 
                            // rule list labels: 
                            // wildcard labels: 
                            if (state.backtracking == 0) {
                                retval.tree = root_0;
                                RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                root_0 = (Tree) adaptor.nil();
                                // 529:57: -> ^( DOT $qualifiedIdentExpression CLASS )
                                {
                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:529:61: ^( DOT $qualifiedIdentExpression CLASS )
                                    {
                                        Tree root_1 = (Tree) adaptor.nil();
                                        root_1 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_1);

                                        adaptor.addChild(root_1, stream_retval.nextTree());
                                        adaptor.addChild(root_1, stream_CLASS.nextNode());

                                        adaptor.addChild(root_0, root_1);
                                    }

                                }

                                retval.tree = root_0;
                            }
                        }

                    }
                    break;
                    case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:531:13: arguments
                    {
                        pushFollow(FOLLOW_arguments_in_qualifiedIdentExpression9205);
                        arguments204 = arguments();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_arguments.add(arguments204.getTree());

                        // AST REWRITE
                        // elements: qualifiedIdentifier, arguments
                        // token labels: 
                        // rule labels: retval
                        // token list labels: 
                        // rule list labels: 
                        // wildcard labels: 
                        if (state.backtracking == 0) {
                            retval.tree = root_0;
                            RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                            root_0 = (Tree) adaptor.nil();
                            // 531:57: -> ^( METHOD_CALL qualifiedIdentifier arguments )
                            {
                                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:531:61: ^( METHOD_CALL qualifiedIdentifier arguments )
                                {
                                    Tree root_1 = (Tree) adaptor.nil();
                                    root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(METHOD_CALL, "METHOD_CALL"), root_1);

                                    adaptor.addChild(root_1, stream_qualifiedIdentifier.nextTree());
                                    adaptor.addChild(root_1, stream_arguments.nextTree());

                                    adaptor.addChild(root_0, root_1);
                                }

                            }

                            retval.tree = root_0;
                        }
                    }
                    break;
                    case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:532:13: outerDot= DOT ( CLASS -> ^( DOT qualifiedIdentifier CLASS ) | genericTypeArgumentListSimplified (Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier genericTypeArgumentListSimplified arguments ) | SUPER innerDot= DOT IDENT arguments -> ^( METHOD_CALL ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT ) genericTypeArgumentListSimplified arguments ) | IDENT arguments -> ^( METHOD_CALL ^( DOT qualifiedIdentifier IDENT ) genericTypeArgumentListSimplified arguments ) ) | THIS -> ^( DOT qualifiedIdentifier THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier arguments ) )
                    {
                        outerDot = (Token) match(input, DOT, FOLLOW_DOT_in_qualifiedIdentExpression9266);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_DOT.add(outerDot);

                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:533:13: ( CLASS -> ^( DOT qualifiedIdentifier CLASS ) | genericTypeArgumentListSimplified (Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier genericTypeArgumentListSimplified arguments ) | SUPER innerDot= DOT IDENT arguments -> ^( METHOD_CALL ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT ) genericTypeArgumentListSimplified arguments ) | IDENT arguments -> ^( METHOD_CALL ^( DOT qualifiedIdentifier IDENT ) genericTypeArgumentListSimplified arguments ) ) | THIS -> ^( DOT qualifiedIdentifier THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier arguments ) )
                        int alt52 = 4;
                        switch (input.LA(1)) {
                            case CLASS: {
                                alt52 = 1;
                            }
                            break;
                            case LESS_THAN: {
                                alt52 = 2;
                            }
                            break;
                            case THIS: {
                                alt52 = 3;
                            }
                            break;
                            case SUPER: {
                                alt52 = 4;
                            }
                            break;
                            default:
                                if (state.backtracking > 0) {
                                    state.failed = true;
                                    return retval;
                                }
                                NoViableAltException nvae
                                    = new NoViableAltException("", 52, 0, input);

                                throw nvae;
                        }

                        switch (alt52) {
                            case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:533:17: CLASS
                            {
                                CLASS205 = (Token) match(input, CLASS, FOLLOW_CLASS_in_qualifiedIdentExpression9284);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    stream_CLASS.add(CLASS205);

                                // AST REWRITE
                                // elements: CLASS, qualifiedIdentifier, DOT
                                // token labels: 
                                // rule labels: retval
                                // token list labels: 
                                // rule list labels: 
                                // wildcard labels: 
                                if (state.backtracking == 0) {
                                    retval.tree = root_0;
                                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                    root_0 = (Tree) adaptor.nil();
                                    // 533:57: -> ^( DOT qualifiedIdentifier CLASS )
                                    {
                                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:533:61: ^( DOT qualifiedIdentifier CLASS )
                                        {
                                            Tree root_1 = (Tree) adaptor.nil();
                                            root_1 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_1);

                                            adaptor.addChild(root_1, stream_qualifiedIdentifier.nextTree());
                                            adaptor.addChild(root_1, stream_CLASS.nextNode());

                                            adaptor.addChild(root_0, root_1);
                                        }

                                    }

                                    retval.tree = root_0;
                                }
                            }
                            break;
                            case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:534:17: genericTypeArgumentListSimplified (Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier genericTypeArgumentListSimplified arguments ) | SUPER innerDot= DOT IDENT arguments -> ^( METHOD_CALL ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT ) genericTypeArgumentListSimplified arguments ) | IDENT arguments -> ^( METHOD_CALL ^( DOT qualifiedIdentifier IDENT ) genericTypeArgumentListSimplified arguments ) )
                            {
                                pushFollow(FOLLOW_genericTypeArgumentListSimplified_in_qualifiedIdentExpression9347);
                                genericTypeArgumentListSimplified206 = genericTypeArgumentListSimplified();

                                state._fsp--;
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    stream_genericTypeArgumentListSimplified.add(genericTypeArgumentListSimplified206.getTree());
                                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:535:17: (Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier genericTypeArgumentListSimplified arguments ) | SUPER innerDot= DOT IDENT arguments -> ^( METHOD_CALL ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT ) genericTypeArgumentListSimplified arguments ) | IDENT arguments -> ^( METHOD_CALL ^( DOT qualifiedIdentifier IDENT ) genericTypeArgumentListSimplified arguments ) )
                                int alt51 = 3;
                                int LA51_0 = input.LA(1);

                                if ((LA51_0 == SUPER)) {
                                    int LA51_1 = input.LA(2);

                                    if ((LA51_1 == DOT)) {
                                        alt51 = 2;
                                    } else if ((LA51_1 == LPAREN)) {
                                        alt51 = 1;
                                    } else {
                                        if (state.backtracking > 0) {
                                            state.failed = true;
                                            return retval;
                                        }
                                        NoViableAltException nvae
                                            = new NoViableAltException("", 51, 1, input);

                                        throw nvae;
                                    }
                                } else if ((LA51_0 == IDENT)) {
                                    alt51 = 3;
                                } else {
                                    if (state.backtracking > 0) {
                                        state.failed = true;
                                        return retval;
                                    }
                                    NoViableAltException nvae
                                        = new NoViableAltException("", 51, 0, input);

                                    throw nvae;
                                }
                                switch (alt51) {
                                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:535:21: Super= SUPER arguments
                                    {
                                        Super = (Token) match(input, SUPER, FOLLOW_SUPER_in_qualifiedIdentExpression9372);
                                        if (state.failed)
                                            return retval;
                                        if (state.backtracking == 0)
                                            stream_SUPER.add(Super);

                                        pushFollow(FOLLOW_arguments_in_qualifiedIdentExpression9374);
                                        arguments207 = arguments();

                                        state._fsp--;
                                        if (state.failed)
                                            return retval;
                                        if (state.backtracking == 0)
                                            stream_arguments.add(arguments207.getTree());

                                        // AST REWRITE
                                        // elements: genericTypeArgumentListSimplified, qualifiedIdentifier, arguments
                                        // token labels: 
                                        // rule labels: retval
                                        // token list labels: 
                                        // rule list labels: 
                                        // wildcard labels: 
                                        if (state.backtracking == 0) {
                                            retval.tree = root_0;
                                            RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                            root_0 = (Tree) adaptor.nil();
                                            // 535:57: -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier genericTypeArgumentListSimplified arguments )
                                            {
                                                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:535:61: ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier genericTypeArgumentListSimplified arguments )
                                                {
                                                    Tree root_1 = (Tree) adaptor.nil();
                                                    root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(SUPER_CONSTRUCTOR_CALL, Super, "SUPER_CONSTRUCTOR_CALL"), root_1);

                                                    adaptor.addChild(root_1, stream_qualifiedIdentifier.nextTree());
                                                    adaptor.addChild(root_1, stream_genericTypeArgumentListSimplified.nextTree());
                                                    adaptor.addChild(root_1, stream_arguments.nextTree());

                                                    adaptor.addChild(root_0, root_1);
                                                }

                                            }

                                            retval.tree = root_0;
                                        }
                                    }
                                    break;
                                    case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:536:21: SUPER innerDot= DOT IDENT arguments
                                    {
                                        SUPER208 = (Token) match(input, SUPER, FOLLOW_SUPER_in_qualifiedIdentExpression9424);
                                        if (state.failed)
                                            return retval;
                                        if (state.backtracking == 0)
                                            stream_SUPER.add(SUPER208);

                                        innerDot = (Token) match(input, DOT, FOLLOW_DOT_in_qualifiedIdentExpression9428);
                                        if (state.failed)
                                            return retval;
                                        if (state.backtracking == 0)
                                            stream_DOT.add(innerDot);

                                        IDENT209 = (Token) match(input, IDENT, FOLLOW_IDENT_in_qualifiedIdentExpression9430);
                                        if (state.failed)
                                            return retval;
                                        if (state.backtracking == 0)
                                            stream_IDENT.add(IDENT209);

                                        pushFollow(FOLLOW_arguments_in_qualifiedIdentExpression9432);
                                        arguments210 = arguments();

                                        state._fsp--;
                                        if (state.failed)
                                            return retval;
                                        if (state.backtracking == 0)
                                            stream_arguments.add(arguments210.getTree());

                                        // AST REWRITE
                                        // elements: innerDot, qualifiedIdentifier, SUPER, IDENT, arguments, genericTypeArgumentListSimplified, outerDot
                                        // token labels: outerDot, innerDot
                                        // rule labels: retval
                                        // token list labels: 
                                        // rule list labels: 
                                        // wildcard labels: 
                                        if (state.backtracking == 0) {
                                            retval.tree = root_0;
                                            RewriteRuleTokenStream stream_outerDot = new RewriteRuleTokenStream(adaptor, "token outerDot", outerDot);
                                            RewriteRuleTokenStream stream_innerDot = new RewriteRuleTokenStream(adaptor, "token innerDot", innerDot);
                                            RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                            root_0 = (Tree) adaptor.nil();
                                            // 536:57: -> ^( METHOD_CALL ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT ) genericTypeArgumentListSimplified arguments )
                                            {
                                                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:536:61: ^( METHOD_CALL ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT ) genericTypeArgumentListSimplified arguments )
                                                {
                                                    Tree root_1 = (Tree) adaptor.nil();
                                                    root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(METHOD_CALL, "METHOD_CALL"), root_1);

                                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:536:75: ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT )
                                                    {
                                                        Tree root_2 = (Tree) adaptor.nil();
                                                        root_2 = (Tree) adaptor.becomeRoot(stream_innerDot.nextNode(), root_2);

                                                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:536:87: ^( $outerDot qualifiedIdentifier SUPER )
                                                        {
                                                            Tree root_3 = (Tree) adaptor.nil();
                                                            root_3 = (Tree) adaptor.becomeRoot(stream_outerDot.nextNode(), root_3);

                                                            adaptor.addChild(root_3, stream_qualifiedIdentifier.nextTree());
                                                            adaptor.addChild(root_3, stream_SUPER.nextNode());

                                                            adaptor.addChild(root_2, root_3);
                                                        }
                                                        adaptor.addChild(root_2, stream_IDENT.nextNode());

                                                        adaptor.addChild(root_1, root_2);
                                                    }
                                                    adaptor.addChild(root_1, stream_genericTypeArgumentListSimplified.nextTree());
                                                    adaptor.addChild(root_1, stream_arguments.nextTree());

                                                    adaptor.addChild(root_0, root_1);
                                                }

                                            }

                                            retval.tree = root_0;
                                        }
                                    }
                                    break;
                                    case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:537:21: IDENT arguments
                                    {
                                        IDENT211 = (Token) match(input, IDENT, FOLLOW_IDENT_in_qualifiedIdentExpression9482);
                                        if (state.failed)
                                            return retval;
                                        if (state.backtracking == 0)
                                            stream_IDENT.add(IDENT211);

                                        pushFollow(FOLLOW_arguments_in_qualifiedIdentExpression9484);
                                        arguments212 = arguments();

                                        state._fsp--;
                                        if (state.failed)
                                            return retval;
                                        if (state.backtracking == 0)
                                            stream_arguments.add(arguments212.getTree());

                                        // AST REWRITE
                                        // elements: DOT, genericTypeArgumentListSimplified, arguments, IDENT, qualifiedIdentifier
                                        // token labels: 
                                        // rule labels: retval
                                        // token list labels: 
                                        // rule list labels: 
                                        // wildcard labels: 
                                        if (state.backtracking == 0) {
                                            retval.tree = root_0;
                                            RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                            root_0 = (Tree) adaptor.nil();
                                            // 537:57: -> ^( METHOD_CALL ^( DOT qualifiedIdentifier IDENT ) genericTypeArgumentListSimplified arguments )
                                            {
                                                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:537:61: ^( METHOD_CALL ^( DOT qualifiedIdentifier IDENT ) genericTypeArgumentListSimplified arguments )
                                                {
                                                    Tree root_1 = (Tree) adaptor.nil();
                                                    root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(METHOD_CALL, "METHOD_CALL"), root_1);

                                                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:537:75: ^( DOT qualifiedIdentifier IDENT )
                                                    {
                                                        Tree root_2 = (Tree) adaptor.nil();
                                                        root_2 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_2);

                                                        adaptor.addChild(root_2, stream_qualifiedIdentifier.nextTree());
                                                        adaptor.addChild(root_2, stream_IDENT.nextNode());

                                                        adaptor.addChild(root_1, root_2);
                                                    }
                                                    adaptor.addChild(root_1, stream_genericTypeArgumentListSimplified.nextTree());
                                                    adaptor.addChild(root_1, stream_arguments.nextTree());

                                                    adaptor.addChild(root_0, root_1);
                                                }

                                            }

                                            retval.tree = root_0;
                                        }
                                    }
                                    break;

                                }

                            }
                            break;
                            case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:539:17: THIS
                            {
                                THIS213 = (Token) match(input, THIS, FOLLOW_THIS_in_qualifiedIdentExpression9559);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    stream_THIS.add(THIS213);

                                // AST REWRITE
                                // elements: THIS, DOT, qualifiedIdentifier
                                // token labels: 
                                // rule labels: retval
                                // token list labels: 
                                // rule list labels: 
                                // wildcard labels: 
                                if (state.backtracking == 0) {
                                    retval.tree = root_0;
                                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                    root_0 = (Tree) adaptor.nil();
                                    // 539:57: -> ^( DOT qualifiedIdentifier THIS )
                                    {
                                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:539:61: ^( DOT qualifiedIdentifier THIS )
                                        {
                                            Tree root_1 = (Tree) adaptor.nil();
                                            root_1 = (Tree) adaptor.becomeRoot(stream_DOT.nextNode(), root_1);

                                            adaptor.addChild(root_1, stream_qualifiedIdentifier.nextTree());
                                            adaptor.addChild(root_1, stream_THIS.nextNode());

                                            adaptor.addChild(root_0, root_1);
                                        }

                                    }

                                    retval.tree = root_0;
                                }
                            }
                            break;
                            case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:540:17: Super= SUPER arguments
                            {
                                Super = (Token) match(input, SUPER, FOLLOW_SUPER_in_qualifiedIdentExpression9625);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    stream_SUPER.add(Super);

                                pushFollow(FOLLOW_arguments_in_qualifiedIdentExpression9627);
                                arguments214 = arguments();

                                state._fsp--;
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    stream_arguments.add(arguments214.getTree());

                                // AST REWRITE
                                // elements: arguments, qualifiedIdentifier
                                // token labels: 
                                // rule labels: retval
                                // token list labels: 
                                // rule list labels: 
                                // wildcard labels: 
                                if (state.backtracking == 0) {
                                    retval.tree = root_0;
                                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                                    root_0 = (Tree) adaptor.nil();
                                    // 540:57: -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier arguments )
                                    {
                                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:540:61: ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier arguments )
                                        {
                                            Tree root_1 = (Tree) adaptor.nil();
                                            root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(SUPER_CONSTRUCTOR_CALL, Super, "SUPER_CONSTRUCTOR_CALL"), root_1);

                                            adaptor.addChild(root_1, stream_qualifiedIdentifier.nextTree());
                                            adaptor.addChild(root_1, stream_arguments.nextTree());

                                            adaptor.addChild(root_0, root_1);
                                        }

                                    }

                                    retval.tree = root_0;
                                }
                            }
                            break;

                        }

                    }
                    break;

                }

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 42, qualifiedIdentExpression_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "qualifiedIdentExpression"
    public static class arguments_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "arguments"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:546:1: arguments : LPAREN ( expressionList )? RPAREN -> ^( ARGUMENT_LIST[$LPAREN, \"ARGUMENT_LIST\"] ( expressionList )? ) ;
    public final VNScriptParser.arguments_return arguments() throws RecognitionException
    {
        VNScriptParser.arguments_return retval = new VNScriptParser.arguments_return();
        retval.start = input.LT(1);
        int arguments_StartIndex = input.index();
        Tree root_0 = null;

        Token LPAREN215 = null;
        Token RPAREN217 = null;
        VNScriptParser.expressionList_return expressionList216 = null;

        Tree LPAREN215_tree = null;
        Tree RPAREN217_tree = null;
        RewriteRuleTokenStream stream_RPAREN = new RewriteRuleTokenStream(adaptor, "token RPAREN");
        RewriteRuleTokenStream stream_LPAREN = new RewriteRuleTokenStream(adaptor, "token LPAREN");
        RewriteRuleSubtreeStream stream_expressionList = new RewriteRuleSubtreeStream(adaptor, "rule expressionList");
        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 43)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:547:5: ( LPAREN ( expressionList )? RPAREN -> ^( ARGUMENT_LIST[$LPAREN, \"ARGUMENT_LIST\"] ( expressionList )? ) )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:547:9: LPAREN ( expressionList )? RPAREN
            {
                LPAREN215 = (Token) match(input, LPAREN, FOLLOW_LPAREN_in_arguments9702);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_LPAREN.add(LPAREN215);

                // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:547:16: ( expressionList )?
                int alt54 = 2;
                int LA54_0 = input.LA(1);

                if ((LA54_0 == DEC || LA54_0 == INC || LA54_0 == LESS_THAN || LA54_0 == LOGICAL_NOT || (LA54_0 >= LPAREN && LA54_0 <= MINUS) || LA54_0 == NOT || LA54_0 == PLUS || LA54_0 == BOOLEAN || LA54_0 == BYTE || LA54_0 == CHAR || LA54_0 == DOUBLE || LA54_0 == FALSE || LA54_0 == FLOAT || (LA54_0 >= INT && LA54_0 <= LONG) || LA54_0 == NULL || LA54_0 == SHORT || LA54_0 == SUPER || LA54_0 == THIS || LA54_0 == TRUE || LA54_0 == VOID || (LA54_0 >= IDENT && LA54_0 <= STRING_LITERAL))) {
                    alt54 = 1;
                }
                switch (alt54) {
                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:0:0: expressionList
                    {
                        pushFollow(FOLLOW_expressionList_in_arguments9704);
                        expressionList216 = expressionList();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_expressionList.add(expressionList216.getTree());

                    }
                    break;

                }

                RPAREN217 = (Token) match(input, RPAREN, FOLLOW_RPAREN_in_arguments9707);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_RPAREN.add(RPAREN217);

                // AST REWRITE
                // elements: expressionList
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (Tree) adaptor.nil();
                    // 548:9: -> ^( ARGUMENT_LIST[$LPAREN, \"ARGUMENT_LIST\"] ( expressionList )? )
                    {
                        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:548:13: ^( ARGUMENT_LIST[$LPAREN, \"ARGUMENT_LIST\"] ( expressionList )? )
                        {
                            Tree root_1 = (Tree) adaptor.nil();
                            root_1 = (Tree) adaptor.becomeRoot((Tree) adaptor.create(ARGUMENT_LIST, LPAREN215, "ARGUMENT_LIST"), root_1);

                            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:548:55: ( expressionList )?
                            if (stream_expressionList.hasNext()) {
                                adaptor.addChild(root_1, stream_expressionList.nextTree());

                            }
                            stream_expressionList.reset();

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 43, arguments_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "arguments"
    public static class literal_return extends ParserRuleReturnScope
    {

        Tree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "literal"
    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:551:1: literal : ( HEX_LITERAL | OCTAL_LITERAL | DECIMAL_LITERAL | FLOATING_POINT_LITERAL | CHARACTER_LITERAL | STRING_LITERAL | TRUE | FALSE | NULL );
    public final VNScriptParser.literal_return literal() throws RecognitionException
    {
        VNScriptParser.literal_return retval = new VNScriptParser.literal_return();
        retval.start = input.LT(1);
        int literal_StartIndex = input.index();
        Tree root_0 = null;

        Token set218 = null;

        Tree set218_tree = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 44)) {
                return retval;
            }
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:552:5: ( HEX_LITERAL | OCTAL_LITERAL | DECIMAL_LITERAL | FLOATING_POINT_LITERAL | CHARACTER_LITERAL | STRING_LITERAL | TRUE | FALSE | NULL )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:
            {
                root_0 = (Tree) adaptor.nil();

                set218 = (Token) input.LT(1);
                if (input.LA(1) == FALSE || input.LA(1) == NULL || input.LA(1) == TRUE || (input.LA(1) >= HEX_LITERAL && input.LA(1) <= STRING_LITERAL)) {
                    input.consume();
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, (Tree) adaptor.create(set218));
                    state.errorRecovery = false;
                    state.failed = false;
                } else {
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    throw mse;
                }

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (Tree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (Tree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
            if (state.backtracking > 0) {
                memoize(input, 44, literal_StartIndex);
            }
        }
        return retval;
    }

    // $ANTLR end "literal"
    // $ANTLR start synpred4_VNScript
    public final void synpred4_VNScript_fragment() throws RecognitionException
    {
        VNScriptParser.statement_return elseStat = null;

        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:232:13: ( ELSE elseStat= statement )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:232:13: ELSE elseStat= statement
        {
            match(input, ELSE, FOLLOW_ELSE_in_synpred4_VNScript4625);
            if (state.failed)
                return;
            pushFollow(FOLLOW_statement_in_synpred4_VNScript4629);
            elseStat = statement();

            state._fsp--;
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred4_VNScript
    // $ANTLR start synpred16_VNScript
    public final void synpred16_VNScript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:278:3: ( range_offset )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:278:3: range_offset
        {
            pushFollow(FOLLOW_range_offset_in_synpred16_VNScript5299);
            range_offset();

            state._fsp--;
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred16_VNScript
    // $ANTLR start synpred17_VNScript
    public final void synpred17_VNScript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:279:4: ( range_left )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:279:4: range_left
        {
            pushFollow(FOLLOW_range_left_in_synpred17_VNScript5312);
            range_left();

            state._fsp--;
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred17_VNScript
    // $ANTLR start synpred18_VNScript
    public final void synpred18_VNScript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:280:4: ( range_single )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:280:4: range_single
        {
            pushFollow(FOLLOW_range_single_in_synpred18_VNScript5325);
            range_single();

            state._fsp--;
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred18_VNScript
    // $ANTLR start synpred19_VNScript
    public final void synpred19_VNScript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:281:4: ( range_right )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:281:4: range_right
        {
            pushFollow(FOLLOW_range_right_in_synpred19_VNScript5338);
            range_right();

            state._fsp--;
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred19_VNScript
    // $ANTLR start synpred62_VNScript
    public final void synpred62_VNScript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:415:9: ( LPAREN type RPAREN unaryExpression )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:415:9: LPAREN type RPAREN unaryExpression
        {
            match(input, LPAREN, FOLLOW_LPAREN_in_synpred62_VNScript6757);
            if (state.failed)
                return;
            pushFollow(FOLLOW_type_in_synpred62_VNScript6759);
            type();

            state._fsp--;
            if (state.failed)
                return;
            match(input, RPAREN, FOLLOW_RPAREN_in_synpred62_VNScript6761);
            if (state.failed)
                return;
            pushFollow(FOLLOW_unaryExpression_in_synpred62_VNScript6763);
            unaryExpression();

            state._fsp--;
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred62_VNScript
    // $ANTLR start synpred63_VNScript
    public final void synpred63_VNScript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:427:21: ( genericTypeArgumentListSimplified )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:427:21: genericTypeArgumentListSimplified
        {
            pushFollow(FOLLOW_genericTypeArgumentListSimplified_in_synpred63_VNScript6956);
            genericTypeArgumentListSimplified();

            state._fsp--;
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred63_VNScript
    // $ANTLR start synpred96_VNScript
    public final void synpred96_VNScript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:505:9: ( GREATER_THAN )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:505:9: GREATER_THAN
        {
            match(input, GREATER_THAN, FOLLOW_GREATER_THAN_in_synpred96_VNScript8847);
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred96_VNScript
    // $ANTLR start synpred97_VNScript
    public final void synpred97_VNScript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:506:9: ( SHIFT_RIGHT )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:506:9: SHIFT_RIGHT
        {
            match(input, SHIFT_RIGHT, FOLLOW_SHIFT_RIGHT_in_synpred97_VNScript8857);
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred97_VNScript
    // $ANTLR start synpred98_VNScript
    public final void synpred98_VNScript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:507:9: ( BIT_SHIFT_RIGHT )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:507:9: BIT_SHIFT_RIGHT
        {
            match(input, BIT_SHIFT_RIGHT, FOLLOW_BIT_SHIFT_RIGHT_in_synpred98_VNScript8867);
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred98_VNScript
    // $ANTLR start synpred99_VNScript
    public final void synpred99_VNScript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:512:50: ( COMMA genericTypeArgumentSimplified )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:512:50: COMMA genericTypeArgumentSimplified
        {
            match(input, COMMA, FOLLOW_COMMA_in_synpred99_VNScript8900);
            if (state.failed)
                return;
            pushFollow(FOLLOW_genericTypeArgumentSimplified_in_synpred99_VNScript8902);
            genericTypeArgumentSimplified();

            state._fsp--;
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred99_VNScript
    // $ANTLR start synpred102_VNScript
    public final void synpred102_VNScript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:527:13: ( ( arrayDeclarator )+ ( DOT CLASS ) )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:527:13: ( arrayDeclarator )+ ( DOT CLASS )
        {
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:527:13: ( arrayDeclarator )+
            int cnt69 = 0;
            loop69:
            do {
                int alt69 = 2;
                int LA69_0 = input.LA(1);

                if ((LA69_0 == LBRACK)) {
                    alt69 = 1;
                }

                switch (alt69) {
                    case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:527:17: arrayDeclarator
                    {
                        pushFollow(FOLLOW_arrayDeclarator_in_synpred102_VNScript9065);
                        arrayDeclarator();

                        state._fsp--;
                        if (state.failed)
                            return;

                    }
                    break;

                    default:
                        if (cnt69 >= 1)
                            break loop69;
                        if (state.backtracking > 0) {
                            state.failed = true;
                            return;
                        }
                        EarlyExitException eee
                            = new EarlyExitException(69, input);
                        throw eee;
                }
                cnt69++;
            } while (true);

            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:529:13: ( DOT CLASS )
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:529:17: DOT CLASS
            {
                match(input, DOT, FOLLOW_DOT_in_synpred102_VNScript9133);
                if (state.failed)
                    return;
                match(input, CLASS, FOLLOW_CLASS_in_synpred102_VNScript9135);
                if (state.failed)
                    return;

            }

        }
    }

    // $ANTLR end synpred102_VNScript
    // $ANTLR start synpred109_VNScript
    public final void synpred109_VNScript_fragment() throws RecognitionException
    {
        Token outerDot = null;
        Token Super = null;
        Token innerDot = null;

        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:532:13: (outerDot= DOT ( CLASS | genericTypeArgumentListSimplified (Super= SUPER arguments | SUPER innerDot= DOT IDENT arguments | IDENT arguments ) | THIS | Super= SUPER arguments ) )
        // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:532:13: outerDot= DOT ( CLASS | genericTypeArgumentListSimplified (Super= SUPER arguments | SUPER innerDot= DOT IDENT arguments | IDENT arguments ) | THIS | Super= SUPER arguments )
        {
            outerDot = (Token) match(input, DOT, FOLLOW_DOT_in_synpred109_VNScript9266);
            if (state.failed)
                return;
            // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:533:13: ( CLASS | genericTypeArgumentListSimplified (Super= SUPER arguments | SUPER innerDot= DOT IDENT arguments | IDENT arguments ) | THIS | Super= SUPER arguments )
            int alt72 = 4;
            switch (input.LA(1)) {
                case CLASS: {
                    alt72 = 1;
                }
                break;
                case LESS_THAN: {
                    alt72 = 2;
                }
                break;
                case THIS: {
                    alt72 = 3;
                }
                break;
                case SUPER: {
                    alt72 = 4;
                }
                break;
                default:
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 72, 0, input);

                    throw nvae;
            }

            switch (alt72) {
                case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:533:17: CLASS
                {
                    match(input, CLASS, FOLLOW_CLASS_in_synpred109_VNScript9284);
                    if (state.failed)
                        return;

                }
                break;
                case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:534:17: genericTypeArgumentListSimplified (Super= SUPER arguments | SUPER innerDot= DOT IDENT arguments | IDENT arguments )
                {
                    pushFollow(FOLLOW_genericTypeArgumentListSimplified_in_synpred109_VNScript9347);
                    genericTypeArgumentListSimplified();

                    state._fsp--;
                    if (state.failed)
                        return;
                    // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:535:17: (Super= SUPER arguments | SUPER innerDot= DOT IDENT arguments | IDENT arguments )
                    int alt71 = 3;
                    int LA71_0 = input.LA(1);

                    if ((LA71_0 == SUPER)) {
                        int LA71_1 = input.LA(2);

                        if ((LA71_1 == DOT)) {
                            alt71 = 2;
                        } else if ((LA71_1 == LPAREN)) {
                            alt71 = 1;
                        } else {
                            if (state.backtracking > 0) {
                                state.failed = true;
                                return;
                            }
                            NoViableAltException nvae
                                = new NoViableAltException("", 71, 1, input);

                            throw nvae;
                        }
                    } else if ((LA71_0 == IDENT)) {
                        alt71 = 3;
                    } else {
                        if (state.backtracking > 0) {
                            state.failed = true;
                            return;
                        }
                        NoViableAltException nvae
                            = new NoViableAltException("", 71, 0, input);

                        throw nvae;
                    }
                    switch (alt71) {
                        case 1: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:535:21: Super= SUPER arguments
                        {
                            Super = (Token) match(input, SUPER, FOLLOW_SUPER_in_synpred109_VNScript9372);
                            if (state.failed)
                                return;
                            pushFollow(FOLLOW_arguments_in_synpred109_VNScript9374);
                            arguments();

                            state._fsp--;
                            if (state.failed)
                                return;

                        }
                        break;
                        case 2: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:536:21: SUPER innerDot= DOT IDENT arguments
                        {
                            match(input, SUPER, FOLLOW_SUPER_in_synpred109_VNScript9424);
                            if (state.failed)
                                return;
                            innerDot = (Token) match(input, DOT, FOLLOW_DOT_in_synpred109_VNScript9428);
                            if (state.failed)
                                return;
                            match(input, IDENT, FOLLOW_IDENT_in_synpred109_VNScript9430);
                            if (state.failed)
                                return;
                            pushFollow(FOLLOW_arguments_in_synpred109_VNScript9432);
                            arguments();

                            state._fsp--;
                            if (state.failed)
                                return;

                        }
                        break;
                        case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:537:21: IDENT arguments
                        {
                            match(input, IDENT, FOLLOW_IDENT_in_synpred109_VNScript9482);
                            if (state.failed)
                                return;
                            pushFollow(FOLLOW_arguments_in_synpred109_VNScript9484);
                            arguments();

                            state._fsp--;
                            if (state.failed)
                                return;

                        }
                        break;

                    }

                }
                break;
                case 3: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:539:17: THIS
                {
                    match(input, THIS, FOLLOW_THIS_in_synpred109_VNScript9559);
                    if (state.failed)
                        return;

                }
                break;
                case 4: // /home/staff/lyczek/NetBeansProjects/NewParser/VNScript.g:540:17: Super= SUPER arguments
                {
                    Super = (Token) match(input, SUPER, FOLLOW_SUPER_in_synpred109_VNScript9625);
                    if (state.failed)
                        return;
                    pushFollow(FOLLOW_arguments_in_synpred109_VNScript9627);
                    arguments();

                    state._fsp--;
                    if (state.failed)
                        return;

                }
                break;

            }

        }
    }

    // $ANTLR end synpred109_VNScript
    // Delegated rules
    public final boolean synpred18_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred18_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred4_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred4_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred109_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred109_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred102_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred102_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred19_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred19_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred96_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred96_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred97_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred97_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred63_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred63_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred98_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred98_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred99_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred99_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred16_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred16_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred17_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred17_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public final boolean synpred62_VNScript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred62_VNScript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    protected DFA4 dfa4 = new DFA4(this);
    protected DFA9 dfa9 = new DFA9(this);
    protected DFA32 dfa32 = new DFA32(this);
    protected DFA45 dfa45 = new DFA45(this);
    protected DFA47 dfa47 = new DFA47(this);
    protected DFA53 dfa53 = new DFA53(this);
    static final String DFA4_eotS
        = "\12\uffff";
    static final String DFA4_eofS
        = "\12\uffff";
    static final String DFA4_minS
        = "\1\14\6\uffff\1\4\2\uffff";
    static final String DFA4_maxS
        = "\1\u00b0\6\uffff\1\u00bd\2\uffff";
    static final String DFA4_acceptS
        = "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\uffff\1\7\1\10";
    static final String DFA4_specialS
        = "\12\uffff}>";
    static final String[] DFA4_transitionS = {
        "\1\6\10\uffff\1\6\1\uffff\1\1\1\uffff\1\6\1\uffff\1\6\1\uffff" +
        "\2\6\3\uffff\1\6\3\uffff\1\6\5\uffff\1\10\12\uffff\1\6\1\uffff" +
        "\1\6\2\uffff\1\6\3\uffff\1\5\1\6\3\uffff\1\6\2\uffff\1\6\1\3" +
        "\1\2\4\uffff\2\6\2\uffff\1\6\5\uffff\1\6\2\uffff\1\6\2\uffff" +
        "\1\6\3\uffff\1\6\1\uffff\1\6\1\uffff\1\4\102\uffff\1\7\6\6",
        "",
        "",
        "",
        "",
        "",
        "",
        "\3\6\1\uffff\2\6\2\uffff\4\6\2\uffff\5\6\1\uffff\3\6\1\uffff" +
        "\6\6\1\uffff\6\6\3\uffff\11\6\27\uffff\1\6\160\uffff\1\11",
        "",
        ""
    };

    static final short[] DFA4_eot = DFA.unpackEncodedString(DFA4_eotS);
    static final short[] DFA4_eof = DFA.unpackEncodedString(DFA4_eofS);
    static final char[] DFA4_min = DFA.unpackEncodedStringToUnsignedChars(DFA4_minS);
    static final char[] DFA4_max = DFA.unpackEncodedStringToUnsignedChars(DFA4_maxS);
    static final short[] DFA4_accept = DFA.unpackEncodedString(DFA4_acceptS);
    static final short[] DFA4_special = DFA.unpackEncodedString(DFA4_specialS);
    static final short[][] DFA4_transition;

    static {
        int numStates = DFA4_transitionS.length;
        DFA4_transition = new short[numStates][];
        for (int i = 0; i < numStates; i++) {
            DFA4_transition[i] = DFA.unpackEncodedString(DFA4_transitionS[i]);
        }
    }

    class DFA4 extends DFA
    {

        public DFA4(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = DFA4_eot;
            this.eof = DFA4_eof;
            this.min = DFA4_min;
            this.max = DFA4_max;
            this.accept = DFA4_accept;
            this.special = DFA4_special;
            this.transition = DFA4_transition;
        }

        public String getDescription()
        {
            return "230:1: statement : ( block | IF parenthesizedExpression ifStat= statement ( ELSE elseStat= statement -> ^( IF parenthesizedExpression $ifStat $elseStat) | -> ^( IF parenthesizedExpression $ifStat) ) | FOR LPAREN forInit SEMI forCondition SEMI forUpdater RPAREN statement -> ^( FOR forInit forCondition forUpdater statement ) | WHILE parenthesizedExpression statement -> ^( WHILE parenthesizedExpression statement ) | DO statement WHILE parenthesizedExpression SEMI -> ^( DO statement parenthesizedExpression ) | expression SEMI | SEMI | declaration SEMI -> ^( DECLARATION declaration ) );";
        }
    }

    static final String DFA9_eotS
        = "\25\uffff";
    static final String DFA9_eofS
        = "\25\uffff";
    static final String DFA9_minS
        = "\1\12\17\0\5\uffff";
    static final String DFA9_maxS
        = "\1\u00b0\17\0\5\uffff";
    static final String DFA9_acceptS
        = "\20\uffff\1\1\1\2\1\3\1\4\1\5";
    static final String DFA9_specialS
        = "\1\uffff\1\0\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13\1\14" +
        "\1\15\1\16\5\uffff}>";
    static final String[] DFA9_transitionS = {
        "\1\17\1\uffff\1\4\10\uffff\1\3\3\uffff\1\12\1\uffff\1\6\1\uffff" +
        "\1\7\1\2\3\uffff\1\5\3\uffff\1\1\20\uffff\1\15\1\uffff\1\15" +
        "\2\uffff\1\15\4\uffff\1\15\3\uffff\1\10\2\uffff\1\15\6\uffff" +
        "\2\15\2\uffff\1\10\5\uffff\1\15\2\uffff\1\14\2\uffff\1\13\3" +
        "\uffff\1\10\1\uffff\1\16\104\uffff\1\11\6\10",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "",
        "",
        "",
        "",
        ""
    };

    static final short[] DFA9_eot = DFA.unpackEncodedString(DFA9_eotS);
    static final short[] DFA9_eof = DFA.unpackEncodedString(DFA9_eofS);
    static final char[] DFA9_min = DFA.unpackEncodedStringToUnsignedChars(DFA9_minS);
    static final char[] DFA9_max = DFA.unpackEncodedStringToUnsignedChars(DFA9_maxS);
    static final short[] DFA9_accept = DFA.unpackEncodedString(DFA9_acceptS);
    static final short[] DFA9_special = DFA.unpackEncodedString(DFA9_specialS);
    static final short[][] DFA9_transition;

    static {
        int numStates = DFA9_transitionS.length;
        DFA9_transition = new short[numStates][];
        for (int i = 0; i < numStates; i++) {
            DFA9_transition[i] = DFA.unpackEncodedString(DFA9_transitionS[i]);
        }
    }

    class DFA9 extends DFA
    {

        public DFA9(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = DFA9_eot;
            this.eof = DFA9_eof;
            this.min = DFA9_min;
            this.max = DFA9_max;
            this.accept = DFA9_accept;
            this.special = DFA9_special;
            this.transition = DFA9_transition;
        }

        public String getDescription()
        {
            return "277:1: range : ( range_offset -> ^( RANGE_OFFSET range_offset ) | range_left -> ^( RANGE_LEFT range_left ) | range_single -> ^( RANGE_SINGLE range_single ) | range_right -> ^( RANGE_RIGHT range_right ) | range_all -> ^( RANGE_ALL ) );";
        }

        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException
        {
            TokenStream input = (TokenStream) _input;
            int _s = s;
            switch (s) {
                case 0:
                    int LA9_1 = input.LA(1);

                    int index9_1 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred16_VNScript())) {
                        s = 16;
                    } else if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_1);
                    if (s >= 0)
                        return s;
                    break;
                case 1:
                    int LA9_2 = input.LA(1);

                    int index9_2 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred16_VNScript())) {
                        s = 16;
                    } else if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_2);
                    if (s >= 0)
                        return s;
                    break;
                case 2:
                    int LA9_3 = input.LA(1);

                    int index9_3 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_3);
                    if (s >= 0)
                        return s;
                    break;
                case 3:
                    int LA9_4 = input.LA(1);

                    int index9_4 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_4);
                    if (s >= 0)
                        return s;
                    break;
                case 4:
                    int LA9_5 = input.LA(1);

                    int index9_5 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_5);
                    if (s >= 0)
                        return s;
                    break;
                case 5:
                    int LA9_6 = input.LA(1);

                    int index9_6 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_6);
                    if (s >= 0)
                        return s;
                    break;
                case 6:
                    int LA9_7 = input.LA(1);

                    int index9_7 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_7);
                    if (s >= 0)
                        return s;
                    break;
                case 7:
                    int LA9_8 = input.LA(1);

                    int index9_8 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_8);
                    if (s >= 0)
                        return s;
                    break;
                case 8:
                    int LA9_9 = input.LA(1);

                    int index9_9 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_9);
                    if (s >= 0)
                        return s;
                    break;
                case 9:
                    int LA9_10 = input.LA(1);

                    int index9_10 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_10);
                    if (s >= 0)
                        return s;
                    break;
                case 10:
                    int LA9_11 = input.LA(1);

                    int index9_11 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_11);
                    if (s >= 0)
                        return s;
                    break;
                case 11:
                    int LA9_12 = input.LA(1);

                    int index9_12 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_12);
                    if (s >= 0)
                        return s;
                    break;
                case 12:
                    int LA9_13 = input.LA(1);

                    int index9_13 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_13);
                    if (s >= 0)
                        return s;
                    break;
                case 13:
                    int LA9_14 = input.LA(1);

                    int index9_14 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred17_VNScript())) {
                        s = 17;
                    } else if ((synpred18_VNScript())) {
                        s = 18;
                    }

                    input.seek(index9_14);
                    if (s >= 0)
                        return s;
                    break;
                case 14:
                    int LA9_15 = input.LA(1);

                    int index9_15 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred19_VNScript())) {
                        s = 19;
                    } else if ((true)) {
                        s = 20;
                    }

                    input.seek(index9_15);
                    if (s >= 0)
                        return s;
                    break;
            }
            if (state.backtracking > 0) {
                state.failed = true;
                return -1;
            }
            NoViableAltException nvae
                = new NoViableAltException(getDescription(), 9, _s, input);
            error(nvae);
            throw nvae;
        }
    }

    static final String DFA32_eotS
        = "\14\uffff";
    static final String DFA32_eofS
        = "\14\uffff";
    static final String DFA32_minS
        = "\1\31\2\uffff\1\0\10\uffff";
    static final String DFA32_maxS
        = "\1\u00b0\2\uffff\1\0\10\uffff";
    static final String DFA32_acceptS
        = "\1\uffff\1\1\1\2\1\uffff\1\4\6\uffff\1\3";
    static final String DFA32_specialS
        = "\3\uffff\1\0\10\uffff}>";
    static final String[] DFA32_transitionS = {
        "\1\4\1\uffff\1\2\1\uffff\1\3\4\uffff\1\1\24\uffff\1\4\1\uffff" +
        "\1\4\2\uffff\1\4\4\uffff\1\4\3\uffff\1\4\2\uffff\1\4\6\uffff" +
        "\2\4\2\uffff\1\4\5\uffff\1\4\2\uffff\1\4\2\uffff\1\4\3\uffff" +
        "\1\4\1\uffff\1\4\104\uffff\7\4",
        "",
        "",
        "\1\uffff",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""
    };

    static final short[] DFA32_eot = DFA.unpackEncodedString(DFA32_eotS);
    static final short[] DFA32_eof = DFA.unpackEncodedString(DFA32_eofS);
    static final char[] DFA32_min = DFA.unpackEncodedStringToUnsignedChars(DFA32_minS);
    static final char[] DFA32_max = DFA.unpackEncodedStringToUnsignedChars(DFA32_maxS);
    static final short[] DFA32_accept = DFA.unpackEncodedString(DFA32_acceptS);
    static final short[] DFA32_special = DFA.unpackEncodedString(DFA32_specialS);
    static final short[][] DFA32_transition;

    static {
        int numStates = DFA32_transitionS.length;
        DFA32_transition = new short[numStates][];
        for (int i = 0; i < numStates; i++) {
            DFA32_transition[i] = DFA.unpackEncodedString(DFA32_transitionS[i]);
        }
    }

    class DFA32 extends DFA
    {

        public DFA32(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 32;
            this.eot = DFA32_eot;
            this.eof = DFA32_eof;
            this.min = DFA32_min;
            this.max = DFA32_max;
            this.accept = DFA32_accept;
            this.special = DFA32_special;
            this.transition = DFA32_transition;
        }

        public String getDescription()
        {
            return "412:1: unaryExpressionNotPlusMinus : ( NOT unaryExpression -> ^( NOT unaryExpression ) | LOGICAL_NOT unaryExpression -> ^( LOGICAL_NOT unaryExpression ) | LPAREN type RPAREN unaryExpression -> ^( CAST_EXPR[$LPAREN, \"CAST_EXPR\"] type unaryExpression ) | postfixedExpression );";
        }

        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException
        {
            TokenStream input = (TokenStream) _input;
            int _s = s;
            switch (s) {
                case 0:
                    int LA32_3 = input.LA(1);

                    int index32_3 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred62_VNScript())) {
                        s = 11;
                    } else if ((true)) {
                        s = 4;
                    }

                    input.seek(index32_3);
                    if (s >= 0)
                        return s;
                    break;
            }
            if (state.backtracking > 0) {
                state.failed = true;
                return -1;
            }
            NoViableAltException nvae
                = new NoViableAltException(getDescription(), 32, _s, input);
            error(nvae);
            throw nvae;
        }
    }

    static final String DFA45_eotS
        = "\13\uffff";
    static final String DFA45_eofS
        = "\13\uffff";
    static final String DFA45_minS
        = "\1\31\5\uffff\1\17\4\uffff";
    static final String DFA45_maxS
        = "\1\u00b0\5\uffff\1\35\4\uffff";
    static final String DFA45_acceptS
        = "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\uffff\1\10\1\11\1\7\1\6";
    static final String DFA45_specialS
        = "\13\uffff}>";
    static final String[] DFA45_transitionS = {
        "\1\4\3\uffff\1\1\31\uffff\1\7\1\uffff\1\7\2\uffff\1\7\4\uffff" +
        "\1\7\3\uffff\1\2\2\uffff\1\7\6\uffff\2\7\2\uffff\1\2\5\uffff" +
        "\1\7\2\uffff\1\6\2\uffff\1\5\3\uffff\1\2\1\uffff\1\10\104\uffff" +
        "\1\3\6\2",
        "",
        "",
        "",
        "",
        "",
        "\1\11\15\uffff\1\12",
        "",
        "",
        "",
        ""
    };

    static final short[] DFA45_eot = DFA.unpackEncodedString(DFA45_eotS);
    static final short[] DFA45_eof = DFA.unpackEncodedString(DFA45_eofS);
    static final char[] DFA45_min = DFA.unpackEncodedStringToUnsignedChars(DFA45_minS);
    static final char[] DFA45_max = DFA.unpackEncodedStringToUnsignedChars(DFA45_maxS);
    static final short[] DFA45_accept = DFA.unpackEncodedString(DFA45_acceptS);
    static final short[] DFA45_special = DFA.unpackEncodedString(DFA45_specialS);
    static final short[][] DFA45_transition;

    static {
        int numStates = DFA45_transitionS.length;
        DFA45_transition = new short[numStates][];
        for (int i = 0; i < numStates; i++) {
            DFA45_transition[i] = DFA.unpackEncodedString(DFA45_transitionS[i]);
        }
    }

    class DFA45 extends DFA
    {

        public DFA45(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 45;
            this.eot = DFA45_eot;
            this.eof = DFA45_eof;
            this.min = DFA45_min;
            this.max = DFA45_max;
            this.accept = DFA45_accept;
            this.special = DFA45_special;
            this.transition = DFA45_transition;
        }

        public String getDescription()
        {
            return "464:1: primaryExpression : ( parenthesizedExpression | literal | qualifiedIdentExpression | genericTypeArgumentListSimplified ( SUPER ( arguments -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) | DOT IDENT arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) genericTypeArgumentListSimplified arguments ) ) | IDENT arguments -> ^( METHOD_CALL IDENT genericTypeArgumentListSimplified arguments ) | THIS arguments -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] genericTypeArgumentListSimplified arguments ) ) | ( THIS -> THIS ) ( arguments -> ^( THIS_CONSTRUCTOR_CALL[$THIS, \"THIS_CONSTRUCTOR_CALL\"] arguments ) )? | SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$SUPER, \"SUPER_CONSTRUCTOR_CALL\"] arguments ) | ( SUPER DOT IDENT ) ( arguments -> ^( METHOD_CALL ^( DOT SUPER IDENT ) arguments ) | -> ^( DOT SUPER IDENT ) ) | ( primitiveType -> primitiveType ) ( arrayDeclarator -> ^( arrayDeclarator $primaryExpression) )* DOT CLASS -> ^( DOT $primaryExpression CLASS ) | VOID DOT CLASS -> ^( DOT VOID CLASS ) );";
        }
    }

    static final String DFA47_eotS
        = "\63\uffff";
    static final String DFA47_eofS
        = "\1\4\62\uffff";
    static final String DFA47_minS
        = "\1\4\3\0\57\uffff";
    static final String DFA47_maxS
        = "\1\u00aa\3\0\57\uffff";
    static final String DFA47_acceptS
        = "\4\uffff\1\4\53\uffff\1\1\1\2\1\3";
    static final String DFA47_specialS
        = "\1\uffff\1\0\1\1\1\2\57\uffff}>";
    static final String[] DFA47_transitionS = {
        "\3\4\1\uffff\1\3\7\4\2\uffff\2\4\1\1\2\4\1\uffff\3\4\1\uffff" +
        "\6\4\1\uffff\7\4\1\uffff\4\4\1\2\5\4\27\uffff\1\4\17\uffff\1" +
        "\4\2\uffff\1\4\112\uffff\1\4",
        "\1\uffff",
        "\1\uffff",
        "\1\uffff",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""
    };

    static final short[] DFA47_eot = DFA.unpackEncodedString(DFA47_eotS);
    static final short[] DFA47_eof = DFA.unpackEncodedString(DFA47_eofS);
    static final char[] DFA47_min = DFA.unpackEncodedStringToUnsignedChars(DFA47_minS);
    static final char[] DFA47_max = DFA.unpackEncodedStringToUnsignedChars(DFA47_maxS);
    static final short[] DFA47_accept = DFA.unpackEncodedString(DFA47_acceptS);
    static final short[] DFA47_special = DFA.unpackEncodedString(DFA47_specialS);
    static final short[][] DFA47_transition;

    static {
        int numStates = DFA47_transitionS.length;
        DFA47_transition = new short[numStates][];
        for (int i = 0; i < numStates; i++) {
            DFA47_transition[i] = DFA.unpackEncodedString(DFA47_transitionS[i]);
        }
    }

    class DFA47 extends DFA
    {

        public DFA47(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 47;
            this.eot = DFA47_eot;
            this.eof = DFA47_eof;
            this.min = DFA47_min;
            this.max = DFA47_max;
            this.accept = DFA47_accept;
            this.special = DFA47_special;
            this.transition = DFA47_transition;
        }

        public String getDescription()
        {
            return "501:1: genericTypeListClosing : ( GREATER_THAN | SHIFT_RIGHT | BIT_SHIFT_RIGHT | );";
        }

        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException
        {
            TokenStream input = (TokenStream) _input;
            int _s = s;
            switch (s) {
                case 0:
                    int LA47_1 = input.LA(1);

                    int index47_1 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred96_VNScript())) {
                        s = 48;
                    } else if ((true)) {
                        s = 4;
                    }

                    input.seek(index47_1);
                    if (s >= 0)
                        return s;
                    break;
                case 1:
                    int LA47_2 = input.LA(1);

                    int index47_2 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred97_VNScript())) {
                        s = 49;
                    } else if ((true)) {
                        s = 4;
                    }

                    input.seek(index47_2);
                    if (s >= 0)
                        return s;
                    break;
                case 2:
                    int LA47_3 = input.LA(1);

                    int index47_3 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred98_VNScript())) {
                        s = 50;
                    } else if ((true)) {
                        s = 4;
                    }

                    input.seek(index47_3);
                    if (s >= 0)
                        return s;
                    break;
            }
            if (state.backtracking > 0) {
                state.failed = true;
                return -1;
            }
            NoViableAltException nvae
                = new NoViableAltException(getDescription(), 47, _s, input);
            error(nvae);
            throw nvae;
        }
    }

    static final String DFA53_eotS
        = "\57\uffff";
    static final String DFA53_eofS
        = "\1\4\56\uffff";
    static final String DFA53_minS
        = "\1\4\1\0\1\uffff\1\0\53\uffff";
    static final String DFA53_maxS
        = "\1\114\1\0\1\uffff\1\0\53\uffff";
    static final String DFA53_acceptS
        = "\2\uffff\1\2\1\uffff\1\4\50\uffff\1\1\1\3";
    static final String DFA53_specialS
        = "\1\uffff\1\0\1\uffff\1\1\53\uffff}>";
    static final String[] DFA53_transitionS = {
        "\3\4\1\uffff\7\4\1\3\2\uffff\4\4\1\1\1\uffff\3\4\1\uffff\1\4" +
        "\1\2\4\4\1\uffff\7\4\1\uffff\12\4\27\uffff\1\4",
        "\1\uffff",
        "",
        "\1\uffff",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""
    };

    static final short[] DFA53_eot = DFA.unpackEncodedString(DFA53_eotS);
    static final short[] DFA53_eof = DFA.unpackEncodedString(DFA53_eofS);
    static final char[] DFA53_min = DFA.unpackEncodedStringToUnsignedChars(DFA53_minS);
    static final char[] DFA53_max = DFA.unpackEncodedStringToUnsignedChars(DFA53_maxS);
    static final short[] DFA53_accept = DFA.unpackEncodedString(DFA53_acceptS);
    static final short[] DFA53_special = DFA.unpackEncodedString(DFA53_specialS);
    static final short[][] DFA53_transition;

    static {
        int numStates = DFA53_transitionS.length;
        DFA53_transition = new short[numStates][];
        for (int i = 0; i < numStates; i++) {
            DFA53_transition[i] = DFA.unpackEncodedString(DFA53_transitionS[i]);
        }
    }

    class DFA53 extends DFA
    {

        public DFA53(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 53;
            this.eot = DFA53_eot;
            this.eof = DFA53_eof;
            this.min = DFA53_min;
            this.max = DFA53_max;
            this.accept = DFA53_accept;
            this.special = DFA53_special;
            this.transition = DFA53_transition;
        }

        public String getDescription()
        {
            return "527:9: ( ( arrayDeclarator -> ^( arrayDeclarator $qualifiedIdentExpression) )+ ( DOT CLASS -> ^( DOT $qualifiedIdentExpression CLASS ) ) | arguments -> ^( METHOD_CALL qualifiedIdentifier arguments ) | outerDot= DOT ( CLASS -> ^( DOT qualifiedIdentifier CLASS ) | genericTypeArgumentListSimplified (Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier genericTypeArgumentListSimplified arguments ) | SUPER innerDot= DOT IDENT arguments -> ^( METHOD_CALL ^( $innerDot ^( $outerDot qualifiedIdentifier SUPER ) IDENT ) genericTypeArgumentListSimplified arguments ) | IDENT arguments -> ^( METHOD_CALL ^( DOT qualifiedIdentifier IDENT ) genericTypeArgumentListSimplified arguments ) ) | THIS -> ^( DOT qualifiedIdentifier THIS ) | Super= SUPER arguments -> ^( SUPER_CONSTRUCTOR_CALL[$Super, \"SUPER_CONSTRUCTOR_CALL\"] qualifiedIdentifier arguments ) ) )?";
        }

        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException
        {
            TokenStream input = (TokenStream) _input;
            int _s = s;
            switch (s) {
                case 0:
                    int LA53_1 = input.LA(1);

                    int index53_1 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred102_VNScript())) {
                        s = 45;
                    } else if ((true)) {
                        s = 4;
                    }

                    input.seek(index53_1);
                    if (s >= 0)
                        return s;
                    break;
                case 1:
                    int LA53_3 = input.LA(1);

                    int index53_3 = input.index();
                    input.rewind();
                    s = -1;
                    if ((synpred109_VNScript())) {
                        s = 46;
                    } else if ((true)) {
                        s = 4;
                    }

                    input.seek(index53_3);
                    if (s >= 0)
                        return s;
                    break;
            }
            if (state.backtracking > 0) {
                state.failed = true;
                return -1;
            }
            NoViableAltException nvae
                = new NoViableAltException(getDescription(), 53, _s, input);
            error(nvae);
            throw nvae;
        }
    }

    public static final BitSet FOLLOW_statement_in_program4523 = new BitSet(new long[]{0x128010446AA01002L, 0x000000A892098723L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_LCURLY_in_block4557 = new BitSet(new long[]{0x128014446AA01000L, 0x000000A892098723L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_statement_in_block4559 = new BitSet(new long[]{0x128014446AA01000L, 0x000000A892098723L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_RCURLY_in_block4562 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_block_in_statement4597 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IF_in_statement4604 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_parenthesizedExpression_in_statement4606 = new BitSet(new long[]{0x128010446AA01000L, 0x000000A892098727L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_statement_in_statement4610 = new BitSet(new long[]{0x0000000000000002L, 0x0000000000000004L});
    public static final BitSet FOLLOW_ELSE_in_statement4625 = new BitSet(new long[]{0x128010446AA01000L, 0x000000A892098723L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_statement_in_statement4629 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FOR_in_statement4795 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_LPAREN_in_statement4797 = new BitSet(new long[]{0x128010446AA01000L, 0x000000A892098723L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_forInit_in_statement4811 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_SEMI_in_statement4813 = new BitSet(new long[]{0x128010446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_forCondition_in_statement4815 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_SEMI_in_statement4817 = new BitSet(new long[]{0x128008446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_forUpdater_in_statement4819 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_RPAREN_in_statement4821 = new BitSet(new long[]{0x128010446AA01000L, 0x000000A892098723L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_statement_in_statement4823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WHILE_in_statement4854 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_parenthesizedExpression_in_statement4856 = new BitSet(new long[]{0x128010446AA01000L, 0x000000A892098723L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_statement_in_statement4858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DO_in_statement4907 = new BitSet(new long[]{0x128010446AA01000L, 0x000000A892098723L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_statement_in_statement4909 = new BitSet(new long[]{0x0000000000000000L, 0x0000008000000000L});
    public static final BitSet FOLLOW_WHILE_in_statement4911 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_parenthesizedExpression_in_statement4913 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_SEMI_in_statement4915 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_statement4956 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_SEMI_in_statement4958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SEMI_in_statement4969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declaration_in_statement4981 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_SEMI_in_statement4983 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_declaration5006 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x2000000000000000L});
    public static final BitSet FOLLOW_189_in_declaration5008 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_expression_in_declaration5011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_forInit5027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declaration_in_forInit5059 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_forCondition5151 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expressionList_in_forUpdater5193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAREN_in_parenthesizedExpression5234 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_expression_in_parenthesizedExpression5236 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_RPAREN_in_parenthesizedExpression5238 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_expressionList5272 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_COMMA_in_expressionList5275 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_expression_in_expressionList5278 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_range_offset_in_range5299 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_range_left_in_range5312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_range_single_in_range5325 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_range_right_in_range5338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_range_all_in_range5351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_range_single5367 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_range_offset5377 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_expression_in_range_offset5384 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_range_left5394 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_COLON_in_range_left5396 = new BitSet(new long[]{0x128000446A201002L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_expression_in_range_left5399 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COLON_in_range_right5409 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_expression_in_range_right5412 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COLON_in_range_all5421 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_range_in_rangeList5434 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_COMMA_in_rangeList5437 = new BitSet(new long[]{0x128000446A201400L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_range_in_rangeList5440 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_assignmentExpression_in_expression5459 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_conditionalExpression_in_assignmentExpression5495 = new BitSet(new long[]{0x001540A280004262L});
    public static final BitSet FOLLOW_ASSIGN_in_assignmentExpression5514 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_PLUS_ASSIGN_in_assignmentExpression5533 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_MINUS_ASSIGN_in_assignmentExpression5552 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_STAR_ASSIGN_in_assignmentExpression5571 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_DIV_ASSIGN_in_assignmentExpression5590 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_AND_ASSIGN_in_assignmentExpression5609 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_OR_ASSIGN_in_assignmentExpression5628 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_XOR_ASSIGN_in_assignmentExpression5647 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_MOD_ASSIGN_in_assignmentExpression5666 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_SHIFT_LEFT_ASSIGN_in_assignmentExpression5685 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_SHIFT_RIGHT_ASSIGN_in_assignmentExpression5704 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_BIT_SHIFT_RIGHT_ASSIGN_in_assignmentExpression5723 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_assignmentExpression_in_assignmentExpression5745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_logicalOrExpression_in_conditionalExpression5770 = new BitSet(new long[]{0x0000010000000002L});
    public static final BitSet FOLLOW_QUESTION_in_conditionalExpression5773 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_assignmentExpression_in_conditionalExpression5776 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_COLON_in_conditionalExpression5778 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_conditionalExpression_in_conditionalExpression5781 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_logicalAndExpression_in_logicalOrExpression5802 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_LOGICAL_OR_in_logicalOrExpression5805 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_logicalAndExpression_in_logicalOrExpression5808 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_inclusiveOrExpression_in_logicalAndExpression5829 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_LOGICAL_AND_in_logicalAndExpression5832 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_inclusiveOrExpression_in_logicalAndExpression5835 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression5856 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_OR_in_inclusiveOrExpression5859 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_exclusiveOrExpression_in_inclusiveOrExpression5862 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_andExpression_in_exclusiveOrExpression5883 = new BitSet(new long[]{0x0008000000000002L});
    public static final BitSet FOLLOW_XOR_in_exclusiveOrExpression5886 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_andExpression_in_exclusiveOrExpression5889 = new BitSet(new long[]{0x0008000000000002L});
    public static final BitSet FOLLOW_equalityExpression_in_andExpression5910 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_AND_in_andExpression5913 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_equalityExpression_in_andExpression5916 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_instanceOfExpression_in_equalityExpression5937 = new BitSet(new long[]{0x0000000800040002L});
    public static final BitSet FOLLOW_EQUAL_in_equalityExpression5956 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_NOT_EQUAL_in_equalityExpression5975 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_instanceOfExpression_in_equalityExpression6005 = new BitSet(new long[]{0x0000000800040002L});
    public static final BitSet FOLLOW_IDENT_in_type6028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_relationalExpression_in_instanceOfExpression6042 = new BitSet(new long[]{0x0000000000000002L, 0x0000000000001000L});
    public static final BitSet FOLLOW_INSTANCEOF_in_instanceOfExpression6045 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_type_in_instanceOfExpression6048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_shiftExpression_in_relationalExpression6069 = new BitSet(new long[]{0x0000000003180002L});
    public static final BitSet FOLLOW_LESS_OR_EQUAL_in_relationalExpression6088 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_GREATER_OR_EQUAL_in_relationalExpression6107 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_LESS_THAN_in_relationalExpression6126 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_GREATER_THAN_in_relationalExpression6145 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_shiftExpression_in_relationalExpression6174 = new BitSet(new long[]{0x0000000003180002L});
    public static final BitSet FOLLOW_additiveExpression_in_shiftExpression6208 = new BitSet(new long[]{0x0000A00000000102L});
    public static final BitSet FOLLOW_BIT_SHIFT_RIGHT_in_shiftExpression6226 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_SHIFT_RIGHT_in_shiftExpression6245 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_SHIFT_LEFT_in_shiftExpression6264 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_additiveExpression_in_shiftExpression6293 = new BitSet(new long[]{0x0000A00000000102L});
    public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression6323 = new BitSet(new long[]{0x0000004040000002L});
    public static final BitSet FOLLOW_PLUS_in_additiveExpression6341 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_MINUS_in_additiveExpression6360 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression6389 = new BitSet(new long[]{0x0000004040000002L});
    public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression6419 = new BitSet(new long[]{0x0002000100002002L});
    public static final BitSet FOLLOW_STAR_in_multiplicativeExpression6438 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_DIV_in_multiplicativeExpression6457 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_MOD_in_multiplicativeExpression6476 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression6505 = new BitSet(new long[]{0x0002000100002002L});
    public static final BitSet FOLLOW_PLUS_in_unaryExpression6539 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_unaryExpression_in_unaryExpression6541 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MINUS_in_unaryExpression6568 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_unaryExpression_in_unaryExpression6570 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INC_in_unaryExpression6596 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_postfixedExpression_in_unaryExpression6598 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DEC_in_unaryExpression6622 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_postfixedExpression_in_unaryExpression6624 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_unaryExpressionNotPlusMinus_in_unaryExpression6648 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NOT_in_unaryExpressionNotPlusMinus6667 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus6669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LOGICAL_NOT_in_unaryExpressionNotPlusMinus6716 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus6718 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAREN_in_unaryExpressionNotPlusMinus6757 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_type_in_unaryExpressionNotPlusMinus6759 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_RPAREN_in_unaryExpressionNotPlusMinus6761 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_unaryExpression_in_unaryExpressionNotPlusMinus6763 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_postfixedExpression_in_unaryExpressionNotPlusMinus6798 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_primaryExpression_in_postfixedExpression6844 = new BitSet(new long[]{0x0000000000609002L});
    public static final BitSet FOLLOW_DOT_in_postfixedExpression6906 = new BitSet(new long[]{0x0000000022609002L, 0x0000000090000000L});
    public static final BitSet FOLLOW_genericTypeArgumentListSimplified_in_postfixedExpression6956 = new BitSet(new long[]{0x0000000020609002L});
    public static final BitSet FOLLOW_arguments_in_postfixedExpression7116 = new BitSet(new long[]{0x0000000000609002L});
    public static final BitSet FOLLOW_THIS_in_postfixedExpression7190 = new BitSet(new long[]{0x0000000000609002L});
    public static final BitSet FOLLOW_SUPER_in_postfixedExpression7253 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_postfixedExpression7255 = new BitSet(new long[]{0x0000000000609002L});
    public static final BitSet FOLLOW_SUPER_in_postfixedExpression7308 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_DOT_in_postfixedExpression7312 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_IDENT_in_postfixedExpression7314 = new BitSet(new long[]{0x0000000020609002L});
    public static final BitSet FOLLOW_arguments_in_postfixedExpression7381 = new BitSet(new long[]{0x0000000000609002L});
    public static final BitSet FOLLOW_LBRACK_in_postfixedExpression7461 = new BitSet(new long[]{0x128000446A201400L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_rangeList_in_postfixedExpression7463 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_RBRACK_in_postfixedExpression7465 = new BitSet(new long[]{0x0000000000609002L});
    public static final BitSet FOLLOW_DOT_in_postfixedExpression7468 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_IDENT_in_postfixedExpression7470 = new BitSet(new long[]{0x0000000000609002L});
    public static final BitSet FOLLOW_INC_in_postfixedExpression7537 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DEC_in_postfixedExpression7561 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LBRACK_in_arrayDeclarator7605 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_RBRACK_in_arrayDeclarator7607 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_primitiveType0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parenthesizedExpression_in_primaryExpression7734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_literal_in_primaryExpression7744 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_qualifiedIdentExpression_in_primaryExpression7754 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_genericTypeArgumentListSimplified_in_primaryExpression7764 = new BitSet(new long[]{0x0000000000000000L, 0x0000000090000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_SUPER_in_primaryExpression7779 = new BitSet(new long[]{0x0000000020008000L});
    public static final BitSet FOLLOW_arguments_in_primaryExpression7797 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DOT_in_primaryExpression7857 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_IDENT_in_primaryExpression7859 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_primaryExpression7861 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_primaryExpression7928 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_primaryExpression7930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_THIS_in_primaryExpression7985 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_primaryExpression7987 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_THIS_in_primaryExpression8052 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_arguments_in_primaryExpression8120 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SUPER_in_primaryExpression8185 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_primaryExpression8187 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SUPER_in_primaryExpression8243 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_DOT_in_primaryExpression8245 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_IDENT_in_primaryExpression8247 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_arguments_in_primaryExpression8271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_primitiveType_in_primaryExpression8413 = new BitSet(new long[]{0x0000000000408000L});
    public static final BitSet FOLLOW_arrayDeclarator_in_primaryExpression8472 = new BitSet(new long[]{0x0000000000408000L});
    public static final BitSet FOLLOW_DOT_in_primaryExpression8535 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_CLASS_in_primaryExpression8537 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VOID_in_primaryExpression8597 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_DOT_in_primaryExpression8599 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_CLASS_in_primaryExpression8601 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_qualifiedIdentifier8676 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_DOT_in_qualifiedIdentifier8719 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_IDENT_in_qualifiedIdentifier8723 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_GREATER_THAN_in_genericTypeListClosing8847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SHIFT_RIGHT_in_genericTypeListClosing8857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BIT_SHIFT_RIGHT_in_genericTypeListClosing8867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LESS_THAN_in_genericTypeArgumentListSimplified8895 = new BitSet(new long[]{0x0000010000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_genericTypeArgumentSimplified_in_genericTypeArgumentListSimplified8897 = new BitSet(new long[]{0x0000800000100900L});
    public static final BitSet FOLLOW_COMMA_in_genericTypeArgumentListSimplified8900 = new BitSet(new long[]{0x0000010000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_genericTypeArgumentSimplified_in_genericTypeArgumentListSimplified8902 = new BitSet(new long[]{0x0000800000100900L});
    public static final BitSet FOLLOW_genericTypeListClosing_in_genericTypeArgumentListSimplified8906 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_type_in_genericTypeArgumentSimplified8948 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUESTION_in_genericTypeArgumentSimplified8958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_qualifiedIdentifier_in_qualifiedIdentExpression8995 = new BitSet(new long[]{0x0000000020408002L});
    public static final BitSet FOLLOW_arrayDeclarator_in_qualifiedIdentExpression9065 = new BitSet(new long[]{0x0000000000408000L});
    public static final BitSet FOLLOW_DOT_in_qualifiedIdentExpression9133 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_CLASS_in_qualifiedIdentExpression9135 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arguments_in_qualifiedIdentExpression9205 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DOT_in_qualifiedIdentExpression9266 = new BitSet(new long[]{0x2000000002000000L, 0x0000000090000000L});
    public static final BitSet FOLLOW_CLASS_in_qualifiedIdentExpression9284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_genericTypeArgumentListSimplified_in_qualifiedIdentExpression9347 = new BitSet(new long[]{0x0000000000000000L, 0x0000000010000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_SUPER_in_qualifiedIdentExpression9372 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_qualifiedIdentExpression9374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SUPER_in_qualifiedIdentExpression9424 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_DOT_in_qualifiedIdentExpression9428 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_IDENT_in_qualifiedIdentExpression9430 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_qualifiedIdentExpression9432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_qualifiedIdentExpression9482 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_qualifiedIdentExpression9484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_THIS_in_qualifiedIdentExpression9559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SUPER_in_qualifiedIdentExpression9625 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_qualifiedIdentExpression9627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAREN_in_arguments9702 = new BitSet(new long[]{0x128008446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_expressionList_in_arguments9704 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_RPAREN_in_arguments9707 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_literal0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ELSE_in_synpred4_VNScript4625 = new BitSet(new long[]{0x128010446AA01000L, 0x000000A892098723L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_statement_in_synpred4_VNScript4629 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_range_offset_in_synpred16_VNScript5299 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_range_left_in_synpred17_VNScript5312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_range_single_in_synpred18_VNScript5325 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_range_right_in_synpred19_VNScript5338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAREN_in_synpred62_VNScript6757 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_type_in_synpred62_VNScript6759 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_RPAREN_in_synpred62_VNScript6761 = new BitSet(new long[]{0x128000446A201000L, 0x0000002892098122L, 0x0001FC0000000000L});
    public static final BitSet FOLLOW_unaryExpression_in_synpred62_VNScript6763 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_genericTypeArgumentListSimplified_in_synpred63_VNScript6956 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_GREATER_THAN_in_synpred96_VNScript8847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SHIFT_RIGHT_in_synpred97_VNScript8857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BIT_SHIFT_RIGHT_in_synpred98_VNScript8867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COMMA_in_synpred99_VNScript8900 = new BitSet(new long[]{0x0000010000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_genericTypeArgumentSimplified_in_synpred99_VNScript8902 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arrayDeclarator_in_synpred102_VNScript9065 = new BitSet(new long[]{0x0000000000408000L});
    public static final BitSet FOLLOW_DOT_in_synpred102_VNScript9133 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_CLASS_in_synpred102_VNScript9135 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DOT_in_synpred109_VNScript9266 = new BitSet(new long[]{0x2000000002000000L, 0x0000000090000000L});
    public static final BitSet FOLLOW_CLASS_in_synpred109_VNScript9284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_genericTypeArgumentListSimplified_in_synpred109_VNScript9347 = new BitSet(new long[]{0x0000000000000000L, 0x0000000010000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_SUPER_in_synpred109_VNScript9372 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_synpred109_VNScript9374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SUPER_in_synpred109_VNScript9424 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_DOT_in_synpred109_VNScript9428 = new BitSet(new long[]{0x0000000000000000L, 0x0000000000000000L, 0x0000040000000000L});
    public static final BitSet FOLLOW_IDENT_in_synpred109_VNScript9430 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_synpred109_VNScript9432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_synpred109_VNScript9482 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_synpred109_VNScript9484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_THIS_in_synpred109_VNScript9559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SUPER_in_synpred109_VNScript9625 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_arguments_in_synpred109_VNScript9627 = new BitSet(new long[]{0x0000000000000002L});

}
