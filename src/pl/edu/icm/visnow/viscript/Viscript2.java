/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript;

import de.hunsicker.jalopy.Jalopy;
import pl.edu.icm.visnow.viscript.types.VNScriptContext;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringWriter;
import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.antlr.runtime.tree.TreeAdaptor;
import org.antlr.runtime.tree.TreeWizard;
import pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets.LogMessage;
import pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets.LogMessage.LogMessageType;
import pl.edu.icm.visnow.lib.basic.utilities.VNScript.widgets.VNScriptLog;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Viscript2
{

    private String javaSource;
    private static final String javaClassTemplate
        = "import java.util.HashMap;\n" +
        "import java.util.Map;\n" +
        "import pl.edu.icm.visnow.viscript.ViscriptStandardMethods;" +
        "import pl.edu.icm.jscic.dataarrays.DataArray;\n" +
        "%s\n\n" +
        "public class %s extends  pl.edu.icm.visnow.viscript.CompiledViscript {\n\n" +
        "@Override\n" +
        "public Map<String, Object> run(Map<String, Object> bindings) {\n" +
        "Map<String, Object> outputBindings = new HashMap<String, Object>();\n\n" +
        "%s\n\n" +
        "return outputBindings;\n" +
        "}\n\n}\n";
    private final String script;
    private final String className;
    private VNScriptContext context;
    private VNScriptLog log;

    public VNScriptLog getLog()
    {
        return log;
    }

    public void setLog(VNScriptLog log)
    {
        this.log = log;
    }

    public Viscript2(String script, String className, VNScriptContext context)
    {
        this.script = script;
        this.className = className;
        this.context = context;
    }

    public void parse() throws IOException, RecognitionException, Exception
    {

        // lexer + parser
        byte[] currentXMLBytes = script.getBytes();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(currentXMLBytes);
        ANTLRInputStream input = new ANTLRInputStream(byteArrayInputStream);
        VNScriptLexer lexer = new VNScriptLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        VNScriptParser parser = new VNScriptParser(tokens);
        parser.setLog(log);
        CommonTree tree = (CommonTree) parser.program().tree;

        if (!log.isError()) {
            // ast transformations
            TreeAdaptor adaptor = new CommonTreeAdaptor();
            TreeWizard wiz = new TreeWizard(adaptor, parser.getTokenNames());
            //            ASTTransforms.pushUpMethodCalls(tree, wiz, context);
            //            ASTTransforms.pushUpArrayExpressionsInMethodArguments(tree, wiz);

            // output java sourcecode
            CommonTreeNodeStream nodes = new CommonTreeNodeStream(tree);
            VNScriptTree treeParser = new VNScriptTree(nodes);
            treeParser.setLog(log);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(output);
            treeParser.setContext(context);
            treeParser.setPrintStream(ps);
            treeParser.program();
            if (!log.isError()) {
                String javaKernel = "";

                // input variables;
                javaKernel += context.createInputBidings();
                javaKernel += output.toString();
                javaKernel += context.createOutputBidings();

                javaSource = String.format(javaClassTemplate, "", className, javaKernel);

                try {
                    Jalopy jalopy = new Jalopy();
                    StringWriter w = new StringWriter();

                    // specify input and output target
                    jalopy.setInput(javaSource, "pl/edu/icm/visnow/viscript");
                    jalopy.setOutput(w);

                    // format and overwrite the given input file
                    jalopy.format();

                    if (jalopy.getState() == Jalopy.State.OK) {
                        log.addMessage(new LogMessage("java source formatted successfully", LogMessageType.INFO));
                        javaSource = w.getBuffer().toString();
                    } else if (jalopy.getState() == Jalopy.State.WARN) {
                        log.addMessage(new LogMessage("java source formatted with warnings", LogMessageType.WARNING));
                        javaSource = w.getBuffer().toString();
                    } else if (jalopy.getState() == Jalopy.State.ERROR) {
                        log.addMessage(new LogMessage("java source could not be formatted", LogMessageType.WARNING));
                    } else {
                        log.addMessage(new LogMessage("java source formatted successfully", LogMessageType.INFO));
                        javaSource = w.getBuffer().toString();
                    }
                } catch (Exception e) {
                    log.addMessage(new LogMessage("java source could not be formatted", LogMessageType.WARNING));
                }

            }
        }
    }

    public String getJavaSource()
    {
        return javaSource;
    }
}
