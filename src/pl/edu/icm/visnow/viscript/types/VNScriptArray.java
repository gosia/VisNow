/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript.types;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class VNScriptArray extends VNScriptExpr
{

    public final String ident;
    public final int veclen;
    public final int vec;
    public final int dimensions;

    public VNScriptArray(String ident, int veclen, int dimensions, String vec)
    {
        this.ident = ident;
        this.veclen = veclen;
        this.dimensions = dimensions;
        if (vec == null) {
            this.vec = 0;
        } else if (vec.equals("x")) {
            this.vec = 0;
        } else if (vec.equals("y")) {
            this.vec = 1;
        } else if (vec.equals("z")) {
            this.vec = 2;
        } else if (vec.equals("w")) {
            this.vec = 3;
        } else {
            this.vec = 0;
        }
    }

    public VNScriptArray(String ident, int veclen, int dimensions)
    {
        this.ident = ident;
        this.veclen = veclen;
        this.dimensions = dimensions;
        this.vec = 0;
    }

    public String createLoopsInit()
    {
        String result = "";

        for (int i = 0; i < dimensions; i++) {
            String loopTempVariableName = VNScriptUtil.getLoopTempVariableName(i);
            String rightRange = rightRange = String.format("_" + ident + "_dims_" + i);

            result += String.format("for ( int %s = 0; %s < (%s); %s++) {\n", loopTempVariableName, loopTempVariableName, rightRange, loopTempVariableName);
        }
        return result;
    }

    public String createLoopsEnd()
    {
        String result = "";
        for (int i = 0; i < dimensions; i++) {
            result += "}\n";
        }
        return result;
    }

    public String createLoopOffsetVariable()
    {
        String offset = "";
        for (int i = 0; i < dimensions; i++) {
            String loopTempVariableName = VNScriptUtil.getLoopTempVariableName(i);
            if (i > 0) {
                offset += " + ";
            }

            offset += String.format("(%s)", loopTempVariableName);

            for (int j = 0; j < i; j++) {
                offset += String.format(" * (_" + ident + "_dims_" + j + ")");
            }
        }
        return offset;
    }

    @Override
    public String toString()
    {
        return toString(vec);
    }

    @Override
    public String toString(int vec)
    {
        String offset = createLoopOffsetVariable().replaceAll("\\$ident\\$", ident);

        if (veclen > 1) {
            return ident + "[ (int) (" + veclen + " * (" + offset + ") + " + vec + ")]";
        } else {
            return ident + "[ (int) (" + offset + ")]";
        }
    }

    @Override
    public String toString(boolean asIdent)
    {
        return ident;
    }

    @Override
    public VNScriptType getType()
    {
        return VNScriptType.ARRAY;
    }
}
