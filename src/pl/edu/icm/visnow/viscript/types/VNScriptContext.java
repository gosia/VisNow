/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript.types;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class VNScriptContext
{

    private int tempArrayVariableCount = 0;
    private int tempScalarVariableCount = 0;
    private final VNScriptContext parent;
    private final Map<String, VNScriptVariable> variables;

    public VNScriptContext(VNScriptContext parent)
    {
        this.parent = parent;
        this.variables = new HashMap<String, VNScriptVariable>();
    }

    public VNScriptContext getParent()
    {
        return parent;
    }

    public boolean variableExists(String s)
    {
        return variables.containsKey(s);
    }

    public VNScriptVariable getVariable(String s)
    {
        if (this.variables.containsKey(s)) {
            return this.variables.get(s);
        } else {
            return null;
        }
    }

    public void putVariable(String s, VNScriptVariable v)
    {
        this.variables.put(s, v);
    }

    public String createVariable(String variableName, VNScriptType type)
    {
        putVariable(variableName, new VNScriptVariable(type, 1));
        return variableName;
    }

    public String createVariable(VNScriptType type)
    {
        String variableName = "";
        switch (type) {
            case ARRAY:
                variableName = "tempArray" + tempArrayVariableCount++;
                break;
            case SCALAR:
                variableName = "tempScalar" + tempScalarVariableCount++;
                break;
        }
        return createVariable(variableName, type);
    }

    public String createInputBidings()
    {
        String r = "";

        for (Entry<String, VNScriptVariable> e : variables.entrySet()) {
            if (e.getValue().isInput() && e.getValue().isUsed()) {
                switch (e.getValue().type) {
                    case ARRAY:
                        r += String.format("float[] %s = ((DataArray) bindings.get(\"%s\")).getFData();\n", e.getKey(), e.getKey());
                        for (int d = 0; d < e.getValue().dimensions; d++) {
                            r += String.format("int _%s_dims_%d = ((int[]) bindings.get(\"%s_dims\"))[%d];\n", e.getKey(), d, e.getKey(), d);
                        }
                        break;
                    case SCALAR:
                        r += String.format("int %s = bindings.get(\"%s\");\n", e.getKey(), e.getKey());
                        break;
                }
            }
        }

        return r + "\n\n";
    }

    public String createOutputBidings()
    {
        String r = "";

        for (Entry<String, VNScriptVariable> e : variables.entrySet()) {
            if (e.getValue().isOutput()) {
                switch (e.getValue().type) {
                    case ARRAY:
                        r += String.format("outputBindings.put(\"%s\", %s);\n", e.getKey(), e.getKey());
                        String dims = "";
                        for (int d = 0; d < e.getValue().dimensions; d++) {
                            if (d != 0) {
                                dims += ", ";
                            }
                            dims += "_" + e.getKey() + "_dims_" + d;
                        }
                        r += String.format("outputBindings.put(\"_dims\", new int[] {%s});\n", dims);
                        break;
                    case SCALAR:
                        r += String.format("outputBindings.put(\"%s\", %s);\n", e.getKey(), e.getKey());
                        break;
                }
            }
        }

        return r + "\n\n";
    }
}
