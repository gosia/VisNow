/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript.types;

import java.util.ArrayList;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class VNScriptRanges
{

    public final ArrayList<VNScriptRange> ranges;

    public VNScriptRanges()
    {
        this.ranges = new ArrayList<VNScriptRange>();
    }

    public boolean isSingle()
    {
        boolean result = true;
        for (VNScriptRange r : ranges) {
            result = result && r.isSingle();
        }
        return result;
    }

    public String createLoopOffsetVariable()
    {
        String offset = "";
        for (int i = 0; i < ranges.size(); i++) {
            VNScriptRange sr = ranges.get(i);
            String loopTempVariableName = VNScriptUtil.getLoopTempVariableName(i);
            if (i > 0) {
                offset += " + ";
            }

            if (sr.type == VNScriptRangeType.LEFT || sr.type == VNScriptRangeType.BOTH || sr.type == VNScriptRangeType.OFFSET) {
                offset += String.format("(%s + %s)", loopTempVariableName, sr.getLeftRangeLimit().replaceAll("\\$dim\\$", Integer.toString(i)));
            } else if (sr.type == VNScriptRangeType.SINGLE) {
                offset += String.format("(%s)", sr.getLeftRangeLimit().replaceAll("\\$dim\\$", Integer.toString(i)));
            } else {
                offset += String.format("(%s)", loopTempVariableName);
            }
            for (int j = 0; j < i; j++) {
                offset += String.format(" * (_$ident$_dims_" + j + ")");
            }
        }

        return offset;

    }

    public String createRangesLoopsInit()
    {
        String result = "";

        int count = 0;

        for (int i = 0; i <
            ranges.size(); i++) {
            VNScriptRange sr = ranges.get(i);
            String loopTempVariableName = VNScriptUtil.getLoopTempVariableName(i);

            if (sr.isSingle()) {
                result += String.format("int %s = 0;\n", loopTempVariableName);

            } else {
                // TODO: dims
                String rightRange = sr.getRightRangeLimit().replaceAll("\\$dim\\$", Integer.toString(i));

                result += String.format("for ( int %s = 0; %s < (%s); %s++) {\n", loopTempVariableName, loopTempVariableName, rightRange, loopTempVariableName);
                count++;

            }

        }
        return result;

    }

    public String createRangesLoopsEnd()
    {
        String result = "";

        int count = 0;

        for (int i = 0; i <
            ranges.size(); i++) {
            VNScriptRange sr = ranges.get(i);

            if (!sr.isSingle()) {
                count++;

            }
        }

        for (int i = 0; i <
            count; i++) {
            result += "}\n";

        }

        return result;

    }
}
