/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript;

// $ANTLR 3.2 Sep 23, 2009 12:02:23 /home/staff/lyczek/viscript.g 2010-02-22 14:46:43
import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class viscriptLexer extends Lexer
{

    public static final int T__29 = 29;
    public static final int T__28 = 28;
    public static final int T__27 = 27;
    public static final int T__26 = 26;
    public static final int T__25 = 25;
    public static final int T__24 = 24;
    public static final int ELSE_OP = 7;
    public static final int ADD_OP = 19;
    public static final int TAB = 15;
    public static final int RANGE = 8;
    public static final int INT = 22;
    public static final int IF_OP = 6;
    public static final int ID = 21;
    public static final int EOF = -1;
    public static final int ASSIGN_OP = 17;
    public static final int T__30 = 30;
    public static final int T__31 = 31;
    public static final int T__32 = 32;
    public static final int PROG = 16;
    public static final int T__33 = 33;
    public static final int JUMP_RANGE = 11;
    public static final int WS = 23;
    public static final int END_RANGE = 10;
    public static final int TAB_PARAMS = 12;
    public static final int FOR_OP = 5;
    public static final int CALL = 13;
    public static final int LOG_OP = 18;
    public static final int WHILE_OP = 4;
    public static final int COMPONENT = 14;
    public static final int BEGIN_RANGE = 9;
    public static final int MUL_OP = 20;

    // delegates
    // delegators
    public viscriptLexer()
    {
        ;
    }

    public viscriptLexer(CharStream input)
    {
        this(input, new RecognizerSharedState());
    }

    public viscriptLexer(CharStream input, RecognizerSharedState state)
    {
        super(input, state);

    }

    public String getGrammarFileName()
    {
        return "/home/staff/lyczek/viscript.g";
    }

    // $ANTLR start "WHILE_OP"
    public final void mWHILE_OP() throws RecognitionException
    {
        try {
            int _type = WHILE_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:3:10: ( 'while' )
            // /home/staff/lyczek/viscript.g:3:12: 'while'
            {
                match("while");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "WHILE_OP"
    // $ANTLR start "FOR_OP"
    public final void mFOR_OP() throws RecognitionException
    {
        try {
            int _type = FOR_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:4:8: ( 'for' )
            // /home/staff/lyczek/viscript.g:4:10: 'for'
            {
                match("for");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "FOR_OP"
    // $ANTLR start "IF_OP"
    public final void mIF_OP() throws RecognitionException
    {
        try {
            int _type = IF_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:5:7: ( 'if' )
            // /home/staff/lyczek/viscript.g:5:9: 'if'
            {
                match("if");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "IF_OP"
    // $ANTLR start "ELSE_OP"
    public final void mELSE_OP() throws RecognitionException
    {
        try {
            int _type = ELSE_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:6:9: ( 'else' )
            // /home/staff/lyczek/viscript.g:6:11: 'else'
            {
                match("else");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "ELSE_OP"
    // $ANTLR start "RANGE"
    public final void mRANGE() throws RecognitionException
    {
        try {
            int _type = RANGE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:7:7: ( 'range' )
            // /home/staff/lyczek/viscript.g:7:9: 'range'
            {
                match("range");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "RANGE"
    // $ANTLR start "BEGIN_RANGE"
    public final void mBEGIN_RANGE() throws RecognitionException
    {
        try {
            int _type = BEGIN_RANGE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:8:13: ( 'begin_range' )
            // /home/staff/lyczek/viscript.g:8:15: 'begin_range'
            {
                match("begin_range");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "BEGIN_RANGE"
    // $ANTLR start "END_RANGE"
    public final void mEND_RANGE() throws RecognitionException
    {
        try {
            int _type = END_RANGE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:9:11: ( 'end_range' )
            // /home/staff/lyczek/viscript.g:9:13: 'end_range'
            {
                match("end_range");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "END_RANGE"
    // $ANTLR start "JUMP_RANGE"
    public final void mJUMP_RANGE() throws RecognitionException
    {
        try {
            int _type = JUMP_RANGE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:10:12: ( 'jump_range' )
            // /home/staff/lyczek/viscript.g:10:14: 'jump_range'
            {
                match("jump_range");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "JUMP_RANGE"
    // $ANTLR start "TAB_PARAMS"
    public final void mTAB_PARAMS() throws RecognitionException
    {
        try {
            int _type = TAB_PARAMS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:11:12: ( 'tab_params' )
            // /home/staff/lyczek/viscript.g:11:14: 'tab_params'
            {
                match("tab_params");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "TAB_PARAMS"
    // $ANTLR start "CALL"
    public final void mCALL() throws RecognitionException
    {
        try {
            int _type = CALL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:12:6: ( 'call' )
            // /home/staff/lyczek/viscript.g:12:8: 'call'
            {
                match("call");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "CALL"
    // $ANTLR start "COMPONENT"
    public final void mCOMPONENT() throws RecognitionException
    {
        try {
            int _type = COMPONENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:13:11: ( 'component' )
            // /home/staff/lyczek/viscript.g:13:13: 'component'
            {
                match("component");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "COMPONENT"
    // $ANTLR start "TAB"
    public final void mTAB() throws RecognitionException
    {
        try {
            int _type = TAB;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:14:5: ( 'tab' )
            // /home/staff/lyczek/viscript.g:14:7: 'tab'
            {
                match("tab");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "TAB"
    // $ANTLR start "PROG"
    public final void mPROG() throws RecognitionException
    {
        try {
            int _type = PROG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:15:6: ( 'prog' )
            // /home/staff/lyczek/viscript.g:15:8: 'prog'
            {
                match("prog");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "PROG"
    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException
    {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:16:7: ( ';' )
            // /home/staff/lyczek/viscript.g:16:9: ';'
            {
                match(';');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "T__24"
    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException
    {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:17:7: ( '(' )
            // /home/staff/lyczek/viscript.g:17:9: '('
            {
                match('(');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "T__25"
    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException
    {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:18:7: ( ')' )
            // /home/staff/lyczek/viscript.g:18:9: ')'
            {
                match(')');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "T__26"
    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException
    {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:19:7: ( '{' )
            // /home/staff/lyczek/viscript.g:19:9: '{'
            {
                match('{');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "T__27"
    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException
    {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:20:7: ( '}' )
            // /home/staff/lyczek/viscript.g:20:9: '}'
            {
                match('}');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "T__28"
    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException
    {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:21:7: ( ':' )
            // /home/staff/lyczek/viscript.g:21:9: ':'
            {
                match(':');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "T__29"
    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException
    {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:22:7: ( ',' )
            // /home/staff/lyczek/viscript.g:22:9: ','
            {
                match(',');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "T__30"
    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException
    {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:23:7: ( '.' )
            // /home/staff/lyczek/viscript.g:23:9: '.'
            {
                match('.');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "T__31"
    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException
    {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:24:7: ( '[' )
            // /home/staff/lyczek/viscript.g:24:9: '['
            {
                match('[');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "T__32"
    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException
    {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:25:7: ( ']' )
            // /home/staff/lyczek/viscript.g:25:9: ']'
            {
                match(']');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "T__33"
    // $ANTLR start "LOG_OP"
    public final void mLOG_OP() throws RecognitionException
    {
        try {
            int _type = LOG_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:66:8: ( ( '>' | '>=' | '<=' | '<' | '==' | '!=' | '&&' | '||' ) )
            // /home/staff/lyczek/viscript.g:66:10: ( '>' | '>=' | '<=' | '<' | '==' | '!=' | '&&' | '||' )
            {
                // /home/staff/lyczek/viscript.g:66:10: ( '>' | '>=' | '<=' | '<' | '==' | '!=' | '&&' | '||' )
                int alt1 = 8;
                alt1 = dfa1.predict(input);
                switch (alt1) {
                    case 1: // /home/staff/lyczek/viscript.g:66:11: '>'
                    {
                        match('>');

                    }
                    break;
                    case 2: // /home/staff/lyczek/viscript.g:66:15: '>='
                    {
                        match(">=");

                    }
                    break;
                    case 3: // /home/staff/lyczek/viscript.g:66:20: '<='
                    {
                        match("<=");

                    }
                    break;
                    case 4: // /home/staff/lyczek/viscript.g:66:25: '<'
                    {
                        match('<');

                    }
                    break;
                    case 5: // /home/staff/lyczek/viscript.g:66:29: '=='
                    {
                        match("==");

                    }
                    break;
                    case 6: // /home/staff/lyczek/viscript.g:66:34: '!='
                    {
                        match("!=");

                    }
                    break;
                    case 7: // /home/staff/lyczek/viscript.g:66:39: '&&'
                    {
                        match("&&");

                    }
                    break;
                    case 8: // /home/staff/lyczek/viscript.g:66:44: '||'
                    {
                        match("||");

                    }
                    break;

                }

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "LOG_OP"
    // $ANTLR start "ADD_OP"
    public final void mADD_OP() throws RecognitionException
    {
        try {
            int _type = ADD_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:67:8: ( ( '+' | '-' ) )
            // /home/staff/lyczek/viscript.g:67:10: ( '+' | '-' )
            {
                if (input.LA(1) == '+' || input.LA(1) == '-') {
                    input.consume();

                } else {
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    recover(mse);
                    throw mse;
                }

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "ADD_OP"
    // $ANTLR start "MUL_OP"
    public final void mMUL_OP() throws RecognitionException
    {
        try {
            int _type = MUL_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:68:8: ( ( '*' | '/' | '%' ) )
            // /home/staff/lyczek/viscript.g:68:10: ( '*' | '/' | '%' )
            {
                if (input.LA(1) == '%' || input.LA(1) == '*' || input.LA(1) == '/') {
                    input.consume();

                } else {
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    recover(mse);
                    throw mse;
                }

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "MUL_OP"
    // $ANTLR start "ASSIGN_OP"
    public final void mASSIGN_OP() throws RecognitionException
    {
        try {
            int _type = ASSIGN_OP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:69:11: ( ( '=' | '+=' | '-=' | '%=' | '*=' | '/=' ) )
            // /home/staff/lyczek/viscript.g:69:13: ( '=' | '+=' | '-=' | '%=' | '*=' | '/=' )
            {
                // /home/staff/lyczek/viscript.g:69:13: ( '=' | '+=' | '-=' | '%=' | '*=' | '/=' )
                int alt2 = 6;
                switch (input.LA(1)) {
                    case '=': {
                        alt2 = 1;
                    }
                    break;
                    case '+': {
                        alt2 = 2;
                    }
                    break;
                    case '-': {
                        alt2 = 3;
                    }
                    break;
                    case '%': {
                        alt2 = 4;
                    }
                    break;
                    case '*': {
                        alt2 = 5;
                    }
                    break;
                    case '/': {
                        alt2 = 6;
                    }
                    break;
                    default:
                        NoViableAltException nvae
                            = new NoViableAltException("", 2, 0, input);

                        throw nvae;
                }

                switch (alt2) {
                    case 1: // /home/staff/lyczek/viscript.g:69:14: '='
                    {
                        match('=');

                    }
                    break;
                    case 2: // /home/staff/lyczek/viscript.g:69:18: '+='
                    {
                        match("+=");

                    }
                    break;
                    case 3: // /home/staff/lyczek/viscript.g:69:23: '-='
                    {
                        match("-=");

                    }
                    break;
                    case 4: // /home/staff/lyczek/viscript.g:69:28: '%='
                    {
                        match("%=");

                    }
                    break;
                    case 5: // /home/staff/lyczek/viscript.g:69:33: '*='
                    {
                        match("*=");

                    }
                    break;
                    case 6: // /home/staff/lyczek/viscript.g:69:38: '/='
                    {
                        match("/=");

                    }
                    break;

                }

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "ASSIGN_OP"
    // $ANTLR start "ID"
    public final void mID() throws RecognitionException
    {
        try {
            int _type = ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:71:4: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' )+ ( '0' .. '9' )* )
            // /home/staff/lyczek/viscript.g:71:6: ( 'a' .. 'z' | 'A' .. 'Z' | '_' )+ ( '0' .. '9' )*
            {
                // /home/staff/lyczek/viscript.g:71:6: ( 'a' .. 'z' | 'A' .. 'Z' | '_' )+
                int cnt3 = 0;
                loop3:
                do {
                    int alt3 = 2;
                    int LA3_0 = input.LA(1);

                    if (((LA3_0 >= 'A' && LA3_0 <= 'Z') || LA3_0 == '_' || (LA3_0 >= 'a' && LA3_0 <= 'z'))) {
                        alt3 = 1;
                    }

                    switch (alt3) {
                        case 1: // /home/staff/lyczek/viscript.g:
                        {
                            if ((input.LA(1) >= 'A' && input.LA(1) <= 'Z') || input.LA(1) == '_' || (input.LA(1) >= 'a' && input.LA(1) <= 'z')) {
                                input.consume();

                            } else {
                                MismatchedSetException mse = new MismatchedSetException(null, input);
                                recover(mse);
                                throw mse;
                            }

                        }
                        break;

                        default:
                            if (cnt3 >= 1)
                                break loop3;
                            EarlyExitException eee
                                = new EarlyExitException(3, input);
                            throw eee;
                    }
                    cnt3++;
                } while (true);

                // /home/staff/lyczek/viscript.g:71:31: ( '0' .. '9' )*
                loop4:
                do {
                    int alt4 = 2;
                    int LA4_0 = input.LA(1);

                    if (((LA4_0 >= '0' && LA4_0 <= '9'))) {
                        alt4 = 1;
                    }

                    switch (alt4) {
                        case 1: // /home/staff/lyczek/viscript.g:71:32: '0' .. '9'
                        {
                            matchRange('0', '9');

                        }
                        break;

                        default:
                            break loop4;
                    }
                } while (true);

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "ID"
    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException
    {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:73:5: ( ( '0' .. '9' )+ )
            // /home/staff/lyczek/viscript.g:73:7: ( '0' .. '9' )+
            {
                // /home/staff/lyczek/viscript.g:73:7: ( '0' .. '9' )+
                int cnt5 = 0;
                loop5:
                do {
                    int alt5 = 2;
                    int LA5_0 = input.LA(1);

                    if (((LA5_0 >= '0' && LA5_0 <= '9'))) {
                        alt5 = 1;
                    }

                    switch (alt5) {
                        case 1: // /home/staff/lyczek/viscript.g:73:8: '0' .. '9'
                        {
                            matchRange('0', '9');

                        }
                        break;

                        default:
                            if (cnt5 >= 1)
                                break loop5;
                            EarlyExitException eee
                                = new EarlyExitException(5, input);
                            throw eee;
                    }
                    cnt5++;
                } while (true);

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "INT"
    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException
    {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/staff/lyczek/viscript.g:75:5: ( ( ' ' | '\\t' | '\\n' )+ )
            // /home/staff/lyczek/viscript.g:75:7: ( ' ' | '\\t' | '\\n' )+
            {
                // /home/staff/lyczek/viscript.g:75:7: ( ' ' | '\\t' | '\\n' )+
                int cnt6 = 0;
                loop6:
                do {
                    int alt6 = 2;
                    int LA6_0 = input.LA(1);

                    if (((LA6_0 >= '\t' && LA6_0 <= '\n') || LA6_0 == ' ')) {
                        alt6 = 1;
                    }

                    switch (alt6) {
                        case 1: // /home/staff/lyczek/viscript.g:
                        {
                            if ((input.LA(1) >= '\t' && input.LA(1) <= '\n') || input.LA(1) == ' ') {
                                input.consume();

                            } else {
                                MismatchedSetException mse = new MismatchedSetException(null, input);
                                recover(mse);
                                throw mse;
                            }

                        }
                        break;

                        default:
                            if (cnt6 >= 1)
                                break loop6;
                            EarlyExitException eee
                                = new EarlyExitException(6, input);
                            throw eee;
                    }
                    cnt6++;
                } while (true);

                skip();

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }

    // $ANTLR end "WS"
    public void mTokens() throws RecognitionException
    {
        // /home/staff/lyczek/viscript.g:1:8: ( WHILE_OP | FOR_OP | IF_OP | ELSE_OP | RANGE | BEGIN_RANGE | END_RANGE | JUMP_RANGE | TAB_PARAMS | CALL | COMPONENT | TAB | PROG | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | LOG_OP | ADD_OP | MUL_OP | ASSIGN_OP | ID | INT | WS )
        int alt7 = 30;
        alt7 = dfa7.predict(input);
        switch (alt7) {
            case 1: // /home/staff/lyczek/viscript.g:1:10: WHILE_OP
            {
                mWHILE_OP();

            }
            break;
            case 2: // /home/staff/lyczek/viscript.g:1:19: FOR_OP
            {
                mFOR_OP();

            }
            break;
            case 3: // /home/staff/lyczek/viscript.g:1:26: IF_OP
            {
                mIF_OP();

            }
            break;
            case 4: // /home/staff/lyczek/viscript.g:1:32: ELSE_OP
            {
                mELSE_OP();

            }
            break;
            case 5: // /home/staff/lyczek/viscript.g:1:40: RANGE
            {
                mRANGE();

            }
            break;
            case 6: // /home/staff/lyczek/viscript.g:1:46: BEGIN_RANGE
            {
                mBEGIN_RANGE();

            }
            break;
            case 7: // /home/staff/lyczek/viscript.g:1:58: END_RANGE
            {
                mEND_RANGE();

            }
            break;
            case 8: // /home/staff/lyczek/viscript.g:1:68: JUMP_RANGE
            {
                mJUMP_RANGE();

            }
            break;
            case 9: // /home/staff/lyczek/viscript.g:1:79: TAB_PARAMS
            {
                mTAB_PARAMS();

            }
            break;
            case 10: // /home/staff/lyczek/viscript.g:1:90: CALL
            {
                mCALL();

            }
            break;
            case 11: // /home/staff/lyczek/viscript.g:1:95: COMPONENT
            {
                mCOMPONENT();

            }
            break;
            case 12: // /home/staff/lyczek/viscript.g:1:105: TAB
            {
                mTAB();

            }
            break;
            case 13: // /home/staff/lyczek/viscript.g:1:109: PROG
            {
                mPROG();

            }
            break;
            case 14: // /home/staff/lyczek/viscript.g:1:114: T__24
            {
                mT__24();

            }
            break;
            case 15: // /home/staff/lyczek/viscript.g:1:120: T__25
            {
                mT__25();

            }
            break;
            case 16: // /home/staff/lyczek/viscript.g:1:126: T__26
            {
                mT__26();

            }
            break;
            case 17: // /home/staff/lyczek/viscript.g:1:132: T__27
            {
                mT__27();

            }
            break;
            case 18: // /home/staff/lyczek/viscript.g:1:138: T__28
            {
                mT__28();

            }
            break;
            case 19: // /home/staff/lyczek/viscript.g:1:144: T__29
            {
                mT__29();

            }
            break;
            case 20: // /home/staff/lyczek/viscript.g:1:150: T__30
            {
                mT__30();

            }
            break;
            case 21: // /home/staff/lyczek/viscript.g:1:156: T__31
            {
                mT__31();

            }
            break;
            case 22: // /home/staff/lyczek/viscript.g:1:162: T__32
            {
                mT__32();

            }
            break;
            case 23: // /home/staff/lyczek/viscript.g:1:168: T__33
            {
                mT__33();

            }
            break;
            case 24: // /home/staff/lyczek/viscript.g:1:174: LOG_OP
            {
                mLOG_OP();

            }
            break;
            case 25: // /home/staff/lyczek/viscript.g:1:181: ADD_OP
            {
                mADD_OP();

            }
            break;
            case 26: // /home/staff/lyczek/viscript.g:1:188: MUL_OP
            {
                mMUL_OP();

            }
            break;
            case 27: // /home/staff/lyczek/viscript.g:1:195: ASSIGN_OP
            {
                mASSIGN_OP();

            }
            break;
            case 28: // /home/staff/lyczek/viscript.g:1:205: ID
            {
                mID();

            }
            break;
            case 29: // /home/staff/lyczek/viscript.g:1:208: INT
            {
                mINT();

            }
            break;
            case 30: // /home/staff/lyczek/viscript.g:1:212: WS
            {
                mWS();

            }
            break;

        }

    }

    protected DFA1 dfa1 = new DFA1(this);
    protected DFA7 dfa7 = new DFA7(this);
    static final String DFA1_eotS
        = "\1\uffff\1\10\1\12\10\uffff";
    static final String DFA1_eofS
        = "\13\uffff";
    static final String DFA1_minS
        = "\1\41\2\75\10\uffff";
    static final String DFA1_maxS
        = "\1\174\2\75\10\uffff";
    static final String DFA1_acceptS
        = "\3\uffff\1\5\1\6\1\7\1\10\1\2\1\1\1\3\1\4";
    static final String DFA1_specialS
        = "\13\uffff}>";
    static final String[] DFA1_transitionS = {
        "\1\4\4\uffff\1\5\25\uffff\1\2\1\3\1\1\75\uffff\1\6",
        "\1\7",
        "\1\11",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""
    };

    static final short[] DFA1_eot = DFA.unpackEncodedString(DFA1_eotS);
    static final short[] DFA1_eof = DFA.unpackEncodedString(DFA1_eofS);
    static final char[] DFA1_min = DFA.unpackEncodedStringToUnsignedChars(DFA1_minS);
    static final char[] DFA1_max = DFA.unpackEncodedStringToUnsignedChars(DFA1_maxS);
    static final short[] DFA1_accept = DFA.unpackEncodedString(DFA1_acceptS);
    static final short[] DFA1_special = DFA.unpackEncodedString(DFA1_specialS);
    static final short[][] DFA1_transition;

    static {
        int numStates = DFA1_transitionS.length;
        DFA1_transition = new short[numStates][];
        for (int i = 0; i < numStates; i++) {
            DFA1_transition[i] = DFA.unpackEncodedString(DFA1_transitionS[i]);
        }
    }

    class DFA1 extends DFA
    {

        public DFA1(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 1;
            this.eot = DFA1_eot;
            this.eof = DFA1_eof;
            this.min = DFA1_min;
            this.max = DFA1_max;
            this.accept = DFA1_accept;
            this.special = DFA1_special;
            this.transition = DFA1_transition;
        }

        public String getDescription()
        {
            return "66:10: ( '>' | '>=' | '<=' | '<' | '==' | '!=' | '&&' | '||' )";
        }
    }

    static final String DFA7_eotS
        = "\1\uffff\12\34\13\uffff\1\53\1\54\1\55\1\54\2\55\3\uffff\2\34\1" +
        "\60\11\34\3\uffff\1\34\1\73\1\uffff\5\34\1\102\4\34\1\uffff\1\107" +
        "\5\34\1\uffff\1\115\1\34\1\117\1\120\1\uffff\1\34\1\122\3\34\1\uffff" +
        "\1\34\2\uffff\1\34\1\uffff\16\34\1\146\3\34\1\152\1\uffff\1\34\1" +
        "\154\1\155\1\uffff\1\156\3\uffff";
    static final String DFA7_eofS
        = "\157\uffff";
    static final String DFA7_minS
        = "\1\11\1\150\1\157\1\146\1\154\1\141\1\145\1\165\2\141\1\162\13\uffff" +
        "\6\75\3\uffff\1\151\1\162\1\60\1\163\1\144\1\156\1\147\1\155\1\142" +
        "\1\154\1\155\1\157\3\uffff\1\154\1\60\1\uffff\1\145\1\137\1\147" +
        "\1\151\1\160\1\60\1\154\1\160\1\147\1\145\1\uffff\1\60\1\162\1\145" +
        "\1\156\1\137\1\160\1\uffff\1\60\1\157\2\60\1\uffff\1\141\1\60\1" +
        "\137\1\162\1\141\1\uffff\1\156\2\uffff\1\156\1\uffff\1\162\1\141" +
        "\1\162\1\145\1\147\1\141\1\156\1\141\1\156\1\145\1\156\1\147\1\155" +
        "\1\164\1\60\1\147\1\145\1\163\1\60\1\uffff\1\145\2\60\1\uffff\1" +
        "\60\3\uffff";
    static final String DFA7_maxS
        = "\1\175\1\150\1\157\1\146\1\156\1\141\1\145\1\165\1\141\1\157\1\162" +
        "\13\uffff\6\75\3\uffff\1\151\1\162\1\172\1\163\1\144\1\156\1\147" +
        "\1\155\1\142\1\154\1\155\1\157\3\uffff\1\154\1\172\1\uffff\1\145" +
        "\1\137\1\147\1\151\1\160\1\172\1\154\1\160\1\147\1\145\1\uffff\1" +
        "\172\1\162\1\145\1\156\1\137\1\160\1\uffff\1\172\1\157\2\172\1\uffff" +
        "\1\141\1\172\1\137\1\162\1\141\1\uffff\1\156\2\uffff\1\156\1\uffff" +
        "\1\162\1\141\1\162\1\145\1\147\1\141\1\156\1\141\1\156\1\145\1\156" +
        "\1\147\1\155\1\164\1\172\1\147\1\145\1\163\1\172\1\uffff\1\145\2" +
        "\172\1\uffff\1\172\3\uffff";
    static final String DFA7_acceptS
        = "\13\uffff\1\16\1\17\1\20\1\21\1\22\1\23\1\24\1\25\1\26\1\27\1\30" +
        "\6\uffff\1\34\1\35\1\36\14\uffff\1\33\1\31\1\32\2\uffff\1\3\12\uffff" +
        "\1\2\6\uffff\1\14\4\uffff\1\4\5\uffff\1\12\1\uffff\1\15\1\1\1\uffff" +
        "\1\5\23\uffff\1\7\3\uffff\1\13\1\uffff\1\10\1\11\1\6";
    static final String DFA7_specialS
        = "\157\uffff}>";
    static final String[] DFA7_transitionS = {
        "\2\36\25\uffff\1\36\1\25\3\uffff\1\30\1\25\1\uffff\1\14\1\15" +
        "\1\32\1\27\1\21\1\31\1\22\1\33\12\35\1\20\1\13\1\25\1\26\1\25" +
        "\2\uffff\32\34\1\23\1\uffff\1\24\1\uffff\1\34\1\uffff\1\34\1" +
        "\6\1\11\1\34\1\4\1\2\2\34\1\3\1\7\5\34\1\12\1\34\1\5\1\34\1" +
        "\10\2\34\1\1\3\34\1\16\1\25\1\17",
        "\1\37",
        "\1\40",
        "\1\41",
        "\1\42\1\uffff\1\43",
        "\1\44",
        "\1\45",
        "\1\46",
        "\1\47",
        "\1\50\15\uffff\1\51",
        "\1\52",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "\1\25",
        "\1\53",
        "\1\53",
        "\1\53",
        "\1\53",
        "\1\53",
        "",
        "",
        "",
        "\1\56",
        "\1\57",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "\1\61",
        "\1\62",
        "\1\63",
        "\1\64",
        "\1\65",
        "\1\66",
        "\1\67",
        "\1\70",
        "\1\71",
        "",
        "",
        "",
        "\1\72",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "",
        "\1\74",
        "\1\75",
        "\1\76",
        "\1\77",
        "\1\100",
        "\12\34\7\uffff\32\34\4\uffff\1\101\1\uffff\32\34",
        "\1\103",
        "\1\104",
        "\1\105",
        "\1\106",
        "",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "\1\110",
        "\1\111",
        "\1\112",
        "\1\113",
        "\1\114",
        "",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "\1\116",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "",
        "\1\121",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "\1\123",
        "\1\124",
        "\1\125",
        "",
        "\1\126",
        "",
        "",
        "\1\127",
        "",
        "\1\130",
        "\1\131",
        "\1\132",
        "\1\133",
        "\1\134",
        "\1\135",
        "\1\136",
        "\1\137",
        "\1\140",
        "\1\141",
        "\1\142",
        "\1\143",
        "\1\144",
        "\1\145",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "\1\147",
        "\1\150",
        "\1\151",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "",
        "\1\153",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "",
        "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
        "",
        "",
        ""
    };

    static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
    static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
    static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
    static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
    static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
    static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
    static final short[][] DFA7_transition;

    static {
        int numStates = DFA7_transitionS.length;
        DFA7_transition = new short[numStates][];
        for (int i = 0; i < numStates; i++) {
            DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
        }
    }

    class DFA7 extends DFA
    {

        public DFA7(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = DFA7_eot;
            this.eof = DFA7_eof;
            this.min = DFA7_min;
            this.max = DFA7_max;
            this.accept = DFA7_accept;
            this.special = DFA7_special;
            this.transition = DFA7_transition;
        }

        public String getDescription()
        {
            return "1:1: Tokens : ( WHILE_OP | FOR_OP | IF_OP | ELSE_OP | RANGE | BEGIN_RANGE | END_RANGE | JUMP_RANGE | TAB_PARAMS | CALL | COMPONENT | TAB | PROG | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | LOG_OP | ADD_OP | MUL_OP | ASSIGN_OP | ID | INT | WS );";
        }
    }

}
