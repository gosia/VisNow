//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.system.config;

import java.awt.Rectangle;
import java.util.Hashtable;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.utils.FloatingPointUtils;
import pl.edu.icm.jscic.utils.InfinityAction;
import pl.edu.icm.jscic.utils.NaNAction;
import pl.edu.icm.visnow.application.application.Application;
import pl.edu.icm.visnow.datamaps.ColorMapManager;
import pl.edu.icm.visnow.datamaps.widgets.ColorMapCellRenderer;
import pl.edu.icm.visnow.lib.basic.viewers.Viewer3D.Viewer3D;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 *
 * @author babor
 */
public class PropertiesEditorPanel extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(PropertiesEditorPanel.class);

    private String preferredMainWindowPosition = "";
    private String preferredViewer3DWindowPosition = "";

    private Hashtable<Integer, JLabel> labels = new Hashtable<Integer, JLabel>();
    private ColorMapCellRenderer comboBoxRenderer;

    /**
     * Creates new form PropertiesEditorPanel
     */
    public PropertiesEditorPanel()
    {
        initComponents();
    }

    public void init()
    {
        JLabel lowEndLabel = new JLabel("low end");
        lowEndLabel.setFont(performanceSlider.getFont());
        labels.put(0, lowEndLabel);
        JLabel stdLabel = new JLabel("standard");
        stdLabel.setFont(performanceSlider.getFont());
        labels.put(2, stdLabel);
        JLabel xtrLabel = new JLabel("extreme");
        xtrLabel.setFont(performanceSlider.getFont());
        labels.put(4, xtrLabel);
        performanceSlider.setLabelTable(labels);
        autoconnectViewerCB.setSelected(VisNow.get().getMainConfig().isAutoconnectViewer());

        startupViewer2DCB.setSelected(VisNow.get().getMainConfig().isStartupViewer2D());
        startupViewer3DCB.setSelected(VisNow.get().getMainConfig().isStartupViewer3D());
        startupFieldViewer3DCB.setSelected(VisNow.get().getMainConfig().isStartupFieldViewer3D());
        startupOrthoViewer3DCB.setSelected(VisNow.get().getMainConfig().isStartupOrthoViewer3D());

        defAppDirTF.setText(VisNow.get().getMainConfig().getDefaultApplicationsPath());
        String tmp = VisNow.get().getMainConfig().getUsableApplicationsPathType();
        if (tmp.equalsIgnoreCase("last")) {
            useAppDirCB.setSelectedItem("last");
        } else if (tmp.equalsIgnoreCase("home")) {
            useAppDirCB.setSelectedItem("home");
        } else if (tmp.equalsIgnoreCase("default")) {
            useAppDirCB.setSelectedItem("default");
        }

        defDatDirTF.setText(VisNow.get().getMainConfig().getDefaultDataPath());
        tmp = VisNow.get().getMainConfig().getUsableDataPathType();
        if (tmp.equalsIgnoreCase("last")) {
            useDatDirCB.setSelectedItem("last");
        } else if (tmp.equalsIgnoreCase("home")) {
            useDatDirCB.setSelectedItem("home");
        } else if (tmp.equalsIgnoreCase("default")) {
            useDatDirCB.setSelectedItem("default");
        }
        infCombo.setSelectedIndex(VisNow.get().getMainConfig().getInfAction());
        nanCombo.setSelectedIndex(VisNow.get().getMainConfig().getNaNAction());
        int nThreads = VisNow.availableProcessors();
        if (nThreads >= Runtime.getRuntime().availableProcessors()) {
            allThreadsRB.setSelected(true);
            threadsSP.setEnabled(false);
            threadsSP.setValue(nThreads);
        } else {
            limitedThreadsRB.setSelected(true);
            threadsSP.setEnabled(true);
            threadsSP.setValue(nThreads);
        }
        comboBoxRenderer = new ColorMapCellRenderer();
        colorMapCombo.setRenderer(comboBoxRenderer);
        colorMapCombo.setModel(ColorMapManager.getInstance().getColorMap1DListModel());
        colorMapCombo.setSelectedIndex(VisNow.get().getMainConfig().getDefaultColorMap());

        preferredMainWindowPosition = VisNow.get().getMainConfig().getMainWindowBounds();
        if (preferredMainWindowPosition == null) preferredMainWindowPosition = "";
        preferredViewer3DWindowPosition = VisNow.get().getMainConfig().getViewer3DWindowBounds();
        if (preferredViewer3DWindowPosition == null) preferredViewer3DWindowPosition = "";

        mainWindowSizeBG.clearSelection();
        viewer3DWindowSizeBG.clearSelection();
        mainWindowAutoButton.setSelected(preferredMainWindowPosition.equals(""));
        mainWindowCurrentButton.setSelected(preferredMainWindowPosition.equals(getMainWindowBounds()));
        viewer3DWindowAutoButton.setSelected(preferredViewer3DWindowPosition.equals(""));
        viewer3DWindowCurrentButton.setSelected(preferredViewer3DWindowPosition.equals(getViewer3DBounds()));
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        mainWindowSizeBG = new javax.swing.ButtonGroup();
        viewer3DWindowSizeBG = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        hardwarePerformancePanel = new javax.swing.JPanel();
        performanceSlider = new javax.swing.JSlider();
        jLabel6 = new javax.swing.JLabel();
        multithreadingAndExceptionPanel = new javax.swing.JPanel();
        multithreadingPanel = new javax.swing.JPanel();
        allThreadsRB = new javax.swing.JRadioButton();
        limitedThreadsRB = new javax.swing.JRadioButton();
        threadsSP = new javax.swing.JSpinner();
        exceptionsPanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        nanCombo = new pl.edu.icm.visnow.gui.widgets.SteppedComboBox();
        infCombo = new pl.edu.icm.visnow.gui.widgets.SteppedComboBox();
        windowSizePanel = new javax.swing.JPanel();
        mainWindowSizePanel = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        mainWindowCurrentButton = new javax.swing.JToggleButton();
        mainWindowAutoButton = new javax.swing.JToggleButton();
        viewer3DWindowSizePanel = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        viewer3DWindowCurrentButton = new javax.swing.JToggleButton();
        viewer3DWindowAutoButton = new javax.swing.JToggleButton();
        viewerDefaultPanel = new javax.swing.JPanel();
        startupViewer2DCB = new javax.swing.JCheckBox();
        startupViewer3DCB = new javax.swing.JCheckBox();
        startupFieldViewer3DCB = new javax.swing.JCheckBox();
        startupOrthoViewer3DCB = new javax.swing.JCheckBox();
        viewerAutoConnectPanel = new javax.swing.JPanel();
        autoconnectViewerCB = new javax.swing.JCheckBox();
        autoconnectOrthoViewer3DCB = new javax.swing.JCheckBox();
        fileChooserPanel = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        defAppDirTF = new javax.swing.JTextField();
        defAppDirBB = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        useAppDirCB = new javax.swing.JComboBox();
        dataFileChooserPanel = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        defDatDirTF = new javax.swing.JTextField();
        defDatDirBB = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        useDatDirCB = new javax.swing.JComboBox();
        unitPanel = new javax.swing.JPanel();
        lengthUnitCombo = new javax.swing.JComboBox();
        timeUnitCombo = new javax.swing.JComboBox();
        velocityUnitCombo = new javax.swing.JComboBox();
        colormapPanel = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        colorMapCombo = new javax.swing.JComboBox();
        filler = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("VisNow properties");
        jLabel1.setName("jLabel1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        add(jLabel1, gridBagConstraints);

        hardwarePerformancePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Hardware performance"));
        hardwarePerformancePanel.setName("hardwarePerformancePanel"); // NOI18N
        hardwarePerformancePanel.setLayout(new java.awt.GridBagLayout());

        performanceSlider.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        performanceSlider.setMajorTickSpacing(2);
        performanceSlider.setMaximum(4);
        performanceSlider.setMinorTickSpacing(1);
        performanceSlider.setPaintLabels(true);
        performanceSlider.setToolTipText("<html>this slider controls continuous update<p>of data mapping (colors, transparency etc.<lp>for low end machines only small objects will be continuously updated<p>to avoid annoying sliders' behavior</html>"); // NOI18N
        performanceSlider.setValue(1);
        performanceSlider.setName("performanceSlider"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        hardwarePerformancePanel.add(performanceSlider, gridBagConstraints);

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel6.setText("<html>This slider controls continuous update of data mapping (colors, transparency etc.)<br/>\nFor low end machines only small objects will be continuously updated<br/>to avoid annoying sliders' behavior</html>");
        jLabel6.setName("jLabel6"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        hardwarePerformancePanel.add(jLabel6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(hardwarePerformancePanel, gridBagConstraints);

        multithreadingAndExceptionPanel.setName("multithreadingAndExceptionPanel"); // NOI18N
        multithreadingAndExceptionPanel.setLayout(new java.awt.GridBagLayout());

        multithreadingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Multithreading"));
        multithreadingPanel.setName("multithreadingPanel"); // NOI18N
        multithreadingPanel.setLayout(new java.awt.GridBagLayout());

        buttonGroup1.add(allThreadsRB);
        allThreadsRB.setSelected(true);
        allThreadsRB.setText("Use all available CPU cores");
        allThreadsRB.setName("allThreadsRB"); // NOI18N
        allThreadsRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                allThreadsRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        multithreadingPanel.add(allThreadsRB, gridBagConstraints);

        buttonGroup1.add(limitedThreadsRB);
        limitedThreadsRB.setText("Limit number of CPU cores to:");
        limitedThreadsRB.setName("limitedThreadsRB"); // NOI18N
        limitedThreadsRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                limitedThreadsRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weighty = 1.0;
        multithreadingPanel.add(limitedThreadsRB, gridBagConstraints);

        threadsSP.setEnabled(false);
        threadsSP.setMinimumSize(new java.awt.Dimension(48, 20));
        threadsSP.setName("threadsSP"); // NOI18N
        threadsSP.setPreferredSize(new java.awt.Dimension(48, 20));
        threadsSP.setValue(Runtime.getRuntime().availableProcessors());
        threadsSP.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                threadsSPStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 0);
        multithreadingPanel.add(threadsSP, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        multithreadingAndExceptionPanel.add(multithreadingPanel, gridBagConstraints);

        exceptionsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Action on exceptional values"));
        exceptionsPanel.setName("exceptionsPanel"); // NOI18N
        exceptionsPanel.setLayout(new java.awt.GridBagLayout());

        jLabel7.setText("On NaN");
        jLabel7.setName("jLabel7"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        exceptionsPanel.add(jLabel7, gridBagConstraints);

        jLabel8.setText("On Infinity ");
        jLabel8.setName("jLabel8"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        exceptionsPanel.add(jLabel8, gridBagConstraints);

        nanCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "set 0", "set number min", "set number max", "set data min", "set data max", "throw exception" }));
        nanCombo.setMinimumSize(new java.awt.Dimension(100, 24));
        nanCombo.setName("nanCombo"); // NOI18N
        nanCombo.setPreferredSize(new java.awt.Dimension(100, 24));
        nanCombo.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                nanComboActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        exceptionsPanel.add(nanCombo, gridBagConstraints);

        infCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "set 0", "set number extreme", "set data extreme", "throw exception" }));
        infCombo.setMinimumSize(new java.awt.Dimension(100, 24));
        infCombo.setName("infCombo"); // NOI18N
        infCombo.setPreferredSize(new java.awt.Dimension(100, 24));
        infCombo.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                infComboActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        exceptionsPanel.add(infCombo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        multithreadingAndExceptionPanel.add(exceptionsPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(multithreadingAndExceptionPanel, gridBagConstraints);

        windowSizePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Window position on startup"));
        windowSizePanel.setName("windowSizePanel"); // NOI18N
        windowSizePanel.setLayout(new java.awt.GridBagLayout());

        mainWindowSizePanel.setName("mainWindowSizePanel"); // NOI18N
        mainWindowSizePanel.setLayout(new java.awt.GridBagLayout());

        jLabel11.setText("Main window:");
        jLabel11.setName("jLabel11"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        mainWindowSizePanel.add(jLabel11, gridBagConstraints);

        mainWindowSizeBG.add(mainWindowCurrentButton);
        mainWindowCurrentButton.setText("Use current position");
        mainWindowCurrentButton.setName("mainWindowCurrentButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 8);
        mainWindowSizePanel.add(mainWindowCurrentButton, gridBagConstraints);

        mainWindowSizeBG.add(mainWindowAutoButton);
        mainWindowAutoButton.setText("Default");
        mainWindowAutoButton.setName("mainWindowAutoButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        mainWindowSizePanel.add(mainWindowAutoButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 16, 4, 4);
        windowSizePanel.add(mainWindowSizePanel, gridBagConstraints);

        viewer3DWindowSizePanel.setName("viewer3DWindowSizePanel"); // NOI18N
        viewer3DWindowSizePanel.setLayout(new java.awt.GridBagLayout());

        jLabel12.setText("Viewer3D window:");
        jLabel12.setName("jLabel12"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        viewer3DWindowSizePanel.add(jLabel12, gridBagConstraints);

        viewer3DWindowSizeBG.add(viewer3DWindowCurrentButton);
        viewer3DWindowCurrentButton.setText("Use current position");
        viewer3DWindowCurrentButton.setName("viewer3DWindowCurrentButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 8);
        viewer3DWindowSizePanel.add(viewer3DWindowCurrentButton, gridBagConstraints);

        viewer3DWindowSizeBG.add(viewer3DWindowAutoButton);
        viewer3DWindowAutoButton.setText("Default");
        viewer3DWindowAutoButton.setName("viewer3DWindowAutoButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        viewer3DWindowSizePanel.add(viewer3DWindowAutoButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 16, 4, 4);
        windowSizePanel.add(viewer3DWindowSizePanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(windowSizePanel, gridBagConstraints);

        viewerDefaultPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Startup viewers"));
        viewerDefaultPanel.setName("viewerDefaultPanel"); // NOI18N
        viewerDefaultPanel.setLayout(new java.awt.GridLayout(2, 2));

        startupViewer2DCB.setText("Viewer 2D");
        startupViewer2DCB.setToolTipText("Initialize Viewer 2D module at VisNow startup");
        startupViewer2DCB.setName("startupViewer2DCB"); // NOI18N
        viewerDefaultPanel.add(startupViewer2DCB);

        startupViewer3DCB.setText("Viewer 3D");
        startupViewer3DCB.setToolTipText("Initialize Viewer 3D module at VisNow startup");
        startupViewer3DCB.setName("startupViewer3DCB"); // NOI18N
        viewerDefaultPanel.add(startupViewer3DCB);

        startupFieldViewer3DCB.setText("Field Viewer 3D");
        startupFieldViewer3DCB.setToolTipText("Initialize Field Viewer 3D module at VisNow startup");
        startupFieldViewer3DCB.setName("startupFieldViewer3DCB"); // NOI18N
        viewerDefaultPanel.add(startupFieldViewer3DCB);

        startupOrthoViewer3DCB.setText("Orthoviewer 3D");
        startupOrthoViewer3DCB.setName("startupOrthoViewer3DCB"); // NOI18N
        viewerDefaultPanel.add(startupOrthoViewer3DCB);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(viewerDefaultPanel, gridBagConstraints);

        viewerAutoConnectPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Auto connect"));
        viewerAutoConnectPanel.setName("viewerAutoConnectPanel"); // NOI18N
        viewerAutoConnectPanel.setLayout(new java.awt.GridLayout(1, 0));

        buttonGroup2.add(autoconnectViewerCB);
        autoconnectViewerCB.setText("To latest Viewer 2D/3D");
        autoconnectViewerCB.setToolTipText("Automatically connect module geometry output to Viewer 3D");
        autoconnectViewerCB.setName("autoconnectViewerCB"); // NOI18N
        viewerAutoConnectPanel.add(autoconnectViewerCB);

        buttonGroup2.add(autoconnectOrthoViewer3DCB);
        autoconnectOrthoViewer3DCB.setText("To Orthoviewer 3D");
        autoconnectOrthoViewer3DCB.setName("autoconnectOrthoViewer3DCB"); // NOI18N
        viewerAutoConnectPanel.add(autoconnectOrthoViewer3DCB);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(viewerAutoConnectPanel, gridBagConstraints);

        fileChooserPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Application file chooser"));
        fileChooserPanel.setName("fileChooserPanel"); // NOI18N
        fileChooserPanel.setLayout(new java.awt.GridBagLayout());

        jPanel7.setName("jPanel7"); // NOI18N
        jPanel7.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("Default application directory:");
        jLabel3.setName("jLabel3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        jPanel7.add(jLabel3, gridBagConstraints);

        defAppDirTF.setName("defAppDirTF"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 15, 0, 0);
        jPanel7.add(defAppDirTF, gridBagConstraints);

        defAppDirBB.setText("Browse...");
        defAppDirBB.setName("defAppDirBB"); // NOI18N
        defAppDirBB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                defAppDirBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 5);
        jPanel7.add(defAppDirBB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        fileChooserPanel.add(jPanel7, gridBagConstraints);

        jPanel8.setName("jPanel8"); // NOI18N
        jPanel8.setLayout(new java.awt.GridBagLayout());

        jLabel5.setText("Start file chooser in:");
        jLabel5.setName("jLabel5"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        jPanel8.add(jLabel5, gridBagConstraints);

        useAppDirCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "default", "last", "home" }));
        useAppDirCB.setName("useAppDirCB"); // NOI18N
        useAppDirCB.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel8.add(useAppDirCB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        fileChooserPanel.add(jPanel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(fileChooserPanel, gridBagConstraints);

        dataFileChooserPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Data file choosers"));
        dataFileChooserPanel.setName("dataFileChooserPanel"); // NOI18N
        dataFileChooserPanel.setLayout(new java.awt.GridBagLayout());

        jPanel4.setName("jPanel4"); // NOI18N
        jPanel4.setLayout(new java.awt.GridBagLayout());

        jLabel2.setText("Default data directory:");
        jLabel2.setName("jLabel2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        jPanel4.add(jLabel2, gridBagConstraints);

        defDatDirTF.setName("defDatDirTF"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 15, 0, 0);
        jPanel4.add(defDatDirTF, gridBagConstraints);

        defDatDirBB.setText("Browse...");
        defDatDirBB.setName("defDatDirBB"); // NOI18N
        defDatDirBB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                defDatDirBBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 5);
        jPanel4.add(defDatDirBB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        dataFileChooserPanel.add(jPanel4, gridBagConstraints);

        jPanel6.setName("jPanel6"); // NOI18N
        jPanel6.setLayout(new java.awt.GridBagLayout());

        jLabel4.setText("Start file chooser in:");
        jLabel4.setName("jLabel4"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        jPanel6.add(jLabel4, gridBagConstraints);

        useDatDirCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "default", "last", "home" }));
        useDatDirCB.setName("useDatDirCB"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel6.add(useDatDirCB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        dataFileChooserPanel.add(jPanel6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(dataFileChooserPanel, gridBagConstraints);

        unitPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Preferred units"));
        unitPanel.setName("unitPanel"); // NOI18N
        unitPanel.setLayout(new java.awt.GridLayout(1, 0));

        lengthUnitCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "millimeter", "centimeter", "meter", "kilometer", " " }));
        lengthUnitCombo.setSelectedIndex(2);
        lengthUnitCombo.setBorder(javax.swing.BorderFactory.createTitledBorder("length/distance"));
        lengthUnitCombo.setName("lengthUnitCombo"); // NOI18N
        unitPanel.add(lengthUnitCombo);

        timeUnitCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "microsecond", "milisecond", "second", "minute", "hour", "day", "year", " " }));
        timeUnitCombo.setSelectedIndex(2);
        timeUnitCombo.setBorder(javax.swing.BorderFactory.createTitledBorder("time"));
        timeUnitCombo.setName("timeUnitCombo"); // NOI18N
        unitPanel.add(timeUnitCombo);

        velocityUnitCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "cm/sec", "m/sec", "km/h", "c" }));
        velocityUnitCombo.setSelectedIndex(1);
        velocityUnitCombo.setBorder(javax.swing.BorderFactory.createTitledBorder("velocity"));
        velocityUnitCombo.setName("velocityUnitCombo"); // NOI18N
        unitPanel.add(velocityUnitCombo);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(unitPanel, gridBagConstraints);

        colormapPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Colormap"));
        colormapPanel.setName("colormapPanel"); // NOI18N
        colormapPanel.setLayout(new java.awt.GridBagLayout());

        jLabel9.setText("Preferred colormap:");
        jLabel9.setName("jLabel9"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 8);
        colormapPanel.add(jLabel9, gridBagConstraints);

        colorMapCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        colorMapCombo.setName("colorMapCombo"); // NOI18N
        colorMapCombo.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                colorMapComboItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        colormapPanel.add(colorMapCombo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(colormapPanel, gridBagConstraints);

        filler.setName("filler"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void defAppDirBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_defAppDirBBActionPerformed
        JFileChooser chooser = new JFileChooser(VisNow.get().getMainConfig().getDefaultApplicationsPath());
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            defAppDirTF.setText(chooser.getSelectedFile().getAbsolutePath());
        }
    }//GEN-LAST:event_defAppDirBBActionPerformed

    private void defDatDirBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_defDatDirBBActionPerformed
        JFileChooser chooser = new JFileChooser(VisNow.get().getMainConfig().getDefaultDataPath());
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            defDatDirTF.setText(chooser.getSelectedFile().getAbsolutePath());
        }
    }//GEN-LAST:event_defDatDirBBActionPerformed

    private void threadsSPStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_threadsSPStateChanged
        int n = (Integer) threadsSP.getValue();
        int N = Runtime.getRuntime().availableProcessors();
        if (n < 1) {
            threadsSP.setValue(1);
            return;
        }

        if (n > N) {
            threadsSP.setValue(N);
            return;
        }
    }//GEN-LAST:event_threadsSPStateChanged

    private void allThreadsRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allThreadsRBActionPerformed
        threadsSP.setEnabled(limitedThreadsRB.isSelected());
    }//GEN-LAST:event_allThreadsRBActionPerformed

    private void limitedThreadsRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limitedThreadsRBActionPerformed
        threadsSP.setEnabled(limitedThreadsRB.isSelected());
    }//GEN-LAST:event_limitedThreadsRBActionPerformed

    private void nanComboActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_nanComboActionPerformed
    {//GEN-HEADEREND:event_nanComboActionPerformed
        FloatingPointUtils.defaultNanAction = NaNAction.values()[nanCombo.getSelectedIndex()];
    }//GEN-LAST:event_nanComboActionPerformed

    private void infComboActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_infComboActionPerformed
    {//GEN-HEADEREND:event_infComboActionPerformed
        FloatingPointUtils.defaultInfinityAction = InfinityAction.values()[infCombo.getSelectedIndex()];
    }//GEN-LAST:event_infComboActionPerformed

    private void colorMapComboItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_colorMapComboItemStateChanged
    {//GEN-HEADEREND:event_colorMapComboItemStateChanged
        int k = colorMapCombo.getSelectedIndex();

    }//GEN-LAST:event_colorMapComboItemStateChanged

    private String getMainWindowBounds()
    {
        Rectangle bounds = VisNow.get().getMainWindow().getBounds();
        return bounds.x + "," + bounds.y + "," + bounds.width + "," + bounds.height;
    }

    private String getViewer3DBounds()
    {
        Application currentApplication = VisNow.get().getMainWindow().getApplicationsPanel().getCurrentApplication();
        if (currentApplication == null) return null;
        else {
            Viewer3D v = currentApplication.getViewer3D();
            if (v == null) return "";
            else {
                Rectangle bounds = v.getWindow().getBounds();
                return bounds.x + "," + bounds.y + "," + bounds.width + "," + bounds.height;
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton allThreadsRB;
    private javax.swing.JCheckBox autoconnectOrthoViewer3DCB;
    private javax.swing.JCheckBox autoconnectViewerCB;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JComboBox colorMapCombo;
    private javax.swing.JPanel colormapPanel;
    private javax.swing.JPanel dataFileChooserPanel;
    private javax.swing.JButton defAppDirBB;
    private javax.swing.JTextField defAppDirTF;
    private javax.swing.JButton defDatDirBB;
    private javax.swing.JTextField defDatDirTF;
    private javax.swing.JPanel exceptionsPanel;
    private javax.swing.JPanel fileChooserPanel;
    private javax.swing.Box.Filler filler;
    private javax.swing.JPanel hardwarePerformancePanel;
    private pl.edu.icm.visnow.gui.widgets.SteppedComboBox infCombo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JComboBox lengthUnitCombo;
    private javax.swing.JRadioButton limitedThreadsRB;
    private javax.swing.JToggleButton mainWindowAutoButton;
    private javax.swing.JToggleButton mainWindowCurrentButton;
    private javax.swing.ButtonGroup mainWindowSizeBG;
    private javax.swing.JPanel mainWindowSizePanel;
    private javax.swing.JPanel multithreadingAndExceptionPanel;
    private javax.swing.JPanel multithreadingPanel;
    private pl.edu.icm.visnow.gui.widgets.SteppedComboBox nanCombo;
    private javax.swing.JSlider performanceSlider;
    private javax.swing.JCheckBox startupFieldViewer3DCB;
    private javax.swing.JCheckBox startupOrthoViewer3DCB;
    private javax.swing.JCheckBox startupViewer2DCB;
    private javax.swing.JCheckBox startupViewer3DCB;
    private javax.swing.JSpinner threadsSP;
    private javax.swing.JComboBox timeUnitCombo;
    private javax.swing.JPanel unitPanel;
    private javax.swing.JComboBox useAppDirCB;
    private javax.swing.JComboBox useDatDirCB;
    private javax.swing.JComboBox velocityUnitCombo;
    private javax.swing.JToggleButton viewer3DWindowAutoButton;
    private javax.swing.JToggleButton viewer3DWindowCurrentButton;
    private javax.swing.ButtonGroup viewer3DWindowSizeBG;
    private javax.swing.JPanel viewer3DWindowSizePanel;
    private javax.swing.JPanel viewerAutoConnectPanel;
    private javax.swing.JPanel viewerDefaultPanel;
    private javax.swing.JPanel windowSizePanel;
    // End of variables declaration//GEN-END:variables
    public void apply()
    {

        VisNow.get().getMainConfig().setStartupViewer2D(startupViewer2DCB.isSelected());
        VisNow.get().getMainConfig().setStartupViewer3D(startupViewer3DCB.isSelected());
        VisNow.get().getMainConfig().setStartupFieldViewer3D(startupFieldViewer3DCB.isSelected());
        VisNow.get().getMainConfig().setAutoconnectViewer(autoconnectViewerCB.isSelected());
        VisNow.get().getMainConfig().setStartupOrthoViewer3D(startupOrthoViewer3DCB.isSelected());
        VisNow.get().getMainConfig().setAutoconnectOrthoViewer3D(autoconnectOrthoViewer3DCB.isSelected());
        VisNow.get().getMainConfig().setDefaultApplicationsPath(defAppDirTF.getText());
        VisNow.get().getMainConfig().setUsableApplicationsPathType((String) useAppDirCB.getSelectedItem());
        VisNow.get().getMainConfig().setDefaultDataPath(defDatDirTF.getText());
        VisNow.get().getMainConfig().setUsableDataPathType((String) useDatDirCB.getSelectedItem());
        VisNow.get().getMainConfig().setColorAdjustingLimit(1 << 4 * performanceSlider.getValue() + 13);
        VisNow.get().getMainConfig().setInfAction(infCombo.getSelectedIndex());
        VisNow.get().getMainConfig().setNaNAction(nanCombo.getSelectedIndex());
        VisNow.get().getMainConfig().setDefaultLengthUnit((String) lengthUnitCombo.getSelectedItem());
        VisNow.get().getMainConfig().setDefaultTimeUnit((String) timeUnitCombo.getSelectedItem());
        VisNow.get().getMainConfig().setDefaultVelocityUnit((String) velocityUnitCombo.getSelectedItem());
        VisNow.get().getMainConfig().setNAvailableThreads(allThreadsRB.isSelected()
                ? Runtime.getRuntime().availableProcessors()
                : (Integer) threadsSP.getValue());
        VisNow.get().getMainConfig().setPerformance(performanceSlider.getValue());
        VisNow.get().getMainConfig().setDefaultColorMap(colorMapCombo.getSelectedIndex());

        if (mainWindowAutoButton.isSelected()) VisNow.get().getMainConfig().setMainWindowBounds("");
        else if (mainWindowCurrentButton.isSelected()) VisNow.get().getMainConfig().setMainWindowBounds(getMainWindowBounds());
        if (viewer3DWindowAutoButton.isSelected()) VisNow.get().getMainConfig().setViewer3DWindowBounds("");
        else if (viewer3DWindowCurrentButton.isSelected() && getViewer3DBounds() != null) VisNow.get().getMainConfig().setViewer3DWindowBounds(getViewer3DBounds());
        VisNow.get().getMainConfig().saveConfig();

    }
}
