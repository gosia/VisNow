@echo off

set JAVA_HOME=$JAVA_HOME
set PATH=%JAVA_HOME%\bin;%PATH%

set VN_PATH=%~dp0
set VN_CLASSPATH=%VN_PATH%\VisNow.jar
set NATIVE_PATH=%VN_PATH%lib\native

for /F "tokens=*" %%i in ('java -cp "%VN_CLASSPATH%" pl.edu.icm.visnow.system.main.WindowsRamTest') do set TOT_MEM=%%i
set JVM_XMX=%TOT_MEM%M
echo Memory available for VisNow: %JVM_XMX%B

java -Xmx%JVM_XMX% -cp "%VN_CLASSPATH%" -splash:splash07.png -Dfile.encoding=utf-8 $APP-STARTCLASS %*

