/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.icm.visnow.izpack;

import com.izforge.izpack.installer.AutomatedInstallData;
import com.izforge.izpack.installer.DataValidator;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class TargetPathValidator implements com.izforge.izpack.installer.DataValidator
{

    @Override
    public Status validateData(AutomatedInstallData aid)
    {

        if (aid.getRules().isConditionTrue("izpack.macinstall")) {
            String installPath = aid.getInstallPath();
            if (!installPath.endsWith(".app")) {
                aid.setInstallPath(installPath + ".app");
            }
        }

        //        if(aid.getRules().isConditionTrue("izpack.linuxinstall")) {
        //            String installPath = aid.getInstallPath();
        //            if(installPath.contains(" ")) {
        //                aid.setInstallPath(installPath.replace(" ", "_"));
        //            }
        //        }        
        return DataValidator.Status.OK;
    }

    @Override
    public String getErrorMessageId()
    {
        return "ERROR";
    }

    @Override
    public String getWarningMessageId()
    {
        return "WARNING";
    }

    @Override
    public boolean getDefaultAnswer()
    {
        return true;
    }

}
