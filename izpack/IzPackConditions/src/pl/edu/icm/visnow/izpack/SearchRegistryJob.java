/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.icm.visnow.izpack;

import com.izforge.izpack.util.AbstractUIProcessHandler;
import com.izforge.izpack.util.Debug;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class SearchRegistryJob
{

    private static final String j3dRegistrySearchCommand = "@echo off\necho Searching Registry for Java3D Installs\nfor /f %%I in ('reg query HKLM\\SOFTWARE\\microsoft\\windows\\currentversion\\uninstall') do echo %%I | find \"{\" > nul && call :All-Installations %%I\necho Search Complete..\ngoto :EOF\n\n:All-Installations\nfor /f \"tokens=2*\" %%T in ('reg query %1 /v DisplayName 2^> nul') do echo . Found - %%U: | find \"Java 3D\" && call :Sun-Java3D-Installs %~n1\ngoto :EOF\n\n:Sun-Java3D-Installs\nset JAVA3D_REGISTRY_FOUND=true\nset JAVA3D_REGISTRY_PATH=%1\necho . Resuming Search..\ngoto :EOF\n\n:Quit\nif \"%JAVA3D_REGISTRY_FOUND%\"==\"true\" (\nexit /B 0\n) else (\nexit /B 1\n)\n";

    public static void run(AbstractUIProcessHandler handler, String[] args)
    {
        //search windows registry
        try {
            Debug.trace("CustomValidator: searching Windows registry for Java3D installations");
            String tmpDir = System.getProperty("java.io.tmpdir");
            String tmpFile = tmpDir + "j3d_search_registry.bat";
            Debug.trace("CustomValidator: creating script " + tmpFile);
            File tmpf = new File(tmpFile);
            if (tmpf.exists()) {
                if (!tmpf.delete())
                    throw new IOException();
            }
            BufferedWriter out = new BufferedWriter(new FileWriter(tmpf));
            out.write(j3dRegistrySearchCommand);
            out.close();

            Process p = Runtime.getRuntime().exec(tmpFile);
            p.waitFor();
            int exitValue = p.exitValue();
            System.out.println("CustomValidator: execute exit value: " + exitValue + " - registry entry " + (exitValue == 0 ? "found" : "not found"));
        } catch (IOException ex) {
            Debug.trace("CustomValidator: canot search Windows registry, error writting search script");
        } catch (InterruptedException ex) {
            Debug.trace("CustomValidator: canot search Windows registry, error executing search script");
        }
    }

    private SearchRegistryJob()
    {
    }

}
