/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.icm.visnow.izpack;

import com.izforge.izpack.installer.AutomatedInstallData;
import com.izforge.izpack.installer.DataValidator;
import com.izforge.izpack.util.Debug;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class CustomValidator implements com.izforge.izpack.installer.DataValidator
{

    private static final String ERROR_MESSAGE_JAVA_VERSION = "ERROR!\n\nJava version too low.\nVisNow requires at least Java 1.6.\n\nPlease upgrade Java or contact\nyour system adminstrator.\n\n";
    private String errorMessage = "";
    private String warningMessage = "";

    @Override
    public Status validateData(AutomatedInstallData aid)
    {

        //validate java version        
        String jv = System.getProperty("java.version");
        jv = jv.substring(0, 5);
        int jVersion0 = 0;
        int jVersion1 = 0;
        int jVersion2 = 0;
        jVersion0 = Integer.parseInt(jv.substring(0, 1));
        jVersion1 = Integer.parseInt(jv.substring(2, 3));
        jVersion2 = Integer.parseInt(jv.substring(4, 5));
        if (jVersion0 < 1 || (jVersion0 == 1 && jVersion1 < 6)) {
            Debug.trace("CustomValidator: Java version validation failed");
            errorMessage = ERROR_MESSAGE_JAVA_VERSION;
            return DataValidator.Status.ERROR;
        }
        Debug.trace("CustomValidator: Java version validation succeded");

        String homeDirPath = System.getProperty("user.home");
        String os = System.getProperty("os.name");
        String testAppPath, finalAppPath, appDir;
        File f;
        boolean ok;

        if (os.startsWith("Windows")) {
            //windows
            testAppPath = System.getenv("PROGRAMFILES");
            appDir = aid.getVariable("APP_NAME") + File.separator + "v" + aid.getVariable("APP_VER");
        } else if (os.startsWith("Mac")) {
            //macos
            testAppPath = "/Applications";
            appDir = aid.getVariable("APP_NAME") + " v" + aid.getVariable("APP_VER");
        } else {
            //assume unix
            testAppPath = "/opt";
            appDir = aid.getVariable("APP_NAME") + File.separator + "v" + aid.getVariable("APP_VER");
        }

        f = new File(testAppPath);
        if (f.exists() && f.canWrite()) {
            try {
                File ff = new File(testAppPath + File.separator + "test.file");
                FileOutputStream fos = new FileOutputStream(ff);
                OutputStreamWriter out = new OutputStreamWriter(fos, "UTF-8");
                out.write("test", 0, 4);
                out.close();
                ff.delete();

                ok = true;
                finalAppPath = testAppPath;
            } catch (Exception ex) {
                ok = false;
                finalAppPath = homeDirPath;
            }
        } else {
            ok = false;
            finalAppPath = homeDirPath;
        }
        Debug.trace("CustomValidator: " + os + " with" + (ok ? "" : "out") + " " + testAppPath + " write permission. Setting installation path to " + finalAppPath);
        aid.setVariable("APPLICATIONS_DEFAULT_ROOT", finalAppPath);
        aid.setVariable("INSTALL_PATH", finalAppPath + File.separator + appDir);

        return DataValidator.Status.OK;
    }

    @Override
    public String getErrorMessageId()
    {
        return errorMessage;
    }

    @Override
    public String getWarningMessageId()
    {
        return warningMessage;
    }

    @Override
    public boolean getDefaultAnswer()
    {
        return false;
    }

}
