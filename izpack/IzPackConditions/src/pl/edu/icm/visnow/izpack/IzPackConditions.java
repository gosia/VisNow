/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.icm.visnow.izpack;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class IzPackConditions
{

    public static boolean IS_ARCH64 = false;
    //public static boolean IS_JAVA3D_PRESENT = false;
    //public static boolean IS_JAVA3D_VERSION = false;

    static {

        //os architecture check
        String arch = System.getProperty("os.arch");
        IS_ARCH64 = arch.contains("64");

        //        //Java3D presence check
        //        try {
        //            Class cl = Class.forName("javax.media.j3d.VirtualUniverse"); 
        //            IS_JAVA3D_PRESENT = true;
        //        } catch (Exception ex) {
        //            IS_JAVA3D_PRESENT = false;
        //        }
        //        //Java3D version check
        //        try {
        //            Class cl = Class.forName("javax.media.j3d.VirtualUniverse");            
        //            IS_JAVA3D_VERSION = Java3DChecker.checkJava3DVersion();
        //        } catch (Exception ex) {
        //            IS_JAVA3D_VERSION = false;
        //        }
    }

    private IzPackConditions()
    {
    }

}
