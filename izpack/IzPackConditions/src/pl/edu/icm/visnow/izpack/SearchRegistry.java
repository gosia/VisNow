/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.icm.visnow.izpack;

import at.jta.Key;
import at.jta.Regor;
import com.izforge.izpack.util.Debug;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.BackingStoreException;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class SearchRegistry
{

    public static String[] searchJava3DEntries() throws BackingStoreException
    {
        Debug.trace("SearchRegistry: starting registry search");
        ArrayList<String> entries = new ArrayList<String>();
        String basePath = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall";

        try {

            Regor reg = new Regor();
            Key HKLM = new Key(Regor._HKEY_LOCAL_MACHINE, "");
            Key uninstallKey = reg.openKey(HKLM, basePath);
            List uninstallList = reg.listKeys(uninstallKey);

            Key entryKey;
            String displayName;
            String publisher;
            String fullPath;
            for (int i = 0; i < uninstallList.size(); i++) {
                String entryStr = (String) uninstallList.get(i);
                if (!entryStr.startsWith("{"))
                    continue;

                entryKey = reg.openKey(HKLM, basePath + "\\" + entryStr);

                publisher = reg.readValueAsString(entryKey, "Publisher");
                displayName = reg.readValueAsString(entryKey, "DisplayName");

                if (publisher == null || displayName == null)
                    continue;

                if (publisher.startsWith("Sun") && displayName.startsWith("Java 3D")) {
                    fullPath = "HKEY_LOCAL_MACHINE\\" + basePath + "\\" + entryStr;
                    Debug.trace("SearchRegistry: found entry at " + fullPath);
                    //entries.add(fullPath);
                    entries.add(entryStr);
                }
            }

        } catch (Exception ex) {
            Debug.trace("SearchRegistry: error searching registry");
            ex.printStackTrace();
            return null;
        }

        String[] out = null;
        if (!entries.isEmpty()) {
            out = new String[entries.size()];
            for (int i = 0; i < out.length; i++) {
                out[i] = entries.get(i);
            }
        }
        return out;
    }

    private SearchRegistry()
    {
    }

}
