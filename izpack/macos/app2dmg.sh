#!/bin/sh

if [ ! $# -eq 2 ] 
then
	echo "usage: app2dmg.sh APPDIR DMGFILE"
	exit
fi

APP_DIR=$1
OUT_DMG=$2
TMP_DIR=/tmp/izpack_tmp

SIZE=`du -s $APP_DIR | awk '{ print $1 }'`
SIZE=`expr $SIZE + 5000`

if [ -f $OUT_DMG ]
then
	rm -f $OUT_DMG
fi

dd if=/dev/zero of=$OUT_DMG bs=1k count=$SIZE
#hformat -l VisNowInstaller $OUT_DMG
/usr/sbin/mkfs.hfsplus -v 'VisNowInstaller' $OUT_DMG


rm -rf $TMP_DIR
mkdir $TMP_DIR
mount -t hfsplus -o loop $OUT_DMG $TMP_DIR
cp -r $APP_DIR $TMP_DIR
umount $TMP_DIR
rm -rf $TMP_DIR
